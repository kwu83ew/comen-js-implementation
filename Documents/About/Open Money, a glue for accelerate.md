# Open Money, a glue for accelerate Open Movement

Imagine the open software developers have a common value-presenter to share together all through the world.
This value-presenter presents the fact that the owner was dedicated X amount of time to design/develop/test an open source software, and because of that good job she/he/organization earned a certain amount of special type of coins as a receipt. So these coins an be spent in order to purchase goods or services or …

If you insist on voluntary contribution that’s OK too. What if you donate whole the coins you earned for your contribution to a charity or organization? It is a good incentive for all parties. Whether contributor, the software or charity or organization to resonate each other. Take a look at "opensource-booster" flowchart.

Hopefully everything should be clear, except the treasury concept. The thing is our imaginary coins must be created beforehand, to be distributable and spendable as well. So we launch a treasury that regularly mints fix amount of coins every day. Now we have coins and we can distribute it between people who did some improvement on an Open Source software, or even better, we can distribute coins between who did something good for world improvement. In such a way we can expand all of goodness we are strive for in open source, all over the glob. 

You may ask Who define what is good for world and how we can qualify the issue? 
Actually there is not a single entity or organization to be able to answer to this question. There is also the problem of “what you think is good, is not what I think is good”. The only solution is voting. Voting for every single decision. Is it feasible? In old classical human interaction it was too costly and nearly impossible, but with blockchain technology it is possible. Although it could be more efficient if we implement a kind of delegation “sub-system”. I intentionally name sub-system, because what we are going to implement is a liquid-democracy, in which everyone can vote individually or delegate it to parties or organizations or a group of specialists. Adding ZKP (Zero Knowledge Proof) on top of it, we have capacity for running thousand polling per day for entire world population with strong privacy protection. 
Is it really & technically possible? Short answer, Yes. 
Recap:
- Volunteers join to network
- There is a long TODO list of software developments, tests, designs, surveys, documenting, translating…
- If you are agree & can & like to do some of them simply do it (or dedicate small amount of time to carry out, even small step) and be a shareholder
- If you think there is something more important to do (e.g. another software), simply propose it and hopefully community accept it and someone will help you to accomplish the job.

Once a task is done, contributor(s) became a shareholder and regularly get paid for next 7 years. 
**Powerful things happen when like-minded people connect**, so at the end of the day we will integrate all goodness of open-software, privacy tools, social networks,... together in a single open product. Accessible for everyone, in an easy-find manner. 
No single small project will be abandoned any more. All will be forever on blockchain and time by time, the brand-new contributors pushing it forward continuously. 
It is all about sharing good faith to make the world a better place, as it deserves. It is deployed for the common good, as open source philosophy is.
This project expands Open source’s frontiers to Open hardware, Open science and Open technology? Where every single paper of researches is accessible for everyone, and most important technologies can be used openly by everyone and every where. And eventually Open communities.


P.S. 
I. For the sake of brevity I wrote the rough idea. In next posts I will send different aspects of project.
II. It is not a fanciful proposal, But a pragmatic one which is absolutely feasible. 
III. This idea is not a complete & closed recipe. It is a pure booting infrastructure, to build a huge freedom building on top of it.

