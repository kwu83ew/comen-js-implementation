# An alternative monetary and wealth distribution model


An alternative monetary and power/wealth distribution model

Why we need to change monetary system? 
The answer is reining in the power of the banking system. 
The most corruption in today's life is because failed banking system -either governmental or commercial -. In next lines I try to explain an alternate economic model in order to start modifying the things that are not working properly. gradually and progressively. 
At the end of day we will have a new power and wealth distribution model, which will be more just than what exist now. 

Notes:
I. For the sake of brevity I wrote the rough idea. You can find more details and answer of all questions in FAQ.
II. It is not a fanciful proposal, But a pragmatic one which is absolutely feasible. 
III. This idea is not a complete & closed recipe. It is a pure booting infrastructure, to build a huge freedom building on top of it.



In next few lines I try to explain an alternate economic model. 
Most economists say, the main point of failure in current financial system are 2 facts.
    • Failed monetary system (governmental banking):
      - Is not auditable nor accountable
      - Is not democratic membership nor democratic  decision making
      
    • Failed fractional reserve (commercial banking)
      - Dept-based economy growth, which is not based on real productivity growth

These critics are obvious for almost all of us which had seen what happened in 2008 and what is going on nowadays all over the world. so instead of argue about that facts, for sake of brevity lets go to proposed solution. It could be a good solution as well as worst one. By the way we already know the old solutions, and how they will finish, don’t we?

“We cannot solve our problems with the same thinking we used when we created them.”
Albert Einstein

First of all we have to think globally, although we wrongly used to think nationwide or even worst local-focused, now we have to think globally, simply because something is local and something is global and economy is one of extremely global issues. If we create inflation and export it, definitely it comes back to us. It is how economy works.

lets start with creating a new money. We assume an imaginary monetary system which day by day mints x amount of coins. Because we need these coins in our wallets to spend it to buy goods and services. You may ask how these coins will be distributed?  The answer is straightforward. The coins will be distributed between people who worked for the network or did something good for the world. It is too wide and vague! For more details please read the FAQ: “Who can own stocks?” and “How are new coins distributed?”
Till now, we have a monetary system with a fix amount of daily minted coins. The coins are dividing between system shareholders, based on their shares amount. The shares are created by proposals of new members. They made a proposal, stating how many hours worked for “common good” and “world benefit”, and the current shareholders accept or negate the proposal. If the current shareholders accept proposal, the proposer will be a new shareholder. 
Being shareholder means you will be get paid from treasury and you can vote for future proposals (including your future proposals). Your vote power or what you get as a shareholder, depends on your shares amount. You dedicate more hours to system and being rewarded by more shares. 
Your shares lasts for 7 years. That is you do something results you will get paid 7 years instead of one time. It is a huge improvement in all aspect of contractual works. You know, this 7 year income works better than most expensive insurance you can buy! You will get paid unconditionally for next 7 years. It works perfectly if you lose your job or in case of passing away. You just need to save a copy of your private keys in a safe for the heirs. 
Now we have a self-modifier game! Old shareholders do not want to add new members. Instead they prefer to increase their shares. But they have to embrace new shareholders. Otherwise how the money popularity, applicability and usability will be expanded? What value has the money if no one use it? For more details look at FAQ: “There is a perverse incentive for shareholders to undervalue the work by other people (up to zero).”

Due the fact that shareholders have to accept new shareholders, it will be better to accept who really did something good for this  money ecosystem. Since ecosystem is global, therefore community accept proposal of people who did something good , really good for world. 

These coins in early days worth nothing. They can be used for paying network costs (e.g. record data on blockchain, send encrypted messages, running smart contracts etc). Over time, need for using network services drives demand for network coins, and it cause to raise up the value of network’s money.

Lets recap. 
- x coins daily minted.
- coins divided between shareholders.
- shareholders have to wisely accept new shareholders.
- no one forced to use this coin. There is no military or legal enforcement.
- as shareholders act well the coin value increases, and as corrupted community is coin value decline.
- new shareholders means less dividends and less coin for each shareholder, demand and supply rule => increasing coin value.
- demand for using  network services, will increase the the demand for network coins, means raise up the coin value.
- this money has intrinsic value too, because are payed in return of real work, which cheap fiat central bank printed monies haven’t.
- there is also halving (the process in which every 20 years, daily minted coins will be half), means more scarcity

as a result, the money is  deflationary. Means it’s values appreciated by time. can this money be subject of hyper-deflation? Probably yes, so we need to regulate it, in order to avoid hyper-deflation , market fluctuate and Pump/Dump conspiracy. lets imagine we have reserved coins as well. That is, every day our monetary system mints x coins, and 4x reserved coins. The reserved coins as it names defined are reserved and can not be spend easily! The reserved coins can be released if a certain time elapsed and the share holders are agree to release it. Again remember “as shareholders act well the coin value increased”. This self-modifier mechanism works pretty well. A cyclic incentive process.


look for graph "recursive-value-flow"


Right now we eliminated the government banking problem. No money out of thin air. No easy money based nothing. No unpredictable money supply. 
We also distributed the money in a fair mechanism. You may ask “what about who can not participate in project”? The short answer is “There is enough space for ALL to get paid from treasury”. For more details please look as these in FAQ: “Who can own stocks?”

Lets go to commercial banking problem which is fractional reserve banking system. Once upon a time the fractional reserve banking was a form of embezzlement or financial fraud, but now it is legalized! The real economy growth comes from real productivity growth and not using the others money to lend and earn interest. This fake growth is like going up on the ladder more rungs to be fallen more harder! 
How we can stop bankers from fractional reserve? We can not! We have to have our money, separated from banks, and stand alone. It was the Bitcoin promises. “financial sovereignty”. If you have your private keys, you control your money, if you don’t have, the bank/exchange/institute… controls your money. That is easy simple. You do not need Banks, so control your money. 
You may ask about “usability” and “applicability” of new money and being in doubt why someone has to accept this money? Please read FAQ: “Scarcity doesn't give value to the coin.”, “How the system creates value (in economic term)?”, “Is its money a kind of utility token?”, “Will these currencies will be vastly used by mass and they will have real usability and applicability?” and “will get around the power and centrality of the largest economic units - the financial and industrial capitals?”

You may want your money works for you (e.g. putting your money in your bank account and get interest). Or you may want to get loan from bank. Or you may want to put your home as collateral and get a loan. 
The good news is you can do almost all of your financial needs by new money as well, and even more efficient and zero fraudulent. 
You lend real money and borrower gets real money, not “fake credit/dept”. In such system we will not face the credit/dept cycles and the economy growth gradually. We have an strong DeFi in our system that has absolutely no fraudulent risk. In addition there is a reputation sub-system by which the parties can leverage the trades with less risk.. the system in whole freed individuals from fiduciary media’s dominance on financial market, specially cases where clients need a kind of credit or collateral for lending or borrowing. For more details look at the project wiki on DeFi. (the project wiki is a fully decentralized wiki, that you can access to it by install the software on your laptop)

hopefully this new monetary system will help us to reach human prosperity.





