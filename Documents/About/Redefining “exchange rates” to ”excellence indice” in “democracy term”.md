# Redefining “exchange rates” to ”excellence indice” in “democracy term”


Let me draw a future in which you vote hundred times in a single day, and your vote impacts immediately. You are not limited to vote only for a person or for a vague plan of a party, instead you can vote for every single changes and upvote it or defiance it immediately. And most importantly this voting doesn’t take your time or energy.

Hello dear reader,
It is Hu, a freedom enthusiast software developer, who believes in Privacy, digital rights and human equality as well. I implemented an innovative mixed technology to cover some drawbacks in internet privacy and center-oriented services as well as wipe out some flaws in economy, hoping more people feel happiness. I do not want to fit project in a particular “ism“ or a certain ideology, doctrine, regime or school of thought, simply because the project is a comprehensive infrastructure of human knowledge and experiences. It belongs to whole humankind, since my intention of that strive is human prosperity and I hope your intention is human prosperity as well. 

The project synthesized different concepts such as blockchain, free(libre) software movement, and some economic doctrine, so formed a software in a genuine ecosystem.  
**In simple words**; I created a software that lets every group of people establish an online community. It is “Community Maker Engine“ software. The community is practically decentralized and censorship resilience. So no one can stop the community, or cut its voice. These  features are core values of the software. Furthermore the software helps communities to run their customized rules and having their money and monetary system. Software also supports uncensorable decentralized domain name, messenger, forums, video channels and wikis. The software called “Comen”.

**You may ask “What is the use of all these?”.**  
Let me draw a future, in which you vote hundred times in a single day, and your vote impacts immediately. You are not limited to vote only for a person or for a vague plan of a party, instead you can vote for every single decision and upvote it or defiance it immediately. And most importantly this voting doesn’t take your time or energy.
Nowadays we vote for some political party or economic change promises every 4 or 5 years -if we believed in election yet-. After election, generally we can not do much in order to control what are doing the congress men or the president or the parliament -despite the fact that it is not an affair for mass, and most people simply do not interested in-. 
Even if -for any reason- an individual (or a group of people) monitors politicians or government decisions and find something wrong, practically she/he/they can not do something effective, unless waiting for next election, to not vote for that party or person and select another one. Something like selecting one kilo potato from one kilo potato! How can people express, “we prefer apples to potatoes”? 
Lets go back to our software.
Assume different group of people, have installed the “Comen software” and established their online communities. Each community will governed by different set of rules, some will be pure democratic and some other communities may have totalitarian rules. Each community has its customized economic systems for monetary, interest rate, inflation rate, wealth distribution and taxation. Eventually we will have different communities. Some communities follow **market economy**, whereas other communities are **pure socialist** and have some kind of coupons for managing “necessity of life”, and some another communities seeking **gift-economy** model and they do not have any form of money at all. By our software all of these different types of communities can be created and established in couple of hours. Even by a newbie software developer.
Inevitably, communities need to trade with each other, since It is almost impossible for one society not to need the goods or services of another. This need for trade between communities (More precisely, the trade between a person who has the money of community 1 and a person who has the money of community 2) creates the concept of **exchange rate between currencies of communities**. Two individual can directly change their different coins with agreement on a change rate between themselves. 
Take a look at schema "Inter-community-exchange-rates".



“For any reason” there will be communities that their money is more valuable than other communities. Later we will go into details “Why the money of a community is valuable than other ones”.
Now suppose the exchange-rates is fairly adjusted and not manipulated by speculators or conspiracy activities. We will discuss this in detail later.

Recap:
- They are coexisting different communities with different currencies, different governing rules and economic system, different order class and different structures.
- There are exchange rates between currencies  of communities.

The exchange rates between different communities generally are flat, and are determined by supply and demand on the open market between communities. There are several technical and fundamental factors that determine what people perceive is a fair exchange rate and alter their supply and demand accordingly. Despite the classical factors such as interest rate, inflation rate, commodities, etc… the value of a **community-based currency** comes form  “the community’s tendency of supporting community’s money”.  The community regularly attempts to keep its currency price favorable for inter-community trades. We should accept this assumption, Otherwise community would never have been formed. These mechanisms together provide a concept of **“exchange rate as  excellence measurement”**.
We can see this measurement -in a very loosely form- already exist in our real world exchange rates. The big difference between currencies of communities and fiat currencies laid in **how the currencies are managed**. In one side we have currencies, managed by governments and most often in non democratic ways, and the other side the currencies that are powered by people. 
The way that current exchange rates work, “exchange rate does not reflect fundamental economic conditions” or at least governments try to uncouple those (through FX intervention), but in our new “community-based arrangement” of world population, the exchange rate reflects both the economic conditions and the political governance of each community. 
Reasonably people will demand currency of some communities that they believe in the community’s principals, and refuse or devaluate the currency of other communities. People can be somewhat socialist and a bit capitalist, as well as naturalist and so on. It is our human nature and nothing is one hundred percent true. So we should pick the best part of each. People are free to create a portfolio of different currencies and also be a member of some different communities as well. Whenever they feel that a community is no longer following their wishes, they simply **"leave the community at no cost"**, unlike the current communities, ideologies, political regimes or countries and territories. They get rid of the community’s money and that’s all. No need to argue, quarrel or fight. **No need to war.**  
We buy and sell million times per day. By each transaction we make an impact on the exchange rate (supply and demand rule). These are our votes. We can vote every day million times for what we stand for, whether it’s a new economic policy or a welfare program in a community or a cultural movement in another community. This is a kind of instant democracy. We express our opinion and impact on exchange rates immediately. In such a way the corrupted community will lose value of its money and its population very quickly -let alone the fact that this kind of corrupted community can not be formed in first place-.
Arbitrage and all kinds of other schemes of speculation and manipulation are not welcomed, but the communities have to deal with. There will probably be chaos in the beginning, but a stabilization curve will begin soon, and at the end there will be some stable currencies, that are supported by matured community members. The remaining currencies fluctuate very little (unlike Bitcoin and all other altcoins) and will be used for some real trading and not only speculation. Remember that we are talking about a community that governing their own money and have proper rules in order to **expand community members**, **rise up community reputation** and **strengthen their money**. In this new paradigm not only “competition in monies is good, and there is no problem with private money” but also it is necessary that different group of people – despite the geolocations – can issue their money.


So far, I've roughly explained the mission of the software and how it works. You may encounter some of these questions.

- Why would people want to join to a community and why would they want money of community in the first place? 
Individuals would like to join community for various reasons, such as supporting a mindset or some principals, participating in an “open source accelerator project” or just economic incentives. As mentioned earlier, like-minded people may establish an online community, and set some rules and economic system that they believe in. Obviously they will try to expand community and invite more people to it. Depending on how well the community works -respecting the principals and values of the community-, individuals may join or leave it. The people are the best judges. They can interact with community at different levels. They can use the money of a community as a "medium of exchange" or as a "store of value" and no more. Or they can involve in community activities and decision making and so on. 
There will be people who benefit from exchange rate fluctuations. They may be persuaded to buy that money as well.
In addition, these coins also can be used to pay for network services. That is, community can set some price for services such as domain name registering, weblog/videolog cost, secure messaging, and so on. 


- There are some online communities and some people joined to this or that community. And they have some kind of tokens (you call it money), and they exchange these tokens for each other. Isn't it like online forums, that already exist hundreds of them? What is special in this idea?
The more important feature of this software is **decentralization**, which means the community (any group of people) can be established and continue to live, while no authority, no central-service or no third party can stop them or censor their voice and contents. The community governs its territory and is standalone. 
About the money of community (token), this is a **community-based monev** that gains its value from community, and not from authorities.


- I am still unconvinced! Where does the value of these currencies come from?
Lets ask “where does the value of fiat currencies come from?”. The value of “fiat currencies” is grounded in government taxes. The state creates money to pay people to do stuff it wants done, and everyone else must trade to acquire that money to pay taxes or the state will use violence, whereas the value of a community’s money is created by the free choice of community members whom – by the regard of their own interest – judge whether the services community’s money provide is useful to them or not. If  I believe in my community’s money I expect to buy some good or services from my community’s member. Say ten million coin for one pizza (as it happened before). It is the zero day for set value of the money of the community. The money’s value come from the exception of future purchasing power. If community acts according to its principles, we expect that the purchasing power of money of community will remain same or even increase day by day. Because the community proved its consistency for some principles. This consistency drives more demand for that money from who believes in those principles. 

- The community members -by the any reason- believed in money of their community, why should the outsiders believe that money worth to accept?
It depends on how the community has introduced itself to the public. The functionality of that particular money is also important. I will describe what I mean by “money functionality” in next few lines when I explain the features of money of my favorite community (named “imagine”).

- How these online communities will affect on **real world**? How they can embody their community?
That is really up to community members. Some communities may prefer to continue in cyberspace. Some other may use this system to handle a “collective or cooperative” activity*, or some will use it as a potential infrastructure for a social movement. There are many use cases for this system in real world. 
I personally use this software to accelerate open source movement and also implement a kind of “good money” and put it in practice as well. I am a free(libre) software proponent (someone know it as FOSS or FLOSS). Therefore I established a community to accelerate free software movement and develop more **privacy tools for mass** as a serious issue in internet age. I set some rules and monetary system aiming to create some incentives to involve more and more people -despite the fact that most of free(libre) developers doing that voluntarily-. The approach is invite as possible as people -and not only software developers- to community. The community called “imagine”, inspired by the **Beatles** famous song you may know. 
By the way, the imagine members are get paid from imagine’s treasury, because of accomplish the tasks that community considered as a **common good for the world**, or as a **good activity for community**. The imagine community runs polling for every single decision, and all community members can vote in proportion to their shares of community. Eventually the imagine’s community will develop/improve many useful software for mass (including “Comen” software itself). The imagine’s community decide “**what to produce, how to produce, for whom to produce, and why**”. They do **real work** to produce **real product** to solve **real world** problem.  
The Comen software by itself is “**the product**” and “**means of production**	” simultaneously. 
So for now the imagine community established one of its blockchain real-world practices. There are many more different types of activities that imagine members can do in order to solve real world problems. The jobs are not limited to cyberspace or digital activities. In imagine we have a long term plan to accomplish physical activities by virtual money. Later I will send an independent post only for covering this chapter.


- How do these online communities affect their members in the real world economically?
It depends on community’s policy. The Comen software has embedded DeFi features (e.g. send & receive money, loan & lending, interest rate setting, and smart contracts to manage all these stuff in a secure way, with almost zero cost). If community members decide to trade inside community (even an small portion of daily turnovers), community will take benefit of these trades aiming to improve community’s members welfare. The community can also run a kind of “mutual banking system”, in order to provide funds for community members. All of these improvements depends on community’s willingness to do so. The software and its infrastructure are designed in a highly flexible mode and supports all these optional features. More members convinced to use DeFi, more “financial sovereignty”, more wealthy community. 
As an example:
The “imagine” community has customized monetary and wealth distribution mechanism, in which it makes a “good money” for its needs. The monetary works like that:
- The community’s money called PAI.
- Every 12 hours 45 million PAI are minting. (numbers are not random numbers)
- The new PAIs right after creating are divided between community members in proportion to their shares.
- The shares of each member are calculated based on how many hours that member dedicated to accomplish communities task. In early days tasks are all activities about software development. Such as coding, testing, documenting, presentations, design, educational stuff, transcript and translation, reviewing, etc… later, community accepts more types of not-online activities, such as common good activities of all kind of NGOs or non profit organizations, etc…
- Once a member accomplish a task, she/he/organization will be paid for 7 years for that single contribute. Two times per day, directly to contributor’s wallet.
- Because of that contribution, the member has voting rights as well, for next 7 years.
- Every 20 years the amount of minting coins will be half (halving), so eventually the total amount of money supply will be a FIX number. This is the one of the features of a “good money”. “imagine” doesn’t allowed to print money out of thin air.
Because of these mechanisms the imagine’s community creates a “good money”. One key feature of a “good money” is “appreciating purchasing power over time” and imagine PAI has this feature. So people willingly accept this money in return of their goods or services, while people can earn this money by participating in community activities as well. 
Having fix income for next 7 years, adds up another useful feature to imagine PAI, which is “credit”. This credit is real definition of credit and it is not like the fractional-reserve definition of credit. The credit in imagine means, you have steady, unconditional income for next 7 years, despite the all changes in world (stagnation, crisis, pandemic,...). It works even better than all insurance you can buy. You do not have to cut a part of your income in advanced, for an uncertain future. We just changing our point of view. People get paid regularly. There is no condition. It works perfectly in case of losing job or even passing away. It just needed to save a copy of private keys in a safe for the heirs. 
You can benefit this steady income for installment purchase as well. There are more benefits for this steady income but for the sake of brevity I stop here.
In these lines I explained the economic effect of one particular community. Different communities are likely to emerge that will have different economic impacts on their members.


- What are the advantages of these new currencies over government fiat currencies?
Not all new currencies will necessarily have more advantage than Fiat currencies, but certainly low-quality currencies will not survive, and "good money" will continue to grow over time. Here is listed what is “good money”.
Generally, the good money must have **intrinsic value**, Something like "labor theory of value" but do not stick that too much. The good money must **appreciates its value over time**. That is, doesn't lose its purchasing power even after decades. It must be **regulated** and do **not fluctuating** too much. It must be **scarce** and has **utility value**, meanwhile it must has optimal **granularity**. The good money must support “**financial sovereignty**” and must **not be ceaseable**, also must be **cheaply transferable**. The good money definitely must have 3 classical factor of money. **Unit of account, Medium of exchange, Store of value**.
imagine PAI is designed to cover all of these features. It has mechanisms and procedures to make it self-regulated, anti-fluctuation and not-inflationary. These features will be explained in an independent post.


- Even if it's the best money, its market is very thin, so there is no utility or applicability in these new currencies.
True, the primary market is thin, but that doesn't mean the market won’t grow forever! The new currencies, as a good money, will gradually prove themselves against the cheap fiat paper currencies, specially when they are accessible for mass with low cost or zero cost and in return of some useful tasks. The imagine PAI is designed to be easily acquirable to people who want to help others. In long run this money will be distributed fairly and because of real helping the world, which provides huge liquidity as well.





- what is the imagine’s perspective?
These are objectives of imagine’s community. 
- Write and release a software as a “community maker engine“ under free(libre), open source software license.
- The endless development of the software, in order to reform the standard internet based on current classic internet. Particularly removing centralism flaws and improving privacy issues.
- Introducing a kind of “good money” and put it in practice meanwhile plan to distribute it fairly.
- Accelerating “open movement”. That includes open software, open hardware, open data, open science, open technology, open money and open communities.
- Interacting philanthropists in order to amplifying and resonating their efforts.

Each of the above goals is a significant change towards improving human society, and I hope we achieve all of these for all people of the world. 


Recap:
- They are different communities with different currencies and different governing rules and economic system.
- There are exchange rates between currencies of communities.
- The greater the excellence of community, the greater the demand for that its money, and this in turn increases the value of that money.
- People simply exchange the different community’s currencies to appreciate their portfolio's value, meanwhile they express their opinions as well.
- These exchange rates express the people feelings about the communities and impact on communities decisions as well.
- The communities correlate  to real day to day life of their members economically. Indeed the community and real life impact on each other.
- People vote for or against every single changes, frequently, by minimum cost, and effect immediately.


###### * since you do not need classic internet domain registration and host renting and all costs of create a website and the maintenance, and the users have “data sovereignty” plus “financial sovereignty”, the software is a great alternative for current internet websites. You will not lose domain name and your data of your website will be always accessible for you and your network, without paying one penny.


Tags: Comen, imagine, cryptocurrency, blockchain, cryptovalue, blockgraph, cryptography, free (libre) software, FOSS, FLOSS, open source, privacy and privacy tools, decentralization, liquid democracy, prosperity

