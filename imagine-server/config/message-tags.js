module.exports = {

    senderStartTag: '---START_SENDER_TAG---',
    senderEndTag: '---END_SENDER_TAG---',
    receiverStartTag: '---START_RECEIVER_TAG---',
    receiverEndTag: '---END_RECEIVER_TAG---',
    hashStartTag: '---START_HASH_TAG---',
    hashEndTag: '---END_HASH_TAG---',

    customEnvelopeTag: 'CUSTOM ENVELOPE',
    customStartEnvelope: '-----START_CUSTOM_ENVELOPE-----',
    customEndEnvelope: '-----END_CUSTOM_ENVELOPE-----',
    NO_ENCRYPTION: 'NO-ENCRYPTION',

    iPGPStartEnvelope: '-----START_iPGP_ENVELOPE-----',
    iPGPEndEnvelope: '-----END_iPGP_ENVELOPE-----',
    
    iPGPStartLineBreak: '(',
    iPGPEndLineBreak: ')\n\r<br>'


}
