const iConsts = require("./constants");

function dspConstants() {
    let out = '\n\n\t------------ CONSTANTS --------------';
    out += `\n\t  Time Gain: ${iConsts.TIME_GAIN}`;
    out += `\n\t  Launch date: ${iConsts.getLaunchDate()}`;
    out += `\n\t  Cycle by minute: ${iConsts.getCycleByMinutes()}`;
    out += `\n\t  Cycle by hour: ${iConsts.CYCLE_BY_HOURS}`;

    out += '\n\n\t---------------------------------------';

    return out;
}

module.exports.dspConstants = dspConstants;