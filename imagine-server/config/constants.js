const getMoment = require('../startup/singleton').instance.getMoment

let machine = null;

let IMG_CONSTS = {

    FORCED_LAUNCH_DATE: "2020-02-02 00:00:00",

    /**
     * so every growth of shares(growth of contribute efforts) and pasing share ceil, causes to growth releaseable coins (by Fibonacci sequence).  
     * until reaching entire world population having shares in imagine's network
     * it IS/MUST absolutely simple & clear rule, understandable, applyable & auditable for ALL.
     * implementation details in "calculateReleasableCoinsBasedOnContributesVolume"
     * */
    MAP_CONTRIBUTE_AMOUNT_TO_MINTING_PERCENTAGE: {
        1: 1,
        2: 1,
        3: 1,
        5: 2,
        8: 3,
        13: 5,
        21: 8,
        34: 13,
        55: 21,
        89: 34,
        144: 55,
        233: 89,
        377: 100
    },
    WORLD_POPULATION: 8000000000,   //8,000,000,000


    CLIENT_VERSION: '0.0.14',
    DEFAULT_CONTRIBUTE_LEVEL: 6,


    //1: a cycle is 720 minutes
    //2: a cycle is 2 minutes
    //5: a cycle is 5 minutes
    //60: a cycle is 60 minutes
    STANDARD_CYCLE_BY_MINUTES: 720,
    TIME_GAIN: 1, // in live environment time gain must be 1, 
    EMAIL_IS_ACTIVE: true, // enables send & receive email 
    BROADCAST_TO_NEIGHBOR_OF_NEIGHBOR: true,    // if machine allowed to publish email of neightbors of neighbors to hei neighbors?
    SUPPORTS_CLONED_TRANSACTION: false,    // after writing a tons of unittests can activate this feature
    SUPPORTS_P4P_TRANSACTION: false,    // after writing a tons of unittests can activate this feature

    // message parsing settings
    MAX_PARSE_ATTEMPS_COUNT: 5, // parse message tentative

    DO_HARDCOPY_DAG_BACKUP: true,   // it creates a DAG backup in sense of blocks
    DO_HARDCOPY_OUTPUT_EMAILS: true, // it creates a local copy of what your machine will send via email
    // ACTIVE_FAKE_NETWORK: true,

    MICRO_PAI: 1,
    MILI_PAI: 1000,
    PAI: 1000000,
    ONE_PAI: 1000000,
    HUNDRED_PAI: 100000000,
    THOUSAND_PAI: 1000000000,
    MILLION_PAI: 1000000000000,
    BILLION_PAI: 1000000000000000,

    // email pop settings
    EMAIL_POP_GAP_BY_MINUTE: 5, // every 5 minutes check inbox

    CONSTS: require('./consts'),


    // machine status
    // NODE_IS_BOOTING: "booting",
    // NODE_IS_READY_TO_GENERATE_NEW_BLOCK: "node_is_ready_to_generate_new_block",

    //bech32 part
    BECH32_ADDRESS_VER: '0', // version must be one char
    TRUNCATE_FOR_BECH32_ADDRESS: 33,


    LAUNCH_YEAR: 2019, // every twelve years the coinbse divided to 2
    HALVING_PERIOD: 20, // every twelve years the coinbse divided to 2


    COIN_ISSUING_INIT_EXPONENT: 52, // the power of 2 for minting new coins
    COINBASE_MATURATION_CYCLES: 2,

    MAX_INT_IN_JS: Number.MAX_SAFE_INTEGER, //between -(2^53 - 1) and (2^53 - 1). 9007199254740991,
    MAP_RESERVE_NUMBER_TO_TIME_LOCK: {
        1: 1,               // the first reserved block, 1 year after minting block, will be releasable by agreement of 80 % of shareholders of that particular block
        2: 1 + 2,           // the second reserved block, 3 years after minting block, will be releasable by agreement of 60 % of shareholders of that particular block
        3: 1 + 2 + 3,       // the third reserved block, 6 years after minting block, will be releasable by agreement of 40 % of shareholders of that particular block
        4: 1 + 2 + 3 + 5    // the forth reserved block, 11 years after minting block, will be releasable by agreement of 20 % of shareholders of that particular block
    },

    DEFAULT_REPAYMENT_SCHEDULE: 2 * 365, // 2 cycle per day * 365 days 
    DEFAULT_MAX_REPAYMENTS: 2 * 365 * 2, // 2 cycle per day * 365 days * 2 years

    // (FLENS) for every 180 shares the share holder has permitted to register one iName.
    // iNames are kind of username/nickname/domain-name/aplication-account/email-address/website-address/social-network account
    // even bank account ALL-IN-ONE handler
    SHARES_PER_INAME: 5 * 6 * 6,   // (5 days * 6 hours which will be normal working hours for human) * average level 
    INAME_UNIT_PRICE_NO_EMAIL: 1000000 * 710, // initialy it is intentionaly super hi value to avoid spammingor greedy name registering(Domain name speculation), TODO: implement voting for INAME_UNIT_PRICE to reduce it time by time
    INAME_UNIT_PRICE_EMAIL: 1000000 * 71, // initialy it is intentionaly super hi value to avoid spammingor greedy name registering(Domain name speculation), TODO: implement voting for INAME_UNIT_PRICE to reduce it time by time
    INAME_OFFSET: 3,
    INAME_THRESHOLD_LENGTH: 15, // iNames longer than 15 characters cost almost nothing

    DECODE_ALL_FREE_POST_FILES: true, // TODO: improve it to support different file types & security levels

    DEFAULT_PORT_NUMBER: 1717,

    getHardDiskReadingGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        if (machine.isInSyncProcess()) {
            if (this.TIME_GAIN == 1)
                return .1; // every .05 minutes check read a file from inbox folder (if exists)
            return this.TIME_GAIN / 155; // it is testing ambianet value

        } else {
            if (this.TIME_GAIN == 1)
                return 2; // every 2 minutes check read a file from inbox folder (if exists)
            return this.TIME_GAIN / 55; // it is testing ambianet value

        }
    },

    // it means maximum how long we suppose some nodes creae a new block(except coinbase block)
    getAcceptableBlocksGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        let gapByMinutes;
        if (this.TIME_GAIN == 1) {
            // live
            gapByMinutes = machine.isInSyncProcess() ? 11 : 71;
        } else {
            // devel
            gapByMinutes = machine.isInSyncProcess() ? (this.TIME_GAIN / 0.15) : (this.TIME_GAIN / 0.5);
        }

        console.log(`acceptable block gap By Minutes: ${gapByMinutes}`);
        return gapByMinutes;
        // if (machine.isInSyncProcess()) {
        //     if (this.TIME_GAIN == 1)
        //         return 11; // in live system we suppose every 71 minutes atleast one node creates a new block(althoug in first days it could be 720 minutes :)
        //     return this.TIME_GAIN / 0.15; // it is testing ambient value
        // } else {
        //     if (this.TIME_GAIN == 1)
        //         return 71; // in live system we suppose every 71 minutes atleast one node creates a new block(althoug in first days it could be 720 minutes :)
        //     return this.TIME_GAIN / 0.5; // it is testing ambient value
        // }
    },

    getDescendentsInvokeGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        if (machine.isInSyncProcess()) {
            if (this.TIME_GAIN == 1)
                return 4; // in live system every 4 minutes the machin should check if she has updated DAG?
            return this.TIME_GAIN / 14; // it is testing ambianet value

        } else {
            if (this.TIME_GAIN == 1)
                return 51; // in live system every 51 minutes the machin should check if she has updated DAG?
            return this.TIME_GAIN / 4; // it is testing ambianet value

        }
    },

    // TODO: make it dynamic/intelligance to auto-adjust based on server load and received blocks(e.g. in sync phase needs less gap between fetches)
    getParsingQGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        let gapByMinute;
        if (this.TIME_GAIN == 1) {
            // live
            if (machine.isInSyncProcess()) {
                gapByMinute = .02; // every 1 minutes check parsing q
            } else {
                gapByMinute = 1; // every 1 minutes check parsing q
            }
        } else {
            //develop
            if (machine.isInSyncProcess()) {
                gapByMinute = this.TIME_GAIN / 270;
            } else {
                gapByMinute = this.TIME_GAIN / 70;
            }
        }
        console.log(`parsing Q Gap = ${gapByMinute}`);
        return gapByMinute;
    },

    getSendingQGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        let gapByMinute;
        if (this.TIME_GAIN == 1) {
            // live
            if (machine.isInSyncProcess()) {
                gapByMinute = .5; // every 1 minutes send to sending q
            } else {
                gapByMinute = 5; // every 5 minutes send to sending q
            }
        } else {
            //develop
            if (machine.isInSyncProcess()) {
                gapByMinute = this.TIME_GAIN / 100;
            } else {
                gapByMinute = this.TIME_GAIN / 70;
            }
        }
        console.log(`sending Q Gap = ${gapByMinute}`);
        return gapByMinute;
    },

    // at least 4 cycle must be a gap between pay to treasury and dividing to shareholders
    TREASURY_MATURATION_CYCLES: 4,
    POW_MAX_DOC_PER_BLOCK: 1,

    getCycleBySeconds: function () {
        if (this.CBM_SETTED === undefined) {
            let cycleByMinutes = (this.TIME_GAIN == 1) ? this.STANDARD_CYCLE_BY_MINUTES : this.TIME_GAIN
            this.setCycleByMinutes(cycleByMinutes) // 12 h  * 60 minute    it used for coinbase cycle,double-spending prevention...
        }
        return this.CYCLE_BY_SECONDS;
    },

    getCycleByMinutes: function () {
        if (this.CBM_SETTED === undefined) {
            let cycleByMinutes = (this.TIME_GAIN == 1) ? this.STANDARD_CYCLE_BY_MINUTES : this.TIME_GAIN
            this.setCycleByMinutes(cycleByMinutes) // 12 h  * 60 minute    it used for coinbase cycle,double-spending prevention...
        }
        return this.CYCLE_BY_MINUTES;
    },

    setCycleByMinutes: function (value) {
        if (this.CBM_SETTED === undefined) {
            this.CBM_SETTED = true;
            this.CYCLE_BY_MINUTES = value;
            // this.COINBASE_CHECK_BY_MINUTE = (this.getCycleByMinutes() / 7);
            this.CYCLE_BY_SECONDS = this.getCycleByMinutes() * 60;
            this.CYCLE_BY_HOURS = this.getCycleByMinutes() / 60; // 1h, 2h, 3h or 12 h     it used for coinbase cycle,double-spending prevention...
        }
    },

    getLaunchDate: function () {
        if (this.LD_SETTED === undefined) {
            let launchDate;
            if ((this.TIME_GAIN == 1) || (this.FORCED_LAUNCH_DATE != null)) {
                launchDate = this.FORCED_LAUNCH_DATE
            } else {
                launchDate = getMoment().subtract({ 'minutes': IMG_CONSTS.getCycleByMinutes() - 1 }).format('YYYY-MM-DD HH:mm:ss')
            }
            this.setLaunchDate(launchDate)
        }
        return this.LAUNCH_DATE;
    },

    setLaunchDate: function (value) {
        if (this.LD_SETTED === undefined) {
            this.LD_SETTED = true;
            this.LAUNCH_DATE = value;
        }
    },

    getInvokeLeavesGap: function () {
        return (this.TIME_GAIN == 1) ? this.STANDARD_CYCLE_BY_MINUTES / 24 : this.TIME_GAIN / 3;
    },

    getNBUTXOsImportGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');

        let gapByMinutes;
        if (this.TIME_GAIN == 1) {
            // live mode
            if (machine.isInSyncProcess()) {
                gapByMinutes = this.STANDARD_CYCLE_BY_MINUTES / 600
            } else {
                gapByMinutes = this.STANDARD_CYCLE_BY_MINUTES / 200
            }
        } else {
            // develope mode
            if (machine.isInSyncProcess()) {
                gapByMinutes = this.TIME_GAIN / 20
            } else {
                gapByMinutes = this.TIME_GAIN / 10
            }
        }

        return gapByMinutes;
    },

    isDevelopMode: function () {
        return false;    // BEFORRELEASE convert to false
    },

    getBlockInvokeGap: function () {
        if (machine == null)
            machine = require('../machine/machine-handler');
        if (machine.isInSyncProcess()) {
            return (this.TIME_GAIN == 1) ? this.STANDARD_CYCLE_BY_MINUTES / 500 : this.TIME_GAIN / 20;
        } else {
            return (this.TIME_GAIN == 1) ? this.STANDARD_CYCLE_BY_MINUTES / 120 : this.TIME_GAIN / 9;
        }
    },

    getMinVotingLongevity: function () {
        let votingLongevity = (IMG_CONSTS.getCycleByMinutes() * 2) / 60;   // at least 2 cycle for voting
        if (votingLongevity != parseInt(votingLongevity))
            votingLongevity = Math.trunc(votingLongevity * 100) / 100;
        return votingLongevity;
    },

    // the maximum time in which local machine wait to attain most possible confidence coinbase block
    // best value is 3/4 of one coinbase cycle
    COINBASE_FLOOR_TIME_TO_RECORD_IN_DAG: 3 / 5,
    MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER: 0.0000000001,
    SHARE_MATURITY_CYCLE: 2,    // 2 cycle after inserting a share in DB, coinbase will include the newly recorded share in it's dividend
    CONTRIBUTION_APPRECIATING_PERIOD: 7, // every little help is appreciated for 7 years
    // to avoid spam proposals, there is a cost equal to 30 days of potential income of the proposal
    // this cost goes to treasury and divide between shareholders

    // in order to put a proposal on voting process, requester has to pay one month income * 3 to treasury
    PROPOSAL_APPLY_COST_SCALE: 3,

    /**
     * hopefully after 10 cycle(5 days) all DAG branches are merged and 
     * ALL transactions from 5 days ago are visible by leaves,
     * so we do not need to mantain spend coins in DB
     */
    KEEP_SPENT_COINS_BY_CYCLE: 10,

    ONE_MINUTE_BY_MILISECOND: 60000,
    SIGHASH: {
        ALL: 'ALL',
        NONE: 'NONE',
        // these have conflict with BIP69, that's why we need custom SIGHASH
        // 'SINGLE': 'SINGLE',
        // 'ALL|ANYONECANPAY': 'ALL|ANYONECANPAY',
        // 'NONE|ANYONECANPAY': 'NONE|ANYONECANPAY',
        // 'SINGLE|ANYONECANPAY': 'SINGLE|ANYONECANPAY',  
        CUSTOM: 'CUSTOM', // TODO: implement it in order to have ability to sign some random inputs & outputs
    },

    MAX_TOLERATED_MISS_BLOCKS: 5,    // if machine messed more than this number, do not allowed to issue a co9inbase block

    SIGN_MSG_LENGTH: 32,
    SUPER_CONTROL_UTXO_DOUBLE_SPENDING: true,
    SUPER_CONTROL_UTXO_UNTIL_COINBASE_MINTING: true,

    TRANSACTION_PADDING_LENGTH: 100,
    TRANSACTION_MINIMUM_LENGTH: 375,     // smallest transaction has 375 charachter length

    /**
     * this "MAX_BLOCK_LENGTH_BY_CHAR" is a single max size of single message after adding headers and encrypting
     * it means real block size(or real GQL messegaes size) must be roughly around 80% - 90%
     */
    MAX_BLOCK_LENGTH_BY_CHAR: 10000000 * 10,
    MAX_DOC_LENGTH_BY_CHAR: 10 * 1024 * 1024 * 9,   // 10 Mega Byte is actually block size
    MAX_DPCostPay_DOC_SIZE: 600,
    MAX_FullDAGDownloadResponse_LENGTH_BY_CHAR: 1500000,

    STAGES: {
        Creating: 'Creating',
        Validating: 'Validating'
    },


    BACKER_PERCENT_OF_BLOCK_FEE: 71 / 100,

    TREASURY_PAYMENTS: [
        'TP_DP',            // Data & Process Costs
        'TP_DONATE_DOUBLE_SPEND',
        'TP_PROPOSAL',      // the costs of proposal register & voting process 
        'TP_PLEDGE',        // the pledging costs
        'TP_PLEDGE_CLOSE',  // closing pledge cost. it culd be different based of the concluder
        'TP_POLLING',       // any type of polling(except proposal polling in which the polling doc is created automatically in each node) the payment labled this tag
        'TP_BALLOT',        // the Ballot costs
        'TP_ADM_POLLING',     // Request for network administrative parameters polling
        // 'TP_REQRELRES',     // Request for Release Reserved coins
        'TP_INAME_REG',     // iName registerig costs
        'TP_INAME_BIND',     // binding to iName costs
        'TP_INAME_MSG',      // send message to iName costs
        'TP_FDOC',           // custom posts
        'TP_REP_FAILD',      // Reputation failed
    ],

    DOC_TYPES: require('./doc-types'),
    BLOCK_TYPES: require('./block-types'),
    CPOST_CLASSES: require('./custom-post-classes'),

    // floating blocks like floating signature, floating votes, collision logs...
    FLOAT_BLOCKS_CATEGORIES: {
        Trx: 'Trx', // collision on spending a refLoc as a fund of a transaction
        FleNS: 'FleNS', // if collision on registring an iName
    },

    SIGNATURE_TYPES: {
        Basic: 'Basic', // Basic signature
        Bitcoin: 'Bitcoin', // Bitcoin like sha256 hash address
        IOT: 'IOT', // simple light signature for Internet Of Things
        Strict: 'Strict', // Strict signature
    },

    TRX_CLASSES: {
        SimpleTx: 'SimpleTx',    // simple "m of n" trx
        P4P: 'P4P', // pay for payment
        Bitcoin: 'Bitcoin', // Bitcoinish transaction to supporting swap transactions
    },
    TRX_ATTRS: {
        TTT: 'TTT', // Transparent Traceable Transaction

    },

    PLEDGE_CLOSE_CLASESS: {
        General: 'General'    //General imagine DNA proposals
    },
    PROPOSAL_CLASESS: {
        General: 'General'    //General imagine DNA proposals
    },
    BALLOT_CLASSES: {
        Basic: 'Basic'    //Basic Ballot type
    },
    GENERAL_CLASESS: {
        Basic: 'Basic'    //Basic message
    },
    INAME_CLASESS: {
        NoDomain: 'NoDomain',       // NoDomains: initially imagine do not accept domain-like strings as an iName. 
        YesDomain: 'YesDomain'      // TODO: after implementing a method to autorizing real clessical domain names owner(e.g. google.com) will be activate YesDomain too. TODO: add supporting unicode domains too
    },



    PLDEGE_ACTIVATE_OR_DEACTIVATE_MATURATION_CYCLE_COUNT: 2,

    // there are 3 ways to unpledge an account
    // 1. ByPledgee: The pledgee redeems account (pledgee Closes Pledge). it is most common way supposed to close pledge and redeem account.
    //    this is most efficient way to avoid entire network engaging unnecessary calculation.
    // 2. ByArbiter: in redeem contract can be add a BECH32 address as a person(or a group of persons) as arbiter. the contract can be closed by arbiter signatures too.
    //    the arbitersinetercept in 2 cases. in case of denial of closing contract by pledgee or for the sake of useless process. 
    //    this method is also cost-less(in sence of network process load) and is a GOOD SAMPLE of trusting and not over-processing by entirenetwork
    //    in other word, by giving rights to someone to be arbiter, and givin small wage, and involving them in a contract, 
    //    the arbiters MUST run a full node (and maybe a full-stack-virtual-machine to evaluate a contract) and the rest of networks doing nothing about this particulare contract
    //    in this implementation even an infinit loop problem doesnot collapse entire network, and more over the contracts can be run as an OPTIONAL-PLUGIN on SOME machines
    //    and for the rest of network, what is important is the final signature of arbiters, adn they only validate signatures in order to confirm the PAIs transformation
    // 3. ByPledger: in case of pledgee refuse to redeem account, pledger requests network to validate redeem cluses by calculate repayments and... 
    //    and finally vote to redeem (or not redeem) the account. this proces has cost, and must be paid by pledger.
    //    althout this has cost for pledger, also has negative effect on pledgee reputation(if network redeem the account).
    // 4. ByNetwork, every time the coinbase dividend toke place, every machines calculate automatically. this method is most costly till implementing an efficient way
    // either pledgee or arbiter can close contract before head and remit repayments 
    PLEDGE_CONCLUDER_TYPES: {
        ByPledgee: 'ByPledgee',
        ByArbiter: 'ByArbiter',
        ByPledger: 'ByPledger',
        ByNetwork: 'ByNetwork'
    },
    PLEDGE_CLASSES: {
        // self authorized pledge
        // in this class the pledger signs a contract to cut regularely from mentioned address. (usually the shareholder address by which received incomes from share dvidend)
        // the diference is in this class all 3 parties(pledger, pledgee, arbiter) can close contract whenever they want.
        // this service is usefull for all type of subscription services e.g. monthly payments for T.V. streaming or even better DAILY payment for using services
        // by activating field "applyableAfter" the contract can be realy closed after certain minute of concluding it. 
        // e.g. customer must use atleast 3 month of service(in order to get discount)
        // TODO: implement it since it cost couple of hours to implementing
        PledgeSA: 'PledgeSA',

        // pledge class P is designated to pledge an account in order to getting loan to apply proposal for voting.
        // in return if proposal accepted by community part of incomes will repayed to loaner untile get paid all loan+interest
        // so, this class of pledge needs 2 signature in certain order, to be valid
        // 1. pledger, signs pledge request and repayment conditions
        // 2. pledgee, sign and accepts pledging and publish all proposal, pledge-contract and payment-transaction at once in one block
        // 3. arbiter, in case of existance, the arbiters sign pledge-contract and they publish ALL 3(proposal, pledge-contract and payment-transaction), 
        //    surely by arbiters signature(arbiterSignsPledge) the pledge hash will be changed. 
        PledgeP: 'PledgeP',

        // Zero-Knowledge Proof implementation of PledgeP TODO: implement it
        PledgePZKP: 'PledgePZKP',


        // gneric pledge in which pledger can pledge an address(either has sufficent income from DNA or not) and get a loan
        // and by time payback the loan to same account
        // of course there is no garanty to payback PAIs excep reputation of pledger or her bussiness account which is pledged
        // TODO: implement it ASAP
        PledgeG: 'PledgeG',


        // lending PAIs by pledging an account with sufficent income
        // TODO: implement it ASAP
        PledgeL: 'PledgeL',

    },

    POLLING_REF_TYPE: {
        Proposal: "Proposal",
        ReqForRelRes: "ReqForRelRes", // Request For release Reserved
        AdmPolling: "AdmPolling",
    },
    POLL_PERFORMANCE_TYPES: {
        Transparent: "Transparent", // Transparent   
        ZKP: "ZKP", // Zero-Knowladge Proof
    },
    VOTE_COUNTING_METHODS: {
        Plurality: "Plurality", // Plurality   
        PluralityLog: "PluralityLog", // Plurality logarythmic and (x )minutes for Y/N/A (x+1/2) Minutes for N/A
        TwoRRo: "TwoRRo", //"Two-Round Runoff" 
        InstantRo: "InstantRo",   //"Instant Runoff" 
        BordaCount: "BordaCount", //"Borda Count" 
    },

    ADMINISTRATIVE_POLLING_CLASSES: {
        Basic: 'Basic'
    },

    REQ_FOR_REL_RES_CLASSES: {
        Basic: 'Basic'
    },

    BINDING_TYPES: {
        IPGP: 'IPGP',
    },

    DEFAULT_LANG: 'eng',
    DEFAULT_VERSION: '0.0.0',

    MESSAGES_TYPES: {
        Plain: 'Plain',  // Plain text
        PledgeP: 'PledgeP',  // a signed pledge and loan request
        PublicKey: 'PublicKey',  // a public key to participate in a sginature
        Bech32: 'Bech32',  // a Bech32 to participate in a sginature. this feature needs to be implementing indent uSets before head, which is possible to impement in 30 hours
        SignedTrx: 'SignedTrx',  // a partialy(or even signed py all parties) signed transaction, which users send to eachother in order to be signe by all necessary parties, and finally send to network
    },

    MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK: 51,

    HD_PATHES: {
        temporary: "temp-mailbox",
        inbox: "temp-mailbox/inbox",
        outbox: "temp-mailbox/outbox",

        hd_backup_dag: 'backup-dag/dag',
        I_CACHE_FILES: 'i-cache-files/cache',

        logs_path: '../../imagine-logs'
    },

    msgTags: require('./message-tags'),

    // hu part 
    HU_DNA_SHARE_ADDRESS: "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl",
    HU_INAME_OWNER_ADDRESS: "im1xqerwvmpvvervv33x93kyvenvgckyd3jxgexvwrzx5cxydejvv6xx5twphg"
};


IMG_CONSTS.DSP_CONSTANTS = true;
IMG_CONSTS.EXT_INFO_SUPPORTED = [
    IMG_CONSTS.BLOCK_TYPES.Normal,
    IMG_CONSTS.BLOCK_TYPES.FSign,
    IMG_CONSTS.BLOCK_TYPES.FVote,
    IMG_CONSTS.BLOCK_TYPES.POW
];


IMG_CONSTS.POLLING_PROFILE_CLASSES = {
    Basic: {
        ppName: 'Basic',
        activated: IMG_CONSTS.CONSTS.YES,
        performType: IMG_CONSTS.POLL_PERFORMANCE_TYPES.Transparent,
        voteAmendmentAllowed: IMG_CONSTS.CONSTS.NO,
        votesCountingMethod: IMG_CONSTS.VOTE_COUNTING_METHODS.PluralityLog,

    },
    ZKP: {
        ppName: 'ZKP',
        activated: IMG_CONSTS.CONSTS.NO,
        performType: IMG_CONSTS.POLL_PERFORMANCE_TYPES.ZKP,
        voteAmendmentAllowed: IMG_CONSTS.CONSTS.NO,
        votesCountingMethod: IMG_CONSTS.VOTE_COUNTING_METHODS.PluralityLog,
    },

}


// IMG_CONSTS.setCycleByMinutes(2 * IMG_CONSTS.TIME_GAIN); // 12 h  * 60 minute    it used for coinbase cycle,double-spending prevention...

// IMG_CONSTS.COINBASE_CHECK_BY_MINUTE = (IMG_CONSTS.getCycleByMinutes() / 10); // less than every 2 hours(71 minutes) the machin controls if she must create a coinbase?


module.exports = IMG_CONSTS;
