module.exports = {
    Coinbase: 'Coinbase',
    RpDoc: 'RpDoc', // Repayment doc
    RlDoc: 'RlDoc', // Release Reserved coins
    BasicTx: 'BasicTx',    // simple "m of n" trx

    // mimblewimble transactions
    MWIngress: 'MWIngress',    // move PAIs from basic transaction to a mimble transaction
    MWTx: 'MWIngress',    // a mimble transaction
    MWEgress: 'MWEgress',    // move back PAIs from a mimble transaction to a basic transaction

    // ZKP zk-snarks transactions
    ZKIngress: 'MWIngress',    // move PAIs from basic transaction to a Zero Knowladge Proof transaction
    ZKTx: 'MWIngress',    // a Zero Knowladge Proof transaction
    ZKEgress: 'MWEgress',    // move back PAIs from a Zero Knowladge Proof transaction to a basic transaction

    // confidential trx Monero-like
    MNIngress: 'MWIngress',    // move PAIs from basic transaction to a Zero Knowladge Proof transaction
    MNTx: 'MWIngress',    // a Zero Knowladge Proof transaction
    MNEgress: 'MWEgress',    // move back PAIs from a Zero Knowladge Proof transaction to a basic transaction
    
    // RGB (colored coins) Transactions
    RGBIngress: 'MWIngress',    // move PAIs from basic transaction to a Zero Knowladge Proof transaction
    RGBTx: 'MWIngress',    // a Zero Knowladge Proof transaction
    RGBEgress: 'MWEgress',    // move back PAIs from a Zero Knowladge Proof transaction to a basic transaction


    DPCostPay: 'DPCostPay',

    DNAProposal: 'DNAProposal',

    AdmPolling: 'AdmPolling',
    // ReqForRelRes: 'ReqForRelRes',    remove it to AdmPolling
    

    Pledge: 'Pledge',
    ClosePledge: 'ClosePledge',

    Polling: 'Polling',
    Ballot: 'Ballot',

    CPost: 'CPost', // Custom Posts (files, Agora posts, wiki pages...)

    INameReg: 'INameReg', // Flens: imagine flexible & largly extensible name service
    INameBind: 'INameBind', // binds a iPGP(later GNU GPG) to an iName
    INameMsgTo: 'INameMsgTo', // general message to a registered iName



    customDoc: '0xa000', // custom usages
}
