const _ = require('lodash');
const iConsts = require('./constants');

let utils = null;
let admPollingsHandler = null;
let clog = null;
let listener = null;

class ConfParamsHandler {

    static getBasePricePerChar(args = {}) {
        args.pollingKey = 'RFRfBasePrice';
        return this.getSingleIntegerValue(args);
    }

    /**
     * returns minimum transaction fee by microPAI
     */
    static getTransactionMinimumFee(args = {}) {
        let cDate = args.cDate;
        return Math.floor(
            iConsts.TRANSACTION_MINIMUM_LENGTH *
            this.getBasePricePerChar({ cDate }) *
            this.getDocExpense({
                cDate,
                dType: iConsts.DOC_TYPES.BasicTx,
                dLen: iConsts.TRANSACTION_MINIMUM_LENGTH
            })
        );
    }

    static prepareDocExpenseDict(args) {
        let cDate = args.cDate;
        let dLen = args.dLen;

        let servicePrices = {}
        servicePrices[iConsts.DOC_TYPES.BasicTx] = this.getBasicTxDPCost({ cDate, dLen })
        servicePrices[iConsts.DOC_TYPES.AdmPolling] = this.getPollingDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.Polling] = this.getPollingDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.Pledge] = this.getPledgeDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.ClosePledge] = this.getClosePledgeDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.DNAProposal] = this.getCloseDNAProposalDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.Ballot] = this.getBallotDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.INameReg] = this.getINameRegDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.INameBind] = this.getINameBindDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.INameMsgTo] = this.getINameMsgDPCost({ cDate });
        servicePrices[iConsts.DOC_TYPES.CPost] = this.getCPostDPCost({ cDate });
        return servicePrices;
    }

    static getDocExpense(args) {
        if (utils == null)
            utils = require('../utils/utils');

        let dType = args.dType;
        let dClass = args.dClass;
        let cDate = args.cDate;
        let dLen = args.dLen;
        // let dLen = _.has(args, 'dLen') ? args.dLen : iConsts.TRANSACTION_MINIMUM_LENGTH;

        if (dLen > iConsts.MAX_DOC_LENGTH_BY_CHAR)
            return 0;

        let servicePrices = this.prepareDocExpenseDict({
            cDate,
            dLen
        });

        // servicePrices[iConsts.DOC_TYPES.BasicTx] = this.getBasicTxDPCost({ cDate, dLen })
        // servicePrices[iConsts.DOC_TYPES.AdmPolling] = this.getPollingDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.Polling] = this.getPollingDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.Pledge] = this.getPledgeDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.ClosePledge] = this.getClosePledgeDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.DNAProposal] = this.getCloseDNAProposalDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.Ballot] = this.getBallotDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.INameReg] = this.getINameRegDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.INameBind] = this.getINameBindDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.INameMsgTo] = this.getINameMsgDPCost({ cDate });
        // servicePrices[iConsts.DOC_TYPES.CPost] = this.getCPostDPCost({ cDate });

        if (_.has(servicePrices, dType))
            return servicePrices[dType];

        // if type of documents is not defined, so accept it as a base feePerByte
        if (listener == null)
            listener = require('../plugin-handler/plugin-handler');
        let pluginPrice = listener.doCallSync('SASH_calc_service_price', args);
        if (_.has(pluginPrice, 'err') && pluginPrice.err != false) {
            utils.exiter(`wrong plugin price calc for ${utils.stringify(args)}`, 434);
        }
        console.log('argssssssss', args);
        console.log(ZZargs);

        if (!_.has(pluginPrice, 'fee')) {
            utils.exiter(`missed price fee for dType(${dType})`, 34)
        }
        return pluginPrice.fee;
    }

    static getBasicTxDPCost(args = {}) {
        args.pollingKey = 'RFRfTxBPrice';
        let TxBasePrice = this.getSingleIntegerValue(args);

        /**
         * TODO: maybe can be modified in next version of transaction to be more fair
         * specially after implementing the indented bach32 unlockers(recursively unlimited unlockers which have another bech32 as an unlocker)
         */
        let dLen = args.dLen;
        let lenFactor = utils.calcLog(dLen, iConsts.MAX_DOC_LENGTH_BY_CHAR).revGain;//(dLen * 1) / (iConsts.MAX_DOC_LENGTH_BY_CHAR * 1);
        let cost = utils.iFloorFloat(TxBasePrice * Math.pow(100 * lenFactor, 20));
        return cost;
    }

    static getPollingDPCost(args = {}) {
        args.pollingKey = 'RFRfPollingPrice';
        return this.getSingleIntegerValue(args);
    }

    static getPledgeDPCost(args = {}) {
        args.pollingKey = 'RFRfPLedgePrice';
        return this.getSingleIntegerValue(args);
    }

    static getClosePledgeDPCost(args = {}) {
        args.pollingKey = 'RFRfClPLedgePrice';
        return this.getSingleIntegerValue(args);
    }

    static getCloseDNAProposalDPCost(args = {}) {
        args.pollingKey = 'RFRfDNAPropPrice';
        return this.getSingleIntegerValue(args);
    }

    static getBallotDPCost(args = {}) {
        args.pollingKey = 'RFRfBallotPrice';
        return this.getSingleIntegerValue(args);
    }

    static getINameRegDPCost(args = {}) {
        args.pollingKey = 'RFRfINameRegPrice';
        return this.getSingleIntegerValue(args);
    }

    static getINameBindDPCost(args = {}) {
        args.pollingKey = 'RFRfINameBndPGPPrice';
        return this.getSingleIntegerValue(args);
    }

    static getINameMsgDPCost(args = {}) {
        args.pollingKey = 'RFRfINameMsgPrice';
        return this.getSingleIntegerValue(args);
    }

    static getCPostDPCost(args = {}) {
        args.pollingKey = 'RFRfCPostPrice';
        return this.getSingleIntegerValue(args);
    }

    static getBlockFixCost(args = {}) {
        args.pollingKey = 'RFRfBlockFixCost';
        return this.getSingleIntegerValue(args);
    }

    // shares parameters settings
    static getMinShareToAllowedIssueFVote(args = {}) {
        args.pollingKey = 'RFRfMinFVote';
        return this.getSingleFloatValue(args);
    }

    static getMinShareToAllowedWiki(args = {}) {
        args.pollingKey = 'RFRfMinS2Wk';
        return this.getSingleFloatValue(args);
    }

    static getMinShareToAllowedDemos(args = {}) {
        args.pollingKey = 'RFRfMinS2DA';
        return this.getSingleFloatValue(args);
    }

    static getMinShareToAllowedVoting(args = {}) {
        args.pollingKey = 'RFRfMinS2V';
        return this.getSingleFloatValue(args);
    }

    static getMinShareToAllowedSignCoinbase(args = {}) {
        args.pollingKey = 'RFRfMinFSign';
        return this.getSingleFloatValue(args);
    }

    static getSingleFloatValue(args = {}) {
        if (utils == null)
            utils = require('../utils/utils');
        if (admPollingsHandler == null)
            admPollingsHandler = require('../web-interface/adm-pollings/adm-pollings-handler');
        if (clog == null)
            clog = require('../loggers/console_logger');

        let pollingKey = args.pollingKey;
        let cDate = args.cDate;
        if (utils._nilEmptyFalse(cDate)) {
            console.log(`invalid cDate for get Single Float Value for(${pollingKey}) ${cDate}`);
            utils.exiter(dummyNotExistVar, 812)
        }

        // fetch from DB the price for calculation Date
        let value = admPollingsHandler.onChain.getAdmValue({
            cDate,
            pollingKey
        });
        value = utils.iFloorFloat(parseFloat(value));
        let msg = `------------> > > > RFRf(float) ${pollingKey}(${cDate}) value: ${value}`;
        console.log(msg);
        clog.app.info(msg);
        return value;
    }

    static getSingleIntegerValue(args = {}) {
        if (utils == null)
            utils = require('../utils/utils');
        if (admPollingsHandler == null)
            admPollingsHandler = require('../web-interface/adm-pollings/adm-pollings-handler');
        if (clog == null)
            clog = require('../loggers/console_logger');

        let pollingKey = args.pollingKey;
        let cDate = args.cDate;
        if (utils._nilEmptyFalse(cDate)) {
            utils.exiter(`invalid cDate for get Single Integer Value for(${pollingKey}) ${cDate}`, 542)
        }

        // fetch from DB the price for calculation Date
        let value = admPollingsHandler.onChain.getAdmValue({
            cDate,
            pollingKey
        });
        value = parseInt(value);
        let msg = `------------> > > > RFRf(int) ${pollingKey}(${cDate}) value: ${value}`;
        console.log(msg);
        clog.app.info(msg);
        return value;
    }

    static getPoWDifficulty(args = {}) {
        if (utils == null)
            utils = require('../utils/utils');

        let cDate = args.cDate;

        if (cDate < '2020-10-01 00:00:00') {
            return 4;
        } else if (('2020-10-01 00:00:00' <= cDate) && (cDate < '2021-00-01 00:00:00')) {
            return 6;
        } else if (('2021-00-01 00:00:00' <= cDate) && (cDate < '2021-06-01 00:00:00')) {
            return 10;
        } else {
            return 18;
        }
    }

}

module.exports = ConfParamsHandler;
