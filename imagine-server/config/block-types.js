module.exports = {
    Genesis: 'Genesis',
    Coinbase: 'Coinbase',
    FSign: 'FSign', // floating signature
    Normal: 'Normal',
    POW: 'POW',
    SusBlock: 'SusBlock', // suspicious/suspended block
    FVote: 'FVote',
    RpBlock: 'RpBlock', // repayment blocks which are used to record repayments in DAG immediately after Coinbase block isuance
    
    /**
     * release Reserved blocks which are used to record issuance of reserved PAIs in DAG
     * the reserves can be released after passing a certain time of creation the Originated Coinbase block and by a certain percentage of vote of shareholders of that coinbase block
     */
    RlBlock: 'RlBlock', // release reserved coins block
}