const _ = require('lodash');
const iConsts = require('../config/constants')
const utils = require('../utils/utils');
const clog = require('../loggers/console_logger');
const nodemailer = require("nodemailer");
const machine = require('../machine/machine-handler');
const fs = require('fs');
const fileHandler = require('../hard-copy/file-handler')
const crypto = require('../crypto/crypto');
const kvHandler = require('../models/kvalue');

let Imap = require('imap');
let inspect = require('util').inspect;




class IMAPFetchHandler {

    static testIMAPReceiveConnection(args) {

        return new Promise((resolve, reject) => {
            IMAPFetchHandler.imap = null;
            clog.app.info(`test Receive Connection args: ${utils.stringify(args)}`);
            let res = IMAPFetchHandler.prepareAConnection(args);
            if (res.err != false)
                return reject(res);
            if (!IMAPFetchHandler.imap == null)
                return reject({ err: true, msg: `couldn't stablish IMAP connection` });

            IMAPFetchHandler.imap.once('ready', function () {
                // console.log(IMAPFetchHandler.imap);
                return resolve({ err: false, msg: `IMAP connection is OK` });
            });

            IMAPFetchHandler.imap.once('error', function (err) {
                console.log(err);
                return reject({ err: true, msg: `couldn't stablish IMAP connection2` });
            });

            IMAPFetchHandler.imap.once('end', function () {
                console.log('Connection ended');
            });

            IMAPFetchHandler.imap.connect();

            // console.log(IMAPFetchHandler.imap);
        });



        // return { err: false, msg: `IMAP connection is OK` }
    }

    static prepareAConnection(args) {
        clog.app.info(`test prepare a Connection args: ${utils.stringify(args)}`);
        let user = _.has(args, 'emailAddress') ? args.emailAddress : '';
        let password = _.has(args, 'password') ? args.password : '';
        let host = _.has(args, 'host') ? args.host : '';
        let port = _.has(args, 'port') ? args.port : '';
        let tls = _.has(args, 'tls') ? args.tls : true;

        if (
            utils._nilEmptyFalse(user) ||
            utils._nilEmptyFalse(password) ||
            utils._nilEmptyFalse(host) ||
            utils._nilEmptyFalse(port)
        ) {
            msg = `missed IMAP parameters ${utils.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg };
        }

        IMAPFetchHandler.emailAddress = user;
        IMAPFetchHandler.imap = new Imap({
            user,
            password,
            host,
            port,//: 993,
            tls,//: true
        });
        return { err: false }

    }

    static fetchInbox(args) {
        
        try {
            clog.app.info(`IMAP fetch args: ${utils.stringify(args)}`);
            IMAPFetchHandler.prepareAConnection(args);
            

            let msg;
            let emailAddress = _.has(args, 'emailAddress') ? args.emailAddress : '';

            if (
                utils._nilEmptyFalse(emailAddress)
            ) {
                msg = `missed IMAP parameters! ${utils.stringify(args)}`;
                clog.app.error(msg);
                return { err: true, msg };
            }

            IMAPFetchHandler.imap.once('ready', function () {
                switch (args.funcMode) {
                    case 'readFromDate':
                        IMAPFetchHandler.readFromDate();
                        break;

                    case 'readFirstNEmails':
                        IMAPFetchHandler.readFirstNEmails();
                        break;

                    case 'readUNSEENs':
                    default:
                        IMAPFetchHandler.readUNSEENs();
                        break;
                }
            });

            IMAPFetchHandler.imap.once('error', function (err) {
                console.log(`err in imap ${utils.stringify(err)}`);
            });

            IMAPFetchHandler.imap.once('end', function () {
                console.log('Connection ended');
            });

            IMAPFetchHandler.imap.connect();
        } catch (e) {
            console.log(`error in fetching inbox`);
            console.log(e);
            return { err: true, msg: utils.stringify(e) };
        }

    }

    static readUNSEENs() {
        IMAPFetchHandler.openInbox(function (err, box) {
            // console.log('box info: ', box);
            if (err) {
                console.log('error in openInbox');
                throw err;
            }
            IMAPFetchHandler.imap.search(['UNSEEN'], (err, results) => {
                if (err) {
                    console.log('error in search Inbox');
                    throw err;
                }
                // console.log('results', results);
                if (results.length == 0) {
                    let msg = `no unseen message exist in ${IMAPFetchHandler.emailAddress}`;
                    clog.app.info(msg);
                    return { err: false, msg }
                }
                var f = IMAPFetchHandler.imap.fetch(
                    results,
                    {
                        bodies: ['HEADER.FIELDS (FROM)', 'TEXT'],
                        struct: true
                    }
                );
                f.on('message', function (msg, seqno) {
                    // console.log('Message #%d', seqno);
                    var prefix = '(#' + seqno + ') ';
                    msg.on('body', function (stream, info) {
                        // console.log('info', info);
                        msg.once('attributes', function (attrs) {
                            console.log('Attributes: %s', inspect(attrs, false, 8));
                        });
                        msg.once('end', function () {
                            console.log('Finished');
                        });

                        var buffer = '';
                        stream.on('data', function (chunk) {
                            buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function () {
                            // console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                            // do process and mrk as seen 
                            IMAPFetchHandler.ParsAndWriteToInboxFolderOnHd(buffer);

                            IMAPFetchHandler.imap.seq.addFlags(seqno, '\\Seen', function (err) {
                                if (err) throw err;
                                let notiMsg = `email seqno(${seqno}) was marked as seen in mailbox`;
                                console.log(notiMsg);
                                clog.app.info(notiMsg);
                            });

                        });

                    });
                    msg.once('attributes', function (attrs) {
                        // console.log('Attributes: %s', inspect(attrs, false, 8));
                    });
                    msg.once('end', function () {
                        console.log('Finished');
                    });
                });
                f.once('error', function (err) {
                    console.log('Fetch error: ' + err);
                });
                f.once('end', function () {
                    console.log('Done fetching all messages!');
                    IMAPFetchHandler.imap.end();
                });
            });
        });
    }

    static readFromDate() {

        IMAPFetchHandler.openInbox(function (err, box) {
            if (err) {
                console.log('must not enter here1');
                throw err;
            } else {
                let seenEmails = kvHandler.getValueSync('seenEmails');
                if (utils._nilEmptyFalse(seenEmails)) {
                    seenEmails = {};
                    kvHandler.upsertKValueSync('seenEmails', utils.stringify(seenEmails));
                } else {
                    // reomve old seen
                    seenEmails = utils.parse(seenEmails);
                    let refreshSeen = {};
                    let cDate = utils.minutesBefore(iConsts.getCycleByMinutes() * 2);

                    /**
                     * this part removes seenemails from db, because of they are old and already handled. and they are not exist in mailBox(removing from inbos still doesn/t implemented)  TODO: fixit
                     * also the old emails didn't fetch from inbox because in search we are using 'SINCE' filter. but it not work properly! TODO: fixit
                     * 
                     */
                    let SINCE_filter_works = false;
                    if (SINCE_filter_works) {
                        for (let aHash of utils.objKeys(seenEmails)) {
                            if (seenEmails[aHash] > cDate)
                                refreshSeen[aHash] = seenEmails[aHash];
                        }
                    }

                    kvHandler.upsertKValueSync('seenEmails', utils.stringify(refreshSeen));
                }
                clog.app.info(`Already seenEmails ${utils.stringify(seenEmails)}`);

                let lastIMAPCheck = kvHandler.getValueSync('lastIMAPCheck');
                if (utils._nilEmptyFalse(lastIMAPCheck)) {
                    lastIMAPCheck = utils.minutesBefore(iConsts.getCycleByMinutes());
                    kvHandler.upsertKValueSync('lastIMAPCheck', lastIMAPCheck);
                }
                clog.app.info(`IMAP check fo emaile after ${lastIMAPCheck}`);
                IMAPFetchHandler.imap.search([['SINCE', lastIMAPCheck]], (err, results) => {
                    if (err) {
                        console.log('must not enter here2');
                        console.log('must not enter here2');
                        console.log('must not enter here2');
                        console.log('must not enter here2');
                        throw err;

                    } else {
                        try {
                            console.log('results', results);
                            var f = IMAPFetchHandler.imap.fetch(results, { bodies: ['HEADER.FIELDS (FROM)', 'TEXT'] });
                            // console.log('after fetch');

                            f.on('message', function (msg, seqno) {
                                // console.log('Message #%d', seqno);
                                var prefix = '(#' + seqno + ') ';
                                msg.on('body', function (stream, info) {
                                    // console.log(prefix + 'Body');
                                    let tmpName = IMAPFetchHandler.emailAddress + 'msg-' + seqno + `-${utils.getNowSSS()}-${Math.random() * 10000}.txt`;
                                    stream.pipe(fs.createWriteStream(`${iConsts.HD_PATHES.temporary}/${tmpName}`));
                                    setTimeout(() => {
                                        console.log(`deleteing inbox ${seqno}`);
                                        IMAPFetchHandler.imap.seq.addFlags(seqno, '\\Deleted', function (err) {
                                            console.log(`err in delete inbox ${err}`);
                                            let notiMsg = `email seqno(${seqno}) was deleted from mailbox`;
                                            console.log(notiMsg);
                                            clog.app.info(notiMsg);
                                        });

                                        // IMAPFetchHandler.ParsAndWritePacket(tmpName);
                                    }, 40000);
                                });
                                msg.once('attributes', function (attrs) {
                                    // console.log('Attributes: %s', inspect(attrs, false, 8));
                                });
                                msg.once('end', function () {
                                    console.log('Finished');
                                });
                            });
                            f.once('error', function (err) {
                                console.log('Fetch error: ' + err);
                            });
                            f.once('end', function () {
                                console.log('Done fetching all messages!');
                                IMAPFetchHandler.imap.end();
                            });
                        } catch (e) {
                            console.log('error in fetch');
                            console.log(e);
                        }

                    }
                });
            }

        });
    }

    static ParsAndWriteToInboxFolderOnHd(content) {
        let msg;
        content = content.replace('==', "B64DoubleEqual"); // dummy base64 == remove
        content = content.replace(/=(\r\n)/gm, ""); // dummy break remover
        content = content.replace(/(\n|\r)/gm, "");
        content = content.replace("B64DoubleEqual", '=='); // dummy base64 == restor
        clog.app.info(`Pars AndWriteToInboxFolderOn Hd: ${content}`);

        if (utils._nilEmptyFalse(content)) {
            msg = `empty content in email`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        if (
            content.includes(iConsts.msgTags.senderStartTag) &&
            content.includes(iConsts.msgTags.senderEndTag) &&
            content.includes(iConsts.msgTags.iPGPStartEnvelope) &&
            content.includes(iConsts.msgTags.iPGPEndEnvelope) &&
            content.includes(iConsts.msgTags.hashStartTag) &&
            content.includes(iConsts.msgTags.hashEndTag)
        ) {
            let emailHash = content.split(iConsts.msgTags.hashStartTag)[1].split(iConsts.msgTags.hashEndTag)[0];
            let sender = content.split(iConsts.msgTags.senderStartTag)[1].split(iConsts.msgTags.senderEndTag)[0];
            let pureContent = content.split(iConsts.msgTags.iPGPStartEnvelope)[1].split(iConsts.msgTags.iPGPEndEnvelope)[0];
            if (utils._nilEmptyFalse(sender) || utils._nilEmptyFalse(pureContent)) {
                msg = `empty pureContent or missed sender in email`;
                clog.app.error(msg);
                return { err: true, msg }
            }
            let finalContent = iConsts.msgTags.senderStartTag + sender + iConsts.msgTags.senderEndTag;
            finalContent += iConsts.msgTags.receiverStartTag + IMAPFetchHandler.emailAddress + iConsts.msgTags.receiverEndTag;
            finalContent += iConsts.msgTags.iPGPStartEnvelope + pureContent + iConsts.msgTags.iPGPEndEnvelope;
            let fileName = `${sender}-${utils.getNowSSS()}-${emailHash}.txt`;
            clog.app.info(`creating file in inbox ${fileName}`);
            let res = fileHandler.packetFileCreateSync({
                dirName: iConsts.HD_PATHES.inbox, //.
                fileName,
                fileContent: finalContent
            });
            if (res.err != false) {
                clog.app.error(`couldn't create file ${fileName}`);
            }
            // }
        } else {
            clog.app.error(`email missed some necessary tags:${utils.stringify(content)}`);

        }
    }

    static readFirstNEmails() {
        IMAPFetchHandler.openInbox((err, box) => {
            console.log('box', box);
            if (err) throw err;
            var f = IMAPFetchHandler.imap.seq.fetch(
                '1:3',
                {
                    markSeen: false,
                    bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT'],
                    struct: true
                }
            );
            // var f = IMAPFetchHandler.imap.fetch(results, { bodies: ['HEADER.FIELDS (FROM)', 'TEXT'] });

            f.on('message', function (msg, seqno) {
                console.log('Message #%d', seqno);
                var prefix = '(#' + seqno + ') ';
                msg.on('body', function (stream, info) {
                    var buffer = '';
                    stream.on('data', function (chunk) {
                        buffer += chunk.toString('utf8');
                    });
                    stream.once('end', function () {
                        console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                        // do process and delete 
                        // console.log(buffer);

                        console.log(`deleteing inbox ${seqno}`);
                        IMAPFetchHandler.imap.seq.addFlags(seqno, '\\Seen', function (err) {
                            if (err) throw err;
                            let notiMsg = `email seqno(${seqno}) was deleted from mailbox`;
                            console.log(notiMsg);
                            clog.app.info(notiMsg);
                        });

                    });
                });
                msg.once('attributes', function (attrs) {
                    console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                });
                msg.once('end', function () {
                    console.log(prefix + 'Finished');
                });
            });
            f.once('error', function (err) {
                console.log('Fetch error: ' + err);
            });
            f.once('end', function () {
                console.log('Done fetching all messages!');
                IMAPFetchHandler.imap.end();
            });
        });
    }

    static openInbox(cb) {
        try{
            IMAPFetchHandler.imap.openBox('INBOX', false, cb);
        }catch(e){
            console.error(e);
        }
    }
}

module.exports = IMAPFetchHandler;