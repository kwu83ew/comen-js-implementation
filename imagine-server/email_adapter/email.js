const _ = require('lodash');
const iConsts = require('../config/constants')
const utils = require('../utils/utils');
const clog = require('../loggers/console_logger');
const nodemailer = require('nodemailer');


class EmailHandler {

    static sendEmailWrapper(args) {
        console.log(`send EmailWrapper args: ${utils.stringify(args)}`);
        clog.app.info(`send EmailWrapper args:${utils.stringify(args)}`);
        const machine = require('../machine/machine-handler');
        let machineSettings = machine.getMProfileSettingsSync()
        let sender = _.has(args, 'sender') ? args.sender : null;

        if (machineSettings.prvEmail.address == sender) {
            args.sender = machineSettings.prvEmail.address;
            args.pass = machineSettings.prvEmail.pwd;
            args.host = machineSettings.prvEmail.outgoingMailServer;
        } else {
            args.sender = machineSettings.pubEmail.address;
            args.pass = machineSettings.pubEmail.pwd;
            args.host = machineSettings.pubEmail.outgoingMailServer;
        }
        return EmailHandler.send_mail(args);
    }

    static send_mail(args) {
        console.log(args);
        clog.app.info(`send_mail args: ${args}`);
        // const machine = require('../machine/machine-handler');
        // let machineSettings = machine.getMProfileSettingsSync()
        let emailTitle = _.has(args, 'title') ? args.title : null;
        let emailBody = _.has(args, 'message') ? args.message : null;
        let receiver = _.has(args, 'receiver') ? args.receiver : null;
        let transporter = nodemailer.createTransport({
            host: args.host,
            service: "inbox",
            auth: {
                user: args.sender,
                pass: args.pass
            }
        });
        var mailOptions = {
            from: args.sender,
            to: receiver, // list of receivers
            subject: emailTitle, // Subject line
            text: emailBody
        };

        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.error(err);
                return ({ err: true, msg: err });
            } else {
                clog.app.info('Message sent: ' + info.response);
                return ({ err: false, msg: info.response });
            };
        });
        return ({ err: false, msg: 'email sent' });
    }
}
EmailHandler.IMAPFetcher = require('./imap-fetch');

module.exports = EmailHandler;