module.exports = {

    HANDSHAKE: "handshake",
    NICETOMEETYOU: "niceToMeetYou",
    HEREISNEWNEIGHBOR: "hereIsNewNeighbor",

    // TODO: move these commands to GQL format
    DAG_INVOKE_LEAVES: "dagInvokeLeaves",
    DAG_LEAVES_INFO: "dagLeavesInfo",
    DAG_INVOKE_BLOCK: "dagInvokeBlock",
    DAG_INVOKE_DESCENDENTS: "dagInvokeDescendents",

}