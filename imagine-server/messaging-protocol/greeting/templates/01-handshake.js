const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const MESSAGE_TYPES = require('../../message-types')

// the node introduce herself to some email which she has.
// these emails got from friends, or somewhere in internet or from seed emails list
// this is the only email could be signed or not, encrypted or not.
// the rest comiunications MUST encrypted and signed
module.exports = function (args = {}) {
    let out = {
        type: MESSAGE_TYPES.HANDSHAKE,
        mVer: "0.0.0",
        connectionType: args.connectionType,
        email: args.email, // sender email
        // the node public key is used to secure comiunication between nodes
        PGPPubKey: args.PGPPubKey //sender's iPGP public key
    };
    return out;
}