const MESSAGE_TYPES = require('../../message-types')

// in response of handshake the receiver(s) send the message hereIsNewNode to her neighbors

module.exports = function(args) {
    return {
        type: MESSAGE_TYPES.HEREISNEWNEIGHBOR,
        mVer: "0.0.0",
        connectionType: args.connectionType,
        newNeighborEmail: args.newNeighborEmail, // sender email
        // the node public key, it will be used for earning transaction fees, also to secure comiunication between nodes
        // maybe use this as a node honestness & reputation  
        newNeighborPGPPubKey: args.newNeighborPGPPubKey //sender's iPGP public key
    }
}