const MESSAGE_TYPES = require('../../message-types')
    // in response of handshake the receiver(s) send the message niceToMeetYou
    // it sends some information about herself
module.exports = function(args) {
    return {
        type: MESSAGE_TYPES.NICETOMEETYOU,
        connectionType: args.connectionType,
        mVer: "0.0.0",
        email: args.email,
        PGPPubKey: args.PGPPubKey,

    }
}