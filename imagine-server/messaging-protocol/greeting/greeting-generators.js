const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const packetTemplates = require('../templates');

const MESSAGE_TYPES = require('../message-types')


class GreetingGenerators {

    static async writeHandshake(args) {
        const machine = require('../../machine/machine-handler');
        clog.app.info(`write Handshake args: ${utils.stringify(args)}`);

        let connectionType = _.has(args, 'connectionType') ? args.connectionType : null;
        let receiverId = _.has(args, 'receiverId') ? args.receiverId : null;

        if (utils._nilEmptyFalse(connectionType))
            return { err: true, msg: `The connectionType can not be ${connectionType}` }

        if (utils._nilEmptyFalse(receiverId))
            return { err: true, msg: `The receiverId can not be ${receiverId}` }

        let receiverInfo = machine.neighborHandler.getNeighborsSync({
            query: [
                ['n_id', receiverId]
            ]
        });
        if (receiverInfo.length != 1)
            return { err: true, msg: `Wrong receiver! ${utils.stringify(receiverInfo)}` }
        receiverInfo = receiverInfo[0]

        let machineSettings = await machine.getMProfileSettingsAsync();
        let email = (connectionType == iConsts.CONSTS.PRIVATE) ? machineSettings.prvEmail.address : machineSettings.pubEmail.address;
        let PGPPubKey = (connectionType == iConsts.CONSTS.PRIVATE) ? machineSettings.prvEmail.PGPPubKey : machineSettings.pubEmail.PGPPubKey;
        let emailBody = packetTemplates.handshake({
            connectionType,
            email,
            PGPPubKey,
            backerAddress: machineSettings.backerAddress    // this is optional(it added in first day of project to send some PAIs to use in system). TODO: remove this line after 6 month
        });
        emailBody = JSON.stringify(emailBody);
        clog.app.info(`packetGenerators.write Handshake emailBody: ${emailBody}`);

        emailBody = crypto.encryptPGP({
            shouldSign: false, // in real world you can not sign an email in negotiation step in which the receiver has not your pgp public key
            shouldCompress: true,
            message: emailBody,
            receiverPubKey: null //receiverInfo.public_key
        });
        emailBody = utils.breakByBR(emailBody);
        emailBody = crypto.wrapPGPEnvelope(emailBody)

        return {
            err: false,
            pkt: {
                title: 'Handshake from a new neighbor',
                sender: email,
                target: receiverInfo.nEmail,
                message: emailBody
            }
        };


        //TODO after successfull sending must save some part the result and change the email to confirmed
    }


    static writeNiceToMeetYou(args) {
        clog.app.info(`packetGenerators.write NiceToMeetYou args: ${utils.stringify(args)}`);

        const machine = require('../../machine/machine-handler');
        // try {
        let connectionType = _.has(args, 'connectionType') ? args.connectionType : null;
        let target = _.has(args, 'target') ? args.target : '';
        let receiverPubKey = _.has(args, 'receiverPubKey') ? args.receiverPubKey : '';
        if (utils._nilEmptyFalse(target) || utils._nilEmptyFalse(receiverPubKey)) {
            return { err: true, msg: `niceToMeetYou missed target email` };
        }

        if (utils._nilEmptyFalse(connectionType))
            return { err: true, msg: `In niceToMeetYou, the connectionType can not be ${connectionType}` }


        let machineSettings = machine.getMProfileSettingsSync()

        let emailBody = packetTemplates.niceToMeetYou({
            connectionType,
            email: machineSettings.pubEmail.address,
            PGPPubKey: machineSettings.pubEmail.PGPPubKey
        });
        emailBody = utils.stringify(emailBody);
        clog.app.info(`packetGenerators.write NiceToMeetYou emailBody1: ${emailBody}`);

        emailBody = crypto.encryptPGP({
            shouldSign: false, // still we can not sign the message, because the receiver(maybe) have not our publick key yet
            shouldCompress: true,
            message: emailBody,
            receiverPubKey
        });
        emailBody = utils.breakByBR(emailBody);
        emailBody = crypto.wrapPGPEnvelope(emailBody)
        clog.app.debug(`packetGenerators.write NiceToMeetYou emailBody2: ${utils.stringify(emailBody)}`)

        return {
            err: false,
            pkt: {
                title: 'niceToMeetYou',
                sender: machineSettings.pubEmail.address,
                target: target,
                message: emailBody
            }
        }
        // } catch (e) {
        //     clog.app.error(e)
        // }

        //TODO after successfull sending must save some part the result and change the email to confirmed
    }



    static writeHereIsNewNeighbor(args) {
        clog.app.info(`writeHereIsNew Neighbor args ${utils.stringify(args)}`);

        let connectionType = _.has(args, 'connectionType') ? args.connectionType : null;
        let machineEmail = _.has(args, 'machineEmail') ? args.machineEmail : null;
        let machinePGPPrvKey = _.has(args, 'machinePGPPrvKey') ? args.machinePGPPrvKey : null;
        let newNeighborEmail = _.has(args, 'newNeighborEmail') ? args.newNeighborEmail : null;
        let newNeighborPGPPubKey = _.has(args, 'newNeighborPGPPubKey') ? args.newNeighborPGPPubKey : null;
        let targetPGPPubKey = _.has(args, 'targetPGPPubKey') ? args.targetPGPPubKey : null;
        let target = _.has(args, 'target') ? args.target : null;

        // clog.app.info('------------ inside hereIsNewNeighbor generator');
        // clog.app.info(args);
        if (utils._nilEmptyFalse(newNeighborEmail) || utils._nilEmptyFalse(newNeighborPGPPubKey) ||
            utils._nilEmptyFalse(target) || utils._nilEmptyFalse(targetPGPPubKey))
            return null;



        let emailBody = packetTemplates.hereIsNewNeighbor({
            connectionType,
            newNeighborEmail: newNeighborEmail,
            newNeighborPGPPubKey: newNeighborPGPPubKey
        });
        emailBody = JSON.stringify(emailBody);
        let params = {
            shouldSign: true, // in real world you can not sign an email in negotiation step in which the receiver has not your pgp public key
            shouldCompress: true,
            message: emailBody,
            sendererPrvKey: machinePGPPrvKey,
            receiverPubKey: targetPGPPubKey
        }
        clog.app.info(`writeHereIsNew Neighbor going to pgp ${utils.stringify(params)}`);
        emailBody = crypto.encryptPGP(params);
        emailBody = utils.breakByBR(emailBody);
        emailBody = crypto.wrapPGPEnvelope(emailBody)

        return {
            title: 'hereIsNewNeighbor',
            sender: machineEmail,
            target: target,
            message: emailBody
        };


        //TODO after successfull sending must save some part the result and change the email to confirmed
    }

}

module.exports = GreetingGenerators;
