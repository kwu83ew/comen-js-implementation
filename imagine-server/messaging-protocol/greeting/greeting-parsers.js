const _ = require('lodash');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const iConsts = require('../../config/constants');
const machine = require('../../machine/machine-handler');
const packetGenerator = require('./greeting-generators');


class GreetingParser {


    static parseHandshake(args) {
        const networker = require('../../network-adapter/network-pusher');
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        clog.app.info(`parseHandshake args: ${utils.stringify(args)}`);

        let message = _.has(args, 'message') ? args.message : null;
        let connectionType = _.has(message, 'connectionType') ? message.connectionType : null;
        let senderEmail = _.has(message, 'email') ? message.email : null;
        let PGPPubKey = _.has(message, 'PGPPubKey') ? message.PGPPubKey : null;
        let backerAddress = _.has(message, 'backerAddress') ? message.backerAddress : null;

        clog.app.debug(connectionType);
        if (utils._nilEmptyFalse(connectionType))
            return { err: true, msg: `invalid connectionType(${connectionType}) in parseHandshake`, shouldPurgeMessage: true }

        // just to be sure handshake happends ONLY ONE TIME for each email at the start
        // if user needs to change publickkey or ... she can send alternate messages like changeMyPublicKey(which MUST be signed with current key)
        // retreive sender's info
        let emailExist = false;
        let senderInfo = machine.neighborHandler.getNeighborsSync({
            query: [
                ['n_mp_code', mpCode],
                ['n_connection_type', connectionType],
                ['n_email', senderEmail],
            ]
        })
        if (senderInfo.length > 0) {
            // some security logs
            emailExist = true;
            clog.sec.info(`!!! the email in parse Handshake ${senderEmail} already inserted`);
        }

        // try {

        if (utils._nilEmptyFalse(senderEmail)) {
            clog.sec.info('!!! invalid email received from neighbor via handshake');
            return { err: true, msg: '!!! invalid email received from neighbor via handshake', shouldPurgeMessage: true }
        }
        if (utils._nilEmptyFalse(senderEmail) || utils._nilEmptyFalse(PGPPubKey)) {
            clog.sec.info('!!! missed PGPPubKey received from neighbor via handshake');
            return { err: true, msg: '!!! missed PGPPubKey received from neighbor via handshake', shouldPurgeMessage: true }
        }

        if (!emailExist) {
            // add new neighbor
            machine.neighborHandler.addANeighborSync({
                nEmail: senderEmail,
                nPGPPubKey: PGPPubKey,
                nIsActive: iConsts.CONSTS.YES,
                nConnectionType: connectionType,
            });
        }

        // send response niceToMeetYou
        let res = packetGenerator.writeNiceToMeetYou({
            connectionType,
            target: senderEmail,
            receiverPubKey: PGPPubKey
        });
        clog.app.info(`packetGenerators.write NiceToMeetYou res: ${utils.stringify(res)}`);
        if (res.err != false) {
            res.shouldPurgeMessage = true;
            return res;
        }
        let sent = networker.iPushSync(res.pkt);

        // broadcast the email to other neighbors
        if (connectionType == iConsts.CONSTS.PUBLIC) {
            setTimeout(() => {
                machine.neighborHandler.floodEmailToNeighbors(senderEmail, PGPPubKey);
            }, 10000);
        }
        return { err: false, msg: '', shouldPurgeMessage: true }

    }


    static parseNiceToMeetYou(args) {
        clog.app.info(`parseNiceToMeetYou args: ${utils.stringify(args)}`);

        let message = _.has(args, 'message') ? args.message : null;

        let email = _.has(message, 'email') ? message.email : null;
        let senderPGPPubKey = _.has(message, 'PGPPubKey') ? message.PGPPubKey : null;
        let senderBackerAddress = _.has(message, 'backerAddress') ? message.backerAddress : null;


        let msg;
        // just to be sure handshake happends ONLY ONE TIME for each email at the start
        // if user needs to change publickkey or ... she can send alternate messages like changeMyPublicKey(which MUST be signed with current key)
        // retreive sender's info
        let senderInfo = machine.neighborHandler.getNeighborsSync({
            query: [
                ['n_email', email]
            ]
        })
        if (senderInfo.length == 0) {
            // some security logs
            msg = `!!! Machine has not this email ${email} as a neighbor`
            clog.sec.info(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // try {

        if (utils._nilEmptyFalse(email) || utils._nilEmptyFalse(senderPGPPubKey)) {
            msg = '!!! invalid email or PGPPubKey received from neighbor via handshake';
            clog.sec.info(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }


        let updates = {
            n_info: utils.stringify({}),
            n_pgp_public_key: senderPGPPubKey,
            n_last_modified: utils.getNow()
        }
        if (!utils._nilEmptyFalse(senderBackerAddress)) {
            updates['n_info'] = utils.stringify({
                backerAddress: senderBackerAddress
            });
        }
        // update neighbor info's PGP public key
        let updateRes = machine.neighborHandler.updateNeighbor({
            query: [
                ['n_email', email],
            ],
            updates
        });

        // TODO: publish this email to my neighbors

        return { err: false, shouldPurgeMessage: true }

        // } catch (err) {
        //     clog.app.error(err)
        //     return { err: true, msg: err, shouldPurgeMessage: null }
        // }

    }



    static parseHereIsNewNeighbor(args) {
        clog.app.info(`parse Here Is New Neighbor args ${utils.stringify(args)}`);

        let message = _.has(args, 'message') ? args.message : null;

        let connectionType = _.has(message, 'connectionType') ? message.connectionType : iConsts.CONSTS.PUBLIC;
        let newNeighborEmail = _.has(message, 'newNeighborEmail') ? message.newNeighborEmail : null;
        let newNeighborPGPPubKey = _.has(message, 'newNeighborPGPPubKey') ? message.newNeighborPGPPubKey : null;

        let msg;
        if (utils._nilEmptyFalse(newNeighborEmail) || utils._nilEmptyFalse(newNeighborPGPPubKey)) {
            msg = '!!! invalid email or PGPPubKey received from neighbor via hereIsNewNeighbor';
            clog.sec.info(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // check if the email is not machine email
        let machineSettings = machine.getMProfileSettingsSync();
        if ((machineSettings.pubEmail.address == newNeighborEmail) || (machineSettings.prvEmail.address == newNeighborEmail)) {
            msg = `!!! invalid newNeighborEmail which is machine email(${newNeighborEmail})`;
            clog.sec.info(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // duplicate email checking
        // if user needs to change publickkey or ... she can send alternate messages like changeMyPublicKey(which MUST be signed with current key)
        // retreive sender's info
        let newNeighborInfo = machine.neighborHandler.getNeighborsSync({
            query: [
                ['n_email', newNeighborEmail]
            ]
        })
        clog.app.info(`new Neighbor Email: ${newNeighborInfo}`);
        if (!utils._nilEmptyFalse(newNeighborInfo) && newNeighborInfo.length > 0) {
            // some security logs
            msg = `!!! Machine already has this email ${newNeighborEmail} as a neighbor`;
            clog.app.info(msg);
            return { err: false, msg, shouldPurgeMessage: true }
        }

        // try {


        // update neighbor info's PGP public key
        console.log('add ANeighborSync-----------------');
        machine.neighborHandler.addANeighborSync({
            nEmail: newNeighborEmail,
            nPGPPubKey: newNeighborPGPPubKey,
            nIsActive: iConsts.CONSTS.YES,
            nConnectionType: connectionType,
        });

        // publish this email to my neighbors too
        if ((connectionType == iConsts.CONSTS.PUBLIC) && iConsts.BROADCAST_TO_NEIGHBOR_OF_NEIGHBOR) {
            setTimeout(() => {
                machine.neighborHandler.floodEmailToNeighbors(newNeighborEmail, newNeighborPGPPubKey);
            }, 10000);
        }

        return { err: false, shouldPurgeMessage: true }

        // } catch (err) {
        //     clog.app.error(err)
        //     return { err: true, msg: err, shouldPurgeMessage: null }
        // }

    }
}

module.exports = GreetingParser;
