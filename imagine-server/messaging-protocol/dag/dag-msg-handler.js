const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const MESSAGE_TYPES = require('../message-types')
const model = require('../../models/interface');
const kvHandler = require('../../models/kvalue');
const clog = require('../../loggers/console_logger');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const leavesHandler = require('../../dag/leaves-handler');
const extinfosHandler = require('../../dag/extinfos-handler');
const missedBlocksHandler = require('../../dag/missed-blocks-handler');
const listener = require('../../plugin-handler/plugin-handler');
const parsingQHandler = require('../../services/parsing-q-manager/parsing-q-handler');
const blockUtils = require('../../dag/block-utils');
const machine = require('../../machine/machine-handler');

class DagMessagingHandler {

    static extractLeavesAndPushInSendingQ(args) {
        clog.app.info('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
        clog.app.info('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
        clog.app.info('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
        clog.app.info(JSON.stringify(args));
        clog.app.info('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
        let leaves = leavesHandler.getLeaveBlocks()
        clog.app.info(`leaves: ${JSON.stringify(leaves)}`);
        let payload = {
            type: MESSAGE_TYPES.DAG_LEAVES_INFO,
            mVer: "0.0.0",
            leaves
        };

        clog.app.info('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
        clog.app.info('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
        clog.app.info('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
        let pushRes = sendingQ.pushIntoSendingQ({
            sqType: MESSAGE_TYPES.DAG_LEAVES_INFO,
            sqCode: args.sq_code,
            sqPayload: utils.stringify(payload),
            sqTitle: `here are leaves info ${utils.getNow()}`,
            sqReceivers: [args.sender]
        })
        clog.app.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        clog.app.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        clog.app.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        return { err: false, shouldPurgeMessage: true }
    }

    static setLastReceivedBlockTimestamp(bType, hash, receiveDate = utils.getNow()) {
        model.sUpsert({
            table: 'i_kvalue',
            idField: 'kv_key',
            idValue: 'last_received_block_timestamp',
            updates: {
                kv_value: utils.stringify({
                    bType,
                    hash,
                    receiveDate
                }),
                kv_last_modified: utils.getNow()
            }
        })
    }


    static handleReceivedLeaveInfo(leaves) {
        const dagHandler = require('../../dag/graph-handler/dag-handler');

        // update last_received_leaves_info_timestamp
        this.setLastReceivedLeaveInfoTimestamp(leaves)

        // control if block exist in local, if not adding to missed blocks to invoke
        let alreadyRecordedInDAG;
        let missedHashes = [];
        utils.objKeys(leaves).forEach(hash => {
            alreadyRecordedInDAG = dagHandler.searchInDAGSync({
                query: [
                    ['b_hash', hash]
                ]
            });
            if (alreadyRecordedInDAG.length == 0)
                missedHashes.push(hash)
        });
        missedBlocksHandler.addMissedBlocksToInvoke(missedHashes);

        //maybe launch missed block invoker
        this.launchMissedBlocksInvoker()
    }

    static setLastReceivedLeaveInfoTimestamp(leaves, date = utils.getNow()) {
        model.sUpsert({
            table: 'i_kvalue',
            idField: 'kv_key',
            idValue: 'last_received_leaves_info_timestamp',
            updates: {
                kv_value: JSON.stringify({
                    leaves: leaves,
                    receiveDate: date
                }),
                kv_last_modified: utils.getNow()
            }

        })
    }


    static setMaybeAskForLatestBlocksFlag(value) {
        if (value == iConsts.CONSTS.YES) {

            // control last_received_leaves_info_timestamp flag
            // if we currently asked for leave information, so do not flood the network with multiple asking
            let lastLeaveInvokeResponse = kvHandler.getValueSync('last_received_leaves_info_timestamp');
            if (utils._nilEmptyFalse(lastLeaveInvokeResponse)) {
                return kvHandler.setSync('maybe_ask_for_latest_blocks', value);
            }

            lastLeaveInvokeResponse = JSON.parse(lastLeaveInvokeResponse)
            // TODO: tune the gap time
            if (utils.timeDiff(lastLeaveInvokeResponse.receiveDate) < iConsts.getInvokeLeavesGap())
                return;

            // control last_received_block_timestamp flag
            // if we are receiving continiuosly new blocks, it doesn't sence to ask for leave information.
            // this case happends in runing a new machin in which the machine has to download entire DAG.
            let lastReceivedBlockTimestamp = this.getLastReceivedBlockTimestamp()
            // TODO: tune the gap time
            if (utils.timeDiff(lastReceivedBlockTimestamp.receiveDate) < iConsts.getInvokeLeavesGap())
                return;


            let machineReqStatus = kvHandler.serachSync({
                query: [
                    ['kv_key', 'maybe_ask_for_latest_blocks']
                ]
            });
            if (machineReqStatus.length > 0) {
                let invokeAge = utils.timeDiff(machineReqStatus[0].kv_last_modified);
                clog.app.info(`invokeAge: ${invokeAge.asMinutes} < invokeGap: ${iConsts.getInvokeLeavesGap()}`);
                clog.cb.info(`invokeAge: ${invokeAge.asMinutes} < invokeGap: ${iConsts.getInvokeLeavesGap()}`);
                if (invokeAge.asMinutes < iConsts.getInvokeLeavesGap()) {
                    return;
                }
            }
            // TODO: tune the gap time
            setTimeout(DagMessagingHandler.launchInvokeLeaves, iConsts.ONE_MINUTE_BY_MILISECOND * 0.5);
        }

        return kvHandler.setSync('maybe_ask_for_latest_blocks', value);
    }

    static getLastReceivedBlockTimestamp() {
        let res = kvHandler.getValueSync('last_received_block_timestamp');
        if (res == null || res.length == 0)
            return { "bType": "Genesis", "hash": "-", "receiveDate": iConsts.getLaunchDate() };
        return utils.parse(res);
    }

    static getMaybeAskForLatestBlocksFlag() {
        return kvHandler.getValueSync('maybe_ask_for_latest_blocks');
    }




    static invokeLeaves() {
        let payload = {
            type: MESSAGE_TYPES.DAG_INVOKE_LEAVES,
            mVer: "0.0.0",
        };
        payload = JSON.stringify(payload)
        clog.cb.info('invoked for keaves');
        sendingQ.pushIntoSendingQ({
            sqType: MESSAGE_TYPES.DAG_INVOKE_LEAVES,
            sqCode: utils.getNow(),
            sqPayload: payload,
            sqTitle: `Invoke Leaves`,
        });
    }

    /**
     * this function controls the possibible abbondoned prerequisities which caused to blocking pasrs queue and finishing to DAG Asyncing
     */
    static launchDummyPrerequisitiesRemover() {
        DagMessagingHandler.dummyPrerequisitiesRemover();
    }

    static dummyPrerequisitiesRemover() {
        const dagHandler = require('../../dag/graph-handler/dag-handler');
        setTimeout(DagMessagingHandler.dummyPrerequisitiesRemover, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getDescendentsInvokeGap());
        // TODO: improve it to be more effieient 
        // first lopp on all blocks in Q
        let queuedPackets = parsingQHandler.qUtils.searchQSync({ fields: ['pq_prerequisites'] });

        for (let aPac of queuedPackets) {
            // control if already recorded in DAG
            let pre = utils.unpackCommaSeperated(aPac.pq_prerequisites);
            let query = [['b_hash', ['IN', pre]]]
            // if (machine.isInSyncProcess())
            //     query.push(['b_utxo_imported', 'Y'])    // to avoid removing prerequisities, before importing UTXOs
            if (Array.isArray(pre) && pre.length > 0) {
                let existedBlocksInDAG = dagHandler.searchInDAGSync({
                    fields: ['b_hash'],
                    query
                });
                if (existedBlocksInDAG.length > 0) {
                    for (let aBlock of existedBlocksInDAG) {
                        clog.app.info(`dummy Prerequisities Remover, removed dependencies to (${utils.hash6c(aBlock.bHash)}) since it is already exist in DAG`);
                        // remove dependency to this block
                        parsingQHandler.qUtils.removePrerequisitesSync(aBlock.bHash);
                    }
                }
            }
        }
    }


    /**
     * this function controls the possibible abbondoned blocks (as a leave) that haven't descendents!
     */
    static launchInvokeDescendents() {
        // prepare something;
        DagMessagingHandler.invokeDescendentsRecursive();
    }

    static invokeDescendentsRecursive() {
        setTimeout(DagMessagingHandler.invokeDescendentsRecursive, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getDescendentsInvokeGap());
        DagMessagingHandler.invokeDescendents();
    }

    /**
     * in case of machine feels long time no block received, she trys to ask neighbors about potentially descendent blocks
     */
    static invokeDescendents(args) {
        clog.app.info(`invoke Descendents args ${utils.stringify(args)}`);
        let denayDoubleSendCheck = _.has(args, 'denayDoubleSendCheck') ? args.denayDoubleSendCheck : false;
        // read latest recorded block in DAG
        const dagHandler = require('../../dag/graph-handler/dag-handler');
        let lastWBLock = dagHandler.walkThrough.getLatestBlock().wBlock;


        if (utils.timeDiff(lastWBLock.bCreationDate).asMinutes > iConsts.getAcceptableBlocksGap()) {
            // control if block's potentially descendent(s) exist in parsing q
            let likeHashRes = require('../../services/parsing-q-manager/parsing-q-handler').qUtils.searchParsingQSync({
                fields: ['pq_type', 'pq_code', 'pq_payload'],
                query: [
                    ['pq_type', ['IN', [iConsts.BLOCK_TYPES.Normal, iConsts.BLOCK_TYPES.Coinbase]]],
                    ['pq_code', lastWBLock.bHash]
                ]
            });
            // invoke network for block probably descendents
            let existedDescendentsInParsingQ = []
            if (likeHashRes.length > 0) {
                for (let wBlock of likeHashRes) {
                    let block = utils.parse(wBlock.pqPayload);
                    // if the existed block in parsing q is descendent of block
                    if (block.ancestors.includes(lastWBLock.bHash))
                        existedDescendentsInParsingQ.push(block.blockHash)
                }
            }
            if (existedDescendentsInParsingQ.length > 0) {
                // controling if the ancestors of descendent exist in local or not
                existedDescendentsInParsingQ = utils.arrayUnique(existedDescendentsInParsingQ);
                return DagMessagingHandler.blockInvokingNeeds(existedDescendentsInParsingQ)
                // set prerequisities null and attemps zero in order to force machine parsing them

            } else {
                // Machine doesn't know about block descendents, so asks network
                DagMessagingHandler.doInvokeDescendents({ lastWBLock, denayDoubleSendCheck });

            }
        }
    }

    static doInvokeDescendents(args) {
        clog.app.info(`do Invoke Descendents args ${utils.stringify(args)}`);
        let lastWBLock = args.lastWBLock;
        let denayDoubleSendCheck = _.has(args, 'denayDoubleSendCheck') ? args.denayDoubleSendCheck : false;
        let blockHash = lastWBLock.bHash;
        // if the last block which exists in DAG is older than 2 cycle time maybe efficient to call full-history
        if (lastWBLock.bCreationDate < utils.minutesBefore(2 * iConsts.getCycleByMinutes())) {
            let LastFullDAGDownloadResponse = kvHandler.getValueSync('LastFullDAGDownloadResponse');
            if (utils._nilEmptyFalse(LastFullDAGDownloadResponse)) {
                kvHandler.upsertKValueSync('LastFullDAGDownloadResponse', utils.minutesBefore(iConsts.getCycleByMinutes()));
            } else {
                if (utils.timeDiff(LastFullDAGDownloadResponse).asMinutes < 5) {
                    let msg = `less than 5 minutes ago invode for full DAG`;
                    clog.app.info(msg);
                    return { err: false, msg }
                }
            }


            // TODO: improve it to not send full req to all neighbors
            let { code, body } = require('../../messaging-protocol/dag/download-full-dag').invokeFullDAGDlRequest({
                startFrom: lastWBLock.bCreationDate
            });
            console.log('invoke Full DAG Dl Request', body);
            sendingQ.pushIntoSendingQ({
                sqType: iConsts.CONSTS.GQL,
                sqCode: code,
                sqPayload: body,
                sqTitle: `Invoke Full DAG blocks after(${lastWBLock.bCreationDate})`,
            });

        } else {
            clog.app.info(`invoking for descendents of ${utils.hash6c(blockHash)}`);
            let payload = {
                type: MESSAGE_TYPES.DAG_INVOKE_DESCENDENTS,
                mVer: "0.0.0",
                blockHash: blockHash,
            };
            payload = utils.stringify(payload)
            clog.cb.info('invoked for descendents');
            sendingQ.pushIntoSendingQ({
                denayDoubleSendCheck,
                sqType: MESSAGE_TYPES.DAG_INVOKE_DESCENDENTS,
                sqCode: blockHash,
                sqPayload: payload,
                sqTitle: `Invoke Descendents(${utils.hash6c(blockHash)})`,
            });
        }
        return { err: false, msg: 'Deacendents invoked' }
    }

    /**
     * the method (going back in history) analyzes block(s) prerequisities and maybe invoke them
     * @param {*} blockHash 
     * @param {*} level 
     */
    static blockInvokingNeeds(blockHashes, level = 7) {
        let msg;
        const dagHandler = require('../../dag/graph-handler/dag-handler');

        if (!Array.isArray(blockHashes))
            blockHashes = [blockHashes];

        let nextLevelBlockHashes = []
        let missedBlocks = [];
        for (let l = 0; l < level; l++) {
            // exists in DAG?
            let existedInDAG = dagHandler.searchInDAGSync({
                fields: ['b_hash'],
                query: [['b_hash', ['IN', blockHashes]]]
            });
            if (existedInDAG.length == blockHashes.length)
                continue; // all blocks are already recorded in local graph

            let arrayDiff = utils.arrayDiff(blockHashes, existedInDAG);

            // control if block exist in parsing_q
            for (let lookingHash of arrayDiff) {
                let existsInParsingQ = require('../../services/parsing-q-manager/parsing-q-handler').qUtils.searchParsingQSync({
                    fields: ['pq_code', 'pq_payload'],
                    query: [['pq_code', utils.hash6c(lookingHash)]]
                });
                if (existsInParsingQ.length == 0) {
                    missedBlocks.push(lookingHash);
                } else {
                    let ancestors = existsInParsingQ.map(x => JSON.parse(x.pqPayload).ancestors);
                    if (!Array.isArray(ancestors) || (ancestors.length == 0)) {
                        msg = `the block(${utils.hash16c(lookingHash)}) has no valid ancestors! ${utils.stringify(existsInParsingQ)}`;
                        console.log(msg);
                        clog.sec.error(msg);
                        return { err: true, msg }
                    }
                    ancestors.forEach(pckedAncestors => {
                        pckedAncestors.forEach(ancestor => {
                            nextLevelBlockHashes.push(ancestor);
                        });
                    });
                }
            }
            blockHashes = utils.arrayUnique(nextLevelBlockHashes);
        }
        missedBlocks = utils.arrayUnique(missedBlocks);
        missedBlocksHandler.addMissedBlocksToInvoke(missedBlocks);
        DagMessagingHandler.recursiveMissedBlocksInvoker();
        return { err: false }
    }

    static launchInvokeLeaves() {
        let shouldI = DagMessagingHandler.getMaybeAskForLatestBlocksFlag();

        // let machineReqStatus = kvHandler.serachSync({
        //     query: [
        //         ['kv_key', 'maybe_ask_for_latest_blocks']
        //     ]
        // })[0];
        // clog.cb.info(`machineReqStatus: ${JSON.stringify(machineReqStatus)}`);

        // let shouldI = _.has(machineReqStatus, 'kv_value') ? machineReqStatus.kv_value : iConsts.CONSTS.NO;
        clog.app.info(`-------- launchInvokeLeaves ----------------- shouldI  ${shouldI} ----------------------------------`);
        if (shouldI == iConsts.CONSTS.YES) {

            // TODO: needs control for latest invoke to not spaming network
            // let invokeAge = utils.timeDiff(machineReqStatus.kv_last_modified);
            // clog.app.info(`invokeAge: ${invokeAge.asMinutes} > invokeGap: ${iConsts.getInvokeLeavesGap()}`);
            // clog.cb.info(`invokeAge: ${invokeAge.asMinutes} > invokeGap: ${iConsts.getInvokeLeavesGap()}`);
            // if (invokeAge.asMinutes > iConsts.getInvokeLeavesGap())
            DagMessagingHandler.invokeLeaves();

            DagMessagingHandler.setMaybeAskForLatestBlocksFlag(iConsts.CONSTS.NO);
        }
    }

    static recursiveMissedBlocksInvoker() {
        let cycle = iutils.getCoinbaseCycleStamp();
        clog.app.info(`ReMiBcInv cycle(${cycle}) called recursiveMissedBlocksInvoker`);
        let missed = missedBlocksHandler.getMissedBlocksToInvoke(2);
        listener.doCallAsync('APSH_control_if_missed_block');
        setTimeout(DagMessagingHandler.recursiveMissedBlocksInvoker, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getBlockInvokeGap());

        if (missed.length > 0) {
            clog.app.info(`ReMiBcInv cycle(${cycle}) recursiveMissedBlocksInvoker has ${missed.length} missed blocks(${missed.map(x => utils.hash6c(x))})`);
            missed.forEach(aMissed => {
                //TODO: FIXME: because of nested including dag-msg-hander & parsing-q-handle which require each other I commented this lines
                //check if not already exist in parsing q
                let existsInParsingQ = require('../../services/parsing-q-manager/parsing-q-handler').qUtils.searchParsingQSync({
                    fields: ["pq_type", "pq_code"],
                    query: [['pq_code', utils.hash6c(aMissed)]]
                });
                if (existsInParsingQ.length == 0) {
                    DagMessagingHandler.invokeBlock(aMissed)
                    missedBlocksHandler.increaseAttempNumber(aMissed);
                }

            });
        }
    }

    static launchMissedBlocksInvoker() {
        // some preparations ...
        DagMessagingHandler.recursiveMissedBlocksInvoker();
    }

    static invokeBlock(blockHash) {
        console.log(`invoking for block(${utils.hash6c(blockHash)})`);
        clog.app.info(`invoking for block ${utils.hash6c(blockHash)}`);
        let payload = {
            type: MESSAGE_TYPES.DAG_INVOKE_BLOCK,
            mVer: "0.0.0",
            blockHash: blockHash,
        };
        payload = JSON.stringify(payload)
        clog.cb.info('invoked for keaves');
        sendingQ.pushIntoSendingQ({
            sqType: MESSAGE_TYPES.DAG_INVOKE_BLOCK,
            sqCode: blockHash,
            sqPayload: payload,
            sqTitle: `Invoke Block(${utils.hash6c(blockHash)})`,
        });
    }

    static handleBlockInvokeReq(args) {
        const dagHandler = require('../../dag/graph-handler/dag-handler');

        let msg;
        let blockHash = args.payload.blockHash;
        let short = utils.hash6c(blockHash)
        clog.app.info(`handleBlockInvokeReq ${short}`);

        let mVer = _.has(args.payload, 'mVer') ? args.payload.mVer : null;
        if (utils._nilEmptyFalse(mVer) || !iutils.isValidVersionNumber(mVer)) {
            msg = `missed mVer invoke block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        // retrieve block from DAG
        let regenBlock = dagHandler.regenerateBlock(blockHash)

        if (utils._nilEmptyFalse(regenBlock.block)) {
            // TODO: the block is valid and does not exist in local. or
            // invalid block invoked, maybe some penal for sender!
            // msg = `The block (${short}) invoked by ${args.sender} does not exist in local. `;
            // clog.sec.error(msg);
            regenBlock.msg += ` invoker ${args.sender}`
            regenBlock.shouldPurgeMessage = true
            regenBlock.err = true
            return regenBlock
        }

        let blockBody = regenBlock.block;

        clog.app.info(`Broadcasting Replay to invoke for block(${short}) type(${blockBody.bType})`);
        sendingQ.pushIntoSendingQ({
            sqType: blockBody.bType,
            sqCode: blockHash,
            sqPayload: utils.stringify(blockBody),
            sqTitle: `Replay to invoke for block(${short}) type(${blockBody.bType})`,
            sqReceivers: [args.sender]
        });
        return { err: false, shouldPurgeMessage: true }
    }

    static handleDescendentsInvokeReq(args) {
        const dagHandler = require('../../dag/graph-handler/dag-handler');
        let msg;
        let blockHash = args.payload.blockHash;
        let short = utils.hash6c(blockHash)
        clog.app.info(`handleDescendentsInvokeReq ${short}`);

        let mVer = _.has(args.payload, 'mVer') ? args.payload.mVer : null;
        if (utils._nilEmptyFalse(mVer) || !iutils.isValidVersionNumber(mVer)) {
            msg = `missed mVer Descend block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        // 1. retrieve descendent blocks from DAG by descendents property
        // 2. if step one hasn's answer tries to find descendents by ancestors link of blocks

        // 1. retrieve descendent blocks from DAG by descendents property
        let wBlock = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_descendents'],
            query: [['b_hash', blockHash]]
        });
        if (wBlock.length == 0) {
            // TODO: the block is valid and does not exist in local. or
            // invalid block invoked, maybe some penal for sender!
            msg = `The block (${short}) invoked (descendents) by ${args.sender} does not exist in local. (could be invalid request or not synched local machine)`;
            clog.sec.warn(msg);
            return { err: true, shouldPurgeMessage: true, msg: msg }
        }
        wBlock = wBlock[0];
        let descendents = wBlock.bDescendents;
        console.log('wBlock.bDescendents');
        console.log('descendents', descendents);
        console.log('wBlock.bDescendents');
        if (!utils._nilEmptyFalse(descendents)) {
            descendents = utils.parse(descendents);
            for (let hash of descendents) {
                let resWBlocks = dagHandler.searchInDAGSync({
                    fields: ['b_body'],
                    query: [['b_hash', hash]]
                });
                if (resWBlocks != null && resWBlocks.length > 0) {
                    let sendingWBLock = resWBlocks[0];
                    let block = blockUtils.openDBSafeObject(sendingWBLock.bBody).content;
                    if (iConsts.EXT_INFO_SUPPORTED.includes(block.bType)) {
                        // if (block.bType == iConsts.BLOCK_TYPES.Normal)
                        //     {
                        // block.bExtInfo = extinfosHandler.get BlockExtInfos(block.blockHash)
                        let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
                        if (bExtInfoRes.bExtInfo == null) {
                            msg = `missed bExtInfo8 (${utils.hash16c(block.blockHash)})`;
                            clog.sec.error(msg);
                            return { err: true, msg }
                        }
                        block.bExtInfo = bExtInfoRes.bExtInfo
                    }

                    sendingQ.pushIntoSendingQ({
                        sqType: block.bType,
                        sqCode: block.blockHash,
                        sqPayload: utils.stringify(block),
                        sqTitle: `Block body ${short} descendents`,
                        sqReceivers: [args.sender]
                    });
                }
            }

            return { err: false, shouldPurgeMessage: true }
        }

        // 2. if step one hasn's answer tries to find descendents by ncestors link of blocks
        // TODO: implement it ASAP

        return { err: true, shouldPurgeMessage: true }
    }

}

module.exports = DagMessagingHandler;
