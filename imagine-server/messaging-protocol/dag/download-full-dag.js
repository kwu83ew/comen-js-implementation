const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const GQLHandler = require('../../services/graphql/graphql-handler')
const dagHandler = require('../../dag/graph-handler/dag-handler');
const blockUtils = require('../../dag/block-utils');
const extinfosHandler = require('../../dag/extinfos-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const fileHandler = require('../../hard-copy/file-handler');

class FullDAGHandler {

    // TODO: implemtn a method to return only blocks blockHash & ancestors in order to make graph as fast as possible.
    // and later because of validating transaction needs, the node gradually downloads actual needed blocks (until reaching full node info)
    static getDAGHeaders(args) {

    }

    static async dlDAGBundle(args) {
        let msg;
        let query = [];
        let startFrom = iConsts.getLaunchDate();
        if (_.has(args, 'startFrom'))
            startFrom = args.startFrom;

        query.push(['b_creation_date', ['>=', startFrom]]);

        let wBlocks = await dagHandler.searchInDAGAsync({
            query,
            order: [['b_creation_date', 'ASC']]
        });
        let blocks = [];
        for (let aWBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(aWBlock.bBody).content;
            // adding bExtInfo 
            let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
            if (bExtInfoRes.bExtInfo != null) {
                block.bExtInfo = bExtInfoRes.bExtInfo
            } else {
                msg = `missed bExtInfo27 (${utils.hash16c(block.blockHash)})`;
                clog.app.error(msg);
            }
            blocks.push(block);
        }


        // also prepare the ballots received time
        let ballots = pollHandler.ballotHandler.searchInOnchainBallots({
            fields: ['ba_hash', 'ba_receive_date', 'ba_vote_r_diff']
        });
        let ballotsReceiveDates = {}
        for (let aBallot of ballots) {
            ballotsReceiveDates[aBallot.baHash] = {
                baReceiveDate: aBallot.baReceiveDate,
                baVoteRDiff: aBallot.baVoteRDiff,
            }
        }

        let DAGBundle = {
            bBVer: '0.0.0',
            blocks,
            ballots: ballotsReceiveDates
        }

        let res = fileHandler.packetFileCreateSync({
            dirName: iConsts.HD_PATHES.hd_backup_dag,
            fileName: `DAGBundle.txt`,
            fileContent: utils.stringify(DAGBundle)
        });
        return res;
    }

    static readDAGBundleIfExist() {
        let DAGBundle = null;
        try {
            DAGBundle = fileHandler.packetFileReadSync({
                fileName: 'DAGBundle.txt',
                appCloneId: 0,
                dirName: `tmp`,
            });
            return DAGBundle;

        } catch (e) {
            return { err: false, DAGBundle: null }
        }
    }

    static archiveDAGBundle() {
        fileHandler.fCopySync(`tmp/DAGBundle.txt`, `tmp/DAGBundle-${utils.getNowSSS()}.txt`);
        fileHandler.deleteFileSync({
            dirName: 'tmp',
            fileName: 'DAGBundle.txt',
            appCloneId: 0
        });
    }

    static invokeFullDAGDlRequest(args) {
        return GQLHandler.makeAPacket({
            cards: [
                {
                    cdType: GQLHandler.cardTypes.FullDAGDownloadRequest,
                    cdVer: '0.0.1',
                    startFrom: args.startFrom,
                }
            ]
        });
    }

    static prepareFullDAGDlResponse(args) {
        let msg;
        console.log(`prepare Full DAG Dl Response: ${utils.stringify(args)}`);
        clog.app.info(`prepare Full DAG Dl Response: ${utils.stringify(args)}`);

        /**
         * TODO: implement sub system to chunk data and send step by step
         * and also not dedicate entire machine to reponse the requests
         */
        let query = [];
        let startFrom = iConsts.getLaunchDate();
        if (_.has(args, 'payload') && _.has(args.payload, 'startFrom'))
            startFrom = args.payload.startFrom;

        query.push(['b_creation_date', ['>=', startFrom]]);

        let wBlocks = dagHandler.searchInDAGSync({
            query,
            order: [['b_creation_date', 'ASC']]
        });
        let cards = [];
        for (let aWBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(aWBlock.bBody).content;
            // adding bExtInfo 
            let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
            if (bExtInfoRes.bExtInfo != null) {
                block.bExtInfo = bExtInfoRes.bExtInfo
            } else {
                msg = `missed bExtInfo14 (${utils.hash16c(block.blockHash)})`;
                clog.app.error(msg);
            }

            let aCard = {
                cdType: GQLHandler.cardTypes.FullDAGDownloadResponse,
                cdVer: '0.0.1',
                block,
            };
            // console.log('aCard from full DAG: ', aCard);
            cards.push(aCard);
            if (utils.stringify(cards).length > iConsts.MAX_FullDAGDownloadResponse_LENGTH_BY_CHAR) {
                msg = `MAX_FullDAGDownloadResponse_LENGTH_BY_CHAR reached the limit `
                msg += `${utils.sepNum(iConsts.MAX_FullDAGDownloadResponse_LENGTH_BY_CHAR)} char, < ${utils.sepNum(utils.stringify(cards).length)} Chars for ${cards.length} cards `;
                msg += `containing the blocks created between ${startFrom} and ${block.creationDate}`;
                console.log(msg);
                clog.app.info(msg);
                break;
            }
        }


        // also prepare the ballots received time
        // TODO: investigate to send ballots chuck by chunk (like blocks)
        let ballots = pollHandler.ballotHandler.searchInOnchainBallots({
            fields: ['ba_hash', 'ba_receive_date', 'ba_vote_r_diff']
        });
        let ballotsReceiveDates = {}
        for (let aBallot of ballots) {
            ballotsReceiveDates[aBallot.baHash] = {
                baReceiveDate: aBallot.baReceiveDate,
                baVoteRDiff: aBallot.baVoteRDiff,
            }
        }
        let aCard = {
            cdType: GQLHandler.cardTypes.BallotsReceiveDates,
            cdVer: '0.0.1',
            ballotsReceiveDates
        };
        console.log('aCard (BallotsReceiveDates) from full DAG: ', aCard);
        clog.app.info(`aCard (BallotsReceiveDates) from full DAG: ${utils.stringify(aCard)}`);
        cards.push(aCard);


        let { code, body } = GQLHandler.makeAPacket({ cards });

        console.log(`goint to insert Full DAG Dl Response: ${code} ${cards.length} cards`);
        console.log(`goint to insert Full DAG Dl utils.stringify(body).length: ${code} ${utils.stringify(body).length} cards`);
        clog.app.info(`goint to insert Full DAG Dl Response: ${code} ${cards.length} cards`);
        let pushRes = sendingQ.pushIntoSendingQ({
            sqType: iConsts.CONSTS.GQL,
            sqCode: code,
            sqPayload: body,
            sqReceivers: [args.sender],
            sqTitle: `GQL Full-DAG-blocks packet(${utils.hash6c(code)})`,
        });
        if (pushRes.err != false) {
            return pushRes;
        }

        return { err: false, msg: `GQL Full-DAG (${wBlocks.length} blocks) packet(${utils.hash6c(code)}) pushed to Q`, shouldPurgeMessage: true }

    }
}

module.exports = FullDAGHandler;



