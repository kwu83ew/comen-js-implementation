const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const clog = require('../loggers/console_logger');
const MESSAGE_TYPES = require('./message-types')
const dagHandler = require('../dag/graph-handler/dag-handler');
const dagMsgHandler = require('./dag/dag-msg-handler');
const missedBlocksHandler = require('../dag/missed-blocks-handler');
const parsingQHandler = require('../services/parsing-q-manager/parsing-q-handler');
const greetingParsers = require('./greeting/greeting-parsers');
const GQLHandler = require('../services/graphql/graphql-handler')
const kvHandler = require('../models/kvalue');

function dispatchMessageSync(args) {
    let msg;
    let sender = _.has(args, 'sender') ? args.sender : null;
    let message = _.has(args, 'message') ? args.message : null;
    let connectionType = _.has(args, 'connectionType') ? args.connectionType : iConsts.CONSTS.PUBLIC;

    if (utils._nilEmptyFalse(sender) || utils._nilEmptyFalse(message))
        return { err: true, msg: 'no sender or empty message to dispach' }

    let creationDate = _.has(message, 'creationDate') ? message.creationDate : '-';


    /**
     * pType (Packet type) is more recent than old one bType(Block type) which was created to support block exchanging, wherease pType is proposed to support packet exchange
     * which is more comprehensive(and expanded concept) than block.
     * each package can contain one or more blocks and misc requests
     * it is a-kind-of graphGL implementation.
     * each packet contains one or more cards, and each card represents a single query or single query result
     */
    let pType = _.has(message, 'pType') ? message.pType : null;
    if (!utils._nilEmptyFalse(pType)) {
        // it is a graphQL packet which includes some cards, each card represents a block or a page of differnt comunication 
        let pVer = _.has(message, 'pVer') ? message.pVer : null;
        if (utils._nilEmptyFalse(pVer))
            return { err: true, msg: 'no msg pVer stated' }

        let err = null;
        let shouldPurgeMessage = false;
        for (let aCard of message.cards) {
            let gqlRes = innerDispatchMessageSync({
                bVer: aCard.cdVer,
                sender,
                connectionType,
                creationDate,
                type: aCard.cdType,
                message: aCard
            });
            console.log('gqlRes', gqlRes);
            if (gqlRes.err != false) {
                err = true;
                msg += gqlRes.msg;
            }
            shouldPurgeMessage |= gqlRes.shouldPurgeMessage;
        }
        return { err, msg, shouldPurgeMessage }

    } else {
        // it is old packet style which is one block per packet
        let type = _.has(message, 'bType') ? message.bType : null;
        if (type == null)
            type = _.has(message, 'type') ? message.type : null;
        if (utils._nilEmptyFalse(type))
            return { err: true, msg: 'no msg type stated' }

        let bVer = _.has(message, 'bVer') ? message.bVer : null;
        let mVer = _.has(message, 'mVer') ? message.mVer : null;    // very very old one TODO: get rid of it ASAP
        if (utils._nilEmptyFalse(bVer) && utils._nilEmptyFalse(mVer))
            return { err: true, msg: 'no msg bVer/mVer stated' }

        return innerDispatchMessageSync({
            mVer,
            bVer,
            sender,
            connectionType,
            creationDate,
            type,
            message
        })
    }
}

function innerDispatchMessageSync(args) {
    let msg, block;

    let creationDate = args.creationDate;
    let type = args.type;
    let sender = args.sender;
    let message = args.message;
    let connectionType = args.connectionType;

    clog.app.info('@@@@@ innerDispatchMessageSync @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
    clog.app.info(`--- dispatching(${type}) message from(${sender})`);
    let dspchRes, code, alreadyRecordedInDAG;

    // FIXME: security issue. what happend if adversary creates million of blocks in minute and send the final descendente?
    // in this case all nodes have to download entire blocks all the way back to find ancestor 
    // and start to validate from the oldest one and add it to DAG(if is VALID)
    // in this process nodes can not control if the blocks in between are valid or not? 
    // so the bandwidth & machine harddisk will be vasted
    // and network will be blocked!
    // here we need implement a system to control creation date of eache received block(profiled for each neighbor or backer address)
    // and limit creating block(e.g 10 bloocks per minute)
    switch (type) {

        case iConsts.BLOCK_TYPES.Normal:
        case iConsts.BLOCK_TYPES.Coinbase:
        case iConsts.BLOCK_TYPES.FSign:
        case iConsts.BLOCK_TYPES.FVote:
        case iConsts.BLOCK_TYPES.POW:
        case iConsts.BLOCK_TYPES.RpBlock:
            // push to table i_parsing_q 
            block = message;
            code = utils.hash6c(block.blockHash);
            if (!iutils.isValidVersionNumber(block.bVer)) {
                msg = `invalid bVer(${block.bVer}) for block(${code}) in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            clog.app.info(`--- pushing block(${code}) type(${type}) from(${sender}) to "i_parsing_q"`);
            alreadyRecordedInDAG = dagHandler.searchInDAGSync({
                query: [
                    ['b_hash', block.blockHash]
                ]
            });
            if (alreadyRecordedInDAG.length > 0) {
                clog.app.info(`Duplicated packet received ${type}-${code}`);
                dspchRes = { err: false, shouldPurgeMessage: true }
            } else {

                dspchRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                    creationDate: block.creationDate,
                    type,
                    code: block.blockHash,
                    sender,
                    message,
                    connectionType
                });

                // if it was a valid block
                if (dspchRes.err == false)
                    dagMsgHandler.setLastReceivedBlockTimestamp(type, block.blockHash)
            }
            // remove from missed blocks (if exist)
            missedBlocksHandler.removeFromMissedBlocks(block.blockHash)
            break;



        // GQL communications
        case GQLHandler.cardTypes.ProposalLoanRequest:  // proposal loan request
        case GQLHandler.cardTypes.FullDAGDownloadRequest:   // request for download full DG history
        case GQLHandler.cardTypes.pleaseRemoveMeFromYourNeighbors:   // request for removing email
            let hash = iutils.doHashObject(message);
            code = utils.hash6c(hash);
            if (!iutils.isValidVersionNumber(message.cdVer)) {
                msg = `invalid cdVer for GQL(${code}) in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            clog.app.info(`@@@@@@@@@@@@@@@@@@@@@@@@@ ${type} @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`);
            clog.app.info(JSON.stringify(args));
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            dspchRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                creationDate,
                type,
                code: hash,
                sender,
                message,
                connectionType
            });
            break;



        case GQLHandler.cardTypes.FullDAGDownloadResponse: // recceived a block via QGL, BTW it must be inserted in Q to be parsed later
            // console.log(`DAGBlock message: ${utils.stringify(message)}`);
            // clog.app.info(`DAGBlock message: ${utils.stringify(message)}`);
            block = message.block;
            if (!iutils.isValidVersionNumber(message.cdVer)) {
                msg = `invalid cdVer for GQL(${block.blockHash}) in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // update flag LastFullDAGDownloadResponse
            kvHandler.upsertKValueSync('LastFullDAGDownloadResponse', utils.getNow());
            // push to table i_parsing_q 
            clog.app.info(`--- GQL: pushing to "i_parsing_q" block(${block.blockHash}) type(${type}) from(${sender})`);

            // control if already exist in DAG
            alreadyRecordedInDAG = dagHandler.searchInDAGSync({
                query: [
                    ['b_hash', block.blockHash]
                ]
            });
            if (alreadyRecordedInDAG.length > 0) {
                clog.app.info(`Duplicated packet received ${type}-${block.blockHash}`);
                dspchRes = { err: false, shouldPurgeMessage: true }

            } else {
                dspchRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                    creationDate: block.creationDate,
                    type: block.bType,
                    code: block.blockHash,
                    sender,
                    message: block,
                    connectionType
                });

                // if it was a valid message
                if (dspchRes.err == false)
                    dagMsgHandler.setLastReceivedBlockTimestamp(block.bType, block.blockHash)
            }
            break;

        case GQLHandler.cardTypes.BallotsReceiveDates: // recceived all ballotes received date via QGL
            clog.app.info(`Ballots Receive date message: ${utils.stringify(message.ballotsReceiveDates)}`);
            if (!iutils.isValidVersionNumber(message.cdVer)) {
                msg = `invalid cdVer for GQL(${block.blockHash}) Ballots Receive Dates in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            try {
                // normalizing/sanitize Ballots Receive Dates and upsert into kv
                let sanBallots = {};
                for (let aBlt of utils.objKeys(message.ballotsReceiveDates)) {
                    sanBallots[utils.stripNonAlphaNumeric(aBlt)] = {
                        baReceiveDate: utils.stripNonInDateString(message.ballotsReceiveDates[aBlt].baReceiveDate.toString()),
                        baVoteRDiff: utils.stripNonNumerics(message.ballotsReceiveDates[aBlt].baVoteRDiff.toString()),
                    }
                }
                kvHandler.upsertKValueSync('ballotsReceiveDates', utils.stringify(sanBallots));
                dspchRes = { err: false, shouldPurgeMessage: true }
            } catch (e) {
                clog.sec.error(e);
                return { err: true, msg: e }
            }
            break;

        case GQLHandler.cardTypes.NodeStatusScreenshot: // recceived an screenshot of neighbor's Machine status
            console.log(`screenshott message: ${message.creationDate}-${message.sender}`);
            clog.app.info(`screenshott message: ${message.creationDate}-${message.sender}`);
            // "cdType":"NodeStatusScreenshot","cdVer":"0.0.1","creationDate":"2020-03-08 13:34:10","":"abc@def.gh",

            if (!iutils.isValidVersionNumber(message.cdVer)) {
                msg = `invalid cdVer for GQL(${block.blockHash}) Node Status Screenshot in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            const nodeScHandler = require('../services/node-screen-shot/node-screen-shot-handler');
            let saveRes = nodeScHandler.pushReportToDB({
                scOwner: `${message.sender}:${message.creationDate}`,
                content: message.finalReport
            });
            saveRes.shouldPurgeMessage = true;
            return saveRes;
            break;

        case GQLHandler.cardTypes.directMsgToNeighbor: // recceived an screenshot of neighbor's Machine status
            console.log(`direct Msg To Neighbor: ${utils.stringify(message)}`);
            clog.app.info(`direct Msg To Neighbor: ${message.creationDate}-${message.sender}`);
            // "cdType":"NodeStatusScreenshot","cdVer":"0.0.1","creationDate":"2020-03-08 13:34:10","":"abc@def.gh",

            if (!iutils.isValidVersionNumber(message.cdVer)) {
                msg = `invalid cdVer for GQL(${block.blockHash}) direct Msg To Neighbor in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            const directmsgHandler = require('./direct-msg-handler');
            let msgRes = directmsgHandler.recordReceivedMsg({
                sender: message.sender,
                receiver: message.receiver,
                directMsgBody: message.directMsgBody
            });
            return msgRes;
            break;



        // DAG comunications
        case MESSAGE_TYPES.DAG_INVOKE_BLOCK:
            code = utils.hash6c(message.blockHash);
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MESSAGE_TYPES.DAG_INVOKE_BLOCK @@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            clog.app.info(JSON.stringify(args));
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            dspchRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                creationDate,
                type,
                code: message.blockHash,
                sender,
                message,
                connectionType
            });
            break;

        case MESSAGE_TYPES.DAG_INVOKE_DESCENDENTS:
            code = utils.hash6c(message.blockHash);
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MESSAGE_TYPES.DAG_INVOKE_DESCENDENTS @@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            clog.app.info(JSON.stringify(args));
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            dspchRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                creationDate,
                type,
                code: message.blockHash,
                sender,
                message,
                connectionType
            });
            break;

        case MESSAGE_TYPES.DAG_INVOKE_LEAVES:
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for  in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MESSAGE_TYPES.DAG_INVOKE_LEAVES @@@@@@@@@@@@@@@@@@@@@@@@@@@');
            clog.app.info(`@@@@@@@@@@@@@@@@@@@@ sender: ${sender} @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`);
            dspchRes = require("./dag/dag-msg-handler").extractLeavesAndPushInSendingQ({
                sq_type: type,
                sq_code: utils.getNow(),
                sender,
                connectionType
            });
            break;

        case MESSAGE_TYPES.DAG_LEAVES_INFO:
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for  in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            dagMsgHandler.handleReceivedLeaveInfo(message.leaves)
            dspchRes = { err: false, shouldPurgeMessage: true }
            break;


        // handshake 
        case MESSAGE_TYPES.HANDSHAKE:
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for  in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // TODO: implement a switch to set off/on for no more new neighbor
            clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MESSAGE_TYPES.HANDSHAKE @@@@@@@@@@@@@@@@@@@@@@@@@@@');
            dspchRes = greetingParsers.parseHandshake({
                sender,
                message,
                connectionType
            });
            clog.app.info(`greetingParsers.parseHandshake dspchRes: ${utils.stringify(dspchRes)} `);
            setTimeout(() => {
                const dagMsgHandler = require('./dag/dag-msg-handler');
                dagMsgHandler.invokeDescendents();
            }, 50000);
            break;

        case MESSAGE_TYPES.NICETOMEETYOU:
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for  in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            dspchRes = greetingParsers.parseNiceToMeetYou({
                sender,
                message,
                connectionType
            })
            setTimeout(() => {
                const dagMsgHandler = require('./dag/dag-msg-handler');
                dagMsgHandler.invokeDescendents();
            }, 50000);
            break;

        case MESSAGE_TYPES.HEREISNEWNEIGHBOR:
            if (!iutils.isValidVersionNumber(args.mVer)) {
                msg = `invalid mVer for in dispatcher! ${type}`
                clog.sec.error(msg);
                return { err: true, msg }
            }
            dspchRes = greetingParsers.parseHereIsNewNeighbor({
                sender,
                message,
                connectionType
            })
            break;

        case iConsts.BLOCK_TYPES.Genesis:
            dspchRes = { err: false, shouldPurgeMessage: true }
            break;

        default:
            code = _.has(message, 'blockHash') ? utils.hash6c(message.blockHash) : '';
            msg = `Unknown Message type(${type}) received from ${sender} HD in inbox ${code} `
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true }


    }

    return dspchRes;

}





module.exports = dispatchMessageSync
