const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const clog = require('../loggers/console_logger');
const model = require('../models/interface');
const sendingQ = require('../services/sending-q-manager/sending-q-manager');
const GQLHandler = require('../services/graphql/graphql-handler');
const sendingQManager = require('../services/sending-q-manager/sending-q-manager');
const machine = require('../machine/machine-handler');


const table = 'i_machine_direct_messages';

class DirectMsgHandler {

    static sendAMsg(args) {
        let sender = machine.getMProfileSettingsSync().pubEmail.address;
        let receiver = args.receiver;
        let card = {
            cdType: GQLHandler.cardTypes.directMsgToNeighbor,
            cdVer: '0.0.1',
            sender,
            receiver,
            directMsgBody: args.directMsgBody,
        }
        let { code, body } = GQLHandler.makeAPacket({
            cards: [card]
        });
        let pushRes = sendingQ.pushIntoSendingQ({
            sqType: iConsts.CONSTS.GQL,
            sqCode: code,
            sqPayload: body,
            sqReceivers: [receiver],
            sqTitle: `Direct Msg from(${sender})`,
        });
        setTimeout(() => {
            // try to push msg ASAP
            sendingQManager.sendOutThePacket();
        }, 1000);
        setTimeout(() => {
            // try to push msg ASAP
            sendingQManager.sendOutThePacket();
        }, 3000);
        setTimeout(() => {
            // try to push msg ASAP
            sendingQManager.sendOutThePacket();
        }, 7000);
        return pushRes;
    }

    static recordReceivedMsg(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let sender = args.sender;
        let receiver = args.receiver;
        let directMsgBody = args.directMsgBody;

        let values = {
            dm_mp_code: mpCode,
            dm_sender: sender,
            dm_receiver: receiver,
            dm_message: directMsgBody,
            dm_creation_date: utils.getNow()
        }
        model.sCreate({
            table,
            values
        })
        return {
            err: false,
            shouldPurgeMessage: true
        };
    }

    static async deleteDirectMsg(args) {
        console.log(`deleteDirectMsg args ${utils.stringify(args)}`);
        await model.aDelete({
            table,
            query: [['dm_id', args.dmId]]

        });
        return { err: false }
    }

    static async getMyDirectMsgs() {
        let records = await model.aRead({
            table,
            order: [['dm_id', 'ASC']]
        });
        return { err: false, records }
    }

    static async refreshMyDirectMsgs() {
        // try to some fetching and pasrdin eventually existed messages
        const networkListener = require('../network-adapter/network-listener');
        const parsingQHandler = require('../services/parsing-q-manager/parsing-q-handler');

        setTimeout(() => {
            // try to push msg ASAP
            if (iConsts.EMAIL_IS_ACTIVE) {
                networkListener.fetchPrvEmailAndWriteOnHardDisk();
                networkListener.fetchPubEmailAndWriteOnHardDisk();
            }
            networkListener.do_reading_harddisk();
            parsingQHandler.qPicker.smartPullQSync();

        }, 1000);
        setTimeout(() => {
            // try to push msg ASAP
            if (iConsts.EMAIL_IS_ACTIVE) {
                networkListener.fetchPrvEmailAndWriteOnHardDisk();
                networkListener.fetchPubEmailAndWriteOnHardDisk();
            }
            networkListener.do_reading_harddisk();
            parsingQHandler.qPicker.smartPullQSync();

        }, 3000);
        setTimeout(() => {
            // try to push msg ASAP
            if (iConsts.EMAIL_IS_ACTIVE) {
                networkListener.fetchPrvEmailAndWriteOnHardDisk();
                networkListener.fetchPubEmailAndWriteOnHardDisk();
            }
            networkListener.do_reading_harddisk();
            parsingQHandler.qPicker.smartPullQSync();

        }, 7000);
        return { err: false }
    }

}

module.exports = DirectMsgHandler;
