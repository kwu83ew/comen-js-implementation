const _ = require('lodash');
var LZUTF8 = require('lzutf8');
const iConsts = require('../config/constants');
const iutils = require('../utils/iutils');
const utils = require('../utils/utils');
const clog = require('../loggers/console_logger');
const docBufferHandler = require('../services/buffer/buffer-handler');
const DNAHandler = require('../dna/dna-handler');
const merkleHandler = require('../crypto/merkle/merkle');

class blockUtils {

    static compressString(args) {
        let content = args.content;
        let doSanitize = _.has(args, 'doSanitize') ? args.doSanitize : true;
        let doUTF8Encode = _.has(args, 'doUTF8Encode') ? args.doUTF8Encode : true;

        if (doSanitize)
            content = utils.sanitizingContent(content);

        if (doUTF8Encode)
            content = utils.encode_utf8(content);

        content = LZUTF8.compress(content, { outputEncoding: 'Base64' });
        return content
    }

    static decompressString(content) {
        try {
            content = LZUTF8.decompress(content, { inputEncoding: "Base64" });
            content = utils.decode_utf8(content)
            return { err: false, content }
        } catch (e) {
            return { err: true, msg: utils.stringify(e) }
        }
    }

    static normalizeAncestors(ancestors) {
        if (!Array.isArray(ancestors))
            ancestors = [ancestors];

        let normalAncs = [];
        for (let anAnc of ancestors) {
            if (!utils._nilEmptyFalse(anAnc) &&
                ((typeof anAnc === 'string' || anAnc instanceof String)) &&
                (anAnc.length == 64))
                normalAncs.push(anAnc);
        }
        normalAncs = normalAncs.sort();
        return normalAncs;
    }


    static ifAncestorsAreValid(ancestors) {
        if (utils._nilEmptyFalse(ancestors))
            return false;

        for (let anAnc of ancestors) {
            if (utils._nilEmptyFalse(anAnc) ||
                (!(typeof anAnc === 'string' || anAnc instanceof String)) ||
                (anAnc.length != 64))
                return false;
        }
        return true;
    }

    static wrapSafeObjectForDB(args) {
        /**
         * to make a safe string to insert in db, jus convert it to base64
         */

        args.content = utils.stringify(args.obj);
        delete args.obj;
        return blockUtils.wrapSafeContentForDB(args);
    }

    static wrapSafeContentForDB(args) {
        /**
         * to make a safe string to insert in db, jus convert it to base64
         */
        let content = args.content;
        let sfVer = _.has(args, 'sfVer') ? args.sfVer : '0.0.0';
        if (sfVer == '0.0.0') {
            let b = Buffer.alloc(content.length, content);
            content = b.toString('base64');
            content = {
                sfVer,
                content
            }
            content = utils.stringify(content);
            return content;
        } else {
            clog.app.error(`unknown sfVer! ${content}`);
            return { err: true }
        }
    }

    static openDBSafeObject(obj) {
        let res = blockUtils.openDBSafeContent(obj);
        if (res.err != false)
            return res;

        res.content = utils.parse(res.content);
        return res;
    }

    static openDBSafeContent(content) {
        /**
         * to make a safe string to insert in db, jus convert it to base64
         */
        try {
            let args = utils.parse(content);
            content = args.content;
            let sfVer = args.sfVer;

            if (sfVer == '0.0.0') {
                // decode base 64
                let buffer = Buffer.from(content, "base64");
                content = buffer.toString("utf8");

            } else {
                clog.app.error(`unknown sfVer! ${content}`);
                return { err: true }
            }
            return { err: false, content };

        } catch (e) {
            console.log(utils.stringify(e));
            clog.sec.error(utils.stringify(e));
            return { err: true, msg: e }
        }
    }

    static verifyDocumentMerkleProof(args) {
        clog.app.info(`verify DocumentMerkleProof args ${utils.stringify(args)}`);
        // let block = args.block;
        let docsRootHash = args.docsRootHash;
        let docHash = args.docHash;
        let proofs = args.proofs;
        let prove = merkleHandler.getRootByAProve(docHash, proofs.hashes, proofs.lHash);
        clog.app.info(`verify DocumentMerkleProof prove ${utils.stringify(prove)}`);
        return (docsRootHash == prove);
    }

    static getDocumentMerkleProof(args) {
        clog.app.info(`get DocumentMerkleProof args ${utils.stringify(args)}`);
        let block = args.block;
        let docHash = args.docHash;
        let docHashes = block.docs.map(x => x.hash);
        let mRes = merkleHandler.merkle(docHashes);
        clog.app.info(`get DocumentMerkleProof mRes ${utils.stringify(mRes)}`);
        return mRes.proofs[docHash];
    }


    /**
     * picks a doc from a block which is complatly loaded in memory
     * @param {*} block 
     * @param {*} docHash 
     */
    static pickDocByHash(block, docHash) {
        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let aDoc = block.docs[docInx];
            if (aDoc.hash == docHash)
                return { doc: aDoc, docInx };
        }
        return null;
    }
    /**
     * 
     * @param {*} args 
     * do a groupping and some general validations on entire documents of a Block
     * TODO: maybe enhance it to use memory buffer
     */
    static groupDocsOfBlock(args) {
        let msg;
        let block = args.block;
        let stage = args.stage;
        let grpdDocuments = {};
        let docIndexByHash = {};
        let docByHash = {};

        let trxDict = {};
        let mapTrxHashToTrxRef = {};
        let mapTrxRefToTrxHash = {};
        let mapReferencerToReferenced = {};
        let mapReferencedToReferencer = {};

        if (!_.has(block, 'docs') || !Array.isArray(block.docs) || (block.docs.length == 0))
            return {
                docByHash, grpdDocuments, docIndexByHash, trxDict,
                mapTrxRefToTrxHash, mapTrxHashToTrxRef,
                mapReferencerToReferenced, mapReferencedToReferencer,
                err: true
            };


        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let aDoc = block.docs[docInx];
            docByHash[aDoc.hash] = aDoc;

            if ((aDoc.creationDate > block.creationDate) || (aDoc.creationDate > utils.getNow())) {
                msg = `${stage}, block(${utils.hash6c(block.blockHash)}) has document with creationdate after block-creationDate!`;
                return { err: true, msg }
            }
            docIndexByHash[aDoc.hash] = docInx;

            if (!iutils.isValidVersionNumber(aDoc.dVer)) {
                msg = `invalid dVer for ${utils.hash6c(aDoc.hash)}) in group Docs Of Block`
                clog.sec.error(msg);
                return { err: true, msg }
            }

            // length control
            let tmpDoc = _.clone(aDoc);
            if (!_.has(tmpDoc, 'dExtInfo'))
                tmpDoc.dExtInfo = block.bExtInfo[docInx];
            if ((tmpDoc.dType != iConsts.DOC_TYPES.DPCostPay) && (parseInt(tmpDoc.dLen) != utils.stringify(tmpDoc).length)) {
                msg = `${stage},  The doc(${tmpDoc.dType} / ${utils.hash6c(tmpDoc.hash)}) stated dLen(${tmpDoc.dLen}), `
                msg += `I. is not same as real length(${utils.stringify(tmpDoc).length})!`
                clog.sec.error(msg);
                return { err: true, msg }
            }


            if (!_.has(grpdDocuments, aDoc.dType))
                grpdDocuments[aDoc.dType] = [];
            grpdDocuments[aDoc.dType].push(aDoc);

            if (_.has(aDoc, 'ref') && !utils._nilEmptyFalse(aDoc.ref)) {
                if (iutils.canBeACostPayerDoc(aDoc.dType)) {
                    trxDict[aDoc.hash] = aDoc;
                    mapTrxHashToTrxRef[aDoc.hash] = aDoc.ref;
                    mapTrxRefToTrxHash[aDoc.ref] = aDoc.hash;
                } else {
                    mapReferencerToReferenced[aDoc.hash] = aDoc.ref;
                    mapReferencedToReferencer[aDoc.ref] = aDoc.hash;
                }
            }
        }

        let payedRefs1 = utils.objKeys(mapTrxRefToTrxHash);
        let payedRefs2 = utils.objKeys(mapTrxHashToTrxRef).map(x => mapTrxHashToTrxRef[x]);
        for (let aDoc of block.docs) {
            if (!iutils.isNoNeedCostPayerDoc(aDoc.dType)) {
                // there must be a transaction to pay for this document
                if (!payedRefs1.includes(aDoc.hash) || !payedRefs2.includes(aDoc.hash)) {
                    if ((aDoc.dType == iConsts.DOC_TYPES.CPost) && (aDoc.dClass == iConsts.CPOST_CLASSES.DMS_Post)) {
                        if (!_.has(aDoc, 'nonce') || utils._nilEmptyFalse(aDoc.nonce) || (block.creationDate > '2021-01-01 00:00:00')) {
                            msg = `${stage},  The document(${utils.hash6c(aDoc.hash)}) DMS_Post has not Nonce & not payed by no transaction`;
                            clog.sec.error(msg);
                            return { err: true, msg }
                        }
                    } else {
                        msg = `${stage}, The document(${utils.hash6c(aDoc.hash)}) is not payed by no transaction`;
                        clog.sec.error(msg);
                        return { err: true, msg }
                    }
                }
            }
        }

        if (utils.objKeys(mapTrxRefToTrxHash).length != utils.objKeys(mapTrxHashToTrxRef).length) {
            msg = `${stage}, transaction count and ref count are different! mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)} mapTrxHashToTrxRef: ${utils.stringify(mapTrxHashToTrxRef)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        for (let aRef of utils.objKeys(mapTrxRefToTrxHash)) {
            if (!_.has(trxDict, mapTrxRefToTrxHash[aRef])) {
                msg = `${stage}, missed some1 transaction to support referenced documents. trxDict:${utils.stringify(trxDict)} mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)}`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }
        if (utils.arrayDiff(utils.objKeys(mapTrxHashToTrxRef), utils.objKeys(trxDict)) != 0) {
            msg = `${stage}, missed some transaction, to support referenced documents. trxDict:${utils.stringify(trxDict)} mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        let docsHashes = utils.objKeys(docByHash);
        for (let aRef of utils.objKeys(mapTrxRefToTrxHash)) {
            if (!docsHashes.includes(aRef)) {
                msg = `${stage}, missed referenced document, which is supported by trx.ref(${utils.hash6c(aRef)})`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }

        if (utils.objKeys(docIndexByHash).length != block.docs.length) {
            msg = `${stage}, There is duplicated doc.hash in block(${utils.hash6c(block.blockHash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        return {
            trxDict, docByHash, grpdDocuments, docIndexByHash,
            mapTrxRefToTrxHash, mapTrxHashToTrxRef,
            mapReferencerToReferenced, mapReferencedToReferencer,
            err: false
        };
    }

    static groupDocsOfPOWBlock(args) {
        let msg;
        let block = args.block;
        let stage = args.stage;
        let grpdDocuments = {};
        let docIndexByHash = {};
        let docByHash = {};


        if (!_.has(block, 'docs') || !Array.isArray(block.docs) || (block.docs.length == 0))
            return { docByHash, grpdDocuments, docIndexByHash, err: true };


        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let aDoc = block.docs[docInx];
            docByHash[aDoc.hash] = aDoc;

            if ((aDoc.creationDate > block.creationDate) || (aDoc.creationDate > utils.getNow())) {
                msg = `${stage}, block(${utils.hash6c(block.blockHash)}) has document with creationdate after block-creationDate!`;
                return { err: true, msg }
            }
            docIndexByHash[aDoc.hash] = docInx;

            if (!iutils.isValidVersionNumber(aDoc.dVer)) {
                msg = `invalid dVer for ${utils.hash6c(aDoc.hash)}) in group Docs Of POW Block`
                clog.sec.error(msg);
                return { err: true, msg }
            }

            // length control
            let tmpDoc = _.clone(aDoc);
            if (!_.has(tmpDoc, 'dExtInfo'))
                tmpDoc.dExtInfo = block.bExtInfo[docInx];
            if ((tmpDoc.dType != iConsts.DOC_TYPES.DPCostPay) && (parseInt(tmpDoc.dLen) != utils.stringify(tmpDoc).length)) {
                msg = `${stage}, POW The doc(${tmpDoc.dType} / ${utils.hash6c(tmpDoc.hash)}) stated dLen(${tmpDoc.dLen}), II. is not same as real length(${utils.stringify(tmpDoc).length})!`
                clog.sec.error(msg);
                return { err: true, msg }
            }

            if (!_.has(grpdDocuments, aDoc.dType))
                grpdDocuments[aDoc.dType] = [];
            grpdDocuments[aDoc.dType].push(aDoc);
        }


        if (utils.objKeys(docIndexByHash).length != block.docs.length) {
            msg = `${stage}, There is duplicated doc.hash in block(${utils.hash6c(block.blockHash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        return { docByHash, grpdDocuments, docIndexByHash, err: false };
    }



    static retrieveAndGroupBufferedDocuments(args = {}) {
        let msg;

        let blockCreationDate = args.blockCreationDate;

        let grpdDocuments = {};
        let docByHash = {};
        let docIndexByHash = {};

        let trxDict = {};
        let mapTrxHashToTrxRef = {};
        let mapTrxRefToTrxHash = {};
        let mapReferencerToReferenced = {};
        let mapReferencedToReferencer = {};

        if (!utils.isValidDateForamt(blockCreationDate)) {
            msg = `creating invalid block-creationDate(${blockCreationDate})!`;
            return { err: true, msg }
        }

        let bufferedDocs = docBufferHandler.searchBufferedDocsSync({
            order: [
                ['bd_dp_cost', 'DESC'],
                ['bd_doc_class', 'ASC'],
                ['bd_insert_date', 'ASC']
            ]
        });
        if (bufferedDocs.length == 0)
            return {
                docByHash, grpdDocuments, docIndexByHash, trxDict,
                mapTrxRefToTrxHash, mapTrxHashToTrxRef,
                mapReferencerToReferenced, mapReferencedToReferencer,
                err: false
            };


        for (let serializedDoc of bufferedDocs) {

            let roughlyBlockSize = utils.stringify(grpdDocuments).length;
            // size control TODO: needs a little tuneing
            if (roughlyBlockSize > iConsts.MAX_BLOCK_LENGTH_BY_CHAR * (80 / 100))
                continue;

            // TODO: it is too important in a unique block exit both trx and refDoc, in some case there are 4 document which must exist in same block
            // add some controll to be sure about it.
            // now it is not the case until reaching buffer  total saize bigger than a single block(almost 10 Mega Byte)

            let aDoc = utils.parse(serializedDoc.bdPayload);


            if (!iutils.isValidVersionNumber(aDoc.dVer)) {
                msg = `invalid dVer for ${utils.hash6c(aDoc.hash)}) in retrieve And Group Buffered Documents`
                clog.sec.error(msg);
                return { err: true, msg }
            }

            if ((!utils.isValidDateForamt(aDoc.creationDate)) || (aDoc.creationDate > blockCreationDate) || (aDoc.creationDate > utils.getNow())) {
                msg = `creating new block, there are some documents with creationdate after block-creationDate(${blockCreationDate})!`;
                return { err: true, msg }
            }

            // length control
            if (parseInt(aDoc.dLen) != utils.stringify(aDoc).length) {
                msg = `create: The doc(${aDoc.dType} / ${utils.hash6c(aDoc.hash)}), stated dLen(${aDoc.dLen}), III. is not same as real length(${utils.stringify(aDoc).length})!`
                clog.sec.error(msg);
                return { err: true, msg }
            }


            if (!_.has(grpdDocuments, aDoc.dType))
                grpdDocuments[aDoc.dType] = [];
            grpdDocuments[aDoc.dType].push(aDoc);

            if (_.has(aDoc, 'ref') && !utils._nilEmptyFalse(aDoc.ref)) {
                if (iutils.canBeACostPayerDoc(aDoc.dType)) {
                    trxDict[aDoc.hash] = aDoc;
                    mapTrxHashToTrxRef[aDoc.hash] = aDoc.ref;
                    mapTrxRefToTrxHash[aDoc.ref] = aDoc.hash;
                } else {
                    mapReferencerToReferenced[aDoc.hash] = aDoc.ref;
                    mapReferencedToReferencer[aDoc.ref] = aDoc.hash;
                }
            }

            docByHash[aDoc.hash] = aDoc;
        }
        clog.app.info(`\n\ngenerating block, grpdDocuments: ${utils.stringify(grpdDocuments)}\n`);
        clog.app.info(`\n\ngenerating block, docByHash: ${utils.stringify(docByHash)}\n`);
        clog.app.info(`\n\ngenerating block, trxDict: ${utils.stringify(trxDict)}\n`);
        clog.app.info(`\n\ngenerating block, mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)}\n`);
        clog.app.info(`\n\ngenerating block, mapTrxHashToTrxRef: ${utils.stringify(mapTrxHashToTrxRef)}\n`);

        if (utils.objKeys(mapTrxRefToTrxHash).length != utils.objKeys(mapTrxHashToTrxRef).length) {
            msg = `create: transaction count and ref count are different! mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)} mapTrxHashToTrxRef: ${utils.stringify(mapTrxHashToTrxRef)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        for (let aRef of utils.objKeys(mapTrxRefToTrxHash)) {
            if (!_.has(trxDict, mapTrxRefToTrxHash[aRef])) {
                msg = `create: missed some3 transaction to support referenced documents. trxDict:${utils.stringify(trxDict)} mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)}`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }
        if (utils.arrayDiff(utils.objKeys(mapTrxHashToTrxRef), utils.objKeys(trxDict)) != 0) {
            msg = `create: missed some2 transaction to support referenced documents. trxDict:${utils.stringify(trxDict)} mapTrxRefToTrxHash: ${utils.stringify(mapTrxRefToTrxHash)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        for (let groupCode of utils.objKeys(grpdDocuments)) {
            msg = `extracted ${grpdDocuments[groupCode].length} documents of ${groupCode}`;
            console.log(`msg: ${msg}`);
            clog.app.info(msg);
        }

        let docsHashes = utils.objKeys(docByHash);
        for (let aRef of utils.objKeys(mapTrxRefToTrxHash)) {
            if (!docsHashes.includes(aRef)) {
                msg = `create! missed referenced document, which is supported by trx.ref(${utils.hash6c(aRef)})`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }

        return {
            docByHash, grpdDocuments, docIndexByHash, trxDict,
            mapTrxRefToTrxHash, mapTrxHashToTrxRef,
            mapReferencerToReferenced, mapReferencedToReferencer,
            err: false
        };
    }


    static retrieveAndGroupBufferedPOWDocuments(args = {}) {
        let msg;

        let blockCreationDate = args.blockCreationDate;

        let grpdDocuments = {};
        let docByHash = {};
        let docIndexByHash = {};

        let trxDict = {};
        let mapTrxHashToTrxRef = {};
        let mapTrxRefToTrxHash = {};
        let mapReferencerToReferenced = {};
        let mapReferencedToReferencer = {};

        if (!utils.isValidDateForamt(blockCreationDate)) {
            msg = `creating invalid block-POW-creationDate(${blockCreationDate})!`;
            return { err: true, msg, shouldPurgeMessage: true }
        }


        let bufferedDocs = docBufferHandler.searchBufferedDocsSync({
            query: [
                ['bd_doc_type', ['IN', [
                    iConsts.DOC_TYPES.CPost,
                ]]]
            ],
            order: [
                ['bd_dp_cost', 'DESC'],
                ['bd_doc_class', 'ASC'],
                ['bd_insert_date', 'ASC']
            ]
        });
        if (bufferedDocs.length == 0)
            return {
                docByHash, grpdDocuments, docIndexByHash, trxDict,
                mapTrxRefToTrxHash, mapTrxHashToTrxRef,
                mapReferencerToReferenced, mapReferencedToReferencer,
                err: false, shouldPurgeMessage: true
            };


        for (let serializedDoc of bufferedDocs) {

            let roughlyBlockSize = utils.stringify(grpdDocuments).length;
            // size control TODO: needs a little tuneing
            if (roughlyBlockSize > iConsts.MAX_BLOCK_LENGTH_BY_CHAR * (80 / 100))
                continue;


            // TODO: it is too important in a unique block exit both trx and refDoc, in some case there are 4 document which must exist in same block
            // add some controll to be sure about it.
            // now it is not the case until reaching buffer  total saize bigger than a single block(almost 10 Mega Byte)

            let aDoc = utils.parse(serializedDoc.bdPayload);

            if (!iutils.isValidVersionNumber(aDoc.dVer)) {
                msg = `invalid dVer for ${utils.hash6c(aDoc.hash)}) in retrieve And Group Buffered POW Documents`
                clog.sec.error(msg);
                return { err: true, msg }
            }

            console.log('aDoc', aDoc);
            if (![iConsts.CPOST_CLASSES.DMS_Post].includes(aDoc.dClass)) {
                msg = `bdDocClass is not supported(${aDoc.dClass})!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }

            if ((!utils.isValidDateForamt(aDoc.creationDate)) || (aDoc.creationDate > blockCreationDate) || (aDoc.creationDate > utils.getNow())) {
                msg = `creating new block, there are some documents with creationdate after block-creationDate(${blockCreationDate})!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // length control
            if (parseInt(aDoc.dLen) != utils.stringify(aDoc).length) {
                msg = `create: The doc(${aDoc.dType} / ${utils.hash6c(aDoc.hash)}), stated dLen(${aDoc.dLen}), IV. is not same as real length(${utils.stringify(aDoc).length})!`
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            if (!_.has(grpdDocuments, aDoc.dType))
                grpdDocuments[aDoc.dType] = [];
            grpdDocuments[aDoc.dType].push(aDoc);

            docByHash[aDoc.hash] = aDoc;
        }
        clog.app.info(`\n\ngenerating block, grpdDocuments: ${utils.stringify(grpdDocuments)}\n`);
        clog.app.info(`\n\ngenerating block, docByHash: ${utils.stringify(docByHash)}\n`);

        for (let groupCode of utils.objKeys(grpdDocuments)) {
            msg = `extracted ${grpdDocuments[groupCode].length} documents of ${groupCode}`;
            console.log(`msg: ${msg}`);
            clog.app.info(msg);
        }

        return {
            docByHash, grpdDocuments, docIndexByHash, trxDict,
            mapTrxRefToTrxHash, mapTrxHashToTrxRef,
            mapReferencerToReferenced, mapReferencedToReferencer,
            err: false, shouldPurgeMessage: true
        };
    }

    static docPresenter(doc, indetns = '') {
        let out = 'doc type not recognised';
        switch (doc.dType) {
            case iConsts.DOC_TYPES.DNAProposal:
                out = DNAHandler.DNAProposalPresenter(doc, indetns);
                break;

        }
        return out;
    }

    static blockTruncateToBroadcast(block) {

        // if (_.has(block, 'receiveDate'))
        //     delete block.receiveDate;
        // if (_.has(block, 'confirmDate'))
        //     delete block.confirmDate;

        return block;
    }


}

module.exports = blockUtils;
