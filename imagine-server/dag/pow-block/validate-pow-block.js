const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const blockHashHandler = require('../hashable');
const listener = require('../../plugin-handler/plugin-handler');
const merkleHandler = require('../../crypto/merkle/merkle');
const blockUtils = require('../block-utils');
const freeDocsInRelatedBlock = require('../documents-in-related-block/free-docs/free-docs');
const POWblockHandler = require('../../dag/pow-block/pow-block-handler');

class POWBlockValidateHandler {

    static validatePOWBlock(args) {
        let block = args.block;
        let stage = args.stage;

        clog.app.info('xxxxxxxxxxxx validate POW Block xxxx xxxxxxxxxxxxxxxx');
        clog.app.info(`\n\n\n${JSON.stringify(block)}\n`);
        // clog.app.info(blockPresenter(block));
        let res, msg;

        let bVer = _.has(block, 'bVer') ? block.bVer : null;
        if (utils._nilEmptyFalse(bVer) || !iutils.isValidVersionNumber(bVer)) {
            msg = `missed bVer POW block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }


        res = POWBlockValidateHandler.validateGeneralRulsForPOWBlock(block);
        if (res.err != false) {
            return res
        }

        res = POWBlockValidateHandler.hashDifficultyCheck(block);
        console.log('ooooooooooooooooooooooooooooooo');
        console.log('ooooooooooooooooooooooooooooooo');
        console.log('ooooooooooooooooooooooooooooooo');
        console.log('res', res);
        console.log('ooooooooooooooooooooooooooooooo');
        console.log('ooooooooooooooooooooooooooooooo');
        if (res.err != false) {
            return res
        }

        if (block.docs.length > iConsts.POW_MAX_DOC_PER_BLOCK) {
            return { err: true, msg: `The POW block(${utils.hash6c(block.blockHash)}) can not include more than 1 document`, shouldPurgeMessage: true }
        }


        let grpdRes = blockUtils.groupDocsOfPOWBlock({ block, stage });
        if (grpdRes.err != false) {
            grpdRes.shouldPurgeMessage = true;
            return grpdRes;
        }

        let grpdDocuments = grpdRes.grpdDocuments;
        let docIndexByHash = grpdRes.docIndexByHash;
        let trxDict = grpdRes.trxDict;
        let docByHash = grpdRes.docByHash;
        let dTyps = utils.objKeys(grpdDocuments).sort();
        clog.app.info(`Block(${utils.hash6c(block.blockHash)}) docs types: ${dTyps.map(x => `${x}: ${grpdDocuments[x].length}`)}`);

        let validateParams = {
            stage,
            docByHash,
            grpdDocuments,
            docIndexByHash,
            block
        };

        // TODO: important! currently the order of validating documents of block is important(e.g. polling must be validate before proposals and pledges)
        // improve the code and remove this dependency


        /**
         * validate free-docs (if exist)
         */
        let freeDocsValidateRes = freeDocsInRelatedBlock.validatePOWFreeDocs(validateParams);
        if (freeDocsValidateRes.err != false) {
            return freeDocsValidateRes;
        }


        // validate...

        clog.app.info(`--- confirmed normal block(${utils.hash6c(block.blockHash)})`);


        let hookValidate = listener.doCallSync('SASH_validate_POW_block', block);
        if (_.has(hookValidate, 'err') && (hookValidate.err != false)) {
            return hookValidate;
        }

        return {
            err: false,
        };

    }

    static validateGeneralRulsForPOWBlock(block) {
        let msg;

        if (!utils.isValidDateForamt(block.creationDate)) {
            msg = `invalid block.creationDate(${block.creationDate})!`;
            return { err: true, msg, shouldPurgeMessage: true }
        }

        if (utils.isGreaterThanNow(block.creationDate)) {
            msg = `Block whith future creation date is not acceptable (${utils.hash6c(block.blockHash)}) ${block.creationDate}`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // docRootHash control
        let docsRootHash = merkleHandler.merkle(block.docs.map(x => x.hash)).root;
        if (block.docsRootHash != docsRootHash) {
            msg = `Mismatch block DocRootHash. locally calculated(${utils.hash6c(docsRootHash)}) remote(${utils.hash6c(block.docsRootHash)})`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // re-calculate block hash
        let blockHashRes = POWblockHandler.calcHashBPOW(block);
        if (blockHashRes.err != false) {
            msg = `wrongggg(${utils.hash6c(blockHashRes.hash)}) remote(${utils.hash6c(block.blockHash)})`
            clog.sec.error(msg);
            blockHashRes.shouldPurgeMessage = true;
            return blockHashRes;
        }
        if (blockHashRes.hash != block.blockHash) {
            msg = `Mismatch blockHash. locally calculated(${utils.hash6c(blockHashRes.hash)}) remote(${utils.hash6c(block.blockHash)})`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // Block length control
        if (utils.stringify(block).length != block.blockLength) {
            msg = `Mismatch block(${utils.hash6c(block.blockHash)}) pow length locally calculated length(${utils.stringify(block).length}) != remote length(${block.blockLength})`;
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        let blockAncestors = block.ancestors;
        if (!Array.isArray(blockAncestors) || blockAncestors.length == 0) {
            msg = `Block(${utils.hash6c(block.blockHash)}) has not any ancestor! `;
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true };
        }

        return { err: false, shouldPurgeMessage: true }
    }

    static hashDifficultyCheck(block) {
        console.log('validate powwwww', block.blockHash);
        let targetPattern = POWblockHandler.getTargetPattern({ cDate: block.creationDate });
        if (block.blockHash.startsWith(targetPattern))
            return { err: false }
        return { err: true, msg: `The block(${utils.hash16c(block.blockHash)}) didn't meet desired hash difficulty`, shouldPurgeMessage: true };
    }
}

module.exports = POWBlockValidateHandler;
