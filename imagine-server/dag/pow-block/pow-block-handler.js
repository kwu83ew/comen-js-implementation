const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const iutils = require('../../utils/iutils');
const utils = require('../../utils/utils');
const blockTplHandler = require('../../docs-structure/block/block-tpl-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const merkleHandler = require('../../crypto/merkle/merkle');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const blockUtils = require('../block-utils');
const blockHashHandler = require('../../dag/hashable');
const leavesHandler = require('../../dag/leaves-handler');
const freeDocsInRelatedBlock = require('../documents-in-related-block/free-docs/free-docs');
let Moment;

class POWBlockCreator {

    static async createAPOWBlock(args = {}) {
        let msg;
        console.log('create ANormalBlock create A NormalBlock create A NormalBlock');

        let stage = iConsts.STAGES.Creating;
        let creationDate = utils.getNow();
        if (_.has(args, 'creationDate') && (args.creationDate != null)) {
            creationDate = args.creationDate;
        }

        let hasFreshLeaves = leavesHandler.hasFreshLeaves(); // younger than one cycle (12 hours)
        if (!hasFreshLeaves) {
            msg = `NB: Machine hasn't fresh leaves!`
            clog.app.info(msg);
            dagMsgHandler.setMaybeAskForLatestBlocksFlag(iConsts.CONSTS.YES);
            return { err: true, msg: msg };
        }

        let block = {
            net: 'im',
            bType: iConsts.BLOCK_TYPES.POW,
            bVer: "0.0.0",
            blockLength: '0000000',
            blockHash: "0000000000000000000000000000000000000000000000000000000000000000",
            nonce: "0000000000000000",
            ancestors: [],
            signals: [], // e.g. ["mimblewimble", "taproot", "schnorrMusic", "outputTimelockActivated"]
            fVotes: [], // floating votes(e.g. coinbase confirmations, susVotes, cloneTransaction...)
            backer: "",
            docs: [],
            docsRootHash: "", // the hash root of merkle tree of transactions
            creationDate,
            signals: iutils.getMachineSignals(),
            bExtInfo: [],
            bExtHash: '',
        };
        let docsHashes = [];
        let externalInfoHashes = [];
        clog.app.info(`preparing The POW block to generate ${creationDate}`);

        let machineSettings = machine.getMProfileSettingsSync();
        block.backer = machineSettings.backerAddress;

        /**
         * the first step of creating a block is appending the transactions
         * each block MUST have at least one transaction
         */
        // let appendTransactionsRes = {
        //     err: false,
        //     externalInfoHashes,
        //     docsHashes,
        //     block
        // }

        let grpdRes = blockUtils.retrieveAndGroupBufferedPOWDocuments({ blockCreationDate: creationDate });
        console.log('\n\n grpdRes', utils.stringify(grpdRes));
        if (grpdRes.err != false)
            return grpdRes;
        let docByHash = grpdRes.docByHash;
        let grpdDocuments = grpdRes.grpdDocuments;

        let appendArgs = { block, creationDate, docsHashes, externalInfoHashes, docByHash, grpdDocuments }

        /**
        * add free Documents(if exist)
        * since block size controlling is not implemented completaly, it is better to put this part at the begening of appending, 
        * just in order to be sure block has enough capacity to include entire docs in buffer
        */
        let addFreeDocsRes = freeDocsInRelatedBlock.appendPOWFreeDocsToBlock(appendArgs);
        clog.app.info(`addFreeDocsRes: ${utils.stringify(addFreeDocsRes)}`);
        if (addFreeDocsRes.err != false) {
            console.log(`addFreeDocsRes ${addFreeDocsRes.msg}`);
            return addFreeDocsRes;
        }
        block = addFreeDocsRes.block;
        docsHashes = addFreeDocsRes.docsHashes;
        externalInfoHashes = addFreeDocsRes.externalInfoHashes;
        if (addFreeDocsRes.addedDocs > 0)
            console.log(`\n\nblock After Adding free-docs: ${utils.stringify(block)}`);


        /**
         * TODO: maybe adding more doc types to be acceptable by POW, e.g. register FleNS contracts, or paying for messenger using by POW instead of classic PAI
         * anyway POW will NOT be used as a coin minting solution
         */

        clog.app.info(`The NORMAL block has ${docsHashes.length} docs`);
        block.docsRootHash = merkleHandler.merkle(docsHashes).root;
        block.bExtHash = merkleHandler.merkle(externalInfoHashes).root;
        if (
            _.has(args, 'ancestors') &&
            !utils._nilEmptyFalse(args.ancestors) &&
            Array.isArray(args.ancestors) &&
            (args.ancestors.length > 0)
        ) {
            block.ancestors = args.ancestors;

        } else {
            let leaves = leavesHandler.getLeaveBlocks();
            leaves = utils.objKeys(leaves);
            let currentAncestors = block.ancestors;
            currentAncestors = _.sortBy(currentAncestors.concat(leaves));
            block.ancestors = currentAncestors;

        }
        console.log(`------------------------- The NORMAL block ancestors will be ${block.ancestors}`);//${bufferedTrxs.length} 
        clog.app.info(`The NORMAL block ancestors will be ${block.ancestors}`);

        // block = blockLib.blockTruncateToBroadcast(block);
        clog.app.info('publishing blockkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
        clog.app.info(JSON.stringify(block));
        clog.app.info('publishing blockkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
        block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);

        // generate a POW hash
        let POWtHashRes = await POWBlockCreator.calcNonce({
            document: block,
            hasherFunc: POWBlockCreator.calcHashBPOW
        });
        if (POWtHashRes.err != false)
            return POWtHashRes;
        block.nonce = POWtHashRes.nonce;
        block.blockHash = POWtHashRes.hash;

        msg = `Sending a POW block to network: ${JSON.stringify(block)}`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        let blockDocUniqRes = blockUtils.groupDocsOfPOWBlock({ block, stage });
        if (blockDocUniqRes.err != false) {
            blockDocUniqRes.shouldPurgeMessage = true;
            return blockDocUniqRes;
        }

        // write file on hard output/send email
        msg = `The POW block(${utils.hash6c(block.blockHash)}) DIFFICULTY(${cnfHandler.getPoWDifficulty()}) generated & pushed to sending q \n\n${utils.stringify(block)}\n`;
        clog.app.info(msg);
        console.log(msg);

        sendingQ.pushIntoSendingQ({
            sqType: iConsts.BLOCK_TYPES.Normal,
            sqCode: block.blockHash,
            sqPayload: utils.stringify(block),
            sqTitle: `normal block(${utils.hash6c(block.blockHash)}) ${utils.getNow()}`,
            // blockHash: block.blockHash
        })
        console.log(`------------------------- The POW. block(${utils.hash6c(block.blockHash)}) generated & pushed to send`);

        // remove from buffer
        docBufferHandler.removeFromBufferSync({ hashes: docsHashes });

        return {
            err: false,
            blockPushedTosendingQ: true,
            msg: 'Block created and pushed to Queue to broadcast to network',
        };


    }


    static calcHashBPOW(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            ancestors: block.ancestors,
            backer: block.backer,
            blockLength: block.blockLength,
            // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash
            bType: block.bType,
            bVer: block.bVer,
            creationDate: block.creationDate,
            descriptions: block.descriptions,
            docsRootHash: block.docsRootHash, // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
            bExtHash: block.bExtHash, // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
            fVotes: block.fVotes,
            net: block.net,
            nonce: block.nonce,
            signals: block.signals,
        };
        let hash = iutils.doHashObject(hashableBlock);
        return { err: false, hash };
    }


    static async calcNonce(args) {
        let document = args.document;
        let nonce;
        let hasherFunc = args.hasherFunc;

        let targetPattern = POWBlockCreator.getTargetPattern({ cDate: document.creationDate });
        console.log('-------------- targetPattern -------', targetPattern);
        console.log('-------------- start hash targeting -------', utils.getNowSSS());
        clog.app.info(`-------------- start hash targeting(${targetPattern}) ------- ${utils.getNowSSS()}`);
        let i = 0;
        POWBlockCreator.shouldExit = false;
        while ((i < Number.MAX_SAFE_INTEGER) && !POWBlockCreator.shouldExit) {
            i++;
            nonce = i.toString().padStart(16, '0');
            document.nonce = nonce;
            let hashRes = await POWBlockCreator._calcNonce(hasherFunc, document);
            console.log('nonce, hash', nonce, hashRes.hash);
            if (hashRes.err != false) {
                POWBlockCreator.shouldExit = true;
                return hashRes;
            }
            if (hashRes.hash.startsWith(targetPattern)) {
                POWBlockCreator.shouldExit = true;
                console.log('-------------- end hash targeting -------', utils.getNowSSS());
                clog.app.info(`-------------- end hash targeting ------- ${utils.getNowSSS()}`);
                return { err: false, nonce, hash: hashRes.hash };
            }
        }
        return { err: true, msg: `the POW couldn't find targeted hash! please try it later` };
    }

    static async _calcNonce(hasherFunc, document) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let hashRes = hasherFunc(document);
                return resolve(hashRes);
            }, 3);
        });
    }


    static getTargetPattern(args) {
        let cDate = _.has(args, 'cDate') ? args.cDate : null;
        if (cDate == null)
            cDate = utils.getNow();
        let difficulty = _.has(args, 'difficulty') ? args.difficulty : cnfHandler.getPoWDifficulty({ cDate });

        if (!Moment)
            Moment = require('../../startup/singleton').instance.Moment;

        let targetPattern = Moment(cDate, 'YYYY-MM-DD HH:mm:ss').format('MMDDHHmmssYYYY');
        targetPattern = `${targetPattern}${targetPattern}`;
        console.log('targetPattern 1 ', targetPattern);
        targetPattern = targetPattern.substr(0, difficulty);
        console.log('targetPattern 2 ', targetPattern);
        return targetPattern;
    }



}

module.exports = POWBlockCreator;
