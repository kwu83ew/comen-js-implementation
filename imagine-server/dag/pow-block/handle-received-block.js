const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const POWBlockVHandler = require('./validate-pow-block');
const dagHandler = require('../graph-handler/dag-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager');
const clog = require('../../loggers/console_logger');
const DNAHandler = require('../../dna/dna-handler');

class POWBlockHandler {

    static handleReceivedPOWBlock(args) {
        clog.app.info('******** handle Received POW Block Sync');
        clog.app.info(JSON.stringify(args));
        clog.app.info(`\n`);
        let block = _.has(args, 'payload') ? args.payload : null;
        // block.receiveDate = utils.getNow();

        clog.app.info(`--- handle POW block(${utils.hash6c(block.blockHash)}) `);
        let validateRes = POWBlockVHandler.validatePOWBlock({
            stage: iConsts.STAGES.Validating,
            block
        });
        clog.app.info(`Received a (${block.bType}) block, validation result: ${JSON.stringify(validateRes)}`);
        if (validateRes.err != false) {
            // maybe do something
            return validateRes;
        }

        let confidence = DNAHandler.getAnAddressShares(block.backer, block.creationDate).percentage;
        //TODO: prepare a mega query to run in atomic transactional mode

        // add block to DAG(refresh leave blocks, remove from utxos, add to utxo12...)
        // insert the block in i_blocks as a confirmed block
        dagHandler.addBH.addBlockToDAG({ block, confidence });
        dagHandler.addBH.postAddBlockToDAG({ block });

        // broadcast block to neighbors
        if (dagHandler.isDAGUptodated()) {
            let pushRes = sendingQ.pushIntoSendingQ({
                sqType: block.bType,
                sqCode: block.blockHash,
                sqPayload: utils.stringify(block),
                sqTitle: `Broadcasting confirmed POW block(${utils.hash6c(block.blockHash)})`,
            })
            clog.app.info(`BCPOWB pushRes: ${JSON.stringify(pushRes)}`);
            return { err: false, shouldPurgeMessage: true };
        }

        return { err: true, shouldPurgeMessage: true }
    }

}


module.exports = POWBlockHandler;
