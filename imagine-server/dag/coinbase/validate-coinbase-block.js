const _ = require('lodash')
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const doCCB = require('./do-create-coinbase-block');
const machine = require('../../machine/machine-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');

class CBV {

    static validateCoinbaseBlock(args) {
        let msg;
        let sender = args.sender;
        let remoteCB = args.payload;

        let cbInfo = iutils.getCoinbaseInfo({
            cycle: remoteCB.cycle
        })

        clog.cb.info(`\n$$$$$$$$$$ validate CoinbaseBlock(${utils.hash6c(remoteCB.blockHash)}) cycle:${remoteCB.cycle} from:${cbInfo.from} $$$$$$$$$$`);
        let res = { err: true, shouldPurgeMessage: true, msg: '', sender: sender };

        res.remoteCB = remoteCB

        if (remoteCB.ancestors.length == 0) {
            msg = `Invalid received coinbase(${utils.hash6c(remoteCB.blockHash)}) from ${sender}. has no ancestors!`
            clog.sec.debug(msg);
            clog.cb.error(msg);
            res.err = true
            res.msg += '\n' + msg
            res.shouldPurgeMessage = true
            return res
        }

        let bVer = _.has(remoteCB, 'bVer') ? remoteCB.bVer : null;
        if (utils._nilEmptyFalse(bVer) || !iutils.isValidVersionNumber(bVer)) {
            msg = `invalid bVer coinbase block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        // in case of synching, we force machine to (maybe)conclude the open pollings
        if (machine.isInSyncProcess())
            pollHandler.concludeHandler.doPollingConcludeTreatment({
                cDate: remoteCB.creationDate
            });

        let { block, err } = doCCB.doGenerateCoinbaseBlock({ cycle: remoteCB.cycle, mode: 'regenerate' });
        let localCB = block;
        // if (utils._nilEmptyFalse(localCB)) {
        //     msg = `machine can not create local CB for cycle ${remoteCB.cycle}`
        //     res.err = true
        //     res.msg += '\n' + msg
        //     res.shouldPurgeMessage = true
        //     return res
        // }
        res.localCB = localCB
        if (utils._nilEmptyFalse(localCB))
            console.log(`big failllllllll ${utils.stringify(remoteCB)}`);

        if (localCB.docsRootHash != remoteCB.docsRootHash) {
            msg = `Discrepancy in docsRootHash locally created coinbase block: ${utils.hash6c(localCB.docsRootHash)} 
        and remote: ${utils.hash6c(remoteCB.docsRootHash)} from ${sender}`;
            clog.app.error(msg)
            clog.app.debug(`remoteCB: ${JSON.stringify(remoteCB)}`);
            clog.app.debug(`localCB: ${JSON.stringify(localCB)}`);
            clog.cb.error(msg)
            clog.cb.debug(`remoteCB: ${JSON.stringify(remoteCB)}`);
            clog.cb.debug(`localCB: ${JSON.stringify(localCB)}`);
            res.err = true
            res.msg += '\n' + msg
            res.shouldPurgeMessage = true
            return res
        }


        // let remoteConfidence = remoteCB.confidence      //calcCBConfidence(remoteCB)
        // res.remoteConfidence = remoteConfidence
        clog.cb.info(`remoteConfidence : ${JSON.stringify(remoteCB.confidence)}`);
        // if (remoteConfidence.err != false) {
        //     remoteConfidence.shouldPurgeMessage = true
        //     return remoteConfidence
        // }
        // let localConfidence = calcCBConfidence(localCB)
        // res.localConfidence = localConfidence
        // clog.cb.info(`localConfidence : ${JSON.stringify(localConfidence)}`);
        // if (localConfidence.err != false) {
        //     localConfidence.shouldPurgeMessage = true
        //     return localConfidence
        // }

        msg = `Valid CB(${utils.hash6c(remoteCB.blockHash)}) received from ${sender}`
        clog.cb.info(msg)
        res.err = false;
        res.msg += '\n' + msg
        res.shouldPurgeMessage = true
        return res
    }
}

module.exports = CBV;
