const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const cbHandler = require('./validate-coinbase-block');
const dagHandler = require('../graph-handler/dag-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager');
const blockUtils = require('../block-utils');

class ReceivedCBHandler {
    
    static handleReceivedCoinbase(args) {
        let msg;
        let block = args.payload;
        let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow()
        block.ancestors = blockUtils.normalizeAncestors(block.ancestors);

        let validateRes = cbHandler.validateCoinbaseBlock(args);
        clog.cb.info(`Received validate CoinbaseBlock result: ${JSON.stringify(validateRes)}`);
        if (validateRes.err != false) {
            // do something
            return { err: true, msg: 'Wrong coinbase block ' + validateRes.msg, shouldPurgeMessage: true }
        }

        dagHandler.addBH.addBlockToDAG({
            block,
            receive_date: receive_date,
            confidence: block.confidence
        });
        dagHandler.addBH.postAddBlockToDAG({ block });


        // broadcast block to neighbors
        if (iutils.isInCurrentCycle(block.creationDate)) {
            let pushRes = sendingQ.pushIntoSendingQ({
                sqType: iConsts.BLOCK_TYPES.Coinbase,
                sqCode: block.blockHash,
                sqPayload: utils.stringify(block),
                sqTitle: `Broadcasting the confirmed coinbase block(${utils.hash6c(block.blockHash)}) in current cycle`,
                // noReceivers: [args.sender]
            })
            clog.app.info(`coinbase pushRes: ${pushRes}`);
        }

        return { err: false, shouldPurgeMessage: true }
    }
}

module.exports = ReceivedCBHandler;
