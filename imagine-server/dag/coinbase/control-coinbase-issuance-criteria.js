const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const machine = require('../../machine/machine-handler');
const crypto = require('../../crypto/crypto');
const leavesHandler = require('../leaves-handler');
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const DNAHandler = require('../../dna/dna-handler');
const missedBlocksHandler = require('../../dag/missed-blocks-handler');
const dagHandler = require('../graph-handler/dag-handler');

class CBCriteria {

    static controlCoinbaseIssuanceCriteria() {
        let msg;
        let curCBRange = iutils.getCoinbaseRange();
        let res = {};
        clog.cb.info(`Coinbase check Range (from ${curCBRange.from} to ${curCBRange.to})`);

        let hasFreshLeaves = leavesHandler.hasFreshLeaves(); // younger than 2 cycle (24 hours)
        if (!hasFreshLeaves) {
            msg = `Machine hasn't fresh leaves!`
            clog.cb.info(msg);
            dagMsgHandler.setMaybeAskForLatestBlocksFlag(iConsts.CONSTS.YES);
            clog.cb.info('The flag "maybe_ask_for_latest_blocks" setted');
            res.canGenCB = false;
            res.msg = msg
            return res;
        }

        // control if already exist in DAG a more confidence Coinbase Block than what machine can create?
        let DAGHasMoreConfidenceCB = this.doesDAGHasMoreConfidenceCB();
        if (DAGHasMoreConfidenceCB) {
            msg = `Machine already has more confidente CB in DAG`
            clog.cb.info(msg);
            res.canGenCB = false;
            res.msg = msg
            return res;
        }


        // // some control to be sure the coinbase block for current 12 hour cycle didn't create still,
        // // or at least I haven't it in my local machine
        // let latestCoinbase = cbBufferHandler.getMostConfidenceFromBuffer(currntCoinbaseTimestamp);
        // if (latestCoinbase.cycle == currntCoinbaseTimestamp) {
        //     // check if after sending cb to other , the local machine has added any new block to DAG? and machine still doesn't receive confirmed cb block?
        //     // so must create nd send new coinbase block
        //     msg = `At least one CB exists on local machine: ${latestCoinbase} (${iutils.getCoinbaseRange().from.split(' ')[1]} - ${iutils.getCoinbaseRange().to.split(' ')[1]})`
        //     clog.cb.info(msg);
        //     res.msg = msg
        //     res.atLeastOneCBExists = true;
        // }


        // postpond coinbase-generating if machine missed some blocks
        let missedBlocks = missedBlocksHandler.getMissedBlocksToInvoke();
        if (missedBlocks.length > 0) {

            // // FIXME: if an adversory sends a bunch of blocks which have ancestors, in machine will finished with a long list of 
            // // missed blocks. so machine(or entire network machines) can not issue new coinbase block!
            // if (missedBlocks.length > iConsts.MAX_TOLERATED_MISS_BLOCKS) {
            //     msg = `Machine missed ${missedBlocks.length} blocks and touched the limitation(${iConsts.MAX_TOLERATED_MISS_BLOCKS}), so can not issue a coinbase block`
            //     clog.cb.info(msg);
            //     res.canGenCB = false;
            //     res.msg = msg
            //     return res;
            // }

            let latenancyFactor = Math.floor(((Math.log(missedBlocks.length + 1) / Math.log(iConsts.MAX_TOLERATED_MISS_BLOCKS)) * iutils.getCoinbaseAgeBySecond()));
            let areWeIn4of5 = (iutils.getCoinbaseAgeBySecond() < (iConsts.getCycleBySeconds() * 4 / 5));
            if (areWeIn4of5 && (iutils.getCoinbaseAgeBySecond() < latenancyFactor)) {
                msg = `Because of ${missedBlocks.length} missed blocks, machine can not create CB before ${latenancyFactor} second age of cycle or atleast 4/5 of cycle age passed`;
                console.log(`msg: ${msg}`);
                clog.cb.info(msg);
                res.canGenCB = false;
                res.msg = msg
                return res;
            }
        }

        // a psudo random mechanisem
        let amIQualified = this.haveIFirstHashedEmail();
        if (!amIQualified) {
            msg = `it is not your turn To Create Coinbase Block!`;
            clog.cb.info(msg);
            res.canGenCB = false;
            res.msg = msg
            return res;
        }


        res.canGenCB = true;
        return res;
    }

    static makeEmailHashDict() {
        let emailsHashDict = {};
        let cycle = iutils.getCoinbaseCycleStamp();
        let machineSettings = machine.getMProfileSettingsSync();
        let machineEmail = machineSettings.pubEmail.address;
        let machineKey = crypto.keccak256(cycle + '::' + machineEmail);
        emailsHashDict[machineKey] = machineEmail;

        let neightbors = machine.neighborHandler.getNeighborsSync({ fields: ['n_email'] });
        clog.cb.info(`neightbors in makeEmail Hash Dict: ${utils.stringify(neightbors)}`);
        neightbors.forEach(neighbor => {
            let key = crypto.keccak256(cycle + '::' + neighbor.nEmail);
            emailsHashDict[key] = neighbor.nEmail;
        });
        return { cycle, machineEmail, machineKey, emailsHashDict };
    }

    static haveIFirstHashedEmail(order = 'asc') {
        let { cycle, machineEmail, machineKey, emailsHashDict } = this.makeEmailHashDict();
        let keys;
        if (order == 'asc') {
            keys = utils.objKeys(emailsHashDict).sort()
        } else {
            keys = utils.objKeys(emailsHashDict).reverse()
        }
        clog.cb.info(`**************************** *** ** * ordered emailsHashDict ${order}: `);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            clog.cb.info(`${i + 1}. ${utils.hash6c(key)}: ${emailsHashDict[key]}`);
        }
        let inx = keys.indexOf(machineKey);
        if (inx == 0) {
            // the machin has minimum hash, so can generate the coinbase
            clog.cb.info(`I have the lowest/heighest hash: ${machineEmail} => ${machineKey}`);
            return true;
        }

        // if the machine email hash is not the smalest, 
        // control it if based on time passed from coinbase-cycle can create the coinbase?
        let { percentage } = DNAHandler.getMachineShares();
        percentage = parseInt(percentage / 5) + 1;
        let subCycle = (iConsts.TIME_GAIN == 1) ? 12 + percentage : 6 + percentage; // who has more shares should try morre times to create a coinbase block
        let cbEmailCounter = iutils.getCoinbaseAgeBySecond() / (iConsts.getCycleBySeconds() / subCycle);
        clog.cb.info(`cbEmailCounter cycle ${cbEmailCounter} ${cbEmailCounter} > ${inx}`);
        if (cbEmailCounter > inx) {
            // it is already passed time and if still no one create the block it is my turn to create it

            clog.cb.info(`It already passed ${cbEmailCounter} of 10 dividend of a cycle and now it's my turn (${machineEmail}) to coinbase!`)
            return true;
        }

        clog.cb.info(`${cycle + '::' + machineEmail} has to wait To Create Coinbase Block! (if does not receive the fresh CBB)`);
        return false;
    }

    static doesDAGHasMoreConfidenceCB() {
        let curCBRange = iutils.getCoinbaseRange();
        let currentCycleCoinbaseWBlocks = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_confidence', 'b_ancestors'],
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase],
                ['b_creation_date', ['>=', curCBRange.from]],
            ]
        });
        clog.cb.info(`currentCycleCoinbaseWBlocks`, currentCycleCoinbaseWBlocks);
        if (currentCycleCoinbaseWBlocks.length == 0)
            return false;

        let alreadyRecordedConfidents = [];
        let alreadyRecordedAncestors = [];
        for (let aWBlock of currentCycleCoinbaseWBlocks) {
            alreadyRecordedConfidents.push(utils.iFloorFloat(parseFloat(aWBlock.bConfidence)));
            alreadyRecordedAncestors = utils.arrayAdd(alreadyRecordedAncestors, utils.parse(aWBlock.bAncestors));
        }
        alreadyRecordedAncestors = utils.arrayUnique(alreadyRecordedAncestors);
        clog.cb.info(`already Recorded Confidents from(${curCBRange.from})`, alreadyRecordedConfidents);
        clog.cb.info(`already Recorded Ancestors from(${curCBRange.from})`, alreadyRecordedAncestors);

        if ((alreadyRecordedConfidents.length == 0) || (alreadyRecordedAncestors.length == 0))
            return false;

        let maxRecordedConfident = alreadyRecordedConfidents.sort().reverse()[0];
        let potentiallyFlotSignInfo = dagHandler.fSigs.aggrigateFloatingSignatures();
        clog.cb.info(`ccccccccccccccccc: ${potentiallyFlotSignInfo.blockHashes.map(x => utils.hash6c(x))} - ${alreadyRecordedAncestors.map(x => utils.hash6c(x))}`);
        clog.cb.info(`potentialy CB will have confidence(${potentiallyFlotSignInfo.confidence}) blocks(${JSON.stringify(potentiallyFlotSignInfo.blockHashes.map(x => utils.hash6c(x)))}) Backers(${JSON.stringify(potentiallyFlotSignInfo.blockHashes.map(x => iutils.shortBech(x)))}) `);
        let notRecordedBlocks = utils.arrayDiff(potentiallyFlotSignInfo.blockHashes, alreadyRecordedAncestors);
        clog.cb.info(`not Recorded Blocks: ${notRecordedBlocks.map(x => utils.hash6c(x))}`);
        if ((potentiallyFlotSignInfo.confidence > maxRecordedConfident) || (notRecordedBlocks.length > 0))
            return true;

        return false;
    }

}



module.exports = CBCriteria;
