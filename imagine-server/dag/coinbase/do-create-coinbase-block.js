const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const getMoment = require('../../startup/singleton').instance.getMoment;
const DNAHandler = require('../../dna/dna-handler');
const trxTpl = require('../../docs-structure/transaction/interface');
const blockTpl = require('../../docs-structure/block/block-tpl-handler');
const blockHasher = require('../hashable');
// const blockSigner = require('../signable');
const merkleHandler = require('../../crypto/merkle/merkle');
// const blockPresenter = require('../block-presenter');
const leavesHandler = require('../leaves-handler');
const trxHashHandler = require('../../transaction/hashable');
const treasuryHandler = require('../../services/treasury/treasury-handler');
const blockUtils = require('../../dag/block-utils');

class CBHandler {
    /**
     * 
     * @param {*} cycle 
     * 
     * although the coinbase core is only shares (which are equal in entire nodes)
     * but the final coinbase block consists of also ancestors links (which are participating in block hash)
     * so it could be possible different nodes generate different coinbaseHash for same blocks.
     * 
     */
    static doGenerateCoinbaseBlock(args = {}) {
        const dagHandler = require('../graph-handler/dag-handler');

        let mode = _.has(args, 'mode') ? args.mode : 'generate';
        let cycle = _.has(args, 'cycle') ? args.cycle : iutils.getCoinbaseCycleStamp();
        clog.cb.info(`do GenerateCoinbaseBlock cycle ${cycle} mode ${args.mode}`);
        let cbInfo = iutils.getCoinbaseInfo({
            cycle: cycle
        })
        let block = this.createCBCore({ cycle, mode: args.mode });

        // connecting to existed leaves as ancestors
        let leaves = leavesHandler.getLeaveBlocks({
            maxCreationDate: cbInfo.from
        });
        clog.cb.info(`do GenerateCoinbaseBlock retrieved cbInfo: ${JSON.stringify(cbInfo)}`);
        clog.cb.info(`do GenerateCoinbaseBlock retrieved leaves from kv: ${JSON.stringify(leaves)} cycle: ${cycle}`);
        leaves = utils.objKeys(leaves);
        clog.cb.info(`do GenerateCoinbaseBlock retrieved leaves from kv: ${JSON.stringify(leaves)} cycle: ${cycle}`);
        // clog.cb.info(`do GenerateCoinbaseBlock block ancestors: ${currentAncestors.join()} `);
        // currentAncestors = utils.arrayAdd(currentAncestors, leaves).sort();
        clog.cb.info(`do GenerateCoinbaseBlock aggrigated ancestors: ${leaves.sort().join()} `);

        let floatingSignatures = dagHandler.fSigs.aggrigateFloatingSignatures();
        clog.cb.info(`locally created block's confidence: ${JSON.stringify(floatingSignatures)}`);
        block.confidence = floatingSignatures.confidence
        leaves = utils.arrayUnique(utils.arrayAdd(leaves, floatingSignatures.blockHashes));

        // if requested cycle is current cycle and machine hasn't fresh leaves, so can not generate a CB block
        if ((mode == 'generate') && (leaves.length == 0) && (cycle == iutils.getCoinbaseCycleStamp())) {
            if (mode == 'generate') {
                clog.app.error(`generating new CB in generating mode faild!! leaves(${leaves.length}))`);
            } else {
                clog.app.info(`strange error generating new CB faild!! mode(${mode} leaves(${leaves.length}))`);
            }
            return { block: null, err: true };
        }

        block.ancestors = blockUtils.normalizeAncestors(leaves);
        // block.signals  = iutils.getMachineSignals();   // since it is not going to broadcost, so do not need the signals 
        clog.cb.info(`do GenerateCoinbaseBlock block.ancestors: ${block.ancestors} `);


        // if the backer is also a shareholder (probably with large amount of shares), 
        // would be more usefull if she also signs the dividends by his private key as a shareholder 
        // and sends also her corresponding publick key
        // this signature wil be used for 2 reson
        // 1. anti rebuild DAG by adverseries
        // 2. prevent fake sus-blocks to apply to network
        // signing block by backer private key (TODO: or delegated private key for the sake of security)

        block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);
        block.blockHash = blockHasher.hashBlock(block);

        // clog.app.info(blockPresenter(block));
        return { block, err: false };
    }

    /**
     * 
     * @param {the time for which cycle is calculated} cycle 
     * 
     * coinbase core is only shares and dividends, 
     * so MUST BE SAME IN EVERY NODES.
     * the inputs are newly minted coins and treasury incomes
     * 
     */
    static createCBCore(args) {
        let cycle = args.cycle;
        let mode = args.mode;

        clog.cb.info(`create CBCore cycle(${cycle}) mode(${mode})`)
        let cDate;
        let mintingYear;
        if (cycle == '') {
            cDate = utils.getNow();
            cycle = iutils.getCoinbaseCycleStamp();
            mintingYear = getMoment().format('YYYY');
        } else {
            if (iConsts.TIME_GAIN == 1) {
                // normally the cycle time is 12 hours
                cDate = cycle;

            } else {
                // here is for test net in which the cycle time is accelerated to have a faster block generator(even 2 minutes)
                let minutes = parseInt(cycle.split(' ')[1]) * iConsts.getCycleByMinutes();
                minutes = iutils.convertMinutesToHHMM(minutes)
                cDate = cycle.split(' ')[0] + ' ' + minutes + ':00';
            }
            mintingYear = cycle.substr(0, 4);
        }

        let blockCreationDate = iutils.getCoinbaseRangeByCycleStamp(cycle).from;
        let trx = trxTpl.getCoinbaseTemplate();
        trx.cycle = cycle;

        let { incomes, fromDate, toDate } = treasuryHandler.calcTreasuryIncomesSync(cDate);
        clog.cb.info(`coinbase cDate(${cDate}) treasury incomes(${utils.sepNum(incomes)}) mcPAIs fromDate(${fromDate}) toDate(${toDate})`);

        trx.treasuryIncomes = incomes;
        trx.treasuryFrom = fromDate;
        trx.treasuryTo = toDate;

        // create coinbase outputs
        let tmpOutDict = {};
        let holders = [];
        let dividends = [];

        /**
         * 
         minted: 2,251,799,813.685248
         burned: 
         share1:    21,110,874.142979
         share2:       985,017.380061
         share3:       422,068.382528
         share4:            38.230606
         */
        let { oneCycleMaxCoins, oneCycleIssued, sumShares, holdersByKey } = iutils.calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore({
            calculateDate: blockCreationDate
        });
        clog.app.info(`>>>>>>>>>>> holdersByKey ${utils.stringify(holdersByKey)}`);
        trx.mintedCoins = oneCycleIssued
        let cycleCoins = trx.treasuryIncomes + trx.mintedCoins;
        clog.cb.info(`DNA cycle sum minted coins+treasury: ${utils.sepNum(cycleCoins)} mcPAIs. Cycle Max Coins:${utils.sepNum(oneCycleMaxCoins)} mcPAIs`);

        _.forOwn(holdersByKey, (aShare, aHolder) => {
            let dividend = utils.floor((utils.iFloorFloat(aShare / sumShares) * cycleCoins));
            // clog.app.info(`\n>>>>>>>>>>> aShare ${aShare}`);
            // clog.app.info(`>>>>>>>>>>> sumFactor ${sumFactor}`);
            // clog.app.info(`>>>>>>>>>>> sumShares ${sumShares}`);
            // clog.app.info(`>>>>>>>>>>> treasuryDividend ${treasuryDividend}`);
            // clog.app.info(`>>>>>>>>>>> mintedDividend ${mintedDividend}`);
            // let dividend = treasuryDividend + mintedDividend;

            holders.push(aHolder)
            dividends.push(dividend)
            if (!_.has(tmpOutDict, dividend))
                tmpOutDict[dividend] = {}
            tmpOutDict[dividend][aHolder] = [aHolder, dividend]

        });
        // in order to have unique hash for coinbase block (even created by different backers) sort it by sahres desc, addresses asc
        dividends = utils.intReverseSort(utils.arrayUnique(dividends));
        holders = holders.sort().reverse();
        for (let dividend of dividends) {
            for (let holder of holders) {
                if (_.has(tmpOutDict, dividend) && _.has(tmpOutDict[dividend], holder))
                    trx.outputs.push(tmpOutDict[dividend][holder]);
            }
        }
        trx.hash = trxHashHandler.doHashTransaction(trx)

        let block = blockTpl.getCoinbaseBlockTemplate();
        block.cycle = cycle;
        block.docs = [trx];
        let mrkl = merkleHandler.merkle([trx.hash])
        block.docsRootHash = mrkl.root;
        block.creationDate = blockCreationDate;

        return block;
    }
}
module.exports = CBHandler;
