const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const pledgeHandler = require('../../contracts/pledge-contract/pledge-handler');
const repayHandler = require('./repayments-handler');
const blockUtils = require('../block-utils');
const machine = require('../../machine/machine-handler');

let dagHandler = null;


class CoinbaseUTXOs {

    static launchImportUTXOsFromCoinbaseBlocks() {
        CoinbaseUTXOs.recursiveImportUTXOs();
    }

    static recursiveImportUTXOs() {
        if (dagHandler == null)
            dagHandler = require('../graph-handler/dag-handler');

        setTimeout(() => {
            CoinbaseUTXOs.recursiveImportUTXOs();
        }, (iConsts.getCycleByMinutes() * iConsts.ONE_MINUTE_BY_MILISECOND) / 13); // normaly less than every hour, machine checks for incomes to put in spendable area 
        let cDate = utils.getNow();
        CoinbaseUTXOs.importCoinbasedUTXOs({ cDate });
    }

    static importCoinbasedUTXOs(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        //find coinbase block with 2 cycle age old, and insert the outputs as a matured & spendable outputs to table trx_utxos
        let msg;
        let { minCreationDate, maxCreationDate } = iutils.getCbUTXOsDateRange(cDate);
        clog.trx.info(`extract maturated UTXOs range: ${minCreationDate.split(' ')[0]} (${minCreationDate.split(' ')[1]}-${maxCreationDate.split(' ')[1]})`);
        if (dagHandler == null)
            dagHandler = require('../graph-handler/dag-handler');

        let coinbases = dagHandler.searchInDAGSync({
            fields: ['b_body'],
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase],
                ['b_utxo_imported', iConsts.CONSTS.NO],
                ['b_creation_date', ['<=', maxCreationDate]],
            ]
        });

        let pledgedAccountsInfo = pledgeHandler.getPledgedAccounts({
            cDate,
            onlyActives: true
        });
        clog.app.info(`pledgedAccountsInfo: ${utils.stringify(pledgedAccountsInfo)}`);
        let pledgedAccounts = utils.objKeys(pledgedAccountsInfo);

        for (let aCBWBlock of coinbases) {
            let block = blockUtils.openDBSafeObject(aCBWBlock.bBody).content;    // do not need safe open check

            // since we examinate Coinbases from 2 cycle past, then we must be sure the entire precedents has visibility of these UTXOs
            // let decends = [block.blockHash];
            // let visibleBys = decends;
            // let i = 0;
            let wBlocks = dagHandler.walkThrough.getAllDescendents({ blockHash: block.blockHash }).wBlocks;
            clog.trx.info(`visibleBys after exclude floating signature blocks(CB): ${utils.stringify(wBlocks)}`);

            let repaymentDocs = [];
            // clog.app.info(`block.docs[0].outputs ${block.docs[0].outputs}`);
            for (let outputIndex = 0; outputIndex < block.docs[0].outputs.length; outputIndex++) {
                let anOutput = block.docs[0].outputs[outputIndex];
                let refLoc = iutils.packCoinRef(block.docs[0].hash, outputIndex.toString());

                /**
                 * if the account is pledged, so entire account incomes must be transferres to repayback transaction and 
                 * from that, cutting repayments and at the end if still remains some coins, return back to shareholder's account
                 */
                if (pledgedAccounts.includes(anOutput[0])) {
                    let aRepDoc = repayHandler.calcRepaymentDetails({
                        coinbaseTrxHash: block.docs[0].hash,
                        outputIndex,
                        coinbasedOutputValue: iutils.convertBigIntToJSInt(anOutput[1]),
                        pledgedAccountsInfo,
                        aPledgedAccount: anOutput[0]
                    });
                    clog.app.info(`cycle(${block.cycle}), repayment Doc:${utils.stringify(aRepDoc)}`);
                    repaymentDocs.push(aRepDoc);

                } else {
                    for (let wBlockEFS of wBlocks) {
                        let uArgs = {
                            cloneCode: block.cycle,
                            refLoc: refLoc,
                            creationDate: wBlockEFS.bCreationDate,
                            visibleBy: wBlockEFS.bHash,
                            address: anOutput[0],
                            value: iutils.convertBigIntToJSInt(anOutput[1]).toString(),
                            refCreationDate: block.creationDate,
                            coinbaseCycle: block.cycle
                        };
                        clog.trx.info(`insert new utxo(CB) ${JSON.stringify(uArgs)}`);
                        clog.trx.info(`--- importing Coinbase block Coins Block(${utils.hash6c(block.blockHash)}) `);
                        utxoHandler.addNewUTXO(uArgs);
                    }

                }
            }

            // if there is some cutting from income, create a new block(RpBlock) and record 
            if (repaymentDocs.length > 0) {
                repayHandler.createRepaymentBlock({
                    block,
                    repaymentDocs,
                    wBlocksEFS: wBlocks
                });
            }


            // // current block inherits the visibility of ancestors
            // // it could be first level ancestors are floating signatures(which haven't entry in trx_utxos), 
            // // so add ancestors of ancestors to be sure we keep real history in utxos
            // let ancestors = block.ancestors;
            // ancestors = utils.arrayAdd(ancestors, dagHandler.walkThrough.getAncestors(ancestors));
            // ancestors = utils.arrayUnique(ancestors);
            // utxoHandler.inheritAncestorsVisbility({
            //     ancestors: ancestors,
            //     creationDate: block.creationDate,
            //     newBlockHash: block.blockHash
            // })

            // update utxo_imported
            dagHandler.updateUtxoImported(block.blockHash, iConsts.CONSTS.YES);
        }
    }

    //TODO some uintteasts need
    // every coinbased incomes will be spendable after 2 cycle and right after starting 3rd cycle
    static calcCoinbasedOutputMaturationDate(cDate = '') {
        if (utils._nilEmptyFalse(cDate))
            cDate = utils.getNow();

        // let matureDate = Moment(t, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': iConsts.COINBASE_MATURATION_CYCLES * iConsts.getCycleByMinutes() }).format('YYYY-MM-DD HH:mm:ss');
        let matureDate = utils.minutesAfter(iConsts.COINBASE_MATURATION_CYCLES * iConsts.getCycleByMinutes(), cDate);
        matureDate = iutils.getCoinbaseRange(matureDate).from
        return matureDate
    }

}

module.exports = CoinbaseUTXOs;
