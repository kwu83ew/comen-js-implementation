const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const cbIC = require('./control-coinbase-issuance-criteria');
const doCCB = require('./do-create-coinbase-block');
const clog = require('../../loggers/console_logger');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const machine = require('../../machine/machine-handler');
const dagHandler = require('../graph-handler/dag-handler');
const missedBlocksHandler = require('../../dag/missed-blocks-handler');
const listener = require('../../plugin-handler/plugin-handler');


class CBCreator {


    /**
     * theorically the coinbase block can be created by any one,
     * and the root hash of the block could be different(because of adifferent ancesters).
     */

    static createCoinbaseBlock() {
        let cbInfo = iutils.getCoinbaseInfo({ cDate: utils.getNow() })
        listener.doCallAsync('APSH_create_coinbase_block', { cbInfo });

        let cbIssuanceCriteriaCtrlRes = cbIC.controlCoinbaseIssuanceCriteria();
        // let atLeastOneCBExists = _.has(cbIssuanceCriteriaCtrlRes, 'atLeastOneCBExists') ? cbIssuanceCriteriaCtrlRes.atLeastOneCBExists : false;
        clog.app.info(`cb Issuance Criteria Ctrl Res: ${JSON.stringify(cbIssuanceCriteriaCtrlRes)}`);
        if (!cbIssuanceCriteriaCtrlRes.canGenCB) {
            cbIssuanceCriteriaCtrlRes.coinbaseCreated = false
            return cbIssuanceCriteriaCtrlRes;
        }

        clog.cb.info(`$$$$$$$$$$ Try to Create Coinbase for Range ${cbInfo.from.split(' ')[0]} from:${cbInfo.fromHour}`);
        let { block, err } = doCCB.doGenerateCoinbaseBlock({ mode: 'generate' });
        if (utils._nilEmptyFalse(block) || (err == true)) {
            clog.cb.error(`Due to an errore, can not create a coinbase block`);
            return null
        }
        let tmpLocalConfidence = block.confidence;

        // if local machine can create a coinbase block with more confidence or ancestors, broadcast it
        let mostConfidenceInDAG = dagHandler.getMostConfidenceCoinbaseBlockFromDAG();
        let DAGHasNotCB = utils._nilEmptyFalse(mostConfidenceInDAG);
        let tmpDAGConfidence, tmpDAGAncestors;
        if (DAGHasNotCB) {
            clog.cb.info(`DAG hasn't cb for cycle ${block.cycle} fromHour:${cbInfo.fromHour})`)
            tmpDAGConfidence = 0.0
            tmpDAGAncestors = []
        } else {
            clog.cb.info(`mostConfidenceInDAG for cycle ${block.cycle} fromHour:${cbInfo.fromHour}): ${utils.stringify(mostConfidenceInDAG)}`)
            tmpDAGConfidence = _.has(mostConfidenceInDAG, 'bConfidence') ? mostConfidenceInDAG.bConfidence : 0.0
            tmpDAGAncestors = _.has(mostConfidenceInDAG, 'bAncestors') ? utils.parse(mostConfidenceInDAG.bAncestors) : []
        }

        let localHasMoreConfidenceThanDAG = (tmpDAGConfidence < tmpLocalConfidence);
        if (localHasMoreConfidenceThanDAG)
            clog.cb.info(`more confidence: local coinbase(${utils.hash6c(block.blockHash)}) has more confidence(${tmpLocalConfidence}) than DAG(${tmpDAGConfidence}) cycle:${block.cycle} from: ${cbInfo.fromHour}`)


        let tmpLocalAncestors = block.ancestors.length
        let ancDiff = utils.arrayDiff(block.ancestors, tmpDAGAncestors)
        if (ancDiff.length > 0) {
            // try to remove repayBack blocks
            let existedRpBlocks = dagHandler.searchInDAGSync({
                fields: ['b_hash'],
                query: [
                    ['b_type', iConsts.BLOCK_TYPES.RpBlock],
                    ['b_hash', ['IN', ancDiff]]
                ]
            });
            if (existedRpBlocks.length > 0)
                ancDiff = utils.arrayDiff(ancDiff, existedRpBlocks.map(x => x.bHash))
        }
        let localHasMoreAncestorsThanDAG = (ancDiff.length > 0)
        if (localHasMoreAncestorsThanDAG)
            clog.cb.info(`more ancestors: local coinbase(${utils.hash6c(block.blockHash)}) has more ancestors(${block.ancestors.map(x => utils.hash6c(x))}) than recordedCB in DAG(${tmpDAGAncestors.map(x => utils.hash6c(x))}) for ${block.cycle} fromHour:${cbInfo.fromHour}`)

        let machineSettings = machine.getMProfileSettingsSync();
        block.backer = machineSettings.backerAddress;
        // let block = blockLib.blockTruncateToBroadcast(block);
        clog.cb.info(`CoinBase block: ${JSON.stringify(block)}`);
        clog.cb.info(`missed BlocksHandler.get MissedBlocksToInvoke(): ${JSON.stringify(missedBlocksHandler.getMissedBlocksToInvoke())}`);

        // FIXME: it is a way to evoid creating too many coinbases which have a little difference because of the ancestors.
        // could it be a security issue? when an adversory in last minutes(before midnight or mid-day) starts to spam network by blocks
        // and most of nodes can not be synched, so too many coinbase blocks creating
        //  
        if ((localHasMoreConfidenceThanDAG || localHasMoreAncestorsThanDAG) &&
            (missedBlocksHandler.getMissedBlocksToInvoke().length < 1)) {

            // broadcast coin base
            if (iutils.isInCurrentCycle(block.creationDate)) {
                sendingQ.pushIntoSendingQ({
                    sqType: iConsts.BLOCK_TYPES.Coinbase,
                    sqCode: block.blockHash,
                    sqPayload: utils.stringify(block),
                    sqTitle: `coinbase block CB(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `,
                });
                clog.cb.info(`\nCoinbase issued because of clause 1 block(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour}`)
                cbIssuanceCriteriaCtrlRes.coinbasePushedTosend = true
                return cbIssuanceCriteriaCtrlRes
            }

        } else if (iutils.passedCertainTimeOfCycleToRecordInDAG() && DAGHasNotCB) {
            // another psudo random emulatore
            // if already passed more than 1/4 of cycle and still no coinbase block recorded in DAG, 
            // so the machine has to create one
            if (cbIC.haveIFirstHashedEmail('desc')) {
                sendingQ.pushIntoSendingQ({
                    sqType: iConsts.BLOCK_TYPES.Coinbase,
                    sqCode: block.blockHash,
                    sqPayload: utils.stringify(block),
                    sqTitle: `coinbase block(${utils.hash6c(block.blockHash)}) issuance cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `,
                });
                clog.cb.info(`\nCoinbase issued because of clause 2 block(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} ${iutils.getCoinbaseCycleStamp()} because of 1/4 of cycle passed`)
                cbIssuanceCriteriaCtrlRes.coinbasePushedTosend = true
                return cbIssuanceCriteriaCtrlRes
            }

        } else {
            clog.cb.info(`\nCoinbase(${utils.hash6c(block.blockHash)}) can be issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour}) 
        but local hasn't neither more confidence nor more ancestors and still not riched to 1/4 of cycle time`)

        }

        cbIssuanceCriteriaCtrlRes.coinbasePushedTosend = false
        return cbIssuanceCriteriaCtrlRes

    }




    static recursiveCreateCB() {
        setTimeout(CBCreator.recursiveCreateCB, iConsts.ONE_MINUTE_BY_MILISECOND * iutils.getCoinbaseCheckGap());
        let res = CBCreator.createCoinbaseBlock();
        clog.cb.info(`create CoinbaseBlock res: ${JSON.stringify(res)}`);
    }


    static launchCreateCB() {
        // some preparations ...
        CBCreator.recursiveCreateCB();
    }
}

module.exports = CBCreator;
