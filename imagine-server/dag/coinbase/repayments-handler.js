const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const trxHashHandler = require('../../transaction/hashable');
const merkleHandler = require('../../crypto/merkle/merkle');
const crypto = require('../../crypto/crypto');
const machine = require('../../machine/machine-handler');
const doCCB = require('./do-create-coinbase-block');

class RepaymentsHandler {

    static getRepayDocTpl() {
        let repayDoc = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.RpDoc,
            dClass: iConsts.DOC_TYPES.RpDoc,
            dVer: "0.0.0",
            cycle: "", // 'yyyy-mm-dd am' / 'yyyy-mm-dd pm'
            inputs: [],
            outputs: []
        };
        return _.clone(repayDoc);
    }

    static getRepayBlockTpl() {
        let repayBlock = {
            net: "im",
            bVer: "0.0.0",
            bType: iConsts.BLOCK_TYPES.RpBlock,
            cycle: "",
            blockLength: "0000000",
            blockHash: "0000000000000000000000000000000000000000000000000000000000000000",
            ancestors: [],
            // signals: [], // e.g. ["mimblewimble", "taproot", "schnorrMusic", "outputTimelockActivated"]
            creationDate: "",
            docsRootHash: "", // the hash root of merkle tree of transactions
            docs: [],
        };
        return _.clone(repayBlock);
    }

    static calcRepaymentDetails(args) {
        clog.cb.info(`calc RepaymentDetails args: ${utils.stringify(args)}`);
        let coinbaseTrxHash = args.coinbaseTrxHash
        let outputIndex = args.outputIndex
        let coinbasedOutputValue = args.coinbasedOutputValue
        let pledgedAccountsInfo = args.pledgedAccountsInfo
        let aPledgedAccount = args.aPledgedAccount
        let coinbaseCycle = args.coinbaseCycle

        let aRepDoc = {
            input: [coinbaseTrxHash, outputIndex.toString()],
            outputs: []
        };
        let totalIncome = coinbasedOutputValue;
        // create a repayment block cut repayment part from income 
        // repaymentInputs.push(refLoc);

        // order pledges by register date and cut by order
        let totalToBeCut = 0;
        let plgAccounsDictKeyByTime = {};
        for (let aPldg of pledgedAccountsInfo[aPledgedAccount]) {
            let key = `${aPldg.pgdActivateDate}_${aPldg.pgdHash}` // make sure for all machine we have a certain order even for same date pledge activated contract
            plgAccounsDictKeyByTime[key] = aPldg;
            totalToBeCut += aPldg.pgdRepaymentAmount;
        }
        clog.app.info(`cycle(${coinbaseCycle}) Pledged Account(${iutils.shortBech8(aPledgedAccount)}): 
        income per cycle(${utils.sepNum(totalIncome)}) mcPAIs totalToBeCut(${utils.sepNum(totalToBeCut)})`);

        // cutting
        for (let aRepayee of utils.objKeys(plgAccounsDictKeyByTime).sort()) {
            // TODO implement an efficient way to check if repayments alredy done? in this case close contract
            let toCut = iutils.convertBigIntToJSInt(plgAccounsDictKeyByTime[aRepayee].pgdRepaymentAmount);
            let repaymentAccount = plgAccounsDictKeyByTime[aRepayee].pgdPledgee;
            if (totalIncome - toCut >= 0) {
                clog.app.info(`cycle(${coinbaseCycle}) Pledged Account(${iutils.shortBech8(aPledgedAccount)}): 
                cutting ${utils.sepNum(toCut)} mcPAIs repayments to pay (${iutils.shortBech8(repaymentAccount)})`);
                // repaymentOutputs.push([repaymentAccount, toCut]);
                aRepDoc.outputs.push([repaymentAccount, toCut]);
                totalIncome = totalIncome - toCut;
            } else {
                clog.app.info(`cycle(${coinbaseCycle}) Pledged Account(${iutils.shortBech8(aPledgedAccount)}): 
                cutting ${utils.sepNum(totalIncome)} mcPAIs (remained PAIs) which not covers completely repayments to 
                (${iutils.shortBech8(repaymentAccount)})`);
                // repaymentOutputs.push([repaymentAccount, totalIncome]);
                aRepDoc.outputs.push([repaymentAccount, totalIncome]);
                totalIncome = 0;
            }
        }

        // pay what remains, to account itself
        if (totalIncome > 0) {
            clog.app.info(`cycle(${coinbaseCycle}) Pledged Account(${iutils.shortBech8(aPledgedAccount)}): 
            paying ${utils.sepNum(totalIncome)} mcPAIs to pledger after cut all repayments`);
            // repaymentOutputs.push([aPledgedAccount, totalIncome]);
            aRepDoc.outputs.push([aPledgedAccount, totalIncome]);
        }

        return aRepDoc;

    }

    static createRepaymentBlock(args) {
        clog.cb.info(`create RepaymentBlock args: ${utils.stringify(args)}`);

        let { block, repaymentDocs, wBlocksEFS } = args;

        let mapDocHashToDoc = {}
        for (let aRepay of repaymentDocs) {
            let repayDoc = this.getRepayDocTpl();
            repayDoc.cycle = block.cycle;
            repayDoc.inputs = [aRepay.input];
            repayDoc.outputs = trxHashHandler.normalizeOutputs(aRepay.outputs).normalizedOutputs;
            repayDoc.hash = trxHashHandler.doHashTransaction(repayDoc);

            mapDocHashToDoc[repayDoc.hash] = repayDoc;

        }

        let repayBlock = this.getRepayBlockTpl();
        repayBlock.cycle = block.cycle;
        repayBlock.creationDate = (iConsts.TIME_GAIN == 1) ? utils.minutesAfter(1, block.creationDate) : utils.secondsAfter(1, block.creationDate);
        repayBlock.ancestors = [block.blockHash];

        let docHashes = utils.objKeys(mapDocHashToDoc).sort(); // in order to provide unique blockHash for entire network
        repayBlock.docs = [];
        for (let aHash of docHashes) {
            repayBlock.docs.push(mapDocHashToDoc[aHash]);
        }
        repayBlock.docsRootHash = merkleHandler.merkle(docHashes).root;
        repayBlock.blockLength = iutils.offsettingLength(utils.stringify(repayBlock).length);
        repayBlock.blockHash = this.calcRpBlockHash(repayBlock);
        // console.log(`the Repayment block(${utils.hash6c(repayBlock.blockHash)}) is created \n\n${utils.stringify(repayBlock)}\n`);
        clog.cb.info(`the Repayment block(${utils.hash6c(repayBlock.blockHash)}) is created \n\n${utils.stringify(repayBlock)}\n`);

        let machineSettings = machine.getMProfileSettingsSync();

        const dagHandler = require('../graph-handler/dag-handler');
        dagHandler.addBH.addBlockToDAG({
            block: repayBlock,
            backerAddress: machineSettings.backerAddress,
            confidence: block.confidence
        });
        // immediately update imported of block
        dagHandler.updateUtxoImported(repayBlock.blockHash, iConsts.CONSTS.YES)
        dagHandler.addBH.postAddBlockToDAG({ block: repayBlock });

        // // immediately remove inputs (used refLoc)
        // for (let anInput of repayDoc.inputs) {
        //     let refLoc = iutils.packCoinRef(anInput[0], anInput[1]);
        //     utxoHandler.removeCoin({ refLoc });
        // }

        // immediately add newly created refLocs
        for (let aDoc of repayBlock.docs) {
            for (let outInx = 0; outInx < aDoc.outputs.length; outInx++) {
                let anOutput = aDoc.outputs[outInx];
                let refLoc = iutils.packCoinRef(aDoc.hash, outInx.toString());
                // immediately import newly created UTXOs
                for (let wBlockEFS of wBlocksEFS) {
                    let uArgs = {
                        cloneCode: repayBlock.cycle,
                        refLoc: refLoc,
                        creationDate: wBlockEFS.bCreationDate,
                        visibleBy: wBlockEFS.bHash,
                        address: anOutput[0],
                        value: iutils.convertBigIntToJSInt(anOutput[1]).toString(),
                        refCreationDate: repayBlock.creationDate
                    };
                    clog.trx.info(`insert new utxo(Repayment) ${JSON.stringify(uArgs)}`);
                    utxoHandler.addNewUTXO(uArgs);
                }
            }
        }
    }

    static handleReceivedRepayBlock(args) {
        return { err: false }
        // machine must create the repayments by itself!

        let msg;
        let block = args.payload;
        let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow()
        block.ancestors = blockUtils.normalizeAncestors(block.ancestors);

        // retgenerate coinbase block for given repayBlock.cycle
        let coinbaseBlock = doCCB.createCBCore({ cycle: block.cycle, mode: 'regenerate' });

        let repaymentDocs = [];
        // clog.app.info(`block.docs[0].outputs ${block.docs[0].outputs}`);
        for (let outputIndex = 0; outputIndex < block.docs[0].outputs.length; outputIndex++) {
            let anOutput = block.docs[0].outputs[outputIndex];
            let refLoc = iutils.packCoinRef(block.docs[0].hash, outputIndex.toString());

            /**
             * if the account is pledged, so entire account incomes must be transferres to repayback transaction and 
             * from that, cutting repayments and at the end if still remains some coins, return back to shareholder's account
             */
            if (pledgedAccounts.includes(anOutput[0])) {
                let aRepDoc = repayHandler.calcRepaymentDetails({
                    coinbaseTrxHash: block.docs[0].hash,
                    outputIndex,
                    coinbasedOutputValue: iutils.convertBigIntToJSInt(anOutput[1]),
                    pledgedAccountsInfo,
                    aPledgedAccount: anOutput[0]
                });
                clog.app.info(`cycle(${block.cycle}), repayment Doc:${utils.stringify(aRepDoc)}`);
                repaymentDocs.push(aRepDoc);

            } else {
                for (let wBlockEFS of wBlocksEFS) {
                    let uArgs = {
                        cloneCode: block.cycle,
                        refLoc: refLoc,
                        creationDate: wBlockEFS.bCreationDate,
                        visibleBy: wBlockEFS.bHash,
                        address: anOutput[0],
                        value: iutils.convertBigIntToJSInt(anOutput[1]).toString(),
                        refCreationDate: block.creationDate,
                        coinbaseCycle: block.cycle
                    };
                    clog.trx.info(`insert new utxo(CB) ${JSON.stringify(uArgs)}`);
                    utxoHandler.addNewUTXO(uArgs);
                }

            }
        }

    }

    static calcRpBlockHash(block) {
        // alphabetical order
        let obj = {
            ancestors: block.ancestors,
            blockLength: block.blockLength,
            bType: block.bType,
            bVer: block.dVer,
            creationDate: block.creationDate,
            docs: block.docs,
            docsRootHash: block.docsRootHash,
            net: block.net,
        }
        return iutils.doHashObject(obj);
    }


}

module.exports = RepaymentsHandler;
