const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
// const getsusVoteTemplate = require('../../docs-structure/block/sus-vote');
const DNAHandler = require('../../dna/dna-handler');
const machine = require('../../machine/machine-handler');
const crypto = require('../../crypto/crypto');
const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const suspectTrxHandler = require('../../transaction/utxo/suspect-transactions-handler');
const collisionsHandler = require('../../services/collisions/collisions-manager');



let flotingVoteVersion0 = {

    // coud be imagine test net (it) or main net (im)
    net: "im",

    bVer: "0.0.0",
    bType: iConsts.BLOCK_TYPES.FVote,
    bCat: "",
    descriptions: null,
    // coud be imagine test net (it) or main net (im)
    blockLength: "0000000", // seialized block size by byte (char). this number is also a part of block root hash

    // root hash of all doc hashes 
    // it is maked based on merkle tree root of transactions, segwits, wikis, SNSs, SSCs, DVCs, ...
    blockHash: "",
    confidence: 0.0, // dentos to signer/backer's shares by percent on signing time

    ancestors: "", // floating signatures MUST be linked to only one ancestor to whom which are signed

    voteData: {},    // the data which is voting for





    // the structure in which contain signature of shareholders(which are backers too) and 
    // by their sign, they confirm the value of shares & DAG screen-shoot on that time.
    // later this confirmation will be used in validating the existance of a "sus-block" at the time of that cycle of coinbase
    //dExtInfo.signature: {}, // there must be only one signature of block backer/signer. it contains also backerAddress(the backer/signer address in which has DNA shares)

    // a list of ancestors blocks, these ancestor's hash also recorded in block root hash
    // if a block linked to an ancestors block, it must not liks to the father of that block
    // a <--- b <---- c
    // if the block linked to b, it must not link to a 
    // the new block must be linked to as many possible as previous blocks (leave blocks)
    // maybe some discount depends on how many block you linked as an ancester!
    // when a new block linke t another block, it MUST not linked to that's block ancester


    // a list of coming feature to signaling whether the node supports or not
    signals: [], // e.g. "mimblewimble,taproot"

    // creation time timestamp, it is a part of block root-hash
    // it also used to calculate spendabality of an output. each output is not spendable befor passing 12 hours of creation time.
    // creation date must be greater than all it's ancesters
    creationDate: "", // 'yyyy-mm-dd hh:mm:ss'

};

// TODO: until missing vitality in imagine network and nodes, in case of facing the collision/dublespend/clone trxs...  
// each node publishes a fVote Block and relay the other's fVotes(by flooding to entire neighbors).
// when network been more active, propagation of these blocks will overwhelm network, 
// so we will put the block information inside a normal block as a fVote document
// in this case if block has other document to publish, will publish all in one block, 
// other wise shuld wait a while and publish fVote block efficiently.

class fVoteHandler {


    // TODO: implent fVDoc in order to create a single document for floating votes and send the vote through a block.
    // in suche a way there is not need to spamming entire network for a single vote.
    // if in time of a half cycle, the machine didn't create a normal block and still has to present her vote(maybe because machine has a valuable shares)
    // then machine can send a legacy single floating vote block
    static createFVoteBlock(args) {
        let cDate = args.cDate;
        clog.app.info(`create FVote Block args: ${utils.stringify(args)}`);
        let msg;
        let uplink = args.uplink;   // the block which we are voting for

        let { percentage } = DNAHandler.getMachineShares();
        let minShare = cnfHandler.getMinShareToAllowedIssueFVote({ cDate });
        if (percentage < minShare) {
            msg = `machine hasn't sufficient shares (${percentage} < ${minShare}) to issue a Floting vote for any collision on block (${utils.hash6c(uplink)})`;
            clog.app.info(msg);
            // TODO: push fVote info to machine buffer in order to broadcost to network in first generated block.
            return { err: true, msg };
        }

        // create floating vote block
        let fVoteBlock = _.clone(flotingVoteVersion0);
        fVoteBlock.bCat = args.bCat;
        fVoteBlock.ancestors = [uplink]; // a sus-block-vote MUST HAVE ONLY ONE ancestors whom is going to be voted
        fVoteBlock.creationDate = utils.getNow();
        fVoteBlock.signals = iutils.getMachineSignals();
        fVoteBlock.confidence = percentage;
        fVoteBlock.voteData = args.voteData;

        let signMsg = this.getSignMsgBFVote(fVoteBlock);
        let { backer, uSet, signatures } = machine.signByMachineKey({ signMsg });
        fVoteBlock.bExtInfo = { uSet, signatures };
        fVoteBlock.backer = backer;
        fVoteBlock.bExtHash = iutils.doHashObject(fVoteBlock.bExtInfo);
        fVoteBlock.blockLength = iutils.offsettingLength(utils.stringify(fVoteBlock).length);
        fVoteBlock.blockHash = fVoteHandler.clacHashBFloatingVote(fVoteBlock);
        return { err: false, block: fVoteBlock };
    }

    static clacHashBFloatingVote(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            backer: block.backer,
            bExtHash: block.bExtHash,
            blockLength: block.blockLength,
        };
        return iutils.doHashObject(hashableBlock);
    }

    static getSignMsgBFVote(block) {
        let signMsg = crypto.convertToSignMsg({
            ancestors: block.ancestors,
            bType: block.bType,
            bVer: block.bVer,
            creationDate: block.creationDate,
            confidence: block.confidence,
            net: block.net,
            voteData: block.voteData,
        });
        return signMsg;
    }

    static handleReceivedFVoteBlock(args) {
        let msg;
        let block = args.payload;
        let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow()

        clog.app.info(`\n\n******** handle Received floating Vote block(${utils.hash6c(block.blockHash)}) \n ${utils.stringify(block)}\n`);

        let validateRes = this.validateFVoteBlock(args);
        clog.trx.info(`\n\nReceived validate fVote block result: ${utils.stringify(validateRes)}\n`);
        if (validateRes.err != false) {
            // do something
            return { err: true, msg: 'Wrong fVote block ' + validateRes.msg, shouldPurgeMessage: true }
        }

        // record in dag
        clog.trx.info(`add a valid FloatingVote(${utils.hash6c(block.blockHash)} to DAG )`)
        clog.trx.info(utils.stringify(block));


        const dagHandler = require('../graph-handler/dag-handler');

        let tmpBlock = _.clone(block);
        dagHandler.addBH.addBlockToDAG({
            block: tmpBlock,
            receive_date: receive_date,
            confidence: block.confidence
        });
        dagHandler.addBH.postAddBlockToDAG({ block });

        // special treatment based on fVote Category
        switch (block.bCat) {
            case iConsts.FLOAT_BLOCKS_CATEGORIES.Trx:
                // also insert suspicious transactions
                let doubleSpends = block.voteData.doubleSpends;
                clog.trx.info(`inserting doubleSpends in db: ${utils.stringify(doubleSpends)}`);
                // console.log(`+++++++++++++++inserting doubleSpends in db: ${utils.stringify(doubleSpends)}`);
                for (let aCoin of utils.objKeys(doubleSpends)) {
                    for (let aTx of doubleSpends[aCoin]) {
                        // console.log(aTx);
                        suspectTrxHandler.addSuspectTransaction({
                            voter: block.backer,
                            voteDate: block.creationDate,
                            loggerBlock: block.blockHash,
                            coin: aCoin,
                            spenderBlock: aTx.spendBlockHash,
                            spenderDoc: aTx.spendDocHash,
                            spendDate: aTx.spendDate,
                            receiveOrder: aTx.spendOrder
                        });

                    }
                }
                break;

            case iConsts.FLOAT_BLOCKS_CATEGORIES.FleNS:
                const flensHandler = require('../../contracts/flens-contract/flens-handler');
                collisionsHandler.handleReceivedFloatingVote({ block })
                break;

        }



        // broadcast to neighbors
        if (iutils.isInCurrentCycle(block.creationDate)) {
            let pushRes = sendingQ.pushIntoSendingQ({
                sqType: block.bType,
                sqCode: block.blockHash,
                sqPayload: utils.stringify(block),
                sqTitle: `Broadcasting the confirmed bVote block(${utils.hash6c(block.blockHash)}) ${block.cycle}`
            })
            clog.app.info(`bVote pushRes: ${pushRes}`);
        }

        return { err: true, msg, shouldPurgeMessage: true };
    }

    static validateFVoteBlock(args) {
        console.log(`args argsargs ${utils.stringify(args)}`);
        let msg;
        let block = args.payload;

        let bVer = _.has(block, 'bVer') ? block.bVer : null;
        if (utils._nilEmptyFalse(bVer) || !iutils.isValidVersionNumber(bVer)) {
            msg = `missed FV block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        // control shares/confidence
        let issuerSharesPercentage = DNAHandler.getAnAddressShares(block.backer, block.creationDate).percentage;
        if (block.confidence != issuerSharesPercentage) {
            msg = `fVote(${utils.hash6c(block.blockHash)}) was rejected! because of wrong confidence(${block.confidence})!=local(${issuerSharesPercentage})`
            clog.app.error(msg);
            console.error(msg);
            return { err: true, msg }
        }

        // control signature
        let isValidUnlock = mOfNHandler.validateSigStruct({
            address: block.backer,
            uSet: block.bExtInfo.uSet
        });
        if (isValidUnlock != true) {
            msg = `Invalid given unlock structure for bVote(${utils.hash6c(block.blockHash)})`;
            clog.trx.error(msg);
            clog.sec.error(msg);
            return { err: true, msg }
        }

        let signMsg = this.getSignMsgBFVote(block);
        for (let singatureInx = 0; singatureInx < block.bExtInfo.uSet.sSets.length; singatureInx++) {
            let verifyRes = crypto.verifySignature(signMsg, block.bExtInfo.signatures[singatureInx], block.bExtInfo.uSet.sSets[singatureInx].sKey);
            if (verifyRes != true) {
                msg = `Invalid given signature for bVote(${utils.hash6c(block.blockHash)})`;
                clog.trx.error(msg);
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }

        msg = `received fVote(${utils.hash6c(block.blockHash)}) is valid`;
        clog.trx.info(msg);
        return { err: false, msg }
    }


}


module.exports = fVoteHandler;
