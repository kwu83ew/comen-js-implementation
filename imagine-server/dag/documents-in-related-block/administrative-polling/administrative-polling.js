const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
// const pollHandler = require('../../../services/polling-handler/general-poll-handler');
const listener = require('../../../plugin-handler/plugin-handler');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const crypto = require('../../../crypto/crypto')
const admPollingsHandler = require('../../../web-interface/adm-pollings/adm-pollings-handler');

class AdmPollingInRelatedBlock {

    static validateAdmPollings(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let admPollings = _.has(grpdDocuments, iConsts.DOC_TYPES.AdmPolling) ? grpdDocuments[iConsts.DOC_TYPES.AdmPolling] : [];
        if (admPollings.length == 0)
            return {
                err: false,
            }

        args.creationDate = block.creationDate;
        let res = this.controlDocs(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `blockBlock(${utils.hash6c(blockHash)}) has duplicated approved admPolling!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let anAdmPll of admPollings) {
            if (!approvedDocHashes.includes(anAdmPll.hash)) {
                msg = `blockBlock(${utils.hash6c(blockHash)}) admPolling(${utils.hash6c(anAdmPll.hash)}), adm polling is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }
    }

    static controlDocs(args) {
        clog.app.info(`control adm pollings args: ${utils.stringify(args)}`);
        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let docByHash = args.docByHash;
        let trxDict = args.trxDict;
        let block = args.block;
        let creationDate = block.creationDate;
        let blockHash = block.blockHash;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;
        let mapReferencedToReferencer = args.mapReferencedToReferencer;
        let docIndexByHash = args.docIndexByHash;

        let ApprovedDocs = [];
        let admPollings = _.has(grpdDocuments, iConsts.DOC_TYPES.AdmPolling) ? grpdDocuments[iConsts.DOC_TYPES.AdmPolling] : [];
        let pollings = _.has(grpdDocuments, iConsts.DOC_TYPES.Polling) ? grpdDocuments[iConsts.DOC_TYPES.Polling] : [];

        if (admPollings.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        if (pollings.length == 0) {
            msg = `there is no polling since block contains atleast one admPolling! ${utils.stringify(admPollings)}`;
            return {
                err: true,
                msg
            }
        }

        msg = `${stage} The NORMAL block will have ${admPollings.length} documents of adm polling `;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let anAdmPll of admPollings) {
            console.log(`\n\n ${stage} anAdmPll`, utils.stringify(anAdmPll));


            // has Valid Subject
            // TODO: do more complete checks such as pFee, pShare,longevity...
            if (!utils.objKeys(admPollingsHandler.POLLING_TYPES).includes(anAdmPll.pSubject)) {
                msg = `invalid adm polling subject(${anAdmPll.pSubject}) for ${anAdmPll.hash}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate payment trx outputs  
            let admPllCost = admPollingsHandler.docHandler.calcAdmPollingDocCost({
                cDate: creationDate,
                stage,
                doc: anAdmPll,
            });
            if (admPllCost.err != false)
                return admPllCost;

            let costPayerTrxOutputs = {}
            let costPayerTrx = trxDict[mapTrxRefToTrxHash[anAdmPll.hash]];
            costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }

            if (!_.has(costPayerTrxOutputs, 'TP_ADM_POLLING') ||
                (costPayerTrxOutputs['TP_ADM_POLLING'] < admPllCost.cost)) {
                msg = `block(${utils.hash6c(blockHash)}) referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for adm Polling costs(${utils.hash6c(anAdmPll.hash)}) 7has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_ADM_POLLING'])} mcPAIs< ${utils.sepNum(admPllCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != anAdmPll.hash) {
                msg = `block(${utils.hash6c(blockHash)}) The anAdmPll(${utils.hash6c(anAdmPll.hash)}) `;
                msg += ` cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let unlockSet, isValidUnlock;
            let mustCutExtInfoAtTheEnd = false;
            if (!_.has(anAdmPll, 'dExtInfo')) {
                mustCutExtInfoAtTheEnd = true;
                anAdmPll.dExtInfo = block.bExtInfo[docIndexByHash[anAdmPll.hash]];
            }

            // control reqRel hash
            if (anAdmPll.hash != admPollingsHandler.docHandler.calcHashDAdmPollingDoc(anAdmPll)) {
                msg = `${stage} anAdmPll.hash(${utils.hash6c(anAdmPll.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            unlockSet = anAdmPll.dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: anAdmPll.creator,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `polling anAdmPll Invalid! given creator unlock structure for (${utils.hash6c(anAdmPll.hash)}): ${utils.stringify({
                    address: anAdmPll.creator,
                    sSets: unlockSet.sSets,
                    lHash: unlockSet.lHash,
                    proofs: unlockSet.proofs,
                    salt: unlockSet.salt
                })}`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // anAdmPll signature & permission validate check
            let { signMsg } = admPollingsHandler.docHandler.getSignMsgDAdmPolling(anAdmPll);
            for (let signInx = 0; signInx < anAdmPll.dExtInfo.signatures.length; signInx++) {
                let aSignature = anAdmPll.dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (verifyRes != true) {
                        msg = `The admPolling, (${utils.hash6c(anAdmPll.hash)}) has invalid creator signature`;
                        return { err: true, msg }
                    }
                } catch (e) {
                    msg = `error in verify anAdmPll Signature (${utils.hash6c(anAdmPll.hash)}): ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg };
                }
            }

            /**
             * some control if pollingDoc is valid... 
             * indeed the main controlls for polling validate take place in polling modules (e.g. pollings-in-related-block & general-poll-handler) 
             **/
            if (!_.has(mapReferencedToReferencer, anAdmPll.hash)) {
                msg = `${stage} anAdmPll.hash(${utils.hash6c(anAdmPll.hash)}) is not referenced by any document!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let pollingDocHash = mapReferencedToReferencer[anAdmPll.hash];
            if (!_.has(docByHash, pollingDocHash)) {
                msg = `${stage} anAdmPll.hash(${utils.hash6c(anAdmPll.hash)}) referenced polling(${utils.hash6c(pollingDocHash)}) is not presented in block!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let pollingDoc = docByHash[pollingDocHash];
            if ((pollingDoc.dType != iConsts.DOC_TYPES.Polling) || (pollingDoc.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName)) {
                msg = `${stage} anAdmPll.hash(${utils.hash6c(anAdmPll.hash)}) is not referenced by any polling document! it is ${pollingDoc.dType} / ${pollingDoc.dClass}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            // TODO: maybe add creator validate!(probably it is not important)


            if (mustCutExtInfoAtTheEnd)
                delete anAdmPll.dExtInfo;

            ApprovedDocs.push({
                _doc: anAdmPll,
                _docHash: anAdmPll.hash,
                _dExtInfo: anAdmPll.dExtInfo,
                _dExtHash: anAdmPll.dExtHash
            });

        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendAdmPollingsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlDocs(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static recordAnAdmPolling(args) {
        clog.app.info(`record An Adm Polling args: ${utils.stringify(args)}`);
        admPollingsHandler.onChain.recordAnAdmPolling(args);
    }

    static removeAdmPolling(args){
    //     clog.app.info(`remove ReqRelRes args: ${utils.stringify(args)}`);
    //     admPollingsHandler.removeReqRelRes(args);
    }


}


module.exports = AdmPollingInRelatedBlock;
