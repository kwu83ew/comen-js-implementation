const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const loanHandler = require('../../../contracts/loan-contract/loan-contract');
const proposalHandler = require('../../../dna/proposal-handler/proposal-handler');
const pollHandler = require('../../../services/polling-handler/general-poll-handler');


class ProposalsInRelatedBlock {

    static validateProposals(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let proposals = _.has(grpdDocuments, iConsts.DOC_TYPES.DNAProposal) ? grpdDocuments[iConsts.DOC_TYPES.DNAProposal] : [];
        if (proposals.length == 0)
            return {
                err: false,
            }

        let res = this.controlProposals(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `blockBlock(${utils.hash6c(blockHash)}) has duplicated approved proposal!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aProp of proposals) {
            if (!approvedDocHashes.includes(aProp.hash)) {
                msg = `blockBlock(${utils.hash6c(blockHash)}) proposal(${utils.hash6c(aProp.hash)}), proposal is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }

    }

    static controlProposals(args) {

        let msg;
        let stage = args.stage;
        let block = args.block;
        let creationDate = block.creationDate;
        let grpdDocuments = args.grpdDocuments;
        let trxDict = args.trxDict;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;


        let ApprovedDocs = [];
        let proposals = _.has(grpdDocuments, iConsts.DOC_TYPES.DNAProposal) ? grpdDocuments[iConsts.DOC_TYPES.DNAProposal] : [];
        if (proposals.length == 0)
            return {
                err: false,
                ApprovedDocs
                // externalInfoHashes,
                // docsHashes,
                // block
            }


        // prepare pledges
        let pledges = _.has(grpdDocuments, iConsts.DOC_TYPES.Pledge) ? grpdDocuments[iConsts.DOC_TYPES.Pledge] : [];

        let mapProposalToPledge = {};
        let mapProposalToTrx = {};
        let pledgeDict = {};
        for (let aPledge of pledges) {
            if (aPledge.dClass == iConsts.PLEDGE_CLASSES.PledgeP) {
                pledgeDict[aPledge.hash] = aPledge;
                mapProposalToPledge[aPledge.proposalRef] = aPledge.hash;
                mapProposalToTrx[aPledge.proposalRef] = aPledge.trx;
            }
        }
        console.log('\n\n pledgeDict', utils.stringify(pledgeDict));
        console.log('\n\n mapProposalToPledge', utils.stringify(mapProposalToPledge));
        console.log('\n\n mapProposalToTrx', utils.stringify(mapProposalToTrx));

        // control if each trx is referenced byonly one pledge?
        let tmpTrxs = utils.objKeys(mapProposalToTrx).map(x => mapProposalToTrx[x]);
        if (tmpTrxs.length != utils.arrayUnique(tmpTrxs).length) {
            msg = `Pledge transactions are not unique ${utils.stringify(mapProposalToTrx)}`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }


        msg = `The NORMAL block will have ${proposals.length} documents of DNA Proposal `;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let aProposal of proposals) {
            console.log('\n\n aProposal', utils.stringify(aProposal));

            // control document hash
            if (aProposal.hash != proposalHandler.doHashProposal(aProposal)) {
                msg = `aProposal.hash(${utils.hash6c(aProposal.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // int help level control
            if (aProposal.helpLevel != Math.trunc(aProposal.helpLevel)) {
                msg = `${stage} proposal(${utils.hash6c(aProposal.hash)}) the help level is not integer!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate voting logevity
            // because of test ambient the longivity can be float and les than 1 hour
            aProposal.votingLongevity = (iConsts.TIME_GAIN == 1) ? parseInt(aProposal.votingLongevity) : parseFloat(aProposal.votingLongevity);
            if (aProposal.votingLongevity < iConsts.getMinVotingLongevity()) {
                msg = `voting Longevity(${aProposal.votingLongevity}) is less than minimum voting longivity(${iConsts.getMinVotingLongevity()})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate voting class
            if (aProposal.pollingProfile != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
                msg = `proposal polling class (${aProposal.pollingProfile}) is not supported yet!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate helpHours
            if (parseInt(aProposal.helpHours) < 0) {
                msg = `proposal helpHours(${aProposal.helpHours}) is less than Zero!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate helpLevel
            if ((parseInt(aProposal.helpLevel) < 0) || (parseInt(aProposal.helpLevel) > 12)) {
                msg = `proposal helpLevel(${aProposal.helpLevel}) is invalid!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // control proposal cost & if exist a trx to pay this cost
            let proposalDocumetnCost = proposalHandler.calcProposalDocumetnCost({
                cDate: creationDate,
                proposal: aProposal,
                stage
            });
            if (proposalDocumetnCost.err != false)
                return proposalDocumetnCost;

            let { oneCycleIncome, applyCost } = proposalHandler.calcProposalApplyCost({
                cCDate: creationDate,
                helpHours: aProposal.helpHours,
                helpLevel: aProposal.helpLevel,
            });
            clog.app.info(`the proposal(${utils.hash6c(aProposal.hash)}) oneCycleIncome(${oneCycleIncome}) proposalDocumetnCost(${proposalDocumetnCost.cost}) + applyCost(${applyCost})`);

            // control if proposal is fund by a pledge contract or by an standalon transaction?
            if (_.has(mapProposalToPledge, aProposal.hash)) {
                // proposal costs is payed by pledgeing potential incomes and lending from pledgee.

                let costPayerTrx = trxDict[mapProposalToTrx[aProposal.hash]];
                let costPayerTrxOutputs = {}
                for (let anOutput of costPayerTrx.outputs) {
                    costPayerTrxOutputs[anOutput[0]] = anOutput[1];
                }


                // control if the trx of doc cost payment is valid
                let refPledge = pledgeDict[mapProposalToPledge[aProposal.hash]];
                if (refPledge.pledger != aProposal.shareholder) {
                    msg = `The Proposal income address(${iutils.shortBech8(aProposal.shareholder)}) is different from pledged one(${iutils.shortBech8(refPledge.pledger)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                if (refPledge.redeemTerms.principal < applyCost + proposalDocumetnCost.cost) {
                    msg = `The Proposal applyCost(${utils.microPAIToPAI6(applyCost)}) + proposalDocumetnCost.cost(${utils.microPAIToPAI6(proposalDocumetnCost.cost)}), is more than requested loan(${utils.microPAIToPAI6(refPledge.redeemTerms.principal)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                if (refPledge.redeemTerms.repaymentAmount > oneCycleIncome) {
                    msg = `The Repayment value(${utils.microPAIToPAI6(pledge.redeemTerms.repaymentAmount)}) is bigger than one cycle income(${utils.microPAIToPAI6(oneCycleIncome)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                if (refPledge.redeemTerms.annualInterest < 0) {
                    msg = `The annualInterest(${refPledge.redeemTerms.annualInterest}) is less than zero!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                if (refPledge.redeemTerms.repaymentSchedule < 0) {
                    msg = `The repaymentSchedule(${refPledge.redeemTerms.repaymentSchedule}) is less than zero!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                let loanDtl = loanHandler.calcLoanRepayments({
                    principal: refPledge.redeemTerms.principal,
                    annualInterest: refPledge.redeemTerms.annualInterest,
                    repaymentAmount: refPledge.redeemTerms.repaymentAmount,
                    repaymentSchedule: refPledge.redeemTerms.repaymentSchedule
                });
                if (loanDtl.err != false) {
                    return loanDtl;
                }


                // control if pledge trx is valid
                if (!_.has(mapProposalToTrx, aProposal.hash)) {
                    msg = `No Trx pays Proposal costs(${utils.hash6c(aProposal.hash)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                if (!_.has(trxDict, mapProposalToTrx[aProposal.hash])) {
                    msg = `referenced Trx(${utils.hash6c(mapProposalToTrx[aProposal.hash])}) for Proposal costs(${utils.hash6c(aProposal.hash)}) does not exist in block!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                // validate payment trx
                if (costPayerTrx.ref != aProposal.hash) {
                    msg = `referenced Trx(${utils.hash6c(mapProposalToTrx[aProposal.hash])}) for Proposal costs(${utils.hash6c(aProposal.hash)}) has different payment ref(${utils.hash6c(costPayerTrx.ref)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                // validate payment trx outputs  
                if (!_.has(costPayerTrxOutputs, 'TP_PROPOSAL') ||
                    (costPayerTrxOutputs['TP_PROPOSAL'] < applyCost + proposalDocumetnCost.cost)) {
                    msg = `referenced Trx(${utils.hash6c(mapProposalToTrx[aProposal.hash])}) for Proposal costs(${utils.hash6c(aProposal.hash)}) doesn't cover all proposal's costs!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                ApprovedDocs.push({
                    _doc: aProposal,
                    _docHash: aProposal.hash,
                    _dExtInfo: 'NOE',
                    _dExtHash: 'NOE'    //aProposal.hash    // since proposals haven't ext Info su as ext InfoHash, we use same as doc hash
                });


            } else {
                // must find the transaction that pays the cost
                // TODO: implement it ASAP
                let aTrxPaysProposalCosts = true;

                let costPayerTrx = trxDict[mapTrxRefToTrxHash[aProposal.hash]];
                let costPayerTrxOutputs = {}
                for (let anOutput of costPayerTrx.outputs) {
                    costPayerTrxOutputs[anOutput[0]] = anOutput[1];
                }


                // validate payment trx outputs  
                if ((!_.has(costPayerTrxOutputs, 'TP_PROPOSAL')) ||
                    (costPayerTrxOutputs['TP_PROPOSAL'] < applyCost + proposalDocumetnCost.cost)) {
                    msg = `${stage}, proposer paying costs, referenced Trx(${utils.hash6c(mapTrxRefToTrxHash[aProposal.hash])}) for Proposal costs(${utils.hash6c(aProposal.hash)}) doesn't cover all proposal's costs!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }


                // if (!aTrxPaysProposalCosts) {
                //     msg = `${stage}, No one pays Proposal costs(${utils.hash6c(aProposal.hash)})!`;
                //     clog.sec.error(msg);
                //     return { err: true, msg, shouldPurgeMessage: true }
                // }

                ApprovedDocs.push({
                    _doc: aProposal,
                    _docHash: aProposal.hash,
                    _dExtInfo: 'NOE',
                    _dExtHash: 'NOE'    // aProposal.hash    // since proposals haven't ext Info su as ext InfoHash, we use same as doc hash
                });


            }

        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendProposalsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlProposals(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                block.bExtInfo.push(aDoc._dExtInfo);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static activatePollingForProposal(args) {
        clog.app.info(`activate Proposal Polling args: ${utils.stringify(args)}`);
        // record in i_proposals (i_proposal)
        proposalHandler.recordANewProposal(args);

        // create a new polling
        let block = args.block;
        let proposal = args.proposal;
        pollHandler.autoCreatePollingForProposal({
            dType: iConsts.DOC_TYPES.Polling,
            dClass: proposal.pollingProfile, // default is iConsts.POLLING_PROFILE_CLASSES.Basic.ppName
            pllVersion: proposal.dVer,

            ref: proposal.hash,
            refType: iConsts.POLLING_REF_TYPE.Proposal,
            refClass: iConsts.PLEDGE_CLASSES.PledgeP,

            startDate: block.creationDate,
            longevity: proposal.votingLongevity,
            creator: proposal.shareholder,
            comment: `Polling for proposal(${utils.hash6c(proposal.hash)}), ${proposal.title} `,
        });

    }

    /**
     * 
     * @param {*} proposal 
     * if the payment for proposal costs or pledge(in case of pledgeP) has problem(double spend or ...) the activated proposal must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeProposal(proposalHash) {
        clog.app.error(`proposals in related block: removeProposal(${proposalHash}`);
        proposalHandler.removeProposal(proposalHash);

    }

}


module.exports = ProposalsInRelatedBlock;
