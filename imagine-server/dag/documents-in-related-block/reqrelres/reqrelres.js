
const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const reservedHandler = require('../../coinbase/reserved-coins-handler');
// const pollHandler = require('../../../services/polling-handler/general-poll-handler');
const listener = require('../../../plugin-handler/plugin-handler');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const crypto = require('../../../crypto/crypto')

class ReqRelResInRelatedBlock {

    static validateReqRelRess(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let reqRels = _.has(grpdDocuments, iConsts.DOC_TYPES.ReqForRelRes) ? grpdDocuments[iConsts.DOC_TYPES.ReqForRelRes] : [];
        if (reqRels.length == 0)
            return {
                err: false,
            }

        args.creationDate = block.creationDate;
        let res = this.controlReqRelRes(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `blockBlock(${utils.hash6c(blockHash)}) has duplicated approved reqRelRes!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aReqRel of reqRels) {
            if (!approvedDocHashes.includes(aReqRel.hash)) {
                msg = `blockBlock(${utils.hash6c(blockHash)}) release coins(${utils.hash6c(aReqRel.hash)}), req res is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }
    }

    static controlReqRelRes(args) {
        clog.app.info(`control Req Rel Res args: ${utils.stringify(args)}`);
        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let docByHash = args.docByHash;
        let trxDict = args.trxDict;
        let block = args.block;
        let creationDate = block.creationDate;
        let blockHash = block.blockHash;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;
        let mapReferencedToReferencer = args.mapReferencedToReferencer;
        let docIndexByHash = args.docIndexByHash;

        let ApprovedDocs = [];
        let reqRels = _.has(grpdDocuments, iConsts.DOC_TYPES.ReqForRelRes) ? grpdDocuments[iConsts.DOC_TYPES.ReqForRelRes] : [];
        let pollings = _.has(grpdDocuments, iConsts.DOC_TYPES.Polling) ? grpdDocuments[iConsts.DOC_TYPES.Polling] : [];

        if (reqRels.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        if (pollings.length == 0) {
            msg = `there is no polling since block contains atleast one reqForRelReserved coins! ${utils.stringify(reqRels)}`;
            return {
                err: true,
                msg
            }
        }

        // // let pollingsDict = {};
        // // let mapPollingRefToPolling = {};
        // for (let aPoll of pollings) {
        //     // pollingsDict[aPoll.hash] = aPoll;
        //     // mapPollingRefToPolling[aPoll.ref] = aPoll;
        // }

        msg = `${stage} The NORMAL block will have ${reqRels.length} documents of Requet For Release Reserved Coins `;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let aReqRel of reqRels) {
            console.log(`\n\n ${stage} aReqRel`, utils.stringify(aReqRel));

            // is releaseable?
            let reserveNumber = parseInt(aReqRel.reserveNumber); // it could be 1,2,3 or 4
            if ((reserveNumber < 0) || (reserveNumber > 4))
                return { err: true, msg: `${stage} Invalide reserve number(${reserveNumber})` };
            // reserveNumber = reserveNumber.toString();
            let reservesDtl = reservedHandler.getReservesDetails({
                blockHashes: [aReqRel.eyeBlock]
            });
            clog.app.info(`${stage} rel Res Dtl ${utils.stringify(reservesDtl)}`);
            if (!_.has(reservesDtl.reserves, aReqRel.eyeBlock)) {
                return {
                    err: true,
                    msg: `${stage} customized Validation eyeblock does not exist2! The eye block(${utils.hash6c(aReqRel.eyeBlock)}) `
                }
            }

            if (!_.has(reservesDtl.reserves[aReqRel.eyeBlock].reservesInfo, reserveNumber)) {
                return {
                    err: true,
                    msg: `${stage} customized Validation reserve number invalid! The eye block(${utils.hash6c(aReqRel.eyeBlock)}) reserve number(${reserveNumber}) `
                }
            }
            let reserveFullInfo = reservesDtl.reserves[aReqRel.eyeBlock];
            if (reserveFullInfo.reservesInfo[reserveNumber].isReleasable != iConsts.CONSTS.YES) {
                msg = `${stage} is not releaseable! ${utils.stringify(reserveFullInfo.reservesInfo[reserveNumber])}`;
                return { err: true, msg };
            }


            // validate payment trx outputs  
            let reqRelCost = reservedHandler.calcReqRelCost({
                cDate: creationDate,
                stage,
                reqRelDoc: aReqRel,
            });
            if (reqRelCost.err != false)
                return reqRelCost;

            let costPayerTrxOutputs = {}
            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aReqRel.hash]];
            costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }

            if (!_.has(costPayerTrxOutputs, 'TP_REQRELRES') ||
                (costPayerTrxOutputs['TP_REQRELRES'] < reqRelCost.cost)) {
                msg = `block(${utils.hash6c(blockHash)}) referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for ReqForRelRes costs(${utils.hash6c(aReqRel.hash)}) 8has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_REQRELRES'])} mcPAIs< ${utils.sepNum(reqRelCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aReqRel.hash) {
                msg = `block(${utils.hash6c(blockHash)}) The aReqRel(${utils.hash6c(aReqRel.hash)}) `;
                msg += ` cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let unlockSet, isValidUnlock, signMsg;
            let mustCutExtInfoAtTheEnd = false;
            if (!_.has(aReqRel, 'dExtInfo')) {
                mustCutExtInfoAtTheEnd = true;
                aReqRel.dExtInfo = block.bExtInfo[docIndexByHash[aReqRel.hash]];
            }

            // control reqRel hash
            if (aReqRel.hash != reservedHandler.calcHashDReqRelDoc(aReqRel)) {
                msg = `${stage} aReqRel.hash(${utils.hash6c(aReqRel.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            unlockSet = aReqRel.dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aReqRel.creator,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `polling aReqRel Invalid! given creator unlock structure for (${utils.hash6c(aReqRel.hash)}): ${utils.stringify({
                    address: aReqRel.creator,
                    sSets: unlockSet.sSets,
                    lHash: unlockSet.lHash,
                    proofs: unlockSet.proofs,
                    salt: unlockSet.salt
                })}`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // aReqRel signature & permission validate check
            signMsg = reservedHandler.getSignMsgDReqRel(aReqRel);
            clog.app.info(`going to verify signMsg ${signMsg}`);
            for (let signInx = 0; signInx < aReqRel.dExtInfo.signatures.length; signInx++) {
                let aSignature = aReqRel.dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (verifyRes != true) {
                        msg = `The ReqRel, (${utils.hash6c(aReqRel.hash)}) has invalid creator signature`;
                        return { err: true, msg }
                    }
                } catch (e) {
                    msg = `error in verify aReqRel Signature (${utils.hash6c(aReqRel.hash)}): ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg };
                }
            }

            /**
             * some control if pollingDoc is valid... 
             * indeed the main controlls for polling validate take place in polling modules (e.g. pollings-in-related-block & general-poll-handler) 
             **/
            if (!_.has(mapReferencedToReferencer, aReqRel.hash)) {
                msg = `${stage} aReqRel.hash(${utils.hash6c(aReqRel.hash)}) is not referenced by any document!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let pollingDocHash = mapReferencedToReferencer[aReqRel.hash];
            if (!_.has(docByHash, pollingDocHash)) {
                msg = `${stage} aReqRel.hash(${utils.hash6c(aReqRel.hash)}) referenced polling(${utils.hash6c(pollingDocHash)}) is not presented in block!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let pollingDoc = docByHash[pollingDocHash];
            if ((pollingDoc.dType != iConsts.DOC_TYPES.Polling) || (pollingDoc.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName)) {
                msg = `${stage} aReqRel.hash(${utils.hash6c(aReqRel.hash)}) is not referenced by any polling document! it is ${pollingDoc.dType} / ${pollingDoc.dClass}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            // TODO: maybe add creator validate!(probably it is not important)


            if (mustCutExtInfoAtTheEnd)
                delete aReqRel.dExtInfo;

            ApprovedDocs.push({
                _doc: aReqRel,
                _docHash: aReqRel.hash,
                _dExtInfo: aReqRel.dExtInfo,
                _dExtHash: aReqRel.dExtHash
            });

        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendReqRelResToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlReqRelRes(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static recordAReqRelRes(args) {
        clog.app.info(`record AReqRelRes args: ${utils.stringify(args)}`);
        reservedHandler.recordAReqRelRes(args);
    }

    static removeReqRelRes(args) {
        clog.app.info(`remove ReqRelRes args: ${utils.stringify(args)}`);
        reservedHandler.removeReqRelRes(args);
    }


}


module.exports = ReqRelResInRelatedBlock;
