const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const pollHandler = require('../../../services/polling-handler/general-poll-handler');
const listener = require('../../../plugin-handler/plugin-handler');
const DNAHandler = require('../../../dna/dna-handler');

class PollingsInRelatedBlock {

    static validatePollings(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let pollings = _.has(grpdDocuments, iConsts.DOC_TYPES.Polling) ? grpdDocuments[iConsts.DOC_TYPES.Polling] : [];
        if (pollings.length == 0)
            return {
                err: false,
            }

        args.creationDate = block.creationDate;
        let res = this.controlPollings(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `blockBlock(${utils.hash6c(blockHash)}) has duplicated approved proposal!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aPoll of pollings) {
            if (!approvedDocHashes.includes(aPoll.hash)) {
                msg = `blockBlock(${utils.hash6c(blockHash)}) polling(${utils.hash6c(aPoll.hash)}), polling is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }
    }

    static controlPollings(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let blockHash = block.blockHash;
        let creationDate = block.creationDate;
        let docByHash = args.docByHash;
        let trxDict = args.trxDict;
        let docIndexByHash = args.docIndexByHash;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;
        let mapReferencerToReferenced = args.mapReferencerToReferenced;


        let ApprovedDocs = [];
        let pollings = _.has(grpdDocuments, iConsts.DOC_TYPES.Polling) ? grpdDocuments[iConsts.DOC_TYPES.Polling] : [];
        if (pollings.length == 0)
            return {
                err: false,
                ApprovedDocs
            }


        // this mappings already done in retrieveAndGroupBufferedDocuments
        // let mapSubjectToPolling = {};
        // for (let aPolling of pollings) {
        //     if (aPolling.dClass == iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
        //         mapReferencerToReferenced[aPolling.hash] = aPolling.ref;
        //         mapSubjectToPolling[aPolling.ref] = aPolling.hash;
        //     }
        // }

        // control if all pollings are payed by a transaction
        let pollingHashes = utils.objKeys(mapReferencerToReferenced);
        let trxRefHashes = utils.objKeys(mapTrxRefToTrxHash);
        if (utils.arrayDiff(pollingHashes, trxRefHashes).length > 0) {
            msg = `There are some pollings which are not payed by transaction pollingHashes: ${utils.stringify(pollingHashes)} trxRefHashes: ${utils.stringify(trxRefHashes)}`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }


        msg = `The NORMAL block will have ${pollings.length} documents of Polling `;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let aPoll of pollings) {
            console.log('\n\n aPoll', utils.stringify(aPoll));

            let referencedDocHash = mapReferencerToReferenced[aPoll.hash];
            let referencedDoc = docByHash[referencedDocHash];
            let dExtInfo;
            if (_.has(aPoll, 'dExtInfo')) {
                dExtInfo = aPoll.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aPoll.hash]];
            }

            // control polling hash
            if (aPoll.hash != pollHandler.calcHashDPolling(aPoll)) {
                msg = `aPoll.hash(${utils.hash6c(aPoll.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // length control already done in document-groupping-phase

            // validate voting logevity
            // because of test ambient the longivity can be float and les than 1 hour
            aPoll.votingLongevity = (iConsts.TIME_GAIN == 1) ? parseInt(aPoll.votingLongevity) : parseFloat(aPoll.votingLongevity);
            aPoll.votingLongevity = pollHandler.normalizeVotingLongevity(aPoll.votingLongevity);
            if (aPoll.votingLongevity < iConsts.getMinVotingLongevity()) {
                msg = `voting Longevity(${aPoll.votingLongevity}) is less than minimum voting longivity(${iConsts.getMinVotingLongevity()})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate voting class
            if (aPoll.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
                msg = `THe polling class (${aPoll.dClass}) is not supported yet!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let potentialVotersCount;

            listener.doCallSync('SASH_custom_validate_polling', { polling: aPoll });

            // validate the document which is refered by polling document(if exist) & also validate the polling cost (based on polling itsefl and eventually related docs)
            switch (aPoll.refType) {
                case iConsts.POLLING_REF_TYPE.Proposal:
                    // maybe some controls!
                    break;

                case iConsts.POLLING_REF_TYPE.AdmPolling:
                    let { holdersByKey } = DNAHandler.getSharesInfo();
                    potentialVotersCount = utils.objKeys(holdersByKey).length;
                    break;

                case iConsts.POLLING_REF_TYPE.ReqForRelRes:

                    const reservedHandler = require('../../coinbase/reserved-coins-handler');
                    let potentialVotersInfo = reservedHandler.calcPotentialVotersCount(referencedDoc.eyeBlock, referencedDoc.reserveNumber);
                    clog.app.info(`potentialVotersInfo ${utils.stringify(potentialVotersInfo)}`);
                    if (potentialVotersInfo.err != false)
                        return potentialVotersInfo;
                    potentialVotersCount = potentialVotersInfo.votersCount;

                    // maybe some controls for POLLING and not for release-request-document!

                    // let cusValRes = reservedHandler.customizedValidation({
                    //     dExtInfo,
                    //     costPayerTrx: trxDict[mapTrxRefToTrxHash[aPoll.hash]],
                    //     polling: aPoll,
                    //     stage,
                    //     docByHash
                    // });
                    // if (cusValRes.err != false)
                    //     return cusValRes;
                    break;

            }

            /**
             * polling costs control which is applyable for all non "iConsts.POLLING_REF_TYPE.Proposal" pollings
             */
            if (aPoll.refType != iConsts.POLLING_REF_TYPE.Proposal) {
                let costPayerTrx = trxDict[mapTrxRefToTrxHash[aPoll.hash]];
                console.log('costPayerTrx costPayerTrx');
                console.log(costPayerTrx);
                console.log('costPayerTrx costPayerTrx');
                let costPayerTrxOutputs = {}
                for (let anOutput of costPayerTrx.outputs) {
                    costPayerTrxOutputs[anOutput[0]] = anOutput[1];
                }

                // control if the trx of doc cost payment is valid
                if (costPayerTrx.ref != aPoll.hash) {
                    msg = `block(${utils.hash6c(blockHash)}) The polling(${utils.hash6c(aPoll.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                let pollingCost = pollHandler.calcPollingCost({
                    cDate: creationDate,
                    stage,
                    polling: aPoll,
                    voters: potentialVotersCount
                });
                if (pollingCost.err != false)
                    return pollingCost;
                clog.app.info(`pollingCost pollingCost pollingCost ${utils.stringify(pollingCost)}`);

                // validate payment trx outputs  
                if (!_.has(costPayerTrxOutputs, 'TP_POLLING') ||
                    (costPayerTrxOutputs['TP_POLLING'] < pollingCost.cost)) {
                    msg = `block(${utils.hash6c(blockHash)}) referenced Trx(${utils.hash6c(costPayerTrx.hash)}) `
                    msg += `for polling costs(${utils.hash6c(aPoll.hash)}) `;
                    msg += `1has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_POLLING'])} mcPAIs< ${utils.sepNum(pollingCost.cost)} mcPAIs`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

            }

            ApprovedDocs.push({
                _doc: aPoll,
                _docHash: aPoll.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aPoll.dExtHash
            });

        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendPollingsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlPollings(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static recordAPolling(args) {
        // create a new polling
        return pollHandler.recordPollingInDB(args);
    }




    /**
     * 
     * @param {*} polling
     * if the payment for ballot costs has problem(double spend or ...) the recorded ballot must be removed from DB
     * this kind of errors are discovered in importing utxo step(12 hours after recording ballot in DAG & local database)
     */
    static removePollingInRelBlock(args) {
        clog.app.error(`remove Polling(${utils.hash6c(args)}) because of failed cost payment `);
        pollHandler.removePollingG({pollingHash: args.pollingHash});

    }

}


module.exports = PollingsInRelatedBlock;
