const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const flensHandler = require('../../../contracts/flens-contract/flens-handler');
const sendingQ = require('../../../services/sending-q-manager/sending-q-manager')
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');

// const flensHandler = require('../../services/polling-handler/general-poll-handler');

class INameBindsInRelatedBlock {

    static recordINameBindInDAG(args) {

        let block = args.block;
        let bindDoc = args.bindDoc;


        clog.app.info(`record A bindDoc(${utils.hash6c(bindDoc.hash)}) `);
        let nowT = utils.getNow();
        flensHandler.binder.recordAbindDoc({ bindDoc });
    }

    static validateINameBinds(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let iNames = _.has(grpdDocuments, iConsts.DOC_TYPES.INameBind) ? grpdDocuments[iConsts.DOC_TYPES.INameBind] : [];
        if (iNames.length == 0)
            return {
                err: false,
            }

        let res = this.controlINameBinds(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved iName-binds!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aBindDoc of iNames) {
            if (!approvedDocHashes.includes(aBindDoc.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) IName-bind(${utils.hash6c(aBindDoc.hash)}), IName is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }


        return {
            err: false,
        }

    }

    static controlINameBinds(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;

        let trxDict = args.trxDict;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;

        let ApprovedDocs = [];

        let bindDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.INameBind) ? grpdDocuments[iConsts.DOC_TYPES.INameBind] : [];
        if (bindDocs.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The NORMAL block have ${bindDocs.length} Bind-iName`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let aBindDoc of bindDocs) {
            console.log('\n\n aBindDoc', utils.stringify(aBindDoc));

            let unlockSet, isValidUnlock, signMsg;

            let dExtInfo;
            if (_.has(aBindDoc, 'dExtInfo')) {
                dExtInfo = aBindDoc.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aBindDoc.hash]];
            }

            // control document hash
            if (aBindDoc.hash != flensHandler.binder.calcHashBBindReqDoc(aBindDoc)) {
                msg = `aBindDoc.hash(${utils.hash6c(aBindDoc.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate owner
            if (!crypto.bech32_isValidAddress(aBindDoc.nbInfo.iPNOwner)) {
                msg = `invalid owner(${iutils.shortBech8(aBindDoc.nbInfo.iPNOwner)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            unlockSet = dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aBindDoc.nbInfo.iPNOwner,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `iName-bind req, Invalid! given unlock structure for req, unlock:${utils.stringify({
                    address: aBindDoc.nbInfo.iPNOwner,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // bind signature & permission validate check
            signMsg = flensHandler.binder.getSignMsgDBindReq(aBindDoc);
            for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
                let aSignature = dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the FleNSBind(${aBindDoc.hash}) has invalid signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify FleNS-bind Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
            }

            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aBindDoc.hash]];
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aBindDoc.hash) {
                msg = `The iName-blind(${utils.hash6c(aBindDoc.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // calculate doc register request cost
            let bindCost = flensHandler.binder.calcINameBindCost({
                cDate: creationDate,
                stage,
                bReqDoc: aBindDoc
            });
            if (bindCost.err != false)
                return bindCost;

            // validate payment trx outputs  
            if (!_.has(costPayerTrxOutputs, 'TP_INAME_BIND') ||
                (costPayerTrxOutputs['TP_INAME_BIND'] < bindCost.cost)) {
                msg = `referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for bind iName costs(${utils.hash6c(aBindDoc.hash)}) 5has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_INAME_BIND'])} mcPAIs< ${utils.sepNum(bindCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            ApprovedDocs.push({
                _doc: aBindDoc,
                _docHash: aBindDoc.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aBindDoc.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendINameBindsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlINameBinds(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }




    /**
     * 
     * @param {*} iName 
     * if the payment for iName costs has problem(double spend or ...) the activated iName must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeBindingBecauseOfPaymentsFail(bindHash) {
        clog.app.error(`removebindDoc(${bindHash}`);
        return flensHandler.binder.removeBind(bindHash);
    }

}


module.exports = INameBindsInRelatedBlock;
