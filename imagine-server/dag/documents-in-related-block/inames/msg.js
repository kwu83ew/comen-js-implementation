const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const flensHandler = require('../../../contracts/flens-contract/flens-handler');
const sendingQ = require('../../../services/sending-q-manager/sending-q-manager')
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');

// const flensHandler = require('../../services/polling-handler/general-poll-handler');

class INameMsgsInRelatedBlock {

    static recordINameMsgInMachine(args) {

        let block = args.block;
        let msgDoc = args.msgDoc;


        clog.app.info(`record A msgDoc(${utils.hash6c(msgDoc.hash)}) `);
        let nowT = utils.getNow();
        // control if receiver/sender is local machine(for all mProfiles)
        let machineControlledINames = flensHandler.getMachineControlledINames();
        clog.app.info(`record INameMsgInMachine-machine ControlledINames: ${utils.stringify(machineControlledINames)}`);
        machineControlledINames = machineControlledINames.records.map(x => x.imn_hash);
        if (machineControlledINames.includes(msgDoc.sINHash) || machineControlledINames.includes(msgDoc.rINHash)) {
            let localRes = flensHandler.msg.recordAmsgDocInLocal({ block, msgDoc });
            if (localRes.err != false)
                clog.app.error(`record A msg Doc In Local Res: ${utils.stringify(localRes)}`);
        }
    }

    static validateINameMsgs(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let iNames = _.has(grpdDocuments, iConsts.DOC_TYPES.INameMsgTo) ? grpdDocuments[iConsts.DOC_TYPES.INameMsgTo] : [];
        if (iNames.length == 0)
            return {
                err: false,
            }

        let res = this.controlMsgs(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved iName-msgs!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aMsgDoc of iNames) {
            if (!approvedDocHashes.includes(aMsgDoc.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) IName-bind(${utils.hash6c(aMsgDoc.hash)}), IName is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }
    }

    static controlMsgs(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;

        let trxDict = args.trxDict;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;

        let ApprovedDocs = [];

        let msgDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.INameMsgTo) ? grpdDocuments[iConsts.DOC_TYPES.INameMsgTo] : [];
        if (msgDocs.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The NORMAL block have ${msgDocs.length} Msg-to-iName`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        for (let aMsgDoc of msgDocs) {
            console.log('\n\n aMsgDoc', utils.stringify(aMsgDoc));

            // control doc hash
            if (aMsgDoc.hash != flensHandler.msg.calcMsgDocHash(aMsgDoc)) {
                msg = `aMsgDoc.hash(${utils.hash6c(aMsgDoc.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aMsgDoc.hash]];
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aMsgDoc.hash) {
                msg = `The iName-blind(${utils.hash6c(aMsgDoc.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // calculate doc register request cost
            let msgCost = flensHandler.msg.calcMsgCost({
                cDate: creationDate,
                stage,
                msgDoc: aMsgDoc
            });
            if (msgCost.err != false)
                return msgCost;

            // validate payment trx outputs  
            if (!_.has(costPayerTrxOutputs, 'TP_INAME_MSG') ||
                (costPayerTrxOutputs['TP_INAME_MSG'] < msgCost.cost)) {
                msg = `referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for msg-to-iName costs(${utils.hash6c(aMsgDoc.hash)}) `;
                msg += `6has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${costPayerTrxOutputs['TP_INAME_MSG']} mcPAIs< ${msgCost.cost} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            ApprovedDocs.push({
                _doc: aMsgDoc,
                _docHash: aMsgDoc.hash,
                _dExtInfo: aMsgDoc.dExtInfo,
                _dExtHash: aMsgDoc.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendINameMsgsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlMsgs(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }




    /**
     * 
     * @param {*} iName 
     * if the payment for iName costs has problem(double spend or ...) the activated iName must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeMsgBecauseOfPaymentsFail(docHash) {
        clog.app.error(`removeMsgDoc(${docHash}`);
        return flensHandler.msg.removeMsg(docHash);
    }

}


module.exports = INameMsgsInRelatedBlock;
