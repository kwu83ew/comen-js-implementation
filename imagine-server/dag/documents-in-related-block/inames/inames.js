const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const flensHandler = require('../../../contracts/flens-contract/flens-handler');
const collisionsHandler = require('../../../services/collisions/collisions-manager');
const sendingQ = require('../../../services/sending-q-manager/sending-q-manager')
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const machine = require('../../../machine/machine-handler');

// const flensHandler = require('../../services/polling-handler/general-poll-handler');

class INamesInRelatedBlock {

    static recordINameInDAG(args) {
        clog.app.info(`record INameInDAG args: ${utils.stringify(args)}`);

        const dagHandler = require('../../graph-handler/dag-handler');
        let msg;
        let block = args.block;
        let flens = args.flens;


        let alreadyRecordedDoc = flensHandler.register.alreadyRegistered(flens.iName);
        if (!alreadyRecordedDoc.isRegistred) {
            let insertRes = flensHandler.register._recordINameInDAG(args);
            if (insertRes.err != false)
                return insertRes;


        } else {
            let clCollisionRef = flensHandler.generateINameHash(flens.iName);
            msg = `collision the iName(${flens.iName}), iNameHash(${utils.hash6c(clCollisionRef)}) 
            already placed in DAG! ${utils.stringify(alreadyRecordedDoc)}`;
            clog.app.error(msg);

            let machineSettings = machine.getMProfileSettingsSync();

            // first controls if the already recorded iName in table iname_records is logged too?
            let dbl = collisionsHandler.getLoggedDocs(alreadyRecordedDoc.inDocHash);
            if (dbl.length == 0) {
                clog.app.info(`loging an recorded iName(${flens.iName})`);
                // retrieve block info to logging it
                let existedWBlocks = dagHandler.getWBlocksByDocHash({
                    docsHashes: [alreadyRecordedDoc.inDocHash]
                }).wBlocks;
                for (let aWBlock of existedWBlocks) {
                    // record the log of existed block. later if there will collisions in next 12 hours, machine will use these l

                    collisionsHandler.logACollision({
                        clVoter: machineSettings.backerAddress,
                        clCollisionRef,
                        clBlockHash: aWBlock.bHash,
                        clDocHash: alreadyRecordedDoc.inDocHash,
                        clCreationDate: aWBlock.bCreationDate,
                        clReceiveDate: aWBlock.bReceiveDate
                    });

                }
            }

            // then logging newly received doc too
            dbl = collisionsHandler.getLoggedDocs(flens.hash);
            if (dbl.length == 0) {
                // record the log of newly received block. later if there will collisions in next 12 hours, machine will use these l
                collisionsHandler.logACollision({
                    clVoter: machineSettings.backerAddress,
                    clCollisionRef,
                    clBlockHash: block.blockHash,
                    clDocHash: flens.hash,
                    clCreationDate: block.creationDate,
                    clReceiveDate: utils.getNow()
                });
            }

            // then preparing floating vote for iName collision & broadcasting to neighbors
            if (dagHandler.isDAGUptodated() && iutils.isYoungerThan2Cycle(block.creationDate)) {
                let fvRes = flensHandler.createFleNSVoteBlock({
                    // voter: machineSettings.backerAddress,
                    blockHash: block.blockHash, // as an uplink, sice floating vote must be attached to block which are voting for.
                    clCollisionRef
                });
                clog.app.info(`create FleNS Vote Block res: ${utils.stringify(fvRes)}`);
                if (fvRes.err != false)
                    return fvRes

                if (utils._nilEmptyFalse(fvRes.block)) {
                    msg = `error in create FleNSVoteBlock: ${utils.stringify(fvRes)}`;
                    clog.app.error(msg);
                    return { err: true, msg }
                }

                // send floating vote for iName collision
                msg = `fVote on FleNS collision iName(${flens.iName}) block(${utils.hash6c(fvRes.block.blockHash)})`;
                clog.app.info(`Broadcasting ${msg}`);
                sendingQ.pushIntoSendingQ({
                    sqType: iConsts.BLOCK_TYPES.FVote,
                    sqCode: fvRes.block.blockHash,
                    sqPayload: utils.stringify(fvRes.block),
                    sqTitle: msg,
                });
                return { err: false, msg: `Broadcasted ${msg}` };
            }
        }
    }


    static validateINames(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let iNames = _.has(grpdDocuments, iConsts.DOC_TYPES.INameReg) ? grpdDocuments[iConsts.DOC_TYPES.INameReg] : [];
        if (iNames.length == 0)
            return {
                err: false,
            }

        let res = this.controlINames(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved iNames!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aFleNS of iNames) {
            if (!approvedDocHashes.includes(aFleNS.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) IName(${utils.hash6c(aFleNS.hash)}), IName is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }


        return {
            err: false,
        }

    }

    static controlINames(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;

        let trxDict = args.trxDict;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;

        let ApprovedDocs = [];

        let FleNSs = _.has(grpdDocuments, iConsts.DOC_TYPES.INameReg) ? grpdDocuments[iConsts.DOC_TYPES.INameReg] : [];
        if (FleNSs.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The NORMAL block have ${FleNSs.length} FleNSs`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        // documents = [];
        let allINamesInBlock = [];
        for (let aFleNS of FleNSs) {
            console.log('\n\n aFleNS', utils.stringify(aFleNS));

            if (allINamesInBlock.includes(aFleNS.iName)) {
                msg = `duplicated iName(${aFleNS.iName}) in a block!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            allINamesInBlock.push(aFleNS.iName);


            let unlockSet, isValidUnlock, signMsg;

            // controll document signature
            let dExtInfo;
            if (_.has(aFleNS, 'dExtInfo')) {
                dExtInfo = aFleNS.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aFleNS.hash]];
            }

            // control document hash
            if (aFleNS.hash != flensHandler.register.calcHashDINameRegReqS(aFleNS)) {
                msg = `aFleNS.hash(${utils.hash6c(aFleNS.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate owner
            if (!crypto.bech32_isValidAddress(aFleNS.iNOwner)) {
                msg = `invalid owner(${iutils.shortBech8(aFleNS.iNOwner)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate owner register capacity
            if (flensHandler.register.ownerRegisteredRecords(aFleNS.iNOwner).available < 1) {
                msg = `owner(${iutils.shortBech8(aFleNS.iNOwner)}) already exceed allowed iNames, so can not register a new one!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // // vote proposal cost & if exist a trx to pay this cost
            // // control if proposal is fund by a pledge contract or by an standalon transaction?
            // if (!_.has(mapTrxRefToTrxHash, aFleNS.hash)) {
            //     msg = `No one pays iName reg-req costs(${utils.hash6c(aFleNS.hash)})!`;
            //     clog.sec.error(msg);
            //     return { err: true, msg, shouldPurgeMessage: true }
            // }


            if (stage == iConsts.STAGES.Creating) {
                // validate if name is already registered!
                if (flensHandler.register.alreadyRegistered(aFleNS.iName).isRegistred) {
                    msg = `the iName(${aFleNS.iName}) already registered!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                // validate if name is already in queue of registering!
                let iNameStat = collisionsHandler.hasCollision(aFleNS.iName);
                if (iNameStat.hasCollision) {
                    msg = `the iName(${aFleNS.iName}) already placed in Queue!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            }


            unlockSet = dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aFleNS.iNOwner,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `${stage}, iName reg req, Invalid! given FleNS unlock structure for req, unlock:${utils.stringify({
                    address: aFleNS.iNOwner,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // FleNS signature & permission validate check
            signMsg = flensHandler.register.getSignMsgDFleNS(aFleNS);
            for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
                let aSignature = dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the FleNS(${aFleNS.hash}) has invalid signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify FleNS Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
            }


            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aFleNS.hash]];
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aFleNS.hash) {
                msg = `The iName(${utils.hash6c(aFleNS.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // calculate doc register request cost
            let iNameCost = flensHandler.register.calcCostDINameRegReq({
                cDate: creationDate,
                stage,
                flens: aFleNS
            });
            if (iNameCost.err != false)
                return iNameCost;

            // validate payment trx outputs  
            if (!_.has(costPayerTrxOutputs, 'TP_INAME_REG') ||
                (costPayerTrxOutputs['TP_INAME_REG'] < iNameCost.cost)) {
                msg = `referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for registering iName costs(${utils.hash6c(aFleNS.hash)}) 4has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_INAME_REG'])} mcPAIs< ${utils.sepNum(iNameCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            ApprovedDocs.push({
                _doc: aFleNS,
                _docHash: aFleNS.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aFleNS.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendINamesToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlINames(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }




    /**
     * 
     * @param {*} iName 
     * if the payment for iName costs has problem(double spend or ...) the activated iName must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeINameBecauseOfPaymentsFail(iNameHash) {
        clog.app.error(`removeBallot(${iNameHash}`);
        return flensHandler.removeIName(iNameHash);
    }

}


module.exports = INamesInRelatedBlock;
