const _ = require('lodash');
const iConsts = require('../../../config/constants');
const cnfHandler = require('../../../config/conf-params');
const clog = require('../../../loggers/console_logger');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const superControlUntilCoinbaseMinting = require('../../validate-inputs-all-the-way-back-to-cb');
const spentCoinsHandler = require('../../../transaction/utxo/stxo-handler');
const calcTrxDPCost = require('../../../transaction/validators/calc-trx-dpcost');
const trxHashHandler = require('../../../transaction/hashable');

class TransactionsInRelatedBlock {

    static appendTransactions(args) {
        let addTrxs = this.bufferHandler.fetchBufferedTransactions(args);
        if (addTrxs.err != false) {
            clog.app.error(`addTrxs err: ${utils.stringify(addTrxs)}`);
        }
        return addTrxs;
    }

    static validateTransactions(args) {
        let block = args.block;
        let stage = args.stage;
        let msg;


        if (utils._nilEmptyFalse(block.bExtInfo)) {
            msg = `Block(${utils.hash6c(block.blockHash)}) missed ext Info! `;
            clog.trx.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        let bOverview = this.overView.prepareBlockOverview({ block });
        if (bOverview.err != false)
            return bOverview;
        let {
            supportedP4P,
            blockUsedCoins,
            mapCoinToSpenderDoc,
            usedCoinsDict
        } = bOverview;

        let maybeInvalidCoins = bOverview.blockNotMaturedCoins;

        let localBlockDPCost = 0;
        let remoteDPCostInfo;
        // let remoteBlockDPCostBacker = 0;
        let transactionMinimumFee = cnfHandler.getTransactionMinimumFee({ cDate: block.creationDate });
        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let trx = block.docs[docInx];

            // do validate only transactions
            if (
                !iutils.isBasicTransaction(trx.dType) &&
                !iutils.isDPCostPayment(trx.dType)
            ) {
                continue;
            }

            // DPCOst payment control
            if (trx.dType == iConsts.DOC_TYPES.DPCostPay) {
                remoteDPCostInfo = this.retrieveDPCostInfo({
                    blockHash: block.blockHash,
                    backer: block.backer,
                    dpDoc: trx
                });
                if (remoteDPCostInfo.err != false)
                    return remoteDPCostInfo;
            }


            // if extInfo & dExtHash are valid
            let tmpTrx = _.clone(trx);
            if (![iConsts.DOC_TYPES.DPCostPay].includes(trx.dType)) {
                tmpTrx.dExtInfo = block.bExtInfo[docInx];
                // console.log('tmpTrx, tmpTrx', tmpTrx);
                if (tmpTrx.dExtHash != trxHashHandler.calcTrxExtRootHash(tmpTrx.dExtInfo)) {
                    msg = `dExtHash is different from local value! block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(tmpTrx.hash)})`;
                    clog.trx.error(msg)
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            }


            let trxStatedDPCost = 0;
            if (supportedP4P.includes(trx.hash)) {
                clog.trx.info(`block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)}) is supported by p4p trx`);
                // so we do not need to controll trx fee, because it is already payed

            } else if ([iConsts.DOC_TYPES.DPCostPay].includes(trx.dType)) {
                // this kind of transaction do not need to have trx-fee

            } else {

                if (!iConsts.SUPPORTS_CLONED_TRANSACTION && (trx.dPIs.length > 1)) {
                    msg = `The network still do not accept Cloned transactions!`
                    clog.trx.error(msg)
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                
                for (let aDPIndex of trx.dPIs) {
                    if (trx.outputs[aDPIndex][0] == block.backer) {
                        trxStatedDPCost = trx.outputs[aDPIndex][1];
                    }
                }
                if (trxStatedDPCost == 0) {
                    msg = `at least one trx hasn't backer fee. block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)})`
                    clog.trx.error(msg)
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                if (trxStatedDPCost < transactionMinimumFee) {
                    msg = `The backer fee of block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)}) is less than Minimum acceptable fee! ${(trxStatedDPCost)}<${(cnfHandler.getTransactionMinimumFee())}`
                    clog.trx.error(msg)
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                let locallyRecalculateTrxDPCost = calcTrxDPCost({
                    cDate: block.creationDate,
                    trx: tmpTrx,
                    supportedP4PTrxLength: trx.dPIs.length,
                    stage: iConsts.STAGES.Validating
                });
                if (locallyRecalculateTrxDPCost.err != false)
                    return locallyRecalculateTrxDPCost;

                if (trxStatedDPCost < locallyRecalculateTrxDPCost.cost) {
                    msg = `The backer fee of block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)}) is less than network values! ${utils.sepNum(trxStatedDPCost)}<${utils.sepNum(locallyRecalculateTrxDPCost.cost)} mcPAIs`
                    clog.trx.error(msg)
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            }

            localBlockDPCost += trxStatedDPCost;
        }
        msg = `Backer Fees Sum = ${utils.sepNum(localBlockDPCost)} for block(${utils.hash6c(block.blockHash)})`;
        clog.trx.info(msg);

        // control if block total trx fees are valid
        let localBlockDPCostBacker = utils.floor(localBlockDPCost * iConsts.BACKER_PERCENT_OF_BLOCK_FEE) - cnfHandler.getBlockFixCost({ cDate: block.creationDate });
        if (localBlockDPCostBacker != remoteDPCostInfo.backerIncomes) {
            msg = `The locally calculated backer fee of block(${utils.hash6c(block.blockHash)}) is not what remote is ! ${utils.microPAIToPAI(localBlockDPCostBacker)}!=${utils.microPAIToPAI(remoteDPCostInfo.backerIncomes)}`
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }
        let localBlockDPCostTreasury = localBlockDPCost - localBlockDPCostBacker;
        if (localBlockDPCostTreasury != remoteDPCostInfo.treasuryIncomes) {
            msg = `The locally calculated treasury remoteBlockDPCot_tils.h(block.blockHash)}) is not what remote is ! ${utils.microPAIToPAI(localBlockDPCostTreasury)}!=${utils.microPAIToPAI(remoteDPCostInfo.treasuryIncomes)}`
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }

        let SCUDS = null;
        if (iConsts.SUPER_CONTROL_UTXO_DOUBLE_SPENDING) {
            /**
             * after being sure about secure and proper functionality of code, we can cut this controll in next monthes
             * finding the block(s) which are used these coins and already are registerg in DAG
             */
            SCUDS = spentCoinsHandler.findCoinsSpendLocations({ coins: blockUsedCoins });
            if (SCUDS.err != false) {
                return SCUDS;
            }
            if (utils.objKeys(SCUDS.spendsDict).length > 0) {
                msg = `SCUDS: SUPER_CONTROL_UTXO_DOUBLE_SPENDING found some double-spending with block(${utils.hash6c(block.blockHash)}) SCUDS.spendsDict: ${utils.stringify(SCUDS.spendsDict)}`;
                clog.sec.error(msg);
                clog.trx.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        let SCUUCM = null;
        if (iConsts.SUPER_CONTROL_UTXO_UNTIL_COINBASE_MINTING) {
            /**
             * most paranoidic and pesimistic control of input validation
             * for now I put this double-controll to also quality controll of the previuos-controls.
             * this control is too costly, so it must be removed or optimized ASAP
             */
            SCUUCM = superControlUntilCoinbaseMinting.trackingBackTheCoins({ spendBlock: block });
            // console.log(SCUUCM);
            if (SCUUCM.err != false) {
                msg = `SuperValidate, block(${utils.hash6c(block.blockHash)}) error message: ${utils.stringify(SCUUCM)}`;
                clog.trx.error(msg);
                return { err: true, msg: SCUUCM.msg, shouldPurgeMessage: true }
            } else {
                clog.trx.info(`SuperValidate, block(${utils.hash6c(block.blockHash)})'s inputs have confirmed path going back to coinbase`);
            }
        }

        let invalidationRes = this.considerInvalidation.considerInvalidCoins({
            blockHash: block.blockHash,
            blockCreationDate: block.creationDate,
            blockUsedCoins,
            maybeInvalidCoins,
            usedCoinsDict,
            mapCoinToSpenderDoc
        });
        if (invalidationRes.err != false)
            return invalidationRes;
        let { invalidCoinsDict, doubleSpends, isSusBlock } = invalidationRes;
        usedCoinsDict = invalidationRes.usedCoinsDict;


        let equationRes = this.considerEquation.validateEquation({
            block,
            usedCoinsDict,
            invalidCoinsDict
        });
        if (equationRes.err != false)
            return equationRes;


        /**
         * control UTXO visibility in DAG history by going back throught ancestors
         * since the block can contains only UTXOs which are took palce in her hsitory
         * in oder words, they are in block's sibility
         */
        if ((isSusBlock == null) && (iConsts.SUPER_CONTROL_UTXO_UNTIL_COINBASE_MINTING == false)) {
            let visRes = this.considerVisibility.controlCoinsVisibilityInGraphHistory({
                blockUsedCoins,
                ancestors: block.ancestors,
                blockHash: block.blockHash
            });
            if (visRes.err != false)
                return visRes;
        }

        return { err: false, isSusBlock, doubleSpends }
    }


    static retrieveDPCostInfo(args) {
        let dpDoc = args.dpDoc;
        let blockHash = args.blockHash;
        let backer = args.backer;
        /**
         * the block cost payment transaction is a document that always has to have no input and 2 outputs.
         * 0. TP_DP   Treasury Payment Data & Process Cost
         * 1. backer fee
         */
        if ((dpDoc.outputs.length != 2) || (dpDoc.outputs[0][0] != 'TP_DP')
            || (dpDoc.outputs[1][0] != backer)) {
            msg = `the block(${utils.hash6c(blockHash)}) dpDoc(${utils.hash6c(dpDoc.hash)}) is an invalid treasury payment `
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }
        if (utils.stringify(dpDoc).length > iConsts.MAX_DPCostPay_DOC_SIZE) {
            msg = `the block(${utils.hash6c(blockHash)}) dpDoc(${utils.hash6c(dpDoc.hash)}) has invalid length for a treasury payment `
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }
        let treasuryIncomes = iutils.convertBigIntToJSInt(dpDoc.outputs[0][1]);
        let backerIncomes = iutils.convertBigIntToJSInt(dpDoc.outputs[1][1]);
        return { err: false, treasuryIncomes, backerIncomes }
    }



}

TransactionsInRelatedBlock.overView = require('./prepare-overview');
TransactionsInRelatedBlock.bufferHandler = require('./buffer-handler');
TransactionsInRelatedBlock.considerInvalidation = require('./consider-invalidation');
TransactionsInRelatedBlock.considerEquation = require('./consider-equation');
TransactionsInRelatedBlock.considerVisibility = require('./consider-coins-visibility');

module.exports = TransactionsInRelatedBlock;
