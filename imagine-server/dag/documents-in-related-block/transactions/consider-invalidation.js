const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const spentCoinsHandler = require('../../../transaction/utxo/stxo-handler');
const dagHandler = require('../../graph-handler/dag-handler');


class ConsiderInvalidation {

    static considerInvalidCoins(args) {
        let msg;
        let blockHash = args.blockHash;
        let blockCreationDate = args.blockCreationDate;
        let blockUsedCoins = args.blockUsedCoins;
        let usedCoinsDict = args.usedCoinsDict;
        let maybeInvalidCoins = args.maybeInvalidCoins;
        let mapCoinToSpenderDoc = args.mapCoinToSpenderDoc;

        // retrieve all spent coins in last 5 days
        let {
            coinsInSpentTable,
            spendsOrder
        } = spentCoinsHandler.makeSpentCoinsDict({
            coins: blockUsedCoins
        });
        if (utils.objKeys(coinsInSpentTable).length > 0) {
            // the inputs which are already spended are invalid coins too
            maybeInvalidCoins = utils.arrayAdd(maybeInvalidCoins, utils.objKeys(coinsInSpentTable));
            maybeInvalidCoins = utils.arrayUnique(maybeInvalidCoins);
        }

        let invalidCoinsDict = {};  // it contains invalid coins historical creation info
        if (maybeInvalidCoins.length > 0) {
            clog.trx.error(`maybe Invalid coins (either because of not matured or already spend): ${maybeInvalidCoins.map(x => iutils.shortCoinRef(x))}`);
            invalidCoinsDict = dagHandler.getCoinsGenerationInfoViaSQL(maybeInvalidCoins);
            clog.trx.info(`invalid Coins Dict: ${utils.stringify(invalidCoinsDict)}`);

            // controll if all potentially invalid coins, have coin creation record in DAG history
            if (utils.objKeys(invalidCoinsDict).length != maybeInvalidCoins.length) {
                msg = `block(${utils.hash6c(blockHash)}) uses some un-existed inputs. may be machine is not synched`;
                clog.trx.error(msg);
                return { err: true, msg, shouldPurgeMessage: true };
            }

            /**
             * control if invalidity is because of using really unmatured outputs(which will be matured in next hours)? 
             * if yes drop block
             */
            for (let aCoin of utils.objKeys(invalidCoinsDict)) {
                let isMat = iutils.isMatured({
                    coinCreationDate: invalidCoinsDict[aCoin].coinGenCreationDate,
                    docType: invalidCoinsDict[aCoin].coinGenDocType
                });
                if ((isMat.err != false) || (isMat.matured != true)) {
                    msg = `block(${utils.hash6c(blockHash)}) uses at least one unmaturated input: ${aCoin}`;
                    clog.trx.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
            }
        }


        for (let anInvalidCoin of utils.objKeys(invalidCoinsDict)) {

            // append also invalid refs to usedCoinsDict
            usedCoinsDict[anInvalidCoin] = {
                utCoin: anInvalidCoin,
                utOAddress: invalidCoinsDict[anInvalidCoin].coinGenOutputAddress,
                utOValue: iutils.convertBigIntToJSInt(invalidCoinsDict[anInvalidCoin].coinGenOutputValue),
                utRefCreationDate: invalidCoinsDict[anInvalidCoin].coinGenCreationDate
            }

            /**
             * adding to spend-input-dictionary the invalid coins in current block too
             * in order to having a complete history & order of entire spent coins of the block
             */
            if (!_.has(coinsInSpentTable, anInvalidCoin))
                coinsInSpentTable[anInvalidCoin] = [];
            coinsInSpentTable[anInvalidCoin].push({
                spendDate: blockCreationDate,
                spendBlockHash: blockHash,
                spendDocHash: mapCoinToSpenderDoc[anInvalidCoin]
            });
            // if (!_.has(spendsOrder, anInvalidCoin))
            //     spendsOrder[anInvalidCoin] = [];
            // spendsOrder[anInvalidCoin].push({
            //     docHash: mapCoinToSpenderDoc[anInvalidCoin],
            //     date: blockCreationDate,
            // });
        }


        // all spent_loc must exist in invalidCoinsDict
        let tmp1 = utils.objKeys(invalidCoinsDict);
        let tmp2 = utils.objKeys(coinsInSpentTable);
        if ((tmp1.length != tmp2.length) || (utils.arrayDiff(tmp1, tmp2).length > 0) || (utils.arrayDiff(tmp2, tmp1).length > 0)) {
            msg = `finding invalidations messed up block(${utils.hash6c(blockHash)}) maybe Invalid Inputs: `;
            msg += `invalid Coins Dict ${utils.stringify(utils.objKeys(invalidCoinsDict))} coins In Spent Table:${utils.stringify(coinsInSpentTable)}`;
            clog.sec.error(msg);
            clog.trx.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        // if (utils.objKeys(coinsInSpentTable).length > 0) {
        //     for (let aCoin of utils.objKeys(coinsInSpentTable)) {
        //         if (!_.has(invalidCoinsDict, aCoin)) {
        //             msg = `invalidCoinsDict in block(${utils.hash6c(blockHash)}) missed at least one coin in history spend-doc`
        //             msg += `(${aCoin} > > ${utils.stringify(coinsInSpentTable)}) invalidCoinsDict=>${JSON.stringify(invalidCoinsDict)}`;
        //             clog.trx.error(msg);
        //             return { err: true, msg, shouldPurgeMessage: true };
        //         }
        //     }
        // }
        let isSusBlock = null;
        if (utils.objKeys(invalidCoinsDict).length > 0) {
            msg = `Some transaction inputs in block(${utils.hash6c(blockHash)}) are not valid `;
            msg += `these are duplicated inputs ${utils.stringify(utils.objKeys(invalidCoinsDict).map(x => iutils.shortCoinRef(x)))}`
            clog.trx.error(msg);
            isSusBlock = true;
        }

        // apllying machine-POV-order to coinsInSpentTable as an order-attr
        for (let aCoin of utils.objKeys(coinsInSpentTable)) {
            // for (let anSpent of coinsInSpentTable[aCoin]) {
            //looping on orders
            for (let inx = 0; inx < coinsInSpentTable[aCoin].length; inx++) {
                coinsInSpentTable[aCoin][inx].spendOrder = inx;
            }
            // }
        }


        return {
            err: false,
            invalidCoinsDict,
            usedCoinsDict,
            doubleSpends: coinsInSpentTable,
            isSusBlock
        }

    }

}
module.exports = ConsiderInvalidation
