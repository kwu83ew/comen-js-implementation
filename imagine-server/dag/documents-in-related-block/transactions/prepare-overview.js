const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const utxoHandler = require('../../../transaction/utxo/utxo-handler');
const rejectedTrxHandler = require('../../normal-block/rejected-transactions-handler');


class OverviewHandler {

    static prepareBlockOverview(args) {
        let msg;
        let block = args.block;
        let supportedP4P = [];
        let trxUniqueness = [];
        let inputsDocHashes = [];
        let blockUsedCoins = [];
        let mapCoinToSpenderDoc = {};

        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let trx = block.docs[docInx];

            if (trx.creationDate > block.creationDate) {
                msg = `The trx(${utils.hash6c(trx.hash)}) is after block(${utils.hash6c(block.blockHash)}) creation-date!`;
                clog.trx.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            trxUniqueness.push(trx.hash);

            // extracting P4P (if exist)
            if ((trx.dType == iConsts.DOC_TYPES.BasicTx) && (trx.dClass == iConsts.TRX_CLASSES.P4P)) {
                if (!iConsts.SUPPORTS_P4P_TRANSACTION) {
                    msg = `Network still doen't support P4P transactions. block(${utils.hash6c(block.blockHash)}) `;
                    clog.trx.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                supportedP4P.push(trx.ref)
            }

            if (iutils.trxHasInput(trx.dType) && !iutils.trxHasNotInput(trx.dType)) {
                for (let input of trx.inputs) {
                    inputsDocHashes.push(input[0]);
                    let aCoin = iutils.packCoinRef(input[0], input[1]);
                    blockUsedCoins.push(aCoin);
                    mapCoinToSpenderDoc[aCoin] = trx.hash;
                }
            }
        }

        // uniquness test
        if (trxUniqueness.length != utils.arrayUnique(trxUniqueness).length) {
            msg = `Duplicating same trx in block body. block(${utils.hash6c(block.blockHash)}) `;
            clog.trx.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // control for using of rejected Transactions refLocs
        // in fact a refLoc can exist in table trx_utxo or not. if not, it doesn't matter whether exist in rejected trx or not.
        // and this controll of rejected trx is not necessary but it is a fastest way to discover a double-spend
        let rejTrx = rejectedTrxHandler.searchRejTrx({
            query: [['rt_doc_hash', ['IN', inputsDocHashes]]]
        });
        if (rejTrx.length > 0) {
            msg = `block(${utils.hash6c(block.blockHash)}) uses rejected transaction's outputs! ${rejTrx.map(x => utils.hash6c(x.rt_doc_hash))}`;
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // control double spending in a block
        // because malisciuos user can use one ref in multiple transaction in same block
        if (blockUsedCoins.length != utils.arrayUnique(blockUsedCoins).length) {
            msg = `Double spending same refs in a block(${utils.hash6c(block.blockHash)})`
            clog.trx.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }
        clog.trx.info(`block(${utils.hash6c(block.blockHash)}) has ${blockUsedCoins.length} inputs Uniqueness ${blockUsedCoins.map(x => iutils.shortCoinRef(x))}`);

        // it is a dictionary for all inputs either valid or invalid
        // it has 3 keys/values (utCoin, utOAddress, utOValue)
        let usedCoinsDict = {};
        // all inputs must be maturated, maturated means it passed at least 12 hours of creeating the outputs and now they are presented in table trx_utxos adn are spendable
        let spendableCoins = [];
        if (blockUsedCoins.length > 0) {
            // check if the refLocs exist in UTXOs?
            let coinsInfo = utxoHandler.getCoinsInfo({ coins: blockUsedCoins });
            if (coinsInfo.length > 0) {
                for (let utInfo of coinsInfo) {
                    spendableCoins.push(utInfo.utCoin);
                    usedCoinsDict[utInfo.utCoin] = utInfo;
                    // the block creationDate MUST be at least 12 hours after the creation date of reflocs
                    if (block.creationDate < utils.minutesAfter(iConsts.getCycleByMinutes(), utInfo.utRefCreationDate)) {
                        msg = `The creation of refLoc(${iutils.shortCoinRef(utInfo.utCoin)}) is after usage in block(${utils.hash6c(block.blockHash)})!`;
                        clog.trx.error(msg);
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                }
            }
        }
        clog.trx.info(`block(${utils.hash6c(block.blockHash)}) has ${spendableCoins.length} maturated Inputs ${spendableCoins.map(x => iutils.shortCoinRef(x))}`);

        // all inputs which are not in spendable coins, potentialy can be invalid
        let blockNotMaturedCoins = utils.arrayDiff(blockUsedCoins, spendableCoins);
        if (blockNotMaturedCoins.length > 0) {
            clog.sec.error(`at ${utils.getNowSSS()} in table trx_utxo missed ${utils.stringify(blockNotMaturedCoins)} inputs! probably is cloned transaction`)
        }

        return {
            err: false,
            supportedP4P,
            blockUsedCoins,
            mapCoinToSpenderDoc,
            usedCoinsDict,
            blockNotMaturedCoins
        }
    }

}

module.exports = OverviewHandler;
