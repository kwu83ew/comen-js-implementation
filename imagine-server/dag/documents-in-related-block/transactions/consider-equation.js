const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const trxSigValidator = require('../../../transaction/validators/trx-signature-validator');
const validateGeneralRulsForTransaction = require('../../../transaction/validators/trx-general-validator');


class ConsiderEquation {

    static validateEquation(args) {
        let block = args.block;
        let usedCoinsDict = args.usedCoinsDict;
        let invalidCoinsDict = args.invalidCoinsDict;
        let validateRes, msg;

        // transaction details check
        let inputsSum = {};
        let outputsSum = {};
        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let doc = block.docs[docInx];
            if (iutils.isBasicTransaction(doc.dType)) {

                // signatures control
                validateRes = trxSigValidator.trxSignatureValidator({
                    block,
                    docInx,
                    coinsDict: usedCoinsDict
                });
                if (validateRes.err != false) {
                    return validateRes
                }

                // control transaction hash
                validateRes = validateGeneralRulsForTransaction(block.blockHash, doc);
                if (validateRes.err != false) {
                    return validateRes
                }

                // equation control
                inputsSum[doc.hash] = 0;
                outputsSum[doc.hash] = 0;
                if (_.has(doc, 'inputs')) {
                    for (let input of doc.inputs) {
                        let aCoin = iutils.packCoinRef(input[0], input[1]);
                        if (_.has(usedCoinsDict, aCoin)) {
                            if (usedCoinsDict[aCoin].utOValue >= Number.MAX_SAFE_INTEGER) {
                                msg = `the transaction(${doc.hash}) in block(${utils.hash6c(block.blockHash)}) has input bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
                                clog.sec.error(msg);
                                clog.trx.error(msg);
                                return { err: true, msg, shouldPurgeMessage: true }
                            }
                            inputsSum[doc.hash] += usedCoinsDict[aCoin].utOValue;

                        } else {
                            /**
                             * trx uses already spent outputs! so try invalidCoinsDict
                             * probably it is a double-spend case, which will be decided after 12 hours, in importing step
                             * BTW ALL trx must have balanced equation (even duoble-spendeds)
                             */
                            if (_.has(invalidCoinsDict, aCoin)) {
                                if (iutils.convertBigIntToJSInt(invalidCoinsDict[aCoin].coinGenOutputValue >= Number.MAX_SAFE_INTEGER)) {
                                    msg = `the transaction(${doc.hash}) in block(${utils.hash6c(block.blockHash)}) has inv-input bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
                                    clog.sec.error(msg);
                                    clog.trx.error(msg);
                                    return { err: true, msg, shouldPurgeMessage: true }
                                }
                                inputsSum[doc.hash] += iutils.convertBigIntToJSInt(invalidCoinsDict[aCoin].coinGenOutputValue);
                            } else {
                                msg = `the input(${aCoin}) in block(${utils.hash6c(block.blockHash)}) absolutely missed! not in tables neither in DAG!`;
                                clog.sec.error(msg);
                                clog.trx.error(msg);
                                return { err: true, msg, shouldPurgeMessage: true }
                            }
                        }
                    }
                }

                if (_.has(doc, 'outputs')) {
                    for (let output of doc.outputs) {
                        if (output[1].toString() != output[1].toString().replace(/\D/g, '')) {
                            msg = `the transaction(${doc.hash}) in block(${utils.hash6c(block.blockHash)}) has not digit charecters! ${utils.microPAIToPAI(output[1])}`;
                            clog.sec.error(msg);
                            clog.trx.error(msg);
                            return { err: true, msg, shouldPurgeMessage: true }
                        }
                        if (output[1] == 0) {
                            msg = `the transaction(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)}) has zero output! ${utils.microPAIToPAI(output[1])}`;
                            clog.sec.error(msg);
                            clog.trx.error(msg);
                            return { err: true, msg, shouldPurgeMessage: true }
                        }
                        if (output[1] < 0) {
                            msg = `the transaction(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)}) has negative output! ${utils.microPAIToPAI(output[1])}`;
                            clog.sec.error(msg);
                            clog.trx.error(msg);
                            return { err: true, msg, shouldPurgeMessage: true }
                        }
                        if (parseInt(output[1]) >= Number.MAX_SAFE_INTEGER) {
                            msg = `the transaction(${doc.hash}) in block(${utils.hash6c(block.blockHash)}) has output bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
                            clog.sec.error(msg);
                            clog.trx.error(msg);
                            return { err: true, msg, shouldPurgeMessage: true }
                        }
                        outputsSum[doc.hash] += parseInt(output[1]);
                    }
                }
                clog.app.info(`inputsSum ${inputsSum[doc.hash]} = ${outputsSum[doc.hash]} output sum`);
                clog.app.info(`inputsHash ${doc.hash} = ${doc.hash}`);
                if (inputsSum[doc.hash] != outputsSum[doc.hash]) {
                    msg = `the transaction is not balanced! doc(${utils.hash6c(doc.hash)}) Block(${utils.hash6c(block.blockHash)})  input(${utils.microPAIToPAI6(inputsSum[doc.hash])}) != output(${utils.microPAIToPAI6(outputsSum[doc.hash])})`;
                    clog.sec.error(msg);
                    clog.trx.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            }
        }

        // calculate block total inputs & outputs
        let blockInputsSum = utils.objKeys(inputsSum).map(x => inputsSum[x]).reduce((a, b) => a + b, 0);
        let blockOutputsSum = utils.objKeys(outputsSum).map(x => outputsSum[x]).reduce((a, b) => a + b, 0);
        if (blockInputsSum != blockOutputsSum) {
            msg = `the block(${utils.hash6c(block.blockHash)}) has not balanced in/out! ${utils.microPAIToPAI6(blockInputsSum)} != ${utils.microPAIToPAI6(blockOutputsSum)}`;
            clog.sec.error(msg);
            clog.trx.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        msg = `the block(${utils.hash6c(block.blockHash)}) input:${utils.microPAIToPAI6(blockInputsSum)} = output:${utils.microPAIToPAI6(blockOutputsSum)}`;
        clog.trx.info(msg);


        return { err: false }
    }
}

module.exports = ConsiderEquation;
