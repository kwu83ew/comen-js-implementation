const _ = require('lodash');
const iConsts = require('../../../config/constants');
const cnfHandler = require('../../../config/conf-params');
const clog = require('../../../loggers/console_logger');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const docBufferHandler = require('../../../services/buffer/buffer-handler');

class TrxBufferHandler {

    static fetchBufferedTransactions(args) {
        // retrieve buffered transactions(simple, p4p, ...)
        let msg;
        let block = args.block;
        let creationDate = block.creationDate;
        let docsHashes = [];
        let externalInfoHashes = [];

        let bufferedTrxs = docBufferHandler.searchBufferedDocsSync({
            query: [
                ['bd_doc_type', ['IN', [iConsts.DOC_TYPES.BasicTx]]]
            ],
            order: [
                ['bd_dp_cost', 'DESC'],
                ['bd_doc_class', 'ASC'],
                ['bd_insert_date', 'ASC']
            ]
        });
        // TODO: currently naivly the query select most payer transaction first. the algorythm must be enhanced. specially to deal with block size, and beeing sure 
        // if prerequsities doc(e.g payer transaction and referenced doscument & somtimes documents) are all placed in same block!

        console.log(`------------------------- The NORMAL block will have ${bufferedTrxs.length} transactions`);
        clog.app.info(`The NORMAL block has ${bufferedTrxs.length} transactions`);


        let supportedP4P = [];// extracting P4P (if exist)
        let transactions = [];// parse TRXs
        for (let serializedTrx of bufferedTrxs) {
            let trx = JSON.parse(serializedTrx.bdPayload);
            transactions.push(trx);
            if (trx.dClass == iConsts.TRX_CLASSES.P4P) {
                if (!iConsts.SUPPORTS_P4P_TRANSACTION) {
                    msg = `Network! still doen't support P4P transactions. block(${utils.hash6c(block.blockHash)}) `;
                    clog.trx.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                supportedP4P.push(trx.ref);
            }
        }


        let blockTotalDPCost = 0;
        let dummySumBlockValue = 0;
        let blockTotalOutput = 0;
        for (let trx of transactions) {

            if (_.has(trx, 'outputs') && Array.isArray(trx.outputs))
                for (let anOutput of trx.outputs) {
                    let anOutputValue = iutils.convertBigIntToJSInt(anOutput[1]);
                    if (anOutputValue == 0) {
                        msg = `creating block, the transaction(${utils.hash6c(trx.hash)}) in block(${utils.hash6c(block.blockHash)}) has zero output! ${utils.microPAIToPAI(anOutputValue)}`;
                        clog.sec.error(msg);
                        clog.trx.error(msg);
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                    if (anOutputValue < 0) {
                        msg = `creating block, the transaction(${utils.hash6c(trx.hash)}) in block(${utils.hash6c(block.blockHash)}) has negative output! ${utils.microPAIToPAI(anOutputValue)}`;
                        clog.sec.error(msg);
                        clog.trx.error(msg);
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                    blockTotalOutput += anOutputValue;
                }

            // collect backer fees
            let DPCost = 0;
            if (supportedP4P.includes(trx.hash)) {
                clog.trx.info(`block(${utils.hash6c(block.blockHash)}) trx: ${utils.hash6c(trx.hash)} is supported by p4p trx, so this trx must not pay trx-fee`);

            } else {
                // find the backer output
                // check if trx is clone, in which client pays for more than one backer in a transaction 
                // in order to ensure more backers put trx in DAG
                for (let aDPIndex of trx.dPIs) {
                    if (trx.outputs[aDPIndex][0] == block.backer) {
                        DPCost = trx.outputs[aDPIndex][1];
                    }
                }
                // for (let anOutput of trx.outputs) {
                //     if ((DPCost == 0) && (anOutput[0] == block.backer)) {
                //         DPCost = anOutput[1];
                //     }
                // }
                if (DPCost == 0) {
                    msg = `can not create block, because at least one trx hasn't backer fee. block(${utils.hash6c(block.blockHash)}) trx: ${utils.hash6c(trx.hash)}`
                    clog.trx.error(msg);
                    console.log(`msg ${msg}`);
                    return { err: true, msg };
                }

                dummySumBlockValue += trx.outputs.map(x => iutils.convertBigIntToJSInt(x[1])).reduce((a, b) => a + b, 0);
            }
            blockTotalDPCost += DPCost;
            docsHashes.push(trx.hash);
            externalInfoHashes.push(trx.dExtHash);
            block.bExtInfo.push(trx.dExtInfo); // in this case segwits
            delete trx.dExtInfo;
            block.docs.push(trx);

        };

        // create treasury payment
        let backerNetFee = utils.floor(blockTotalDPCost * iConsts.BACKER_PERCENT_OF_BLOCK_FEE) - cnfHandler.getBlockFixCost({ cDate: creationDate });
        if (backerNetFee < 0) {
            msg = `THe block can not cover broadcasting costs! \nblock Total DPCost(${blockTotalDPCost})\nbacker Net Fee(${backerNetFee})`;
            clog.app.error(msg);
            return { err: true, msg };
        }

        let treasury = blockTotalDPCost - backerNetFee;
        let DPCostTrx = treasuryHandler.createDPCostPaymentTrx({
            creationDate,
            treasury,
            backerNetFee,
        });
        console.log('DPCostTrx', DPCostTrx);
        docsHashes.unshift(DPCostTrx.hash);
        block.docs.unshift(DPCostTrx);
        block.bExtInfo.unshift("NOE");   // althougt it is empty but must be exits, in order to having right index in block ext Infos array
        externalInfoHashes.unshift("NOE");   // althougt it is empty but must be exits, in order to having right index in block ext Infos array

        return {
            err: false,
            dummySumBlockValue,
            blockTotalOutput,
            externalInfoHashes,
            docsHashes,
            block
        }
    }
}

module.exports = TrxBufferHandler;
