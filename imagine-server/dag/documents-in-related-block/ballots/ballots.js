const _ = require('lodash');
const iConsts = require('../../../config/constants');
const cnfHandler = require('../../../config/conf-params');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const DNAHandler = require('../../../dna/dna-handler');
const pollHandler = require('../../../services/polling-handler/general-poll-handler');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const machine = require('../../../machine/machine-handler');
const kvHandler = require('../../../models/kvalue');

class BallotsInRelatedBlock {

    static validateBallots(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let ballots = _.has(grpdDocuments, iConsts.DOC_TYPES.Ballot) ? grpdDocuments[iConsts.DOC_TYPES.Ballot] : [];
        if (ballots.length == 0)
            return {
                err: false,
            }

        let res = this.controlBallots(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved Ballots!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aBllt of ballots) {
            if (!approvedDocHashes.includes(aBllt.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) Ballot(${utils.hash6c(aBllt.hash)}), Ballot is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }

    }

    static controlBallots(args) {

        let msg;
        let grpdDocuments = args.grpdDocuments;
        let stage = args.stage;
        let blockHash = _.has(args, 'blockHash') ? args.blockHash : 'blockIsInCreationPhase!';
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;

        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;
        let trxDict = args.trxDict;


        let ApprovedDocs = [];
        let _preReqAncestors = [];

        let ballots = _.has(grpdDocuments, iConsts.DOC_TYPES.Ballot) ? grpdDocuments[iConsts.DOC_TYPES.Ballot] : [];
        if (ballots.length == 0)
            return {
                err: false,
                ApprovedDocs,
                _preReqAncestors
            }

        msg = `block(${utils.hash6c(blockHash)}) The NORMAL block have ${ballots.length} Ballots`;
        console.log(`msg: ${msg}`);
        clog.app.info(msg);

        // documents = [];

        for (let aBallot of ballots) {
            console.log('\n\n aBallot', utils.stringify(aBallot));

            let unlockSet, isValidUnlock;
            let dExtInfo;
            if (_.has(aBallot, 'dExtInfo')) {
                dExtInfo = aBallot.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aBallot.hash]];
            }

            unlockSet = dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aBallot.voter,
                uSet: unlockSet
            });

            if (isValidUnlock != true) {
                msg = `voter req, Invalid! given voter unlock structure for req, unlock:${utils.stringify({
                    address: aBallot.voter,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // ballot signature & permission validate check
            let { signedHash, signMsg } = pollHandler.ballotHandler.getSignMsgDBallot(aBallot);
            for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
                let aSignature = dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the Ballot(${utils.hash6c(aBallot.hash)}) has invalid signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify FleNS Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
            }

            // control document hash
            if (aBallot.hash != pollHandler.ballotHandler.calcHashDBallot(aBallot)) {
                msg = `block(${utils.hash6c(blockHash)}) aBallot.hash(${utils.hash6c(aBallot.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate vote range
            if ((parseInt(aBallot.vote) < -100) || (parseInt(aBallot.vote) > 100) || (parseInt(aBallot.vote) != parseFloat(aBallot.vote))) {
                msg = `block(${utils.hash6c(blockHash)}) vote value is invalid (${aBallot.vote})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate voter
            if (!crypto.bech32_isValidAddress(aBallot.voter)) {
                msg = `block(${utils.hash6c(blockHash)}) invalid voter(${iutils.shortBech8(aBallot.voter)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // validate voter shares
            if (DNAHandler.getAnAddressShares(aBallot.voter, creationDate).percentage < cnfHandler.getMinShareToAllowedVoting({ cDate: creationDate })) {
                msg = `block(${utils.hash6c(blockHash)}) there is no share for voter(${iutils.shortBech8(aBallot.voter)})!`;
                msg = `block(${(blockHash)}) there is no share for voter(${iutils.shortBech8(aBallot.voter)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // // vote proposal cost & if exist a trx to pay this cost
            // // control if ballot cost is payed by a transaction?
            // if (!_.has(mapTrxRefToTrxHash, aBallot.hash)) {
            //     msg = `block(${utils.hash6c(blockHash)}) No one pays Ballot costs(${utils.hash6c(aBallot.hash)})!`;
            //     clog.sec.error(msg);
            //     return { err: true, msg, shouldPurgeMessage: true }
            // }

            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aBallot.hash]];
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }

            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aBallot.hash) {
                msg = `block(${utils.hash6c(blockHash)}) The ballot(${utils.hash6c(aBallot.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            // calculate ballot cost
            let ballotCost = pollHandler.ballotHandler.calcBallotCost({
                cDate: creationDate,
                ballot: aBallot,
                stage: stage
            });
            if (ballotCost.err != false)
                return ballotCost;
            // validate payment trx outputs  
            if (!_.has(costPayerTrxOutputs, 'TP_BALLOT') ||
                (costPayerTrxOutputs['TP_BALLOT'] < ballotCost.cost)) {
                msg = `block(${utils.hash6c(blockHash)}) referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for Ballot costs(${utils.hash6c(aBallot.hash)}) 2has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_BALLOT'])} mcPAIs< ${utils.sepNum(ballotCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let alreadyVoted = pollHandler.ballotHandler.alreadyVoted({
                voter: aBallot.voter,
                ref: aBallot.ref
            });
            let voteAmendmentAllowed = iConsts.CONSTS.NO;   // TODO: it must be retreived from dynamc POLLING_PROFILE_CLASSES. implement it!
            if (alreadyVoted) {
                if (voteAmendmentAllowed == iConsts.CONSTS.NO) {
                    msg = `block(${utils.hash6c(blockHash)}) Voter(${iutils.shortBech8(aBallot.voter)}), already voted for polling(${utils.hash6c(aBallot.ref)})`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            }

            if (stage == iConsts.STAGES.Creating) {
                // control if referenced polling exist in DAG and also adding polling-container-blockHash to newly-creating-block as an acestor
                // in such a way other machine if they do not have polling block in local DAG, they do not process this block until receiving prerequsit block
                const dagHandler = require('../../graph-handler/dag-handler');
                let { blockHashes, mapDocToBlock } = dagHandler.getBlockHashesByDocHashes({ docsHashes: [aBallot.ref] });
                if (Array.isArray(blockHashes))
                    _preReqAncestors = utils.arrayUnique(utils.arrayAdd(_preReqAncestors, blockHashes));
            }

            ApprovedDocs.push({
                _doc: aBallot,
                _docHash: aBallot.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aBallot.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs,
            _preReqAncestors
        }
    }

    static appendBallotsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlBallots(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }
        if (res._preReqAncestors.length > 0) {
            clog.app.info(`block(${utils.hash6c(block.blockHash)}) ha _preReqAncestors because of Ballots refs to polling-blocks(${res._preReqAncestors.map(x => utils.hash6c(x))})`);
            block.ancestors = utils.arrayUnique(utils.arrayAdd(block.ancestors, res._preReqAncestors));
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }


    static recordABallot(args) {
        let msg;
        let block = args.block;
        let ballot = args.ballot;

        let shares = DNAHandler.getAnAddressShares(ballot.voter, block.creationDate).shares;    // it chenged from ballot.creatioindate to block.creationdate! be sure it works either in real time or synching

        let pollingsInfo = pollHandler.getPollingInfo({ query: [['pll_hash', ballot.ref]] });
        if (pollingsInfo.length == 0) {
            // there are 2 possibilities, or the polling is not valid
            // or polling document still does not exist in node
            console.log(ballot);
            msg = `There is no polling for the Ballot(${utils.hash6c(ballot.hash)}) `;
            clog.sec.error(msg);
            return { err: false, msg }
        }
        let dateOfStartVoting = pollingsInfo[0].pllStartDate;

        // * @param {*} voteCreationTimeByMinute: timeDiff by minutes(statingVoteDateRecordedInBloc.creationDate - dateOfStartVoting)
        // * @param {*} voteReceiveingTimeByMinute: timeDiff by minutes(receivingDate - dateOfStartVoting)

        clog.app.info(`record A Ballot(${utils.hash6c(ballot.hash)}) baCreationDateDiff(${block.creationDate} -- ${dateOfStartVoting}): ${utils.stringify(utils.timeDiff(dateOfStartVoting, block.creationDate))}`);
        let nowT = utils.getNow();
        let baReceiveDate = nowT;
        let baReceiveDateDiff = utils.timeDiff(dateOfStartVoting, nowT).asMinutes;
        if (machine.isInSyncProcess()) {
            let ballotsReceiveTimeDict = kvHandler.getValueSync('ballotsReceiveDates');
            if (!utils._nilEmptyFalse(ballotsReceiveTimeDict)) {
                ballotsReceiveTimeDict = utils.parse(ballotsReceiveTimeDict)
                if (_.has(ballotsReceiveTimeDict, ballot.hash)) {
                    baReceiveDate = ballotsReceiveTimeDict[ballot.hash].baReceiveDate;
                    baReceiveDateDiff = ballotsReceiveTimeDict[ballot.hash].baVoteRDiff;
                }
            }
        }
        pollHandler.ballotHandler.recordBallotInDB({
            baHash: ballot.hash,
            baPollingRef: ballot.ref,
            baCreationDate: block.creationDate,    // ballot creation date, extracted from container block.creationDate
            baReceiveDate,
            baVoter: ballot.voter,
            baVoterShares: shares,
            baVote: ballot.vote,
            baComment: ballot.dComment,
            baCreationDateDiff: utils.timeDiff(dateOfStartVoting, block.creationDate).asMinutes,
            baReceiveDateDiff
        });
    }


    /**
     * 
     * @param {*} ballot 
     * if the payment for ballot costs has problem(double spend or ...) the recorded ballot must be removed from DB
     * this kind of errors are discovered in importing utxo step(12 hours after recording ballot in DAG & local database)
     */
    static removeBallot(ballotHash) {
        clog.app.error(`removeBallot(${ballotHash}`);
        pollHandler.ballotHandler.removeBallot(ballotHash);

    }

}


module.exports = BallotsInRelatedBlock;
