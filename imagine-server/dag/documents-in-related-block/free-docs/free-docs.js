const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const freeDocHandler = require('../../../services/free-doc-handler/free-doc-handler');


class FreeDocsInRelatedBlock {

    static customPostSpecialTreatments(args) {
        clog.app.info(`custom Post Special Treatments: ${utils.stringify(args)}`);
        return freeDocHandler.customTreatments(args);
    }

    static validateFreeDocs(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let fDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.CPost) ? grpdDocuments[iConsts.DOC_TYPES.CPost] : [];
        if (fDocs.length == 0)
            return {
                err: false,
            }

        let res = this.controlFDocs(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved fDocs!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aFDoc of fDocs) {
            if (!approvedDocHashes.includes(aFDoc.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) freeDoc(${utils.hash6c(aFDoc.hash)}), freeDoc is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }


        return {
            err: false,
        }

    }

    static validatePOWFreeDocs(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let fDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.CPost) ? grpdDocuments[iConsts.DOC_TYPES.CPost] : [];
        if (fDocs.length == 0)
            return {
                err: false,
            }

        let res = this.controlPOWFDocs(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved fDocs!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aFDoc of fDocs) {
            if (!approvedDocHashes.includes(aFDoc.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) freeDoc(${utils.hash6c(aFDoc.hash)}), freeDoc is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }


        return {
            err: false,
        }

    }

    static controlFDocs(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;

        let trxDict = args.trxDict;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;

        let ApprovedDocs = [];

        let fDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.CPost) ? grpdDocuments[iConsts.DOC_TYPES.CPost] : [];
        if (fDocs.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The NORMAL block has ${fDocs.length} fDocs`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        for (let aFDoc of fDocs) {
            console.log('\n\n aFDoc', utils.stringify(aFDoc));

            // control doc length limitation
            if (parseInt(aFDoc.dLen) > iConsts.MAX_DOC_LENGTH_BY_CHAR) {
                msg = `aFDoc.hash(${utils.hash6c(aFDoc.hash)}) exceeds allowed size(${aFDoc.dLen} > ${iConsts.MAX_DOC_LENGTH_BY_CHAR})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // controll document signature
            let dExtInfo;
            if (_.has(aFDoc, 'dExtInfo')) {
                dExtInfo = aFDoc.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aFDoc.hash]];
            }

            // control document hash
            let custHashRes = freeDocHandler.customHash({ fDoc: aFDoc })
            if ((custHashRes.err != false) || (aFDoc.hash != custHashRes.hash))
                return custHashRes;


            // validate signer (if exist)
            let custValRes = freeDocHandler.customValidate({ fDoc: aFDoc, dExtInfo });
            if (custValRes.err != false) {
                clog.sec.error(`custValRes: ${utils.stringify(custValRes)}`);
                return custValRes;
            }

            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aFDoc.hash]];
            if (utils._nilEmptyFalse(costPayerTrx)) {
                msg = `The freedoc(${utils.hash6c(aFDoc.hash)}) has not any cost payer!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aFDoc.hash) {
                msg = `The freedoc(${utils.hash6c(aFDoc.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // calculate doc register request cost
            let cPostCost = freeDocHandler.calcCostDCustomPost({
                cDate: creationDate,
                stage,
                cPost: aFDoc
            });
            if ((cPostCost.err != false) || (cPostCost.cost == 0))
                return cPostCost;

            // validate payment trx outputs  
            if (!_.has(costPayerTrxOutputs, 'TP_FDOC') ||
                (costPayerTrxOutputs['TP_FDOC'] < cPostCost.cost)) {
                msg = `referenced Trx(${utils.hash6c(costPayerTrx.hash)}) for free doc costs(${utils.hash6c(aFDoc.hash)}) 3has less payment ref(${utils.hash6c(costPayerTrx.ref)})! ${utils.sepNum(costPayerTrxOutputs['TP_FDOC'])} mcPAIs< ${utils.sepNum(cPostCost.cost)} mcPAIs`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            ApprovedDocs.push({
                _doc: aFDoc,
                _docHash: aFDoc.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aFDoc.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static controlPOWFDocs(args) {

        let msg;
        let stage = args.stage;
        let grpdDocuments = args.grpdDocuments;
        let block = args.block;
        let creationDate = block.creationDate;
        let docIndexByHash = args.docIndexByHash;


        let ApprovedDocs = [];

        let fDocs = _.has(grpdDocuments, iConsts.DOC_TYPES.CPost) ? grpdDocuments[iConsts.DOC_TYPES.CPost] : [];
        if (fDocs.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The POW block has ${fDocs.length} fDocs`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        for (let aFDoc of fDocs) {
            console.log('\n\n aFDoc', utils.stringify(aFDoc));

            // control doc length limitation
            if (parseInt(aFDoc.dLen) > iConsts.MAX_DOC_LENGTH_BY_CHAR) {
                msg = `aFDoc.hash(${utils.hash6c(aFDoc.hash)}) exceeds allowed size(${aFDoc.dLen} > ${iConsts.MAX_DOC_LENGTH_BY_CHAR})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // controll document signature
            let dExtInfo;
            if (_.has(aFDoc, 'dExtInfo')) {
                dExtInfo = aFDoc.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aFDoc.hash]];
            }

            // control document hash
            let custHashRes = freeDocHandler.customHash({ fDoc: aFDoc })
            if ((custHashRes.err != false) || (aFDoc.hash != custHashRes.hash))
                return custHashRes;


            // validate signer (if exist)
            let custValRes = freeDocHandler.customValidate({ fDoc: aFDoc, dExtInfo });
            if (custValRes.err != false) {
                clog.sec.error(`custValRes: ${utils.stringify(custValRes)}`);
                return custValRes;
            }

            // calculate doc register request cost
            let cPostCost = freeDocHandler.calcCostDCustomPost({
                cDate: creationDate,
                stage,
                cPost: aFDoc
            });
            if ((cPostCost.err != false) || (cPostCost.cost == 0))
                return cPostCost;

            ApprovedDocs.push({
                _doc: aFDoc,
                _docHash: aFDoc.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aFDoc.dExtHash
            });
        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendFreeDocsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlFDocs(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static appendPOWFreeDocsToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.controlPOWFDocs(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }




    /**
     * 
     * @param {*} iName 
     * if the payment for iName costs has problem(double spend or ...) the activated iName must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeINameBecauseOfPaymentsFail(iNameHash) {
        clog.app.error(`removeBallot(${iNameHash}`);
        return freeDocHandler.removeIName(iNameHash);
    }

}


module.exports = FreeDocsInRelatedBlock;
