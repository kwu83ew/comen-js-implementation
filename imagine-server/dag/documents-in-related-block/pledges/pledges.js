const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const loanHandler = require('../../../contracts/loan-contract/loan-contract');
const pPledgeHandler = require('../../../contracts/pledge-contract/proposal-pledge-contract');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const myContractsHandler = require('../../../services/my-contracts/my-contracts-handler');
const pldegeHandler = require('../../../contracts/pledge-contract/pledge-handler');

class PledgesInRelatedBLock {

    static validatePledges(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let pledges = _.has(grpdDocuments, iConsts.DOC_TYPES.Pledge) ? grpdDocuments[iConsts.DOC_TYPES.Pledge] : [];
        if (pledges.length == 0)
            return {
                err: false,
            }

        let res = this.preparePledges(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved pledge!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aPledge of pledges) {
            if (!approvedDocHashes.includes(aPledge.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) pledge(${utils.hash6c(aPledge.hash)}), pledge is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }

    }

    static preparePledges(args) {
        let msg;
        let grpdDocuments = args.grpdDocuments;
        let docIndexByHash = args.docIndexByHash;
        let block = args.block;
        let creationDate = block.creationDate;
        let stage = args.stage;
        let trxDict = args.trxDict;
        // let trxDict = {};
        // let trxs = grpdDocuments[iConsts.DOC_TYPES.BasicTx];
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;
        // for (let aTrx of trxs) {
        //     trxDict[aTrx.hash] = aTrx;
        // }
        let nowT = utils.getNow();

        let ApprovedDocs = [];

        let pledges = _.has(grpdDocuments, iConsts.DOC_TYPES.Pledge) ? grpdDocuments[iConsts.DOC_TYPES.Pledge] : [];
        if (pledges.length == 0)
            return {
                err: false,
                ApprovedDocs
                // docsHashes,
                // externalInfoHashes,
                // block
            }

        let proposalsDict = {}
        if (_.has(grpdDocuments, iConsts.DOC_TYPES.DNAProposal)) {
            for (let aProp of grpdDocuments[iConsts.DOC_TYPES.DNAProposal]) {
                proposalsDict[aProp.hash] = aProp;
            }
        }


        msg = `The NORMAL block will have ${pledges.length} documents of Pleadge`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        for (let aPledge of pledges) {
            console.log('\n\n aPledge', utils.stringify(aPledge));

            // control pledge hash
            if (aPledge.hash != pPledgeHandler.calcHashDPledge(aPledge)) {
                msg = `aPledge.hash(${utils.hash6c(aPledge.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // control dates
            if (aPledge.pledgerSignDate > creationDate) {
                msg = `pledgerSignDate(${aPledge.pledgerSignDate}) is newer than block creationdate(${creationDate})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.pledgerSignDate > nowT) {
                msg = `pledgerSignDate(${aPledge.pledgerSignDate}) is newer than block now(${nowT})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.pledgeeSignDate > creationDate) {
                msg = `pledgeeSignDate(${aPledge.pledgeeSignDate}) is newer than block creationdate(${creationDate})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.pledgeeSignDate > nowT) {
                msg = `pledgeeSignDate(${aPledge.pledgeeSignDate}) is newer than block now(${nowT})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.pledgerSignDate > aPledge.pledgeeSignDate) {
                msg = `pledgerSignDate(${aPledge.pledgerSignDate}) is newer than pledgeeSignDate(${aPledge.pledgeeSignDate})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // TODO: add arbiter signature date & validity controls too

            let unlockSet, isValidUnlock, signMsg;

            // controll document signature
            let dExtInfo;
            if (_.has(aPledge, 'dExtInfo')) {
                dExtInfo = aPledge.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aPledge.hash]];
            }

            // length control already done in document-groupping-phase

            unlockSet = dExtInfo.pledgerUSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aPledge.pledger,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `prepare Pledges, Invalid! given pledger unlock structure for pledge req, unlock:${utils.stringify({
                    address: aPledge.pledger,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // pledger signature & permission validate check
            signMsg = pPledgeHandler.getSignMsgAsPledger({ pledge: aPledge, dExtInfo });
            let permitedToPledge = iConsts.CONSTS.NO;
            for (let signInx = 0; signInx < dExtInfo.pledgerSignatures.length; signInx++) {
                let aSignature = dExtInfo.pledgerSignatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the Pledge to pledgee(${aPledge.pledgee}) has invalid pledger signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify pledger Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
                if (unlockSet.sSets[signInx].pPledge == iConsts.CONSTS.YES)
                    permitedToPledge = iConsts.CONSTS.YES;
            }
            if (permitedToPledge != iConsts.CONSTS.YES) {
                msg = `Block creating, the signer of Pledge to pledgee(${aPledge.pledgee}) has not permited to pledge`;
                clog.sec.error(msg);
                return { err: true, msg: msg, shouldPurgeMessage: true }
            }


            // controll pledgee signature
            unlockSet = dExtInfo.pledgeeUSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aPledge.pledgee,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `Block creating, Invalid! given unlock pledgee structure for pledge req, unlock:${utils.stringify({
                    address: aPledge.pledgee,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            // pledger signature
            signMsg = pPledgeHandler.getSignMsgAsPledgee({ pledge: aPledge, dExtInfo });
            for (let signInx = 0; signInx < dExtInfo.pledgeeSignatures.length; signInx++) {
                let aSignature = dExtInfo.pledgeeSignatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the Pledge to pledgee(${aPledge.pledgee}) has invalid pledgee signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify pledgee Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
            }


            // control if the trx of doc cost payment is valid
            if (aPledge.redeemTerms.principal < 0) {
                msg = `loan prinsipal(${utils.microPAIToPAI6(aPledge.redeemTerms.principal)}) is less than zero!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.redeemTerms.repaymentAmount < 0) {
                msg = `Repayment value(${utils.microPAIToPAI6(pledge.redeemTerms.repaymentAmount)}) is less than zero!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.redeemTerms.annualInterest < 0) {
                msg = `annualInterest(${aPledge.redeemTerms.annualInterest}) is less than zero!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            if (aPledge.redeemTerms.repaymentSchedule < 0) {
                msg = `repaymentSchedule(${aPledge.redeemTerms.repaymentSchedule}) is less than zero!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }
            let loanDtl = loanHandler.calcLoanRepayments({
                principal: aPledge.redeemTerms.principal,
                annualInterest: aPledge.redeemTerms.annualInterest,
                repaymentAmount: aPledge.redeemTerms.repaymentAmount,
                repaymentSchedule: aPledge.redeemTerms.repaymentSchedule
            });
            if (loanDtl.err != false) {
                return loanDtl;
            }

            // if proposal & pledge cost is payed?
            let proposalCostPayerTrx = trxDict[aPledge.trx];
            let proposalCostPayerTrxOutputs = {}
            for (let anOutput of proposalCostPayerTrx.outputs) {
                proposalCostPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            let pledgeDocCostPayerTrx = trxDict[mapTrxRefToTrxHash[aPledge.hash]];
            let pledgeDocCostPayerTrxOutputs = {}
            for (let anOutput of pledgeDocCostPayerTrx.outputs) {
                pledgeDocCostPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }

            // if pledge is PledgeP class, 
            if (aPledge.dClass == iConsts.PLEDGE_CLASSES.PledgeP) {
                // do a bunch of control on pledgeRequest

                if (!_.has(proposalsDict, aPledge.proposalRef)) {
                    msg = `referenced proposal(${utils.hash6c(aPledge.proposalRef)}) for Pledge(${utils.hash6c(aPledge.hash)}) does not exist in block!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                let proposal = proposalsDict[aPledge.proposalRef];
                let dplValidateRes = pPledgeHandler.validatePledgerSignedRequest({
                    proposal,
                    pledge: aPledge,
                    dExtInfo,
                    stage,
                    blockCreationDate: block.creationDate
                });
                if (dplValidateRes.err != false)
                    return dplValidateRes;

                // validate payment trx
                if (proposalCostPayerTrx.ref != aPledge.proposalRef) {
                    msg = `referenced Trx(${utils.hash6c(aPledge.trx)}) for Pledge Proposal costs(${utils.hash6c(aPledge.proposalRef)}) has different payment ref(${utils.hash6c(proposalCostPayerTrx.ref)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                // validate payment trx outputs  
                if (!_.has(proposalCostPayerTrxOutputs, 'TP_PROPOSAL') ||
                    (proposalCostPayerTrxOutputs['TP_PROPOSAL'] != aPledge.redeemTerms.principal)) {
                    msg = `the proposal costs(${utils.microPAIToPAI6(proposalCostPayerTrxOutputs['TP_PROPOSAL'])}) `;
                    msg += `referenced Trx(${aPledge.trx}) for pledge Proposal(${utils.hash6c(aPledge.proposalRef)}) `;
                    msg += `has different amount(${utils.microPAIToPAI6(aPledge.redeemTerms.principal)})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
                let pldgeContractCost = pPledgeHandler.calcPldgeDocumentCost({
                    cDate: creationDate,
                    stage,
                    pledge: aPledge,
                    repaymentsNumber: dplValidateRes.repaymentsNumber
                });
                if (!_.has(pledgeDocCostPayerTrxOutputs, 'TP_PLEDGE') ||
                    (pledgeDocCostPayerTrxOutputs['TP_PLEDGE'] < pldgeContractCost.cost)) {
                    msg = `the remote pledge contract costs(${pledgeDocCostPayerTrxOutputs['TP_PLEDGE']}) referenced Trx(${utils.hash6c(aPledge.trx)}) `;
                    msg += `for pledge-doc contract(${utils.hash6c(aPledge.proposalRef)}) `;
                    msg += `has different than locally recalculated amount(${pldgeContractCost.cost}) !`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            } else {
                msg = `System doesn't support dClass='${aPledge.dClass}!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            ApprovedDocs.push({
                _doc: aPledge,
                _docHash: aPledge.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aPledge.dExtHash
            });

        }


        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendPledgesToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.preparePledges(args);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static activatePledge(args) {
        clog.app.info(`applyPledge args: ${utils.stringify(args)}`);
        pPledgeHandler.insertAPledge(args);
        let pledge = args.pledge;


        // let addInfo = walletAddressHandler.getAddressesInfoSync([pledge.pledger, pledge.pledgee, pledge.arbiter]);
        // if (addInfo.length == 0)
        //     return { err: false }    // non of these 3 address not controlled by machine wallet
        // let addDict = {};
        // for (let anAdd of addInfo) {
        //     addDict[anAdd.waAddress] = anAdd;
        // }
        let signerInfo = pldegeHandler.recognizeSignerTypeInfo({ pledge });
        let desc;
        desc = `The contract(${utils.hash16c(pledge.hash)}) `
        if (signerInfo.signerType == 'pledgee') {
            desc += `pledger(${iutils.shortBech16(pledge.pledger)}) pledgee(You: ${iutils.shortBech16(pledge.pledgee)}) arbiter(${iutils.shortBech16(pledge.arbiter)})`;
        } else if (signerInfo.signerType == 'arbiter') {
            desc += `pledger(${iutils.shortBech16(pledge.pledger)}) pledgee(${iutils.shortBech16(pledge.pledgee)}) arbiter(You: ${iutils.shortBech16(pledge.arbiter)})`;
        } else if (signerInfo.signerType == 'pledger') {
            desc += `pledger(You: ${iutils.shortBech16(pledge.pledger)}) pledgee(${iutils.shortBech16(pledge.pledgee)}) arbiter(${iutils.shortBech16(pledge.arbiter)})`;
        }

        // insert pledge contract in machine local db (if controlled by machine)
        myContractsHandler.addContractToDb({
            lcType: pledge.dType,
            lcClass: pledge.dClass,
            lcRefHash: pledge.hash,
            lcDescriptions: desc,
            lcBody: pledge
        })


    }

    /**
     * 
     * @param {*} pledge 
     * if the payment for pledge or proposal(in case of pledgeP) has problem(double spend or ...) the activated pledge must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removePledgeBecauseOfPaymentsFail(pledgeHash) {
        clog.app.error(`remove PledgeBecauseOfPaymentsFail(${pledgeHash}`);
        pPledgeHandler.removePledgeBecauseOfPaymentsFail(pledgeHash);
    }

}


module.exports = PledgesInRelatedBLock;
