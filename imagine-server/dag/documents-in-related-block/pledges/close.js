const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const clog = require('../../../loggers/console_logger');
const crypto = require('../../../crypto/crypto');
const loanHandler = require('../../../contracts/loan-contract/loan-contract');
const closePledgeHandler = require('../../../contracts/pledge-contract/pledge-close');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const myContractsHandler = require('../../../services/my-contracts/my-contracts-handler');
const pPledgeHandler = require('../../../contracts/pledge-contract/proposal-pledge-contract');
const pldegeHandler = require('../../../contracts/pledge-contract/pledge-handler');
const closepldegeHandler = require('../../../contracts/pledge-contract/pledge-close');

class ClosePledgesInRelatedBlock {

    static validateClosePledges(args) {
        let msg;
        let grpdDocuments = _.has(args, 'grpdDocuments') ? args.grpdDocuments : {};
        let block = args.block;
        let blockHash = block.blockHash;
        let closePledges = _.has(grpdDocuments, iConsts.DOC_TYPES.ClosePledge) ? grpdDocuments[iConsts.DOC_TYPES.ClosePledge] : [];
        if (closePledges.length == 0)
            return {
                err: false,
            }

        let res = this.prepareClosePledges(args);
        if (res.err != false)
            return res;

        let approvedDocHashes = res.ApprovedDocs.map(x => x._docHash);
        if (approvedDocHashes.length != utils.arrayUnique(approvedDocHashes).length) {
            msg = `Block(${utils.hash6c(blockHash)}) has duplicated approved close-pledge!`;
            return { err: true, msg, shouldPurgeMessage: true }
        };

        // control if all documents are approved
        for (let aCPledge of closePledges) {
            if (!approvedDocHashes.includes(aCPledge.hash)) {
                msg = `Block(${utils.hash6c(blockHash)}) closePledge(${utils.hash6c(aCPledge.hash)}), closePledge is not approved!`;
                return { err: true, msg, shouldPurgeMessage: true }
            }
        }

        return {
            err: false,
        }

    }

    static prepareClosePledges(args) {
        let msg;
        let grpdDocuments = args.grpdDocuments;
        let docIndexByHash = args.docIndexByHash;
        let mapTrxRefToTrxHash = args.mapTrxRefToTrxHash;
        let trxDict = args.trxDict;
        let block = args.block;
        let creationDate = block.creationDate;
        let stage = args.stage;

        let nowT = utils.getNow();

        let ApprovedDocs = [];

        let closePledges = _.has(grpdDocuments, iConsts.DOC_TYPES.ClosePledge) ? grpdDocuments[iConsts.DOC_TYPES.ClosePledge] : [];
        if (closePledges.length == 0)
            return {
                err: false,
                ApprovedDocs
            }

        msg = `The NORMAL block will have ${closePledges.length} documents of ClosePleadge`;
        clog.app.info(msg);

        for (let aCPledge of closePledges) {
            console.log('\n\n aCPledge', utils.stringify(aCPledge));

            // control pledge hash
            if (aCPledge.hash != closePledgeHandler.calcHashDPledgeClose(aCPledge)) {
                msg = `aCPledge.hash(${utils.hash6c(aCPledge.hash)}) is not same as locally recalculated`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }


            let unlockSet, isValidUnlock, signMsg;
            let dExtInfo;
            if (_.has(aCPledge, 'dExtInfo')) {
                dExtInfo = aCPledge.dExtInfo;
            } else {
                dExtInfo = block.bExtInfo[docIndexByHash[aCPledge.hash]];
            }

            // length control already done in document-groupping-phase

            unlockSet = dExtInfo.uSet;
            isValidUnlock = mOfNHandler.validateSigStruct({
                address: aCPledge.concluder,
                uSet: unlockSet
            });
            if (isValidUnlock != true) {
                msg = `prepare Pledges, Invalid! given pedgee/arbiter unlock structure for close-pledge req, unlock:${utils.stringify({
                    address: aCPledge.concluder,
                    unlockSet
                })}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // control if concluder is the person in referenced pledge contract?
            let refPledge = pldegeHandler.searchInPledgedAccounts({
                query: [['pgd_hash', aCPledge.ref]]
            });
            if (refPledge.length != 1)
                return { err: true, msg: `referenced pledge-contract(${utils.hash6c(aCPledge.ref)}) to close, is not valid ${utils.stringify(refPledge)}` }

            refPledge = refPledge[0];

            if (![iConsts.PLEDGE_CLASSES.PledgeP].includes(refPledge.pgdClass)) {
                msg = `System still doesn't support closing dClass='${refPledge.pgdClass}!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            let controlled = false;
            switch (aCPledge.concludeType) {
                case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee:
                    controlled = true;
                    if (aCPledge.concluder != refPledge.pgdPledgee)
                        return { err: true, msg: `the concluder of pledge-close-contract(${utils.hash6c(aCPledge.hash)}), is not pledgee of referenced-pledge-contract ${utils.stringify(refPledge)}` }
                    break;

                case iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter:
                    controlled = true;
                    if (aCPledge.concluder != refPledge.pgdArbiter)
                        return { err: true, msg: `the concluder of pledge-close-contract(${utils.hash6c(aCPledge.hash)}), is not arbiter of referenced-pledge-contract ${utils.stringify(refPledge)}` }
                    break;

                case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger:
                    controlled = true;
                    if (aCPledge.concluder != refPledge.pgdPledger)
                        return { err: true, msg: `the concluder of pledge-close-contract(${utils.hash6c(aCPledge.hash)}), is not pledger of referenced-pledge-contract ${utils.stringify(refPledge)}` }
                    break;
            }
            if (!controlled)
                return { err: true, msg: `the concludeType of pledge-close-contract(${utils.hash6c(aCPledge.hash)}), is UNKNOWN ${utils.stringify(aCPledge)}` }


            // concluder signature & permission validate check
            signMsg = closePledgeHandler.getSignMsgDForCloseContract(aCPledge);
            let permitedToClosePledge = iConsts.CONSTS.NO;
            for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
                let aSignature = dExtInfo.signatures[signInx];
                try {
                    let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                    if (!verifyRes) {
                        msg = `Block creating, the close-Pledge(${utils.hash6c(aCPledge.hash)}) has invalid concluder signature`;
                        return { err: true, msg, shouldPurgeMessage: true }
                    }
                } catch (e) {
                    msg = `error in verify pledge-close Signature: ${utils.stringify(args)}`;
                    clog.trx.error(msg);
                    clog.trx.error(e);
                    return { err: true, msg, shouldPurgeMessage: true };
                }
                if (unlockSet.sSets[signInx].pPledge == iConsts.CONSTS.YES)
                    permitedToClosePledge = iConsts.CONSTS.YES;
            }
            if (permitedToClosePledge != iConsts.CONSTS.YES) {
                msg = `Block creating, the close-Pledge(${utils.hash6c(aCPledge.hash)}) has not permited to close-pledge`;
                clog.sec.error(msg);
                return { err: true, msg: msg, shouldPurgeMessage: true }
            }


            // // control if concluding cost is payed by a transaction?
            // if (!_.has(mapTrxRefToTrxHash, aCPledge.hash)) {
            //     msg = `block(${utils.hash6c(block.blockHash)}) No one pays conclude pledge costs(${utils.hash6c(aCPledge.hash)})!`;
            //     clog.sec.error(msg);
            //     return { err: true, msg, shouldPurgeMessage: true }
            // }

            let costPayerTrx = trxDict[mapTrxRefToTrxHash[aCPledge.hash]];
            let costPayerTrxOutputs = {}
            for (let anOutput of costPayerTrx.outputs) {
                costPayerTrxOutputs[anOutput[0]] = anOutput[1];
            }
            // control if the trx of doc cost payment is valid
            if (costPayerTrx.ref != aCPledge.hash) {
                msg = `block(${utils.hash6c(blockHash)}) The close-pldg(${utils.hash6c(aCPledge.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            // if ref-pledge is PledgeP class, 
            if (refPledge.pgdClass == iConsts.PLEDGE_CLASSES.PledgeP) {
                let signerInfo = pldegeHandler.recognizeSignerTypeInfo({
                    pledge: {
                        pledger: refPledge.pgdPledger,
                        pledgee: refPledge.pgdPledgee,
                        arbiter: refPledge.pgdArbiter,
                    },
                    signerAddress: aCPledge.concluder
                })
                // validate payment trx outputs  
                let closingCost = closepldegeHandler.calcClosePldgCost({
                    cDate: creationDate,
                    stage,
                    closePledgeDoc: aCPledge,
                    byType: signerInfo.byType
                });
                if (closingCost.err != false)
                    return closingCost;

                if (!_.has(costPayerTrxOutputs, 'TP_PLEDGE_CLOSE') ||
                    (costPayerTrxOutputs['TP_PLEDGE_CLOSE'] != closingCost.cost)) {
                    msg = `the pledge close costs(${costPayerTrxOutputs['TP_PLEDGE_CLOSE']}) referenced Trx(${aCPledge.trx}) for pledge(${utils.hash6c(aCPledge.ref)}) has different amount(${closingCost.cost})!`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

            } else {
                msg = `System still doesn't support, closing dClass='${refPledge.pgdClass}!`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true }
            }

            ApprovedDocs.push({
                _doc: aCPledge,
                _docHash: aCPledge.hash,
                _dExtInfo: dExtInfo,
                _dExtHash: aCPledge.dExtHash
            });

        }

        return {
            err: false,
            ApprovedDocs
        }
    }

    static appendClosePledgesToBlock(args) {
        let block = args.block;
        let docsHashes = args.docsHashes;
        let externalInfoHashes = args.externalInfoHashes;

        args.stage = iConsts.STAGES.Creating;
        let res = this.prepareClosePledges(args);
        console.log(`prepareClosePledges res: ${utils.stringify(res)}`);
        if (res.err != false)
            return res;

        if (res.ApprovedDocs.length > 0) {
            for (let aDoc of res.ApprovedDocs) {
                block.bExtInfo.push(aDoc._dExtInfo);
                delete aDoc._doc.dExtInfo;
                block.docs.push(aDoc._doc);
                docsHashes.push(aDoc._docHash);
                externalInfoHashes.push(aDoc._dExtHash);
            }
        }

        return {
            err: false,
            docsHashes,
            externalInfoHashes,
            block,
            addedDocs: res.ApprovedDocs.length
        }
    }

    static applyPledgeClosing(args) {
        clog.app.info(`applyPledge args: ${utils.stringify(args)}`);
        return closepldegeHandler.doApplyClosingPledge(args);


        // // let addInfo = walletAddressHandler.getAddressesInfoSync([pledge.pledger, pledge.pledgee, pledge.arbiter]);
        // // if (addInfo.length == 0)
        // //     return { err: false }    // non of these 3 address not controlled by machine wallet
        // // let addDict = {};
        // // for (let anAdd of addInfo) {
        // //     addDict[anAdd.waAddress] = anAdd;
        // // }
        // let signerInfo = pldegeHandler.recognizeSignerTypeInfo({ pledge });
        // let desc;
        // desc = `The contract(${utils.hash16c(pledge.hash)}) `
        // if (signerInfo.signerType == 'pledgee') {
        //     desc += `pledger(${iutils.shortBech16(pledge.pledger)}) pledgee(You: ${iutils.shortBech16(pledge.pledgee)}) arbiter(${iutils.shortBech16(pledge.arbiter)})`;
        // } else if (signerInfo.signerType == 'arbiter') {
        //     desc += `pledger(${iutils.shortBech16(pledge.pledger)}) pledgee(${iutils.shortBech16(pledge.pledgee)}) arbiter(You: ${iutils.shortBech16(pledge.arbiter)})`;
        // } else if (signerInfo.signerType == 'pledger') {
        //     desc += `pledger(You: ${iutils.shortBech16(pledge.pledger)}) pledgee(${iutils.shortBech16(pledge.pledgee)}) arbiter(${iutils.shortBech16(pledge.arbiter)})`;
        // }

        // // insert pledge contract in machine local db (if controlled by machine)
        // myContractsHandler.addContractToDb({
        //     lcType: pledge.dType,
        //     lcClass: pledge.dClass,
        //     lcRefHash: pledge.hash,
        //     lcDescriptions: desc,
        //     lcBody: pledge
        // })


    }

    /**
     * 
     * @param {*} pledge 
     * if the payment for pledge or proposal(in case of pledgeP) has problem(double spend or ...) the activated pledge must be removed from DB
     * this kind of errors are discovered in importing utxo step
     */
    static removeClosePledgeBecauseOfPaymentsFail(pgdHash) {
        clog.app.error(`remove ClosePledgeBecauseOfPaymentsFail(${pgdHash}`);
        closePledgeHandler.reOpenPledgeBecauseOfPaymentsFail({ pgdHash });
    }

}


module.exports = ClosePledgesInRelatedBlock;
