// module.exports.initGenesis = ;

module.exports.extractBlockHashableParts = require("./hashable").extractBlockHashableParts;
module.exports.hashBlock = require("./hashable").hashBlock;
module.exports.calculateDocsRootHash = require("./hashable").calculateDocsRootHash;
module.exports.calculateDocsRootHash = require("./hashable").calculateDocsRootHash;
// module.exports.calculateClonedTransactionsRootHash = require("./hashable").calculateClonedTransactionsRootHash;
module.exports.blockPresenter = require("./block-presenter");