const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require("../utils/utils");
const model = require("../models/interface");
const clog = require('../loggers/console_logger');

const table = 'i_kvalue';

class LeavesHandler {
    /**
     * 
     * @param {block hash} hash 
     * @param {creation date} date 
     * @param {block bType} bType 
     * 
     */
    static addToLeaveBlocks(hash, date, bType = iConsts.BLOCK_TYPES.Normal) {
        let current = this.getLeaveBlocks();
        current[hash] = { date, bType };
        current = JSON.stringify(current);
        model.sUpsert({
            table,
            idField: 'kv_key',
            idValue: 'dag_leave_blocks',
            updates: { kv_value: current, kv_last_modified: utils.getNow() }
        });
    }

    static removeFromLeaveBlocks(hashes = []) {
        let current = this.getLeaveBlocks();
        let newCurrent = {};
        _.forOwn(current, (value, key) => {
            if (!hashes.includes(key)) {
                newCurrent[key] = value;
            }
        });
        current = JSON.stringify(newCurrent);
        model.sUpsert({
            table,
            idField: 'kv_key',
            idValue: 'dag_leave_blocks',
            updates: { kv_value: current, kv_last_modified: utils.getNow() }
        });
    }

    static initLeaveBlocks(hash, date, bType = iConsts.BLOCK_TYPES.Normal) {
        let current = this.getLeaveBlocks();
        if (utils.objKeys(current).length > 0) {
            this.addToLeaveBlocks(hash, date, bType);

        } else {
            let d = {}
            d[hash] = { date: date, bType };
            model.sUpsert({
                table,
                idField: 'kv_key',
                idValue: 'dag_leave_blocks',
                updates: { kv_value: JSON.stringify(d), kv_last_modified: utils.getNow() }
            });
        }
    }

    static resetLeaveBlocks() {
        model.sUpsert({
            table,
            idField: 'kv_key',
            idValue: 'dag_leave_blocks',
            updates: { kv_value: JSON.stringify({}), kv_last_modified: utils.getNow() }
        });
    }


    static getLeaveBlocks(args) {

        let res = model.sRead({
            table,
            fields: ['kv_value'],
            query: [
                ['kv_key', 'dag_leave_blocks']
            ]
        });

        if (res.length == 0)
            return {};

        res = JSON.parse(res[0].kv_value);

        let maxCreationDate = _.has(args, 'maxCreationDate') ? args.maxCreationDate : null;
        // filter by maxCreationdate
        if (!utils._nilEmptyFalse(maxCreationDate)) {
            let filteredRes = {}
            for (let key of utils.objKeys(res)) {
                if ((res[key].bType == iConsts.BLOCK_TYPES.Genesis) || (res[key].date < maxCreationDate))
                    filteredRes[key] = res[key]
            }
            res = filteredRes
        }

        return res;
    }

    static getFreshLeaves() {
        // the leaves younger than two cylce (24 hours) old
        let leaves = this.getLeaveBlocks();
        let msg;

        clog.app.info(`leaves ${JSON.stringify(leaves)}`);
        if (utils._nilEmptyFalse(leaves))
            return {};

        let now = utils.getNow();
        let leaveAge;
        let out = {};
        _.forOwn(leaves, (value, key) => {
            leaveAge = utils.timeDiff(value.date, now).asMinutes;
            msg = `leave age(${leaveAge})minutes is ${(leaveAge < iConsts.getCycleByMinutes() * 2) ? 'younger' : 'older'} than 2 cycle `;
            clog.cb.info(msg);
            if (leaveAge < iConsts.getCycleByMinutes() * 2)
                out[key] = value;
        });
        return out;
    }

    static hasFreshLeaves() {
        let freshLeaves = utils.objKeys(this.getFreshLeaves());
        let freshLeavesStr = freshLeaves.sort().map(x => utils.hash6c(x)).join();
        let msg = `Found ${utils.objLength(freshLeaves)} freshLeave(s): ${freshLeavesStr} `;
        clog.app.info(msg);
        clog.cb.info(msg);
        if (utils.objLength(freshLeaves) > 0)
            return true;
        return false;
    }
}

module.exports = LeavesHandler;