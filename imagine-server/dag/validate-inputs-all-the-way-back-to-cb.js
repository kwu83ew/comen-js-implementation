const _ = require('lodash');
const iConsts = require('../config/constants');
const clog = require('../loggers/console_logger');
const utxoHandler = require('../transaction/utxo/utxo-handler');
const dagHandler = require('./graph-handler/dag-handler');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const extinfosHandler = require('../dag/extinfos-handler');
const blockUtils = require('../dag/block-utils');

let level = 0;
let levelAnc = 0;
let coinTrack = [];
let isSuperValidateDebug = true;


let resObj1 = {
    calledAncestors: [],
    ref: null,
    err: false,
    msg: ''
}
function setter1(args) {
    utils.objKeys(args).forEach(key => {
        resObj1[key] = args[key];
    });

    // if (_.has(args, 'ref'))
    //     resObj1.ref = args.ref;

    // if (_.has(args, 'err'))
    //     resObj1.err = args.err;

    // if (_.has(args, 'msg'))
    //     resObj1.msg = args.msg;

}
function getter1(k = null) {
    if (k == null)
        return resObj1;

    // clog.trx.info(`SuperValidate, getter1111 called for key: ${k}`);
    if (_.has(resObj1, k))
        return resObj1[k];
    return null;
}


class SuperControlUntilCoinbaseMinting {

    /**
     * 
     * @param {*} spendBlock 
     * @param {*} interestedDocs 
     * going back all the way to find the Coinbase/RlBlock block for every spent inputs in given block
     */
    static trackingBackTheCoins(args) {
        let spendBlock = args.spendBlock;
        let interestedDocs = _.has(args, 'interestedDocs') ? args.interestedDocs : [];
        let interestedCoins = _.has(args, 'interestedCoins') ? args.interestedCoins : [];

        level = 0;
        coinTrack = [];
        if (isSuperValidateDebug) {
            clog.trx.info(`${level}.SCUUCM, Block(${utils.hash6c(spendBlock.blockHash)})`);
            clog.trx.info(`${level}.SCUUCM, spendBlock: ${utils.stringify(spendBlock)}`);
        }
        let res = SuperControlUntilCoinbaseMinting.trackingBackTheCoinsRecursive({
            spendBlock,
            interestedDocs,
            interestedCoins,
            descendent: `Generated`
        });
        if (isSuperValidateDebug) {
            clog.trx.info(`${level}.SCUUCM, coin Track: ${utils.stringify(coinTrack)}`);
        }
        res.coinTrack = coinTrack;
        return res;
    }

    static trackingBackTheCoinsRecursive(args) {
        let spendBlock = args.spendBlock;
        let descendent = args.descendent;
        let interestedDocs = args.interestedDocs;
        let interestedCoins = args.interestedCoins;

        let msg;
        level++;

        let traceBack = {
            level,
            bType: spendBlock.bType,
            creationDate: spendBlock.creationDate,
            blockHash: spendBlock.blockHash,
            descendent: descendent,
            interestedDocs: interestedDocs,
            interestedCoins: interestedCoins,
        };
        if ((level == 1) && (interestedDocs.length == 0)) {
            // log all docs as interested
            traceBack.interestedDocs = spendBlock.docs.map(x => x.hash);
        }
        if ((level == 1) && (interestedCoins.length == 0)) {
            // log all coins as interested
            if (_.has(spendBlock, 'docs')) {
                for (let aDoc of spendBlock.docs) {
                    if (_.has(aDoc, 'inputs')) {
                        for (let anInp of aDoc.inputs) {
                            traceBack.interestedCoins.push(iutils.packCoinRef(anInp[0], anInp[1]))
                        }
                    }
                }
            }
        }
        coinTrack.push(traceBack);


        // retrieve spend block extra info
        let bExtInfo;
        if (_.has(spendBlock, 'bExtInfo')) {
            bExtInfo = spendBlock.bExtInfo;
        } else {
            // bExtInfo = extinfosHandler.get BlockExtInfos(spendBlock.blockHash);
            let bExtInfoRes = extinfosHandler.getBlockExtInfos(spendBlock.blockHash);
            // if (bExtInfoRes.bExtInfo == null) {
            //     msg = `missed bExtInfo6 (${utils.hash16c(spendBlock.blockHash)})`;
            //     clog.sec.error(msg);
            //     return { err: true, msg }
            // }
            bExtInfo = bExtInfoRes.bExtInfo
        }


        for (let docInx = 0; docInx < spendBlock.docs.length; docInx++) {
            let doc = spendBlock.docs[docInx];

            if (isSuperValidateDebug) {
                clog.trx.info(`${level}.SCUUCM, spendBlock.blockHash: ${utils.hash6c(spendBlock.blockHash)} doc(${utils.hash6c(doc.hash)}) docInx(${docInx} / ${doc.dType})`);
            }

            // if it is not first level, so it MUST have certain interested docHash to trace back
            if ((level != 1) && !interestedDocs.includes(doc.hash))
                continue;

            /**
             * in first level we can ask for control of all/some docs in block
             * if is insisted to control certain docs and current doc is not one of mentioned docs, 
             * so contiue the loop
             */
            if ((level == 1) &&
                (interestedDocs.length > 0) &&
                (!interestedDocs.includes(doc.hash))
            ) {
                continue;
            }


            if (isSuperValidateDebug)
                clog.trx.info(`${level}.SCUUCM, control doc(${utils.hash6c(doc.hash)})`);


            if (!iutils.trxHasInput(doc.dType)) {
                /**
                 * transaction has not input, so it must be trx in which the fresh output was created. 
                 * either because of being Coinbase Trx or being the DPCostPay Trx
                 */
                if (iutils.trxHasNotInput(doc.dType)) {
                    if (interestedDocs.includes(doc.hash) || ((level == 1) && (interestedDocs.length == 0))) {
                        // we found one interested fresh coin creating location
                        coinTrack.push({
                            level,
                            bType: spendBlock.bType,
                            creationDate: spendBlock.creationDate,
                            blockHash: spendBlock.blockHash,
                            dType: doc.dType,
                            descendent: doc.hash,
                            interestedDocs: doc.hash,
                            interestedCoins: ['fresh coin']
                        });
                    }
                    continue;

                } else {
                    msg = `${level}.SCUUCM, block(${utils.hash6c(spendBlock.blockHash)}) doc(${utils.hash6c(doc.hash)}) must be Coinbase or DPCostPay and it is ${spendBlock.bType}/${doc.dType}`;
                    clog.sec.error(msg);
                    return { err: true, msg }
                }
            }

            // console.log(`${level}.SCUUCM, ext Infos: ${JSON.stringify(ext Infos)} of block(${utils.hash6c(spendBlock.blockHash)})`);
            // clog.trx.info(`${level}.SCUUCM, ext Infos: ${JSON.stringify(ext Infos)} of block(${utils.hash6c(spendBlock.blockHash)})`);
            let docExtInfos = _.has(bExtInfo, docInx) ? bExtInfo[docInx] : null;

            if (isSuperValidateDebug)
                clog.trx.info(`${level}.SCUUCM, 1, the doc(${utils.hash6c(doc.hash)}) has some inputs(${doc.inputs.length}) to trace back`);

            for (let inputIndex = 0; inputIndex < doc.inputs.length; inputIndex++) {
                let input = doc.inputs[inputIndex];

                let spendHash = input[0];
                let spendIndex = input[1];

                // mapping input ref to proper container block
                let refBlockHashes = dagHandler.getBlockHashesByDocHashes({ docsHashes: [spendHash] }).blockHashes;
                if (isSuperValidateDebug) {
                    clog.trx.info(`${level}.SCUUCM, docHash ${utils.hash6c(spendHash)}`);
                    clog.trx.info(`${level}.SCUUCM, refBlockHashes ${refBlockHashes.map(x => utils.hash6c(x))}`);
                }
                if (refBlockHashes.length == 0) {
                    msg = `${level}.SCUUCM the refloc ${input} does not mapped to it's container-block!`;
                    clog.sec.error(msg);
                    return { err: true, msg }
                }


                // another paranoic test
                let controlRefInHirarechy = true;
                if (controlRefInHirarechy) {
                    // go back to history by ancestors link to find reference location(which block creates this ref as output) for spendHash 

                    levelAnc = 0;
                    SuperControlUntilCoinbaseMinting.lookForDocByAnc({
                        vSetter: setter1,
                        vGetter: getter1,
                        block: spendBlock,
                        docHash: spendHash,
                        level
                    });
                    clog.trx.info(`${level}.SCUUCM, getter1 finalresssssssssssssssss ${JSON.stringify(getter1())}`);

                    if (getter1('err') == true) {
                        return {
                            err: getter1('err'),
                            msg: getter1('msg')
                        };
                    }

                    if (refBlockHashes.indexOf(getter1('ref')) == -1) {
                        msg = `${level}.SCUUCM, the docHash(${utils.hash6c(spendHash)}) which is used in doc(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(spendBlock.blockHash)}) 
                            is referring to 2 diff blocks by Ancestors(${getter1('ref')}) &  i_docs_blocks_map(${refBlockHashes.map(x => utils.hash6c(x))})`;
                        clog.sec.error(msg);
                        return { err: true, msg }
                    }

                }

                // retrieve container block info
                let refWBlocks = dagHandler.searchInDAGSync({
                    query: [
                        ['b_hash', ['IN', refBlockHashes]],
                    ]
                });
                if (refWBlocks.length != utils.arrayUnique(refBlockHashes).length) {
                    msg = `${level}.SCUUCM, some of the blocks(${refBlockHashes.map(x => utils.hash6c(x))}) do not exist in DAG (${refWBlocks.length})`;
                    clog.sec.error(msg);
                    return { err: true, msg }
                }
                for (let refWBlock of refWBlocks) {
                    // let refBlock = JSON.parse(refWBlock.body);
                    let refBlock = blockUtils.openDBSafeObject(refWBlock.bBody).content;

                    // clog.trx.info(`${level}.SCUUCM, minutessssssssssssssssssssssssssssssss: ${refBlock.creationDate}, ${spendBlock.creationDate} ${JSON.stringify(utils.timeDiff(refBlock.creationDate, spendBlock.creationDate))}`);
                    // defeniatelay refblock must be created AT LEAST one cycle before to be having spendable outputs
                    if (
                        (spendBlock.bType != iConsts.BLOCK_TYPES.RpBlock) &&
                        (utils.timeDiff(refBlock.creationDate, spendBlock.creationDate).asMinutes < iConsts.getCycleByMinutes())
                    ) {
                        msg = `${level}.SCUUCM, block(${utils.hash6c(spendBlock.blockHash)} ${spendBlock.creationDate}) `
                        msg += `uses an output of block(${utils.hash6c(refBlock.blockHash)} ${refBlock.creationDate}) before being maturated`;
                        clog.sec.error(msg);
                        return { err: true, msg }
                    }

                    // looking for refBlock.output = spendBlock.input
                    let inputExistInRefBlock = false;
                    for (let refDoc of refBlock.docs) {
                        if (refDoc.hash == spendHash) {
                            inputExistInRefBlock = true;

                            if (!iutils.trxHasNotInput(refDoc.dType)) {
                                // also output address of ref controls!
                                const mOfNHandler = require('../transaction/signature-structure-handler/general-m-of-n-handler');
                                let isValidUnlock = mOfNHandler.validateSigStruct({
                                    address: refDoc.outputs[spendIndex][0],
                                    uSet: docExtInfos[inputIndex].uSet
                                });
                                if (isValidUnlock != true) {
                                    msg = `${level}.SCUUCM, Invalid given unlock structure for trx:${spendHash} input:${inputIndex}`;
                                    clog.sec.error(msg);
                                    return { err: true, msg }
                                }
                            }

                            let matureCycles = iutils.getMatureCyclesCount(refDoc.dType)
                            if (matureCycles.err != false)
                                return matureCycles;
                            matureCycles = matureCycles.number;
                            if (matureCycles > 1) {
                                // control maturity
                                if (
                                    (![iConsts.BLOCK_TYPES.RpBlock].includes(spendBlock.bType)) &&
                                    (utils.timeDiff(refBlock.creationDate, spendBlock.creationDate).asMinutes < (matureCycles * iConsts.getCycleByMinutes()))
                                ) {
                                    msg = `${level}.SCUUCM, block(${utils.hash6c(spendBlock.blockHash)} ${spendBlock.creationDate}) `;
                                    msg += `uses an output(${refDoc.dType} ${utils.hash6c(refDoc.hash)}) of block(${utils.hash6c(refBlock.blockHash)} ${refBlock.creationDate}) `;
                                    msg += `before being maturated by ${matureCycles} cycles`;
                                    clog.sec.error(msg);
                                    return { err: true, msg }
                                }
                            }

                            // controlling the ref of ref
                            return SuperControlUntilCoinbaseMinting.trackingBackTheCoinsRecursive({
                                spendBlock: refBlock,
                                descendent: doc.hash,
                                interestedDocs: [refDoc.hash],
                                interestedCoins: []
                            });

                        }
                    }

                    if (!inputExistInRefBlock) {
                        msg = `${level}.SCUUCM, the input(${utils.hash6c(spendHash)}) which is used in block(${utils.hash6c(spendBlock.blockHash)}) do not exist in block(${utils.hash6c(refBlock.blockHash)})`;
                        clog.sec.error(msg);
                        return { err: true, msg }
                    }
                }
            }
        }
        return { err: false }
    }

    static lookForDocByAnc(args) {
        let vSetter = args.vSetter;
        vSetter({
            ref: null,
            calledAncestors: [],
            err: false,
            msg: ''
        });
        args.level = 0;
        SuperControlUntilCoinbaseMinting.lookForDocByAncRecursive(args);
    }

    static lookForDocByAncRecursive(args) {
        let vSetter = args.vSetter;
        let vGetter = args.vGetter;
        let block = args.block;
        let docHash = args.docHash;
        let level = _.has(args, 'level') ? args.level : 'u0';

        if (vGetter('ref') != null)
            clog.trx.info(`\n${level}.${levelAnc} SuperValidate, founded ref=${vGetter('ref')}`);


        levelAnc++;
        let msg;
        if (isSuperValidateDebug)
            clog.trx.info(`\n${level}.${levelAnc} SuperValidate, lookForDocByAnc Recursive: looking for docHash(${utils.hash6c(docHash)}) in block(${utils.hash6c(block.blockHash)})`);

        // check if invoked doc exist in this block
        if ((![iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote].includes(block.bType)) && _.has(block, 'docs') && (block.docs.map(x => x.hash).includes(docHash))) {
            clog.trx.info(`\n${level}.${levelAnc} SuperValidate, OK. Found ref for docHash(${utils.hash6c(docHash)}) in block(${utils.hash6c(block.blockHash)})`);
            vSetter({ ref: block.blockHash, err: false });
            return;
        }


        let ancestors = block.ancestors;
        if (isSuperValidateDebug)
            clog.trx.info(`${level}.${levelAnc} SuperValidate, loopiong on ancestors(${utils.stringify(ancestors)})`);

        if (ancestors.length == 0) {
            msg = `${level}.SCUUCM, block(${utils.hash6c(block.blockHash)}) has no ancestors(${block.bType})`; // (${JSON.stringify(block)}) \n\n and still can not find the doc(${docHash})`;
            clog.trx.info(msg);
            return;
        }
        for (let ancHash of ancestors) {
            let ancWBlocks = dagHandler.searchInDAGSync({
                fields: ['b_body'],
                query: [
                    ['b_hash', ancHash],
                ],
                log: false
            });
            if (ancWBlocks.length != 1) {
                msg = `${level}.SCUUCM, block(${utils.hash6c(block.blockHash)}) has an ancestor(${ancHash}) which doesn't exist in DAG (${ancWBlocks.length})`;
                clog.sec.error(msg);
                vSetter({ err: true, msg });
                return;
            }
            let ancWBlock = ancWBlocks[0];
            let ancBlock = blockUtils.openDBSafeObject(ancWBlock.bBody).content;
            if (isSuperValidateDebug)
                clog.trx.info(`${level}.${levelAnc} SuperValidate, refBlockHashFoundByAncestors ============(${JSON.stringify(vGetter('ref'))})`);

            if ((vGetter('ref') == null) && (vGetter('calledAncestors').indexOf(ancBlock.blockHash) == -1)) {
                let tmp = vGetter('calledAncestors')
                tmp.push(ancBlock.blockHash);
                vSetter({ calledAncestors: tmp })
                SuperControlUntilCoinbaseMinting.lookForDocByAncRecursive({
                    vSetter,
                    vGetter,
                    block: ancBlock,
                    docHash,
                    level
                });
            }
        };

    }
}

module.exports = SuperControlUntilCoinbaseMinting;
