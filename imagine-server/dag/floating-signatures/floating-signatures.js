const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const getFloatingSignatureTemplate = require('../../docs-structure/block/floating-signature');
const dagHandler = require('../graph-handler/dag-handler');
const DNAHandler = require('../../dna/dna-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const blockHasher = require('../hashable');
const machine = require('../../machine/machine-handler');
const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');
const crypto = require('../../crypto/crypto');
const kv = require('../../models/kvalue');
const listener = require('../../plugin-handler/plugin-handler');

class FloatingSignature {

    static launchSignCB() {
        // some preparations ...
        FloatingSignature.recursiveSignCB();
    }

    static recursiveSignCB() {
        setTimeout(FloatingSignature.recursiveSignCB, iConsts.ONE_MINUTE_BY_MILISECOND * iutils.getCoinbaseCheckGap());
        let res = FloatingSignature.maybeSignCoinbase();
    }

    static maybeSignCoinbase() {
        let msg;
        // if machine has minimum shares?
        let { backer, percentage } = DNAHandler.getMachineShares();
        if (percentage == null) {
            utils.exiter(`percentage==nullllllllllllllllllllllllll`, 123)
        }
        let cbInfo = iutils.getCoinbaseInfo({ cDate: utils.getNow() })

        let minShare = cnfHandler.getMinShareToAllowedSignCoinbase({ cDate: utils.getNow() });
        if (percentage < minShare) {
            clog.cb.info(`machine hasn't sufficient shares(min ${minShare}) to sign coinbase block`);
            return;
        }


        // search if current cycle coinbase block is recorded in DAG
        clog.cb.info(`search CB for current cycle(${cbInfo.cycleStamp} start fromHour ${cbInfo.fromHour})`);
        let existCBWBlocks = dagHandler.searchInDAGSync({
            fields: ['b_hash'],
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase],
                ['b_cycle', cbInfo.cycleStamp]
            ]
        });
        if (utils._nilEmptyFalse(existCBWBlocks) || (existCBWBlocks.length == 0)) {
            clog.cb.info(`current cycle(${cbInfo.cycleStamp} start fromHour ${cbInfo.fromHour}) still hasn't recorded coinbase block`);
            return;
        }

        // check if machine didn't sign already?
        let coinbasHashes = existCBWBlocks.map(x => x.bHash)
        clog.cb.info(`maybe Sign Coinbase for coinbasHashes(${utils.stringify(coinbasHashes.map(x => utils.hash6c(x)))})`);
        let alreadySigned = dagHandler.searchInDAGSync({
            fields: ['b_ancestors', 'b_backer'],
            query: [
                ['b_type', iConsts.BLOCK_TYPES.FSign],
                ['b_ancestors', ['LIKE:OR', coinbasHashes.map(x=>`%${x}%`)]]    // since each floating-signature block MUST be linked ONLY ONE ANCESTOR which is coinbase block
            ]
        });
        let alreadySignedCoinbases = {}
        for (let aFloat of alreadySigned) {
            let tmpAncs = utils.parse(aFloat.bAncestors);
            if (Array.isArray(tmpAncs)) {
                for (let anAnc of tmpAncs) {
                    if (!_.has(alreadySignedCoinbases, anAnc))
                        alreadySignedCoinbases[anAnc] = []
                    alreadySignedCoinbases[anAnc].push(aFloat.bBacker)
                }
            }
        }
        clog.cb.info(`existed alreadySignedCoinbases floats: ${JSON.stringify(alreadySignedCoinbases)}`);

        // if there are coinbases at all hasn't any signature?
        let missedSignatureCoinbases = utils.arrayDiff(coinbasHashes, utils.objKeys(alreadySignedCoinbases))
        clog.cb.info(`missed Coinbases without FSign: ${JSON.stringify(missedSignatureCoinbases)}`);

        // since the blocks are not saving in DAG directly(even machine by itself created them)
        // and they will recorded in DAG after receiving from a neighbor
        // we use containter newly_propagated_floating_signatures to avoid double sending a floating signature
        let npfs = kv.getValueSync('newly_propagated_floating_signatures');
        if (!utils._nilEmptyFalse(npfs)) {
            npfs = JSON.parse(npfs);
        } else {
            npfs = {}
        }
        clog.cb.info(`newly propagated floating signatures: ${JSON.stringify(npfs)}`);

        let hashAsKey, signDate;
        for (let CBBlockHash of missedSignatureCoinbases) {
            // avoid duplicate sending float
            hashAsKey = utils.hash6c(CBBlockHash);
            signDate = _.has(npfs, hashAsKey) ? npfs[hashAsKey] : '2009-01-03 18:02:55';

            // if machine hasn't signed this CB yet, and propagation is older than 3/4 of a cycle, re-send it
            let shouldSignCb = (!_.has(npfs, hashAsKey) && (utils.timeDiff(signDate).asMinutes > (iConsts.getCycleByMinutes() * 0.75)));
            // if machine already signed this CB, and propagation is older than 90% of a cycle, re-send it
            shouldSignCb |= (!_.has(npfs, hashAsKey) && (utils.timeDiff(signDate).asMinutes > (iConsts.getCycleByMinutes() * 0.90)));

            if (shouldSignCb) {
                npfs[hashAsKey] = utils.getNow();

                // create floating signature block
                let flSBlock = getFloatingSignatureTemplate();

                flSBlock.creationDate = utils.getNow();
                flSBlock.cycle = cbInfo.cycleStamp;
                flSBlock.confidence = percentage;
                flSBlock.ancestors = [CBBlockHash] // a floating signature block MUST HAVE ONLY ONE ancestors whom is going to be signed
                flSBlock.bExtInfo = machine.signByMachineKey({
                    signMsg: crypto.convertToSignMsg(flSBlock.ancestors)
                });
                flSBlock.backer = flSBlock.bExtInfo.backer;
                delete flSBlock.bExtInfo.backer;
                flSBlock.bExtHash = iutils.doHashObject(flSBlock.bExtInfo);
                flSBlock.blockLength = iutils.offsettingLength(JSON.stringify(flSBlock).length);
                flSBlock.blockHash = FloatingSignature.calcHashBFloatingSignature(flSBlock);



                // broadcast floating signature block
                msg = `Floating signature block(${utils.hash6c(flSBlock.blockHash)}) issued by ${machine.getMProfileSettingsSync().pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `
                clog.cb.info(`prepare to sending Floating ${msg}`);
                clog.cb.info(`fSign block(${utils.hash6c(flSBlock.blockHash)}) ${utils.stringify(flSBlock)}`);

                sendingQ.pushIntoSendingQ({
                    sqType: iConsts.BLOCK_TYPES.FSign,
                    sqCode: flSBlock.blockHash,
                    sqPayload: utils.stringify(flSBlock),
                    sqTitle: msg
                });
            }

        }

        // if there are coinbases which local machine did not signed
        for (let aCoinbase of utils.objKeys(alreadySignedCoinbases)) {
            if (!alreadySignedCoinbases[aCoinbase].includes(backer)) {
                // avoid duplicate sending float
                hashAsKey = utils.hash6c(aCoinbase);
                signDate = _.has(npfs, hashAsKey) ? npfs[hashAsKey] : '2009-01-03 18:02:55';

                // if propagation is older than 1/2 of a cycle, re-send it
                if (utils.timeDiff(signDate).asMinutes > (iConsts.getCycleByMinutes() / 2)) {
                    npfs[hashAsKey] = utils.getNow();

                    // local machine has to sign coinbase block
                    let flSBlock = getFloatingSignatureTemplate();

                    flSBlock.creationDate = utils.getNow();
                    flSBlock.cycle = cbInfo.cycleStamp;
                    flSBlock.confidence = percentage;
                    flSBlock.ancestors = [aCoinbase] // a floating signature block MUST HAVE ONLY ONE ancestors whom is going to be signed
                    flSBlock.bExtInfo = machine.signByMachineKey({
                        signMsg: crypto.convertToSignMsg(flSBlock.ancestors)
                    });
                    flSBlock.backer = flSBlock.bExtInfo.backer;
                    delete flSBlock.bExtInfo.backer;
                    flSBlock.bExtHash = iutils.doHashObject(flSBlock.bExtInfo);
                    flSBlock.blockLength = iutils.offsettingLength(JSON.stringify(flSBlock).length);
                    flSBlock.blockHash = FloatingSignature.calcHashBFloatingSignature(flSBlock);
                    let title = `Floating signature(missed local signature) block(${utils.hash6c(flSBlock.blockHash)}) issued by ${machine.getMProfileSettingsSync().pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `;
                    clog.cb.info(`adding local sign: ${title}`);

                    // broadcast floating signature block
                    sendingQ.pushIntoSendingQ({
                        sqType: iConsts.BLOCK_TYPES.FSign,
                        sqCode: flSBlock.blockHash,
                        sqPayload: utils.stringify(flSBlock),
                        sqTitle: title,
                    });
                }
            }
        }

        // remove pretty old npfs
        let freshNPFS = {}
        utils.objKeys(npfs).forEach(key => {
            if (utils.timeDiff(npfs[key]).asMinutes < (iConsts.getCycleByMinutes() * 2))
                freshNPFS[key] = npfs[key]
        });
        kv.setSync('newly_propagated_floating_signatures', JSON.stringify(freshNPFS));

    }


    static calcHashBFloatingSignature(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            ancestors: block.ancestors,
            backer: block.backer,
            bType: block.bType,
            bVer: block.bVer,
            confidence: block.confidence,
            creationDate: block.creationDate,
            cycle: block.cycle,
            bExtHash: block.bExtHash,
            net: block.net,
        };
        return iutils.doHashObject(hashableBlock);
    }


    static validateFSBlock(args) {
        let msg;

        let block = args.payload;

        let bVer = _.has(block, 'bVer') ? block.bVer : null;
        if (utils._nilEmptyFalse(bVer) || !iutils.isValidVersionNumber(bVer)) {
            msg = `missed FS block ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        // control shares/confidence
        clog.app.info(`validate FSBlock blockCreationDate(${block.creationDate}) backer(${block.backer}) `);
        let issuerSharesPercentage = DNAHandler.getAnAddressShares(block.backer, block.creationDate).percentage;
        if (block.confidence != issuerSharesPercentage) {
            msg = `FSBlock(${utils.hash6c(block.blockHash)}) rejected because of wrong! confidence(${block.confidence}) `
            msg += `!=local(${issuerSharesPercentage})`
            clog.cb.error(msg);
            console.error(msg);
            return { err: true, msg }
        }

        // control signature
        if (!_.has(block, 'bExtInfo')) {
            msg = `FSBlock(${utils.hash6c(block.blockHash)}) rejected because of missed bExtInfo`
            clog.cb.error(msg);
            console.error(msg);
            return { err: true, msg }
        }
        if (!_.has(block.bExtInfo, 'uSet')) {
            msg = `FSBlock(${utils.hash6c(block.blockHash)}) rejected because of missed uSet`
            clog.cb.error(msg);
            console.error(msg);
            return { err: true, msg }
        }
        let isValidUnlock = mOfNHandler.validateSigStruct({
            address: block.backer,
            uSet: block.bExtInfo.uSet
        });
        if (isValidUnlock != true) {
            msg = `Invalid given uSet structure for FSBlock(${utils.hash6c(block.blockHash)})`;
            clog.cb.error(msg);
            clog.sec.error(msg);
            return { err: true, msg }
        }

        let signMsg = crypto.convertToSignMsg(block.ancestors)
        for (let singatureInx = 0; singatureInx < block.bExtInfo.uSet.sSets.length; singatureInx++) {
            let verifyRes = crypto.verifySignature(signMsg, block.bExtInfo.signatures[singatureInx], block.bExtInfo.uSet.sSets[singatureInx].sKey);
            if (verifyRes != true) {
                msg = `Invalid given signature for FSBlock(${utils.hash6c(block.blockHash)})`;
                clog.cb.error(msg);
                clog.sec.error(msg);
                return { err: true, msg }
            }
        }

        msg = `received FSBlock(${utils.hash6c(block.blockHash)}) is valid`;
        clog.cb.info(msg);
        return { err: false, msg }

        // } catch (e) {
        //     console.log(e);
        //     msg = `${utils.stringify(e)} \nfailed on validate FSBlock(${utils.hash6c(block.blockHash)})`;
        //     args.err = msg;
        //     listener.doCallSync('SPSH_failed_on_validate_FSBlock', args);
        //     clog.cb.error(msg);
        //     clog.cb.error(`removing block(${utils.hash6c(block.blockHash)}) from i_parsing_q ${utils.stringify(block)}`);
        //     return { err: true, msg }

        // }
    }

    static handleReceivedFSBlock(args) {
        clog.cb.info(`hhhhhhhhhhhhhanddddd handleReceivedFSBlock`);
        clog.cb.info(JSON.stringify(args));
        let msg;
        let block = args.payload;
        let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow()

        let validateRes = FloatingSignature.validateFSBlock(args);
        clog.cb.info(`hhhhhhhhhhhhhanddddd handleReceivedFSBlock`);
        clog.cb.info(`Received validateFSBlock result: ${JSON.stringify(validateRes)}`);
        if (validateRes.err != false) {
            // do something
            return { err: true, msg: 'Wrong fs block ' + validateRes.msg, shouldPurgeMessage: true }
        }

        // record in dag
        clog.cb.info(`add a valid FSBlock(${utils.hash6c(block.blockHash)}) to DAG ${JSON.stringify(block)}`)
        dagHandler.addBH.addBlockToDAG({
            block,
            receive_date: receive_date,
            confidence: block.confidence
        });
        dagHandler.addBH.postAddBlockToDAG({ block });

        // broadcast to neighbors
        if (iutils.isInCurrentCycle(block.creationDate)) {
            let pushRes = sendingQ.pushIntoSendingQ({
                sqType: block.bType,
                sqCode: block.blockHash,
                sqPayload: utils.stringify(block),
                sqTitle: `Broadcasting the confirmed FS block(${utils.hash6c(block.blockHash)}) ${block.cycle}`
            })
            clog.app.info(`FS pushRes: ${pushRes}`);
        }

        return { err: false, shouldPurgeMessage: true }

    }
}




module.exports = FloatingSignature;
