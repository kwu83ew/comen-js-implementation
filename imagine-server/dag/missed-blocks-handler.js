const _ = require('lodash');
const iConsts = require('../config/constants');
const clog = require('../loggers/console_logger');
const utils = require('../utils/utils');
const model = require('../models/interface');


const table = 'i_missed_blocks';


class MissedBlocksHandler {


    static recursiveMissedlocksManager(args = {}) {
        setTimeout(() => {
            MissedBlocksHandler.recursiveMissedlocksManager();
            MissedBlocksHandler.refreshMissedBlock();
            MissedBlocksHandler.invokeAllMissed();
        }, 60000);
    }

    static invokeAllMissed() {
        const dagMsgHandler = require('../messaging-protocol/dag/dag-msg-handler');

        let missedBlocks = MissedBlocksHandler.getMissedBlocksToInvoke();
        missedBlocks = utils.arrayUnique(missedBlocks);
        console.log('missedBlocks', missedBlocks);

        for (let blockHash of missedBlocks) {
            setTimeout(() => {
                dagMsgHandler.invokeBlock(blockHash);
                MissedBlocksHandler.increaseAttempNumber(blockHash);
            }, 2000);
        }

        // let maxInvoke = 10;
        // if (missedBlocks.length < 10)
        //     let maxInvoke = missedBlocks.length;

        // for (let inx = 0; inx < maxInvoke; inx++) {
        //     dagMsgHandler.invokeBlock(missedBlocks[inx]);
        // }

        return { err: false };
    }

    static refreshMissedBlock() {
        const parsingQHandler = require('../services/parsing-q-manager/parsing-q-handler');
        //aggregate prerequisities in parsing q table and push to missed table 9if doesn's exist on DAG)
        let res = parsingQHandler.qUtils.searchParsingQSync({
            fields: ['pq_prerequisites']
        });
        let prerequisites = res.map(x => x.pqPrerequisites);
        prerequisites = prerequisites.join(',');
        prerequisites = utils.arrayUnique(utils.unpackCommaSeperated(prerequisites));



        // insert into missed
        MissedBlocksHandler.addMissedBlocksToInvoke(prerequisites);

        return { err: false };
    }

    static removeFromMissedBlocks(blockHash) {
        model.sDelete({
            table,
            query: [['mb_block_hash', blockHash]]
        });
    }


    static getMissedBlocksToInvoke(limit = null) {
        let q = `SELECT mb_block_hash FROM ${table} ORDER BY mb_invoke_attempts, mb_descendents_count DESC, mb_last_invoke_date, mb_insert_date`
        let values = [];
        if (limit != null) {
            values.push(limit);
            q += ' LIMIT $1'
        }
        let missedBlocks = model.sQuery(q, values).map(x => x.mb_block_hash);
        return missedBlocks;
    }

    static increaseAttempNumber(blockHash) {
        let attemp = model.sRead({
            table,
            query: [['mb_block_hash', blockHash]]
        });
        if (attemp.length > 0) {
            attemp = attemp[0]['mb_invoke_attempts'];
        } else {
            attemp = 0;
        }

        model.sUpdate({
            table,
            query: [
                ['mb_block_hash', blockHash]
            ],
            updates: {
                mb_invoke_attempts: attemp + 1,
                mb_last_invoke_date: utils.getNow()
            }
        });
    }


    /**
     * 
     * @param {string} hashes an array of block hashes
     */
    static addMissedBlocksToInvoke(hashes = []) {
        clog.app.info(`maybe add Missed Blocks To Invoke hashes: ${hashes}`);
        console.log(`maybe add Missed Blocks To Invoke hashes: ${hashes}`);

        const dagHandler = require('./graph-handler/dag-handler');

        if (utils._nilEmptyFalse(hashes) || !Array.isArray(hashes) || (hashes.length == 0))
            return;

        // control if already exist in DAG
        let existedInDAG = dagHandler.searchInDAGSync({
            fields: ['b_hash'],
            query: [
                ['b_hash', ['IN', hashes]],
            ]
        });
        if (existedInDAG.length > 0) {
            existedInDAG = existedInDAG.map(x => x.bHash);
            clog.app.info(`${existedInDAG.length} of ${hashes.length} missed blocks already exist in DAG`);
            hashes = utils.arrayDiff(hashes, existedInDAG);
        }

        // control if already exist in missed block table
        let missedBlocks = this.getMissedBlocksToInvoke();
        missedBlocks = utils.arrayUnique(missedBlocks);
        if (missedBlocks.length > 0) {
            // missedBlocks = missedBlocks.map(x => x.block_hash);
            clog.app.info(`${missedBlocks.length} of ${hashes.length}  missed blocks already exist in table missed blocks`);
            hashes = utils.arrayDiff(hashes, missedBlocks);
        }

        // control if already exist in parsing q
        const parsingQHandler = require('../services/parsing-q-manager/parsing-q-handler');
        let existInParse = parsingQHandler.qUtils.searchParsingQSync({
            fields: ['pq_code'],
            query: [['pq_code', ['IN', hashes]]]
        });
        if (existInParse.length > 0) {
            existInParse = existInParse.map(x => x.pqCode);
            clog.app.info(`${existInParse.length} blocks of seemly missed blocks ${hashes.length} already exist in table parsing queue`);
            hashes = utils.arrayDiff(hashes, existInParse);
        }

        console.log('going to insert missed blocks', hashes);
        for (let hash of hashes) {
            let dbl = model.sRead({
                table,
                query: [
                    ['mb_block_hash', hash]
                ]
            });
            if (dbl.length > 0)
                continue;

            model.sCreate({
                table,
                values: {
                    'mb_block_hash': hash,
                    'mb_insert_date': utils.getNow(),
                    'mb_last_invoke_date': utils.getNow(),
                    'mb_invoke_attempts': 0,
                    'mb_descendents_count': 0
                }
            })
        }
    }


}

module.exports = MissedBlocksHandler;
