const _ = require('lodash');
const model = require('../models/interface');
const clog = require('../loggers/console_logger');
const utils = require('../utils/utils');
const blockUtils = require('./block-utils');


const table = 'i_block_extinfos';

class ExtInfosHandler {

    static insertExtInfo(args) {
        let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
        clog.app.info(`insertExtInfo (${JSON.stringify(args)})`);
        //dbl chk
        let dbl = model.sRead({
            table,
            query: [['x_block_hash', args.blockHash]]
        });
        if (dbl.length > 0)
            return;

        let safeBExtInfo = blockUtils.wrapSafeObjectForDB({ obj: args.bExtInfo });

        model.sCreate({
            lockDb,
            table,
            values: {
                x_block_hash: args.blockHash,
                x_detail: safeBExtInfo,
                x_creation_date: args.creationDate
            },
            log: false
        });
    }

    static getBlockExtInfos(blockHash) {
        let res = model.sRead({
            table,
            query: [['x_block_hash', blockHash]]
        });
        if (res.length != 1) {
            let msg = `get Block Ext Infos: the block(${utils.hash6c(blockHash)}) has not ext Info`;
            clog.app.info(msg);
            return { err: false, msg, bExtInfo: null }
        }
        let bExtInfo = blockUtils.openDBSafeObject(res[0].x_detail).content;
        return { err: false, bExtInfo }
    }
}




module.exports = ExtInfosHandler;