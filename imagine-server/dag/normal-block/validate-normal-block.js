const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
// const pluginHanlder = require('../../plugins/plugin-handler')
const listener = require('../../plugin-handler/plugin-handler');
const merkleHandler = require('../../crypto/merkle/merkle');
const blockUtils = require('../block-utils');
const ballotsInRelatedBlock = require('../documents-in-related-block/ballots/ballots');
const freeDocsInRelatedBlock = require('../documents-in-related-block/free-docs/free-docs');
const iNameBindsInRelatedBlock = require('../documents-in-related-block/inames/ipgp-binds');
const iNamesInRelatedBlock = require('../documents-in-related-block/inames/inames');
const closePledgeInRelatedBlock = require('../documents-in-related-block/pledges/close');
const iNameMsgsInRelatedBlock = require('../documents-in-related-block/inames/msg');
const pledgeInRelatedBlock = require('../documents-in-related-block/pledges/pledges');
const pollingsInRelatedBlock = require('../documents-in-related-block/pollings/pollings');
const admPollingsInRelatedBlock = require('../documents-in-related-block/administrative-polling/administrative-polling');
const proposalsInRelatedBlock = require('../documents-in-related-block/proosals/proposals');
const reqRelRessInRelatedBlock = require('../documents-in-related-block/reqrelres/reqrelres');
const transactionsInRelatedBlock = require('../documents-in-related-block/transactions/transactions');
const blockHandler = require('./normal-block-handler/normal-block-handler');

class NormalBlockValidateHandler {

    static validateNormalBlock(args) {
        let hookValidate = listener.doCallSync('SASH_before_validate_normal_block', args);

        let block = args.block;
        let stage = args.stage;

        clog.app.info('xxxxxxxxxxxx validate Normal Block xxxxxxxxxxxxxxxxxxxx');
        clog.app.info(`\n\n\n${utils.stringify(block)}\n`);
        // clog.app.info(blockPresenter(block));
        let res, msg;


        res = NormalBlockValidateHandler.validateGeneralRulsForNormalBlock(block);
        if (res.err != false) {
            return res
        }

        let validateTrxsRes = transactionsInRelatedBlock.validateTransactions({ block, stage });
        if (validateTrxsRes.err != false) {
            return validateTrxsRes
        }

        let grpdRes = blockUtils.groupDocsOfBlock({ block, stage });
        if (grpdRes.err != false) {
            grpdRes.shouldPurgeMessage = true;
            return grpdRes;
        }

        let grpdDocuments = grpdRes.grpdDocuments;
        let docIndexByHash = grpdRes.docIndexByHash;
        let trxDict = grpdRes.trxDict;
        let docByHash = grpdRes.docByHash;
        let mapTrxHashToTrxRef = grpdRes.mapTrxHashToTrxRef;
        let mapTrxRefToTrxHash = grpdRes.mapTrxRefToTrxHash;
        let mapReferencerToReferenced = grpdRes.mapReferencerToReferenced;
        let mapReferencedToReferencer = grpdRes.mapReferencedToReferencer;
        let dTyps = utils.objKeys(grpdDocuments).sort();
        clog.app.info(`Block(${utils.hash6c(block.blockHash)}) docs types: ${dTyps.map(x => `${x}: ${grpdDocuments[x].length}`)}`);

        console.log('\n\n mapTrxRefToTrxHash', utils.stringify(mapTrxRefToTrxHash));

        // control if each trx is referenced to only one Document?
        let tmpTrxs = utils.objKeys(mapTrxRefToTrxHash).map(x => mapTrxRefToTrxHash[x]);
        if (tmpTrxs.length != utils.arrayUnique(tmpTrxs).length) {
            msg = `invalid block! same transaction is used as a ref for different docs! ${utils.stringify(mapTrxRefToTrxHash)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        let validateParams = {
            stage,
            trxDict,
            mapTrxHashToTrxRef,
            mapTrxRefToTrxHash,
            mapReferencerToReferenced,
            mapReferencedToReferencer,
            docByHash,
            grpdDocuments,
            docIndexByHash,
            block
        };

        // TODO: important! currently the order of validating documents of block is important(e.g. polling must be validate before proposals and pledges)
        // improve the code and remove this dependency

        /**
         * validate polling request(if exist)
         */
        let pollingsValidateRes = pollingsInRelatedBlock.validatePollings(validateParams);
        if (pollingsValidateRes.err != false) {
            return pollingsValidateRes;
        }

        /**
         * validate requests for administrative polling(if exist)
         */
        let admPollingValidateRes = admPollingsInRelatedBlock.validateAdmPollings(validateParams);
        if (admPollingValidateRes.err != false) {
            return admPollingValidateRes;
        }

        /**
         * validate reqRelRes request(if exist)
         * TODO: move it to validateAdmPollings
         */
        let reserveCoinsValidateRes = reqRelRessInRelatedBlock.validateReqRelRess(validateParams);
        if (reserveCoinsValidateRes.err != false) {
            return reserveCoinsValidateRes;
        }

        /**
         * validate vote-ballots (if exist)
         */
        let ballotsValidateRes = ballotsInRelatedBlock.validateBallots(validateParams);
        if (ballotsValidateRes.err != false) {
            return ballotsValidateRes;
        }

        /**
         * validate proposals (if exist)
         */
        let proposalsValidateRes = proposalsInRelatedBlock.validateProposals(validateParams);
        if (proposalsValidateRes.err != false) {
            return proposalsValidateRes;
        }

        /**
         * validate pledges (if exist)
         */
        let pledgesValidateRes = pledgeInRelatedBlock.validatePledges(validateParams);
        if (pledgesValidateRes.err != false) {
            return pledgesValidateRes;
        }

        /**
         * validate close pledges (if exist)
         */
        let closePledgesValidateRes = closePledgeInRelatedBlock.validateClosePledges(validateParams);
        if (closePledgesValidateRes.err != false) {
            return closePledgesValidateRes;
        }

        /**
         * validate iNames (if exist)
         */
        let iNamesValidateRes = iNamesInRelatedBlock.validateINames(validateParams);
        if (iNamesValidateRes.err != false) {
            return iNamesValidateRes;
        }

        /**
         * validate bind-iNames (if exist)
         */
        let iNameBindsValidateRes = iNameBindsInRelatedBlock.validateINameBinds(validateParams);
        if (iNameBindsValidateRes.err != false) {
            return iNameBindsValidateRes;
        }

        /**
         * validate msg-to-iNames (if exist)
         */
        let iNameMsgsValidateRes = iNameMsgsInRelatedBlock.validateINameMsgs(validateParams);
        if (iNameMsgsValidateRes.err != false) {
            return iNameMsgsValidateRes;
        }

        /**
         * validate free-docs (if exist)
         */
        let freeDocsValidateRes = freeDocsInRelatedBlock.validateFreeDocs(validateParams);
        if (freeDocsValidateRes.err != false) {
            return freeDocsValidateRes;
        }


        // validate...

        clog.app.info(`--- confirmed normal block(${utils.hash6c(block.blockHash)})`);


        hookValidate = listener.doCallSync('SASH_validate_normal_block', block);
        if (_.has(hookValidate, 'err') && (hookValidate.err != false)) {
            return hookValidate;
        }

        return {
            err: false,
            isSusBlock: validateTrxsRes.isSusBlock,
            doubleSpends: validateTrxsRes.doubleSpends
        };

    }

    static validateGeneralRulsForNormalBlock(block) {
        let msg;

        if (!utils.isValidDateForamt(block.creationDate)) {
            msg = `invalid block.creationDate(${block.creationDate})!`;
            return { err: true, msg, shouldPurgeMessage: true }
        }

        if (utils.isGreaterThanNow(block.creationDate)) {
            msg = `Block whith future creation date is not acceptable (${utils.hash6c(block.blockHash)}) ${block.creationDate}`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // docRootHash control
        let docsRootHash = merkleHandler.merkle(block.docs.map(x => x.hash)).root;
        if (block.docsRootHash != docsRootHash) {
            msg = `Mismatch block DocRootHash. locally calculated(${utils.hash6c(docsRootHash)}) remote(${utils.hash6c(block.docsRootHash)})`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        if (!utils.isValidHash(block.blockHash)) {
            msg = `Invalid blockHash (${block.blockHash})`;
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // re-calculate block hash
        let blockHash = blockHandler.calcHashBNormalBlock(block);
        if (blockHash != block.blockHash) {
            msg = `Mismatch blockHash. locally calculated(${utils.hash6c(blockHash)}) remote(${utils.hash6c(block.blockHash)})`
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // Block length control
        if (utils.stringify(block).length != block.blockLength) {
            msg = `Mismatch block(${utils.hash6c(block.blockHash)}) length locally calculated length(${utils.stringify(block).length}) != remote length(${block.blockLength})`;
            clog.sec.error(msg)
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        let blockAncestors = block.ancestors;
        if (!Array.isArray(blockAncestors) || blockAncestors.length == 0) {
            msg = `Block(${utils.hash6c(block.blockHash)}) has not any ancestor! `;
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true };
        }

        return { err: false, shouldPurgeMessage: true }
    }
}

module.exports = NormalBlockValidateHandler;
