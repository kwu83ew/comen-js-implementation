const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const normalBlockHandler = require('./validate-normal-block');
const dagHandler = require('../graph-handler/dag-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const spentCoinsHandler = require('../../transaction/utxo/stxo-handler');
const DNAHandler = require('../../dna/dna-handler');
const machine = require('../../machine/machine-handler');
const fVHandler = require('../../dag/floating-vote/floating-vote-handler');

class NormalBlockHandler {

    static handleReceivedNormalBlock(args) {
        clog.app.info('******** handle Received Normal Block Sync');
        clog.app.info(utils.stringify(args));
        clog.app.info(`\n`);
        let block = _.has(args, 'payload') ? args.payload : null;
        // block.receiveDate = utils.getNow();

        let bVer = _.has(block, 'bVer') ? block.bVer : null;
        if (utils._nilEmptyFalse(bVer) || !iutils.isValidVersionNumber(bVer)) {
            msg = `missed bVer normal block ${utils.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        clog.app.info(`--- handle normal block(${utils.hash6c(block.blockHash)}) `);
        let validateRes = normalBlockHandler.validateNormalBlock({
            stage: iConsts.STAGES.Validating,
            block
        });
        clog.app.info(`Received a (${block.bType}) block(${utils.hash6c(block.blockHash)}), validation result: ${utils.stringify(validateRes)}`);
        if (validateRes.err != false) {
            // maybe do something
            return validateRes;
        }

        let confidence = DNAHandler.getAnAddressShares(block.backer, block.creationDate).percentage;
        //TODO: prepare a mega query to run in atomic transactional mode

        // add block to DAG(refresh leave blocks, remove from utxos, add to utxo12...)
        // insert the block in i_blocks as a confirmed block
        dagHandler.addBH.addBlockToDAG({ block, confidence });
        dagHandler.addBH.postAddBlockToDAG({ block });

        // remove used UTXOs
        utxoHandler.removeUsedCoinsByBlock(block);
        // sometimes because of async nature of code, after deleting a utxo another thread is going to re insert deleted utxo
        // or maybe because of lock DB not works perfectly
        // so double delete make it secure
        // FIXME: find an elegant way to resolve it
        setTimeout(() => {
            utxoHandler.removeUsedCoinsByBlock(block);
        }, 60000);

        // log spend TxOs
        let cDate = utils.getNow();
        // if machine is in sync mode, we send half a cycle after creationdate to avoid deleting all spend records in table "trx_spend"
        if (machine.isInSyncProcess())
            cDate = block.creationDate;
        spentCoinsHandler.markAsSpentAllBlockInputs({
            block,
            cDate
        });

        // broadcast block to neighbors
        if (dagHandler.isDAGUptodated()) {
            let pushRes = sendingQ.pushIntoSendingQ({
                sqType: block.bType,
                sqCode: block.blockHash,
                sqPayload: utils.stringify(block),
                sqTitle: `Broadcasting confirmed normal block(${utils.hash6c(block.blockHash)})`,
            })
            clog.app.info(`BCNB pushRes: ${utils.stringify(pushRes)}`);


            if (_.has(validateRes, 'isSusBlock') && (validateRes.isSusBlock != null)) {
                let susVoteRes = fVHandler.createFVoteBlock({
                    cDate,
                    uplink: block.blockHash,
                    bCat: iConsts.FLOAT_BLOCKS_CATEGORIES.Trx,
                    voteData: {
                        doubleSpends: validateRes.doubleSpends
                    }
                });

                if (susVoteRes.err != false) {
                    clog.app.error(`\n\nFailed on generating susVote: ${utils.stringify(susVoteRes)}\n`);
                    return { err: false, shouldPurgeMessage: true };
                }

                console.log(`\n\nBroadcasting susVote block(${utils.hash6c(block.blockHash)}): ${utils.stringify(susVoteRes)}\n`);
                clog.app.info(`\n\nBroadcasting susVote block(${utils.hash6c(block.blockHash)}): ${utils.stringify(susVoteRes)}\n`);
                let pushBVRes = sendingQ.pushIntoSendingQ({
                    sqType: susVoteRes.block.bType,
                    sqCode: susVoteRes.block.blockHash,
                    sqPayload: utils.stringify(susVoteRes.block),
                    sqTitle: `Broadcasting susVote block$(${utils.hash6c(block.blockHash)})`,
                })
                clog.app.info(`susVote pushRes: ${utils.stringify(pushBVRes)}`);
            }
            return { err: false, shouldPurgeMessage: true };

        } else if (machine.isInSyncProcess()) {
            if (_.has(validateRes, 'isSusBlock') && (validateRes.isSusBlock != null))
                clog.app.info(`machine in sync mode found a sus block ${utils.hash6c(block.blockHash)}`);
            return { err: false, shouldPurgeMessage: true };

        }

        return { err: true, shouldPurgeMessage: true }
    }

}


module.exports = NormalBlockHandler;
