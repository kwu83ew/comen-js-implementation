// const _ = require('lodash');
// const iConsts = require('../../config/constants');
// const clog = require('../../loggers/console_logger');
// const utils = require('../../utils/utils');
// const iutils = require('../../utils/iutils');
// const model = require('../../models/interface');
// const blockHashHandler = require('../hashable');
// const blockTplHandler = require('../../docs-structure/block/block-tpl-handler');
// // const getsusVoteTemplate = require('../../docs-structure/block/sus-vote');
// const DNAHandler = require('../../dna/dna-handler');
// const blockHasher = require('../hashable');
// const machine = require('../../machine/machine-handler');
// const crypto = require('../../crypto/crypto');
// const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');
// const dagHandler = require('../graph-handler/dag-handler');
// const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
// const suspectTrxHandler = require('../../transaction/utxo/suspect-transactions-handler');
// const extinfosHandler = require('../extinfos-handler');
// const fVHandler = require('../floating-vote/floating-vote-handler');

// // const table = '';

// class susVoteHandler {

//     // static createSusVote(args) {
//     //     return fVHandler.create FVoteBlock({
//     //         cDate: args.cDate,
//     //         uplink: args.blockHash,
//     //         bCat: iConsts.FLOAT_BLOCKS_CATEGORIES.Trx,
//     //         voteData: {
//     //             doubleSpends: args.doubleSpends
//     //         }
//     //     });
//     // }


//     //     // static handleReceivedSusVoteBlock(args) {
//     //     //     let msg;
//     //     //     let block = args.payload;
//     //     //     let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow()

//     //     //     clog.app.info(`\n\n******** handle Received Sus Vote block(${utils.hash6c(block.blockHash)}) \n ${JSON.stringify(block)}\n`);

//     //     //     let validateRes = susVoteHandler.validateSusVoteBlock(args);
//     //     //     clog.trx.info(`\n\nReceived validateBVote result: ${JSON.stringify(validateRes)}\n`);
//     //     //     if (validateRes.err != false) {
//     //     //         // do something
//     //     //         return { err: true, msg: 'Wrong bVote block ' + validateRes.msg, shouldPurgeMessage: true }
//     //     //     }

//     //     //     // record in dag
//     //     //     clog.trx.info(`add a valid FloatingVote(${utils.hash6c(block.blockHash)} to DAG )`)
//     //     //     clog.trx.info(JSON.stringify(block));

//     //     //     // // save doubleSpends & signature as ext infos in seperated table
//     //     //     // extinfosHandler.insertExtInfo({
//     //     //     //     blockHash: block.blockHash,
//     //     //     //     ext Infos: JSON.stringify(block.bExtInfo),
//     //     //     //     creationDate: block.creationDate
//     //     //     // });

//     //     //     let tmpBlock = _.clone(block);
//     //     //     dagHandler.addBlockToDAG({
//     //     //         block: tmpBlock,
//     //     //         receive_date: receive_date,
//     //     //         confidence: block.confidence
//     //     //     });
//     //     //     dagHandler.postAddBlockToDAG({ block });

//     //     //     // also insert suspicious transactions
//     //     //     let doubleSpends = block.bExtInfo.doubleSpends;
//     //     //     clog.trx.info(`inserting doubleSpends in db: ${JSON.stringify(doubleSpends)}`);
//     //     //     for (let refLoc of utils.objKeys(doubleSpends)) {
//     //     //         for (let aTx of doubleSpends[refLoc]) {
//     //     //             suspectTrxHandler.add SuspectTransaction({
//     //     //                 voter: block.backe rAddress,
//     //     //                 voteDate: block.creationDate,
//     //     //                 coin,
//     //     //                 blockHash: aTx.spendBlockHash,
//     //     //                 docHash: aTx.spendDocHash,
//     //     //                 creationDate: aTx.spend Date,
//     //     //                 receiveOrder: aTx.order
//     //     //             });

//     //     //             // Remove sus docs from doc map (i_docs_blocks_map) too. DO REALLY NEED TO REMOVE RECORD FROM MAPPING?
//     //     //             // dagHandler.removeMappingForDocs([aTx.spendDocHash]);

//     //     //         }
//     //     //     }


//     //     //     // broadcast to neighbors
//     //     //     if (iutils.isInCurrentCycle(block.creationDate)) {
//     //     //         let pushRes = sendingQ.pushIntoSendingQ({
//     //     //             sqType: block.bType,
//     //     //             sqCode: utils.hash6c(block.blockHash),
//     //     //             sqPayload: JSON.stringify(block),
//     //     //             sqTitle: `Broadcasting the confirmed bVote block(${utils.hash6c(block.blockHash)}) ${block.cycle}`
//     //     //         })
//     //     //         clog.app.info(`bVote pushRes: ${pushRes}`);
//     //     //     }

//     //     //     return { err: true, msg, shouldPurgeMessage: true };
//     //     // }

//     //     static validateSusVoteBlock(args) {
//     //         let msg;
//     //         let block = args.payload;
//     //         let signature = block.bExtInfo.signature;
//     //         // control shares/confidence
//     //         let issuerSharesPercentage = DNAHandler.getAnAddressShares(block.backe rAddress, block.creationDate).percentage;
//     //         if (block.confidence != issuerSharesPercentage) {
//     //             msg = `bVote(${utils.hash6c(block.blockHash)}) rejected because of wrong confidence(${block.confidence})!=local(${issuerSharesPercentage})`
//     //             clog.trx.error(msg);
//     //             console.error(msg);
//     //             return { err: true, msg }
//     //         }

//     //         // control signature
//     //         if (isValidUnblock != true) {
//     //             msg = `Invalid given unlock structure for sVote(${utils.hash6c(block.blockHash)})`;
//     //             clog.trx.error(msg);
//     //             clog.sec.error(msg);
//     //             return { err: true, msg }
//     //         }

//     //         let signMsg = crypto.convertToSignMsg([block.ancestors.sort(), block.bExtInfo.doubleSpends]);
//     //         for (let singatureInx = 0; singatureInx < signature.unlock.sSets.length; singatureInx++) {
//     //             let verifyRes = crypto.verifySignature(signMsg, signature.signs[singatureInx], signature.unlock.sSets[singatureInx].sKey);
//     //             if (verifyRes != true) {
//     //                 msg = `Invalid given signature for sVote(${utils.hash6c(block.blockHash)})`;
//     //                 clog.trx.error(msg);
//     //                 clog.sec.error(msg);
//     //                 return { err: true, msg }
//     //             }
//     //         }

//     //         msg = `received sVote(${utils.hash6c(block.blockHash)}) is valid`;
//     //         clog.trx.info(msg);
//     //         return { err: false, msg }
//     //     }



//     //     // static create SusVote(susBlock, doubleSpends) {
//     //     //     let { backerAddress, shares, percentage } = DNAHandler.getMachineShares();
//     //     //     if (percentage < cnfHandler.getMinShareToAllowedIssueFVote()) {
//     //     //         clog.trx.info(`machine hasn't sufficient shares to sign coinbase block`);
//     //     //         return null;
//     //     //     }

//     //     //     // create sus voting block
//     //     //     let fVoteBlock = getsusVoteTemplate();

//     //     //     fVoteBlock.creationDate = utils.getNow();
//     //     //     fVoteBlock.signals = iutils.getMachineSignals();
//     //     //     // in theory could possible more than one block using same refLoc, 
//     //     //     // so here we put the first one which is already recorded in machine's local DAG
//     //     //     // (by creation date which each blocks claim)
//     //     //     fVoteBlock.confidence = percentage;
//     //     //     fVoteBlock.ancestors = susBlock.blockHash; // a sus-block-vote MUST HAVE ONLY ONE ancestors whom is going to be voted
//     //     //     let signMsg = crypto.convertToSignMsg([fVoteBlock.ancestors.sort(), doubleSpends]);    // ancestors is susBLock too, so we sign it
//     //     //     fVoteBlock.bExtInfo = machine.signByMachineKey({ signMsg });
//     //     //     fVoteBlock.bExtInfo.doubleSpends = doubleSpends;
//     //     //     fVoteBlock.backe rAddress = fVoteBlock.bExtInfo.backer;
//     //     //     delete fVoteBlock.bExtInfo.backer;
//     //     //     fVoteBlock.bExtHash = crypto.keccak256(JSON.stringify(fVoteBlock.bExtInfo));
//     //     //     fVoteBlock.blockLength = iutils.offsettingLength(JSON.stringify(fVoteBlock).length);
//     //     //     fVoteBlock.blockHash = blockHasher.hashBlock(fVoteBlock);
//     //     //     return fVoteBlock;
//     //     // }

// }


// module.exports = susVoteHandler;
