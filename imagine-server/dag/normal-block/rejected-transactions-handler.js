const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const model = require("../../models/interface");
const blockHashHandler = require("../hashable");
const blockTplHandler = require("../../docs-structure/block/block-tpl-handler");


const table = 'i_trx_rejected_transactions';// it is only a log, and no parts in code must not relays on this table

class RejectedTransactionsHandler {

    static searchRejTrx(args = {}) {
        let query = _.has(args, 'query') ? args.query : [];

        let res = model.sRead({
            table,
            query
        });

        return res;
    }

    static async searchRejTrxAsync(args = {}) {
        args.table = table;
        let res = await model.aRead(args);
        return res;
    }

    static addTransaction(args) {
        model.sCreate({
            table,
            values: {
                rt_block_hash: args.blockHash,
                rt_doc_hash: args.docHash,
                rt_coin: args.refLoc,
                rt_insert_date: utils.getNow()
            }
        })
    }

}
module.exports = RejectedTransactionsHandler;
