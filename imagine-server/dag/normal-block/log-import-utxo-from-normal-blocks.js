const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require("../../models/interface");





const logTable = 'i_logs_block_import_report';

class LogNormalBlockUTXOsImport {

    static async getImportLogs(args) {
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', args);
        let blockHash = args.blockHash;
        let records = await model.aRead({
            table: logTable,
            query: [['li_block_hash', blockHash]],
            order: [['li_insert_date', 'ASC']]
        });
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', records);
        return records;
    }

    static logImport(args) {
        // console.log(`\n\n log Import args ${utils.stringify(args)}`);
        let aBlockAnaRes = args.aBlockAnaRes;

        // shortening to logging

        if (_.has(aBlockAnaRes.blockDPCost_Treasury, 'refLoc')) {
            aBlockAnaRes.blockDPCost_Treasury.refLoc = iutils.shortCoinRef(aBlockAnaRes.blockDPCost_Treasury.refLoc);
        }

        if (_.has(aBlockAnaRes.blockDPCost_Backer, 'refLoc')) {
            aBlockAnaRes.blockDPCost_Backer.refLoc = iutils.shortCoinRef(aBlockAnaRes.blockDPCost_Backer.refLoc);
        }

        if (_.has(aBlockAnaRes.blockDPCost_Backer, 'address')) {
            aBlockAnaRes.blockDPCost_Backer.address = iutils.shortBech6(aBlockAnaRes.blockDPCost_Backer.address);
        }

        if (_.has(aBlockAnaRes, 'transactionsDetection')) {
            for (let aTrx of utils.objKeys(aBlockAnaRes.transactionsDetection)) {
                aBlockAnaRes.transactionsDetection[utils.hash6c(aTrx)] = aBlockAnaRes.transactionsDetection[aTrx];
                delete (aBlockAnaRes.transactionsDetection[aTrx]);
            }
        }

        if (_.has(aBlockAnaRes, 'aSingleTrxDPCost')) {
            for (let docHash of utils.objKeys(aBlockAnaRes.aSingleTrxDPCost)) {
                aBlockAnaRes.aSingleTrxDPCost[docHash].refLoc = iutils.shortCoinRef(aBlockAnaRes.aSingleTrxDPCost[docHash].refLoc);
                aBlockAnaRes.aSingleTrxDPCost[docHash].address = iutils.shortBech6(aBlockAnaRes.aSingleTrxDPCost[docHash].address);
                aBlockAnaRes.aSingleTrxDPCost[utils.hash6c(docHash)] = aBlockAnaRes.aSingleTrxDPCost[docHash];
                delete (aBlockAnaRes.aSingleTrxDPCost[docHash]);
            }
        }
        if (_.has(aBlockAnaRes, 'importableUTXOs')) {
            for (let aUTXO of aBlockAnaRes.importableUTXOs) {
                aUTXO.newCoin = iutils.shortCoinRef(aUTXO.newCoin);
                aUTXO.address = iutils.shortBech6(aUTXO.address);
            }
        }

        if (_.has(aBlockAnaRes, 'rejectedTransactions')) {
            for (let docHash of utils.objKeys(aBlockAnaRes.rejectedTransactions)) {
                aBlockAnaRes.rejectedTransactions[utils.hash6c(docHash)] = aBlockAnaRes.rejectedTransactions[docHash].map(x => iutils.shortCoinRef(x))
                delete aBlockAnaRes.rejectedTransactions[docHash];
            }
        }

        aBlockAnaRes.mustNotImportTrxOutputs.map(x => utils.hash6c(x));

        if (_.has(aBlockAnaRes, 'toBeRestoredRefLocs')) {
            for (let aRestor of aBlockAnaRes.toBeRestoredRefLocs) {
                aRestor.refLoc = iutils.shortCoinRef(aRestor.refLoc);
                aRestor.address = iutils.shortBech6(aRestor.address);
            }
        }

        let tmpDPCostsRefLocs = [];
        if (_.has(aBlockAnaRes, 'DPCostsRefLocs')) {
            for (let aRefLoc of aBlockAnaRes.DPCostsRefLocs) {
                tmpDPCostsRefLocs.push(iutils.shortCoinRef(aRefLoc));
            }
        }
        aBlockAnaRes.DPCostsRefLocs = tmpDPCostsRefLocs;

        let tmpMap = {};
        if (_.has(aBlockAnaRes, 'mapUTrxRefToTrxHash')) {
            for (let key of utils.objKeys(aBlockAnaRes.mapUTrxRefToTrxHash)) {
                tmpMap[utils.hash6c(key)] = utils.hash6c(aBlockAnaRes.mapUTrxRefToTrxHash[key]);
            }
        }
        aBlockAnaRes.mapUTrxRefToTrxHash = tmpMap;

        tmpMap = {};
        if (_.has(aBlockAnaRes, 'mapUTrxHashToTrxRef')) {
            for (let key of utils.objKeys(aBlockAnaRes.mapUTrxHashToTrxRef)) {
                tmpMap[utils.hash6c(key)] = utils.hash6c(aBlockAnaRes.mapUTrxHashToTrxRef[key]);
            }
        }
        aBlockAnaRes.mapUTrxHashToTrxRef = tmpMap;

        // tmpMap = {};
        // if (_.has(aBlockAnaRes, 'mapProposalToPledgedContract')) {
        //     for (let key of utils.objKeys(aBlockAnaRes.mapProposalToPledgedContract)) {
        //         tmpMap[utils.hash6c(key)] = utils.hash6c(aBlockAnaRes.mapProposalToPledgedContract[key]);
        //     }
        // }
        // aBlockAnaRes.mapProposalToPledgedContract = tmpMap;
        // tmpMap = {};
        // if (_.has(aBlockAnaRes, 'mapTrxHashToPledge')) {
        //     for (let key of utils.objKeys(aBlockAnaRes.mapTrxHashToPledge)) {
        //         tmpMap[utils.hash6c(key)] = utils.hash6c(aBlockAnaRes.mapTrxHashToPledge[key]);
        //     }
        // }
        // aBlockAnaRes.mapTrxHashToPledge = tmpMap;
        // tmpMap = {};
        // if (_.has(aBlockAnaRes, 'mapPledgeHashToPayerTrxHash')) {
        //     for (let key of utils.objKeys(aBlockAnaRes.mapPledgeHashToPayerTrxHash)) {
        //         tmpMap[utils.hash6c(key)] = utils.hash6c(aBlockAnaRes.mapPledgeHashToPayerTrxHash[key]);
        //     }
        // }
        // aBlockAnaRes.mapPledgeHashToPayerTrxHash = tmpMap;

        tmpMap = {};
        if (_.has(aBlockAnaRes, 'ballotsCostStatus')) {
            for (let key of utils.objKeys(aBlockAnaRes.ballotsCostStatus)) {
                tmpMap[utils.hash6c(key)] = aBlockAnaRes.ballotsCostStatus[key];
            }
        }
        aBlockAnaRes.ballotsCostStatus = tmpMap;

        tmpMap = {};
        if (_.has(aBlockAnaRes, 'proposalsCostStatus')) {
            for (let key of utils.objKeys(aBlockAnaRes.proposalsCostStatus)) {
                tmpMap[utils.hash6c(key)] = aBlockAnaRes.proposalsCostStatus[key];
            }
        }
        aBlockAnaRes.proposalsCostStatus = tmpMap;

        tmpMap = {};
        if (_.has(aBlockAnaRes, 'pledgesCostStatus')) {
            for (let key of utils.objKeys(aBlockAnaRes.pledgesCostStatus)) {
                tmpMap[utils.hash6c(key)] = aBlockAnaRes.pledgesCostStatus[key];
            }
        }
        aBlockAnaRes.pledgesCostStatus = tmpMap;

        tmpMap = {};
        if (_.has(aBlockAnaRes, 'pledgesCloseCostStatus')) {
            for (let key of utils.objKeys(aBlockAnaRes.pledgesCloseCostStatus)) {
                tmpMap[utils.hash6c(key)] = aBlockAnaRes.pledgesCloseCostStatus[key];
            }
        }
        aBlockAnaRes.pledgesCloseCostStatus = tmpMap;

        if (_.has(aBlockAnaRes, 'blockTreasuryLogs')) {
            for (let elem of aBlockAnaRes.blockTreasuryLogs) {
                elem.refLoc = iutils.shortCoinRef(elem.refLoc);
            }
        }

        if (_.has(aBlockAnaRes, 'blockAlterTreasuryIncomes')) {
            for (let aType of utils.objKeys(aBlockAnaRes.blockAlterTreasuryIncomes)) {
                for (let aRow of aBlockAnaRes.blockAlterTreasuryIncomes[aType]) {
                    aRow.TPTrxHash = utils.hash6c(aRow.TPTrxHash);
                    aRow.refLoc = iutils.shortCoinRef(aRow.TPRefLoc);
                }
            }
        }

        // sus vote analysed shortening
        let tmpValidityChk = {};
        if (_.has(aBlockAnaRes, 'transactionsValidityCheck')) {
            for (let trxHash of utils.objKeys(aBlockAnaRes.transactionsValidityCheck)) {
                let shortHash = trxHash;    // `trx(${utils.hash6c(trxHash)})`;
                tmpValidityChk[shortHash] = {
                    rawVotes: [],
                    coinsAndVotersDict: {},
                    coinsAndOrderedSpendersDict: {},
                    coinsAndPositionsDict: {},
                    susVoteRes: {}
                };

                if (_.has(aBlockAnaRes.transactionsValidityCheck[trxHash], 'allCoinsAreValid'))
                    tmpValidityChk[shortHash].allCoinsAreValid = aBlockAnaRes.transactionsValidityCheck[trxHash].allCoinsAreValid;
                if (_.has(aBlockAnaRes.transactionsValidityCheck[trxHash], 'valid'))
                    tmpValidityChk[shortHash].valid = aBlockAnaRes.transactionsValidityCheck[trxHash].valid;
                if (_.has(aBlockAnaRes.transactionsValidityCheck[trxHash], 'cloned'))
                    tmpValidityChk[shortHash].cloned = aBlockAnaRes.transactionsValidityCheck[trxHash].cloned;

                for (let aVote of aBlockAnaRes.transactionsValidityCheck[trxHash].rawVotes) {
                    tmpValidityChk[shortHash].rawVotes.push({
                        voter: iutils.shortBech6(aVote.st_voter),
                        voteDate: aVote.st_vote_date,
                        coin: iutils.shortCoinRef(aVote.st_coin),
                        block: utils.hash6c(aVote.st_spender_block),
                        trx: utils.hash6c(aVote.st_spender_doc),
                        receiveOrder: aVote.st_receive_order,
                        stSpendDate: aVote.st_spend_date
                    });
                }


                // coinsAndVotersDict
                for (let aRefLoc of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict)) {
                    tmpValidityChk[shortHash].coinsAndVotersDict[iutils.shortCoinRef(aRefLoc)] = {};
                    for (let anAddress of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict[aRefLoc])) {
                        tmpValidityChk[shortHash].coinsAndVotersDict[iutils.shortCoinRef(aRefLoc)][iutils.shortBech6(anAddress)] = {
                            docsInfo: {},
                            rOrders: {}
                        }
                        for (let aDocHash of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict[aRefLoc][anAddress].docsInfo)) {
                            tmpValidityChk[shortHash].coinsAndVotersDict[iutils.shortCoinRef(aRefLoc)][iutils.shortBech6(anAddress)].docsInfo[`trx(${utils.hash6c(aDocHash)})`] =
                                aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict[aRefLoc][anAddress].docsInfo[aDocHash];
                        }
                        for (let anOrder of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict[aRefLoc][anAddress].rOrders)) {
                            tmpValidityChk[shortHash].coinsAndVotersDict[iutils.shortCoinRef(aRefLoc)][iutils.shortBech6(anAddress)].rOrders[anOrder] =
                                utils.hash6c(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndVotersDict[aRefLoc][anAddress].rOrders[anOrder]);
                        }
                    }
                }

                // coinsAndOrderedSpendersDict
                for (let aRefLoc of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndOrderedSpendersDict)) {
                    tmpValidityChk[shortHash].coinsAndOrderedSpendersDict[iutils.shortCoinRef(aRefLoc)] = [];
                    for (let aPosition of aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndOrderedSpendersDict[aRefLoc]) {
                        let newPosition = {};
                        for (let aDocInPos of utils.objKeys(aPosition)) {
                            newPosition[utils.hash6c(aDocInPos)] = {
                                docs: aPosition[aDocInPos].docs
                            }
                            newPosition[utils.hash6c(aDocInPos)].voteData = {
                                docHash: utils.hash6c(aPosition[aDocInPos].voteData.docHash),
                                inside6VotersCount: aPosition[aDocInPos].voteData.inside6VotersCount,
                                inside6VotesGain: aPosition[aDocInPos].voteData.inside6VotesGain,
                                outside6VotersCount: aPosition[aDocInPos].voteData.outside6VotersCount,
                                outside6VotesGain: aPosition[aDocInPos].voteData.outside6VotesGain,
                                voteGain: aPosition[aDocInPos].voteData.voteGain,
                                voteGain: aPosition[aDocInPos].voteData.voteGain,
                                details: {}
                            }
                            for (let aBechDtl of utils.objKeys(aPosition[aDocInPos].voteData.details)) {
                                newPosition[utils.hash6c(aDocInPos)].voteData.details[iutils.shortBech6(aBechDtl)] = aPosition[aDocInPos].voteData.details[aBechDtl];
                            }
                        }

                        tmpValidityChk[shortHash].coinsAndOrderedSpendersDict[iutils.shortCoinRef(aRefLoc)].push(newPosition);
                    }


                    // coinsAndPositionsDict
                    for (let aRefLoc of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict)) {
                        tmpValidityChk[shortHash].coinsAndPositionsDict[iutils.shortCoinRef(aRefLoc)] = {};
                        for (let aPosition of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict[aRefLoc])) {
                            tmpValidityChk[shortHash].coinsAndPositionsDict[iutils.shortCoinRef(aRefLoc)][aPosition] = {
                                inside6h: [],
                                outside6h: [],
                                insideTotal: aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict[aRefLoc][aPosition].insideTotal,
                                outsideTotal: aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict[aRefLoc][aPosition].outsideTotal
                            }
                            for (let aDoc of aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict[aRefLoc][aPosition].inside6h) {
                                tmpValidityChk[shortHash].coinsAndPositionsDict[iutils.shortCoinRef(aRefLoc)][aPosition].inside6h.push({
                                    docHash: utils.hash6c(aDoc.docHash),
                                    votes: aDoc.votes,
                                    voters: aDoc.voters,
                                });
                            }
                            for (let aDoc of aBlockAnaRes.transactionsValidityCheck[trxHash].coinsAndPositionsDict[aRefLoc][aPosition].outside6h) {
                                tmpValidityChk[shortHash].coinsAndPositionsDict[iutils.shortCoinRef(aRefLoc)][aPosition].outside6h.push({
                                    docHash: utils.hash6c(aDoc.docHash),
                                    votes: aDoc.votes,
                                    voters: aDoc.voters,
                                });
                            }
                        }
                    }

                    // susVoteRes
                    for (let aRefLoc of utils.objKeys(aBlockAnaRes.transactionsValidityCheck[trxHash].susVoteRes)) {
                        tmpValidityChk[shortHash].susVoteRes[iutils.shortCoinRef(aRefLoc)] = aBlockAnaRes.transactionsValidityCheck[trxHash].susVoteRes[aRefLoc];
                    }

                }



                //


            }
        }
        // aBlockAnaRes.tmpValidityChk = tmpValidityChk;
        delete (aBlockAnaRes.transactionsValidityCheck);


        this.doLog({
            title: 'aBlockAnaRes',
            blockHash: args.blockHash,
            report: utils.stringify(aBlockAnaRes)
        });

        this.doLog({
            title: 'transactions Validity Check',
            blockHash: args.blockHash,
            report: utils.stringify(tmpValidityChk)
        });

    }

    static doLog(args) {
        model.sCreate({
            table: logTable,
            values: {
                li_block_hash: args.blockHash,
                li_title: args.title,
                li_report: args.report,
                li_insert_date: utils.getNow()
            }
        });
    }

}

module.exports = LogNormalBlockUTXOsImport;
