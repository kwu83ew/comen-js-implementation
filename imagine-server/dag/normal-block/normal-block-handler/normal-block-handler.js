const _ = require('lodash');
const iConsts = require('../../../config/constants');
const iutils = require('../../../utils/iutils');
const utils = require('../../../utils/utils');
const blockTplHandler = require('../../../docs-structure/block/block-tpl-handler');
const docBufferHandler = require('../../../services/buffer/buffer-handler');
const leavesHandler = require('../../leaves-handler');
const clog = require('../../../loggers/console_logger');
const machine = require('../../../machine/machine-handler');
const merkleHandler = require('../../../crypto/merkle/merkle');
const sendingQ = require('../../../services/sending-q-manager/sending-q-manager')
const ballotsInRelatedBlock = require('../../documents-in-related-block/ballots/ballots');
const freeDocsInRelatedBlock = require('../../documents-in-related-block/free-docs/free-docs');
const iNamesInRelatedBlock = require('../../documents-in-related-block/inames/inames');
const iNameBindsInRelatedBlock = require('../../documents-in-related-block/inames/ipgp-binds');
const iNameMsgsInRelatedBlock = require('../../documents-in-related-block/inames/msg');
const pledgeInRelatedBlock = require('../../documents-in-related-block/pledges/pledges');
const closePledgeInRelatedBlock = require('../../documents-in-related-block/pledges/close');
const pollingsInRelatedBlock = require('../../documents-in-related-block/pollings/pollings');
const proposalsInRelatedBlock = require('../../documents-in-related-block/proosals/proposals');
const admPollingsInRelatedBlock = require('../../documents-in-related-block/administrative-polling/administrative-polling');
const reqRelRessInRelatedBlock = require('../../documents-in-related-block/reqrelres/reqrelres');
const transactionsInRelatedBlock = require('../../documents-in-related-block/transactions/transactions');
const dagMsgHandler = require('../../../messaging-protocol/dag/dag-msg-handler');
const blockUtils = require('../../block-utils');

let allowedToDoubleSpend = true;    // BEFORRELEASE convert to false

class NormalBlockCreator {

    static createANormalBlock(args = {}) {
        let msg;
        console.log('create ANormalBlock create A NormalBlock create A NormalBlock');

        let stage = iConsts.STAGES.Creating;
        let creationDate = utils.getNow();
        if (_.has(args, 'creationDate') && (args.creationDate != null)) {
            creationDate = args.creationDate;
        }

        let hasFreshLeaves = leavesHandler.hasFreshLeaves(); // younger than one cycle (12 hours)
        if (!hasFreshLeaves) {
            msg = `NB: Machine hasn't fresh leaves!`
            clog.app.info(msg);
            dagMsgHandler.setMaybeAskForLatestBlocksFlag(iConsts.CONSTS.YES);
            return { err: true, msg: msg };
        }

        let block = blockTplHandler.getNormalBlockTemplate();
        block.creationDate = creationDate;
        block.signals = iutils.getMachineSignals();
        block.docs = [];
        let docsHashes = [];
        block.bExtHash = '';
        let externalInfoHashes = [];
        block.bExtInfo = [];
        clog.app.info(`preparing The NORMAL block to generate ${creationDate}`);

        let machineSettings = machine.getMProfileSettingsSync();
        block.backer = machineSettings.backerAddress;

        /**
         * the first step of creating a block is appending the transactions
         * each block MUST have at least one transaction
         */
        let appendTransactionsRes = transactionsInRelatedBlock.appendTransactions({
            block
        });
        if (appendTransactionsRes.err != false) {
            clog.app.error(`appendTransactionsRes err: ${utils.stringify(appendTransactionsRes)}`);
            return appendTransactionsRes;
        }

        let dummySumBlockValue = appendTransactionsRes.dummySumBlockValue;
        let blockTotalOutput = appendTransactionsRes.blockTotalOutput;
        block = appendTransactionsRes.block;
        docsHashes = appendTransactionsRes.docsHashes;
        externalInfoHashes = appendTransactionsRes.externalInfoHashes;
        console.log(`\n\nblock After adding Trxs: ${utils.stringify(block)}`);

        let grpdRes = blockUtils.retrieveAndGroupBufferedDocuments({ blockCreationDate: creationDate });
        console.log('\n\n grpdRes', utils.stringify(grpdRes));
        if (grpdRes.err != false) {
            clog.app.error(`retrieve And GroupBufferedDocuments Res ${utils.stringify(grpdRes)}`);
            grpdRes.shouldPurgeMessage = true;
            return grpdRes;
        }
        let docByHash = grpdRes.docByHash;
        let grpdDocuments = grpdRes.grpdDocuments;
        let trxDict = grpdRes.trxDict;
        let mapTrxRefToTrxHash = grpdRes.mapTrxRefToTrxHash;
        let mapTrxHashToTrxRef = grpdRes.mapTrxHashToTrxRef;
        let mapReferencerToReferenced = grpdRes.mapReferencerToReferenced;
        let mapReferencedToReferencer = grpdRes.mapReferencedToReferencer;


        // control if each trx is referenced to only one Document?
        let tmpTrxs = utils.objKeys(mapTrxRefToTrxHash).map(x => mapTrxRefToTrxHash[x]);
        if (tmpTrxs.length != utils.arrayUnique(tmpTrxs).length) {
            msg = `same transaction is used as a ref for different docs! ${utils.stringify(mapTrxRefToTrxHash)}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        let appendArgs = {
            block, creationDate, docsHashes, externalInfoHashes, trxDict,
            mapTrxRefToTrxHash, mapTrxHashToTrxRef,
            mapReferencerToReferenced, mapReferencedToReferencer,
            docByHash, grpdDocuments
        }

        // TODO: important! currently the order of adding documents to block is important(e.g. polling must be added before proposalsand pledges)
        // improve the code and remove this dependency


        /**
        * add free Documents(if exist)
        * since block size controlling is not implemented completaly, it is better to put this part at the begening of appending, 
        * just in order to be sure block has enough capacity to include entire docs in buffer
        */
        let addFreeDocsRes = freeDocsInRelatedBlock.appendFreeDocsToBlock(appendArgs);
        clog.app.info(`addFreeDocsRes: ${utils.stringify(addFreeDocsRes)}`);
        if (addFreeDocsRes.err != false) {
            clog.app.error(`addFreeDocsRes ${addFreeDocsRes.msg}`);
            return addFreeDocsRes;
        }
        block = addFreeDocsRes.block;
        docsHashes = addFreeDocsRes.docsHashes;
        externalInfoHashes = addFreeDocsRes.externalInfoHashes;
        if (addFreeDocsRes.addedDocs > 0)
            console.log(`\n\nblock After Adding free-docs: ${utils.stringify(block)}`);


        /**
         * add vote-ballots(if exist)
         */
        let addBallotRes = ballotsInRelatedBlock.appendBallotsToBlock(appendArgs);
        if (addBallotRes.err != false) {
            clog.app.error(`addBallotRes ${addBallotRes.msg}`);
            return addBallotRes;
        }
        block = addBallotRes.block;
        docsHashes = addBallotRes.docsHashes;
        externalInfoHashes = addBallotRes.externalInfoHashes;
        if (addBallotRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding Ballots:  ${utils.stringify(block)}`);


        /**
         * add iName-reg-req(if exist)
         */
        let addINameRes = iNamesInRelatedBlock.appendINamesToBlock(appendArgs);
        if (addINameRes.err != false) {
            clog.app.error(`addINameRes ${addINameRes.msg}`);
            return addINameRes;
        }
        block = addINameRes.block;
        docsHashes = addINameRes.docsHashes;
        externalInfoHashes = addINameRes.externalInfoHashes;
        if (addINameRes.addedDocs > 0)
            console.log('block After Adding iNames: ', utils.stringify(block));


        /**
         * add bind iName(if exist)
         */
        let addInameBindingRes = iNameBindsInRelatedBlock.appendINameBindsToBlock(appendArgs);
        clog.app.info(`addInameBindingRes: ${utils.stringify(addInameBindingRes)}`);
        if (addInameBindingRes.err != false) {
            clog.app.error(`addInameBindingRes ${addInameBindingRes.msg}`);
            return addInameBindingRes;
        }
        block = addInameBindingRes.block;
        docsHashes = addInameBindingRes.docsHashes;
        externalInfoHashes = addInameBindingRes.externalInfoHashes;
        if (addInameBindingRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding iName-binding-iPGP: ${utils.stringify(block)}`);

        /**
         * add msg to iName(if exist)
         */
        let addInameMsgRes = iNameMsgsInRelatedBlock.appendINameMsgsToBlock(appendArgs);
        clog.app.info(`addInameMsgRes: ${utils.stringify(addInameMsgRes)}`);
        if (addInameMsgRes.err != false) {
            clog.app.error(`addInameMsgRes ${addInameMsgRes.msg}`);
            return addInameMsgRes;
        }
        block = addInameMsgRes.block;
        docsHashes = addInameMsgRes.docsHashes;
        externalInfoHashes = addInameMsgRes.externalInfoHashes;
        if (addInameMsgRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding iName-register: ${utils.stringify(block)}`);


        /**
         * add admPolling(if exist)
         */
        let addAdmPolling = admPollingsInRelatedBlock.appendAdmPollingsToBlock(appendArgs);
        if (addAdmPolling.err != false) {
            clog.app.error(`addAdmPolling ${addAdmPolling.msg}`);
            return addAdmPolling;
        }
        block = addAdmPolling.block;
        docsHashes = addAdmPolling.docsHashes;
        externalInfoHashes = addAdmPolling.externalInfoHashes;
        if (addAdmPolling.addedDocs > 0)
            console.log(`\n\nblockAfterAdding Adm Polling: ${utils.stringify(block)}`);


        /**
         * add ReqForRelRes(if exist)
         * TODO: move it to appendAdmPollingsToBlock
         */
        let addRelCoinsRes = reqRelRessInRelatedBlock.appendReqRelResToBlock(appendArgs);
        if (addRelCoinsRes.err != false) {
            clog.app.error(`addRelCoinsRes ${addRelCoinsRes.msg}`);
            return addRelCoinsRes;
        }
        block = addRelCoinsRes.block;
        docsHashes = addRelCoinsRes.docsHashes;
        externalInfoHashes = addRelCoinsRes.externalInfoHashes;
        if (addRelCoinsRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding ReqRelRes: ${utils.stringify(block)}`);


        /**
         * add polling(if exist) except pollings for proposal which are generating automatically
         */
        let addPollRes = pollingsInRelatedBlock.appendPollingsToBlock(appendArgs);
        if (addPollRes.err != false) {
            console.log(`addPollRes ${addPollRes.msg}`);
            return addPollRes;
        }
        block = addPollRes.block;
        docsHashes = addPollRes.docsHashes;
        externalInfoHashes = addPollRes.externalInfoHashes;
        if (addPollRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding Pollings: ${utils.stringify(block)}`);


        /**
         * add proposals(if exist)
         */
        let addPropRes = proposalsInRelatedBlock.appendProposalsToBlock(appendArgs);
        if (addPropRes.err != false) {
            console.log(`addPropRes ${utils.stringify(addPropRes.msg)}`);
            return addPropRes;
        }
        block = addPropRes.block;
        docsHashes = addPropRes.docsHashes;
        externalInfoHashes = addPropRes.externalInfoHashes;
        if (addPropRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding Proposals: ${utils.stringify(block)}`);



        /**
         * add pledges(if exist)
         */
        let addPledgesRes = pledgeInRelatedBlock.appendPledgesToBlock(appendArgs);
        if (addPledgesRes.err != false) {
            console.log(`addPledgesRes ${addPledgesRes.msg}`);
            return addPledgesRes;
        }
        block = addPledgesRes.block;
        docsHashes = addPledgesRes.docsHashes;
        externalInfoHashes = addPledgesRes.externalInfoHashes;
        if (addPledgesRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding pledges: ${utils.stringify(block)}`);

        /**
         * add redeem pledges(if exist)
         */
        let addClosePledgesRes = closePledgeInRelatedBlock.appendClosePledgesToBlock(appendArgs);
        clog.app.info(`addClosePledgesRes: ${utils.stringify(addClosePledgesRes)}`);
        if (addClosePledgesRes.err != false) {
            console.log(`addClosePledgesRes ${addClosePledgesRes.msg}`);
            return addClosePledgesRes;
        }
        block = addClosePledgesRes.block;
        docsHashes = addClosePledgesRes.docsHashes;
        externalInfoHashes = addClosePledgesRes.externalInfoHashes;
        if (addClosePledgesRes.addedDocs > 0)
            console.log(`\n\nblockAfterAdding close-pledges: ${utils.stringify(block)}`);









        // retrieve wiki page
        // retrieve demos text
        // retrieve ...




        console.log(`------------------------- The NORMAL block has ${docsHashes.length} docs value: ${dummySumBlockValue}`);//${bufferedTrxs.length} 
        clog.app.info(`The NORMAL block has ${docsHashes.length} docs`);
        block.docsRootHash = merkleHandler.merkle(docsHashes).root;
        block.bExtHash = merkleHandler.merkle(externalInfoHashes).root;
        if (
            _.has(args, 'ancestors') &&
            !utils._nilEmptyFalse(args.ancestors) &&
            Array.isArray(args.ancestors) &&
            (args.ancestors.length > 0)
        ) {
            block.ancestors = args.ancestors;

        } else {
            let leaves = leavesHandler.getLeaveBlocks();
            leaves = utils.objKeys(leaves);
            let currentAncestors = block.ancestors;
            currentAncestors = _.sortBy(currentAncestors.concat(leaves));
            block.ancestors = currentAncestors;

        }
        block.ancestors = blockUtils.normalizeAncestors(block.ancestors);
        console.log(`------------------------- The NORMAL block ancestors will be ${block.ancestors}`);//${bufferedTrxs.length} 
        clog.app.info(`The NORMAL block ancestors will be ${block.ancestors}`);


        // block = blockLib.blockTruncateToBroadcast(block);
        clog.app.info('publishing blockkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
        clog.app.info(JSON.stringify(block));
        clog.app.info('publishing blockkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
        block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);
        block.blockHash = this.calcHashBNormalBlock(block);
        msg = `Sending a normal block to network: ${JSON.stringify(block)}`;
        console.log(`msg ${msg}`);
        clog.app.info(msg);

        let blockDocUniqRes = blockUtils.groupDocsOfBlock({ block, stage });
        if (blockDocUniqRes.err != false) {
            blockDocUniqRes.shouldPurgeMessage = true;
            return blockDocUniqRes;
        }

        // re-validate block transactions
        if (!allowedToDoubleSpend) {
            let validateTrxsRes = transactionsInRelatedBlock.validateTransactions({ block, stage });
            if (validateTrxsRes.err != false) {
                return validateTrxsRes
            }
        }


        // write file on hard output/send email
        clog.app.info(`The NORMAL! block(${utils.hash6c(block.blockHash)}) value(${blockTotalOutput}) microPAI generated & pushed to sending q \n\n${utils.stringify(block)}\n`)
        console.log(`------------------------- The NORMAL? block(${utils.hash6c(block.blockHash)}) value(${blockTotalOutput}) microPAI, generated & pushed to sending q 11111111111111`)


        sendingQ.pushIntoSendingQ({
            sqType: iConsts.BLOCK_TYPES.Normal,
            sqCode: block.blockHash,
            sqPayload: utils.stringify(block),
            sqTitle: `normal block(${utils.hash6c(block.blockHash)}) ${utils.getNow()}`,
            // blockHash: block.blockHash
        })
        console.log(`------------------------- The NORMAL. block(${utils.hash6c(block.blockHash)}) generated & pushed to sending q 2222222222222`)

        // remove from buffer
        docBufferHandler.removeFromBufferSync({ hashes: docsHashes });

        return {
            err: false,
            blockPushedTosendingQ: true,
            msg: 'Block created and pushed to Queue to broadcast to network',
        };


    }

    static calcHashBNormalBlock(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            ancestors: block.ancestors,
            backer: block.backer,
            blockLength: block.blockLength,
            // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash
            bType: block.bType,
            creationDate: block.creationDate,
            descriptions: block.descriptions,
            docsRootHash: block.docsRootHash, // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
            bExtHash: block.bExtHash, // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
            fVotes: block.fVotes,
            net: block.net,
            signals: block.signals,
            bVer: block.bVer,
        };
        clog.app.info(`The NORMAL! block hashable ${utils.stringify(hashableBlock)}\n`)
        return iutils.doHashObject(hashableBlock);
    }

}

module.exports = NormalBlockCreator;
