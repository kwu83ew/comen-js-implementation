// const _ = require('lodash');
// const iConsts = require('../../../config/constants');
// const utils = require('../../../utils/utils');
// const iutils = require('../../../utils/iutils');
// const model = require('../../../models/interface');
// const clog = require('../../../loggers/console_logger');
// const treasuryHandler = require('../../../services/treasury/treasury-handler');
// const blockUtils = require('../../block-utils');


// class ReqRelResCostHandler {

//     static importReqRelResCost(args) {
//         let block = args.block;
//         let aBlockAnaRes = args.aBlockAnaRes;
//         let mapReferencedToReferencer = aBlockAnaRes.mapReferencedToReferencer;

//         let costPaymentStatus = {};

//         if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_REQRELRES')) {
//             for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_REQRELRES']) {
//                 // if ReqRelRes costs was payed by a valid trx
//                 let docCostIsPayed = true;

//                 if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
//                     docCostIsPayed = false;
//                     costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for ReqRelRes is rejected because of doublespending` };
//                 }

//                 if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
//                     docCostIsPayed = false;
//                     costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The ReqRelRes costs is not supported by any trx';
//                 }
//                 let reqRelHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
//                 let reqRelDoc = blockUtils.pickDocByHash(block, reqRelHash).doc;

//                 if (reqRelDoc.dClass != 'Basic') {
//                     docCostIsPayed = false;
//                     costPaymentStatus[reqRelHash] = `ReqRel dClass(${reqRelDoc.dClass}) is not supported`;
//                 }

//                 let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[reqRelHash];
//                 let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
//                 if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
//                     docCostIsPayed = false;
//                     costPaymentStatus[reqRelHash] = 'supporter transaction is rejected because of doublespending';
//                 }

//                 clog.app.info(`remove it: a dummy log test for mapping reqRel(${reqRelHash}) to polling(${mapReferencedToReferencer[reqRelHash]})`);

//                 if (docCostIsPayed) {
//                     costPaymentStatus[reqRelHash] = 'Done';
//                     treasuryHandler.insertIncome({
//                         cat: 'TP_REQRELRES',
//                         title: `TP_REQRELRES ReqRelRes(${utils.hash6c(reqRelHash)})`,
//                         descriptions: `TP_REQRELRES ReqRelRes(${utils.hash6c(reqRelHash)})`,
//                         creationDate: block.creationDate,
//                         blockHash: block.blockHash,
//                         refLoc: anAlterTPOutput.TPRefLoc,
//                         value: anAlterTPOutput.TPValue
//                     });

//                 } else {
//                     // remove reqRel because of failed payment
//                     const reqRelsInRelatedBlock = require('../../documents-in-related-block/reqrelres/reqrelres');
//                     reqRelsInRelatedBlock.removeReqRelRes({ reqRelHash });

//                     // also remove related polling
//                     const pollingsInRelatedBlock = require('../../documents-in-related-block/pollings/pollings');
//                     pollingsInRelatedBlock.remove Polling({ pollingHash: mapReferencedToReferencer[reqRelHash] });
//                 }
//             }
//         }
//         return costPaymentStatus;
//     }
// }

// module.exports = ReqRelResCostHandler;
