const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const suspectTrxHandler = require('../../../transaction/utxo/suspect-transactions-handler');
const extinfosHandler = require('../../extinfos-handler');
const DNAHandler = require('../../../dna/dna-handler');
const trxCoinsAnalyzer = require('./analyze-a-transaction-coins');

let msg;

class UTXOAnalyzer {

    static analyzeBlockUsedCoins(block) {
        // UTXO Import Module Common Data Container
        let UIMCDC = {
            importableUTXOs: [],
            cutCeasedTrxFromUTXOs: [],
            supportedP4P: [],
            blockDPCost_Treasury: null,
            blockDPCost_Backer: null,
            blockTreasuryLogs: [],

            P4PDocs: [],

            blockAlterTreasuryIncomes: {},

            trxUDict: {},
            mapUTrxRefToTrxHash: {},
            mapUTrxHashToTrxRef: {},

            // mapTrxHashToPledge: {},
            // mapProposalToPledgedContract: {},
            // mapPledgeHashToPayerTrxHash: {},
            aSingleTrxDPCost: {},
            DPCostsRefLocs: [],
            toBeRestoredRefLocs: [],
            timeLockedDocs: [],

            mustNotImportTrxOutputs: [],    // because trx is rejected or donated for double-spending
            transactionsDetection: {},
            transactionsValidityCheck: {},
            susInputsDetection: [],
            rejectedTransactions: {},   // for each input refLoc must be inserted on record(even in one same transaction)

            mapUReferencerToReferenced: {},
            mapUReferencedToReferencer: {},

            oTimeLockedRelatedDocs: {},    // TODO: implemet it ASAP 
        }

        // retrieve extinfo
        let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
        if (bExtInfoRes.bExtInfo == null) {
            msg = `missed bExtInfo5 (${utils.hash16c(block.blockHash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        let bExtInfo = bExtInfoRes.bExtInfo

        // FIXME: is it possible to code reach here with a normal block while there is no entry in sus records?
        // in this case normal block's output will be recorded as spendable outputs!!!!
        UIMCDC.susVotesByBlockHash = suspectTrxHandler.getSusInfoByBlockHash(block.blockHash);

        UIMCDC.blockIsSusCase = UIMCDC.susVotesByBlockHash.hasSusRecords;
        clog.trx.info(`extract UTXOs from block(${utils.hash6c(block.blockHash)}) blockIsSusCase:(${UIMCDC.blockIsSusCase})`);

        // control shares
        UIMCDC.minimumFloatingVote = iConsts.MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK;
        let huPercent = DNAHandler.getAnAddressShares(iConsts.HU_DNA_SHARE_ADDRESS, block.creationDate).percentage;
        // whenever comunity riched 29 percent of voting power, so they will not more hu's voting in susBlock consideration
        if ((huPercent < 71) && (100 - huPercent < iConsts.MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK)) {
            UIMCDC.minimumFloatingVote = 100 - huPercent;
        }

        if (UIMCDC.blockIsSusCase) {
            clog.sec.error(`UIMCDC.susVotesByBlockHash: ${utils.stringify(UIMCDC.susVotesByBlockHash)}`);
            if (!_.has(UIMCDC.susVotesByBlockHash.votesDict, block.blockHash)) {
                msg = `the sus block(${utils.hash6c(block.blockHash)}) hasn't even one vote! so has to wait to more susVotes to be inserted`;
                UIMCDC.doesEnoughSusVotesExist = 'notEnoughSusVotesExist';
                return UIMCDC;
            }

            UIMCDC.currentVotesPercentage = UIMCDC.susVotesByBlockHash.votesDict[block.blockHash].sumPercent;
            if (UIMCDC.currentVotesPercentage < UIMCDC.minimumFloatingVote) {
                // the machine has to wait to enought susVotes to be inserted
                msg = `the sus block(${utils.hash6c(block.blockHash)}) has ${UIMCDC.susVotesByBlockHash.votesDict[block.blockHash].sumPercent} percents, the machine has to wait to more susVotes to be inserted`;
                UIMCDC.doesEnoughSusVotesExist = 'notEnoughSusVotesExist';
                return UIMCDC;
            }
            UIMCDC.doesEnoughSusVotesExist = true;
            clog.trx.warn(`the block(${utils.hash6c(block.blockHash)}) discovered as an suspiciuos, cloned or P4P transaction`);
        }


        // retrieve p4p supported transactions
        for (let aDoc of block.docs) {
            if (aDoc.dClass == iConsts.TRX_CLASSES.P4P)
                UIMCDC.P4PDocs.push(aDoc);
        }
        // retrieve org ceased backer fee outputs
        if (UIMCDC.P4PDocs.length > 0) {
            for (let aDoc of UIMCDC.P4PDocs) {
                for (let outInx of aDoc.dPIs) {
                    if (aDoc.outputs[outInx][0] != block.backer) {
                        /**
                         * this output of orginal ceased transaction must not be imported in UTXOs because of taking place in this block
                         * cut From Importable UTXOs Because OF Referenced Ceased Trx 
                         **/
                        UIMCDC.cutCeasedTrxFromUTXOs.push(iutils.packCoinRef(aDoc.hash, outInx));
                    }
                }
            }
        }


        // looping on documents in a block
        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let aDoc = block.docs[docInx];

            if (_.has(aDoc, 'ref') && !utils._nilEmptyFalse(aDoc.ref)) {
                if (iutils.isBasicTransaction(aDoc.dType)) {
                    UIMCDC.trxUDict[aDoc.hash] = aDoc;
                    UIMCDC.mapUTrxHashToTrxRef[aDoc.hash] = aDoc.ref;
                    UIMCDC.mapUTrxRefToTrxHash[aDoc.ref] = aDoc.hash;
                } else {
                    UIMCDC.mapUReferencerToReferenced[aDoc.hash] = aDoc.ref;
                    UIMCDC.mapUReferencedToReferencer[aDoc.ref] = aDoc.hash;
                }
            }

            if (!iutils.isBasicTransaction(aDoc.dType) && !iutils.isDPCostPayment(aDoc.dType))
                continue;

            let docExtInfo = _.has(bExtInfo, docInx) ? bExtInfo[docInx] : null;
            UIMCDC = trxCoinsAnalyzer.analyzeATransactionCoins({
                block,
                doc: aDoc,
                docExtInfo,
                UIMCDC
            });
        }

        /**
         * since each doc type need different treatment, so maybe it is not good to have a general controller. 
         * here we can only remove not-payed-docs from blockAlterTreasuryIncomes
         * control if all documents costs are paying by a transaction?
         **/
        clog.app.info(`UIMCDC.blockAlterTreasuryIncomes333 ${utils.stringify(UIMCDC.blockAlterTreasuryIncomes)}`);
        for (let aDocType of utils.objKeys(UIMCDC.blockAlterTreasuryIncomes)) {
            for (let aDoc of UIMCDC.blockAlterTreasuryIncomes[aDocType]) {

            }
        }

        return UIMCDC;

    }
}

module.exports = UTXOAnalyzer
