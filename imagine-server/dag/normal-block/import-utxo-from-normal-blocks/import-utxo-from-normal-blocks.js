const _ = require('lodash');
const iConsts = require('../../../config/constants');
const cnfHandler = require('../../../config/conf-params');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const clog = require('../../../loggers/console_logger');
const utxoHandler = require('../../../transaction/utxo/utxo-handler');
const dagHandler = require('../../graph-handler/dag-handler');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const outputTimeLockHandler = require('../../../transaction/output-time-lock-handler');
const listener = require('../../../plugin-handler/plugin-handler');
const logNormalBlockUTXOsImport = require('../log-import-utxo-from-normal-blocks');
const utxoAnalyzer = require('./analyze-block-utxos');
const ballotCostsHandler = require('./import-ballots-cost');
const pollingCostsHandler = require('./import-pollings-cost');
const admPollingCostsHandler = require('./import-adm-pollings-cost');
const propCostsHandler = require('./import-proposals-cost');
const flensCostsHandler = require('./import-flens-costs/import-flens-costs');
const fPostCostsHandler = require('./import-free-posts-cost/import-free-posts-cost');
const pledgeCostsHandler = require('./import-pledges-cost');
const closePledgeCostsHandler = require('./import-close-pledges-cost');
const blockUtils = require('../../block-utils');
const machine = require('../../../machine/machine-handler');
const kvHandler = require('../../../models/kvalue');


let msg;


const OUTPUT_TIMELOCK_IS_ENABLED = false;    //TODO: develop, teset and release this feature ASAP

class NormalUTXOs {

    static launchImportUTXOsFromNormalBlocks() {
        NormalUTXOs.recursiveImportUTXOs();
    }

    static recursiveImportUTXOs() {
        setTimeout(() => {
            NormalUTXOs.recursiveImportUTXOs();
        }, iConsts.getNBUTXOsImportGap() * iConsts.ONE_MINUTE_BY_MILISECOND); // normaly less than every hour checks for incomes to put in spendable area 
        let cDate = utils.getNow();
        NormalUTXOs.doImportUTXOs({ cDate });
    }

    static doImportUTXOs(args = {}) {
        NormalUTXOs.importNormalBlockUTXOs(args);
        utxoHandler.refreshVisibility();

        if (OUTPUT_TIMELOCK_IS_ENABLED)
            outputTimeLockHandler.importTimeLocked();
    }

    static importNormalBlockUTXOs(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        let wBlocks = this.retrieveProperBlocks({ cDate });
        if (wBlocks.records.length == 0)
            return { err: false, msg: `There is no importable normal block for time(${cDate})` };
        clog.trx.info(`extract matured UTXOs(NormalBlock) on cDate(${cDate}) from blocks: ${wBlocks.records.map(x => utils.hash6c(x.bHash))}`);

        for (let wBlock of wBlocks.records) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;

            let aBlockAnaRes = utxoAnalyzer.analyzeBlockUsedCoins(block);
            aBlockAnaRes.DPCostsRefLocs = utils.objKeys(aBlockAnaRes.aSingleTrxDPCost).map(aDocHash => aBlockAnaRes.aSingleTrxDPCost[aDocHash].refLoc);
            aBlockAnaRes.mustNotImportTrxOutputs = utils.arrayUnique(aBlockAnaRes.mustNotImportTrxOutputs);
            console.log(`aBlockAnaRes(${utils.hash6c(block.blockHash)}) ${utils.stringify(aBlockAnaRes)}`);
            clog.trx.info(`aBlockAnaRes(${utils.hash6c(block.blockHash)}) ${utils.stringify(aBlockAnaRes)}`);


            if (machine.isInSyncProcess()) {
                let currentWNVTBIA = kvHandler.getValueSync('SusBlockWNVTBIA');
                if (currentWNVTBIA == null) {
                    currentWNVTBIA = [];
                    currentWNVTBIA = utils.stringify(currentWNVTBIA);
                    kvHandler.upsertKValueSync('SusBlockWNVTBIA', currentWNVTBIA)
                } else {
                    currentWNVTBIA = utils.parse(currentWNVTBIA);
                }
                if (currentWNVTBIA.includes(block.blockHash)) {
                    if (!aBlockAnaRes.blockIsSusCase) {
                        /**
                         * during insert to parsing q, machine recognized the block is suspicious and must has some FVotes 
                         * and now machine recognized the sus votes still not being considered
                         * so returns back inorder to giving more time to machine to insert upcoming sus votes in few later seconds
                         */
                        clog.trx.info(`can not import block coins because parsingQ recognization was sus and now there is no vote! Block(${utils.hash6c(block.blockHash)}) blockIsSusCase(${aBlockAnaRes.blockIsSusCase}) ${utils.stringify(aBlockAnaRes)}`);

                        return;
                    }
                }

            }

            if (aBlockAnaRes.doesEnoughSusVotesExist == 'notEnoughSusVotesExist') {
                // log block import report
                logNormalBlockUTXOsImport.logImport({
                    blockHash: block.blockHash,
                    aBlockAnaRes: _.clone(aBlockAnaRes)
                });
                return;
            }

            // find all descendent of current block(if exist)
            let wBlocksDescendents = dagHandler.walkThrough.getAllDescendents({ blockHash: block.blockHash }).wBlocks;


            // donate double spended funds(if exist)
            if (_.has(aBlockAnaRes, 'blockTreasuryLogs')) {
                for (let anEntry of aBlockAnaRes.blockTreasuryLogs) {
                    treasuryHandler.donateTransactionInput({
                        title: anEntry.title,
                        cat: anEntry.cat,
                        descriptions: anEntry.descriptions,
                        creationDate: block.creationDate,
                        value: anEntry.value,
                        blockHash: block.blockHash,
                        refLoc: anEntry.refLoc
                    });
                }
            }


            // calculate if Block Trx Fee must be modified
            let toCut = 0;
            let toCutFromTreasuryFee = 0;
            let toCutFromBackerFee = 0;
            for (let docHash of aBlockAnaRes.mustNotImportTrxOutputs) {
                toCut += aBlockAnaRes.aSingleTrxDPCost[docHash].value; // cut the DPCost of rejected/donated transaction from block incomes
            }
            if (toCut > 0) {
                toCutFromBackerFee = utils.floor(toCut * iConsts.BACKER_PERCENT_OF_BLOCK_FEE);// - cnfHandler.getBlockFixCost();
                toCutFromTreasuryFee = utils.floor(toCut - toCutFromBackerFee);
            }
            aBlockAnaRes.toCutFromBackerFee = toCutFromBackerFee;
            aBlockAnaRes.toCutFromTreasuryFee = toCutFromTreasuryFee;

            if (
                (utils.objKeys(aBlockAnaRes.rejectedTransactions).length > 0)

            ) {
                listener.doCallSync('SPSH_block_has_double_spend_input', { block, aBlockAnaRes });
            }

            // import block DPCost Backer & Treasury
            aBlockAnaRes.blockDPCost_BackerFinal = aBlockAnaRes.blockDPCost_Backer.value - aBlockAnaRes.toCutFromBackerFee;
            aBlockAnaRes.blockDPCost_TreasuryFinal = aBlockAnaRes.blockDPCost_Treasury.value - aBlockAnaRes.toCutFromTreasuryFee;
            if (aBlockAnaRes.blockDPCost_BackerFinal < 0)
                aBlockAnaRes.blockDPCost_TreasuryFinal += aBlockAnaRes.blockDPCost_BackerFinal; // to cover cnfHandler.getBlockFixCost()
            aBlockAnaRes.blockHasIncome = (
                (aBlockAnaRes.blockDPCost_BackerFinal > 0) &&
                (aBlockAnaRes.blockDPCost_TreasuryFinal > 0)
            );
            if (aBlockAnaRes.blockHasIncome) {

                // import backer's income
                clog.trx.info(`--- importing Normal block Coins(Backer) Block(${utils.hash6c(block.blockHash)}) `);
                for (let aWBlock of wBlocksDescendents) {
                    let uArgs = {
                        refLoc: aBlockAnaRes.blockDPCost_Backer.refLoc,
                        address: aBlockAnaRes.blockDPCost_Backer.address,
                        value: aBlockAnaRes.blockDPCost_BackerFinal,

                        cloneCode: block.cycle,
                        creationDate: aWBlock.bCreationDate,
                        visibleBy: aWBlock.bHash,
                        refCreationDate: block.creationDate
                    };
                    utxoHandler.addNewUTXO(uArgs)
                    // clog.trx.info(`insert new utxo(NB) ${JSON.stringify(uArgs)}`);
                    // console.log(`\n\n\n insert new utxo(NB) ${JSON.stringify(uArgs)}`);
                }


                // import blockDPCost_Treasury
                let title = aBlockAnaRes.blockDPCost_Treasury.title;

                if (aBlockAnaRes.mustNotImportTrxOutputs.length > 0) {
                    // cut fees because of rejected transactions or ...
                    title += ` - rejected TRXs(${aBlockAnaRes.mustNotImportTrxOutputs.map(x => utils.hash6c(x)).join()}) = sum(${aBlockAnaRes.toCutFromTreasuryFee})`
                }
                treasuryHandler.insertIncome({
                    cat: aBlockAnaRes.blockDPCost_Treasury.cat,
                    title: title,
                    descriptions: aBlockAnaRes.blockDPCost_Treasury.descriptions,
                    creationDate: block.creationDate,
                    blockHash: block.blockHash,
                    refLoc: aBlockAnaRes.blockDPCost_Treasury.refLoc,
                    value: aBlockAnaRes.blockDPCost_TreasuryFinal
                });

                // TODO: refactor this part and redesign code for more elegant architecture! all imports are doing almost same process

                // import free-docs costs payments to treasury
                aBlockAnaRes.fDocsCostStatus = fPostCostsHandler.importFDocsCost({ block, aBlockAnaRes });

                // import Ballot costs payments to treasury
                aBlockAnaRes.ballotsCostStatus = ballotCostsHandler.importBallotsCost({ block, aBlockAnaRes });

                // import Polling costs payments to treasury
                aBlockAnaRes.pollingsCostStatus = pollingCostsHandler.importPollingsCost({ block, aBlockAnaRes });

                // import request for adm polling costs payments to treasury
                aBlockAnaRes.reqRelResCostStatus = admPollingCostsHandler.importAdmPollingsCost({ block, aBlockAnaRes });

                // TODO: remove to 
                // // import request for relaese reserved coins costs payments to treasury
                // aBlockAnaRes.reqRelResCostStatus = reqRelResCostsHandler.importReqRelResCost({ block, aBlockAnaRes });

                // import proposal costs payments to treasury
                aBlockAnaRes.proposalsCostStatus = propCostsHandler.importProposalsCost({ block, aBlockAnaRes });

                // import FleNS costs(register, binding,...) payments to treasury
                aBlockAnaRes.FleNSCostStatus = flensCostsHandler.importFleNSCost({ block, aBlockAnaRes });

                // import pledge costs payments to treasury
                aBlockAnaRes.pledgesCostStatus = pledgeCostsHandler.importPledgesCost({ block, aBlockAnaRes });

                // import close pledge costs payments to treasury
                aBlockAnaRes.pledgesCloseCostStatus = closePledgeCostsHandler.importClosePledgesCost({ block, aBlockAnaRes });



                // import normal UTXOs
                for (let aUTXO of aBlockAnaRes.importableUTXOs) {
                    // remove Ceased transaction's DPCost, if they are in a same block with related P4P transaction
                    // or if the transaction is in some other block which is created by backers which are not listed in dPIs list of transaction
                    if (aBlockAnaRes.cutCeasedTrxFromUTXOs.includes(aUTXO.newCoin))
                        continue;
                    if (aBlockAnaRes.DPCostsRefLocs.includes(aUTXO.newCoin))
                        continue;

                    // looping on all descendents of current block, to be sure all desacendent can see thei utxo in their history
                    clog.trx.info(`--- importing Normal block Coins() Block(${utils.hash6c(block.blockHash)}) `);
                    for (let aWBlock of wBlocksDescendents) {
                        let uArgs = {
                            refLoc: aUTXO.newCoin,
                            address: aUTXO.address,
                            value: aUTXO.value,

                            cloneCode: block.cycle,
                            creationDate: aWBlock.bCreationDate,
                            visibleBy: aWBlock.bHash,
                            refCreationDate: block.creationDate
                        };
                        utxoHandler.addNewUTXO(uArgs);
                    }
                }
            }


            // restoring UTXOs of rejected transactions
            console.log(`toBeRestoredRefLocs: ${utils.stringify(aBlockAnaRes.toBeRestoredRefLocs)}`);
            for (let aUTXO of aBlockAnaRes.toBeRestoredRefLocs) {
                // looping on all descendents of current block, to be sure all desacendent can see thei utxo in their history
                clog.trx.info(`--- importing Normal block Coins(restored) Block(${utils.hash6c(block.blockHash)}) `);
                for (let aWBlock of wBlocksDescendents) {
                    let uArgs = {
                        refLoc: aUTXO.refLoc,
                        address: aUTXO.address,
                        value: aUTXO.value,
                        refCreationDate: aUTXO.refCreationDate,
                        cloneCode: aUTXO.cycle,

                        creationDate: aWBlock.bCreationDate,
                        visibleBy: aWBlock.bHash
                    };
                    utxoHandler.addNewUTXO(uArgs);
                }
            }


            // log block import report
            logNormalBlockUTXOsImport.logImport({
                blockHash: block.blockHash,
                aBlockAnaRes: _.clone(aBlockAnaRes)
            });


            // update utxo_imported
            dagHandler.updateUtxoImported(block.blockHash, iConsts.CONSTS.YES);

        }
    }

    static retrieveProperBlocks(args) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        //find normal block with 12 hours age old, and insert the outputs as a matured & spendable outputs to table trx_utxos
        let minCreationDate = utils.minutesBefore(iConsts.getCycleByMinutes(), cDate)
        clog.trx.info(`importing matured UTXOs(Nornam Block) before: ${minCreationDate}`);

        let query = [
            ['b_type', ['IN', [iConsts.BLOCK_TYPES.Normal]]],
            ['b_utxo_imported', iConsts.CONSTS.NO],
            ['b_creation_date', ['<=', minCreationDate]],   // (12 hours * 60 minutes) from now
        ];
        if (dagHandler.DAGHasBlocksWhichAreCreatedInCurrrentCycle()) {
            /**
             * by (DAGHasBlocksWhichAreCreatedInCurrrentCycle) clause we are almost sure the machine is synched
             * so must avoiding immidiately importing blocks with fake-old-creationDate
             * all above condition & clauses are valid for a normal working machine. 
             * but if machine newly get synched, it has some blocks which are newly received but belongs to 
             * some old cycles
             * so we control if machine was in sync mode in last 12 hours? if no we add the b_receive_date condition
             */
            let lastSyncStatus = machine.getLastSyncStatus();
            clog.app.info(`last SyncStatus in import Normal Block UTXOs: ${utils.stringify(lastSyncStatus)}`);
            if (lastSyncStatus.lastTimeMachineWasInSyncMode < utils.minutesBefore(iConsts.getCycleByMinutes()))
                query.push(['b_receive_date', ['<', minCreationDate]]);
        }
        let records = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_body'],
            query,
            order: [['b_creation_date', 'ASC']]
        });
        if (records.length == 0)
            return { err: false, records: [] };

        return { err: false, records };

    }

}

module.exports = NormalUTXOs;
