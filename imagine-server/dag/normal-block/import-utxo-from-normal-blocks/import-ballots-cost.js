const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const ballotsInRelatedBlock = require('../../documents-in-related-block/ballots/ballots');
const blockUtils = require('../../block-utils');


class BallotCostHandler {

    static importBallotsCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_BALLOT')) {
            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_BALLOT']) {
                // if ballot costs was payed by a valid trx
                let docCostIsPayed = true;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for Ballot is rejected because of doublespending` };
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The Ballot costs is not supported by any trx';
                }
                let ballotHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let ballotDoc = blockUtils.pickDocByHash(block, ballotHash).doc;

                if (ballotDoc.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
                    docCostIsPayed = false;
                    costPaymentStatus[ballotHash] = `Ballot dClass(${ballotDoc.dClass}) is not supported`;
                }

                let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[ballotHash];
                let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
                if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[ballotHash] = 'supporter transaction is rejected because of doublespending';
                }

                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_BALLOT)`);

                    costPaymentStatus[ballotHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_BALLOT',
                        title: `TP_BALLOT Ballot(${utils.hash6c(ballotHash)})`,
                        descriptions: `TP_BALLOT Ballot(${utils.hash6c(ballotHash)})`,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_BALLOT)`);

                    ballotsInRelatedBlock.removeBallot(ballotHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = BallotCostHandler;
