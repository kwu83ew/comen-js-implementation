const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const listener = require('../../../plugin-handler/plugin-handler');
const pledgeInRelatedBlock = require('../../documents-in-related-block/pledges/pledges');
const proposalsInRelatedBlock = require('../../documents-in-related-block/proosals/proposals');
const blockUtils = require('../../block-utils');


class ProposalCostHandler {

    static importProposalsCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_PROPOSAL')) {
            clog.trx.info(`try to import TP_PROPOSAL: ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_PROPOSAL'])}`)

            let mapPledgeHashToPayerTrxHash = {};
            let mapProposalToPledgedContract = {};
            for (let aDoc of block.docs) {
                // retrieve proposals & pledge mappings
                if (
                    (aDoc.dType == iConsts.DOC_TYPES.Pledge) &&
                    (aDoc.dClass == iConsts.PLEDGE_CLASSES.PledgeP) &&
                    _.has(aDoc, 'proposalRef')) {
                    mapPledgeHashToPayerTrxHash[aDoc.hash] = aDoc.trx;
                    mapProposalToPledgedContract[aDoc.proposalRef] = aDoc.hash;
                    // mapTrxHashToPledge[aDoc.trx] = aDoc.hash;
                }
    
            }


            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_PROPOSAL']) {
                // if proposal costs was payed by Pledging & a lending transaction
                let docCostIsPayed = true;
                
                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for Proposal is rejected because of doublespending`};
                }
                
                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: 'The proposal costs is not supported by any trx' };
                }
                
                let proposalHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let proposalDoc = blockUtils.pickDocByHash(block, proposalHash).doc;
                costPaymentStatus[proposalHash] = {};

                if (proposalDoc.dClass != iConsts.PROPOSAL_CLASESS.General) {
                    docCostIsPayed = false;
                    costPaymentStatus[proposalHash] = { msg: `Proposal dClass(${proposalDoc.dClass}) is not supported` };
                }

                let incomeTitle;
                let payerTrxHash = aBlockAnaRes.mapUTrxRefToTrxHash[proposalHash];

                if (_.has(mapProposalToPledgedContract, proposalHash)) {
                    let supporterPledge = mapProposalToPledgedContract[proposalHash];
                    if (!_.has(mapPledgeHashToPayerTrxHash, supporterPledge)) {
                        docCostIsPayed = false;
                        costPaymentStatus[proposalHash] = { msg: 'proposal is not payed by a transaction' };
                    }

                    let payerTrxHash2 = mapPledgeHashToPayerTrxHash[supporterPledge];
                    if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, payerTrxHash2) || (aBlockAnaRes.mapUTrxHashToTrxRef[payerTrxHash2] != proposalHash)) {
                        docCostIsPayed = false;
                        costPaymentStatus[proposalHash] = { msg: 'supporter transaction is referred to different doc' };
                    }

                    if (payerTrxHash != payerTrxHash2) {
                        docCostIsPayed = false;
                        costPaymentStatus[proposalHash] = { msg: `not same payerTrxHash! ${payerTrxHash}!=${payerTrxHash2}` };
                    }

                    incomeTitle = `TP_PROPOSAL Proposal(${utils.hash6c(proposalHash)}) Pledge(${utils.hash6c(supporterPledge)}) Trx(${utils.hash6c(payerTrxHash)}) `;

                } else {
                    // probably the proposal costs payed by a transaction directly signed by proposer
                    incomeTitle = `TP_PROPOSAL Proposal(${utils.hash6c(proposalHash)}) Trx(${utils.hash6c(payerTrxHash)}) `;

                }

                if (utils.objKeys(aBlockAnaRes.rejectedTransactions).includes(payerTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[proposalHash] = { msg: 'supporter transaction is rejected because of doublespending' };
                }

                if (!_.has(aBlockAnaRes.mapUTrxRefToTrxHash, proposalHash) || (aBlockAnaRes.mapUTrxRefToTrxHash[proposalHash] != payerTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[proposalHash] = { msg: 'supporter transaction ref is defferent than proposal hash' };
                }

                costPaymentStatus[proposalHash].isPayed = docCostIsPayed;
                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PROPOSAL)`);

                    costPaymentStatus[proposalHash] = { msg: 'Done' };
                    treasuryHandler.insertIncome({
                        cat: 'TP_PROPOSAL',
                        title: incomeTitle,
                        descriptions: incomeTitle,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PROPOSAL)`);    

                    clog.sec.error(`costPaymentStatus[proposalHash]: ${utils.stringify(costPaymentStatus[proposalHash])}`)
                    // since by adding block to DAG, the proposals(and related pollings) also were added to DAG, and after 12 hours we found the payment
                    // for that particulare proposal is failed, so we must remove both from data base
                    // also removing pledge!(if exist)
                    proposalsInRelatedBlock.removeProposal(proposalHash);

                    const pollHandler = require('../../../services/polling-handler/general-poll-handler');
                    pollHandler.removePollingByRelatedProposal(proposalHash);

                    pledgeInRelatedBlock.removePledgeBecauseOfPaymentsFail(supporterPledge);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = ProposalCostHandler;
