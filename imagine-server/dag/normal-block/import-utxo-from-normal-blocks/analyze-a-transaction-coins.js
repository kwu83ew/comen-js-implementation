const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const clog = require('../../../loggers/console_logger');
const suspectTrxHandler = require('../../../transaction/utxo/suspect-transactions-handler');
const dagHandler = require('../../graph-handler/dag-handler');
const rejectedTrxHandler = require('../rejected-transactions-handler');
const outputTimeLockHandler = require('../../../transaction/output-time-lock-handler');
const trxHashHandler = require('../../../transaction/hashable');


const OUTPUT_TIMELOCK_IS_ENABLED = false;    //TODO: develope, teset and release this feature ASAP
let msg;
let UIMCD2C;

class AnalyzeTrxCoins {


    static analyzeATransactionCoins(args) {
        let block = args.block;
        let doc = args.doc;
        let docExtInfo = args.docExtInfo;
        UIMCD2C = args.UIMCDC;



        /**
         * find DPCost payments, to not importing in UTXOs 
         * and also in case of doublespending removing these incomes from blockTotalTrxFee & treauryTotalTrxFeeIncome
         */
        if (_.has(doc, 'dPIs') && Array.isArray(doc.dPIs)) {
            for (let inx of doc.dPIs) {
                if ((doc.dType != iConsts.DOC_TYPES.DPCostPay) && (doc.outputs[inx][0] == block.backer)) {
                    UIMCD2C.aSingleTrxDPCost[doc.hash] = {
                        refLoc: iutils.packCoinRef(doc.hash, parseInt(inx)),
                        address: doc.outputs[inx][0],
                        value: iutils.convertBigIntToJSInt(doc.outputs[inx][1]),
                        refCreationDate: block.creationDate
                    };
                }
            }
        }

        UIMCD2C.transactionsDetection[doc.hash] = 'Normal';


        if (OUTPUT_TIMELOCK_IS_ENABLED) {

            // retrieve latest redeemTimes(if exist)
            let { docInputs, docMaxRedeem } = outputTimeLockHandler.getDocInputsAndMaxRedeem({
                doc,
                docExtInfo
            });
            let isOutputTimeLockedRelatedDoc = outputTimeLockHandler.isTimeLockRelated(docInputs);
            if (isOutputTimeLockedRelatedDoc)
                UIMCD2C.oTimeLockedRelatedDocs[doc.hash] = outputTimeLockHandler.isTimeLockRelated(docInputs);
            clog.trx.info(`doc(${utils.hash6c(doc.hash)}) is time Locked Related Doc(${isOutputTimeLockedRelatedDoc}) inputs(${docInputs.map(x => iutils.shortCoinRef(x))})`);
            console.log(`doc(${utils.hash6c(doc.hash)}) is time Locked Related Doc(${isOutputTimeLockedRelatedDoc}) inputs(${docInputs.map(x => iutils.shortCoinRef(x))})`);
            if ((0 < docMaxRedeem) || isOutputTimeLockedRelatedDoc) {
                // save redeem info to apply it on right time, and return
                let refLocBlocks = dagHandler.getCoinsGenerationInfoViaSQL(docInputs);
                for (let refLoc of docInputs) {
                    UIMCD2C.timeLockedDocs.push({
                        blockHash: block.blockHash,
                        docHash: doc.hash,
                        pureHash: trxHashHandler.getPureHash(doc),
                        refLoc,
                        doc,
                        redeemTime: utils.minutesAfter(docMaxRedeem + iConsts.getCycleByMinutes(), block.creationDate),
                        docMaxRedeem,
                        cloneCode: block.cycle,
                        refCreationDate: refLocBlocks[refLoc].coinGenCreationDate
                    });
                }
                let redeemTime = utils.minutesAfter(docMaxRedeem + iConsts.getCycleByMinutes(), block.creationDate);
                console.log(`doc(${utils.hash6c(doc.hash)}) created On(${block.creationDate}) MaxRedeem: ${utils.stringify(docMaxRedeem)} redeemTime(${redeemTime})`);
                clog.trx.info(`doc(${utils.hash6c(doc.hash)}) created On(${block.creationDate}) MaxRedeem: ${utils.stringify(docMaxRedeem)} redeemTime(${redeemTime})`);

                // do nothing more with this document
                UIMCD2C.transactionsDetection[doc.hash] = 'timeLocked';
                return UIMCD2C;
            }
        }

        if (!UIMCD2C.blockIsSusCase) {
            UIMCD2C.canImportNormally = true;
            AnalyzeTrxCoins.extractDocImportableUTXOs({
                block,
                doc
            });
            return UIMCD2C;
        }


        let validity;
        let consideredSusRefLocs = [];

        // control if there is atleast one susVote for this document?
        let susVotesForDoc = suspectTrxHandler.getSusInfoByDocHash({ invokedDocHash: doc.hash });
        console.log(`vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv`);
        console.log(`vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv`, susVotesForDoc);
        if (_.has(susVotesForDoc, 'allCoinsAreValid')) {
            msg = `doc(${utils.hash6c(doc.hash)}) of block(${utils.hash6c(block.blockHash)}) `
            msg += `is not sus (even if block is blockIsSusCase) allCoinsOfDocAreValid`
            clog.trx.info(msg);
            UIMCD2C.canImportNormally = true;
            UIMCD2C.transactionsDetection[doc.hash] = 'There is no sus vote for doc';
            AnalyzeTrxCoins.extractDocImportableUTXOs({
                block,
                doc
            });
            return UIMCD2C;
        }

        let { rawVotes } = suspectTrxHandler.retrieveVoterPercentages({ rawVotes: susVotesForDoc.rawVotes })

        // analyze votes and decide about dobiuos transactions
        validity = suspectTrxHandler.checkDocValidity({ doLog: true, invokedDocHash: doc.hash, rawVotes });
        UIMCD2C.transactionsValidityCheck[doc.hash] = validity;

        let canImportNormally = false;
        if (_.has(validity, 'cloned') && (validity.cloned == 'cloned')) {
            // the document/transaction is cloned, so can import outputs regularely
            msg = `recognized a cloned trx(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)})`
            clog.trx.info(msg);
            canImportNormally = true;
            UIMCD2C.transactionsDetection[doc.hash] = 'cloned';
        }
        consideredSusRefLocs = _.has(validity, 'susVoteRes') ? utils.objKeys(validity.susVoteRes) : [];
        if ((consideredSusRefLocs.length == 1) && (validity.susVoteRes[consideredSusRefLocs[0]].valid == true)) {
            msg = `Only one sus-input exist and it is valid trx(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)})`;
            clog.trx.info(msg);
            canImportNormally = true;
            UIMCD2C.transactionsDetection[doc.hash] = 'Only one sus-input exist and it is valid';
        }
        UIMCD2C.canImportNormally = canImportNormally;
        if (canImportNormally) {
            // so doc can be imported normally
            AnalyzeTrxCoins.extractDocImportableUTXOs({
                block,
                doc
            });
            return UIMCD2C;
        }

        // transaction needs special treatment.
        // no out put of this transaction will be imported to UTXOs
        // depends on vote result, transaction inputs can be 
        // 1. donated to treasury, 
        // 2. restored in UTXOs, 
        // 3. denied
        return this.doSusTreatments({
            block,
            doc,
            validity,
            consideredSusRefLocs,
            UIMCD2C,
        });

    }

    static doSusTreatments(args) {
        let block = args.block;
        let doc = args.doc;
        let validity = args.validity;
        let consideredSusRefLocs = args.consideredSusRefLocs;

        let deniedInputs = [];  // not restored and not donates. because already is spent properly
        let toBeRemovedUTXOs = [];  // the UTXOs thast must be remove from valid utxos

        clog.trx.info(`validity response for block(${utils.hash6c(block.blockHash)}) doc(${utils.hash6c(doc.hash)}) ${JSON.stringify(validity)}`);
        console.log(`validity response for block(${utils.hash6c(block.blockHash)}) doc(${utils.hash6c(doc.hash)}) ${JSON.stringify(validity)}`);
        let susVoteRes = _.has(validity, 'susVoteRes') ? validity.susVoteRes : null;

        console.log('consideredReLocsconsidered RefLocsconsidered RefLocsconsidered RefLocsconsidered RefLocs', consideredSusRefLocs);
        clog.trx.info(`sus susVoteRes entered in checking block(${utils.hash6c(block.blockHash)}) doc(${utils.hash6c(doc.hash)})`);

        for (let refLoc of consideredSusRefLocs) {

            if (susVoteRes[refLoc].valid == true) {
                // this input is valid, but by rejecting whole transaction and restoring inputs the input will remain un-spended.
                UIMCD2C.toBeRestoredRefLocs.push(refLoc);
                UIMCD2C.susInputsDetection.push({
                    refLoc: iutils.shortCoinRef(refLoc),
                    detection: 'valid input of consideredSusRefLocs!!!'
                });
                continue;
            }

            if (susVoteRes[refLoc].action == 'reject') {
                // even one rejection or donation of inputs is enough to cut trxFee dfrom blockTrxFee & bloockTreasuryFee
                UIMCD2C.mustNotImportTrxOutputs.push(doc.hash);
                UIMCD2C.transactionsDetection[doc.hash] = 'reject';

                clog.trx.error(`susVotes: Reject ref(${iutils.shortCoinRef(refLoc)}) of transaction(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)}) because of susVotes`);
                toBeRemovedUTXOs.push(refLoc);  // probably it already done before
                deniedInputs.push(refLoc);
                UIMCD2C.susInputsDetection.push({
                    refLoc: iutils.shortCoinRef(refLoc),
                    detection: 'Already spended input'
                });

                if (!_.has(UIMCD2C.rejectedTransactions, doc.hash))
                    UIMCD2C.rejectedTransactions[doc.hash] = [];
                UIMCD2C.rejectedTransactions[doc.hash].push(refLoc);
                rejectedTrxHandler.addTransaction({
                    blockHash: block.blockHash,
                    docHash: doc.hash,
                    refLoc
                });
                continue;
            }

            if (susVoteRes[refLoc].action == 'donate') {
                // even one rejection or donation of inputs is enough to cut trxFee dfrom blockTrxFee & bloockTreasuryFee
                UIMCD2C.mustNotImportTrxOutputs.push(doc.hash);

                clog.trx.error(`susVotes: Donating ref(${iutils.shortCoinRef(refLoc)}) of transaction(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)}) because of susVotes`);
                // donate conflicted input, and since do not consider transaction, so the other inputs remaine untouched
                let donateRefLocsBlocks = dagHandler.retrieveBlocksInWhichARefLocHaveBeenProduced(refLoc);
                clog.trx.info(`donate Transaction Input. blocks by refLoc: ${JSON.stringify(donateRefLocsBlocks)}`);
                // big FIXME: for cloning transactions issue
                let refBlock = donateRefLocsBlocks[0];
                UIMCD2C.blockTreasuryLogs.push({
                    title: `Donate because of DOUBLE-SPENDING`,
                    cat: `TP_DONATE_DOUBLE_SPEND`,
                    descriptions: `Pay to Treasury because of trx Conflict in block(${utils.hash6c(block.blockHash)}).doc(${utils.hash6c(doc.hash)})`,
                    refLoc,
                    value: refBlock.outputValue,
                    donateRefLocsBlocks: donateRefLocsBlocks,
                });
                UIMCD2C.susInputsDetection.push({
                    refLoc: iutils.shortCoinRef(refLoc),
                    detection: 'donate input'
                })

                if (!_.has(UIMCD2C.rejectedTransactions, doc.hash))
                    UIMCD2C.rejectedTransactions[doc.hash] = [];
                UIMCD2C.rejectedTransactions[doc.hash].push(refLoc);

                // record rejected transaction
                rejectedTrxHandler.addTransaction({
                    blockHash: block.blockHash,
                    docHash: doc.hash,
                    refLoc
                });


            } else {
                utils.exiter('\n\n\nstrange situation in sus voting', 98);
            }

        }

        // the entire UTXOs of a block during adding block to DAG are removed from trx_utxos
        // so here we have to resotore used UTXOs of this rejected-transaction or donated-transaction into spendable UTXOs, 
        // except conflicted refLocs
        for (let input of doc.inputs) {
            let refLoc = iutils.packCoinRef(input[0], input[1]);
            if (!consideredSusRefLocs.includes(refLoc)) {
                let refLocBlock = dagHandler.getCoinsGenerationInfoViaSQL([refLoc]);
                UIMCD2C.toBeRestoredRefLocs.push({
                    cycle: refLocBlock[refLoc].coinGenCycle,
                    refLoc: refLoc,
                    address: refLocBlock[refLoc].coinGenOutputAddress,
                    value: iutils.convertBigIntToJSInt(refLocBlock[refLoc].coinGenOutputValue),
                    refCreationDate: refLocBlock[refLoc].coinGenCreationDate,
                });
                UIMCD2C.susInputsDetection.push({
                    refLoc: iutils.shortCoinRef(refLoc),
                    detection: 'valid input'
                });
            }
        }

        return UIMCD2C;

    }


    static extractDocImportableUTXOs(args) {
        let block = args.block;
        let doc = args.doc;

        clog.trx.info(`importing UTXOs from block(${utils.hash6c(block.blockHash)}) doc(${utils.hash6c(doc.hash)})`);

        if (doc.dType == iConsts.DOC_TYPES.DPCostPay) {
            // the block fee payment transaction always has to have no input and 2 outputs.
            // 1. TP_DP
            // 2. backer fee
            UIMCD2C.blockDPCost_Treasury = {
                cat: 'TP_DP',
                title: `TP_DP Backing fee block(${utils.hash6c(block.blockHash)})`,
                descriptions: `Backing fee block(${utils.hash6c(block.blockHash)})`,
                // creationDate: block.creationDate,
                // blockHash: block.blockHash,
                refLoc: iutils.packCoinRef(doc.hash, 0),
                value: iutils.convertBigIntToJSInt(doc.outputs[0][1])
            };

            UIMCD2C.blockDPCost_Backer = {
                // cloneCode: block.cycle,
                refLoc: iutils.packCoinRef(doc.hash, 1),
                // creationDate: wBlockEFS.creation_date,
                // visibleBy: wBlockEFS.block_hash,
                address: doc.outputs[1][0],
                value: doc.outputs[1][1],
                // refCreationDate: block.creationDate
            };
            return;
        }

        if (!_.has(doc, 'outputs'))
            return;

        for (let outputIndex = 0; outputIndex < doc.outputs.length; outputIndex++) {
            let anOutput = doc.outputs[outputIndex];
            let newCoin = false;

            // exclude backerfee from all documents outputs, except the integrated one in which there is also treasury income
            if ((iConsts.TREASURY_PAYMENTS.includes(anOutput[0])) && (doc.dType != iConsts.DOC_TYPES.DPCostPay)) {
                // // insert treasury incomes
                // let ttl = `Treasury income(${anOutput[0]}) block(${utils.hash6c(block.blockHash)}) doc(${utils.hash6c(doc.hash)})`
                // UIMCD2C.blockTreasuryLogs.push({
                //     cat: anOutput[0],
                //     title: ttl,
                //     descriptions: ttl,
                //     // creationDate: block.creationDate,
                //     // blockHash: block.blockHash,
                //     refLoc: iutils.packCoinRef(doc.hash, parseInt(outputIndex)),
                //     value: iutils.convertBigIntToJSInt(anOutput[1])
                // });

                if (!_.has(UIMCD2C.blockAlterTreasuryIncomes, anOutput[0]))
                    UIMCD2C.blockAlterTreasuryIncomes[anOutput[0]] = [];
                let msg = `going to insert a blockAlterTreasuryIncomes ${anOutput[0]} `;
                clog.app.info(msg);
                UIMCD2C.blockAlterTreasuryIncomes[anOutput[0]].push({
                    TPTrxHash: doc.hash,
                    TPRefLoc: iutils.packCoinRef(doc.hash, parseInt(outputIndex)),
                    TPValue: iutils.convertBigIntToJSInt(anOutput[1])
                });
                clog.app.info(`UIMCD2C.blockAlterTreasuryIncomes ${utils.stringify(UIMCD2C.blockAlterTreasuryIncomes)}`);


            } else {
                newCoin = iutils.packCoinRef(doc.hash, parseInt(outputIndex));

            }
            let outputValue = iutils.convertBigIntToJSInt(anOutput[1]);

            // exclude backerfee(s), the one intended for this current backer and potentially other cloned backer's fee
            if (
                (doc.dType != iConsts.DOC_TYPES.DPCostPay)
                && _.has(doc, 'dPIs')
                && Array.isArray(doc.dPIs)
                && doc.dPIs.includes(outputIndex)
                // && (anOutput[0] == block.backer)
            ) {
                newCoin = false;  //cutCeasedTrxFromUTXOs
            }

            if (newCoin) {
                let uArgs = {
                    newCoin,
                    address: anOutput[0],
                    value: outputValue,
                };
                UIMCD2C.importableUTXOs.push(uArgs);
            }
        }

        return;

    }

}

module.exports = AnalyzeTrxCoins;
