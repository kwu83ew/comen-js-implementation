const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const closePledgeInRelatedBlock = require('../../documents-in-related-block/pledges/close');
// const closePledgeHandler = require('../../../contracts/pledge-contract/pledge-close');
const blockUtils = require('../../block-utils');


class PledgeCloseCostHandler {

    static importClosePledgesCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_PLEDGE_CLOSE')) {
            clog.trx.info(`importing TP_PLEDGE_CLOSE: ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_PLEDGE_CLOSE'])}`);
            console.log(`importing TP_PLEDGE_CLOSE: ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_PLEDGE_CLOSE'])}`);

            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_PLEDGE_CLOSE']) {
                // if closing cost is payed by a valid trx
                let docCostIsPayed = true;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for close-pledge is rejected because of doublespending`;
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The Close Pledge costs is not supported by any trx!';
                }
                console.log('mapTrxHashToTrxRef mapTrxHashToTrxRef ', aBlockAnaRes.mapUTrxHashToTrxRef);
                let theDocHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let theDoc = blockUtils.pickDocByHash(block, theDocHash).doc;

                if ((theDoc.dType != iConsts.DOC_TYPES.ClosePledge) || (theDoc.dClass != iConsts.PLEDGE_CLOSE_CLASESS.General)) {
                    docCostIsPayed = false;
                    costPaymentStatus[theDocHash] = `Close Pledge dType(${theDoc.dType}) or dClass(${theDoc.dClass}) is not supported`;
                }

                let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[theDocHash];
                let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
                if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[theDocHash] = 'supporter transaction for close pledge is rejected because of doublespending';
                }

                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PLEDGE_CLOSE)`);

                    costPaymentStatus[theDocHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_PLEDGE_CLOSE',
                        title: `TP_PLEDGE_CLOSE doc(${utils.hash6c(theDocHash)})`,
                        descriptions: `TP_PLEDGE_CLOSE doc(${utils.hash6c(theDocHash)})`,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PLEDGE_CLOSE)`);

                    // re-pleadging account
                    closePledgeInRelatedBlock.removeClosePledgeBecauseOfPaymentsFail(theDocHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = PledgeCloseCostHandler;
