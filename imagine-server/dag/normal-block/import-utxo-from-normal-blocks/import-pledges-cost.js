
const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const pledgeInRelatedBlock = require('../../documents-in-related-block/pledges/pledges');
const proposalsInRelatedBlock = require('../../documents-in-related-block/proosals/proposals');
const blockUtils = require('../../block-utils');


class pledgeCostHandler {

    static importPledgesCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_PLEDGE')) {
            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_PLEDGE']) {
                // if proposal costs was payed by Pledging & a lending transaction
                let docCostIsPayed = true;
                // let proposalHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let pledgeHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                console.log('anAlterTPOutput.TPTrxHash', anAlterTPOutput.TPTrxHash);
                console.log('pledgeHash', pledgeHash);

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[pledgeHash] = `Pledge(${utils.hash6c(pledgeHash)}) is payed by a rejected transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)})`;
                }

                let pledgeDoc = blockUtils.pickDocByHash(block, pledgeHash).doc;
                if (pledgeDoc.dClass != iConsts.PLEDGE_CLASSES.PledgeP) {
                    docCostIsPayed = false;
                    costPaymentStatus[pledgeHash] = `Pledge dClass(${pledgeDoc.dClass}) is not supported`;
                }

                // probably all these controls do not have sence

                // if (proposalDoc.dClass != iConsts.PLEDGE_CLASSES.PledgeP) {
                //     docCostIsPayed = false;
                //     costPaymentStatus[proposalHash] = `Proposal dClass(${proposalDoc.dClass}) is not supported`;
                // }

                // if (!_.has(aBlockAnaRes.mapProposalToPledgedContract, proposalHash)) {
                //     docCostIsPayed = false;
                //     costPaymentStatus[proposalHash] = 'proposal is not supported by pledge contract';
                // }
                // if (!_.has(aBlockAnaRes.mapUTrxRefToTrxHash, proposalHash) || (aBlockAnaRes.mapUTrxRefToTrxHash[proposalHash] != supporterTrx)) {
                //     docCostIsPayed = false;
                //     costPaymentStatus[proposalHash] = 'supporter transaction ref is defferent than proposal hash';
                // }

                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PLEDGE)`);

                    costPaymentStatus[pledgeHash] = 'Done';
                    // let title = `TP_PLEDGE Proposal(${(pledgeHash)}) Pledge(${(pledgeHash)}) Trx(${(anAlterTPOutput.TPTrxHash)})`;
                    let title = `TP_PLEDGE Proposal(${utils.hash6c(pledgeHash)}) Pledge(${utils.hash6c(pledgeHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)})`;
                    treasuryHandler.insertIncome({
                        cat: 'TP_PLEDGE',
                        title,
                        descriptions: title,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_PLEDGE)`);

                    pledgeInRelatedBlock.removePledgeBecauseOfPaymentsFail(pledgeHash);
                    let proposalHash = blockUtils.pickDocByHash(block, pledgeHash).doc.proposalRef;
                    proposalsInRelatedBlock.removeProposal(proposalHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = pledgeCostHandler;
