const _ = require('lodash');
const iConsts = require('../../../../config/constants');
const utils = require('../../../../utils/utils');
const iutils = require('../../../../utils/iutils');
const model = require('../../../../models/interface');
const blockUtils = require('../../../block-utils');
const iNameInRelatedBlock = require('../../../documents-in-related-block/inames/inames');

class FleNSCostsHandler {

    static importFleNSCost(args) {
        // let block = args.block;
        // let aBlockAnaRes = args.aBlockAnaRes;

        let FleNSCostStatus = {};

        // handle iName register costs
        FleNSCostStatus['TP_INAME_REG'] = require('./register-cost').importRegCost(args);

        FleNSCostStatus['TP_INAME_BIND'] = require('./bind-to-iname-cost').importBindingCost(args);

        FleNSCostStatus['TP_INAME_MSG'] = require('./msg-to-iname-cost').importINameMsgCost(args);

        
        return FleNSCostStatus;
    }
}

module.exports = FleNSCostsHandler;