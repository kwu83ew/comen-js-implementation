const _ = require('lodash');
const iConsts = require('../../../../config/constants');
const utils = require('../../../../utils/utils');
const iutils = require('../../../../utils/iutils');
const model = require('../../../../models/interface');
const clog = require('../../../../loggers/console_logger');
const treasuryHandler = require('../../../../services/treasury/treasury-handler');
const blockUtils = require('../../../block-utils');
const iNameInRelatedBlock = require('../../../documents-in-related-block/inames/inames');

class FleNSRegCostsHandler {

    static importRegCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        // handle iName register costs

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_INAME_REG')) {
            clog.trx.info(`importing FleNS payments ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_REG'])}`);

            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_REG']) {
                // if proposal costs was payed by Pledging & a lending transaction
                let docCostIsPayed = true;

                let iNameHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let iNamelDoc = blockUtils.pickDocByHash(block, iNameHash).doc;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[iNameHash] = `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for reg-iname is rejected because of doublespending`;
                }


                // if (!_.has(aBlockAnaRes.mapUTrxRefToTrxHash, iNameHash) ||
                //     (aBlockAnaRes.mapUTrxRefToTrxHash[iNameHash] != supporterTrx)) {
                //     docCostIsPayed = false;
                //     costPaymentStatus[iNameHash] = 'No one payes for iName register request';
                // }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The iName costs is not supported by any trx';
                }



                // if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, supporterTrx) || (aBlockAnaRes.mapUTrxHashToTrxRef[supporterTrx] != iNameHash)) {
                //     docCostIsPayed = false;
                //     costPaymentStatus[iNameHash] = 'supporter transaction is referred to different doc';
                // }


                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_REG)`);

                    costPaymentStatus[iNameHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_INAME_REG',
                        title: `TP_INAME_REG iName(${utils.hash6c(iNameHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        descriptions: `TP_INAME_REG iName(${utils.hash6c(iNameHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_REG)`);    

                    clog.app.error(`costPaymentStatus not payed ${utils.stringify(costPaymentStatus)}`);
                    iNameInRelatedBlock.removeINameBecauseOfPaymentsFail(iNameHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = FleNSRegCostsHandler;
