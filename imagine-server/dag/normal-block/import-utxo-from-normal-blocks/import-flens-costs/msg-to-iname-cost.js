const _ = require('lodash');
const iConsts = require('../../../../config/constants');
const utils = require('../../../../utils/utils');
const iutils = require('../../../../utils/iutils');
const model = require('../../../../models/interface');
const clog = require('../../../../loggers/console_logger');
const treasuryHandler = require('../../../../services/treasury/treasury-handler');
const blockUtils = require('../../../block-utils');
const iNameMsgsInRelatedBlock = require('../../../documents-in-related-block/inames/msg');

class FleNSMsgCostsHandler {

    static importINameMsgCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        // handle msg-to-iName costs

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_INAME_MSG')) {
            clog.trx.info(`importing FleNS-bind payments ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_MSG'])}`);

            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_MSG']) {
                // if proposal costs was payed by Pledging & a lending transaction
                let docCostIsPayed = true;

                let docHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let theDoc = blockUtils.pickDocByHash(block, docHash).doc;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[docHash] = `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for Msg-to-iName is rejected because of doublespending`;
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The Msg-to-iName costs is not supported by any trx';
                }


                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_MSG)`);

                    costPaymentStatus[docHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_INAME_MSG',
                        title: `TP_INAME_MSG iName(${utils.hash6c(docHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        descriptions: `TP_INAME_MSG iname(${utils.hash6c(docHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_MSG)`);    

                    clog.app.error(`costPaymentStatus not payed ${utils.stringify(costPaymentStatus)}`);
                    iNameMsgsInRelatedBlock.removeMsgBecauseOfPaymentsFail(docHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = FleNSMsgCostsHandler;
