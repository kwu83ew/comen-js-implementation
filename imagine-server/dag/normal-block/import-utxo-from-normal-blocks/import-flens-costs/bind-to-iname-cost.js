const _ = require('lodash');
const iConsts = require('../../../../config/constants');
const utils = require('../../../../utils/utils');
const iutils = require('../../../../utils/iutils');
const model = require('../../../../models/interface');
const clog = require('../../../../loggers/console_logger');
const treasuryHandler = require('../../../../services/treasury/treasury-handler');
const blockUtils = require('../../../block-utils');
const iNameBindInRelatedBlock = require('../../../documents-in-related-block/inames/ipgp-binds');

class FleNSBindingCostsHandler {

    static importBindingCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        // handle iName register costs

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_INAME_BIND')) {
            clog.trx.info(`importing FleNS-bind payments ${utils.stringify(aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_BIND'])}`);

            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_INAME_BIND']) {
                // if proposal costs was payed by Pledging & a lending transaction
                let docCostIsPayed = true;

                let bindHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let bindDoc = blockUtils.pickDocByHash(block, bindHash).doc;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[bindHash] = `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for iName-bind is rejected because of doublespending`;
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The iName-bind costs is not supported by any trx';
                }



                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_BIND)`);

                    costPaymentStatus[bindHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_INAME_BIND',
                        title: `TP_INAME_BIND iName(${utils.hash6c(bindHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        descriptions: `TP_INAME_BIND iname(${utils.hash6c(bindHash)}) Trx(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) `,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_INAME_BIND)`);    

                    clog.app.error(`costPaymentStatus not payed ${utils.stringify(costPaymentStatus)}`);
                    iNameBindInRelatedBlock.removeBindingBecauseOfPaymentsFail(bindHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = FleNSBindingCostsHandler;
