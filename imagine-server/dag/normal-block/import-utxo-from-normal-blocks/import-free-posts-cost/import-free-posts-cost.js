const _ = require('lodash');
const iConsts = require('../../../../config/constants');
const utils = require('../../../../utils/utils');
const iutils = require('../../../../utils/iutils');
const model = require('../../../../models/interface');
const clog = require('../../../../loggers/console_logger');
const treasuryHandler = require('../../../../services/treasury/treasury-handler');
const fDocsInRelatedBlock = require('../../../documents-in-related-block/free-docs/free-docs');
const blockUtils = require('../../../block-utils');


class FreePostCostHandler {

    static importFDocsCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;

        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_FDOC')) {
            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_FDOC']) {
                // if free-doc costs was payed by a valid trx
                let docCostIsPayed = true;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = {
                        msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for free-doc is rejected because of doublespending`
                    };
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The free-doc costs is not supported by any trx';
                }
                let docHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let theDoc = blockUtils.pickDocByHash(block, docHash).doc;

                let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[docHash];
                let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
                if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[docHash] = 'supporter transaction is rejected because of doublespending';
                }

                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_FDOC)`);

                    costPaymentStatus[docHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_FDOC',
                        title: `TP_FDOC fDoc(${utils.hash6c(docHash)})`,
                        descriptions: `TP_FDOC fDoc(${utils.hash6c(docHash)})`,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_FDOC)`);    

                    fDocsInRelatedBlock.removeFDoc(docHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = FreePostCostHandler;

