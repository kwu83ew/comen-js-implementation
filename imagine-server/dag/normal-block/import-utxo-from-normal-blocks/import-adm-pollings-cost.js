const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const blockUtils = require('../../block-utils');


class PollingCostHandler {

    static importAdmPollingsCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;


        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_ADM_POLLING')) {
            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_ADM_POLLING']) {
                // if polling costs was payed by a valid trx
                let docCostIsPayed = true;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for Adm Polling is rejected because of doublespending` };
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The Adm Polling costs is not supported by any trx';
                }
                let supportedDocHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let supportedDoc = blockUtils.pickDocByHash(block, supportedDocHash).doc;

                if (supportedDoc.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
                    docCostIsPayed = false;
                    costPaymentStatus[supportedDocHash] = `admPolling dClass(${supportedDoc.dClass}) is not supported`;
                }

                let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[supportedDocHash];
                let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
                if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[supportedDocHash] = 'supporter transaction is rejected because of doublespending';
                }


                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_ADM_POLLING)`);

                    costPaymentStatus[supportedDocHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_ADM_POLLING',
                        title: `TP_ADM_POLLING Adm Polling(${utils.hash6c(supportedDocHash)})`,
                        descriptions: `TP_ADM_POLLING Adm Polling(${utils.hash6c(supportedDocHash)})`,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_ADM_POLLING)`);

                    const admPollingsInRelatedBlock = require('../../documents-in-related-block/administrative-polling/administrative-polling');
                    admPollingsInRelatedBlock.removeAdmPolling({ docHash: supportedDocHash });

                    const pollHandler = require('../../../services/polling-handler/general-poll-handler');
                    pollHandler.removePollingByRelatedAdmPolling(supportedDocHash);

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = PollingCostHandler;
