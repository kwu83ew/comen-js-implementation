const _ = require('lodash');
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');
const treasuryHandler = require('../../../services/treasury/treasury-handler');
const blockUtils = require('../../block-utils');


class PollingCostHandler {

    static importPollingsCost(args) {
        let block = args.block;
        let aBlockAnaRes = args.aBlockAnaRes;


        let costPaymentStatus = {};

        if (_.has(aBlockAnaRes.blockAlterTreasuryIncomes, 'TP_POLLING')) {
            for (let anAlterTPOutput of aBlockAnaRes.blockAlterTreasuryIncomes['TP_POLLING']) {
                // if polling costs was payed by a valid trx
                let docCostIsPayed = true;

                if (_.has(aBlockAnaRes.rejectedTransactions, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = { msg: `supporter transaction(${utils.hash6c(anAlterTPOutput.TPTrxHash)}) for Polling is rejected because of doublespending` };
                }

                if (!_.has(aBlockAnaRes.mapUTrxHashToTrxRef, anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[anAlterTPOutput.TPTrxHash] = 'The Polling costs is not supported by any trx';
                }
                let pollingHash = aBlockAnaRes.mapUTrxHashToTrxRef[anAlterTPOutput.TPTrxHash];
                let pollingDoc = blockUtils.pickDocByHash(block, pollingHash).doc;

                if (pollingDoc.dClass != iConsts.POLLING_PROFILE_CLASSES.Basic.ppName) {
                    docCostIsPayed = false;
                    costPaymentStatus[pollingHash] = `Polling dClass(${pollingDoc.dClass}) is not supported`;
                }

                let supporterTrx = aBlockAnaRes.mapUTrxRefToTrxHash[pollingHash];
                let rejs = utils.objKeys(aBlockAnaRes.rejectedTransactions);
                if (rejs.includes(supporterTrx) || rejs.includes(anAlterTPOutput.TPTrxHash)) {
                    docCostIsPayed = false;
                    costPaymentStatus[pollingHash] = 'supporter transaction is rejected because of doublespending';
                }


                if (docCostIsPayed) {
                    clog.app.info(`Successfully tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_POLLING)`);    

                    costPaymentStatus[pollingHash] = 'Done';
                    treasuryHandler.insertIncome({
                        cat: 'TP_POLLING',
                        title: `TP_POLLING Polling(${utils.hash6c(pollingHash)})`,
                        descriptions: `TP_POLLING Polling(${utils.hash6c(pollingHash)})`,
                        creationDate: block.creationDate,
                        blockHash: block.blockHash,
                        refLoc: anAlterTPOutput.TPRefLoc,
                        value: anAlterTPOutput.TPValue
                    });

                } else {
                    clog.sec.error(`Failed tp_... Block(${utils.hash6c(block.blockHash)}) Coin(${iutils.shortCoinRef(anAlterTPOutput.TPRefLoc)}) importing(TP_POLLING)`);    

                    const pollingsInRelatedBlock = require('../../documents-in-related-block/pollings/pollings');
                    pollingsInRelatedBlock.removePollingInRelBlock({ pollingHash });

                    // remove referenced & related doc for which there is a polling
                    switch (pollingDoc.refType) {

                        case iConsts.DOC_TYPES.ReqForRelRes:
                            const reqRelsInRelatedBlock = require('../../documents-in-related-block/reqrelres/reqrelres');
                            reqRelsInRelatedBlock.removeReqRelRes({ reqRelHash: pollingDoc.ref});
                            break;
                    }

                }
            }
        }
        return costPaymentStatus;
    }
}

module.exports = PollingCostHandler;
