// const _ = require('lodash');
// const iConsts = require('../../config/constants');
// const clog = require('../../loggers/console_logger');
// const utils = require("../../utils/utils");
// const iutils = require("../../utils/iutils");
// const model = require("../../models/interface");
// const blockHashHandler = require("../hashable");
// const blockTplHandler = require("../../docs-structure/block/block-tpl-handler");
// const bExtInfoHandler = require('../extinfos-handler');
// const dagHandler = require("../dag-handler");
// const DNAHandler = require("../../dna/dna-handler");
// const utxoHandler = require("../../transaction/utxo/utxo-handler");
// const sendingQ = require("../../services/sending-q-manager/sending-q-manager");
// const susVoteHandler = require("./sus-vote-handler");


// // const table = 'i_trx_donated_outputs';

// class SusBlockHandler {

//     static handleReceivedSusBlock(args) {
//         let msg;
//         let remoteSusBlock = args.payload;
//         clog.app.info(`******** handle Received sus Block(${utils.hash6c(remoteSusBlock.blockHash)})`);
//         console.log('SusBlockHandler.handleReceivedBlock', remoteSusBlock);

//         // try to regenerate duplicated-normal block from bBlock,
//         // then validate normal block and probably regenerate sus Block (and it's blockHash) in local
//         // at the end remoteSusBlock.blockHash must be equal localBBlock.blockHash

//         // try to regenerate duplicated-normal block from bBlock
//         let normalBlock = this.convertSusBlockToNormalBlock(remoteSusBlock);
//         const normalBlockHandler = require("./validate-normal-block");
//         let validateRes = normalBlockHandler.validateNormalBlock(normalBlock);
//         clog.app.info(`Received a regenerated sus block, validation result: ${JSON.stringify(validateRes)}`);
//         if (validateRes.err != false) {
//             // maybe do something
//             return validateRes;
//         }

//         let localBBlock;
//         if (!_.has(validateRes, 'susBlock'))
//             localBBlock = null;

//         if (_.has(validateRes, 'susBlock') && (validateRes.susBlock == null))
//             localBBlock = null;

//         localBBlock = validateRes.susBlock;
//         if (localBBlock == null) {
//             msg = `it supposed to local machine creates sus block for given remot susBlock(${utils.hash6c(remoteSusBlock.blockHash)}) where dosn't happend!`
//             clog.app.error(msg);
//             return { err: true, msg, shouldPurgeMessage: true };
//         }

//         if (localBBlock.blockHash != remoteSusBlock.blockHash) {
//             msg = `The locally created sus block(${utils.hash6c(localBBlock.blockHash)}) isn's same as remote(${utils.hash6c(remoteSusBlock.blockHash)}) one`
//             clog.app.error(msg);
//             clog.app.error(`remoteSusBlock: ${JSON.stringify(remoteSusBlock)}`);
//             clog.app.error(`localBBlock: ${JSON.stringify(localBBlock)}`);
//             return { err: true, msg, shouldPurgeMessage: true };
//         }


//         let confidence = DNAHandler.getAnAddressShares(remoteSusBlock.backe rAddress, remoteSusBlock.creationDate).percentage;

//         // add block to DAG(refresh leave blocks, remove from utxos, add to utxo12...)
//         // insert the block in i_blocks as a confirmed block
//         dagHandler.addBlockToDAG({ block: remoteSusBlock, confidence });
//         dagHandler.postAddBlockToDAG({ block: remoteSusBlock });

//         // remove used UTXOs
//         utxoHandler.remove UsedOutputsByBlock(remoteSusBlock);
//         // log spend TxOs

//         if (dagHandler.isDAGUptodated()) {
//             clog.app.info(`re-Broadcasting sus block$(${utils.hash6c(remoteSusBlock.blockHash)})`);
//             let pushBBRes = sendingQ.pushIntoSendingQ({
//                 sq_type: remoteSusBlock.bType,
//                 sq_code: utils.hash6c(remoteSusBlock.blockHash),
//                 payload: JSON.stringify(remoteSusBlock),
//                 title: `re-Broadcasting bBlock$(${utils.hash6c(remoteSusBlock.blockHash)})`,
//             })
//             clog.app.info(`sB pushRes: ${JSON.stringify(pushBBRes)}`);

//             // sign my own vote and broadcast susVote to neighbors
//             let susVote = susVoteHandler.create SusVote(remoteSusBlock, validateRes.doubleSpends);
//             let pushBVRes = sendingQ.pushIntoSendingQ({
//                 sq_type: susVote.bType,
//                 sq_code: utils.hash6c(susVote.blockHash),
//                 payload: JSON.stringify(susVote),
//                 title: `Broadcasting susVote block$(${utils.hash6c(remoteSusBlock.blockHash)})`,
//             })
//             clog.app.info(`susVote pushRes: ${JSON.stringify(pushBVRes)}`);
//         }

//         return { err: false, shouldPurgeMessage: true };
//     }


//     // static search(args) {
//     //     let query = [];
//     //     if (_.has(args, 'refLocs'))
//     //         query.push(['ref_loc', ['IN', args.refLocs]]);

//     //     let res = model.sRead({
//     //         table,
//     //         query
//     //     });

//     //     return res;
//     // }

//     static createSusBlock(blockOrg) {
//         let block = _.clone(blockOrg)
//         clog.trx.info(`--- creating sus-block for double-spend block(${utils.hash6c(blockOrg.blockHash)})`);
//         console.log(`--- creating sus-block for double-spend block(${utils.hash6c(blockOrg.blockHash)})`);
//         block.bType = iConsts.BLOCK_TYPES.SusBlock

//         block.normalBlockHash = _.cloneDeep(blockOrg.blockHash);
//         block.normalBlockLength = _.cloneDeep(blockOrg.blockLength);

//         block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);
//         block.blockHash = blockHashHandler.hashBlock(block);
        
//         clog.trx.info(`--------------------------------- block(${utils.hash6c(block.normalBlockHash)}) converted to SusBlockblock(${utils.hash6c(block.blockHash)}) ---------------------------------`);
//         console.log(`--------------------------------- block(${utils.hash6c(block.normalBlockHash)}) converted to SusBlockblock(${utils.hash6c(block.blockHash)}) ---------------------------------`);
//         clog.trx.info(`new SusBlock(${utils.hash6c(block.blockHash)}) \n\n${JSON.stringify(block)}\n\n`);
//         // bBlock.dblSpendReferences = bBlock.dblSpendReferences.sort(); // will be Sus for one year
//         // bBlock.abrogatedTransactions = bBlock.abrogatedTransactions.sort();   // will be cancelled
//         return block;
//     }


//     static convertSusBlockToNormalBlock(bBlock) {
//         let block = _.cloneDeep(bBlock);
//         block.bType = iConsts.BLOCK_TYPES.Normal;
//         block.blockHash = block.normalBlockHash;
//         delete block.normalBlockHash;
//         block.blockLength = block.normalBlockLength;
//         delete block.normalBlockLength;
//         return block;
//     }

//     // static cloneDblBlockToASusBlock(dblBlock) {
//     //     let bBlock = blockTplHandler.getSusBlockTemplate();
//     //     bBlock.net = dblBlock.net;
//     //     bBlock.bVer = dblBlock.bVer;
//     //     bBlock.descriptions = dblBlock.descriptions;
//     //     bBlock.ancestors = dblBlock.ancestors;
//     //     bBlock.descendents = dblBlock.descendents;
//     //     bBlock.signals = dblBlock.signals;
//     //     bBlock.backer = dblBlock.backer;
//     //     bBlock.creationDate = dblBlock.creationDate;
//     //     bBlock.bExtInfo = dblBlock.bExtInfo;
//     //     bBlock.bExtHash = dblBlock.bExtHash;
//     //     bBlock.clonedTransactionsRootHash = dblBlock.clonedTransactionsRootHash;
//     //     bBlock.clonedTransactions = dblBlock.clonedTransactions;
//     //     bBlock.docsRootHash = dblBlock.docsRootHash;
//     //     bBlock.docs = dblBlock.docs;
//     //     bBlock.dblBlockHash = dblBlock.blockHash;
//     //     bBlock.dblBlockLength = dblBlock.blockLength;
//     //     return bBlock;
//     // }
// }



// module.exports = SusBlockHandler;
