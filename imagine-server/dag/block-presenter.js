const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require("../utils/utils");
// const transactionPresenter = require("../transaction-presenter").transactionPresenter
const blockUtils = require('../dag/block-utils');

function blockPresenter(block) {
    let out = '\n';
    out += '\nNetwork: ' + block.net;
    out += '\nBlock Hash: ' + block.blockHash;
    out += '\n\tBlock Type: ' + block.bType + ' \tVersion: ' + block.version;
    out += '\n\tLength: ' + block.blockLength;

    let creation = block.creationDate
    if ((_.has, block, 'cycle') && !utils._nilEmptyFalse(block.cycle))
        creation += ' ' + block.cycle
    out += '\n\tCreation Date: ' + creation;

    // if (_.has(block, 'receiveDate') && !utils._nilEmptyFalse(block.receiveDate))
    //     out += '\n\tReceive Date: ' + block.receiveDate;


    if (_.has(block, 'ancestors') && !utils._nilEmptyFalse(block.ancestors))
        out += '\n\tAncestors: ' + block.ancestors;

    if (_.has(block, 'descendents') && !utils._nilEmptyFalse(block.descendents))
        out += '\n\tDescendents: ' + block.descendents;

    if (_.has(block, 'signals') && !utils._nilEmptyFalse(block.signals))
        out += '\n\tSignals: ' + block.signals;

    if (block.bType == iConsts.BLOCK_TYPES.Coinbase) {
        out += '\n\tMinting Cycle: ' + block.docs[0].cycle + ' => ' + utils.microPAIToPAI(block.docs[0].mintedCoins);
        out += '\n\tTrreasury Cycle: ' + [block.docs[0].treasuryFrom, block.docs[0].treasuryTo].join(' - ') + ' => ' + utils.microPAIToPAI(block.docs[0].treasuryIncomes);

    }

    out += '\n\n\tDocuments: ' + ((!utils._nilEmptyFalse(block.docs)) ? block.docs.length : "No Docs");
    out += '\n\tDoc Root: ' + ((!utils._nilEmptyFalse(block.docsRootHash)) ? block.docsRootHash : "No Docs");
    if (Array.isArray(block.docs))
        block.docs.forEach(doc => {
            out += '\n\t' + blockUtils.docPresenter(doc, '\t\t');

        });


    if (_.has(block, 'bExtInfo')) {
        out += `\n\tExtInfos (Block): ${block.bExtInfo.length}`;
        for (let i = 0; i < block.bExtInfo.length; i++) {
            let aTrxSeg = block.bExtInfo[i];
            for (let j = 0; j < aTrxSeg.length; j++) {
                let aSeg = aTrxSeg[j];

                // out += '>>>>>>>>>>>>>>>>>>>>>>> ' + JSON.stringify(aSeg);
                // continue
                // out += `\n\n\t Trx(${i}) Signers: ${aSeguSets.sSets.length}`;
                // aSeguSets.sSets.forEach(aSignSet => {
                //     out += `\n\t\tPublicKey + inputTimeLock: ${aSignSet.sKey} + ${aSignSet.iTLock}`;
                // });
                // out += `\n\tProofs: ${aSeguSets.proofs.length}`;
                // aSeguSets.proofs.forEach(aProof => {
                //     out += `\n\t\t${aProof}`;
                // });
                // out += `\n\tLeft Hash: \n\t\t${aSeguSets.lHash}`;
                // out += `\n\tSignatures: ${aSeg.signatures.length}`;
                // aSeg.signatures.forEach(aSig => {
                //     out += `\n\t\t${aSig}`;
                // });
            }

        }
    }
    out += `\n`;


    return out;
}

module.exports = blockPresenter;