const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const crypto = require('../crypto/crypto');
const merkleHandler = require('../crypto/merkle/merkle');
const clog = require('../loggers/console_logger');

class HashableHandler {

    static hashBlock(block) {
        let hashableBlock = this.extractBlockHashableParts(block);
        return iutils.doHashObject(hashableBlock);
    }

    static extractBlockHashableParts(block) {
        let hashableBlock;
        switch (block.bType) {

            // case iConsts.BLOCK_TYPES.Normal:
            //     // case iConsts.BLOCK_TYPES.SusBlock:
            //     hashableBlock = this.extractBlockHashableParts_normal(block);
            //     break;

            case iConsts.BLOCK_TYPES.FSign:
                hashableBlock = this.extractBlockHashableParts_floatingSignature(block);
                break;

            case iConsts.BLOCK_TYPES.FVote:
                hashableBlock = this.extractBlockHashableParts_floatingVote(block);
                break;

            case iConsts.BLOCK_TYPES.Coinbase:
                hashableBlock = this.extractBlockHashableParts_coinbase(block);
                break;

            case iConsts.BLOCK_TYPES.Genesis:
                hashableBlock = HashableHandler.extractBlockHashableParts_genesis(block);
                break;

            default:
                clog.app.error(`strange block(${utils.hash6c(block.blockHash)}) type(${block.bType}) exists!`);
                break;
        }
        return hashableBlock;
    }

    static extractBlockHashableParts_genesis(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            ancestors: block.ancestors,
            backer: block.backer,
            blockLength: block.blockLength,
            bType: block.bType,
            bVer: block.bVer,
            creationDate: block.creationDate,
            descriptions: block.descriptions,
            docsRootHash: block.docsRootHash, // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
            dExtHash: block.dExtHash, // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
            fVotes: block.fVotes,
            net: block.net,
            signals: block.signals,
        };
        return hashableBlock;
    }

    // static extractBlockHashableParts_normal(block) {
    //     // in order to have almost same hash! we sort the attribiutes alphabeticaly
    //     let hashableBlock = {
    //         ancestors: block.ancestors,
    //         backer: block.backer,
    //         blockLength: block.blockLength,
    //         // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash
    //         bType: block.bType,
    //         creationDate: block.creationDate,
    //         descriptions: block.descriptions,
    //         docsRootHash: block.docsRootHash, // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
    //         dExtHash: block.dExtHash, // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
    //         fVotes: block.fVotes,
    //         net: block.net,
    //         signals: block.signals,
    //         bVer: block.bVer,
    //     };
    //     return hashableBlock;
    // }

    // static extractBlockHashableParts_sus(block) {
    //     // in order to have almost same hash! we sort the attribiutes alphabeticaly
    //     let hashableBlock = {
    //         abrogatedTransactions: block.abrogatedTransactions,
    //         ancestors: block.ancestors,
    //         backer: block.backer,
    //         blockLength: block.blockLength,
    //         clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash
    //         creationDate: block.creationDate,
    //         dblBlockHash: block.dblBlockHash,
    //         dblBlockLength: block.dblBlockLength,
    //         dblSpendReferences: block.dblSpendReferences,
    //         descriptions: block.descriptions,
    //         docsRootHash: block.docsRootHash, // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
    //         dExtHash: block.dExtHash, // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
    //         net: block.net,
    //         signals: block.signals,
    //         bType: block.bType,
    //         bVer: block.bVer,
    //     };
    //     return hashableBlock;
    // }

    // static extractBlockHashableParts_floatingSignature(block) {
    //     // in order to have almost same hash! we sort the attribiutes alphabeticaly
    //     let hashableBlock = {
    //         ancestors: block.ancestors,
    //         backer: block.backer,
    //         bType: block.bType,
    //         confidence: block.confidence,
    //         creationDate: block.creationDate,
    //         cycle: block.cycle,
    //         bExtHash: block.bExtHash,
    //         net: block.net,
    //         bVer: block.bVer,
    //     };
    //     return hashableBlock;
    // }

    // static extractBlockHashableParts_floatingVote(block) {
    //     // in order to have almost same hash! we sort the attribiutes alphabeticaly
    //     let hashableBlock = {
    //         ancestors: block.ancestors,
    //         backer: block.backer,
    //         bType: block.bType,
    //         confidence: block.confidence,
    //         creationDate: block.creationDate,
    //         confidence: block.confidence,
    //         bExtHash: block.bExtHash,
    //         net: block.net,
    //         bVer: block.bVer,
    //     };
    //     return hashableBlock;
    // }

    static extractBlockHashableParts_coinbase(block) {
        // in order to have almost same hash! we sort the attribiutes alphabeticaly
        let hashableBlock = {
            ancestors: block.ancestors,
            bType: block.bType,
            bVer: block.bVer,
            confidence: block.confidence,
            creationDate: block.creationDate,
            cycle: block.cycle,
            net: block.net,
            docsRootHash: block.docsRootHash, // note that we do not put the transactions directly in block hash, instead using transactions-merkle-root-hash
        };
        return hashableBlock;
    }

    static calculateDocsRootHash(block) {
        let docs = [];
        if (block.docs === null) return "";
        if (block.docs.length === 0) return "";
        block.docs.forEach(doc => {
            docs.push(doc.hash);
        });
        let m = merkleHandler.merkle(docs);
        return m.root;
    }
}

module.exports = HashableHandler;