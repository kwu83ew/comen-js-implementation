const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const iutils = require('../../utils/iutils')
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const DNAHandler = require('../../dna/dna-handler');


const table = 'i_blocks';

class WalkThroughDag {

    /**
     * returns latest block which is already recorded in DAG
     */
    static getLatestBlock() {
        let msg;
        let lastWBLock = WalkThroughDag._super.searchInDAGSync({
            fields: ['b_hash', 'b_creation_date'],
            order: [['b_creation_date', 'DESC']],
            limit: 1
        });
        if (lastWBLock.length == 0) {
            msg = `The DAG hasn't any node!`
            clog.sec.error(msg);
            return { err: true, msg }
        }
        let wBlock = lastWBLock[0];
        return {
            err: false,
            wBlock
        }
    }


    /**
     * 
     * @param {*} blockHashes 
     * @param {*} level 
     * this is not aggregative and returns ONLY given level's ancestors and not the blocks in backing track way
     */
    static getAncestors(blockHashes = null, level = 1) {
        if (utils._nil(blockHashes) || (Array.isArray(blockHashes) && (blockHashes.length == 0)))
            return [];

        let res = WalkThroughDag._super.searchInDAGSync({
            fields: ["b_ancestors"],
            query: [
                ['b_hash', ['IN', blockHashes]]
            ],
            log: false
        });
        if (res.length == 0)
            return [];

        let out = [];
        for (let aRes of res) [
            out = utils.arrayAdd(out, utils.parse(aRes.bAncestors))
        ]
        out = utils.arrayUnique(out);

        if (level == 1)
            return out;

        return WalkThroughDag.getAncestors(out, level - 1);
    }

    /**
 * 
 * @param {*} blockHash 
 * returns all descendents of block(include the block also)
 */
    static getAllDescendents(args) {
        let blockHash = args.blockHash;
        let retrieveValidityPercentage = _.has(args, 'retrieveValidityPercentage') ? args.retrieveValidityPercentage : false;
        let decends = [blockHash];
        let visibleBys = decends;
        let i = 0;
        clog.trx.info(`NB visibleBys ${i++}: ${visibleBys.map(x => utils.hash6c(x))}`);
        while (decends.length > 0) {
            decends = WalkThroughDag.getDescendents({ blockHashes: decends, level: 1 });
            visibleBys = utils.arrayUnique(utils.arrayAdd(visibleBys, decends));
            clog.trx.info(`NB visibleBys ${i++}: ${visibleBys.map(x => utils.hash6c(x))}`);
        }
        // exclude floating signature blocks
        let fields = ['b_hash', 'b_cycle', 'b_creation_date'];
        if (retrieveValidityPercentage)
            fields.push('b_backer');
        let wBlocksEFS = WalkThroughDag._super.excludeFloatingBlocks({ blockHashes: visibleBys, fields });
        let validityPercentage = 0.0;
        if (retrieveValidityPercentage) {
            let backerOnDateSharePercentage = {}
            for (let aBlock of wBlocksEFS) {
                if (validityPercentage > 100.0)
                    break;

                if (!_.has(backerOnDateSharePercentage, aBlock.bBacker))
                    backerOnDateSharePercentage[aBlock.bBacker] = {}
                if (!_.has(backerOnDateSharePercentage[aBlock.bBacker], aBlock.bCreationDate)) {
                    let percentage = DNAHandler.getAnAddressShares(aBlock.bBacker, aBlock.bCreationDate).percentage;
                    backerOnDateSharePercentage[aBlock.bBacker][aBlock.bCreationDate] = percentage;
                    validityPercentage += percentage
                } else {
                    validityPercentage += backerOnDateSharePercentage[aBlock.bBacker][aBlock.bCreationDate]
                }
                console.log(`backer/Date/percentage, validityPercentage: `,
                    aBlock.bBacker,
                    aBlock.bCreationDate,
                    backerOnDateSharePercentage[aBlock.bBacker][aBlock.bCreationDate],
                    validityPercentage
                );
            }
        }
        clog.trx.info(`visibleBys after exclude floating signature blocks(NB): ${wBlocksEFS.map(x => utils.hash6c(x.bHash))}`);
        if (validityPercentage > 100)
            validityPercentage = 100.0;
        return { err: false, wBlocks: wBlocksEFS, validityPercentage };
    }

    static getDescendents(args) {
        let blockHashes = args.blockHashes;
        let level = _.has(args, 'level') ? args.level : 1;

        let msg;
        if (utils._nil(blockHashes) || (Array.isArray(blockHashes) && (blockHashes.length == 0)))
            return [];

        clog.app.info(`get Descendents of blocks(${blockHashes.map(x => utils.hash6c(x))})`);
        // 1. retrieve descendent blocks from DAG by descendents property
        // 2. if step one hasn's answer tries to find descendents by ancestors link of blocks

        // 1. retrieve descendent blocks from DAG by descendents property
        let wBlocks = WalkThroughDag._super.searchInDAGSync({
            fields: ['b_hash', 'b_descendents'],
            query: [['b_hash', ['IN', blockHashes]]]
        });
        if (wBlocks.length == 0) {
            // TODO: the block(s) is valid and does not exist in local. or
            // invalid block invoked, maybe some penal for sender!
            msg = `The blocks(${blockHashes.map(x => utils.hash6c(x)).join()}) looking descendents does not exist in local`;
            clog.app.info(msg);
            return [];
        }
        let descendents = [];
        for (let wBlock of wBlocks) {
            let descendentWasFound = false;
            if (!utils._nilEmptyFalse(wBlock.bDescendents)) {
                let desc = utils.parse(wBlock.bDescendents);
                if (Array.isArray(desc) && (desc.length > 0)) {
                    descendentWasFound = true;
                    descendents = utils.arrayAdd(descendents, desc);
                }
            }
            if (!descendentWasFound) {
                let desc = WalkThroughDag.findDescendetsBlockByAncestors(wBlock.bHash);
                descendents = utils.arrayAdd(descendents, desc);
            }
        };
        descendents = utils.arrayUnique(descendents);

        if (level == 1)
            return descendents;

        return WalkThroughDag.getDescendents({ blockHashes: descendents, level: level - 1 });
    }

    /**
     * 
     * @param {*} blockHash 
     * returns the first generation descentents of a given block, by finding all blocks in DAG which have the given block as a ancestor
     */
    static findDescendetsBlockByAncestors(blockHash) {
        clog.app.info(`find DescendetsBlockByAncestors lokking for desc of block(${utils.hash6c(blockHash)})`);
        let res = WalkThroughDag._super.searchInDAGSync({
            fields: ['b_ancestors', 'b_hash'],
            query: [
                ['b_ancestors', ['LIKE', `%${blockHash}%`]]
            ],
            log: false
        });
        if (res.length == 0)
            return [];
        res = res.map(x => x.bHash);
        return res;
    }

    /**
     * 
     * @param {*} args 
     * returns all ancestors of given block(s), which are youngre than a certain age (byMinutes or cycle)
     */
    static returnAncestorsYoungerThan(args) {
        clog.trx.info(`return AncestorsYoungerThan args ${utils.stringify(args)}`);
        let cycle = _.has(args, 'cycle') ? args.cycle : null;
        let byDate = _.has(args, 'byDate') ? args.byDate : null;
        let byMinutes = _.has(args, 'byMinutes') ? args.byMinutes : null;
        let blockHashes = _.has(args, 'blockHashes') ? args.blockHashes : [];
        if (utils._nil(blockHashes) || (Array.isArray(blockHashes) && (blockHashes.length == 0)))
            return [];

        let _date;
        if (byDate != null) {
            _date = byDate;
        } else if (byMinutes != null) {
            _date = utils.minutesBefore(byMinutes);
        } else if (cycle != null) {
            _date = utils.minutesBefore(iConsts.getCycleByMinutes() * cycle);
        }

        WalkThroughDag.ancestors = [];
        WalkThroughDag.recursive_backwardInTime({
            blockHashes,
            _date
        });

        return WalkThroughDag.ancestors;
    }

    static recursive_backwardInTime(args) {
        // console.log(`::::::::::: recursive_backwardInTime args: ${utils.stringify(args)}`);
        let blockHashes = _.has(args, 'blockHashes') ? args.blockHashes : [];
        let _date = args._date;
        if (utils._nil(blockHashes) || (Array.isArray(blockHashes) && (blockHashes.length == 0)))
            return [];

        let res = WalkThroughDag._super.searchInDAGSync({
            fields: ["b_ancestors"],
            query: [
                ['b_hash', ['IN', blockHashes]],
                ['b_creation_date', ['>', _date]],
            ],
            // log: false
        });
        if (res.length == 0)
            return [];

        let out = [];
        for (let aRes of res) [
            out = utils.arrayAdd(out, utils.parse(aRes.bAncestors))
        ]
        out = utils.arrayUnique(out);
        WalkThroughDag.ancestors = utils.arrayAdd(WalkThroughDag.ancestors, out);
        // console.log('out----------', out);

        if (out.length > 0)
            WalkThroughDag.recursive_backwardInTime({
                blockHashes: out,
                _date
            });
    }


}

module.exports = WalkThroughDag;
