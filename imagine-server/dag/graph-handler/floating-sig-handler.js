const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const iutils = require('../../utils/iutils')
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');


const table = 'i_blocks';



class FloatingSignaturesHandler {
    /**
    * 
    * @param {time} cDate
    * the functions accepts a time and searchs for all floating signatures which are signed the prev coinbase block(either linked or not linked blocks) 
    */
    static aggrigateFloatingSignatures(cDate = null) {
        cDate = (utils._nilEmptyFalse(cDate)) ? utils.getNow() : cDate;

        let msg = '';
        let res = {
            err: false,
            msg
        }

        // retrieve prev cycle info
        let initCoinbaseRange = iutils.getCoinbaseRange(iConsts.getLaunchDate());
        if (utils.getNow() > initCoinbaseRange.to) {
            let prevCoinbaseInfo = iutils.getPrevCoinbaseInfo(cDate);
            clog.cb.info(`rerieve prevCoinbaseInfo ${cDate}=>${JSON.stringify(prevCoinbaseInfo)} `)

            // retrieve prev cycle coinbases
            let prvCoinbaseBlocks = FloatingSignaturesHandler._super.searchInDAGSync({
                fields: ['b_hash'],
                query: [
                    ['b_type', iConsts.BLOCK_TYPES.Coinbase],
                    ['b_cycle', prevCoinbaseInfo.cycleStamp],
                    ['b_creation_date', ['>=', prevCoinbaseInfo.from]]
                ],
                order: [
                    ['b_confidence', 'DESC'],
                    ['b_hash', 'ASC']
                ]
            });
            clog.cb.info('argsssssssssssssssss11111111111111');
            clog.cb.info(`prvCoinbaseBlocks: ${utils.stringify(prvCoinbaseBlocks)}`);
            prvCoinbaseBlocks = prvCoinbaseBlocks.map(x => x.bHash);

            // retrieve all floating signature blocks which are created in prev cycle
            clog.cb.info(`rerieve floating signatures for cycle ${prevCoinbaseInfo.cycleStamp} from ${prevCoinbaseInfo.from}`)
            let args = {
                fields: ['b_hash', 'b_ancestors', 'b_confidence', 'b_backer'],
                query: [
                    ['b_type', iConsts.BLOCK_TYPES.FSign],
                    ['b_cycle', prevCoinbaseInfo.cycleStamp],
                    ['b_creation_date', ['>=', prevCoinbaseInfo.from]], // TODO add a max Date to reduce results

                ],
                order: [
                    ['b_confidence', 'DESC'],
                    ['b_hash', 'ASC']
                ]
            }
            // if (prvCoinbaseBlocks.length > 0)
            //     args.query.push(['b_ancestors', ['IN', prvCoinbaseBlocks]]);

            // clog.cb.info('argsssssssssssssssss');
            // clog.cb.info('argsssssssssssssssss');
            // clog.cb.info(`argsssssssssssssssss: ${utils.stringify(args)}`);
            let fSWBlocks = FloatingSignaturesHandler._super.searchInDAGSync(args);
            // clog.cb.info(`fSWBlocks vvv ${utils.stringify(fSWBlocks)}`);
            // console.log(`fSWBlocks vvv ${utils.stringify(fSWBlocks)}`);
            // clog.cb.info('argsssssssssssssssssllllllllllll');
            let blockHashes = []
            let backers = {}
            for (let fSWBlock of fSWBlocks) {

                // drop float if it is not linked to proper coinbase block
                let isLinkedToPropoerCB = false;
                let tmpAncs = utils.parse(fSWBlock.bAncestors);
                for (let anAnc of tmpAncs) {
                    if (prvCoinbaseBlocks.includes(anAnc))
                        isLinkedToPropoerCB = true
                }
                if (!isLinkedToPropoerCB)
                    continue;

                backers[fSWBlock.bBacker] = fSWBlock.bConfidence
                blockHashes.push(fSWBlock.bHash)
            }
            let confidence = 0.0;
            for (let bckr of utils.objKeys(backers)) {
                confidence += parseFloat(backers[bckr]);
            }
            confidence = utils.iFloorFloat(confidence);

            return {
                confidence,
                blockHashes,
                backers: utils.objKeys(backers)
            }

        } else {
            // machine is in init cycle, so there is no floating signture
            let genesis = FloatingSignaturesHandler._super.searchInDAGSync({
                query: [
                    ['b_type', iConsts.BLOCK_TYPES.Genesis],
                ]
            });

            return {
                confidence: 100.00,
                blockHashes: [genesis[0].bHash] // only the genesis block hash
            }
        }

    }

}

module.exports = FloatingSignaturesHandler;
