const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const iutils = require('../../utils/iutils')
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const listener = require('../../plugin-handler/plugin-handler');
const blockUtils = require('../block-utils');


const table = 'i_blocks';

class WatcherDAG{

    static getNotImportedNormalBlock() {
        let wBlocks = WatcherDAG._super.searchInDAGSync({
            query: [
                ['b_utxo_imported', iConsts.CONSTS.NO],
                ['b_type', ['IN', [iConsts.BLOCK_TYPES.Normal]]]
            ]
        });
        let sum = 0;
        let maybeDblSpends = {};
        let processedOutputs = [];
        for (let wBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;

            for (let doc of block.docs) {
                if (![iConsts.DOC_TYPES.BasicTx].includes(doc.dType))
                    continue;

                // since DPCostPay docs alredy are in transactions , so we do not calculate it 2 times

                // analyze inputs
                for (let input of doc.inputs) {
                    let refLocc = iutils.packCoinRef(input[0], input[1])
                    if (!_.has(maybeDblSpends, refLocc)) {
                        maybeDblSpends[refLocc] = 0
                    }
                    maybeDblSpends[refLocc] += 1;
                }

                // analyze outputs
                // console.log(doc);

                for (let outputInx = 0; outputInx < doc.outputs.length; outputInx++) {
                    let output = doc.outputs[outputInx];
                    if (_.has(doc, 'dPIs')) {
                        sum += output[1];
                        // cutting DPCosts to prevent double counting
                        if (doc.dPIs.includes(outputInx)) {
                            processedOutputs.push([
                                block.creationDate.split(' ')[1],
                                utils.hash6c(doc.hash),
                                'DPCost',
                                utils.microPAIToPAI(output[1])
                            ]);
                        } else {


                            if (iConsts.TREASURY_PAYMENTS.includes(output[0])) {
                                processedOutputs.push([
                                    block.creationDate.split(' ')[1],
                                    utils.hash6c(doc.hash),
                                    output[0],
                                    utils.microPAIToPAI(output[1])
                                ]);
                            } else {
                                processedOutputs.push([
                                    block.creationDate.split(' ')[1],
                                    utils.hash6c(doc.hash),
                                    iutils.shortBech(output[0]),
                                    utils.microPAIToPAI(output[1])
                                ]);
                            }
                        }
                    } else {
                        // must not come in this part!
                    }
                }
            }
        }
        let dblSpends = []
        for (let ref of utils.objKeys(maybeDblSpends)) {
            if (maybeDblSpends[ref] > 1) {
                dblSpends.push(`${iutils.shortCoinRef(ref)}->${maybeDblSpends[ref]}`)
            }
        }
        processedOutputs = processedOutputs.map(x => x.join('\t   ')).join('\n');

        return { sum, dblSpends, processedOutputs };
    }

    static getNotImportedCoinbaseBlocks() {
        let wBlocks = WatcherDAG._super.searchInDAGSync({
            query: [
                ['b_utxo_imported', iConsts.CONSTS.NO],
                ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]],
                ['b_type', ['IN', [iConsts.BLOCK_TYPES.Coinbase]]],
            ]
        });
        let sum = 0;
        let maybeDblSpends = {};
        let processedOutputs = [];
        let calculatedCoinbase = [];
        for (let wBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;

            if (calculatedCoinbase.includes(block.cycle))
                continue;

            if (block.bType == iConsts.BLOCK_TYPES.Coinbase)
                calculatedCoinbase.push(block.cycle);

            // analyze outputs
            let doc = block.docs[0];
            for (let outputInx = 0; outputInx < doc.outputs.length; outputInx++) {
                let output = doc.outputs[outputInx];

                sum += output[1];
                // console.log(`output: ${output[1]}`);

                processedOutputs.push([
                    wBlock.bType,
                    block.creationDate.split(' ')[1],
                    utils.hash6c(doc.hash),
                    iutils.shortBech(output[0]),
                    utils.microPAIToPAI(output[1])
                ]);
            }
        }
        let dblSpends = []
        for (let ref of utils.objKeys(maybeDblSpends)) {
            if (maybeDblSpends[ref] > 1) {
                dblSpends.push(`${iutils.shortCoinRef(ref)}->${maybeDblSpends[ref]}`)
            }
        }
        processedOutputs = processedOutputs.map(x => x.join('\t   ')).join('\n');
        let coinbaseValue = (calculatedCoinbase.length * iutils.calcPotentialMicroPaiPerOneCycle(utils.getNow().split('-')[0]))

        return { sum, dblSpends, processedOutputs, coinbaseValue };
    }


   


    static analyzeDAGHealth(args = {}) {
        let shouldUpdateDescendants = _.has(args, 'shouldUpdateDescendants') ? args.shouldUpdateDescendants : false;
        // check if there are abb,andoned leaves
        let wBlocks = WatcherDAG._super.searchInDAGSync({
            fields: ['b_hash', 'b_type', 'b_ancestors', 'b_descendents', 'b_creation_date'],
            order: [
                ['b_creation_date', 'DESC']
            ]
        });
        let childByDad = {};
        let blocksInfo = {};
        for (let wBlock of wBlocks) {
            blocksInfo[utils.hash6c(wBlock.bHash)] = {
                bType: wBlock.bType,
                bCreationDate: wBlock.bCreationDate
            };
            let ancestors = utils.parse(wBlock.bAncestors);
            for (let dadHash of ancestors) {
                childByDad[utils.hash6c(dadHash)] = utils.hash6c(wBlock.bHash)
            };
        };
        // console.log(blocksInfo);
        let leaves = []
        let tmp = []
        utils.objKeys(blocksInfo).forEach(hash => {
            if (!_.has(childByDad, hash))
                tmp.push(hash)
        });

        tmp = tmp.sort()
        tmp.forEach(hash => {
            leaves.push({
                hash: '#' + hash,
                bType: blocksInfo[hash].bType,
                bCreationDate: blocksInfo[hash].bCreationDate.split(' ')[1]
            })
        });
        // console.log(leaves);
        return leaves;
    }


    static getCBBlocksStat() {
        let wBlocks = WatcherDAG._super.searchInDAGSync({
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase]
            ],
            order: [['b_creation_date', 'asc']]
        });
        let mintedMicroPAIs = 0;
        let outputsByBlock = {};

        let floorishMicroPAIs = 0;    //missedMicroPAIs
        let burnedByBlock = {}; //missedMicroPAIsBlocks

        let waitedCBToBeSpendable = 0;
        let consideredCycles = [];
        for (let wBlock of wBlocks) {
            if (consideredCycles.includes(wBlock.bCycle))
                continue;

            consideredCycles.push(wBlock.bCycle);
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            let blockOutputsSum = 0;
            if (_.has(block, 'docs') && utils.isArray(block.docs))
                for (let aDoc of block.docs) {
                    if (_.has(aDoc, 'outputs') && utils.isArray(aDoc.outputs))
                        for (let output of aDoc.outputs) {
                            blockOutputsSum += iutils.converStringToJSInt(output[1]);
                        }
                }
            let blockMissedPAIs = block.docs[0].treasuryIncomes + block.docs[0].mintedCoins - blockOutputsSum;

            mintedMicroPAIs += block.docs[0].mintedCoins;
            outputsByBlock[block.blockHash] = blockOutputsSum;

            floorishMicroPAIs += blockMissedPAIs
            if (blockMissedPAIs > 0)
                burnedByBlock[block.blockHash] = blockMissedPAIs;

            if (wBlock.bUtxoImported == iConsts.CONSTS.NO)
                waitedCBToBeSpendable += blockOutputsSum
        }

        return { floorishMicroPAIs, burnedByBlock, mintedMicroPAIs, outputsByBlock, waitedCBToBeSpendable };
    }

}

module.exports = WatcherDAG;
