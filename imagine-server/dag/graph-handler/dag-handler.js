const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const iutils = require('../../utils/iutils')
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const extinfosHandler = require('../extinfos-handler');
const listener = require('../../plugin-handler/plugin-handler');
const blockUtils = require('../block-utils');


const table = 'i_blocks';


class DAGHandler {

    static updateUtxoImported(blockHash, status) {
        clog.cb.info(`update Utxo is Imported ${utils.hash6c(blockHash)}=${status}`)
        model.sUpdate({
            table,
            query: [
                ['b_hash', blockHash]
            ],
            updates: { b_utxo_imported: status }
        });
    }

    static searchInDAGSync(args = {}) {
        args.table = table;
        let res = model.sRead(args);
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static async searchInDAGAsync(args = {}) {
        args.table = table;
        let res = await model.aRead(args);
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static zzremoveMappingForDocs(docHashes = []) {
        model.sDelete({
            table: 'i_docs_blocks_map',
            query: [['dbm_doc_hash', ['IN', docHashes]]]
        });
        return;
    }

    static retrieveDocByDocHash(args = {}) {
        clog.app.info(`retrieve DocByDocHash args ${utils.stringify(args)}`);

        let docHash = args.docHash;
        let needDExtInfo = _.has(args, 'needDExtInfo') ? args.needDExtInfo : false;
        let needDocMerkleProof = _.has(args, 'needDocMerkleProof') ? args.needDocMerkleProof : false;
        needDExtInfo |= needDocMerkleProof;

        let wBlocks = DAGHandler.getWBlocksByDocHash({ docsHashes: [docHash] }).wBlocks;
        if (wBlocks.length == 0)
            return { err: false, document: null };

        let block = blockUtils.openDBSafeObject(wBlocks[0].bBody).content;
        let document = null;
        let documentIndex = null;
        for (let docInx = 0; docInx < block.docs.length; docInx++) {
            let aDoc = block.docs[docInx];
            if ((document == null) && (aDoc.hash == docHash)) {
                documentIndex = docInx;
                document = aDoc;
                // break;
            }
        }

        if (needDExtInfo) {
            // block.bExtInfo = extinfosHandler.get BlockExtInfos(block.blockHash);
            let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
            if (bExtInfoRes.bExtInfo == null) {
                msg = `missed bExtInfo3 (${utils.hash16c(block.blockHash)})`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            block.bExtInfo = bExtInfoRes.bExtInfo
        }

        let proofs = null;
        if (needDocMerkleProof) {
            //implment it
            proofs = blockUtils.getDocumentMerkleProof({
                block,
                docHash
            });
        }

        return { err: false, document, documentIndex, proofs, bExtInfo: block.bExtInfo }
    }

    static getWBlocksByDocHash(args) {
        let docsHashes = args.docsHashes;
        let hashesType = _.has(args, 'hashesType') ? args.hashesType : iConsts.CONSTS.COMPLETE;
        let { blockHashes, mapDocToBlock } = DAGHandler.getBlockHashesByDocHashes({ docsHashes, hashesType });
        let wBlocks = DAGHandler.searchInDAGSync({
            query: [
                ['b_hash', ['IN', blockHashes]]
            ]
        });
        return { wBlocks, mapDocToBlock };
    }

    static getBlockHashesByDocHashes(args) {
        let docsHashes = args.docsHashes;
        let hashesType = _.has(args, 'hashesType') ? args.hashesType : iConsts.CONSTS.COMPLETE;

        let query = [];
        if (hashesType == iConsts.CONSTS.SHORT) {
            query.push(['dbm_doc_hash', ['LIKE:OR', docsHashes.map(x => `${x}%`)]])
        } else {
            query.push(['dbm_doc_hash', ['IN', docsHashes]])
        }

        let res = model.sRead({
            table: 'i_docs_blocks_map',
            fields: ['dbm_block_hash', 'dbm_doc_hash', 'dbm_doc_index'],
            query,
            log: false
        });
        let blockHashes = [];
        let mapDocToBlock = {};
        for (let element of res) {
            blockHashes.push(element.dbm_block_hash);
            // since we can have more than 1 coinbase block for each cycle, so mapping a document to its container block could be 1 to n mapping
            // also in lone transactions we have same psituation in which a certain transaction can take place in different blocks by different backers
            if (!_.has(mapDocToBlock, element.dbm_doc_hash))
                mapDocToBlock[element.dbm_doc_hash] = [];
            mapDocToBlock[element.dbm_doc_hash].push({
                blockHash: element.dbm_block_hash,
                docIndex: element.dbm_doc_index,
            });
        }
        return { blockHashes, mapDocToBlock }
    }


    /**
     * 
     * @param {string} blockHash 
     * static retrieves complete version of recorded block in 2 different tables i_blocks & i_block_segwits
     */
    static regenerateBlock(blockHash) {
        listener.doCallSync('SPSH_before_regenerate_block', { blockHash });
        let msg = '';
        let wBlock = this.searchInDAGSync({
            query: [
                ['b_hash', blockHash]
            ]
        });
        if (wBlock.length == 0) {
            // TODO: the block is valid and does not exist in local. or
            // invalid block invoked, maybe some penal for sender!
            msg = `The block (${utils.hash6c(blockHash)}) doesn't exist in DAG. 
        (could be invalid request or not synched local machine)`;
            clog.sec.info(msg);
            return { err: true, block: null, msg: msg }
        }

        wBlock = wBlock[0];
        let block = blockUtils.openDBSafeObject(wBlock.bBody).content;

        // if block has ext Infos, add it also
        if (iConsts.EXT_INFO_SUPPORTED.includes(block.bType)) {
            // block.bExtInfo = extinfosHandler.get BlockExtInfos(blockHash);
            let bExtInfoRes = extinfosHandler.getBlockExtInfos(blockHash);
            if (bExtInfoRes.bExtInfo == null) {
                msg = `missed bExtInfo4 (${utils.hash16c(blockHash)})`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            block.bExtInfo = bExtInfoRes.bExtInfo
        }

        return { err: false, block, msg }
    }


    static getMostConfidenceCoinbaseBlockFromDAG(cDate = null) {
        cDate = (utils._nilEmptyFalse(cDate)) ? utils.getNow() : cDate;

        let cbInfo = iutils.getCoinbaseInfo({ cDate })

        let currentCBsInDAG = this.searchInDAGSync({
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase],
                ['b_creation_date', ['>=', cbInfo.from]]
            ],
            order: [
                ['b_confidence', 'DESC']
            ],
            limit: 1
        });
        if (utils._nilEmptyFalse(currentCBsInDAG))
            return null

        return currentCBsInDAG[0];
    }

    static isDAGUptodated(_t = null) {
        let latestBlockDate = this.searchInDAGSync({
            fields: ['b_creation_date'],
            order: [
                ['b_creation_date', 'DESC']
            ],
            limit: 1
        });
        if (latestBlockDate.length == 0)
            return false;
        latestBlockDate = latestBlockDate[0]['bCreationDate'];
        return iutils.isYoungerThan2Cycle(latestBlockDate);
        // FIXME: it must be more sophisticated such as missed blocks count, last received blocks which are in parsingQ...
    }

    // need to be fixed because of array response of get BlockHashByDocHash
    static retrieveBlocksInWhichARefLocHaveBeenProduced(refLoc) {
        clog.trx.info(`retrieve Blocks In Which A Ref Loc Produced for refLoc: ${refLoc}`);
        let { blockHashes } = DAGHandler.getBlockHashesByDocHashes({
            docsHashes: [iutils.unpackCoinRef(refLoc).docHash]
        });
        let wBlocks = DAGHandler.searchInDAGSync({
            table,
            query: [['b_hash', ['IN', blockHashes]]]
        });
        if (wBlocks.length == 0) {
            clog.trx.error(`retrieve Blocks In Which A Ref Loc Produced for refLoc: ${refLoc} not exist`);
            return null;
        }

        // it could be possible some docHash exactly exist in more than one block
        // e.g multiplicated coinbase blocks which are created in same cycle
        // e.g cloned transactions(the same transaction which takes part in different blocks by diferent backers)
        // therfore we return list of blocks
        let res = [];
        for (let wBlock of wBlocks) {
            // console.log('wBlock.bBody', wBlock.bBody);
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            for (let docIndex = 0; docIndex < block.docs.length; docIndex++) {
                let doc = block.docs[docIndex];
                if (iutils.trxHasOutput(doc.dType)) {
                    for (let outputIndex = 0; outputIndex < doc.outputs.length; outputIndex++) {
                        let tmpRefLoc = iutils.packCoinRef(doc.hash, outputIndex);
                        if (tmpRefLoc == refLoc) {
                            res.push({
                                blockHash: block.blockHash,
                                docHash: doc.hash,
                                docIndex,
                                outputIndex,
                                outputAddress: doc.outputs[outputIndex][0],
                                outputValue: doc.outputs[outputIndex][1]
                            });
                        }
                    }
                }
            }
        }
        return res;
    }

    static DAGHasBlocksWhichAreCreatedInCurrrentCycle(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let latestBlockDate = this.searchInDAGSync({
            fields: ['b_creation_date'],
            order: [
                ['b_creation_date', 'DESC']
            ],
            limit: 1
        });
        if (latestBlockDate.length == 0)
            return false;
        latestBlockDate = latestBlockDate[0]['bCreationDate'];
        return utils.timeDiff(latestBlockDate, cDate).asMinutes < iConsts.getCycleByMinutes();
    }

    /**
  * 
  * @param {*} coins 
  * finding the blocks in which were created given coins
  */
    static getCoinsGenerationInfoViaSQL(coins) {
        let docsHashes = coins.map(x => iutils.unpackCoinRef(x).docHash);
        clog.trx.info(`find Output Info By RefLocViaSQL for docs: ${docsHashes.map(x => utils.hash6c(x))}`);
        // let wBlocks = this.searchInDAGSync({
        //     fields: ['b_body', 'b_cycle'],
        //     query: [
        //         ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]],
        //         ['b_body', ['LIKE:OR', docsHashes]],
        //     ]
        // });

        let wBlocks = DAGHandler.getWBlocksByDocHash({ docsHashes }).wBlocks;
        // console.log('find Output Info By RefLocViaSQL', wBlocks);
        let outputsDict = {};
        for (let wBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            if (_.has(block, 'docs')) {
                for (let doc of block.docs) {
                    if (_.has(doc, 'outputs')) {
                        for (let outputInx = 0; outputInx < doc.outputs.length; outputInx++) {
                            let output = doc.outputs[outputInx];
                            let aCoin = iutils.packCoinRef(doc.hash, outputInx);
                            outputsDict[aCoin] = {
                                coinGenCycle: wBlock.bCycle,
                                coinGenBlockHash: block.blockHash,
                                coinGenBType: block.bType,
                                coinGenCreationDate: block.creationDate,
                                coinGenDocType: doc.dType,
                                coinGenRefLoc: aCoin,
                                coinGenOutputAddress: output[0],
                                coinGenOutputValue: output[1]
                            }
                        }
                    }
                }
            }
        }

        let res = {}
        for (let aCoin of coins) {
            if (_.has(outputsDict, aCoin))
                res[aCoin] = outputsDict[aCoin];
        }
        return res;
    }

    static excludeFloatingBlocks(args) {
        // exclude floating signature blocks
        let blockHashes = _.has(args, 'blockHashes') ? args.blockHashes : [];
        let fields = _.has(args, 'fields') ? args.fields : ['b_hash', 'b_cycle', 'b_creation_date'];
        if ((utils._nilEmptyFalse(blockHashes)) || (!Array.isArray(blockHashes)) || (blockHashes.length == 0))
            return [];

        let wBlocksEFS = DAGHandler.searchInDAGSync({
            fields,
            query: [
                ['b_hash', ['IN', blockHashes]],
                ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]],
            ],
            log: false
        });
        return wBlocksEFS;
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'b_hash'))
            out.bHash = elm.b_hash;
        if (_.has(elm, 'b_type'))
            out.bType = elm.b_type;
        if (_.has(elm, 'b_cycle'))
            out.bCycle = elm.b_cycle;
        if (_.has(elm, 'b_confidence'))
            out.bConfidence = elm.b_confidence;
        if (_.has(elm, 'b_docs_root_hash'))
            out.bDocsRootHash = elm.b_docs_root_hash;
        if (_.has(elm, 'b_ext_root_hash'))
            out.bExtRootHash = elm.b_ext_root_hash;
        if (_.has(elm, 'b_signals'))
            out.bSignals = elm.b_signals;
        if (_.has(elm, 'b_trxs_count'))
            out.bTrxsCount = elm.b_trxs_count;
        if (_.has(elm, 'b_docs_count'))
            out.bDocsCount = elm.b_docs_count;
        if (_.has(elm, 'b_ancestors_count'))
            out.bAncestorsCount = elm.b_ancestors_count;
        if (_.has(elm, 'b_ancestors'))
            out.bAncestors = elm.b_ancestors;
        if (_.has(elm, 'b_descendents'))
            out.bDescendents = elm.b_descendents;
        if (_.has(elm, 'b_body'))
            out.bBody = elm.b_body;
        if (_.has(elm, 'b_creation_date'))
            out.bCreationDate = elm.b_creation_date;
        if (_.has(elm, 'b_receive_date'))
            out.bReceiveDate = elm.b_receive_date;
        if (_.has(elm, 'b_confirm_date'))
            out.bConfirmDate = elm.b_confirm_date;
        if (_.has(elm, 'b_backer'))
            out.bBacker = elm.b_backer;
        if (_.has(elm, 'b_utxo_imported'))
            out.bUtxoImported = elm.b_utxo_imported;
        return out;
    }

}

DAGHandler.addBH = require('./add-block-handler');
DAGHandler.addBH._super = DAGHandler;

DAGHandler.walkThrough = require('./walk-through-dag');
DAGHandler.walkThrough._super = DAGHandler;

DAGHandler.watcher = require('./watcher');
DAGHandler.watcher._super = DAGHandler;

DAGHandler.fSigs = require('./floating-sig-handler');
DAGHandler.fSigs._super = DAGHandler;

module.exports = DAGHandler;
