const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const leavesHandler = require('../leaves-handler');
const extinfosHandler = require('../extinfos-handler');
const listener = require('../../plugin-handler/plugin-handler');
const ballotsInRelatedBlock = require('../documents-in-related-block/ballots/ballots');
const iNamesInRelatedBlock = require('../documents-in-related-block/inames/inames');
const iNameBindsInRelatedBlock = require('../documents-in-related-block/inames/ipgp-binds');
const iNameMsgsInRelatedBlock = require('../documents-in-related-block/inames/msg');
const freeDocsInRelatedBlock = require('../documents-in-related-block/free-docs/free-docs');
const closePledgeInRelatedBlock = require('../documents-in-related-block/pledges/close');
const pledgeInRelatedBlock = require('../documents-in-related-block/pledges/pledges');
const pollingsInRelatedBlock = require('../documents-in-related-block/pollings/pollings');
const admPollingsInRelatedBlock = require('../documents-in-related-block/administrative-polling/administrative-polling');
const proposalsInRelatedBlock = require('../documents-in-related-block/proosals/proposals');
const scepticalDataIntegrityCheck = require('../sceptical-data-integrity-check');
const fileHandler = require('../../hard-copy/file-handler');
const signalsHandler = require('../../services/signals-handler/signals-handler');
const blockUtils = require('../block-utils');
const coinbaseUtxo = require('../../dag/coinbase/import-utxo-from-coinbases');

const table = 'i_blocks';


class AddBlockHandler {

    /**
     * this static insert the confirmed and validated block to table i_blocks, which means adding to blockchain (DAG)
     * @param {*} args 
     */
    static addBlockToDAG(args) {
        let receive_date = _.has(args, 'receive_date') ? args.receive_date : utils.getNow();
        let confirm_date = _.has(args, 'confirm_date') ? args.confirm_date : utils.getNow();
        let backerAddress = _.has(args, 'backerAddress') ? args.backerAddress : '';

        let block = args.block

        // duplicate check
        let res = model.sRead({
            table,
            fields: ["b_hash"],
            query: [
                ['b_hash', block.blockHash]
            ]
        });
        if (res.length > 0)
            return;

        utils.exitIfGreaterThanNow(block.creationDate);
        let bExtInfo = _.has(block, 'bExtInfo') ? block.bExtInfo : null;

        let blockTmp = _.cloneDeep(block);

        // save hard copy of blocks(timestamped by receive date) to have backup
        // in case of curruptions in DAG or bootstrp the DAG, machine doesn't need to download again entire DAG
        // you can simply copy files from ~/server/backup-dag to folder ~/server/temporary/inbox
        if (iConsts.DO_HARDCOPY_DAG_BACKUP) {
            res = fileHandler.packetFileCreateSync({
                dirName: iConsts.HD_PATHES.hd_backup_dag,
                fileName: `${utils.getNowSSS()}_${blockTmp.bType}_${blockTmp.blockHash}.txt`,
                fileContent: utils.stringify(blockTmp)
            });
        }


        //TODO: implementing atomicity(transactional) either in Js or DB
        blockTmp.ancestors = _.has(blockTmp, 'ancestors') ? blockTmp.ancestors : [];

        if (utils._nilEmptyFalse(backerAddress) && _.has(blockTmp, 'backer')) {
            backerAddress = blockTmp.backer
        }

        let backer_address = '';
        if (_.has(blockTmp, 'backer')) {
            backer_address = blockTmp.backer
        }

        if (_.has(blockTmp, 'bExtInfo'))
            delete blockTmp.bExtInfo;

        let descendents = _.has(blockTmp, 'descendents') ? blockTmp.descendents : [];

        let safeBlock = blockUtils.wrapSafeObjectForDB({ obj: blockTmp });
        let values = {
            b_hash: _.has(blockTmp, 'blockHash') ? block.blockHash : "",
            b_type: blockTmp.bType,
            b_confidence: _.has(args, 'confidence') ? args.confidence : 0.0,
            b_docs_root_hash: _.has(blockTmp, 'docsRootHash') ? blockTmp.docsRootHash : "",
            b_ext_root_hash: _.has(blockTmp, 'bExtHash') ? blockTmp.bExtHash : "",
            b_signals: _.has(blockTmp, 'signals') ? utils.stringify(blockTmp.signals) : "",
            b_trxs_count: _.has(blockTmp, 'transactions') ? blockTmp.docs.length : 0,
            b_ancestors: utils.stringify(blockTmp.ancestors),
            b_ancestors_count: blockTmp.ancestors.length,
            b_descendents: utils.stringify(descendents),
            b_creation_date: _.has(blockTmp, 'creationDate') ? blockTmp.creationDate : "",
            b_cycle: _.has(blockTmp, 'cycle') ? blockTmp.cycle : iutils.getCoinbaseRange(blockTmp.creationDate).from,
            b_receive_date: receive_date,
            b_confirm_date: confirm_date,
            b_backer: backerAddress,
            b_body: safeBlock,
            b_utxo_imported: iConsts.CONSTS.NO
        }
        clog.app.info(`--- recording block in DAG Block(${utils.hash6c(block.blockHash)})`);
        res = model.sCreate({
            lockDb: true,
            table,
            values: values
        });


        //save external infos(e.g. segwits, signatures, Cumbersome data/image/video...)) in seperated table
        clog.app.info(`block(${utils.hash6c(block.blockHash)}) save external infos `);
        if (!utils._nilEmptyFalse(bExtInfo))
            extinfosHandler.insertExtInfo({
                lockDb: true,
                blockHash: block.blockHash,
                bExtInfo: bExtInfo,
                creationDate: block.creationDate
            });


        // adjusting leave blocks
        leavesHandler.removeFromLeaveBlocks(blockTmp.ancestors);
        leavesHandler.addToLeaveBlocks(block.blockHash, block.creationDate, _.has(block, 'bType') ? block.bType : "UB");


        // update signals info
        signalsHandler.handleBlockSignals({
            backerAddress: backer_address,
            blockHash: blockTmp.blockHash,
            creationDate: blockTmp.creationDate,
            signals: blockTmp.signals,
        });

        //FIXME: filter suspicious docs!
        if (_.has(blockTmp, 'docs')) {
            clog.app.info(`block(${utils.hash6c(block.blockHash)}) connect documents and blocks, activate PollingForProposal, activate Pledge`);
            for (let docInx = 0; docInx < blockTmp.docs.length; docInx++) {
                let aDoc = blockTmp.docs[docInx];

                // TODO: refactore it for a good looking code and get ride of this switch cases
                switch (aDoc.dType) {
                    case iConsts.DOC_TYPES.Polling:
                        // recording block in DAG means also inserting Polling in DB(if exist) 
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to insert Polling(${utils.hash6c(aDoc.hash)}) `);
                        pollingsInRelatedBlock.recordAPolling({
                            polling: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.AdmPolling:
                        // recording block in DAG means also inserting Request For Release Reserved coins in DB(if exist) 
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to insert Polling(${utils.hash6c(aDoc.hash)}) `);
                        admPollingsInRelatedBlock.recordAnAdmPolling({
                            doc: aDoc,
                            block: blockTmp
                        });
                        break;

                    // case iConsts.DOC_TYPES.ReqForRelRes:
                    // TODO: move it to recordAnAdmPolling
                    //     // recording block in DAG means also inserting Request For Release Reserved coins in DB(if exist) 
                    //     clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to insert Polling(${utils.hash6c(aDoc.hash)}) `);
                    //     reqRelResInRelatedBlock.recordAReqRelRes({
                    //         reqRelRes: aDoc,
                    //         block: blockTmp
                    //     });
                    //     break;

                    case iConsts.DOC_TYPES.Ballot:
                        // recording block in DAG means also inserting ballots in DB(if exist)
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to insert Ballot(${utils.hash6c(aDoc.hash)}) `);
                        ballotsInRelatedBlock.recordABallot({
                            ballot: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.INameReg:
                        // recording block in DAG means also inserting iName in name_records
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to register iName(${utils.hash6c(aDoc.hash)}) `);
                        iNamesInRelatedBlock.recordINameInDAG({
                            flens: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.INameBind:
                        // recording block in DAG means also inserting iName-bindings in name_bindings
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to register iName-bind(${utils.hash6c(aDoc.hash)}) `);
                        iNameBindsInRelatedBlock.recordINameBindInDAG({
                            bindDoc: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.INameMsgTo:
                        // recording block in DAG means also inserting iName-msg in machine_messages(if machine is receiver)
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to register iName-bind(${utils.hash6c(aDoc.hash)}) `);
                        iNameMsgsInRelatedBlock.recordINameMsgInMachine({
                            msgDoc: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.DNAProposal:
                        // recording block in DAG means also starting voting period for proposals inside block(if exist)
                        // it implicitly leads to create a new polling and activate it in order to collect shareholders opinion about proposal
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to activate Proposal Polling(${utils.hash6c(aDoc.hash)}) `);
                        proposalsInRelatedBlock.activatePollingForProposal({
                            proposal: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.ClosePledge:
                        closePledgeInRelatedBlock.applyPledgeClosing({ pgdHash: aDoc.ref, blockCreationDate: block.creationDate });
                        break;

                    case iConsts.DOC_TYPES.Pledge:
                        // recording block in DAG means also activating pledges
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to apply Pledge(${utils.hash6c(aDoc.hash)}) `);
                        pledgeInRelatedBlock.activatePledge({
                            pledge: aDoc,
                            block: blockTmp
                        });
                        break;

                    case iConsts.DOC_TYPES.CPost:
                        // recording block in DAG means also MAYBE encode file and save it in local folder. depends on machine settings & signer reputation
                        clog.app.info(`block(${utils.hash6c(block.blockHash)}) try to write free-doc(${utils.hash6c(aDoc.hash)}) `);
                        let insertRes = freeDocsInRelatedBlock.customPostSpecialTreatments({
                            fDoc: aDoc,
                            block: blockTmp
                        });
                        if (insertRes != false) {
                            clog.sec.error(`custom Post Special Treatments res: ${utils.stringify(insertRes)}`);
                        }
                        break;

                }

                // connect documents and blocks
                model.sCreate({
                    table: 'i_docs_blocks_map',
                    values: {
                        'dbm_block_hash': blockTmp.blockHash,
                        'dbm_doc_index': docInx,
                        'dbm_doc_hash': aDoc.hash,
                        'dbm_last_control': utils.getNow()
                    }
                });
            };
        }

        // update descendents link of ancestors block. this descendents fild is used locally and never will be broadcasted
        AddBlockHandler.appendDescendents(blockTmp.ancestors, [blockTmp.blockHash]);

        res = scepticalDataIntegrityCheck.scepticalDataIntegrityCheck(blockTmp.blockHash);
        if (res.err != false) {
            clog.app.info(`error in sceptical Data Integrity Check: ${utils.stringify(res)}`);
            return res;
        }

        // if machine is in sync mode so try to import UTXOs ASAP
        // if (machine.isInSyncProcess()) {
        let cDate = utils.minutesAfter(iConsts.getCycleByMinutes() * 2, block.creationDate)
        if (cDate > utils.getNow())
            cDate = utils.getNow();
        if (block.bType == iConsts.BLOCK_TYPES.Coinbase) {
            // dummy async calling
            setTimeout(() => {
                coinbaseUtxo.importCoinbasedUTXOs({
                    cDate
                });
            }, 1000);

        } else if (block.bType == iConsts.BLOCK_TYPES.Normal) {
            const transferUtxo = require('../../dag/normal-block/import-utxo-from-normal-blocks/import-utxo-from-normal-blocks');
            // dummy async calling
            setTimeout(() => {
                transferUtxo.importNormalBlockUTXOs({
                    cDate
                });
            }, 1000);

        }
        // }
        return { err: false }
    }

    static postAddBlockToDAG(args) {
        let block = args.block
        // remove perequisity, if any block in parsing Q was needed to this block

        require('../../services/parsing-q-manager/parsing-q-handler').qUtils.removePrerequisitesSync(block.blockHash);

        /**
         * sometimes (e.g. repayback blocks which can be created by delay and causing to add block to missed blocks)
         * we need to doublecheck if the block still is in missed blocks list and remove it
         */
        const missedBlocksHandler = require('../../dag/missed-blocks-handler');
        missedBlocksHandler.removeFromMissedBlocks(block.blockHash);

        /**
         * inherit UTXO visibilities of ancestors of newly DAG-added block 
         * current block inherits the visibility of it's ancestors
         * possibly first level ancestors can be floating signatures(which haven't entry in table trx_utxos), 
         * so add ancestors of ancestors too, in order to being sure we keep good and reliable history in utxos
         */
        if (![iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote].includes(block.bType)) {
            let ancestors = block.ancestors;
            ancestors = utils.arrayAdd(ancestors, AddBlockHandler._super.walkThrough.getAncestors(ancestors));
            ancestors = utils.arrayAdd(ancestors, AddBlockHandler._super.walkThrough.getAncestors(ancestors));
            ancestors = utils.arrayUnique(ancestors);
            require("../../transaction/utxo/utxo-handler").inheritAncestorsVisbility({
                blockHashes: ancestors,
                creationDate: block.creationDate,
                newBlockHash: block.blockHash
            });
        }


        /**
         * remove from i_trx_utxo the the entries which are visible by blocks that are not "leave" any more and
         * they are placed 4 level backward(or older) in history 
         * since it is a optional process to lightening DB loadness, it could be done for 8 level previous too
         */
        let ancestors = AddBlockHandler._super.walkThrough.getAncestors(block.ancestors, 8);
        if (ancestors.length > 0) {
            // to be sure the visibility creation date is 2 cycles older than block creation date
            let { minCreationDate } = iutils.getCbUTXOsDateRange(block.creationDate);
            let wBlocks = AddBlockHandler._super.searchInDAGSync({
                query: [
                    ['b_hash', ['IN', ancestors]],
                    ['b_creation_date', ['<', minCreationDate]]
                ]
            });
            if (wBlocks.length > 0) {
                ancestors = wBlocks.map(x => x.bHash);
                if (ancestors.length > 0) {
                    clog.trx.info(`removing visible_by ${ancestors.map(x => utils.hash6c(x))}`);
                    require('../../transaction/utxo/utxo-handler').removeVisibleOutputsByBlocks(ancestors);
                }
            }
        }

    }


    /**
     * 
     * @param {array} blockHashes 
     * @param {array} descendents 
     * adds given descendents to given blocks
     */
    static appendDescendents(blockHashes, newDescendents) {
        if (!Array.isArray(blockHashes))
            blockHashes = [blockHashes]

        if (utils._nilEmptyFalse(newDescendents))
            newDescendents = [];
        if (!Array.isArray(newDescendents))
            newDescendents = [newDescendents];

        for (let aBlockHash of blockHashes) {
            let res = model.sRead({
                table,
                fields: ['b_hash', 'b_descendents'],
                query: [
                    ['b_hash', aBlockHash]
                ]
            });
            if (res.length == 1) {
                let currentDescend = res[0].b_descendents;
                if (utils._nilEmptyFalse(currentDescend)) {
                    currentDescend = [];
                } else {
                    currentDescend = utils.parse(currentDescend);
                }
                if (utils._nilEmptyFalse(currentDescend) || (!Array.isArray(currentDescend)))
                    currentDescend = [];

                let finalDescendents = utils.arrayUnique(utils.arrayAdd(newDescendents, currentDescend));
                model.sUpdate({
                    table,
                    query: [
                        ['b_hash', aBlockHash]
                    ],
                    updates: { b_descendents: utils.stringify(finalDescendents) },
                    log: false
                });
            }
        }
    }

}

module.exports = AddBlockHandler;
