const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const clog = require('../loggers/console_logger');
const model = require('../models/interface');
const leavesHandler = require('./leaves-handler');
const extinfosHandler = require('./extinfos-handler');
const listener = require('../plugin-handler/plugin-handler');
const ballotsInRelatedBlock = require('./documents-in-related-block/ballots/ballots');
const iNamesInRelatedBlock = require('./documents-in-related-block/inames/inames');
const pledgeInRelatedBlock = require('./documents-in-related-block/pledges/pledges');
const proposalsInRelatedBlock = require('./documents-in-related-block/proosals/proposals');



class ScepticalDataIntegrityCheck {

    /**
     * sceptical data integrity check
     * controls if ALL information of received block recorded properly?
     * since still does not implemented "atomic block process" application must control recording-block-in-DAG-actin result
     */
    static scepticalDataIntegrityCheck(blockHash) {
        let msg;
        const dagHandler = require('./graph-handler/dag-handler');

        // a series of checking to ensure if app succide to import all documents placed in block properly in database?

        // is block hash valid?
        // retrieve block from DAG
        let regBlock = dagHandler.regenerateBlock(blockHash);
        if (regBlock.err != false) {
            msg = `sDIC block(${utils.hash6c(blockHash)}) regenerating failed ${regBlock.msg}`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let block = regBlock.block;

        if (block.blockHash != blockHash) {
            msg = `sDIC block(${utils.hash6c(blockHash)}) has invalid hash(${utils.hash6c(block.blockHash)})`;
            clog.app.error(msg);
            return { err: true, msg }
        }




        // validate transactions recording
        // validate proposals recording(if exist)
        // validate pledges recording(if exist)
        // validate Ballots recording(if exist)
        // validate ...













        return { err: false };
    }


}

module.exports = ScepticalDataIntegrityCheck;