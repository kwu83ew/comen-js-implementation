const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const crypto = require('../crypto/crypto')
const mOfNHandler = require('../transaction/signature-structure-handler/general-m-of-n-handler');
const clog = require('../loggers/console_logger');

class StrictAddressHandler {

    static createANewStrictAddress(args) {
        let signatureVersion = _.has(args, 'signatureVersion') ? args.signatureVersion : '0.0.1';
        let signatureMod = _.has(args, 'signatureMod') ? args.signatureMod : '2/3'; // by default is two of three (m of n === m/n)
        let sSets = _.has(args, 'sSets') ? args.sSets : null;

        let signatureType = iConsts.SIGNATURE_TYPES.Strict;
        let privContainer = {};
        let pubToPrvMap = {};
        let keyPairs = [];
        let trxUnlockMerkle;
        let mSignaturesCount, nSignaturesCount;
        mSignaturesCount = signatureMod.split('/')[0]
        nSignaturesCount = signatureMod.split('/')[1]

        if (utils._nilEmptyFalse(sSets)) {
            sSets = [];
            for (let i = 0; i < nSignaturesCount; i++) {
                keyPairs[i] = crypto.generatePairKey();
                pubToPrvMap[keyPairs[i].pubKey] = keyPairs[i].prvKey
                let aSimpleDefaultSignSet = { sKey: keyPairs[i].pubKey, pPledge: iConsts.CONSTS.NO, pDelegate: iConsts.CONSTS.NO }; // it is default of only existing pubKey{sKey:publkey}
                if (i == 0) {
                    aSimpleDefaultSignSet.pPledge = iConsts.CONSTS.YES;  // only one signature permitted to pledge account
                    aSimpleDefaultSignSet.pDelegate = iConsts.CONSTS.YES;  // only one signature permitted to delegate 
                }
                sSets.push([iutils.doHashObject(aSimpleDefaultSignSet), aSimpleDefaultSignSet]);
            }
        }

        trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            customSalt: 'PURE_LEAVE',
            sType: signatureType,
            sVer: signatureVersion,
            sSets,
            neccessarySignaturesCount: mSignaturesCount
        });

        for (let anUnlockerSet of trxUnlockMerkle.uSets) {
            privContainer[anUnlockerSet.salt] = [];
            for (let aSignSet of anUnlockerSet.sSets)
                privContainer[anUnlockerSet.salt].push(pubToPrvMap[aSignSet.sKey]);

            // test unlock structure & signature 
            let isValidUnlock = mOfNHandler.validateSigStruct({
                address: trxUnlockMerkle.accountAddress,
                uSet: anUnlockerSet
            });
            if (isValidUnlock == true) {
                console.log(`new address ${iutils.shortBech(trxUnlockMerkle.accountAddress)} created & tested successfully`);
            } else {
                console.error('Curropted strict address created!?');
                utils.exiter(trxUnlockMerkle, 623);
            }
        }
        trxUnlockMerkle.privContainer = privContainer;
        // console.log(`\n trxUnlockMerkle: ${utils.stringify(trxUnlockMerkle)}\n`);

        // validate signature of new address
        let signMsg = iutils.convertTitleToHash('Imagine all the people living life in peace').substring(0, iConsts.SIGN_MSG_LENGTH);
        let fetchedPrivContainer = trxUnlockMerkle.privContainer;
        for (let aUSet of trxUnlockMerkle.uSets) {
            for (let inx = 0; inx < aUSet.sSets.length; inx++) {
                let aSignature = crypto.signMsg(signMsg, fetchedPrivContainer[aUSet.salt][inx]);
                let verifyRes = crypto.verifySignature(signMsg, aSignature, aUSet.sSets[inx].sKey);
                if (verifyRes != true) {
                    console.error('Curropted strict address created signature!?');
                    utils.exiter(trxUnlockMerkle, 923);
                }
            }
        }

        let res = {
            address: trxUnlockMerkle.accountAddress,
            title: `${signatureType}:${signatureMod}`,
            detail: trxUnlockMerkle
        }
        return res;
    }

}

module.exports = StrictAddressHandler;