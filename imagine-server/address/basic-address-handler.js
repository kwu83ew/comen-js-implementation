const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const crypto = require('../crypto/crypto');
const mOfNHandler = require('../transaction/signature-structure-handler/general-m-of-n-handler');
const clog = require('../loggers/console_logger');

class BasicAddressHandler {


    static createANewBasicAddress(args) {
        let mSignaturesCount, nSignaturesCount;
        let sSets = null;
        console.log(`create NewBasicAddress args`, args);
        let signatureType = iConsts.SIGNATURE_TYPES.Basic; // by default is two of three (m of n === m/n)
        let signatureVersion = _.has(args, 'signatureVersion') ? args.signatureVersion : '0.0.0'; // by default is two of three (m of n === m/n)
        let signatureMod = _.has(args, 'signatureMod') ? args.signatureMod : '2/3'; // by default is two of three (m of n === m/n)
        console.log(`signatureType:signatureMod ${signatureType}:${signatureMod}`);

        let privContainer = {};

        let pubToPrvMap = {};
        let keyPairs = [];
        let trxUnlockMerkle;


        mSignaturesCount = signatureMod.split('/')[0]
        nSignaturesCount = signatureMod.split('/')[1]
        sSets = _.has(args, 'sSets') ? args.sSets : null;

        if (utils._nilEmptyFalse(sSets)) {
            sSets = [];
            for (let i = 0; i < nSignaturesCount; i++) {
                keyPairs[i] = crypto.generatePairKey();
                pubToPrvMap[keyPairs[i].pubKey] = keyPairs[i].prvKey
                let aSimpleDefaultSignSet = { sKey: keyPairs[i].pubKey }; // it is default of only existing pubKey{sKey:publkey}
                sSets.push([iutils.doHashObject(aSimpleDefaultSignSet), aSimpleDefaultSignSet]);
            }
        }

        console.log(`\n sSets: ${utils.stringify(sSets)}\n`);

        trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets,
            neccessarySignaturesCount: mSignaturesCount
        });
        // console.log(`\n trxUnlockMerkle========: ${utils.stringify(trxUnlockMerkle)}\n`);

        for (let anUnlockerSet of trxUnlockMerkle.uSets) {
            privContainer[anUnlockerSet.salt] = [];
            for (let aSignSet of anUnlockerSet.sSets)
                privContainer[anUnlockerSet.salt].push(pubToPrvMap[aSignSet.sKey]);

            // test unlock structure & signature 
            let isValidUnlock = mOfNHandler.validateSigStruct({
                address: trxUnlockMerkle.accountAddress,
                uSet: anUnlockerSet
            });
            if (isValidUnlock) {
                clog.trx.info(`new Bassic address ${iutils.shortBech(trxUnlockMerkle.accountAddress)} created & tested successfully`);
            } else {
                console.error('Curropted Basic address created!');
                console.error(trxUnlockMerkle);
                utils.exiter(trxUnlockMerkle, 33);
            }
        }
        trxUnlockMerkle.privContainer = privContainer;
        // console.log(`\n trxUnlockMerkle: ${utils.stringify(trxUnlockMerkle)}\n`);

        // validate signature of new address
        let signMsg = iutils.convertTitleToHash('Imagine all the people living life in peace').substring(0, iConsts.SIGN_MSG_LENGTH);
        let fetchedPrivContainer = trxUnlockMerkle.privContainer;
        for (let aUSet of trxUnlockMerkle.uSets) {
            for (let inx = 0; inx < aUSet.sSets.length; inx++) {
                let aSignature = crypto.signMsg(signMsg, fetchedPrivContainer[aUSet.salt][inx]);
                let verifyRes = crypto.verifySignature(signMsg, aSignature, aUSet.sSets[inx].sKey);
                if (verifyRes != true) {
                    let msg = `Curropted Basic address created signature!? ${utils.stringify(trxUnlockMerkle)}`
                    utils.exiter(msg, 423);
                }
            }
        }

        let res = {
            address: trxUnlockMerkle.accountAddress,
            title: `${signatureType}:${signatureMod}`,
            detail: trxUnlockMerkle
        }
        return res;
    }

}

module.exports = BasicAddressHandler;