// const _ = require('lodash');
// const iConsts = require('../config/constants');
// const utils = require('../utils/utils')
// const iutils = require('../utils/iutils')
// const clog = require('../loggers/console_logger');
// const crypto = require("../crypto/crypto");
// const mOfNHandler = require("../transaction/signature-structure-handler/general-m-of-n-handler");

// const DOES_SUPPORT_RECURSIVE_BECH = false;  //TODO: implement it ASAP

// let indent = 0;
// class ComplexAddressHandler {

//     static prepareAddresses(args) {
//         let nSignaturesCount = Math.floor(Math.random() * 4) + 2;
//         let mSignaturesCount = Math.floor(Math.random() * nSignaturesCount) + 1;
//         if (mSignaturesCount > nSignaturesCount) mSignaturesCount = nSignaturesCount;
//         mSignaturesCount = 2;
//         nSignaturesCount = 3;
//         let signatureType = ` complex ${mSignaturesCount}/${nSignaturesCount}`
//         console.log(`\n\n ComplexAddressHandler: ${mSignaturesCount}/${nSignaturesCount} \n`);

//         // retrieve some address to use as a restricted receivers
//         let addressesDict = {};
//         const walletAddressHandler = require("../web-interface/wallet/wallet-address-handler");
//         let walletAddresses = walletAddressHandler.getAddressesListSync();
//         for (let address of walletAddresses) {
//             addressesDict[address.address] = {
//                 title: address.title,
//                 detail: utils.parse(address.detail)
//             };
//         }
//         walletAddresses = walletAddresses.map(x => x.address);
//         let msg;
//         console.log(utils.stringify(walletAddresses));
//         console.log(utils.stringify(addressesDict));

//         let rawSignSets = [];
//         // let signers = [];
//         // let bechUnlockers = {};
//         let pubToPrvMap = {};
//         let tmpPrvKey;

//         let createdSignatures = 0;
//         while (createdSignatures < nSignaturesCount) {
//             let randomBool = Math.floor(Math.random() * 10);
//             let aSigner;


//             if ((createdSignatures == 0) && !utils._nilEmptyFalse(args.customBechUnlocker)) {
//                 aSigner = args.customBechUnlocker;  // client injects a bech32 address as a part of lock/unlock encumbrances
//                 tmpPrvKey = addressesDict[aSigner].detail;
//                 if (!DOES_SUPPORT_RECURSIVE_BECH) {
//                     // control if unlockers have bechAddress?
//                     let hasBech = false;
//                     for (let anUnlocker of addressesDict[aSigner].detail.unlockers) {
//                         for (let sDignSet of anUnlocker.sSets) {
//                             if (crypto.bech32_isValidAddress(sDignSet[0])) {
//                                 hasBech = true;
//                             }
//                         }
//                     }
//                     if (hasBech) {
//                         msg = `1.System still doesn't support bech indenting in unlock structure`
//                         clog.app.error(msg);
//                         console.log(msg);
//                         continue;
//                     }
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`, utils.stringify(addressesDict[aSigner]));
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                 }
//                 // privContainer = args.parent.addToDictContainer(privContainer, aSigner, addressesDict[aSigner].detail.unlockers);
//                 // privContainer = walletAddressHandler.fillPrivContainer(privContainer, addressesDict[aSigner].detail);
//                 // console.log(`\n\n custom privContainer privContainer: ${utils.stringify(privContainer)}\n`);

//             } else if ((randomBool > 5) && (indent < 100)) {
//                 // use bech32 address instead of public-key 
//                 // in theory each address can be unlimited bech32-indenting, but we limit it to 100 layer for sack of machins process consuming
//                 aSigner = walletAddresses[Math.floor(Math.random() * walletAddresses.length)];
//                 tmpPrvKey = addressesDict[aSigner].detail;
//                 if (!DOES_SUPPORT_RECURSIVE_BECH) {
//                     // control if unlockers have bechAddress?
//                     let hasBech = false;
//                     for (let anUnlocker of addressesDict[aSigner].detail.unlockers) {
//                         for (let sDignSet of anUnlocker.sSets) {
//                             if (crypto.bech32_isValidAddress(sDignSet[0])) {
//                                 hasBech = true;
//                             }
//                         }
//                     }
//                     if (hasBech) {
//                         msg = `2.System still doesn't support bech indenting in unlock structure`
//                         clog.app.error(msg);
//                         console.log(msg);
//                         continue;
//                     }
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`, utils.stringify(addressesDict[aSigner]));
//                     console.log(`tmpPrvKey tmpPrvKey tmpPrvKey`);
//                 }
//                 // if (!_.has(privContainer, aSigner))
//                 //     privContainer[aSigner] = [];
//                 // privContainer[aSigner].push(addressesDict[aSigner].detail.unlockers);
//                 // privContainer = args.parent.addToDictContainer(privContainer, aSigner, addressesDict[aSigner].detail.unlockers);

//                 // // privContainer: addressesDict[aSigner].detail.privContainer
//                 // console.log(`\n\n privContainer privContainer: ${utils.stringify(privContainer)}\n`);
//                 // // control infinite loop
//                 // privContainer = walletAddressHandler.fillPrivContainer(privContainer, addressesDict[aSigner].detail);

//             } else {
//                 let keyPair = crypto.generatePairKey();
//                 aSigner = keyPair.pubKey;
//                 tmpPrvKey = keyPair.prvKey;

//                 // if (!_.has(privContainer, aSigner))
//                 //     privContainer[aSigner] = [];
//                 // privContainer[aSigner].push(keyPair[i].prvKey);
//                 // privContainer = args.parent.addToDictContainer(privContainer, aSigner, keyPair[i].prvKey);

//             }

//             let outputTimeLock = Math.floor(Math.random() * (iConsts.getCycleByMinutes() * 3));
//             if ((outputTimeLock < (iConsts.getCycleByMinutes() / 3)) || (outputTimeLock > (iConsts.getCycleByMinutes() * (5 / 7))))
//                 outputTimeLock = 0; // dummy decision to give more chance for beeing 0

//             let dailySpendLimitTotal = 0;
//             let allowedReceivers = [];
//             // let receiversCount = Math.floor(Math.random() * 10);
//             // for (let rec = 0; rec < receiversCount; rec++) {
//             //     let dailySpendLimit = Math.floor(Math.random() * 100000000);
//             //     if ((dailySpendLimit < 100) || (dailySpendLimit > 10000000) || ((dailySpendLimit > 100000) && (dailySpendLimit < 105000)))
//             //         dailySpendLimit = 0; // dummy decision to give more chance for beeing 0

//             //     dailySpendLimitTotal += dailySpendLimit;
//             //     allowedReceivers.push(
//             //         [
//             //             walletAddresses[Math.floor(Math.random() * walletAddresses.length)],    // a random receiver
//             //             dailySpendLimit     // daily-spend-limit for this receiver
//             //         ]
//             //     )
//             // }

//             let aSignSet = [
//                 aSigner,
//                 allowedReceivers,
//                 Math.floor(Math.random() * dailySpendLimitTotal * 2),   // total daily-spend-limit
//                 Math.floor(Math.random() * dailySpendLimitTotal / 20)   // arbitary daily spend limit (usually used for paying transaction DPCost)
//             ];
//             let aSignSetHash = crypto.keccak256(utils.stringify(aSignSet));
//             rawSignSets.push([aSignSetHash, aSignSet]);
//             pubToPrvMap[aSignSetHash] = tmpPrvKey;
//             // signers.push(key);

//             createdSignatures++;
//         }
//         return { pubToPrvMap, rawSignSets, mSignaturesCount, nSignaturesCount }
//     }

//     static makeARandomAddress(args) {
//         return this.createAddress(this.prepareAddresses(args));
//     }
    
//     static createAddress(args) {
//         let privContainer = {};
//         let { pubToPrvMap, rawSignSets, mSignaturesCount, nSignaturesCount } = args;
//         console.log(`\n ------ pubToPrvMap: ${utils.stringify(pubToPrvMap)}\n`);
//         console.log(`\n ------ ${mSignaturesCount} of n rawSignSets: ${utils.stringify(rawSignSets)}\n`);
//         let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({sSets:rawSignSets, neccessarySignaturesCount: mSignaturesCount});
//         console.log(`\n ------ trxUnlockMerkl1e1: ${utils.stringify(trxUnlockMerkle)}\n`);
//         for (let anUnlockerSet of trxUnlockMerkle.unlockers) {
//             privContainer[anUnlockerSet.salt] = { unlockers: [], privContainer: {} };
//             // bechUnlockers[anUnlockerSet.salt] = [];
//             for (let aSignSet of anUnlockerSet.sSets) {
//                 let tmpPrv = pubToPrvMap[crypto.keccak256(utils.stringify(aSignSet))];
//                 if ((typeof tmpPrv === 'string' || tmpPrv instanceof String)) {
//                     privContainer[anUnlockerSet.salt].unlockers.push(null);
//                     // privContainer[anUnlockerSet.salt].privContainer.push(tmpPrv);
//                 } else {
//                     // set also privContainer of sub bech address
//                     for (let anIndentUnlocker of tmpPrv.unlockers) {
//                         privContainer[anUnlockerSet.salt].unlockers.push(tmpPrv.unlockers);
//                         privContainer[anUnlockerSet.salt].privContainer[anIndentUnlocker.salt] = {
//                             unlockers: tmpPrv.privContainer[anIndentUnlocker.salt].unlockers,
//                             privContainer: {}    // TODO implement recursive bech address
//                         };
//                         // for (let anIndentSignSet of anIndentUnlocker.sSets) {
//                         //     let indentTmpPrv = pubToPrvMap[crypto.keccak256(utils.stringify(aSignSet))];
//                         // }
//                     }

//                 }

//                 // test unlock structure & signature 
//                 let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
//                     uSet: {
//                         address: trxUnlockMerkle.accountAddress,
//                         sSets: anUnlockerSet.sSets,
//                         lHash: anUnlockerSet.lHash,
//                         proofs: anUnlockerSet.proofs,
//                         salt: anUnlockerSet.salt
//                     }
//                 });
//                 if (isValidUnblock == true) {
//                     console.log(`new address ${iutils.shortBech8(trxUnlockMerkle.accountAddress)} created & tested successfully`);
//                 } else {
//                     console.error('Curropted address created!');
//                     console.error(trxUnlockMerkle);
//                 }
//             }
//         }
//         // trxUnlockMerkle.bechUnlockers = bechUnlockers;
//         trxUnlockMerkle.privContainer = privContainer;
//         console.log(`\n ------ trxUnlockMerkle2: ${utils.stringify(trxUnlockMerkle)}\n`);

//         let res = { mSignaturesCount, nSignaturesCount, privContainer, trxUnlockMerkle, signatureType }
//         console.log(`\n\n ressss: ${utils.stringify(res)}\n`);

//         return res;
//     }
// }


// module.exports = ComplexAddressHandler;