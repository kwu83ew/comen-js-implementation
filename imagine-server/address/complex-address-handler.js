const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const clog = require('../loggers/console_logger');
const crypto = require("../crypto/crypto");
const mOfNHandler = require("../transaction/signature-structure-handler/general-m-of-n-handler");

const DOES_SUPPORT_RECURSIVE_BECH = false;  //TODO: implement it ASAP

let indent = 0;
class ComplexAddressHandler {

    static prepareAddresses(args) {
        let nSignaturesCount = Math.floor(Math.random() * 4) + 2;
        let mSignaturesCount = Math.floor(Math.random() * nSignaturesCount) + 1;
        if (mSignaturesCount > nSignaturesCount) mSignaturesCount = nSignaturesCount;
        let signatureType = `Basic`;
        let signatureVersion = `0.0.0`;
        let signatureMod = `${mSignaturesCount}/${nSignaturesCount}`
        console.log(`\n\n ComplexAddressHandler: ${mSignaturesCount}/${nSignaturesCount} \n`);

        // retrieve some address to use as a restricted receivers
        let addressesDict = {};
        const walletAddressHandler = require('../web-interface/wallet/wallet-address-handler');
        let walletAddresses = walletAddressHandler.getAddressesListSync();
        for (let address of walletAddresses) {
            addressesDict[address.waAddress] = {
                title: address.title,
                detail: utils.parse(address.detail)
            };
        }
        walletAddresses = walletAddresses.map(x => x.waAddress);
        let msg;
        console.log('walletAddresses', utils.stringify(walletAddresses));
        console.log('addressesDict', utils.stringify(addressesDict));

        let rawSignSets = [];
        let pubToPrvMap = {};
        let tmpPrvKey;

        let createdSignatures = 0;
        while (createdSignatures < nSignaturesCount) {
            let aSigner;


            let keyPair = crypto.generatePairKey();
            aSigner = keyPair.pubKey;
            tmpPrvKey = keyPair.prvKey;

            let inputTimeLock = Math.floor(Math.random() * (iConsts.getCycleByMinutes() * 4));
            if ((inputTimeLock < (iConsts.getCycleByMinutes() / 4))) {
                inputTimeLock = 0; // dummy decision to give more chance for beeing 0
            } else {
                inputTimeLock += iConsts.getCycleByMinutes();
            }

            let outputTimeLock = Math.floor(Math.random() * (iConsts.getCycleByMinutes() * 3));
            if ((outputTimeLock < (iConsts.getCycleByMinutes() / 3)) || (outputTimeLock > (iConsts.getCycleByMinutes() * (5 / 7))))
                outputTimeLock = 0; // dummy decision to give more chance for beeing 0

            let dailySpendLimitTotal = 0;
            let allowedReceivers = [];
            let allowedReceiversPercent = [];
            let receiversCount = Math.floor(Math.random() * 4);
            for (let rec = 0; rec < receiversCount; rec++) {
                let dailySpendLimit = Math.floor(Math.random() * 100000000);
                if ((dailySpendLimit < 100) || (dailySpendLimit > 10000000) || ((dailySpendLimit > 100000) && (dailySpendLimit < 105000)))
                    dailySpendLimit = 0; // dummy decision to give more chance for beeing 0

                dailySpendLimitTotal += dailySpendLimit;
                allowedReceivers.push(
                    [
                        walletAddresses[Math.floor(Math.random() * walletAddresses.length)],    // a random receiver
                        {
                            y: dailySpendLimit * 365,
                            m: dailySpendLimit * 30,
                            w: dailySpendLimit * 7,
                            d: dailySpendLimit
                        }     // spend-limit for this receiver
                    ]
                )
                allowedReceiversPercent.push(
                    [
                        walletAddresses[Math.floor(Math.random() * walletAddresses.length)],    // a random receiver
                        Math.random()*100
                    ]
                )
            }

            let permittedToPledge = Math.floor(Math.random() * 100);
            if (permittedToPledge < 50) {
                permittedToPledge = iConsts.CONSTS.NO;
            } else {
                permittedToPledge = iConsts.CONSTS.YES;
            }

            let permittedDelegate = Math.floor(Math.random() * 100);
            if (permittedDelegate < 50) {
                permittedDelegate = iConsts.CONSTS.NO;
            } else {
                permittedDelegate = iConsts.CONSTS.YES;
            }

            let totalSpendLimit = Math.floor(Math.random() * dailySpendLimitTotal * 2);
            let arbitrarySpendLimit = Math.floor(Math.random() * dailySpendLimitTotal / 20);
            let aSignSet = {
                sKey: aSigner,
                iTLock: inputTimeLock,
                oTLock: outputTimeLock,
                pSpends: allowedReceivers,   // permitted spends
                pPSpends: allowedReceiversPercent,   // permitted percentage spends. a signature can have only one of pSpends or pPSpends
                pSpendsLimit: {
                    y: totalSpendLimit * 365,
                    m: totalSpendLimit * 30,
                    w: totalSpendLimit * 7,
                    d: totalSpendLimit
                },
                //arbitrary Spends Limit: tipically used to pay DPCosts of transaction
                aSpendsLimit: {
                    y: arbitrarySpendLimit * 365,
                    m: arbitrarySpendLimit * 30,
                    w: arbitrarySpendLimit * 7,
                    d: arbitrarySpendLimit
                },
                pPledge: permittedToPledge,   //permitted Pledge
                pDelegate: permittedDelegate   //permitted Pledge
            };
            let aSignSetHash = iutils.doHashObject(aSignSet);
            rawSignSets.push([aSignSetHash, aSignSet]);
            pubToPrvMap[aSignSetHash] = tmpPrvKey;
            // signers.push(key);

            createdSignatures++;
        }
        return { signatureType, signatureVersion, signatureMod, pubToPrvMap, rawSignSets, mSignaturesCount, nSignaturesCount }
    }

    static createANewComplexModAddress(args) {
        console.log(`createNewAddress args`, args);


        // create a dummy complex signature for test system
        let preparedArgs = this.prepareAddresses();
        let { 
            mSignaturesCount, 
            nSignaturesCount, 
            privContainer, 
            trxUnlockMerkle, 
            signatureType, 
            signatureMod } = this.createAddress(preparedArgs);

        let res = {
            address: trxUnlockMerkle.accountAddress,
            title: signatureMod,
            detail: trxUnlockMerkle
        }
        return res;

    }

    static createAddress(args) {
        let privContainer = {};
        let { signatureType, signatureVersion, signatureMod, pubToPrvMap, 
            rawSignSets, mSignaturesCount, nSignaturesCount } = args;

        console.log(`\n ------ pubToPrvMap: ${utils.stringify(pubToPrvMap)}\n`);
        console.log(`\n ------ ${mSignaturesCount} of n rawSignSets: ${utils.stringify(rawSignSets)}\n`);
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: rawSignSets,
            neccessarySignaturesCount: mSignaturesCount
        });
        console.log(`\n ------ trxUnlockMerkl1e1: ${utils.stringify(trxUnlockMerkle)}\n`);
        for (let anUnlockerSet of trxUnlockMerkle.uSets) {
            privContainer[anUnlockerSet.salt] = [];
            for (let aSignSet of anUnlockerSet.sSets) {
                let tmpPrv = pubToPrvMap[iutils.doHashObject(aSignSet)];
                privContainer[anUnlockerSet.salt].push(tmpPrv);

                // test unlock structure & signature 
                let isValidUnlock = mOfNHandler.validateSigStruct({
                    address: trxUnlockMerkle.accountAddress,
                    uSet: anUnlockerSet
                });
                if (isValidUnlock == true) {
                    console.log(`new address ${iutils.shortBech8(trxUnlockMerkle.accountAddress)} created & tested successfully`);
                } else {
                    console.error('Curropted complex address created!!');
                    utils.exiter(trxUnlockMerkle, 123);
                }
            }
        }
        // trxUnlockMerkle.bechUnlockers = bechUnlockers;
        trxUnlockMerkle.privContainer = privContainer;
        console.log(`\n ------ trxUnlockMerkle2: ${utils.stringify(trxUnlockMerkle)}\n`);

        // validate signature of new address
        let signMsg = iutils.convertTitleToHash('Imagine all the people living life in peace').substring(0, iConsts.SIGN_MSG_LENGTH);
        let fetchedPrivContainer = trxUnlockMerkle.privContainer;
        for (let aUSet of trxUnlockMerkle.uSets) {
            for (let inx = 0; inx < aUSet.sSets.length; inx++) {
                let aSignature = crypto.signMsg(signMsg, fetchedPrivContainer[aUSet.salt][inx]);
                let verifyRes = crypto.verifySignature(signMsg, aSignature, aUSet.sSets[inx].sKey);
                if (verifyRes != true) {
                    console.error('Curropted complex address created signature!?');
                    utils.exiter(trxUnlockMerkle, 123);
                }
            }
        }

        let res = { mSignaturesCount, nSignaturesCount, privContainer, trxUnlockMerkle, signatureType, signatureMod }
        console.log(`\n\n ressss: ${utils.stringify(res)}\n`);

        return res;
    }
}

module.exports = ComplexAddressHandler;