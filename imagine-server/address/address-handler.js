const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils')
const iutils = require('../utils/iutils')
const crypto = require('../crypto/crypto')
const clog = require('../loggers/console_logger');
const basicAddressHandler = require('./basic-address-handler');
const complexAddressHandler = require('./complex-address-handler');
const strictAddressHandler = require('./strict-address-handler');
const bitcoinAddressHandler = require('./bitcoin-address-handler');

class AddressHandler {

    static addToDictContainer(privContainer, aSigner, value) {
        if (!_.has(privContainer, aSigner))
            privContainer[aSigner] = {};
        privContainer[aSigner][iutils.doHashObject(value)] = value;
        return privContainer;
    }

    static createANewAddress(args) {
        let signatureType = _.has(args, 'signatureType') ? args.signatureType : iConsts.SIGNATURE_TYPES.Basic;
        let signatureMod = _.has(args, 'signatureMod') ? args.signatureMod : '2/3'; // by default is two of three (m of n === m/n)
        switch (signatureType) {

            case iConsts.SIGNATURE_TYPES.Basic:
                if (signatureMod == 'complex') {
                    return complexAddressHandler.createANewComplexModAddress(args);
                } else {
                    return basicAddressHandler.createANewBasicAddress(args);
                }
                break;

            case iConsts.SIGNATURE_TYPES.Strict:
                return strictAddressHandler.createANewStrictAddress(args);
                break;

            case iConsts.SIGNATURE_TYPES.Bitcoin:
                return bitcoinAddressHandler.createANewBitcoinAddress(args);
                break;

        }
        let msg = `Unknown address signatureType(${signatureType}) to create!`;
        clog.app.error(msg);
        console.log(msg);
        utils.exiter(msg, 473);
    }
}

module.exports = AddressHandler;
