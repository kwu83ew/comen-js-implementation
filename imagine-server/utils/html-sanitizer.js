const sanitizeHtml = require('sanitize-html');

/**
 * https://www.npmjs.com/package/sanitize-html
 * 
 * npm install sanitize-html
 * 
 * TODO: improve it for full cover OWASP
 * 
 */
class HtmlSanitizer {

	static sanitizeHtml(dirty) {
		return sanitizeHtml(dirty);
	}

}
module.exports = HtmlSanitizer;