const _ = require('lodash');
const iConsts = require('../config/constants');
const clog = require('../loggers/console_logger');

let Moment = false; // TODO: fix this priority includings
let getMoment = false; // TODO: fix this priority includings
const utf8 = require('utf8');



class utils {

    static breakByBR(content, chunkSize = 100) {
        function chunkSubstr(str, size) {
            const numChunks = Math.ceil(str.length / size)
            const chunks = new Array(numChunks)

            for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
                chunks[i] = str.substr(o, size)
            }

            return chunks
        }
        content = chunkSubstr(content, chunkSize);
        content = content.join(`${iConsts.msgTags.iPGPEndLineBreak}${iConsts.msgTags.iPGPStartLineBreak}`)
        content = `${iConsts.msgTags.iPGPStartLineBreak}${content}${iConsts.msgTags.iPGPEndLineBreak}`
        // content = content.match(/.{1,2}/g);
        return content;
    }

    static stripBR(content) {
        if (content.indexOf('(') != -1) {
            content = content.match(/\([^)]*\)/g).map((s) => { return s.substr(1, s.indexOf(')') - 1); });
            content = content.join('');
        } else {
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            console.log('zzzzzzzzz^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            const crypto = require('../crypto/crypto');
            console.log(crypto.b64Decode(content));
            console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
            dummFyBreakerZ.splei('*')
            console.log(dummyBreakerz);

        }
        return content;
    }

    static exiter(msg, number = '') {
        console.log(msg);
        console.error(msg);
        process.exit(number);
    }

    static syntaxHighlight(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }
    
    static syntaxHighlight2(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '\t' + match + '';
        });
    }

    /**
     * this function converts science presentation of very small/big numbers to string
     * @param {*} num 
     */
    static numberToString(num) {
        let numStr = String(num);

        if (Math.abs(num) < 1.0) {
            let e = parseInt(num.toString().split('e-')[1]);
            if (e) {
                let negative = num < 0;
                if (negative) num *= -1
                num *= Math.pow(10, e - 1);
                numStr = '0.' + (new Array(e)).join('0') + num.toString().substring(2);
                if (negative) numStr = "-" + numStr;
            }
        }
        else {
            let e = parseInt(num.toString().split('+')[1]);
            if (e > 20) {
                e -= 20;
                num /= Math.pow(10, e);
                numStr = num.toString() + (new Array(e + 1)).join('0');
            }
        }

        return numStr;
    }

    static decodeURIComponent(uri) {
        return decodeURIComponent(uri);
    }

    static encode_utf8(s) {
        // var words = encode_utf8('Marché')
        // Original
        return unescape(encodeURIComponent(s));
    }

    static decode_utf8(s) {
        return decodeURIComponent(escape(s));
    }

    static isValidDateForamt(cDate = '') {
        if (!Moment)
            Moment = require("../startup/singleton").instance.Moment;
        return Moment(cDate, 'YYYY-MM-DD HH:mm:ss', true).isValid()
    }

    static getAllMethodNames(obj) {
        return Object.getOwnPropertyNames(obj).concat(Object.getOwnPropertyNames(obj.__proto__))
    }

    static calcLog(x, range, exp = 17) {
        let hundredPercent = Math.log(Math.pow(range, exp));
        let y = (x >= range) ? 0 : Math.log(Math.pow((range - x), exp));
        let gain = this.iFloorFloat((y * 100) / hundredPercent);

        if ((gain == Number.POSITIVE_INFINITY || gain == Number.NEGATIVE_INFINITY))
            gain = Number.MAX_VALUE;

        let revGain = (1 / gain);
        if ((revGain == Number.POSITIVE_INFINITY || revGain == Number.NEGATIVE_INFINITY))
            revGain = Number.MAX_VALUE;

        return {
            x,
            y,
            gain,
            revGain
        };
    }

    static sanitizingContent(str) {
        // TODO: improve it ASAP
        let htmlSanitizer = require('./html-sanitizer');
        return htmlSanitizer.sanitizeHtml(str);
    }

    static utf8Encode(str) {
        return utf8.encode(str);
    }

    static utf8Decode(str) {
        return utf8.decode(str);
    }

    static parse(str) {
        return JSON.parse(str);
    }

    static stringify(obj) {
        return JSON.stringify(obj);
    }

    static isString(value) {
        return typeof value === 'string' || value instanceof String;
    }

    static isNumber(value) {
        return typeof value === 'number' && isFinite(value);
    }

    static isArray(value) {
        return value && typeof value === 'object' && value.constructor === Array;
    }

    static isFunction(value) {
        return typeof value === 'function';
    }

    static isObject(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }

    static intReverseSort(arr) {
        return arr.map(x => x.toString().padStart(17, '0')).sort().reverse().map(x => parseInt(x));
    }

    static floor(v) {
        return Math.floor(v)
    }

    static customFloorFloat(number, percision = 11) {
        const per = Math.pow(10, percision);
        return Math.trunc(number * per) / per;
    }

    static iFloorFloat(number) {
        return this.customFloorFloat(number, 11);
    }

    static isAValidEmailFormat(email) {
        // let expression1 = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/gmi
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    static isAnURL(url) {
        let regex;

        let expressions = [
            /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
            /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gmi
        ];

        let isURL = false;
        for (let exp of expressions) {
            let regex = new RegExp(exp);
            if (url.match(regex)) {
                isURL = true;
            }
        }
        return isURL;
    }

    /**
     * 
     * @param {*} number 
     * PAIs are always Integer, but their value is micro.
     * it means every presentation of PAIs must be in floatingpoint with 3 digit at the right side and drop another 3 digit to be more readable.
     * 1000000 microPAI = 1 PAI and presented for user like 1.000 PAI
     */
    static microPAIToPAI(microPAI) {
        microPAI = Math.floor(microPAI);
        let PAI = Math.floor(microPAI / 1000000);
        return this.sepNum(PAI);
    }

    static microPAIToPAI3(microPAI) {
        microPAI = Math.floor(microPAI).toString();
        let negativeSign = '';
        if (microPAI.substring(0, 1) == '-') {
            if (microPAI.toString().length > 4)
                negativeSign = '-';
            microPAI = microPAI.substring(1);
        }
        microPAI = microPAI.toString().padStart(7, '0');
        return negativeSign + [this.sepNum(microPAI.substr(0, microPAI.length - 6)), '.', microPAI.substr(-6, 3)].join('');
    }

    static microPAIToPAI6(microPAI) {
        microPAI = Math.floor(microPAI).toString();
        let negativeSign = '';
        if (microPAI.substring(0, 1) == '-') {
            negativeSign = '-';
            microPAI = microPAI.substring(1);
        }
        if (microPAI.length < 8) {
            microPAI = microPAI.toString().padStart(7, '0');
        }
        return negativeSign + [this.sepNum(microPAI.substr(0, microPAI.length - 6)), '.', microPAI.substr(-6, 6)].join('');
    }

    static normalizeSpaces(s) {
        if (this._nilEmptyFalse(s))
            return '';

        s = s.trim();
        while (s.includes("  "))
            s = s.replace('  ', ' ');
        return s;
    }

    static shortenLog(str) {
        let MAX_LOG_LENGTH = 100000;
        return str.substr(0, MAX_LOG_LENGTH);
    }

    static _nil(inp) {
        if (inp === null || inp === undefined || inp == null || inp == undefined)
            return true
        return false
    }

    static _notNil(inp) {
        return !this._nil(inp)
    }

    //TODO: NaN does not work properly
    static _NaN(inp) {
        if (inp === NaN || inp == NaN)
            return true
        return false
    }

    static _emptyArray(inp) {
        if (Array.isArray(inp) && (inp.length == 0))
            return true;
        if (Array.isArray(inp) && (inp.length > 0))
            return false;
        return null; // TIXME: tobe considered
    }

    static _empty(inp) {
        // even empty array is not empty
        if (Array.isArray(inp))
            return false;

        if (!this._nil(inp) && (inp === '' || inp == '')) {
            // to fix a bug in js in which false is equal '' 
            if (this._nil(inp.length)) {
                return false
            }
            return true
        }
        return false
    }

    //TODO: _notEmpty does not work properly
    static _notEmpty(inp) {
        return !this._empty(inp)
    }

    static _false(inp) {
        // even empty array is not false
        if (Array.isArray(inp))
            return false;

        if (!this._nil(inp) && ((inp === false) || (inp == false)))
            return true
        return false
    }

    static _true(inp) {
        if (!this._nil(inp) && (inp === true || inp == true))
            return true
        return false
    }

    static _nilEmptyFalse(inp) {
        if (this._nil(inp))
            return true

        if (this._empty(inp))
            return true

        if (this._false(inp))
            return true

        return false
    }

    static isItExpired(creationDate, ttlByMinutes) {
        try {
            creationDate = creationDate.getTime()
        } catch (e) { }
        expTime = new Date(require('../startup/singleton').instance.getMoment().subtract({ 'hours': 0, 'minutes': ttlByMinutes })).getTime();
        // clog.app.info(`creationDate: ${creationDate}`);
        // clog.app.info(`expTime: ${expTime}`); //.getTime()
        if (creationDate < expTime)
            return true
        return false
    }

    static objPresenter(o) {
        _.forOwn(o, (val, key) => {
            clog.app.info(key, val);
        });
    }

    static sepNum(s) {
        return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    static stripNonAlphaNumeric(s) {
        return s.replace(/[^0-9a-zA-Z]/gi, '');
    }

    static isValidHash(s) {
        if (this.stripNonHex(s) != s)
            return false;
        // TODO add some more control such as length control,...
        return true;
    }

    static stripNonHex(s) {
        return s.replace(/[^0-9a-fA-F]/gi, '');
    }

    static stripNonInDateString(s) {
        return s.replace(/[^0-9\- :]/g, '');
    }

    static stripNonNumerics(s) {
        return s.replace(/\D/g, '');
    }

    static stripNonAscii(s) {
        return s.replace(/[^\x00-\x7F]/g, "");
    }

    static arrayUnique(a) {
        a = Array.from(new Set(a));
        return a;
    }

    static removeFromArray(arr, value) {
        return _.remove(arr, function (n) {
            return n != value;
        });
    }

    static nativeJSDateStr() {
        let t = new Date().toISOString().slice(0, 19);
        t = t.replace('T', ' ');
        return t;
    }

    static timeDiff(fromT, toT = null) {
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        if (this._nilEmptyFalse(toT))
            toT = this.getNow();
        let diff = Moment(toT).diff(Moment(fromT));
        let d = Moment.duration(diff);
        if (!d._isValid) {
            return (new Error('Invalid time difference'));
        }
        return {
            'years': Math.floor(d.years()),
            'asYears': Math.floor(d.asYears()),
            'months': Math.floor(d.months()),
            'asMonths': Math.floor(d.asMonths()),
            'days': Math.floor(d.days()),
            'asDays': Math.floor(d.asDays()),
            'hours': Math.floor(d.hours()),
            'asHours': Math.floor(d.asHours()),
            'minutes': Math.floor(d.minutes()),
            'asMinutes': Math.floor(d.asMinutes()),
            'seconds': Math.floor(d.seconds()),
            'asSeconds': Math.floor(d.asSeconds()),
        }
    }

    static objLength(o) {
        return _.size(o);
    }

    static queryHas(query, value) {
        for (let aQ of query)
            if (aQ[0] == value)
                return true;
        return false;
    }

    static objKeys(o) {
        return Object.keys(o);
    }

    // TODO: unittests for packCommaSeperated, unpackCommaSeperated, normalizeCommaSeperatedStr, removeNullMembersFromCommaSeperated
    static unpackCommaSeperated(str) {
        str = this.normalizeCommaSeperatedStr(str)
        let arr = str.split(',');
        if (!Array.isArray(arr))
            arr = [arr];
        return this.removeNullMembersFromCommaSeperated(arr);
    }

    static packCommaSeperated(arr) {
        arr = this.removeNullMembersFromCommaSeperated(arr);
        if (arr.length == 0)
            return '';
        if (arr.length == 1)
            return arr[0];
        return arr.join(',');
    }

    static uniqueCommaSeperatedStr(str) {
        return this.packCommaSeperated(this.arrayUnique(this.unpackCommaSeperated(str)))
    }

    static removeNullMembersFromCommaSeperated(arr) {
        let newArr = []
        arr.forEach(element => {
            if (!this._nilEmptyFalse(element))
                newArr.push(element);
        });
        return newArr;
    }

    static normalizeCommaSeperatedStr(str) {
        if (this._nilEmptyFalse(str) || (str === undefined))
            return '';

        str = str.toString("utf8");

        str = str.replace(',,', ',');
        if (str.endsWith(",")) {
            str = str.substring(0, str.length - 1);
        }
        if (str.startsWith(",")) {
            str = str.substring(1);
        }
        return str;
        // prerequisites = prerequisites.replaceAll(",$", "");
    }

    static arrayRemoveByIndex(arr, inx) {
        let tmp = _.clone(arr)
        tmp.splice(inx, 1);
        return tmp;
    }

    static arrayRemoveByValue(arr, value) {

        return arr.filter(function (ele) {
            return ele != value;
        });

    }

    /**
     * 
     * @param {superset}  
     * @param {subset}  
     * output = superset - subset
     * 
     */
    static arrayDiff(superset, subset) {
        let reminedValues = [];
        superset.forEach(element => {
            if (subset.indexOf(element) == -1)
                reminedValues.push(element);
        });
        return reminedValues;
    }

    static arrayAdd(arr1, arr2) {
        return arr1.concat(arr2);
    }

    static isGreaterThanNow(cDate) {
        if (!getMoment) {
            getMoment = require('../startup/singleton').instance.getMoment;
        }

        if (cDate > getMoment().format('YYYY-MM-DD HH:mm:ss')) {
            return true;
        }
        return false;

    }

    /**
     * @param {*} accept 'backInTimesByMinutes' and goes back in time by minutes
     * @param {*} t it is start time to going back from which
     */
    static minutesBefore(backInTimesByMinutes, cDate = null) {
        if (cDate == null) {
            if (!getMoment) {
                getMoment = require('../startup/singleton').instance.getMoment;
            }
            cDate = getMoment().format('YYYY-MM-DD HH:mm:ss');
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        return Moment(cDate, "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': backInTimesByMinutes }).format("YYYY-MM-DD HH:mm:ss");
    }

    static yearsBefore(backInTimesByYears, cDate = null) {
        if (cDate == null) {
            if (!getMoment) {
                getMoment = require('../startup/singleton').instance.getMoment;
            }
            cDate = getMoment().format('YYYY-MM-DD HH:mm:ss');
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        return Moment(cDate, "YYYY-MM-DD HH:mm:ss").subtract({ 'years': backInTimesByYears }).format("YYYY-MM-DD HH:mm:ss");
    }

    static minutesAfter(forwardInTimesByMinutes, cDate = null) {
        if (cDate == null) {
            if (!getMoment) {
                getMoment = require('../startup/singleton').instance.getMoment;
            }
            t = getMoment().format('YYYY-MM-DD HH:mm:ss');
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        return Moment(cDate, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': forwardInTimesByMinutes }).format("YYYY-MM-DD HH:mm:ss");
    }

    static secondsAfter(forwardInTimesBySeconds, cDate = null) {
        if (cDate == null) {
            if (!getMoment) {
                getMoment = require('../startup/singleton').instance.getMoment;
            }
            t = getMoment().format('YYYY-MM-DD HH:mm:ss');
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        return Moment(cDate, "YYYY-MM-DD HH:mm:ss").add({ 'seconds': forwardInTimesBySeconds }).format("YYYY-MM-DD HH:mm:ss");
    }

    static yearsAfter(forwardInTimesByYears, cDate = null) {
        if (cDate == null) {
            if (!getMoment) {
                getMoment = require('../startup/singleton').instance.getMoment;
            }
            t = getMoment().format('YYYY-MM-DD HH:mm:ss');
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        return Moment(cDate, "YYYY-MM-DD HH:mm:ss").add({ 'years': forwardInTimesByYears }).format("YYYY-MM-DD HH:mm:ss");
    }

    static getNow() {
        if (!getMoment) {
            getMoment = require('../startup/singleton').instance.getMoment;
        }
        return getMoment().format('YYYY-MM-DD HH:mm:ss');   // all dates in imaginge must following ISO 8601 and timezone greenwich
    }

    /**
     * supports miliseconds
     */
    static getNowSSS() {
        if (!getMoment) {
            getMoment = require('../startup/singleton').instance.getMoment;
        }
        return getMoment().format('YYYY-MM-DD HH:mm:ss.SSS')
    }

    static getCurrentYear() {
        return this.getNow().split('-')[0];
    }

    static exitIfGreaterThanNow(t) {
        if (this.isGreaterThanNow(t)) {
            clog.app.info(`time can not be the future! ${t}`);
            console.log(`time can not be the future! ${t}`);
            process.exit(5555);
        }
    }

    static sleepBySecond(seconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > seconds * 1000) {
                break;
            }
        }
    }

    // accepts a dictionary and return an array of values, sorted by dict key
    static orderbyKey(dict) {
        let sortedValues = [];
        this.objKeys(dict).sort().forEach(element => {
            sortedValues.push(dict[element]);
        });
        return sortedValues;
    }

    static hash6c(hash) {
        if (hash == undefined)
            return 'undefined6';
        return hash.substr(0, 6);
    }

    static hash8c(hash) {
        if (hash == undefined)
            return 'undefined8';
        return hash.substr(0, 8);
    }

    static hash16c(hash) {
        if (hash == undefined)
            return 'undefined16';
        return hash.substr(0, 16);
    }

    static hash32c(hash) {
        if (hash == undefined)
            return 'undefined32';
        return hash.substr(0, 32);
    }

    static hash64c(hash) {
        if (hash == undefined)
            return 'undefined64';
        return hash.substr(0, 64);
    }



};

utils.heapPermutation = require('./heap-permutation');

module.exports = utils
