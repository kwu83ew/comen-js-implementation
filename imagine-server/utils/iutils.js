const _ = require('lodash');
const utils = require('./utils');
const iConsts = require('../config/constants');
const listener = require('../plugin-handler/plugin-handler');

let DNAHandler = null;
let Moment = false; // TODO: fix this priority includings
let getMoment = false; // TODO: fix this priority includings

let clog = null;
let machine = null;

class IUtils {

    static getMinLongevity() {
        return (iConsts.getCycleByMinutes() * 2) / 60;
    }

    static getCoinbaseCheckGap(cDate = null) {
        let gapByMinutes;
        if (cDate == null)
            cDate = utils.getNow();

        if (machine == null)
            machine = require('../machine/machine-handler');

        if (machine.isInSyncProcess()) {
            gapByMinutes = iConsts.getCycleByMinutes() / 6;

        } else {
            gapByMinutes = iConsts.getCycleByMinutes() / 24;

            if (iConsts.TIME_GAIN == 1) {
                let hour = parseInt(cDate.split(' ')[1].split(':')[0]);
                if (hour >= 12)
                    hour -= 12;

                if ((0 <= hour) && (hour < 3)) {
                    gapByMinutes = 30;
                } else if ((3 <= hour) && (hour < 6)) {
                    gapByMinutes = 60;
                } else if ((6 <= hour) && (hour < 10)) {
                    gapByMinutes = 120;
                } else if ((10 <= hour) && (hour < 11)) {
                    gapByMinutes = 60;
                } else if (11 <= hour) {
                    gapByMinutes = 30;
                }

            }
        }
        console.log(`gapByMinutes ${gapByMinutes}`);
        return gapByMinutes;
    }

    static getICachePath(args = {}) {
        const db = require('../startup/db2');
        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let port = iConsts.DEFAULT_PORT_NUMBER;
        if (appCloneId > 0)
            port = port + appCloneId;
        return `http://localhost:${port}/api/icache`;
    }

    static isValidVersionNumber(ver) {
        if (utils._nilEmptyFalse(ver))
            return false;

        let verDtl = ver.split('.');
        if (verDtl.length != 3)
            return false;

        for (let aSeg of verDtl) {
            if (parseInt(aSeg) != aSeg)
                return false;
        }

        return true;
    }

    static isValidLanguageCode(lang) {
        if (utils._nilEmptyFalse(lang))
            return false;

        if (lang.length != 3)
            return false;

        return true;
    }

    /**
     * The documents which do not need another doc to pay their cost.
     * instead they can pay the cost of another docs
     * 
     * @param {} dType 
     */
    static isNoNeedCostPayerDoc(dType) {
        return ([
            iConsts.DOC_TYPES.BasicTx,
            iConsts.DOC_TYPES.DPCostPay,
            iConsts.DOC_TYPES.RpDoc,
            iConsts.DOC_TYPES.RlDoc
        ].includes(dType))
    }

    static canBeACostPayerDoc(dType) {
        return [iConsts.DOC_TYPES.BasicTx].includes(dType)
    }

    static isBasicTransaction(dType) {
        // Note: the iConsts.DOC_TYPES.Coinbase  and iConsts.DOC_TYPES.DPCostPay altough are transactions, but have special tretmnent
        // Note: the iConsts.DOC_TYPES.RpDoc altough is a transaction, but since is created directly by node and based on validated coinbase info, so does not need validate
        return [iConsts.DOC_TYPES.BasicTx].includes(dType);
    }

    static isCoinbase(dType) {
        return [iConsts.DOC_TYPES.Coinbase].includes(dType);
    }

    static isDPCostPayment(dType) {
        return [iConsts.DOC_TYPES.DPCostPay].includes(dType);
    }

    static trxHasInput(dType) {
        return [
            iConsts.DOC_TYPES.BasicTx,
            iConsts.DOC_TYPES.RpDoc
        ].includes(dType);
    }

    static trxHasNotInput(dType) {
        return [
            iConsts.DOC_TYPES.Coinbase,
            iConsts.DOC_TYPES.DPCostPay,
            iConsts.DOC_TYPES.RlDoc
        ].includes(dType);
    }

    static trxHasOutput(dType) {
        return [
            iConsts.DOC_TYPES.Coinbase,
            iConsts.DOC_TYPES.BasicTx,
            iConsts.DOC_TYPES.RpDoc
        ].includes(dType);
    }


    static offsettingLength(len, pad = 7) {
        return len.toString().padStart(pad, '0');
    }

    static paddingDocLength(len, pad = 7) {
        return len.toString().padStart(pad, '0');
    }

    static convertBigIntToJSInt(inp) {
        return parseInt(inp);
        // return inp; // after pg.types.setTypeParser FIX, no more needed manually type casting for pg. but node-pg still nedded this mapping
    }

    static converStringToJSInt(inp) {
        return parseInt(inp);
    }

    static getMatureCyclesCount(dType) {
        if (clog == null)
            clog = require('../loggers/console_logger');

        let matureCycles = 1;
        clog.app.info(`dTypeeeee ${dType}`);
        switch (dType) {
            case iConsts.DOC_TYPES.BasicTx:
            case iConsts.DOC_TYPES.DPCostPay:
                matureCycles = 1;
                break;

            case iConsts.DOC_TYPES.Coinbase:
            case iConsts.DOC_TYPES.RpDoc:
            case iConsts.DOC_TYPES.RlDoc:
                matureCycles = 2;
                break;

            default:
                msg = `${level}.SCUUCM, block(${utils.hash6c(refBlock.blockHash)} and doc(${utils.hash6c(refDoc.hash)} has strange doc-type${refDoc.dType}) `;
                clog.sec.error(msg);
                return { err: true, msg }
                break;
        }
        return { err: false, number: matureCycles }
    }

    static isMatured(args) {
        let coinCreationDate = args.coinCreationDate;
        let docType = args.docType;
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        let matureCycles = this.getMatureCyclesCount(docType)
        if (matureCycles.err != false) {
            matureCycles.matured = false;
            return matureCycles;
        }
        matureCycles = matureCycles.number;

        if (matureCycles > 0) {
            // control maturity
            if (
                // (spendBlock.bType != iConsts.BLOCK_TYPES.RpBlock) &&
                (utils.timeDiff(coinCreationDate, cDate).asMinutes < (matureCycles * iConsts.getCycleByMinutes()))
            ) {
                msg = `${level}.is Matured: block(${utils.hash6c(spendBlock.blockHash)} ${cDate}) `;
                msg += `uses an output(${refDoc.dType} ${utils.hash6c(refDoc.hash)})  ${coinCreationDate}) `;
                msg += `before being maturated by ${matureCycles} cycles`;
                clog.sec.error(msg);
                return { err: true, matured: false, msg }
            }
        }
        return { err: false, matured: true }

        // if (!getMoment) {
        //     getMoment = require("../startup/singleton").instance.getMoment;
        // }

        // if (!Moment) {
        //     Moment = require("../startup/singleton").instance.Moment;
        // }

        // if (clog == null)
        //     clog = require('../loggers/console_logger');

        // let range = null;
        // switch (docType) {
        //     case iConsts.DOC_TYPES.BasicTx:
        //     case iConsts.DOC_TYPES.DPCostPay:
        //         if (cDate == null)
        //             cDate = getMoment().format('YYYY-MM-DD HH:mm:ss');
        //         range = {
        //             maxCreationDate: Moment(cDate, "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss")
        //         };
        //         break;

        //     case iConsts.DOC_TYPES.Coinbase:
        //         range = this.getCbUTXOsDateRange(cDate);
        //         break;

        //     default:
        //         clog.app.error(`is Matured is called by undefined type! (${docType})`);
        //         console.log(`is Matured is called by undefined type! (${docType})`);
        //         process.exit(345);


        // }
        // if (range == null)
        //     return false;
        // return (refBlockCreationDate < range.maxCreationDate);
    }

    static packCoinRef(refTrxHash, outputIndex) {
        return [refTrxHash, outputIndex.toString()].join(':');
    }

    static unpackCoinRef(refLoc) {
        let segments = refLoc.split(':');
        return {
            docHash: segments[0],
            outputIndex: segments[1],
        }
    }

    static shortCoinRef(refLoc) {
        return [
            utils.hash6c(refLoc.split(':')[0]),
            refLoc.split(':')[1]
        ].join(':')
    }

    static shortCoinRef16(refLoc) {
        return [
            utils.hash16c(refLoc.split(':')[0]),
            refLoc.split(':')[1]
        ].join(':')
    }

    static packCoinSpendLoc(blockHash, trxHash) {
        return [blockHash, trxHash].join(':');
    }

    static unpackCoinSpendLoc(refLoc) {
        let segments = refLoc.split(':');
        return {
            blockHash: segments[0],
            docHash: segments[1],
        }
    }

    static shortBech(bech32) {
        return [bech32.substr(0, 3), bech32.substr(-4)].join('.')
    }

    static shortBech6(bech32) {
        return [bech32.substr(0, 3), bech32.substr(-6)].join('.')
    }

    static shortBech8(bech32) {
        return [bech32.substr(0, 3), bech32.substr(-8)].join('.')
    }

    static shortBech16(bech32) {
        return [bech32.substr(0, 3), bech32.substr(-16)].join('.')
    }

    /**
     * 
     * @param {*} cDate reference date 
     * @param {*} backByCycle is a number to indicate how old the cycle before you need
     */
    static getACycleRange(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let backByCycle = _.has(args, 'backByCycle') ? args.backByCycle : 0;
        let forwardByCycle = _.has(args, 'forwardByCycle') ? args.forwardByCycle : 0;
        if (utils._nilEmptyFalse(cDate))
            cDate = utils.getNow();

        if (iConsts.TIME_GAIN == 1) {
            // one extra step to resolve +- summer time
            let h = parseInt(cDate.split(' ')[1].split(':')[0]);
            if (h >= 12) {
                h = '18:00:00';
            } else {
                h = '06:00:00';
            }
            cDate = `${cDate.split(' ')[0]} ${h}`
        }
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }

        let minCreationDate;
        if (forwardByCycle == 0) {
            minCreationDate = Moment(cDate, "YYYY-MM-DD HH:mm:ss").subtract({
                'minutes': backByCycle * iConsts.getCycleByMinutes()
            });
        } else {
            minCreationDate = Moment(cDate, "YYYY-MM-DD HH:mm:ss").add({
                'minutes': forwardByCycle * iConsts.getCycleByMinutes()
            });
        }

        let day = minCreationDate.format("YYYY-MM-DD");
        let minutes = minCreationDate.format("HH:mm").split(':');
        minutes = (parseInt(minutes[0]) * 60) + parseInt(minutes[1]);
        let startMinute = Math.floor(minutes / iConsts.getCycleByMinutes()) * iConsts.getCycleByMinutes();
        let endMinute = startMinute + iConsts.getCycleByMinutes() - 1;
        return {
            minCreationDate: day + ' ' + IUtils.convertMinutesToHHMM(startMinute) + ':00',
            maxCreationDate: day + ' ' + IUtils.convertMinutesToHHMM(endMinute) + ':59'
        };

    }

    static getSelectedMProfile() {
        const model = require('../models/interface');
        let res = model.sRead({
            table: 'i_kvalue',
            query: [['kv_key', 'selected_profile']]
        });
        // console.log('resresresresres', res);
        if (res.length != 1) {
            console.log('NoooOOOOOOOOOOOOOOooooooooooooo profile!');
            process.exit(345)
        }
        return res[0].kv_value;
    }

    static getCbUTXOsDateRange(cDate = null) {
        return this.getACycleRange({ cDate, backByCycle: iConsts.COINBASE_MATURATION_CYCLES });
    }

    static getNetworkSignals() {
    }

    static getMachineSignals() {
        let signals = [{ sig: 'clientVersion', ver: iConsts.CLIENT_VERSION }];

        signals = listener.doCallSync('SASH_signals', { signals });
        // console.log(`signals ${utils.stringify(signals)}`);
        if (Array.isArray(signals.signals))
            return utils.arrayUnique(signals.signals).sort();
        return [];
    }

    static mapDocCodeToDocType(code) {
        if (!_.has(IUtils, 'docCodeDict')) {
            IUtils.docCodeDict = {};
            _.forOwn(iConsts.DOC_TYPES, (val, key) => {
                IUtils.docCodeDict[val] = key;
            });
        }
        return IUtils.docCodeDict[code];
    }

    static normalizeCBSignatures(sigs1, sigs2) {
        // [[signature1, publickey1], [signature2, publickey2]] ordered by signature

        if (utils._nilEmptyFalse(sigs1))
            sigs1 = []
        if (!Array.isArray(sigs1))
            sigs1 = [sigs1]

        if (utils._nilEmptyFalse(sigs2))
            sigs2 = []
        if (!Array.isArray(sigs2))
            sigs2 = [sigs1]

        let tmpDict = {}
        sigs1.forEach(sig => {
            tmpDict[sig[0]] = [sig[0], sig[1], sig[2]]
        });
        sigs2.forEach(sig => {
            tmpDict[sig[0]] = [sig[0], sig[1], sig[2]]
        });
        let res = []
        utils.objKeys(tmpDict).sort().forEach(k => {
            res.push([tmpDict[k][0], tmpDict[k][1], tmpDict[k][2]])
        });
        return res;
    }

    // it seems we do not need the big number module at all
    static calcPotentialMicroPaiPerOneCycle(year = null) {
        if (year == null)
            year = this.getCurrentYear();

        year = parseInt(year);
        let halvingCycleNumber = Math.floor((year - iConsts.LAUNCH_YEAR) / iConsts.HALVING_PERIOD);
        let oneCycleMaxCoins = Math.pow(2, (iConsts.COIN_ISSUING_INIT_EXPONENT - halvingCycleNumber));
        return oneCycleMaxCoins;
    }

    static calculateReleasableCoinsBasedOnContributesVolume(sumShares) {
        let contributes = Math.floor((sumShares * 100) / iConsts.WORLD_POPULATION);
        // console.log('sumShares', sumShares);
        // console.log('contributes', contributes);
        let releseablePercentage = 1;
        for (let target of utils.objKeys(iConsts.MAP_CONTRIBUTE_AMOUNT_TO_MINTING_PERCENTAGE)) {
            if (contributes >= target) {
                releseablePercentage = iConsts.MAP_CONTRIBUTE_AMOUNT_TO_MINTING_PERCENTAGE[target]
            }
        }
        if (releseablePercentage > 100)
            releseablePercentage = 100;
        return releseablePercentage;
    }

    static calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore(args) {
        let calculateDate = (_.has(args, 'calculateDate')) ? args.calculateDate : null;

        if (calculateDate == null)
            calculateDate = utils.getNow();

        if (DNAHandler == null)
            DNAHandler = require('../dna/dna-handler');

        let oneCycleMaxCoins = this.calcPotentialMicroPaiPerOneCycle(calculateDate.split('-')[0]);
        let { sumShares, holdersByKey } = DNAHandler.getSharesInfo(calculateDate);

        let releseablePercentage = this.calculateReleasableCoinsBasedOnContributesVolume(sumShares) / 100;
        let oneCycleIssued = Math.floor(releseablePercentage * oneCycleMaxCoins);
        return { oneCycleMaxCoins, oneCycleIssued, sumShares, holdersByKey };
    }

    static predictReleaseableMicroPAIsPerOneCycle(args) {
        let cCDate = (_.has(args, 'cCDate')) ? args.cCDate : null;    //contribute creation date
        let prevDue = (_.has(args, 'prevDue')) ? args.prevDue : null;    //prevDue date
        let due = (_.has(args, 'due')) ? args.due : null;    //due date
        let annualContributeGrowthRate = (_.has(args, 'annualContributeGrowthRate')) ? args.annualContributeGrowthRate : 0;
        let currentTotalSahres = (_.has(args, 'currentTotalSahres')) ? args.currentTotalSahres : null;

        if (cCDate == null)
            cCDate = utils.getNow();
        if (due == null)
            due = utils.getNow();

        if (cCDate > utils.getNow()) {
            console.log(`for now the formule does not support future contribute calculations. TODO: implement it`);
            return 0
        }

        let oneCycleIssued, sumShares;


        let definiteReleaseable = this.calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore({
            calculateDate: cCDate
        });
        oneCycleIssued = definiteReleaseable.oneCycleIssued

        if (currentTotalSahres != null) {
            sumShares = currentTotalSahres;
        } else {
            sumShares = definiteReleaseable.sumShares
        }

        if (due > prevDue) {
            // add potentially contributes in next days
            let futureDays = utils.timeDiff(prevDue, due).asDays;
            let newShares = sumShares * (((annualContributeGrowthRate) / 100) / 365) * futureDays;
            sumShares += newShares;
            let releseablePercentage = this.calculateReleasableCoinsBasedOnContributesVolume(sumShares) / 100;
            oneCycleIssued = Math.floor(releseablePercentage * definiteReleaseable.oneCycleMaxCoins);
        }


        let res = {
            oneCycleMaxCoins: definiteReleaseable.oneCycleMaxCoins,
            oneCycleIssued,
            sumShares
        };
        return res;
    }

    /**
     * 
     * @param {*} args 
     * return approxmatly incomes in next n years, based on your contribution and epotesic contributions growth
     */
    static predictFutureIncomes(args) {
        if (clog == null)
            clog = require('../loggers/console_logger');
        clog.app.info(`predictFutureIncomes args ${utils.stringify(args)}`);

        let months = (_.has(args, 'months')) ? args.months : 7 * 12;    // future months
        let cCDate = (_.has(args, 'cCDate')) ? args.cCDate : null;    // contribute creation date (start date)
        let theContribute = (_.has(args, 'theContribute')) ? args.theContribute : 1;   // contributeHours * contributeLevel
        let annualContributeGrowthRate = (_.has(args, 'annualContributeGrowthRate')) ? args.annualContributeGrowthRate : 100;
        let currentTotalSahres = (_.has(args, 'currentTotalSahres')) ? args.currentTotalSahres : null;

        if (cCDate == null)
            cCDate = utils.getNow();

        let totalIncomes = 0;
        let monthlyIncomes = [];
        let oneCycleIncome, incomePerMonth, due;
        let prevDue = cCDate;
        for (let month = 0; month < months; month++) {
            due = utils.minutesAfter((60 * 24) * (365 / 12) * month, cCDate);   //TODO change it to more accurate on month starting day
            // due = utils.minutesAfter((60 * 24) * (365/12) , prevDue);   //TODO change it to more accurate on month starting day

            let { oneCycleMaxCoins, oneCycleIssued, sumShares } = this.predictReleaseableMicroPAIsPerOneCycle({
                annualContributeGrowthRate: annualContributeGrowthRate,
                currentTotalSahres,
                cCDate,
                prevDue,
                due
            });
            oneCycleIncome = Math.floor(oneCycleIssued * (theContribute / sumShares));  // one cycle income
            incomePerMonth = Math.floor(oneCycleIssued * (theContribute / sumShares) * (2 * 30));  // one month income almost = one cycle income * 2 perDay * 30 perMonth
            clog.app.info(`predictFutureIncomes sumShares ${utils.stringify(sumShares)}`);

            monthlyIncomes.push({
                oneCycleMaxCoins,
                oneCycleIssued,
                oneCycleIncome,
                due: due.split(' ')[0],
                incomePerMonth,
                sumShares: Math.floor(sumShares)
            });
            totalIncomes += incomePerMonth;

            prevDue = due;
            currentTotalSahres = sumShares
        }
        // console.log(monthlyIncomes);
        let res = {
            definiteIncomes: totalIncomes,
            reserveIncomes: totalIncomes * 3,   // 1 block released immidiately wheras 3 copy of that will be releaseable in next 3 months by voting of shareholders of block on creation time of block
            treasuryIncomes: Math.floor(totalIncomes / 1000000),  //TODO treasuryIncomes should be calcullated in a more smart way :)
            monthlyIncomes,
            firstIncomeDate: cCDate,
            lastIncomeDate: due
        };
        // console.log(res);
        return res;
    }

    static getRegisteredINames(args) {
        const crypto = require("../crypto/crypto");
        const flensHandler = require('../contracts/flens-contract/flens-handler');
        let res = flensHandler.register.searchRegisteredINames({});
        // console.log('ressss ', res);
        if (res.err != false)
            return res;

        let newRes = [];
        for (let aRes of res.records) {
            aRes.ownerColor = `#${utils.hash6c(crypto.keccak256(aRes.inOwner))}`;
            newRes.push(aRes);
        }
        return newRes;
    }

    static getReservesDetails(args) {
        const reservedHandler = require('../dag/coinbase/reserved-coins-handler');
        return reservedHandler.getReservesDetails();
    }

    static predictFutureCoinIssuance(args) {
        let years = (_.has(args, 'years')) ? args.years : 20;    // future years
        let startFromDate = (_.has(args, 'startFromDate')) ? args.startFromDate : null;    // contribute creation date (start date)
        let annualContributeGrowthRate = (_.has(args, 'annualContributeGrowthRate')) ? args.annualContributeGrowthRate : 100;
        let currentTotalSahres = (_.has(args, 'currentTotalSahres')) ? args.currentTotalSahres : null;

        if (startFromDate == null)
            startFromDate = utils.getNow();

        let monthlyIssuanceInfo = [];
        let due;
        let prevDue = startFromDate;
        for (let month = 0; month < 12 * years; month++) {
            due = utils.minutesAfter((60 * 24) * (365 / 12) * month, startFromDate);   //TODO change it to more accurate on month starting day
            // due = utils.minutesAfter((60 * 24) * (365/12) , prevDue);   //TODO change it to more accurate on month starting day

            let { oneCycleMaxCoins, oneCycleIssued, sumShares } = this.predictReleaseableMicroPAIsPerOneCycle({
                annualContributeGrowthRate: annualContributeGrowthRate,
                currentTotalSahres,
                cCDate: startFromDate,
                prevDue,
                due
            });

            monthlyIssuanceInfo.push({
                oneCycleMaxCoins,
                oneCycleIssued,
                due: due.split(' ')[0],
                sumShares: Math.floor(sumShares)
            });

            prevDue = due;
            currentTotalSahres = sumShares
        }
        return monthlyIssuanceInfo;
    }

    static calcMicroPAIFullIssuance(year) {
        let issuancePer12Hour = this.calcPotentialMicroPaiPerOneCycle(year);
        let reserves = {};
        let reservesSumPerYear = 0;
        let totalYear = issuancePer12Hour * 2 * 365;
        for (let reserve of [1, 2, 3]) {
            if (year - reserve >= iConsts.LAUNCH_YEAR) {
                let vote = (100 - (reserve * 25));
                let resmicroPAIPer12Hour = Math.floor(this.calcPotentialMicroPaiPerOneCycle(year - reserve));
                reservesSumPerYear += (resmicroPAIPer12Hour * 2 * 365);
                let becauseOfIssuedIn = (year - reserve).toString();
                reserves[becauseOfIssuedIn] = {
                    reservePer12Hour: resmicroPAIPer12Hour,
                    reservePerDay: resmicroPAIPer12Hour * 2,
                    reservePerYear: resmicroPAIPer12Hour * 2 * 365,
                    vote: vote
                }
            }
        }
        totalYear += reservesSumPerYear;
        return {
            cycle: Math.floor((year - iConsts.LAUNCH_YEAR) / iConsts.HALVING_PERIOD) + 1,
            year,
            issuancePer12Hour,
            issuancePerDay: issuancePer12Hour * 2,
            issuancePerYear: issuancePer12Hour * 2 * 365,
            reserves,
            reservesSumPerYear,
            totalYear
        }
    }

    // static getTarget(t_ = null) {
    //     if (t_ == null)
    //         t_ = utils.getNow();
    //     // return '0';//t_.split('-')[0].substr(2, 2), 
    //     return [t_.split('-')[0].substr(2, 2), t_.split('-')[1], t_.split('-')[2].split(' ')[0]].join('');//t_.split('-')[0].substr(2, 2), 
    // }

    static isAmOrPm(minutes) {
        if (minutes >= 720)
            return '12:00:00';
        return '00:00:00';

    }

    static getPrevCoinbaseInfo(cDate = null) {
        // if (!Moment) {
        //     Moment = require("../startup/singleton").instance.Moment;
        // }
        // let prevCycle = Moment(IUtils.getCoinbaseRange(cDate).from, "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': 1 }).format("YYYY-MM-DD HH:mm:ss");
        let prevCycle = IUtils.getACycleRange({ cDate, backByCycle: 1 }).minCreationDate;

        return IUtils.getCoinbaseInfo({ cDate: prevCycle });
    }

    static getCoinbaseInfo(args = {}) {
        let out = {};
        if (_.has(args, 'cDate')) {
            let cDate = args.cDate;
            out.cycleStamp = IUtils.getCoinbaseCycleStamp(cDate)
            let { from, to } = IUtils.getCoinbaseRange(cDate);
            out.from = from;
            out.fromHour = from.split(' ')[1];
            out.to = to;
            out.toHour = to.split(' ')[1];

        } else if (_.has(args, 'cycle')) {
            let _cycle = args.cycle; // cycle stamp like '2011-02-26 am'
            out.cycleStamp = _cycle;
            let { from, to } = IUtils.getCoinbaseRangeByCycleStamp(_cycle);
            out.from = from;
            out.fromHour = from.split(' ')[1];
            out.to = to;
            out.toHour = to.split(' ')[1];

        }
        return out;
    }

    static getCoinbaseCycleStamp(cDate = null) {
        if (!getMoment) {
            getMoment = require("../startup/singleton").instance.getMoment;
        }

        let day;
        if (cDate === null) {
            day = getMoment().format('YYYY-MM-DD');
        } else {
            day = cDate.split(' ')[0];
        }
        if (iConsts.TIME_GAIN == 1) {
            return (day + ' ' + IUtils.getCoinbaseCycleNumber(cDate));

        } else {
            return (day + ' ' + _.padStart(IUtils.getCoinbaseCycleNumber(cDate), 3, '0').toString());

        }
    }

    static getCoinbaseRangeByCycleStamp(cycle) {
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        let cycleDtl = cycle.split(' ');
        if (cycleDtl[1] == '00:00:00') {
            let from = cycleDtl[0] + ' 00:00:00';
            let to = cycleDtl[0] + ' 11:59:59';
            return { from, to }

        } else if (cycleDtl[1] == '12:00:00') {
            let from = cycleDtl[0] + ' 12:00:00';
            let to = cycleDtl[0] + ' 23:59:59';
            return { from, to }

        } else {
            let cDate = Moment(cycleDtl[0] + ' 00:00:01', 'YYYY-MM-DD HH:mm:ss')
                .add({ 'minutes': cycleDtl[1] * iConsts.getCycleByMinutes() })
                .format("YYYY-MM-DD HH:mm:ss");
            return IUtils.getCoinbaseRange(cDate);

        }
    }

    static getCoinbaseRange(cDate = null) {
        let {
            minCreationDate,
            maxCreationDate
        } = IUtils.getACycleRange({ cDate });
        return {
            from: minCreationDate,
            to: maxCreationDate
        }
    }

    static getCoinbaseAgeByMinutes(cDate = null) {
        if (cDate == null)
            cDate = utils.getNow();
        return utils.timeDiff(IUtils.getACycleRange({ cDate }).minCreationDate, cDate).asMinutes;

        // if (!getMoment) {
        //     getMoment = require("../startup/singleton").instance.getMoment;
        // }

        // let day, minutes;
        // if (cDate === null) {
        //     day = getMoment().format('YYYY-MM-DD');
        //     minutes = IUtils.getNowByMinutes();
        // } else {
        //     day = cDate.split(' ')[0];
        //     let timeDtl = cDate.split(' ')[1].split(':');
        //     minutes = (parseInt(timeDtl[0]) * 60) + parseInt(timeDtl[1]);
        // }
        // let startMinute = parseInt(minutes / iConsts.getCycleByMinutes()) * iConsts.getCycleByMinutes();
        // // let endMinute = startMinute + iConsts.getCycleByMinutes() - 1;

        // return minutes - startMinute;
    }

    static getCoinbaseAgeBySecond(cDate = null) {
        return IUtils.getCoinbaseAgeByMinutes(cDate) * 60;

        // if (!getMoment) {
        //     getMoment = require("../startup/singleton").instance.getMoment;
        // }

        // let day, seconds;
        // if (cDate === null) {
        //     day = getMoment().format('YYYY-MM-DD');
        //     seconds = IUtils.getNowBySeconds();
        // } else {
        //     day = cDate.split(' ')[0];
        //     let timeDtl = cDate.split(' ')[1].split(':');
        //     seconds = (parseInt(timeDtl[0]) * 3600) + (parseInt(timeDtl[1]) * 60) + parseInt(timeDtl[2]);
        // }
        // let startSeconds = parseInt(seconds / (iConsts.getCycleByMinutes() * 60)) * iConsts.getCycleByMinutes() * 60;
        // return seconds - startSeconds;
    }

    static isOlderThanSomeCycles(cDate, cyclesCount = 1, dummyTime = utils.getNow()) {
        if (!Moment) {
            Moment = require("../startup/singleton").instance.Moment;
        }
        let currentCBFrom = IUtils.getCoinbaseRange(dummyTime).from;
        let prevCBFrom = Moment(currentCBFrom, "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': cyclesCount * iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");
        return (cDate < prevCBFrom)
    }

    static convertMinutesToHHMM(minutes) {
        return _.padStart(parseInt(minutes / 60), 2, '0') + ':' + _.padStart((minutes % 60), 2, '0');
    }

    static getNowByMinutes() {
        if (!getMoment) {
            getMoment = require("../startup/singleton").instance.getMoment;
        }
        let nowByMinutes = (parseInt(getMoment().format('HH')) * 60) + parseInt(getMoment().format('mm'));
        return nowByMinutes;
    }

    static getNowBySeconds() {
        if (!getMoment) {
            getMoment = require("../startup/singleton").instance.getMoment;
        }
        let nowBySeconds = (parseInt(getMoment().format('HH')) * 3600) + (parseInt(getMoment().format('mm')) * 60) + parseInt(getMoment().format('ss'));
        return nowBySeconds;
    }

    static getCycleCountPerDay() {
        return parseInt(24 * 60 / iConsts.getCycleByMinutes());
    }

    static getCoinbaseCycleNumber(_t = null) {
        let minutes;
        if (_t === null) {
            minutes = IUtils.getNowByMinutes();
        } else {
            minutes = _t.split(' ')[1].split(':');
            minutes = (parseInt(minutes[0]) * 60) + parseInt(minutes[1]);
        }
        let cycleNumber;
        if (iConsts.TIME_GAIN == 1) {
            cycleNumber = IUtils.isAmOrPm(minutes);
        } else {
            cycleNumber = parseInt(minutes / iConsts.getCycleByMinutes());
        }
        return cycleNumber;
    }

    static isInCurrentCycle(cDate) {
        return (cDate >= IUtils.getCoinbaseRange().from);
    }

    static isYoungerThan2Cycle(cDate) {
        return (cDate >= IUtils.getACycleRange({ backByCycle: 1 }).minCreationDate)
        // if (!Moment) {
        //     Moment = require("../startup/singleton").instance.Moment;
        // }
        // // console.log(IUtils.getCoinbaseRange().from, t);
        // let prevCycle = Moment(IUtils.getCoinbaseRange().from, "YYYY-MM-DD HH:mm:ss").
        // subtract({ 'minutes': 1 }).format("YYYY-MM-DD HH:mm:ss");

        // if (cDate >= IUtils.getCoinbaseRange(prevCycle).from)
        //     return true;
        // return false;
    }

    // if passed 1/4 of a cycle time and still the coinbase block is not created, so broadcast one of them
    static passedCertainTimeOfCycleToRecordInDAG(_t = null) {
        if (clog == null)
            clog = require('../loggers/console_logger');

        const cbIC = require('../dag/coinbase/control-coinbase-issuance-criteria')
        let { machineKey, emailsHashDict } = cbIC.makeEmailHashDict();
        let machineIndex = utils.objKeys(emailsHashDict).sort().indexOf(machineKey) + 2;
        clog.cb.info(`psudo-random CB creation machineIndex: ${machineIndex}`);
        console.log(`psudo-random CB creation machineIndex: ${machineIndex}`);

        let cycleBYMinutes = (iConsts.TIME_GAIN == 1) ? iConsts.STANDARD_CYCLE_BY_MINUTES : iConsts.TIME_GAIN
        let res = utils.timeDiff(IUtils.getCoinbaseRange(_t).from).asSeconds >= (cycleBYMinutes * 60 * iConsts.COINBASE_FLOOR_TIME_TO_RECORD_IN_DAG * (1 + (machineIndex ** 7 / 131)));
        clog.cb.info(`passed CertainTimeOfCycleToRecordInDAG? ${res}`);
        console.log(`passed CertainTimeOfCycleToRecordInDAG? ${res}`);
        return res;
    }

    static convertTitleToHash(title) {
        if (IUtils.crypto == null)
            IUtils.crypto = require('../crypto/crypto');

        return IUtils.crypto.keccak256(title.toString());
    }


    static doHashOrderedObject(hashables) {
        if (IUtils.crypto == null)
            IUtils.crypto = require('../crypto/crypto');

        let hashArray = [];
        for (let key of utils.objKeys(hashables).sort()) {
            hashArray.push(hashables[key]);
        }
        return IUtils.crypto.keccak256(utils.stringify(hashArray));
    }

    static doHashObject(obj) {
        if (IUtils.crypto == null)
            IUtils.crypto = require('../crypto/crypto');

        return IUtils.crypto.keccak256(utils.stringify(obj));
    }


}
IUtils.crypto = null;
IUtils.validators = require('./validators.js');

module.exports = IUtils
