const _ = require('lodash');

// TODO: some unit tests for heapPermutation
function heapPermutation() {
    this.shouldBeUnique = false;
    this.premutions = [];
    this.premutionsStringify = [];

    this.printArr = function (a, n) {
        let out = []
        let strOut = ''
        for (i = 0; i < n; i++)
            out.push(a[i]);

        strOut = JSON.stringify(_.sortBy(out));

        if (!this.shouldBeUnique) {
            this.premutions.push(out);
            this.premutionsStringify.push(strOut);

        } else {
            if (this.premutionsStringify.indexOf(strOut) == -1) {
                this.premutions.push(out);
                this.premutionsStringify.push(strOut);
            }
        }
    };


    this.heapP = function (a) {
        this._heapP(a, a.length);
    };

    this._heapP = function (a, n, s = null) {
        if (s === null)
            s = a.length;
        // if size becomes 1 then prints the obtained 
        // permutation 
        let arr = a;
        let size = s;
        let num = n;
        if (size == 1) {
            this.printArr(arr, num);
            // return;  
        }

        for (let i = 0; i < size; i++) {
            this._heapP(arr, num, size - 1);

            // if size is odd, swap first and last 
            // element 
            if (size % 2 == 1) {
                tmp = arr[0];
                arr[0] = arr[size - 1];
                arr[size - 1] = tmp;

                // swap(a[0], a[size-1]); 

                // If size is even, swap ith and last 
                // element 
            } else {
                tmp = arr[i];
                arr[i] = arr[size - 1];
                arr[size - 1] = tmp;
                // swap(a[i], a[size - 1]);
            }
        }
    };

}

module.exports = heapPermutation;
