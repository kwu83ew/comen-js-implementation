const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandler = require('../wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../wallet/wallet-handler-local-utxos');
const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');

class FreePostsHandler {

    static recordFile(args) {
        // TODO: too many security issue to not alloweing upload harmful files
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : 'cFileToBuffer';
        let prepareRes = freeDocHandler.fileDocHandler.prepareAfileDocument(args);
        // clog.trx.info(prepareRes);
        if (prepareRes.err != false)
            return prepareRes;
        let fileDoc = prepareRes.fileDoc;

        // prepare payment doc too
        // calculate post cost
        let cPostCost = freeDocHandler.calcCostDCustomPost({
            cDate: utils.getNow(),
            cPost: { dLen: fileDoc.dLen, dType: fileDoc.dType, dClass: fileDoc.dClass },
            stage: iConsts.STAGES.Creating
        });
        clog.trx.info(`cPostCost Cost ${utils.stringify(cPostCost)}`);
        if ((cPostCost.err != false) || (cPostCost.cost == 0))
            return cPostCost;

        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_FDOC', cPostCost.cost]
        ];

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: cPostCost.cost * 1.5
        });
        if (spendables.err != false) {
            clog.trx.error('get Some Inputs res: ', utils.stringify(spendables));
            return spendables;
        }
        console.log('spendables', spendables);

        // return { err: false, msg: 'binding doc createsd' }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(cPostCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: fileDoc.hash,
            description: 'Pay for recording custom post on chain',
            inputs,
            outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        clog.trx.info('signed custom post costs trx: ', utils.stringify(trxDtl));


        // push trx & fileDoc Req to Block buffer
        let res = docBufferHandler.pushInSync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = docBufferHandler.pushInSync({ doc: fileDoc, DPCost: cPostCost.cost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);


        if (dTarget == 'cFileToBuffer')
            return { err: false, msg: `Your file was pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }
}

module.exports = FreePostsHandler;
