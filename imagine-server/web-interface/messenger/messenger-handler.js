const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const flensHandler = require('../../contracts/flens-contract/flens-handler');

class MessengerHandler {

    static listMyIPGPBindings(args = {}) {
        let myIPGPBindings = flensHandler.binder.listMyIPGPBindings();
        return myIPGPBindings;
    }

    static async sendPlainTextMessage(args = {}) {
        return await flensHandler.msg.sendPlainTextMessage(args);
    }
    
    static loadTextMessages(args = {}) {
        return flensHandler.msg.loadTextMessages(args);
    }


}

module.exports = MessengerHandler;