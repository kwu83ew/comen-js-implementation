const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandler = require('../wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../wallet/wallet-handler-local-utxos');
const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');
const flensHandler = require('../../contracts/flens-contract/flens-handler');
const rendererHandler = require('../../services/render-handler/render-handler');
const crypto = require('../../crypto/crypto')
const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
const pledgeHandler = require('../../contracts/pledge-contract/pledge-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const contractsHandler = require('../../web-interface/contracts/contracts-handler');

class DemosHandler {


    static sendNewPost(args) {
        console.log(`send New Post args: ${utils.stringify(args)}`);
        clog.app.info(`send New Post args: ${utils.stringify(args)}`);
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // create proper doc and send it to network
        args.napOpinion = utils.sanitizingContent(args.napOpinion);
        let newPostRes = freeDocHandler.demos.posts.prepareNewPost(args);
        clog.app.info(`new Agora post Res: ${utils.stringify(newPostRes)}`);
        if (newPostRes.err != false)
            return newPostRes;

        let napCostPayMode = _.has(args, 'napCostPayMode') ? args.napCostPayMode : 'normal';
        if (napCostPayMode == 'byPoW') {
            // push to Block buffer and publish immediately

            let res = docBufferHandler.pushInSync({ doc: newPostRes.fDoc, DPCost: 0 });
            if (res.err != false)
                return res;

            const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
            let r = wBroadcastBlock.broadcastBlock({ costPayMode: 'byPoW' });
            return r;

        } else {
            // prepare payment doc too
            return freeDocHandler.payForFreeDocAndPushToBuffer({ fDoc: newPostRes.fDoc, dTarget });

        }
    }

    static buildANewAgora(args) {
        console.log(`build ANewAgora args: ${utils.stringify(args)}`);
        clog.app.info(`build ANewAgora args: ${utils.stringify(args)}`);
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // create proper doc and send it to network
        let newAgoraRegRes = freeDocHandler.demos.prepareNewAgoraRegisterDoc(args);
        clog.app.info(`new AgoraRegRes: ${utils.stringify(newAgoraRegRes)}`);
        if (newAgoraRegRes.err != false)
            return newAgoraRegRes;

        // prepare payment doc too
        return freeDocHandler.payForFreeDocAndPushToBuffer({ fDoc: newAgoraRegRes.fDoc, dTarget });
    }

    // static getAgoraHashByTitle(args) {
    //     console.log(`get Agora Hash By Title args: ${utils.stringify(args)}`);
    //     clog.app.info(`get Agora Hash By Title args: ${utils.stringify(args)}`);
    //     let napAgTitle = _.has(args, 'napAgTitle') ? args.napAgTitle : '';
    //     //    napAgTitle = encodeURIComponent(napAgTitle);
    //     napAgTitle = decodeURIComponent(napAgTitle);
    //     let agRes = freeDocHandler.demos.searchInAgoras({
    //         query: [['ag_title', napAgTitle]]
    //     });
    //     clog.app.info(`new agRes Res: ${utils.stringify(agRes)}`);
    //     return { err: false, napAgUqHash: agRes[0].ag_unique_hash };
    // }

    static retrieveAgorasInfo(args) {
        let msg = '';
        let getPosts = _.has(args, 'getPosts') ? args.getPosts : false;
        console.log(`retrieve Agoras Info args: ${utils.stringify(args)}`);
        let AgTitle = utils.decodeURIComponent(args.AgTitle);
        let agoras = freeDocHandler.demos.searchInAgoras({
            query: [
                ['ag_in_hash', flensHandler.generateINameHash(args.iName)],
                ['ag_title', AgTitle],
            ]
        });
        if (agoras.length != 1) {
            msg = `There is no Agora for ${args.iName}/${AgTitle}`;
            return { err: true, msg }
        }
        let agoraInfo = agoras[0];
        if (!getPosts)
            return { err: false, agoraInfo }
        agoraInfo.iName = args.iName;

        let posts = freeDocHandler.demos.posts.searchInPosts({
            query: [
                ['ap_ag_unique_hash', agoraInfo.ag_unique_hash]
            ],
            order: [['ap_creation_date', 'ASC']]
        });
        for (let aPost of posts) {
            let ap_attrs = utils.parse(aPost.ap_attrs);
            let ap_opinion = aPost.ap_opinion;
            let attrActions = null;
            if (utils._nilEmptyFalse(ap_attrs)) {
                // it is a normal post, so render
                ap_opinion = rendererHandler.renderToHTML(aPost.ap_opinion);
            } else {
                // mabe need special treatment befor displying
                if (_.has(ap_attrs, 'isPreProposal') && (ap_attrs.isPreProposal == iConsts.CONSTS.YES)) {
                    let buffer = Buffer.from(ap_opinion, "base64");
                    ap_opinion = buffer.toString("utf8");
                    ap_opinion = utils.parse(ap_opinion);
                    let proposalHTML = proposalHandler.renderProposalDocumentToHTML(ap_opinion.proposal);
                    let pledgeHTML = pledgeHandler.renderPledgeDocumentToHTML(ap_opinion.pledge);
                    let addrDtl = walletAddressHandler.getAddressesInfoSync([ap_opinion.pledge.pledgee]);

                    // present attr buttons too!
                    attrActions = [];
                    if (addrDtl.length == 1) {
                        attrActions.push({ aType: 'Button', aCaption: ` Confirm `, aAct: `convertCPostToPLRBundle`, aParams: aPost.ap_doc_hash });
                    } else {
                        // attrActions.push({ aType: 'Button', aCaption: ` Confirm `, aAct: `alert`, aParams: 'You are not the desired pledgee! but you can be, try offer a better loan to user' });
                        attrActions.push({ aType: 'Button', aCaption: ` I can offer better loan! `, aAct: `alert`, aParams: 'It is not implemented yet!' });
                    }
                    ap_opinion = `${proposalHTML}<br>${pledgeHTML}`;
                }
            }

            aPost.attrActions = attrActions;
            aPost.ap_opinion = ap_opinion;
            aPost.authorColor = `#${utils.hash6c(crypto.keccak256(aPost.ap_creator))}`;
        }

        return { err: false, agoraInfo, posts }
    }

    static convertCPostToPLRBundle(args) {
        console.log(`convert CPost To PLR Bundle args: ${utils.stringify(args)}`);
        clog.app.info(`convert CPost To PLR Bundle args: ${utils.stringify(args)}`);
        return contractsHandler.convertCPostToPLRBundle(args.docHash);
    }

    static getOnchainAgoras(args) {
        let records = freeDocHandler.demos.searchInAgoras({
            order: [['ag_title', 'ASC']]
        });

        // retrieve Agoras iNames info
        let iNHashes = records.map(x => x.ag_in_hash);
        let iNames = flensHandler.register.searchRegisteredINames({
            needBindingsInfoToo: false,
            query: [
                ['in_hash', ['IN', utils.arrayUnique(iNHashes)]],
            ]
        });
        let iNamesDict = {};
        for (let anIName of iNames.records) {
            iNamesDict[anIName.inHash] = anIName.iName;
        }
        for (let aRec of records) {
            aRec['iName'] = (_.has(iNamesDict, aRec.ag_in_hash)) ? iNamesDict[aRec.ag_in_hash] : '-';
            aRec['url'] = `${aRec['iName']}/demos/${aRec.ag_title}`;
        }
        // console.log('iNames, records', records);

        return { err: false, records };
    }
}

module.exports = DemosHandler;