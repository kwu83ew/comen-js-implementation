const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const doCCB = require('../../dag/coinbase/do-create-coinbase-block');
const blockLib = require('../../dag/lib');
const clog = require('../../loggers/console_logger');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const machine = require('../../machine/machine-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const leavesHandler = require('../../dag/leaves-handler');
const blockHasher = require('../../dag/hashable');
const blockHandler = require('../../dag/normal-block/normal-block-handler/normal-block-handler');
const POWblockHandler = require('../../dag/pow-block/pow-block-handler');
const kvHandler = require('../../models/kvalue');


class BlockPusher {

    static async broadcastBlockAsync(args) {
        clog.http.info(`get: utils/broadcastBlock -> ${utils.stringify(args)}`);
        console.log(`get: utils/broadcastBlock -> ${utils.stringify(args)}`);
        let cheatingCreationDate = null;
        let cheatingAncestors = null;
        if ((_.has(args, 'createDateType')) && (args.createDateType == 'cheat')) {
            cheatingCreationDate = await kvHandler.getValueAsync('cheatingCreationDate');
            cheatingAncestors = await kvHandler.getValueAsync('cheatingAncestors');
        }

        let costPayMode = _.has(args, 'costPayMode') ? args.costPayMode : 'normal';
        let res;
        if (costPayMode == 'byPoW') {
            res = await POWblockHandler.createAPOWBlock({
                creationDate: cheatingCreationDate,
                ancestors: (!utils._nilEmptyFalse(cheatingAncestors)) ? utils.parse(cheatingAncestors) : null
            });

        } else {
            res = blockHandler.createANormalBlock({
                creationDate: cheatingCreationDate,
                ancestors: (!utils._nilEmptyFalse(cheatingAncestors)) ? utils.parse(cheatingAncestors) : null
            });
            if (res.err != false)
                console.log(`\n\nresBas ${utils.stringify(res)}`);

        }
        return res;
    }

    static broadcastBlock(args) {
        clog.http.info(`get: utils/broadcastBlock -> ${utils.stringify(args)}`);
        console.log(`get: utils/broadcastBlock -> ${utils.stringify(args)}`);
        let cheatingCreationDate = null;
        let cheatingAncestors = null;
        if ((_.has(args, 'createDateType')) && (args.createDateType == 'cheat')) {
            cheatingCreationDate = kvHandler.getValueSync('cheatingCreationDate');
            cheatingAncestors = kvHandler.getValueSync('cheatingAncestors');
        }

        let costPayMode = _.has(args, 'costPayMode') ? args.costPayMode : 'normal';
        let res;
        if (costPayMode == 'byPoW') {
            res = POWblockHandler.createAPOWBlock({
                creationDate: cheatingCreationDate,
                ancestors: (!utils._nilEmptyFalse(cheatingAncestors)) ? utils.parse(cheatingAncestors) : null
            });

        } else {
            res = blockHandler.createANormalBlock({
                creationDate: cheatingCreationDate,
                ancestors: (!utils._nilEmptyFalse(cheatingAncestors)) ? utils.parse(cheatingAncestors) : null
            });
            if (res.err != false)
                console.log(`\n\nresB ${utils.stringify(res)}`);

        }

        return res;
    }
}

module.exports = BlockPusher;