const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const kvHandler = require('../../models/kvalue');
const Moment = require('../../startup/singleton').instance.Moment;
const walletAddressHandler = require('./wallet-address-handler');
const coinbaseUTXOs = require('../../dag/coinbase/import-utxo-from-coinbases');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const machine = require('../../machine/machine-handler');
const blockUtils = require('../../dag/block-utils');
const cnfHandler = require('../../config/conf-params');

const table = 'i_machine_wallet_funds';


class WalletHandler {

    static async insertAnUTXOInWallet(args) {
        args.wfmpCode = _.has(args, 'wfmpCode') ? args.wfmpCode : iutils.getSelectedMProfile();
        let dblChk = await model.aRead({
            table,
            query: [
                ['wf_mp_code', args.wfmpCode],
                ['wf_trx_hash', args.wfTrxHash],
                ['wf_o_index', args.wfOIndex],
            ],
            log: false
        });
        if (dblChk.length > 0) {
            // maybe update!

        } else {
            //insert
            await model.aCreate({
                table,
                values: {
                    wf_mp_code: args.wfmpCode,
                    wf_address: args.wfAddress,
                    wf_block_hash: args.wfBlockHash,
                    wf_trx_type: args.wfTrxType,
                    wf_trx_hash: args.wfTrxHash,
                    wf_o_index: args.wfOIndex,
                    wf_o_value: args.wfOValue,
                    wf_creation_date: args.wfCreationDate,
                    wf_mature_date: args.wfMatureDate,
                    wf_last_modified: utils.getNow()
                }
            })

        }
    }

    static async deleteFromFunds(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'wf_mp_code'))
            query.push(['wf_mp_code', mpCode]);
        args.query = query;
        args.table = table;
        await model.aDelete(args);
    }

    static async updateFundsFromNewBlock(wBlock, walletAddresses) {
        // since all validation controls (relatd to transaction input/output) are already done before recording wBlock in DAG, 
        //here we trust the wBlock information and just extract the funds from transactions

        let mpCode = iutils.getSelectedMProfile();
        let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
        clog.app.info(`update wallet funds: block(${utils.hash6c(wBlock.bHash)}) ${wBlock.bType} for addresses(${walletAddresses.map(x => iutils.shortBech(x))})`);
        // extract unmatured outputs
        if (_.has(block, 'docs'))
            for (let aDoc of block.docs) {
                switch (aDoc.dType) {

                    case (iConsts.DOC_TYPES.Coinbase):
                        for (let oInx = 0; oInx < aDoc.outputs.length; oInx++) {
                            let anOutput = aDoc.outputs[oInx];
                            if (!walletAddresses.includes(anOutput[0]))
                                continue;

                            await WalletHandler.insertAnUTXOInWallet({
                                wfmpCode: mpCode,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfAddress: anOutput[0],
                                wfBlockHash: block.blockHash,
                                wfTrxType: aDoc.dType,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfOValue: anOutput[1],
                                wfCreationDate: block.creationDate,
                                wfMatureDate: coinbaseUTXOs.calcCoinbasedOutputMaturationDate(block.creationDate)
                            });
                        }
                        break;

                    case (iConsts.DOC_TYPES.RlDoc):
                        // FIXME: needs more tests before relaese the first reserved block
                        for (let oInx = 0; oInx < aDoc.outputs.length; oInx++) {
                            let anOutput = aDoc.outputs[oInx];
                            if (!walletAddresses.includes(anOutput[0]))
                                continue;

                            await WalletHandler.insertAnUTXOInWallet({
                                wfmpCode: mpCode,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfAddress: anOutput[0],
                                wfBlockHash: block.blockHash,
                                wfTrxType: aDoc.dType,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfOValue: anOutput[1],
                                wfCreationDate: block.creationDate,
                                wfMatureDate: coinbaseUTXOs.calcCoinbasedOutputMaturationDate(block.creationDate)
                            });
                        }
                        break;

                    case (iConsts.DOC_TYPES.DPCostPay):
                        for (let oInx = 0; oInx < aDoc.outputs.length; oInx++) {
                            let anOutput = aDoc.outputs[oInx]

                            // import only wallet controlled funds, implicitely removes the "TP_DP" outputs too.
                            if (!walletAddresses.includes(anOutput[0]))
                                continue;

                            await WalletHandler.insertAnUTXOInWallet({
                                wfmpCode: mpCode,
                                wfTrxHash: aDoc.hash,
                                wfAddress: anOutput[0],
                                wfBlockHash: block.blockHash,
                                wfTrxType: aDoc.dType,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfOValue: anOutput[1],
                                wfCreationDate: block.creationDate,
                                wfMatureDate: Moment(block.creationDate, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': iConsts.getCycleByMinutes() }).format('YYYY-MM-DD HH:mm:ss')
                            });
                        }
                        break;

                    case (iConsts.DOC_TYPES.BasicTx):
                        for (let oInx = 0; oInx < aDoc.outputs.length; oInx++) {
                            let anOutput = aDoc.outputs[oInx]

                            // import only wallet controlled funds, implicitely removes the outputs dedicated to treasury payments too
                            if (!walletAddresses.includes(anOutput[0]))
                                continue;

                            // do not import DPCost payment outputs, because they are already spen in DPCostPay doc
                            if (aDoc.dPIs.includes(oInx))
                                continue;

                            await WalletHandler.insertAnUTXOInWallet({
                                wfmpCode: mpCode,
                                wfTrxHash: aDoc.hash,
                                wfAddress: anOutput[0],
                                wfBlockHash: block.blockHash,
                                wfTrxType: aDoc.dType,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfOValue: anOutput[1],
                                wfCreationDate: block.creationDate,
                                wfMatureDate: Moment(block.creationDate, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': iConsts.getCycleByMinutes() }).format('YYYY-MM-DD HH:mm:ss')
                            });
                        }

                        // removing spent UTXOs in block too
                        for (let input of aDoc.inputs) {
                            await WalletHandler.deleteFromFunds({
                                query: [
                                    ['wf_mp_code', mpCode],
                                    ['wf_trx_hash', input[0]],
                                    ['wf_o_index', input[1]]
                                ]
                            });
                        }
                        break;

                    case (iConsts.DOC_TYPES.RpDoc):
                        for (let oInx = 0; oInx < aDoc.outputs.length; oInx++) {
                            let anOutput = aDoc.outputs[oInx]

                            // import only wallet controlled funds, implicitely removes the outputs dedicated to treasury payments too
                            if (!walletAddresses.includes(anOutput[0]))
                                continue;

                            await WalletHandler.insertAnUTXOInWallet({
                                wfmpCode: mpCode,
                                wfTrxHash: aDoc.hash,
                                wfAddress: anOutput[0],
                                wfBlockHash: block.blockHash,
                                wfTrxType: aDoc.dType,
                                wfTrxHash: aDoc.hash,
                                wfOIndex: oInx,
                                wfOValue: anOutput[1],
                                wfCreationDate: block.creationDate,
                                wfMatureDate: Moment(block.creationDate, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': iConsts.getCycleByMinutes() }).format('YYYY-MM-DD HH:mm:ss')
                            });
                        }

                        // removing spent UTXOs in block too
                        for (let input of aDoc.inputs) {
                            await WalletHandler.deleteFromFunds({
                                query: [
                                    ['wf_mp_code', mpCode],
                                    ['wf_trx_hash', input[0]],
                                    ['wf_o_index', input[1]]
                                ]
                            });
                        }
                        break;

                }

            }
    }

    static async refreshFunds() {
        console.log('refreshFunds');
        let mpCode = iutils.getSelectedMProfile();

        const dagHandler = require('../../dag/graph-handler/dag-handler');

        //prepare the wallet addreses:
        let addresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
        addresses = addresses.map(x => x.waAddress)

        let latestUpdate = await kvHandler.getValueAsync('latest_refresh_funds');
        // clog.app.info(iConsts.getLaunchDate());
        clog.app.info(`latest_refresh_funds: ${latestUpdate}`);
        if (utils._nilEmptyFalse(latestUpdate)) {
            latestUpdate = iConsts.getLaunchDate()
            await kvHandler.upsertKValueAsync('latest_refresh_funds', latestUpdate)
        }


        let wBlocks = await dagHandler.searchInDAGAsync({
            query: [
                ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]],
                ['b_creation_date', ['>=', iConsts.getLaunchDate()]] // TODO improve it to reduce process load. (e.g. use latestUpdate instead)
            ],
            order: [['b_creation_date', 'ASC']]
        });
        clog.app.info(`looking into blocks[${wBlocks.map(x => utils.hash6c(x.bHash))}] to retrieve wallet-controlable funds`);

        // FIXME: (improve it) remove this and search in i_blocks only new blocks
        await model.aDelete({
            table,
            query: [['wf_mp_code', mpCode]]
        });

        for (let wBlock of wBlocks) {
            await this.updateFundsFromNewBlock(wBlock, addresses);
        }

        await kvHandler.upsertKValueAsync('latest_refresh_funds', utils.getNow())
    }

    static async getFundsList() {
        let mpCode = iutils.getSelectedMProfile();
        await this.refreshFunds();
        let res = model.sRead({
            table,
            query: [
                ['wf_mp_code', mpCode]
            ],
            order: [
                ['wf_mature_date', 'ASC']
            ]
        });
        res = res.map(x => this.convertFields(x));

        for (let aRes of res) {
            aRes.wfTrxType = aRes.wfTrxType;
            aRes.wfOValue = iutils.convertBigIntToJSInt(aRes.wfOValue)
        };
        return res;
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'wf_id'))
            out.wfId = elm.wf_id;
        if (_.has(elm, 'wf_mp_code'))
            out.wfmpCode = elm.wf_mp_code;
        if (_.has(elm, 'wf_address'))
            out.wfAddress = elm.wf_address;
        if (_.has(elm, 'wf_block_hash'))
            out.wfBlockHash = elm.wf_block_hash;
        if (_.has(elm, 'wf_trx_type'))
            out.wfTrxType = elm.wf_trx_type;
        if (_.has(elm, 'wf_trx_hash'))
            out.wfTrxHash = elm.wf_trx_hash;
        if (_.has(elm, 'wf_o_index'))
            out.wfOIndex = elm.wf_o_index;
        if (_.has(elm, 'wf_o_value'))
            out.wfOValue = elm.wf_o_value;
        if (_.has(elm, 'wf_creation_date'))
            out.wfCreationDate = elm.wf_creation_date;
        if (_.has(elm, 'wf_mature_date'))
            out.wfMatureDate = elm.wf_mature_date;
        if (_.has(elm, 'wf_last_modified'))
            out.wfLastModified = elm.wf_last_modified;
        return out;
    }

    static async getTrxList(req) {
        console.log('getTrxList for excel reporter');
        const dagHandler = require('../../dag/graph-handler/dag-handler');


        //prepare the wallet addreses:
        let addresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
        addresses = addresses.map(x => x.waAddress);
        let addressTrxs = {};
        for (let add of addresses) {
            addressTrxs[add] = { trxs: [], incomes: 0, outcomes: 0 }
        }
        console.log(`addresses ${addresses}`);

        let wBlocks = await dagHandler.searchInDAGAsync({
            fields: ['b_body'],
            query: [
                ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]],
                ['b_creation_date', ['>=', iConsts.getLaunchDate()]] // TODO improve it to reduce process load. (e.g. use latestUpdate instead)
            ],
            order: [['b_creation_date', 'ASC']]
        });

        let refLoc;
        let mapRefToAddresas = {};
        for (let wBlock of wBlocks) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            let refLocs = [];
            for (let doc of block.docs) {

                if (iutils.trxHasOutput(doc.dType)) {
                    // console.log(`analyz trx(outputs) of block(${utils.hash6c(block.blockHash)}) ${iutils.mapDocCodeToDocType(doc.dType)}`);
                    for (let outputInx = 0; outputInx < doc.outputs.length; outputInx++) {
                        let output = doc.outputs[outputInx];
                        if (addresses.indexOf(output[0]) != -1) {
                            refLoc = iutils.packCoinRef(doc.hash, outputInx);
                            refLocs.push(refLoc);
                            mapRefToAddresas[refLoc] = output;
                            addressTrxs[output[0]].incomes += output[1];
                            addressTrxs[output[0]].trxs.push({ date: block.creationDate, value: output[1], refLoc, type: 'income' });
                        }

                    }
                }

                if (iutils.trxHasInput(doc.dType)) {
                    // console.log(`analyz trx(inputs) of block(${utils.hash6c(block.blockHash)}) ${iutils.mapDocCodeToDocType(doc.dType)}`);
                    for (let input of doc.inputs) {
                        refLoc = iutils.packCoinRef(input[0], input[1]);
                        if (_.has(mapRefToAddresas, refLoc)) {
                            // this spend input belonga to wallet funds
                            addressTrxs[mapRefToAddresas[refLoc][0]].outcomes += mapRefToAddresas[refLoc][1];
                            addressTrxs[mapRefToAddresas[refLoc][0]].trxs.push({ date: block.creationDate, value: mapRefToAddresas[refLoc][1], refLoc, type: 'outcome' });
                        }
                    }
                }

            }
        }
        // console.log(`\nmapRefToAddresas: ${utils.stringify(mapRefToAddresas)}`);
        // console.log(`\naddressTrxs: ${utils.stringify(addressTrxs)}`);

        return addressTrxs;

    }

    static async creatARandomTrxWrapper(description = 'rand trx') {
        let maxDPCost = 120000000;
        let trxNeededInfo = await this.creatARandomTrx({
            maxDPCost
        });
        console.log('creatARandomTrxWrapper: ', utils.stringify(trxNeededInfo));

        let newArgs = {

            maxDPCost,
            DPCostChangeIndex: trxNeededInfo.outputs.length - 1, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,

            description,
            inputs: trxNeededInfo.inputs,
            outputs: trxNeededInfo.outputs,

        }
        let trxDtl = this.makeATransaction(newArgs);
        return trxDtl;
    }



    static async retrieveSpendableUTXOsAsync(args) {
        let walletAddresses = _.has(args, 'walletAddresses') ? args.walletAddresses : null;
        if (walletAddresses == null) {
            walletAddresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
            walletAddresses = walletAddresses.map(x => x.waAddress);
        }
        let UTXOs = utxoHandler.extractUTXOsBYAddresses(walletAddresses);
        return UTXOs;
    }

    static async getAnOutputAddressAsync(args) {
        let makeNewAddress = _.has(args, 'makeNewAddress') ? args.makeNewAddress : false;

        if (makeNewAddress) {
            return await walletAddressHandler.createNewAddressAsync(args);
        } else {
            let addresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
            addresses = addresses.map(x => x.wfAddress);
            return addresses[Math.floor(Math.random() * addresses.length)];
        }
    }

    static getAnOutputAddressSync(args) {
        let makeNewAddress = _.has(args, 'makeNewAddress') ? args.makeNewAddress : false;

        if (makeNewAddress) {
            return walletAddressHandler.createNewAddressSync(args);
        } else {
            let addresses = walletAddressHandler.getAddressesListSync({ sum: false, fields: ['wa_address'] });
            addresses = addresses.map(x => x.wfAddress);
            return addresses[Math.floor(Math.random() * addresses.length)];
        }
    }



    static async creatARandomTrx(args) {
        args._super = this;
        return await require('./wallet-handler-make-rnd-trx')._creatARandomTrx(args);
    }

    static async walletSigner(args) {
        args._super = this;
        let r = await require('./wallet-handler-signer')(args);
        console.log(`_walletSigner res `, r);
        return r
    }

    static async changePAIs(args) {
        let value = args.minValue;
        let maxDPCost = args.maxDPCost;
        let signatureType = _.has(args, 'signatureType') ? args.signatureType : `Basic`;
        let signatureVersion = _.has(args, 'signatureVersion') ? args.signatureVersion : `0.0.0`;
        let signatureMod = _.has(args, 'signatureMod') ? args.signatureMod : `1/1`;
        let description = _.has(args, 'description') ? args.description : `Change PAI transaction`;
        let backerAddress = _.has(args, 'backerAddress') ? args.backerAddress : null;
        if (utils._nilEmptyFalse(backerAddress)) {
            let machineSettings = machine.getMProfileSettingsSync();
            backerAddress = machineSettings.backerAddress;
        }

        // TODO: add inputs as an optional parameter of function
        let spendables = this.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: value,
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        console.log('spendables', utils.stringify(spendables));
        let spendableMicroPAIs = spendables.sumCoinValues;
        console.log('sumCoinValues spendableMicroPAIs', spendableMicroPAIs);

        let exponent_ = 15;
        let outputs = [];
        let normalTrxDPCost = cnfHandler.getTransactionMinimumFee({ cDate: utils.getNow() }) / 2;   // estimaterd cost for each output
        console.log('normalTrxDPCost', normalTrxDPCost);
        while ((exponent_ > 7) && (spendableMicroPAIs > normalTrxDPCost * outputs.length)) {
            let div = Math.pow(10, exponent_)   // minimum output is 1000 PAI == 1000000 mili PAI = 10^6 mili PAI
            console.log(`e: ${exponent_} -> ${div}`);
            if (((spendableMicroPAIs - div) > maxDPCost) && (spendableMicroPAIs > div)) {
                let outputCountOfThisPosition = Math.floor(spendableMicroPAIs / div);
                console.log(`pos: ${outputCountOfThisPosition}`);
                for (let i = 0; i < outputCountOfThisPosition; i++) {
                    let address = await walletAddressHandler.createNewAddressAsync({ signatureType, signatureMod });
                    console.log(`address:`, address);
                    outputs.push([address, div]);
                }
                spendableMicroPAIs -= div * outputCountOfThisPosition;
            }

            // 5 
            let half5 = Math.floor(div / 2);
            if (((spendableMicroPAIs - half5) > maxDPCost) && (spendableMicroPAIs >= half5)) {
                let address = await walletAddressHandler.createNewAddressAsync({ signatureType, signatureMod });
                console.log(`address:`, address);
                outputs.push([address, half5]);
                spendableMicroPAIs -= half5;
            }

            exponent_--;
        }

        // console.log('spendableMicroPAIs', spendableMicroPAIs);
        // console.log('outputs', outputs);
        if (outputs.length == 0) {
            console.log('there is not output for changePAIs');
            return null
        }

        let tmpTrx = this.makeATransaction({
            maxDPCost,

            DPCostChangeIndex: outputs.length - 1, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,

            description,
            inputs: spendables.selectedCoins,
            outputs
        });
        console.log('\n\ntrx5 trx5 trx5 ', utils.stringify(tmpTrx));

        return tmpTrx;
    }




}

WalletHandler.makeATransaction = require('./wallet-handler-make-trx')._makeATransaction;
WalletHandler.coinPicker = require('./wallet-handler-get-some-inputs');

module.exports = WalletHandler;
