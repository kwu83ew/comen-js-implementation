const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const iutils = require('../../utils/iutils')
const crypto = require('../../crypto/crypto')
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const addressHandler = require('../../address/address-handler');

const tableAddresses = 'i_machine_wallet_addresses';
const tableFunds = 'i_machine_wallet_funds';

let privContainer;

let privateKeys;


class WalletAddressHandler {

    static fillPrivContainer(prvCntnr, detail) {
        privContainer = prvCntnr;
        this._recursiveFillPrivContainer(detail);
        return privContainer;
    }

    static _recursiveFillPrivContainer(detail) {
        console.log(`\n\n detail ${utils.stringify(detail)}\n`);
        if (_.has(detail, 'privContainer')) {
            for (let aSigner of utils.objKeys(detail.privContainer)) {
                for (let aHashKey of utils.objKeys(detail.privContainer[aSigner])) {
                    privContainer = addressHandler.addToDictContainer(privContainer, aSigner, detail.privContainer[aSigner][aHashKey]);
                    // privContainer = ;[aSigner][aHashKey] = detail.privContainer[aSigner][aHashKey];

                    console.log(`\n......... ${aSigner} ${utils.objKeys(privContainer).includes(aSigner)} ${utils.objKeys(privContainer[aSigner]).includes(aHashKey)}`);
                    if (crypto.bech32_isValidAddress(aSigner) &&
                        (
                            !utils.objKeys(privContainer).includes(aSigner) ||
                            !utils.objKeys(privContainer[aSigner]).includes(aHashKey)
                        )
                    ) {
                        try {
                            clog.app.info(`retrieve bech32(${aSigner}) because it was a sub-pub-key of an address`)
                            let addressInfo = addressHandler.getAddressesInfoSync([aSigner]);
                            let newDetail = utils.parse(addressInfo[0].detail);
                            console.log(`\n\n ......................rec rec detail ${utils.stringify(newDetail)}\n`);
                            this._recursiveFillPrivContainer(newDetail);
                        } catch (e) {

                        }
                    }

                }
            }

        }
    }

    static async getAddressesListAsync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'wa_mp_code') && (mpCode != iConsts.CONSTS.ALL))
            query.push(['wa_mp_code', mpCode]);
        args.query = query;

        args.table = tableAddresses;
        let walletAddresses = await model.aRead(args);
        // clog.app.info(`i machine_wallet_addresses: ${JSON.stringify(walletAddresses)}`);
        walletAddresses = walletAddresses.map(x => this.convertFields(x));

        let sum = _.has(args, 'sum') ? args.sum : false;
        if (sum === false)
            return walletAddresses;


        let addressDict = {};
        let nowT = utils.getNow();
        let q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date<$2 GROUP BY wf_address`;
        let tmpRes = await model.aQuery(q, [mpCode, nowT]);
        if (Array.isArray(tmpRes))
            tmpRes.forEach(aRow => {
                addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
                addressDict[aRow.wf_address] = {
                    'matSum': iutils.convertBigIntToJSInt(aRow.sum),
                    'matCount': aRow.count,
                }
            });
        q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date>=$2 GROUP BY wf_address`;
        tmpRes = await model.aQuery(q, [mpCode, nowT]);
        if (Array.isArray(tmpRes))
            tmpRes.forEach(aRow => {
                addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
                addressDict[aRow.wf_address]['unmatSum'] = iutils.convertBigIntToJSInt(aRow.sum);
                addressDict[aRow.wf_address]['unmatCount'] = aRow.count;
            });
        // clog.app.info(addressDict);

        for (let anWAddrs of walletAddresses) {
            anWAddrs['matSum'] = ((_.has(addressDict, anWAddrs.waAddress)) && (_.has(addressDict[anWAddrs.waAddress], 'matSum'))) ? addressDict[anWAddrs.waAddress]['matSum'] : '';
            anWAddrs['matCount'] = ((_.has(addressDict, anWAddrs.waAddress)) && (_.has(addressDict[anWAddrs.waAddress], 'matCount'))) ? addressDict[anWAddrs.waAddress]['matCount'] : '';
            anWAddrs['unmatSum'] = ((_.has(addressDict, anWAddrs.waAddress)) && (_.has(addressDict[anWAddrs.waAddress], 'unmatSum'))) ? addressDict[anWAddrs.waAddress]['unmatSum'] : '';
            anWAddrs['unmatCount'] = ((_.has(addressDict, anWAddrs.waAddress)) && (_.has(addressDict[anWAddrs.waAddress], 'unmatCount'))) ? addressDict[anWAddrs.waAddress]['unmatCount'] : '';
        };
        // clog.app.info(walletAddresses);
        return walletAddresses;
    }

    static getAddressesListSync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'wa_mp_code') && (mpCode != iConsts.CONSTS.ALL))
            query.push(['wa_mp_code', mpCode]);
        args.query = query;

        args.table = tableAddresses;
        let walletAddresses = model.sRead(args);
        // clog.app.info(`i machine_wallet_addresses: ${JSON.stringify(walletAddresses)}`);

        walletAddresses = walletAddresses.map(x => this.convertFields(x));

        let sum = _.has(args, 'sum') ? args.sum : false;
        if (sum === false)
            return walletAddresses;

        let addressDict = {};
        let nowT = utils.getNow();
        let q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date<$2 GROUP BY wf_address`;
        let tmpRes = model.sQuery(q, [mpCode, nowT]);
        if (Array.isArray(tmpRes))
            tmpRes.forEach(aRow => {
                addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
                addressDict[aRow.wf_address] = {
                    'matSum': iutils.convertBigIntToJSInt(aRow.sum),
                    'matCount': aRow.count,
                }
            });
        q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date>=$2 GROUP BY wf_address`;
        tmpRes = model.sQuery(q, [mpCode, nowT]);
        if (Array.isArray(tmpRes))
            tmpRes.forEach(aRow => {
                addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
                addressDict[aRow.wf_address]['unmatSum'] = iutils.convertBigIntToJSInt(aRow.sum);
                addressDict[aRow.wf_address]['unmatCount'] = aRow.count;
            });
        // clog.app.info(addressDict);

        for (let anAddrs of walletAddresses) {
            anAddrs['matSum'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'matSum'))) ? addressDict[anAddrs.waAddress]['matSum'] : '';
            anAddrs['matCount'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'matCount'))) ? addressDict[anAddrs.waAddress]['matCount'] : '';
            anAddrs['unmatSum'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'unmatSum'))) ? addressDict[anAddrs.waAddress]['unmatSum'] : '';
            anAddrs['unmatCount'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'unmatCount'))) ? addressDict[anAddrs.waAddress]['unmatCount'] : '';
        };
        // clog.app.info(addresses);
        return addresses;
    }

    static deleteAddressesOffAProfile(wa_mp_code) {
        clog.app.info(`delete Addresses Off A Profile: ${wa_mp_code}`);
        model.sDelete({
            table: tableAddresses,
            query: [['wa_mp_code', wa_mp_code]]
        });
        return { err: false }
    }

    static async createNewAddressAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        console.log('createNewAddressAsync');

        let address = addressHandler.createANewAddress(args);
        await model.aCreate({
            table: tableAddresses,
            values: {
                wa_mp_code: mpCode,
                wa_address: address.address,
                wa_title: address.title,
                wa_detail: utils.stringify(address.detail),
                wa_creation_date: utils.getNow()
            }
        });
        return address.address;
    }

    static createNewAddressSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let address = addressHandler.createANewAddress(args);
        model.sCreate({
            table: tableAddresses,
            values: {
                wa_mp_code: mpCode,
                wa_address: address.address,
                wa_title: address.title,
                wa_detail: utils.stringify(address.detail),
                wa_creation_date: utils.getNow()
            }
        });
        return address.address;
    }

    static addAnAddressToWallet(args) {
        let waAddress = args.waAddress;
        let waTitle = args.waTitle;
        let waDetail = args.waDetail;
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let waCreationDate = _.has(args, 'waCreationDate') ? args.waCreationDate : utils.getNow();

        let exist = model.sRead({
            table: tableAddresses,
            query: [
                ['wa_mp_code', mpCode],
                ['wa_address', waAddress]
            ]
        });
        if (exist.length == 0) {
            // insert
            model.sCreate({
                table: tableAddresses,
                values: {
                    wa_mp_code: mpCode,
                    wa_address: waAddress,
                    wa_title: waTitle,
                    wa_creation_date: waCreationDate,
                    wa_detail: waDetail
                }
            });
            return { err: false, msg: `The profile:address(${mpCode}:${waAddress}) added to DB` }
        } else {
            // update address
            model.sUpdate({
                table: tableAddresses,
                query: [
                    ['wa_mp_code', mpCode],
                    ['wa_address', waAddress]
                ],
                updates: {
                    wa_title: waTitle,
                }
            });
            return { err: false, msg: `The profile:address(${mpCode}:${waAddress}) was updated` }
        }
    }

    static searchWalletAdress(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'wa_mp_code') && (mpCode != iConsts.CONSTS.ALL))
            query.push(['wa_mp_code', mpCode]);
        args.query = query;

        args.table = tableAddresses;
        let res = model.sRead(args);
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFields(x));
        return res;
    }


    static getAddressesInfoSync(addresses = []) {
        let mpCode = iutils.getSelectedMProfile();

        if (!Array.isArray(addresses))
            addresses = [addresses];

        let args = {
            query: [
                ['wa_mp_code', mpCode],
                ['wa_address', ['IN', addresses]]
            ]
        }
        let res = this.searchWalletAdress(args)
        return res;
    }

    static upsertAnAddress(args) {
        let msg;
        let res = model.sRead({
            table: tableAddresses,
            query: [
                ['wa_mp_code', args.wampCode],
                ['wa_address', args.waAddress]
            ]
        });
        if (res.length == 0) {
            //insert
            model.sCreate({
                table: tableAddresses,
                values: {
                    wa_mp_code: args.wampCode,
                    wa_address: args.waAddress,
                    wa_title: args.waTitle,
                    wa_detail: args.waDetail,
                    wa_creation_date: args.waCreationDate
                }
            });
            msg = `The address inserted. ${args.wampCode}/${args.waTitle}: ${args.waAddress}`
        } else {
            // update
            model.sUpdate({
                table: tableAddresses,
                query: [
                    ['wa_mp_code', args.wampCode],
                    ['wa_address', args.waAddress]
                ],
                updates: {
                    wa_title: args.waTitle,
                    wa_detail: args.waDetail,
                    wa_creation_date: args.waCreationDate
                }
            });
            msg = `The address updated. ${args.wampCode}/${args.waTitle}: ${args.waAddress}`
        }
        console.log(msg);
        clog.app.info(msg);
        return { err: false, msg }
    }

    static signByAnAddress(args) {
        let msg;
        let signerAddress = args.signerAddress;
        let signMsg = args.signMsg;
        let unlockerIndex = _.has(args, 'unlockerIndex') ? args.unlockerIndex : 0;

        let addrDtl = WalletAddressHandler.getAddressesInfoSync([signerAddress]);
        if (addrDtl.length == 0)
            return { err: true, msg: `the signer address ${iutils.shortBech16(signerAddress)} is not controlled by wallet` };

        addrDtl = utils.parse(addrDtl[0].waDetail);
        let unlockSet = addrDtl.uSets[unlockerIndex]

        let signatures = [];
        console.log('unlockSet', unlockSet);
        console.log('sSets', unlockSet.sSets);
        for (let inx = 0; inx < unlockSet.sSets.length; inx++) {
            let signature = crypto.signMsg(signMsg, addrDtl.privContainer[unlockSet.salt][inx]);

            let verifyRes = crypto.verifySignature(signMsg, signature, unlockSet.sSets[inx].sKey);
            if (verifyRes != true) {
                let msg = `sign By An Address creates invalid signature! ${utils.stringify(args)}`;
                return { err: true, msg }
            }
            signatures.push(signature);
        }
        if (signatures.length == 0) {
            msg = `The message couldn't be signed`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        return { err: false, signMsg, unlockerIndex, signerAddress, uSet: unlockSet, signatures };
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'wa_id'))
            out.waId = elm.wa_id;
        if (_.has(elm, 'wa_mp_code'))
            out.wampCode = elm.wa_mp_code;
        if (_.has(elm, 'wa_address'))
            out.waAddress = elm.wa_address;
        if (_.has(elm, 'wa_title'))
            out.waTitle = elm.wa_title;
        if (_.has(elm, 'wa_detail'))
            out.waDetail = elm.wa_detail;
        if (_.has(elm, 'wa_creation_date'))
            out.waCreationDate = elm.wa_creation_date;
        return out;
    }

}

module.exports = WalletAddressHandler;
