const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const walletAddressHandler = require('./wallet-address-handler');
const clog = require('../../loggers/console_logger');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const spentCoinsHandler = require('../../transaction/utxo/stxo-handler');

class RandomTrxHandler {
    static async _creatARandomTrx(args) {
        let msg;
        console.log('rxNeededInfo.output', args);
        clog.trx.info(`creatARandomTrx creatARandomTrx args:${utils.stringify(args)}`);

        let minInpCount = _.has(args, 'minInpCount') ? args.minInpCount : 7;
        let maxInpCount = _.has(args, 'maxInpCount') ? args.maxInpCount : 1;
        let minOutCount = _.has(args, 'minOutCount') ? args.minOutCount : 3;
        let maxDPCost = _.has(args, 'maxDPCost') ? args.maxDPCost : 300000;
        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
        let paiReceivers = _.has(args, 'paiReceivers') ? args.paiReceivers : [];
        let unEqua = _.has(args, 'unEqua') ? args.unEqua : 0;


        let minimum_refSpndDate = utils.getNow();
        let minimum_refSpendBlock = 'unknown';


        let walletAddresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
        walletAddresses = walletAddresses.map(x => x.waAddress);
        clog.trx.info(`walletAddresses: walletAddresses: ${utils.stringify(walletAddresses.map(x => iutils.shortBech(x)))}`);
        // create a random transaction

        // create a list of valid UTXOs
        let spendableUTXOs = await args._super.retrieveSpendableUTXOsAsync({ walletAddresses }); //will return valid(or invalid in case of doublespend tests) inputs
        if (spendableUTXOs.length == 0) {
            msg = 'creatARandomTrx.there is no spendableUTXOs';
            clog.trx.info(msg);
            return { err: true, msg };
        }
        clog.trx.info(`creatARandomTrx.spendableUTXOs: ${spendableUTXOs.map(x => iutils.shortCoinRef(x.utCoin)).join(', ')}`);

        // find some random inputs
        let spUTXOs = []; // the spent UTXOs
        maxInpCount = (spendableUTXOs.length >= maxInpCount) ? maxInpCount : spendableUTXOs.length;
        minInpCount = (maxInpCount >= minInpCount) ? minInpCount : maxInpCount;
        let inputCount = Math.floor(Math.random() * maxInpCount) + minInpCount;
        let doublespendsCount = Math.floor(Math.random() * (inputCount / 2)) + 1;

        let ancestors = [];

        let sumUTXOs = -maxDPCost; // cutting DPCosts 
        let addedCoins = [];
        let tmpSpendableUTXOs = [];
        for (let inputInx = 0; inputInx < inputCount; inputInx++) {
            let spendableInx, selectedUtxo;
            if ((mode == 'dbl') && (inputInx < doublespendsCount)) {
                // forcing to have at least one duplicated/spend input
                let dblSpendCandidates = spentCoinsHandler.getInputCreationLocationsForAddresses(walletAddresses);
                clog.trx.info(`creatARandomTrx.dblSpendCandidates1: ${dblSpendCandidates.length}`);

                // removing already added refs to block
                let tmpdblSpendCandidates = [];
                dblSpendCandidates.forEach(elm => {
                    if (!addedCoins.includes(elm.coin)) {
                        tmpdblSpendCandidates.push(elm);
                    }
                });
                dblSpendCandidates = tmpdblSpendCandidates;
                clog.trx.info(`creatARandomTrx.dblSpendCandidates2: ${dblSpendCandidates.length}`);

                clog.trx.info(`\ncreatARandomTrx.spentlist (DBL UTXOs): ${utils.stringify(dblSpendCandidates)}`);
                if (dblSpendCandidates.length == 0) {
                    msg = `There is not spend refLocs in DAG`;
                    console.log(msg);
                    return { err: true, msg };
                }
                selectedUtxo = dblSpendCandidates[Math.floor(Math.random() * (dblSpendCandidates.length))];
                addedCoins.push(selectedUtxo.coin);
                clog.trx.info(`\ncreatARandomTrx.selected DBL Utxo: ${utils.stringify(selectedUtxo)}`);


                // modify minimumDate
                if (selectedUtxo.spendDate < minimum_refSpndDate) {
                    minimum_refSpndDate = selectedUtxo.spendDate;
                    minimum_refSpendBlock = selectedUtxo.spendBlock;
                }

                // find proper ancestors for fake block(1 to 5 level randomly going back randomly)
                let hirarchyLevel = Math.floor(Math.random() * 4) + 1;
                let ancs = dagHandler.walkThrough.getAncestors([selectedUtxo.spendBlock], hirarchyLevel);
                msg = `creatARandomTrx.fuonded ancesters for real-existed-spend-block(${utils.hash6c(selectedUtxo.spendBlock)}) in (${hirarchyLevel} level deeper: ${ancs.map(x => utils.hash6c(x))})`;
                console.log(`msg ${msg}`);
                clog.trx.info(msg);
                let wBlocks = dagHandler.searchInDAGSync({
                    fields: ['b_hash', 'b_creation_date'],
                    query: [
                        ['b_hash', ['IN', ancs]]
                    ]
                });
                ancestors = utils.arrayAdd(ancestors, wBlocks.map(x => x.bHash));


                // adapting selectedUtxo
                selectedUtxo.utCoin = selectedUtxo.coin;
                selectedUtxo.utOValue = selectedUtxo.value;
                selectedUtxo.utOAddress = selectedUtxo.address;

            } else {
                // removing already added refs to block
                // console.log('spendableUTXOs,', spendableUTXOs);
                let tmpSpendableUTXOs = [];
                spendableUTXOs.forEach(elm => {
                    if (!addedCoins.includes(elm.utCoin)) {
                        tmpSpendableUTXOs.push(elm);
                    }
                });
                spendableUTXOs = tmpSpendableUTXOs;
                spendableInx = Math.floor(Math.random() * (spendableUTXOs.length - 1));
                selectedUtxo = spendableUTXOs[spendableInx];
                addedCoins.push(selectedUtxo.utCoin);
            }

            console.log(`\nspending selectedUtxo:`, selectedUtxo);
            clog.trx.info(`\ncreatARandomTrx.spending selectedUtxo: ${utils.stringify(selectedUtxo)}`);

            sumUTXOs += iutils.convertBigIntToJSInt(selectedUtxo.utOValue);

            spUTXOs.push({
                "address": selectedUtxo.utOAddress,
                "value": selectedUtxo.utOValue,
                "trx_hash": iutils.unpackCoinRef(selectedUtxo.utCoin).docHash,
                "o_index": iutils.unpackCoinRef(selectedUtxo.utCoin).outputIndex
            });



            // remove selected utxo from spendableUTXOs
            let tmp = [];

            spendableUTXOs.forEach(element => {
                if (element.utCoin != selectedUtxo.utCoin)
                    tmp.push(element);
            });
            spendableUTXOs = tmp;
        }
        if (utils._nilEmptyFalse(sumUTXOs) || (sumUTXOs < 0)) {
            msg = `couldn't prepare enough inputs to cover DPCost! ${utils.microPAIToPAI(maxDPCost)} ${utils.stringify(spUTXOs)}`;
            console.log(`msg ${msg}`);
            clog.sec.error(msg);
            return { err: true, msg };
        }


        console.log(`input sum microPAI ${sumUTXOs}`);
        // sumUTXOs += parseInt(unEqua);

        let ancestorBlocks = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_creation_date'],
            query: [
                ['b_hash', ['IN', ancestors]],
            ]
        });
        clog.trx.info(`ancestorBlocks-body: ${utils.stringify(ancestorBlocks)}`);

        let cheatingBlockCreateDate;
        if (mode == 'dbl') {
            // find the oldest ancestors from potentially ancestors
            let oldestAnc = { bCreationDate: utils.getNow() };
            for (let anc of ancestorBlocks)
                if (anc.bCreationDate <= oldestAnc.bCreationDate)
                    oldestAnc = anc;
            let timeDiff = utils.timeDiff(oldestAnc.bCreationDate, minimum_refSpndDate).asMinutes;
            msg = `creatARandomTrx.the oldest ancestor(${utils.hash6c(oldestAnc.bHash)}) is ${timeDiff} minutes older than oldest spend block(${utils.hash6c(minimum_refSpendBlock)}) created on(${minimum_refSpndDate})`;
            console.log(`msg ${msg}`);
            clog.trx.info(msg);
            // try to cheat network by creating a block with creation date is older than the oldest uaseg in DAG
            // time diff between cheat-blockl and real-block can be seroc, less than a half-cyle, half-cyle, more than half-cycle, or more than a full-cycle
            timeDiff = Math.floor((Math.random() * (timeDiff - 1)) + 1);
            cheatingBlockCreateDate = utils.minutesBefore(timeDiff, minimum_refSpndDate); // shif back a little for cheat block creationdate
            msg = `creatARandomTrx.cheat block generation date will be ${cheatingBlockCreateDate}`;
            console.log(`msg ${msg}`);
            clog.trx.info(msg);
        }

        let finalCreationDate = (mode == 'dbl') ? cheatingBlockCreateDate : utils.getNow();

        let addressesDict = {};
        let addressesInfo = walletAddressHandler.getAddressesInfoSync(walletAddresses);
        addressesInfo.forEach(element => {
            addressesDict[element.waAddress] = {};
            addressesDict[element.waAddress]['title'] = element.waTitle;
            addressesDict[element.waAddress]['detail'] = JSON.parse(element.waDetail);
        });
        let addresses = utils.objKeys(addressesDict);


        let outputs = [];

        // // add some random receiver
        // let aReceiverPai = Math.floor(Math.random() * (sumUTXOs / 2));
        // let add = addresses[Math.floor(Math.random() * addresses.length)];
        // if (!utils._nilEmptyFalse(add)) {
        //     outputs.push([addresses[Math.floor(Math.random() * (addresses.length - 1))], aReceiverPai]);
        //     sumUTXOs -= aReceiverPai;
        // }

        // if client sent paiReceivers, machine choses from them
        let inx = 0;
        while ((sumUTXOs > 0) && (inx < paiReceivers.length)) {
            let aReceiverPai = Math.floor(Math.random() * (sumUTXOs - 1));
            outputs.push([paiReceivers[inx], aReceiverPai]);
            sumUTXOs -= aReceiverPai;
            inx++;
        }

        // append more random receiver from the wallet addresses
        if (sumUTXOs > 0) {
            let aReceiverPai = Math.floor(Math.random() * (sumUTXOs / 2));
            let add = addresses[Math.floor(Math.random() * (addresses.length - 1))];
            if (!utils._nilEmptyFalse(add)) {
                outputs.push([addresses[Math.floor(Math.random() * (addresses.length - 1))], aReceiverPai]);
                sumUTXOs -= aReceiverPai;
            }

            add = addresses[Math.floor(Math.random() * (addresses.length - 1))];
            if (!utils._nilEmptyFalse(add)) {
                outputs.push([add, sumUTXOs]);
                sumUTXOs = 0;
            }
        }

        // if still remains pai add it to first output
        if (sumUTXOs > 0)
            outputs[0][1] += sumUTXOs;


        let outPAIs = outputs.map(x => x[1]);
        console.log(`sum outputs(${outputs.map(x => x[1]).reduce((a, b) => a + b, 0)}) ${outPAIs} `);
        clog.trx.info(`creatARandomTrx.trx outputs: ${utils.stringify(outputs)}`);


        // find the unlockers
        let utxoInfo = {};
        let inputs = [];


        let inputsDict = {};
        spUTXOs.forEach(utxo => {
            utxoInfo[utxo.address] = utxo;
            let unlockerIndex = _.has(utxo, 'unlockerIndex') ? utxo.unlockerIndex : Math.floor(Math.random() * addressesDict[utxo.address].detail.uSets.length); // if client didn't mention, chose the first unlock struct FIXME =0
            clog.trx.info(`creatARandomTrx.unlockerIndex: ${unlockerIndex}`)

            inputs.push([utxo.trx_hash, utxo.o_index]);

            let detail = addressesDict[utxo.address].detail;
            let anUnlockSet = detail.uSets[unlockerIndex];
            inputsDict[iutils.packCoinRef(utxo.trx_hash, utxo.o_index)] = {
                address: utxo.address,
                value: utxo.value,
                uSet: anUnlockSet,
                privateKeys: detail.privContainer[anUnlockSet.salt]
            }

        });

        if (mode != 'dbl') {
            const walletHandlerLocalUTXOs = require("./wallet-handler-local-utxos");
            let doesUTXOUsed = await walletHandlerLocalUTXOs.doesUsedUTXO({ inputs });
            if (doesUTXOUsed) {
                msg = `creatARandomTrx.One or more funds are already used and you must not use it in next 12 hours in order to to avoid double spending.`;
                console.log(`msg ${msg}`);
                clog.sec.error(msg);
                clog.sec.error(utils.stringify(trx));
                return { err: true, msg };
            }
        }


        if (mode == 'dbl') {
            msg = `creatARandomTrx.Looking for ancestors, older than (${cheatingBlockCreateDate}) in ${ancestorBlocks.length} ancestors ${utils.stringify(ancestorBlocks)}`;
            console.log(msg);
            clog.trx.info(msg);
            let tmp = [];
            for (let anc of ancestorBlocks)
                if (anc.bCreationDate <= cheatingBlockCreateDate)
                    tmp.push(anc.bHash);//
            ancestorBlocks = tmp;
            console.log(`creatARandomTrx.selected ancestors `, ancestorBlocks);
            if (ancestorBlocks.length == 0) {
                msg = `couldn't find ancetors for dbl-spending. try again`;
                console.log(msg);
                return { err: true, msg };
            }
        }

        ancestors = ancestorBlocks.map(x => x.bHash);

        if (mode == 'dbl') {
            msg = `creatARandomTrx.Create a block whit cheat date(${finalCreationDate}) instead of real(${utils.getNow()})`;
            clog.trx.info(msg);
            console.log(`msg ${msg}`);
        }


        return {
            err: false,
            creationDate: finalCreationDate,
            ancestors,
            inputs: inputsDict,
            outputs: outputs,
            unEqua
        };
    }
}


module.exports = RandomTrxHandler;
