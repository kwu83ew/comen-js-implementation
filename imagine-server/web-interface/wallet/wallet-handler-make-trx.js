const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const trxHashHandler = require('../../transaction/hashable');
const signingInputOutputs = require('../../transaction/input-output-signer').signingInputOutputs;
const calcTrxDPCost = require('../../transaction/validators/calc-trx-dpcost');

class TransactionMake {

    static _makeATransaction(args) {
        console.log('\n\n\n makeATransaction args', utils.stringify(args));
        clog.trx.info(`\n\n\n makeATransaction args: ${utils.stringify(args)}`);

        let msg;
        let tNow = utils.getNow();
        let unEqua = _.has(args, 'unEqua') ? args.unEqua : null;
        let dType = _.has(args, 'dType') ? args.dType : iConsts.DOC_TYPES.BasicTx;
        let dClass = _.has(args, 'dClass') ? args.dClass : iConsts.TRX_CLASSES.SimpleTx;
        let dVer = _.has(args, 'dVer') ? args.dVer : '0.0.2';
        let sigHash = _.has(args, 'sigHash') ? args.sigHash : iConsts.SIGHASH.ALL;
        let ref = _.has(args, 'ref') ? args.ref : '';
        let description = _.has(args, 'description') ? args.description : '';
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : tNow;
        let inputs = _.has(args, 'inputs') ? args.inputs : [];
        let inputsValue = _.has(args, 'inputsValue') ? args.inputsValue : null;   // by mili PAI
        let outputs = _.has(args, 'outputs') ? args.outputs : [];
        let outputsValue = _.has(args, 'outputsValue') ? args.outputsValue : null;
        let dDPCost = _.has(args, 'dDPCost') ? args.dDPCost : null;   // definit Desired DataProcessCost which user wants to pay
        let maxDPCost = _.has(args, 'maxDPCost') ? args.maxDPCost : 0;
        let DPCostChangeIndex = _.has(args, 'DPCostChangeIndex') ? args.DPCostChangeIndex : outputs.length - 1;
        let supportedP4PTrxLength = _.has(args, 'supportedP4PTrxLength') ? iutils.convertBigIntToJSInt(args.supportedP4PTrxLength) : 0;
        if (!utils._nilEmptyFalse(dDPCost))
            maxDPCost = dDPCost;

        if (utils.objKeys(inputs).length == 0) {
            msg = `try to make transaction without inputs!`;
            clog.trx.error(msg);
            console.log(`msg ${msg}`);
            return { err: true, msg };
        }
        if (utils._nilEmptyFalse(inputsValue)) {
            inputsValue = 0;
            for (let aCoin of utils.objKeys(inputs)) {
                inputsValue += inputs[aCoin].value;
            };
        }
        if (utils._nilEmptyFalse(inputsValue) || (inputsValue == 0)) {
            msg = `try to make transaction without inputs!!`;
            clog.trx.error(msg);
            console.log(`msg ${msg}`);
            return { err: true, msg };
        }
        if (utils._nilEmptyFalse(outputs) || (outputs.length == 0)) {
            msg = `try to make transaction without output accounts!`;
            clog.trx.error(msg);
            console.log(`msg ${msg}`);
            return { err: true, msg };
        }
        outputsValue = 0;
        if (utils._nilEmptyFalse(outputsValue)) {
            outputs.forEach(out => {
                outputsValue += out[1];
                if (parseInt(out[1]) < 0) {
                    msg = `make trx, at least one negative output exist!`;
                    clog.trx.error(msg);
                    console.log(`msg ${msg}`);
                    return { err: true, msg };
                }
            });
        }
        if (utils._nilEmptyFalse(outputsValue) || (outputsValue == 0)) {
            msg = `try to make transaction without outputs!!`;
            clog.trx.error(msg);
            console.log(`msg ${msg}`);
            return { err: true, msg };
        }

        let backersAddresses = _.has(args, 'backersAddresses') ? args.backersAddresses : null;
        if (utils._nilEmptyFalse(backersAddresses) || (Array.isArray(backersAddresses) && (backersAddresses.length == 0))) {
            let machineSettings = machine.getMProfileSettingsSync();
            backersAddresses = [machineSettings.backerAddress];  // in case of clone, a transaction can have more than one backer
        }

        // signing the transaction
        let dExtInfo = [];
        let theCoins = utils.arrayUnique(utils.objKeys(inputs)).sort(); //BIP69  inputs
        let coinsTuple = theCoins.map(x => [iutils.unpackCoinRef(x).docHash, iutils.unpackCoinRef(x).outputIndex]);
        for (let aCoin of theCoins) {
            let signatures = [];
            for (let aPrivateKey of inputs[aCoin].privateKeys) {
                // console.log(`\n aPrivateKey ${utils.stringify(aPrivateKey)}\n`);
                let signature = signingInputOutputs({
                    creationDate: creationDate,
                    inputs: coinsTuple,
                    outputs,
                    prvKey: aPrivateKey,
                    sigHash
                });
                signatures.push([signature, sigHash]);
            }
            dExtInfo.push(
                {
                    uSet: inputs[aCoin].uSet,
                    // bechUnlockers: inputs[aCoin].bechUnlockers,
                    signatures
                }
            );

        }

        let trx = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dLen: "0000000",
            dType,
            dClass,
            dPIs: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],   // data & process cost, which are payed by one output(or more output for cloned transactions)
            dVer,
            ref,
            description,
            creationDate,
            inputs: coinsTuple,
            outputs,
            dExtInfo,
            dExtHash: '0000000000000000000000000000000000000000000000000000000000000000',
        };



        // adding Data & Process Cost for transaction
        let extraCharBecauseOfNewOutputs = backersAddresses.length * utils.stringify(['00000000000000000000000000000000000000000000000000000000000000', '00000000000000000000']).length;
        // let dLen = utils.stringify(trx).length + iConsts.TRANSACTION_PADDING_LENGTH;

        trx.dLen = iutils.offsettingLength(utils.stringify(trx).length);
        let trxDPCostRes = calcTrxDPCost({
            cDate: tNow,
            trx,
            supportedP4PTrxLength,
            extraLength: extraCharBecauseOfNewOutputs,
            stage: iConsts.STAGES.Creating
        });
        clog.app.info(`trx DPCost Res: ${utils.stringify(trxDPCostRes)}`);

        if (trxDPCostRes.err != false)
            return trxDPCostRes;

        // let eDPCost = Math.floor((dLen + extraCharBecauseOfNewOutputs) * microPAIPerByte);  //effective DPCost
        // if (dClass == iConsts.TRX_CLASSES.P4P) {
        //     eDPCost += Math.floor((supportedP4PTrxLength) * microPAIPerByte) + 1;  //new effective DPCost. +1 is because of possible aggregated 2 floats excedds 1.00
        //     console.log('exxxxxxxxx PAI because of P4P', Math.floor((supportedP4PTrxLength) * microPAIPerByte) + 1);
        // }

        let finalDPCost = 0;
        if (!utils._nilEmptyFalse(dDPCost)) {
            if (trxDPCostRes.cost > dDPCost) {
                msg = `Transaction calculated cost(${trxDPCostRes.cost})microPAIs is heigher than your desired(${dDPCost})microPAIs`;
                clog.trx.error(msg);
                console.log(`msg ${msg}`);
                return { err: true, msg };
            } else {
                finalDPCost = dDPCost;
            }
        } else {
            if ((maxDPCost > 0) && (trxDPCostRes.cost > maxDPCost)) {
                msg = `Transaction trxDPCostRes.cost(${trxDPCostRes.cost})microPAIs is heigher than your max(${maxDPCost})microPAIs`;
                clog.trx.error(msg);
                console.log(`msg ${msg}`);
                return { err: true, msg };
            } else {
                finalDPCost = trxDPCostRes.cost;
            }
        }



        // adding DP Costs payment outputs
        let dPKeys = [];
        backersAddresses.forEach(aBacker => {
            dPKeys.push([aBacker, finalDPCost].join());
            trx.outputs.push([aBacker, finalDPCost])
        });
        console.log(`adjusting trx output[${DPCostChangeIndex}] because of DPCosts change`);

        console.log(`sum inputsValue: ${inputsValue}`);
        console.log(`sum outputsValue: ${outputsValue}`);
        console.log(`sum DPCost: ${(finalDPCost * backersAddresses.length)}`);

        let change = inputsValue - outputsValue - (finalDPCost * backersAddresses.length);//TODO add possibilitiy for user to pay diffrent DPCost to different backers(in case of cloning trx)
        console.log(`change [${change}] because of DPCosts `);
        let finalChange = iutils.convertBigIntToJSInt(trx.outputs[DPCostChangeIndex][1]) + change;
        if (finalChange > 0) {
            trx.outputs[DPCostChangeIndex][1] = iutils.convertBigIntToJSInt(trx.outputs[DPCostChangeIndex][1]) + change;

            // test for unequality
            if (unEqua > 0)
                trx.outputs[DPCostChangeIndex][1]++;

        } else {
            trx.outputs = utils.arrayRemoveByIndex(trx.outputs, DPCostChangeIndex);
        }

        trx.outputs = trxHashHandler.normalizeOutputs(trx.outputs).normalizedOutputs; // BIP69 outputs
        let dPIs = [];
        dPKeys.forEach(aDPKey => {
            dPIs.push(trxHashHandler.getOutputIndexByAddressValue(trx.outputs, aDPKey));
        });
        trx.dPIs = utils.arrayUnique(dPIs).sort();

        // double-check outputs
        for (let anOut of trx.outputs) {
            if (parseInt(anOut[1]) <= 0) {
                msg = `at least one negative/zero output exist`;
                clog.trx.error(msg);
                clog.trx.error(trx);
                console.log(`msg ${msg}`);
                return { err: true, msg };
            }
        }

        // again signing, because of output re-ordering
        trx.dExtInfo = [];
        for (let aCoin of theCoins) {
            let signatures = [];
            for (let aPrivateKey of inputs[aCoin].privateKeys) {

                let signature = signingInputOutputs({
                    creationDate: creationDate,
                    inputs: coinsTuple,
                    outputs: trx.outputs,
                    prvKey: aPrivateKey,
                    sigHash
                });
                signatures.push([signature, sigHash]);
            }
            trx.dExtInfo.push(
                {
                    uSet: inputs[aCoin].uSet,
                    // bechUnlockers: inputs[aCoin].bechUnlockers,
                    signatures
                }
            );

        }
        trx.dExtHash = trxHashHandler.calcTrxExtRootHash(trx.dExtInfo);
        trx.dLen = iutils.offsettingLength(utils.stringify(trx).length);
        trx.hash = trxHashHandler.doHashTransaction(trx);
        // console.log(`trx444:  ${utils.stringify(trx)}`);

        return { err: false, trx, DPCost: finalDPCost };
    }
}
module.exports = TransactionMake;
