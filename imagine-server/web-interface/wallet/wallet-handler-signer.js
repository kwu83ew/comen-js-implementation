const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const walletAddressHandler = require('./wallet-address-handler');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const machine = require('../../machine/machine-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');

async function _walletSigner(args) {
    console.log(`walletSigner: ${utils.stringify(args)}`);
    clog.http.info(`wallet Signer args: ${utils.stringify(args)}`);
    let spUTXOs = _.has(args, 'spUTXOs') ? args.spUTXOs : null; // the spent UTXOs
    let spReceiver = _.has(args, 'spReceiver') ? args.spReceiver : null;    // the money receiver
    let spChangeBack = _.has(args, 'spChangeBack') ? args.spChangeBack : null;
    let spPais = _.has(args, 'spPais') ? utils.floor(args.spPais * iConsts.PAI) : null;    // transfered value by microPAI
    let spOutputUnit = _.has(args, 'spOutputUnit') ? utils.floor(args.spOutputUnit * iConsts.PAI) : 0;     // ouput units in microPAI
    let dDPCost = _.has(args, 'spDPCost') ? utils.floor(args.spDPCost * iConsts.PAI) : null;     // backer fee in microPAI
    let description = _.has(args, 'description') ? args.description : "";

    let msg = '';
    if (utils._nilEmptyFalse(spUTXOs) || utils._nilEmptyFalse(spReceiver) || utils._nilEmptyFalse(spPais) || utils._nilEmptyFalse(dDPCost)) {
        msg = `Missed info to Sign a trx ${JSON.stringify(args)}`;
        clog.sec.error(msg);
        return { err: true, msg };
    }

    //TODO: controll if mentioned UTXOs are spendable and avoid double spending

    let addresses = [];
    let sumUTXOs = 0;
    let inputsDict = {};
    for (let i = 0; i < spUTXOs.length; i++) {
        addresses.push(spUTXOs[i].address);
        let coin = iutils.packCoinRef(spUTXOs[i].trx_hash, spUTXOs[i].o_index);
        clog.trx.info(`sign an input ref_loc: ${coin}`)
        let utInfo = await utxoHandler.getCoinsInfoASync({
            coins: [coin]
        });
        if (utils._nilEmptyFalse(utInfo) || (utInfo.length == 0)) {
            msg = `can not sign input ref_loc: ${refLoc}. because doesn't exist in trx_utxos`;
            clog.sec.error(msg);
            return { err: true, msg };
        }
        sumUTXOs += iutils.convertBigIntToJSInt(utInfo[0].utOValue);
        inputsDict[coin] = {
            value: utInfo[0].utOValue
        }
    }

    let changeBack = sumUTXOs - spPais - dDPCost;
    if (changeBack < 0) {
        msg = `Output more than inut fund! ${sumUTXOs}-${spPais}-${dDPCost}<0`;
        clog.sec.error(msg);
        return { err: true, msg: msg };
    }

    let machineSettings = await machine.getMProfileSettingsAsync();
    let outputs = [];
    outputs.push([(utils._nilEmptyFalse(spChangeBack) ? machineSettings.backerAddress : spChangeBack), changeBack]);  // change back

    if (spOutputUnit == 0) {
        outputs.push([spReceiver, spPais]) // receiver
        
    } else {
        while (spPais >= spOutputUnit) {
            outputs.push([spReceiver, spOutputUnit]);
            spPais -= spOutputUnit;
        }
        if (spPais > 0)
            outputs.push([spReceiver, spPais]);
    }
    clog.trx.info(`trx outputs: ${outputs}`);

    let addressesInfo = walletAddressHandler.getAddressesInfoSync(addresses);

    let addressesDict = {};
    addressesInfo.forEach(element => {
        addressesDict[element.waAddress] = {};
        addressesDict[element.waAddress]['title'] = element.waTitle;
        addressesDict[element.waAddress]['detail'] = utils.parse(element.waDetail);
    });

    // find the unlockers
    let utxoInfo = {};
    let inputs = [];
    for (let utxo of spUTXOs) {
        utxoInfo[utxo.address] = utxo;
        let unlockerIndex = _.has(utxo, 'unlockerIndex') ? utxo.unlockerIndex : 0; // if client didn't mention, chose the first unlock struct FIXME =0

        inputs.push([utxo.trx_hash, utxo.o_index]);

        let uSet = addressesDict[utxo.address].detail.uSets[unlockerIndex];
        let coin = iutils.packCoinRef(utxo.trx_hash, utxo.o_index);
        inputsDict[coin].address = utxo.address;
        inputsDict[coin].uSet = uSet;
        inputsDict[coin].privateKeys = addressesDict[utxo.address].detail.privContainer[uSet.salt];
    }
    console.log(`\n\n inputsDict: ${utils.stringify(inputsDict)}\n`);

    let newArgs = {

        dDPCost,
        DPCostChangeIndex: 0, // to change back

        dType: iConsts.DOC_TYPES.BasicTx,
        dClass: iConsts.TRX_CLASSES.SimpleTx,

        description: '',
        inputs: inputsDict,
        outputs: outputs

    }

    let trxDtl = args._super.makeATransaction(newArgs);
    if (trxDtl.err != false)
        return trxDtl;
    console.log(`trxDtl: ${utils.stringify(trxDtl)}`);
    clog.trx.info(`_walletSigner. trxDtl: ${JSON.stringify(trxDtl)}`);

    // push transaction to Block buffer
    await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
    const walletHandlerLocalUTXOs = require("./wallet-handler-local-utxos");
    walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

    return {};

}

module.exports = _walletSigner;
