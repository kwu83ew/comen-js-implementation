const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');


const table = 'i_machine_used_coins';


class WalletHandlerLocalUTXOs {

    static async doesUsedUTXO(trx) {
        let mpCode = iutils.getSelectedMProfile();
        console.log('does Used UTXO');

        // un-mark un used utxos
        let restoreQ = `
            DELETE FROM ${table} 
            WHERE lu_mp_code=$1 
            AND lu_insert_date < $2
            AND lu_ref_loc IN (SELECT ut_coin FROM i_trx_utxos) 
            AND lu_spend_loc NOT IN (SELECT dbm_doc_hash FROM i_docs_blocks_map)`;
        model.sQuery(restoreQ, [mpCode, utils.minutesBefore(iConsts.getCycleByMinutes())]);

        let spendStatus = {}
        for (let i = 0; i < trx.inputs.length; i++) {
            let lu_ref_loc = iutils.packCoinRef(trx.inputs[i][0], trx.inputs[i][1]);
            let res = await model.aRead({
                table,
                query: [
                    ['lu_mp_code', mpCode],
                    ['lu_ref_loc', lu_ref_loc]
                ]
            });
            spendStatus[lu_ref_loc] = res
        }
        clog.app.info(`spendStatus spendStatus spendStatus ${utils.stringify(spendStatus)}`);

        let usedUTXOs = []
        utils.objKeys(spendStatus).forEach(element => {
            if (spendStatus[element].length > 0)
                usedUTXOs.push(element);
        });
        if (usedUTXOs.length > 0)
            return true;

        return false;
    }

    static locallyMarkUTXOAsUsed(trx) {
        let mpCode = iutils.getSelectedMProfile();
        for (let i = 0; i < trx.inputs.length; i++) {
            let refLoc = iutils.packCoinRef(trx.inputs[i][0], trx.inputs[i][1]);
            model.sCreate({
                table,
                values: {
                    lu_mp_code: mpCode,
                    lu_ref_loc: refLoc,
                    lu_spend_loc: trx.hash,
                    lu_insert_date: utils.getNow()
                }
            });
        }
    }

    static searchLocallyMarkedUTXOs(args) {
        let query = _.has(args, 'query') ? args.query : [];
        let mpCode = iutils.getSelectedMProfile();
        query.push(['lu_mp_code', mpCode]);

        let res = model.sRead({
            table,
            query
        });
        return res;
    }

    static restorUnUsedUTXOs() {
        let mpCode = iutils.getSelectedMProfile();
        // retrtieve all marked as spen which are not recorded in DAG and markewd as used before than 2 cycle
        let q = `
            SELECT * FROM ${table} WHERE lu_mp_code=$1 AND lu_ref_loc NOT IN (SELECT sp_coin FROM i_trx_spend) 
            AND lu_insert_date<$2
        `;
        let cDate = utils.minutesBefore(iConsts.getCycleByMinutes());
        let res = model.sQuery(q, [mpCode, cDate]);
        console.log(`found ${res.length} unused UTXOs`);
        for (let row of res) {
            this.removeRef(row.lu_ref_loc);
        }
    }

    static removeRef(luRefLoc) {
        let mpCode = iutils.getSelectedMProfile();
        console.log(`removing unused UTXO from i machine_used_utxos (${iutils.shortCoinRef(luRefLoc)})`);
        model.sDelete({
            table,
            query: [
                ['lu_mp_code', mpCode],
                ['lu_ref_loc', luRefLoc]
            ]
        });
    }

}

module.exports = WalletHandlerLocalUTXOs;
