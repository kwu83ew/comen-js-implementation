const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const walletAddressHandler = require('./wallet-address-handler');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const rejectedTrxHandler = require('../../dag/normal-block/rejected-transactions-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');

class UTXOPicker {

    static getSomeCoins(args) {
        let msg;
        const minimumSpendable = _.has(args, 'minimumSpendable') ? args.minimumSpendable : iConsts.MINIMUM_TRANSACTION_FEE;  // atleat 1 pai for transaction fee
        let excludeRefLocs = _.has(args, 'excludeRefLocs') ? args.excludeRefLocs : [];
        const selectionMethod = _.has(args, 'selectionMethod') ? args.selectionMethod : 'random'; // it could be also biggerFirst, smallerFirst
        const unlockerIndex = _.has(args, 'unlockerIndex') ? args.unlockerIndex : 0;

        msg = `fetch some inputs equal to ${utils.microPAIToPAI6(minimumSpendable)} PAIs to use in a transaction`;
        clog.trx.info(msg);


        // retrieve rejected transactions
        let rejectedCoins = rejectedTrxHandler.searchRejTrx();
        rejectedCoins = rejectedCoins.map(x => x.rt_coin);
        clog.trx.info(`rejected RefLocs1: ${rejectedCoins.map(x => iutils.shortCoinRef(x))}`);

        // intentioinally sending rejected funds to test network
        if (_.has(args, 'allowedToUseRejectedCoinsToTest') && (args.allowedToUseRejectedCoinsToTest == true)) {
            rejectedCoins = [];
            clog.trx.info(`rejected RefLocs2: ${rejectedCoins.map(x => iutils.shortCoinRef(x))}`);
        }

        let addressesDict = {};
        let addressesInfo = walletAddressHandler.getAddressesListSync();

        for (let address of addressesInfo) {
            addressesDict[address.waAddress] = {
                title: address.waTitle,
                detail: JSON.parse(address.waDetail)
            };
        }
        let addresses = utils.objKeys(addressesDict);
        clog.trx.info(`The ${addresses.length} addresses which are controlled by wallet/profile: ${addresses.map(x => iutils.shortBech8(x))}`);

        let spendableUTXOs = utxoHandler.extractUTXOsBYAddresses(addresses);
        if (spendableUTXOs.length == 0) {
            msg = `Wallet couldn't find proper coins(${utils.sepNum(minimumSpendable)} micro PAI) to spend!`;
            clog.trx.info(msg);
            return { err: true, msg };
        }
        clog.trx.info(`going to select one of these ${spendableUTXOs.length} UTXOs: ${utils.stringify(spendableUTXOs)}`);


        let locallyMarkedUTXOs = walletHandlerLocalUTXOs.searchLocallyMarkedUTXOs().map(x => x.lu_ref_loc);
        clog.trx.info(`Localy marked UTXOs as spend coins(${locallyMarkedUTXOs.length}): ${locallyMarkedUTXOs.map(x => iutils.shortCoinRef(x))}`);
        let tmpCoins = [];
        for (let aCoin of spendableUTXOs) {
            if (!locallyMarkedUTXOs.includes(aCoin.utCoin))
                tmpCoins.push(aCoin)
        }
        spendableUTXOs = tmpCoins;
        clog.trx.info(`spendable coins after removing localy markewd coins (${spendableUTXOs.length} coins): ${spendableUTXOs.map(x => iutils.shortCoinRef(x.utCoin))}`);
        if (spendableUTXOs.length == 0) {
            msg = `Wallet hasn't any un-spended coins to use!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let sumCoinValues = 0;
        let selectedCoins = {};
        let loopXCount = 0; // to prevent unlimited cycle
        while ((sumCoinValues < minimumSpendable) && (spendableUTXOs.length > 0) && (loopXCount < 1000)) {
            loopXCount++;

            clog.trx.info(`Get Some Inputs, excluding ${excludeRefLocs.length} UTXOs ${excludeRefLocs.map(x => iutils.shortCoinRef(x))}`);

            // exclude used refs
            let tmpSpendableUTXOs = [];
            spendableUTXOs.forEach(elm => {
                if (!excludeRefLocs.includes(elm.utCoin) &&
                    !rejectedCoins.includes(elm.utCoin)
                ) {
                    tmpSpendableUTXOs.push(elm);
                }
            });
            spendableUTXOs = tmpSpendableUTXOs;
            clog.trx.info(`going to select one of these real spendable ${spendableUTXOs.length} UTXOs: ${utils.stringify(spendableUTXOs)}`);

            let selectedUtxo = null;
            let values = [];
            let objByValues = {};
            switch (selectionMethod) {
                case 'precise':
                    values = [];
                    objByValues = {};
                    for (let elm of spendableUTXOs) {
                        values.push(elm.utOValue);
                        objByValues[elm.utOValue] = elm;
                    }
                    for (let aValue of values.sort()) {
                        if ((selectedUtxo == null) && (aValue >= (minimumSpendable - sumCoinValues)))
                            selectedUtxo = objByValues[aValue];
                    }
                    if (selectedUtxo == null)
                        selectedUtxo = objByValues[values.sort().reverse()[0]];
                    break;

                case 'biggerFirst':
                    values = [];
                    objByValues = {};
                    for (let elm of spendableUTXOs) {
                        values.push(elm.utOValue);
                        objByValues[elm.utOValue] = elm;
                    }
                    selectedUtxo = objByValues[values.sort().reverse()[0]]
                    break;

                case 'smallerFirst':
                    // console.log('spendableUTXOs', spendableUTXOs);
                    values = [];
                    objByValues = {};
                    for (let elm of spendableUTXOs) {
                        values.push(elm.utOValue);
                        objByValues[elm.utOValue] = elm;
                    }
                    selectedUtxo = objByValues[values.sort()[0]]
                    break;

                case 'random':
                default:
                    let spendableInx = Math.floor(Math.random() * spendableUTXOs.length);
                    selectedUtxo = spendableUTXOs[spendableInx];
                    break;
            }

            clog.trx.info(`The selected UTXO: ${utils.stringify(selectedUtxo)}`);
            if (!utils._nilEmptyFalse(selectedUtxo)) {
                // console.log('selectedUtxo', selectedUtxo);
                excludeRefLocs.push(selectedUtxo.utCoin);
                sumCoinValues += selectedUtxo.utOValue;

                let detail = addressesDict[selectedUtxo.utOAddress].detail;
                let anUnlockSet = detail.uSets[unlockerIndex];

                selectedCoins[selectedUtxo.utCoin] = {
                    "address": selectedUtxo.utOAddress,
                    "value": selectedUtxo.utOValue,
                    uSet: anUnlockSet,
                    privateKeys: detail.privContainer[anUnlockSet.salt]
                };
            }
            clog.trx.info(`The selected Coins: ${utils.stringify(selectedCoins)}`);

        }


        if (utils.objKeys(selectedCoins).length == 0) {
            msg = 'no input selected to spend'
            clog.app.info(msg);
            return { err: true, msg };
        }


        return { err: false, selectedCoins, sumCoinValues };

    }

}

module.exports = UTXOPicker;
