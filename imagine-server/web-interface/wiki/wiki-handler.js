const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandler = require('../wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../wallet/wallet-handler-local-utxos');
const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');
const flensHandler = require('../../contracts/flens-contract/flens-handler');
const rendererHandler = require('../../services/render-handler/render-handler');

class WikiHandler {

    static createNewWikiPage(args) {
        console.log(`build/edit A New wiki args: ${utils.stringify(args)}`);
        clog.app.info(`build/edit A New wiki args: ${utils.stringify(args)}`);
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // create proper doc and send it to network
        let newWikiPageRegRes = freeDocHandler.wiki.pages.prepareNewWikiPageRegReq(args);
        clog.app.info(`new wiki RegRes: ${utils.stringify(newWikiPageRegRes)}`);
        if (newWikiPageRegRes.err != false)
            return newWikiPageRegRes;

        // prepare payment doc too
        return freeDocHandler.payForFreeDocAndPushToBuffer({ fDoc: newWikiPageRegRes.fDoc, dTarget });
    }

    // static editAWikiPage(args) {
    //     console.log(`edit a wiki args: ${utils.stringify(args)}`);
    //     clog.app.info(`edit a wiki args: ${utils.stringify(args)}`);
    //     let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

    //     // create proper doc and send it to network
    //     let newWikiPageRegRes = freeDocHandler.wiki.prepareEditWikiPageRegReq(args);
    //     clog.app.info(`edit wiki RegRes: ${utils.stringify(newWikiPageRegRes)}`);
    //     if (newWikiPageRegRes.err != false)
    //         return newWikiPageRegRes;

    //     // prepare payment doc too
    //     return freeDocHandler.payForFreeDocAndPushToBuffer({ fDoc: newWikiPageRegRes.fDoc, dTarget });
    // }

    static getWkPageInfo(args) {
        // console.log('get WkPageInfo args', args);
        let wkpUniqueHash = _.has(args, 'wkpUniqueHash') ? args.wkpUniqueHash : '';
        let renderToHtml = _.has(args, 'renderToHtml') ? args.renderToHtml : true;
        let query = _.has(args, 'query') ? args.query : [];
        let iName, wkPgTitle;
        if (wkpUniqueHash != '') {
            query.push(['wkp_unique_hash', wkpUniqueHash])
        } else {
            iName = _.has(args, 'iName') ? args.iName : '';
            wkPgTitle = _.has(args, 'wkPgTitle') ? args.wkPgTitle : '';
            if (utils._nilEmptyFalse(iName) || utils._nilEmptyFalse(wkPgTitle))
                return { err: true, msg: `missed requested page ${utils.stringify(args)}` }
                
            let inHash = iutils.convertTitleToHash(iName);
            query.push(['wkp_title', wkPgTitle]);
            query.push(['wkp_in_hash', inHash]);
        }

        let onchainRes = freeDocHandler.wiki.pages.searchInWikiPages({
            query,
            needContentsToo: true
        });
        if (onchainRes.err != false) {
            console.log(`onchainRes ${utils.stringify(onchainRes)}`);
        }
        if (onchainRes.records.length == 0) {
            return { err: false, record: { content: `the page "${wkPgTitle}" does not exist! would you like to create it?` } }
        }
        let record = onchainRes.records[0];
        if (renderToHtml)
            record.content = rendererHandler.renderToHTML(record.content);
        return { err: false, record };
    }

    static getOnchainWkPages(args) {
        let onchainRes = freeDocHandler.wiki.pages.searchInWikiPages({ needContentsToo: false });
        if (onchainRes.err != false) {
            console.log(`onchainRes ${utils.stringify(onchainRes)}`);
            return onchainRes;
        }
        let records = onchainRes.records;
        // retrieve wiki iNames info
        let iNHashes = records.map(x => x.wkp_in_hash);
        let iNames = flensHandler.register.searchRegisteredINames({
            needBindingsInfoToo: false,
            query: [
                ['in_hash', ['IN', utils.arrayUnique(iNHashes)]],
            ]
        });
        let iNamesDict = {};
        for (let anIName of iNames.records) {
            iNamesDict[anIName.inHash] = anIName.iName;
        }
        // console.log('iNamesDict, iNamesDict', iNamesDict);
        for (let aRec of records) {
            aRec['iName'] = (_.has(iNamesDict, aRec.wkp_in_hash)) ? iNamesDict[aRec.wkp_in_hash] : '-';
            aRec['url'] = `${aRec['iName']}/wiki/${aRec.wkp_title}`;
            aRec.content = rendererHandler.renderToHTML(aRec.content);
        }
        // console.log('iNames, records', records);

        return { err: false, records };
    }

}


module.exports = WikiHandler;
