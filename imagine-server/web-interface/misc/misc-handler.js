const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const machine = require('../../machine/machine-handler');
const iNameHandler = require('../iname/iname-handler');
const flensHandler = require('../../contracts/flens-contract/flens-handler');


class MiscHandler {

    static async registreIName(args) {
        let res = await flensHandler.register.iNameRegReq({
            dTarget: args.dTarget,
            iName: args.iName,
            iNameOwner: machine.getMProfileSettingsSync().backerAddress
        });
        console.log('registr IName res', res);
        return res;
    }

    static estimateINameRegReqCost(args) {
        // calculate flens cost
        let iNameCost = flensHandler.estimateINameRegReqCost({
            cDate: utils.getNow(),
            stage: iConsts.STAGES.Creating,
            iName: args.iName,
            dType: iConsts.DOC_TYPES.INameReg,
            dClass: iConsts.INAME_CLASESS.NoDomain,
        });
        return iNameCost;
    }
}

module.exports = MiscHandler


