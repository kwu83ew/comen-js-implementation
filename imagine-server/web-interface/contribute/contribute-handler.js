const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
const pPledgeHandler = require('../../contracts/pledge-contract/proposal-pledge-contract');
const pledgeHandler = require('../../contracts/pledge-contract/pledge-handler');
const loanHandler = require('../../contracts/loan-contract/loan-contract');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const crypto = require('../../crypto/crypto')
const machine = require('../../machine/machine-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const GQLHandler = require('../../services/graphql/graphql-handler')
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');

class ContributeHandler {

    static async reviewBallotAndComments(args) {
        return { err: false, msg: 'Must be implemented' };
    }

    /**
     * voting a proposal
     * 
     * @param {} args 
     * 
     */
    static async voteProposal(args) {
        let votingRes = await pollHandler.ballotHandler.doVote({
            dTarget: _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER,
            ref: args.pllHash,
            vote: args.vote,
            voteComment: args.voteComment
        });
        return votingRes;
    }

    static deleteDraftPledgeReq(args) {
        let msg;
        clog.app.info(`deleteDraftPledgeReq args: ${utils.stringify(args)}`);
        let draftPledgeId = _.has(args, 'dplId') ? args.dplId : null;
        return pPledgeHandler.removeDraftPledge({
            query: [['dpl_id', draftPledgeId]]
        });
    }

    static sendLoanRequest(args) {
        let msg;
        clog.app.info(`sendLoanRequest args: ${utils.stringify(args)}`);
        let draftPledgeId = _.has(args, 'dplId') ? args.dplId : null;
        let pledgeeEmail = _.has(args, 'lrPledgeeEmail') ? args.lrPledgeeEmail : null;

        // control if neighbor exist in db
        let neightbors = machine.neighborHandler.getNeighborsSync({ query: [['n_email', pledgeeEmail]] });
        if (neightbors.length != 1) {
            msg = `The pledgee email "${pledgeeEmail}" doesn't exist in your neighbors email`;
            return { err: true, msg }
        }

        // retrieve draft loan request
        let draftPledges = pledgeHandler.searchInDraftPledges({ query: [['dpl_id', draftPledgeId]] });
        if (draftPledges.length != 1) {
            msg = `The loan request "${draftPledgeId}" doesn't exist in your draft loan requests`;
            return { err: true, msg }
        }
        let draftPledge = draftPledges[0];

        //retrieve draft proposal
        let proposals = proposalHandler.searchInDraftProposal({
            query: [['pd_hash', draftPledge.dplProposalRef]]
        });
        if (proposals.length != 1) {
            msg = `The draft proposal "${utils.hash6c(draftPledge.dplProposalRef)}" doesn't exist in your draft loan requests`;
            return { err: true, msg }
        }
        // console.log('proposal', proposals);
        let proposal = proposals[0];

        let finalLoanReq = {
            proposal: utils.parse(proposal.pdBody),
            pledge: utils.parse(draftPledge.dplBody),
        };
        // console.log('finalLanReq', utils.stringify(finalLoanReq));

        let { code, body } = GQLHandler.makeAPacket({
            cards: [
                {
                    cdType: GQLHandler.cardTypes.ProposalLoanRequest,
                    cdVer: '0.0.4',
                    proposal: utils.parse(proposal.pdBody),
                    pledgerSignedPledge: utils.parse(draftPledge.dplBody)
                }
            ]
        });

        let pushRes = sendingQ.pushIntoSendingQ({
            sqType: iConsts.CONSTS.GQL,
            sqCode: code,
            sqPayload: body,
            sqReceivers: [pledgeeEmail],
            sqTitle: `GQL ProposalLoanRequest packet(${utils.hash6c(code)})`,
        });
        if (pushRes.err != false) {
            return pushRes;
        }

        return { err: false, msg: `Proposal Loan Request sent to pledgee` }
    }

    static sendLReqToAgora(args) {
        let msg;
        clog.app.info(`send L Req To Agora Pay By POW args: ${utils.stringify(args)}`);
        let draftPledgeId = _.has(args, 'dplId') ? args.dplId : null;
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // retrieve draft loan request
        let draftPledges = pledgeHandler.searchInDraftPledges({ query: [['dpl_id', draftPledgeId]] });
        if (draftPledges.length != 1) {
            msg = `The loan request "${draftPledgeId}" doesn't exist in your draft loan requests`;
            return { err: true, msg }
        }
        let draftPledge = draftPledges[0];

        //retrieve draft proposal
        let proposals = proposalHandler.searchInDraftProposal({
            query: [['pd_hash', draftPledge.dplProposalRef]]
        });
        if (proposals.length != 1) {
            msg = `The draft proposal "${utils.hash6c(draftPledge.dplProposalRef)}" doesn't exist in your draft loan requests`;
            return { err: true, msg }
        }
        // console.log('proposal', proposals);
        let proposal = proposals[0];
        proposal = utils.parse(proposal.pdBody);

        let finalLoanReq = {
            proposal,
            pledge: utils.parse(draftPledge.dplBody),
        };
        // console.log('finalLanReq', utils.stringify(finalLoanReq));

        let iNameHash = iutils.convertTitleToHash('imagine');
        let agoraHash = iutils.convertTitleToHash('Pre Proposals and Discussions');
        let agoraUniqueHash = crypto.keccak256(`${iNameHash}/${agoraHash}`);

        let newPostRes = freeDocHandler.demos.posts.prepareNewPost({
            signer: proposal.shareholder,
            napAgUqHash: agoraUniqueHash,   // uniq hash of Agora of 'Pre Proposals and Discussions'
            dAttrs: { isPreProposal: iConsts.CONSTS.YES },
            napOpinion: finalLoanReq
        });
        clog.app.info(`new Agora post Res: ${utils.stringify(newPostRes)}`);
        if (newPostRes.err != false)
            return newPostRes;

        let costPayMode = _.has(args, 'costPayMode') ? args.costPayMode : 'normal';
        if (costPayMode == 'byPoW') {
            // push to Block buffer and publish immediately

            let res = docBufferHandler.pushInSync({ doc: newPostRes.fDoc, DPCost: 0 });
            if (res.err != false)
                return res;

            const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
            let r = wBroadcastBlock.broadcastBlock({ costPayMode: 'byPoW' });
            return r;

        } else {
            // prepare payment doc too
            let buffRes = freeDocHandler.payForFreeDocAndPushToBuffer({ fDoc: newPostRes.fDoc, dTarget });
            if (buffRes.err != false)
                clog.app.error(buffRes);
            return buffRes;

        }
    }

    static async signAndPayProposalCosts(args) {
        let msg;
        clog.app.info(`sign AndPayProposalCosts args: ${utils.stringify(args)}`);
        let stage = iConsts.STAGES.Creating;

        let draft = proposalHandler.searchInDraftProposal({
            query: [
                ['pd_id', args.pdId]
            ]
        });
        console.log(draft);
        if (draft.length != 1)
            return { err: true, msg: `The proposal draft(${args.pdId}) does not exist in Draft proposals` };

        draft = draft[0];
        clog.app.info(`preparing to pay for proposal ${draft.pdBody} `);

        let proposal = utils.parse(draft.pdBody);
        if (parseInt(proposal.dLen) != draft.pdBody.length) {
            msg = `The proposal length(${draft.pdBody.length}), is not same as proposal${draft.pdBody}`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let proposalDocumetnCost = proposalHandler.calcProposalDocumetnCost({
            cDate: utils.getNow(),
            proposal,
            stage
        });
        if (proposalDocumetnCost.err != false)
            return proposalDocumetnCost;


        let { oneCycleIncome, applyCost } = proposalHandler.calcProposalApplyCost({
            helpHours: proposal.helpHours,
            helpLevel: proposal.helpLevel,
        });
        clog.app.info(`the proposal(${utils.hash6c(proposal.hash)}) oneCycleIncome(${oneCycleIncome}) proposalDocumetnCost(${proposalDocumetnCost.cost}) + applyCost(${applyCost})`);
        let proposalFinalCost = proposalDocumetnCost.cost + applyCost;

        // create a transaction for payment
        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_PROPOSAL', proposalFinalCost]
        ];
        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(proposalFinalCost * 1.2) + proposalFinalCost // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(proposalFinalCost * 0.7),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: proposal.hash,
            description: 'Payed(by Proposer) for applying proposal to Vote process',
            inputs: inputs,
            outputs: outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        clog.app.info(`signed proposal cost trx: ${utils.stringify(trxDtl)}`);

        // push trx & proposal Req to Block buffer
        let res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = await docBufferHandler.pushInAsync({ doc: proposal, DPCost: proposalFinalCost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);


        if (args.dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `The proposal(${utils.hash6c(proposal.hash)}) was signed and pushed to Block Buffer` }

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }

    static bindProposalLoanPledge(args) {
        let msg;
        clog.app.info(`bind ProposalLoanPledge args: ${utils.stringify(args)}`);

        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let lrRef = _.has(args, 'lrRef') ? args.lrRef : null;
        if (utils._nilEmptyFalse(lrRef)) {
            msg = `The proposal ref ${lrRef} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let lrPledgee = _.has(args, 'lrPledgee') ? args.lrPledgee : null;
        if (utils._nilEmptyFalse(lrPledgee)) {
            msg = `The Pledgee ${lrPledgee} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(lrPledgee)) {
            msg = `The Pledgee Address ${lrPledgee} is not a valid Bech32 address!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let lrArbiter = _.has(args, 'lrArbiter') ? args.lrArbiter : null;
        if (!utils._nilEmptyFalse(lrArbiter)) {
            if (!crypto.bech32_isValidAddress(lrArbiter)) {
                msg = `The Arbiter Address ${lrArbiter} is not a valid Bech32 address!`;
                clog.app.error(msg);
                return { err: true, msg }
            }
        }
        let lrAccountAddress = _.has(args, 'lrAccountAddress') ? args.lrAccountAddress : null;
        if (utils._nilEmptyFalse(lrAccountAddress)) {
            msg = `The Pledger ${lrAccountAddress} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(lrAccountAddress)) {
            msg = `The Account Address ${lrAccountAddress} is not a valid Bech32 address!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let principal = _.has(args, 'lrPrincipal') ? args.lrPrincipal : null;
        let annualInterest = _.has(args, 'lrAnnualInterest') ? args.lrAnnualInterest : null;
        let repaymentAmount = _.has(args, 'lrRepaymentAmount') ? args.lrRepaymentAmount : null;
        let repaymentSchedule = _.has(args, 'lrRepaymentSchedule') ? args.lrRepaymentSchedule : iConsts.DEFAULT_REPAYMENT_SCHEDULE;

        // let draft = proposalHandler.extractDraftByHash(lrRef);
        // let body = utils.parse(draft.pd_body);
        // console.log(draft);

        // control if signer has permitted to pledge?
        let addInfo = walletAddressHandler.getAddressesInfoSync([lrAccountAddress]);
        if (addInfo.length != 1) {
            msg = `The Invoice Address ${lrAccountAddress} is not controlled by your wallet!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let addrDtl = utils.parse(addInfo[0].waDetail);
        let pPledge = iConsts.CONSTS.NO;
        for (let anUnlockerSet of addrDtl.uSets) {
            if (pPledge == iConsts.CONSTS.YES)
                continue;

            // console.log('anUnlockerSet', anUnlockerSet);
            // console.log('sSets', anUnlockerSet.sSets);
            for (let aSign of anUnlockerSet.sSets) {
                if (aSign.pPledge == iConsts.CONSTS.YES)
                    pPledge = iConsts.CONSTS.YES;
            }
        }
        if (pPledge == iConsts.CONSTS.NO) {
            msg = `The Invoice Address ${lrAccountAddress} doesn't permitted to Pledge!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // validate repayment clauses
        let loanDtl = loanHandler.calcLoanRepayments({
            principal,
            annualInterest,
            repaymentAmount,
            repaymentSchedule,
            repaymentOffset: 0   // TODO: implement offset 
        });
        if (loanDtl.err != false) {
            return loanDtl;
        }

        // pledge account
        let pledgedContract = pPledgeHandler.doPledgeAddress({
            creationDate,
            dType: iConsts.DOC_TYPES.Pledge,
            dClass: iConsts.PLEDGE_CLASSES.PledgeP,
            proposalRef: lrRef, //proposalRef
            pledger: lrAccountAddress,
            pledgee: lrPledgee,
            arbiter: lrArbiter,
            principal,
            annualInterest,
            repaymentAmount,
            repaymentSchedule,
            repaymentsNumber: loanDtl.repaymentsNumber,
            repaymentOffset: 0  // TODO: implement offset 
        });
        // console.log(pledgedContract);
        clog.app.info(`pledgedContract: ${utils.stringify(pledgedContract)}`);
        if (pledgedContract.err != false) {
            return pledgedContract;
        }
        // save pledged contract in db
        pPledgeHandler.savepldgDraft({ draft: pledgedContract.signedPledge });

        return { err: false }
    }

    static fillLoanRequestForm(args) {
        let msg;
        // console.log(`fillLoanRequestForm args: ${utils.stringify(args)}`);
        let draft = proposalHandler.searchInDraftProposal({
            query: [
                ['pd_id', args.pdId]
            ]
        });
        // console.log(draft);
        draft = draft[0];
        let proposal = utils.parse(draft.pdBody);

        if (parseInt(proposal.dLen) != draft.pdBody.length) {
            msg = `The proposal length(${draft.pdBody.length}) is not same as proposal${draft.pdBody}`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let proposalDocumetnCost = proposalHandler.calcProposalDocumetnCost({
            cDate: utils.getNow(),
            proposal,
            stage: iConsts.STAGES.Creating
        });
        if (proposalDocumetnCost.err != false)
            return proposalDocumetnCost;

        console.log('proposalDocumetnCost proposalDocumetnCost proposalDocumetnCost', proposalDocumetnCost);

        let { oneCycleIncome, applyCost } = proposalHandler.calcProposalApplyCost({
            helpHours: proposal.helpHours,
            helpLevel: proposal.helpLevel,
        });// 3 * first month income


        // let theContribute = body.helpHours * body.helpLevel;
        // let incomeInfo = iutils.predictFutureIncomes({
        //     months: 1,
        //     annualContributeGrowthRate: 100,
        //     theContribute
        // });
        // console.log('incomeInfo', incomeInfo);

        let res = {
            lrTitle: draft.pdTitle,
            lrTags: draft.pdTags,
            lrDescription: draft.pdDescription,
            lrVotingLongevity: proposal.votingLongevity,
            lrPollingProfile: proposal.pollingProfile,
            lrAccountAddress: draft.pdCotributerAccount,
            lrRef: draft.pdHash,
            lrHelpHours: proposal.helpHours,
            lrHelpLevel: proposal.helpLevel,
            lrOneCycleIncome: oneCycleIncome,
            lrPrincipal: applyCost + proposalDocumetnCost.cost,   // 3 * first month income
        };

        // console.log('res', res);
        return res;
    }

    static async predictFutureIncomes(args) {
        // console.log(`predictFutureIncomes args: ${utils.stringify(args)}`);
        let theContribute = args.pdHelpLevel * args.pdHelpHours;
        let incomeInfo = iutils.predictFutureIncomes({
            months: 7 * 12,
            annualContributeGrowthRate: args.cAGrowth,
            theContribute
        })
        return incomeInfo;
    }

    static async prepareDraftProposal(args) {
        let msg;
        clog.app.info(`prepare Draft Proposal args: ${utils.stringify(args)}`);
        let pdCotributerAccount = _.has(args, 'pdCotributerAccount') ? args.pdCotributerAccount : null;
        if (utils._nilEmptyFalse(pdCotributerAccount)) {
            msg = `The Pledger ${pdCotributerAccount} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(pdCotributerAccount)) {
            msg = `The Pledger Address ${pdCotributerAccount} is not a valid Bech32 address!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        return proposalHandler.prepareDraftProposal(args);
    }

    static async deleteDraftProposal(args) {
        // console.log(`deleteDraftProposal args: ${utils.stringify(args)}`);
        return proposalHandler.deleteDraftProposal(args.pdId);
    }

    static async loadMyDraftProposals(args) {
        // console.log(`loadMyDraftProposals args: ${utils.stringify(args)}`);
        return proposalHandler.loadMyDraftProposals(args);
    }

    static getOnchainProposalsList(args) {
        // console.log(`get OnchainProposalsList args: ${utils.stringify(args)}`);
        let machineSettings = machine.getMProfileSettingsSync();
        args.voter = machineSettings.backerAddress;
        return proposalHandler.getOnchainProposalsList(args);
    }


    static calcMyIncome(inx, issuance, otherWorks, myWork) {

        let initWorks = 9 * (100 * 12 * 8 * 7)  // 403,200 level9 * 100hourPerMonth * 12month * 8years * 7Person
        let totalWorks = otherWorks + myWork

        if (inx < iConsts.CONTRIBUTION_APPRECIATING_PERIOD)
            totalWorks += initWorks;

        let payPerHour = issuance / totalWorks
        let myEarnMicro = myWork * payPerHour
        let myEarnPAI = myEarnMicro / 1000000.0
        let changeRate = 700.0 / myEarnPAI
        return {
            totalWorks,
            myEarnPAI,
            changeRate
        }
    }

}

module.exports = ContributeHandler;
