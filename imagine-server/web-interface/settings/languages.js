
//TODO: complete this list
// ref: https://iso639-3.sil.org/code_tables/639/data/all

const languages = [
    { iso: "ara", enName: "Arabic", lName: "العربية" },
    { iso: "eng", enName: "English", lName: "English" },
    { iso: "fas", enName: "Persian", lName: "فارسی" },
    { iso: "fra", enName: "French", lName: "French" },
    { iso: "hye", enName: "Armenian", lName: "Հայերեն" },
    { iso: "ita", enName: "Italian", lName: "Italiano" },
    { iso: "jpn", enName: "Japanese", lName: "日本語 (にほんご)" },
    { iso: "srd", enName: "Sardinian", lName: "sardu" },
    { iso: "snd", enName: "Sindhi", lName: "सिन्धी, سنڌي، سندھی" },
    { iso: "sin", enName: "Sinhalese", lName: "සිංහල" },
    { iso: "spa", enName: "Spanish", lName: "español" },
    { iso: "tur", enName: "Turkish", lName: "Türkçe" },
];

module.exports = languages;