const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
let languages = require('./languages');
const emailHandler = require('../../email_adapter/email');




class MachineSettingsHandler {

    static async loadMachineNeighbors(args) {

        // load machin profiles & settings
        let neighbors = await machine.neighborHandler.getNeighborsAsync({ mpCode: args.selectedMPCode });
        let publicNeighbors = [];
        let privateNeighbors = [];
        for (let aNei of neighbors) {
            if (aNei.nConnectionType == iConsts.CONSTS.PRIVATE) {
                privateNeighbors.push(aNei);
            } else {
                publicNeighbors.push(aNei);
            }
        }
        return { privateNeighbors, publicNeighbors };
    }

    static loadMachineProfile(args) {

        // load machin profiles & settings
        let selectedMP = machine.getMProfileSettingsSync({ mpCode: args.selectedMPCode })

        delete selectedMP.prvEmail.PGPPrvKey;
        delete selectedMP.pubEmail.PGPPrvKey;
        delete selectedMP.backerDetail.uSets;
        delete selectedMP.backerDetail.privContainer;

        // load common settings
        // let commonSettings = machine.comonSetting().getCommonSettingsSync();

        // load ... settings

        // let finalSettings = utils.stringify({
        //     machineStatus: machineSettings.status,
        //     // machinLastConfirmedBlockDate: machineSettings.lastConfirmedBlockDate,
        //     machinEmails: [machineSettings.pubEmail],
        //     backerAddress: machineSettings.backerAddress,
        //     backerDetail: machineSettings.backerDetail.uSets,
        //     prefLang: machineSettings.language,
        // });
        return { selectedMP };



    }

    static getActiveMP() {
        let res = iutils.getSelectedMProfile();
        return { activeMP: res };
    }

    static setActiveMP(args) {
        let res = machine.setActiveMP(args);
        return res;
    }

    static addNewMProfile(args) {
        let res = machine.addNewMProfile(args);
        return res;
    }

    static async saveEmailsSettings(args) {
        let res = await machine.updateMachineEmailSettingsAsync(args);
        return res;
    }

    static async testEmailServerIncomeConnection(args) {
        // console.log(args);
        let popRes = await emailHandler.IMAPFetcher.testIMAPReceiveConnection({
            emailAddress: args.address,
            password: args.pwd,
            host: args.incomingMailServer,
            port: args.incomeIMAP,
            emailAddress: args.address,
        });
        console.log('popRes', popRes);
        return popRes;
    }

    static async testEmailServerOutgoingConnection(args) {
        console.log(args);
        let sendRes = emailHandler.send_mail({
            sender: args.address,
            title: 'test SMPT sending',
            message: 'test SMPT sending',
            receiver: args.receiver,
            pass: args.pwd,
            host: args.outgoingMailServer,
        });
        console.log('sendRes', sendRes);
        return sendRes;
    }

    static async getMProfilesList() {
        let res = await machine.searchMProfilesAsync();
        return res;
    }

    static getLanguagesList() {
        return _.orderBy(languages, 'enName', 'asc');
    }

    static addNeighborS(args) {
        let res = machine.addNeighborM(args);
        return res;
    }

    static cutNeighbor(args) {
        let res = machine.cutNeighbor(args);
        console.log(res);
        return res;
    }

    static async handshakeNeighbor(args) {
        let res = await machine.handshakeNeighbor(args);
        return res;
    }

    static presentEmailToNeighbors(args) {
        let res = machine.neighborHandler.floodEmailToNeighbors(args.nEmail);
        return res;
    }

    static deleteMProfile(args) {
        let res = machine.deleteMProfile(args);
        return res;
    }


}

module.exports = MachineSettingsHandler;