const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const machine = require('../../machine/machine-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');



let admPollingTpl = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dType: iConsts.DOC_TYPES.AdmPolling,
    dClass: iConsts.ADMINISTRATIVE_POLLING_CLASSES.Basic,
    dLen: "0000000",
    dVer: "0.0.0",
    dComment: "",
    pSubject: "",
    pValues: {},
    creationDate: "",
    creator: "",    // the bech32 address of shareholder
    dExtInfo: {},
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000"
};


class PollingDocHandler {

    /**
     * request for release reserved coins
     * @param {*} args 
     */
    static async makeReqForAdmPolling(args) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let pSubject = args.pSubject;
        let pValues = args.pValues;

        let votersCount = args.votersCount;

        let pollingDType = _.has(args, 'dType') ? args.dType : iConsts.DOC_TYPES.Polling;
        let pollingDClass = _.has(args, 'dClass') ? args.dClass : iConsts.POLLING_PROFILE_CLASSES.Basic.ppName;
        let votingLongevity = _.has(pValues, 'longevity') ? pValues.longevity : iConsts.getMinVotingLongevity();
        let creator = _.has(args, 'creator') ? args.creator : machine.getMProfileSettingsSync().backerAddress;
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        let dComment;
        if (_.has(args, 'dComment')) {
            dComment = args.dComment;
        } else {
            let v;
            if (_.has(pValues, 'share')) {
                v = `${pValues.share} %`
            } else if (_.has(pValues, 'pFee')) {
                v = `${pValues.pFee} micro PAI`
            }
            dComment = `Request For Administrative polling to refine(${pSubject}) to (${v})`;
        }

        // 1. create a req-rel-res-doc for block release
        let admPollingDoc = _.clone(admPollingTpl);
        admPollingDoc.creationDate = cDate;
        admPollingDoc.pSubject = pSubject;
        admPollingDoc.pValues = pValues;
        admPollingDoc.dComment = dComment;
        admPollingDoc.creator = creator;

        let { signedHash, signMsg } = this.getSignMsgDAdmPolling(admPollingDoc);
        let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: creator });
        if (signatureInfo.err != false)
            return signatureInfo;
        admPollingDoc.dExtInfo = {
            signatures: signatureInfo.signatures,
            uSet: signatureInfo.uSet
        }
        admPollingDoc.dExtHash = iutils.doHashObject({
            signatures: admPollingDoc.dExtInfo.signatures,
            signedHash,
            uSet: admPollingDoc.dExtInfo.uSet
        });

        admPollingDoc.dLen = iutils.paddingDocLength(utils.stringify(admPollingDoc).length);
        admPollingDoc.hash = this.calcHashDAdmPollingDoc(admPollingDoc);


        // 2. create a administrative-polling for block release
        let pollingDoc = pollHandler.prepareNewPolling({
            dType: pollingDType,
            dClass: pollingDClass,
            dVer: '0.0.9',
            creationDate: admPollingDoc.creationDate,
            creator: admPollingDoc.creator,
            dComment: admPollingDoc.dComment,

            ref: admPollingDoc.hash,
            refType: admPollingDoc.dType,
            refClass: admPollingDoc.dClass,

            longevity: votingLongevity
        });


        // 3. create trx to pay ReqForRelRes doc cost
        let admPollingCost = this.calcAdmPollingDocCost({
            cDate,
            stage: iConsts.STAGES.Creating,
            doc: admPollingDoc
        });
        if (admPollingCost.err != false)
            return admPollingCost;

        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_ADM_POLLING', admPollingCost.cost]
        ];
        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(admPollingCost.cost * 1.3) // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend in admPolling1`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(admPollingCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: admPollingDoc.hash,
            description: 'Pay for administrative Polling (request)',
            inputs,
            outputs,
        }
        let admPollingDocTrxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (admPollingDocTrxDtl.err != false)
            return admPollingDocTrxDtl;
        console.log('signed req rel res cost trx: ', utils.stringify(admPollingDocTrxDtl));


        // 4. create trx to pay polling costs
        let pArgs = {
            cDate,
            stage: iConsts.STAGES.Creating,
            polling: pollingDoc,
            voters: votersCount
        };
        clog.app.info(`calc polling costs for ${utils.stringify(pArgs)}`);
        let pollingCost = pollHandler.calcPollingCost(pArgs);
        if (pollingCost.err != false)
            return pollingCost;

        // create a transaction for payment
        changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_POLLING', pollingCost.cost]
        ];
        spendables = walletHandler.coinPicker.getSomeCoins({
            excludeRefLocs: utils.objKeys(inputs),  // avoid double spending inputs
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(pollingCost.cost * 1.3) // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend in admPolling2`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        inputs = spendables.selectedCoins;
        trxNeededArgs = {
            maxDPCost: utils.floor(pollingCost.cost * .3),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: pollingDoc.hash,
            description: 'Pay for administrative Polling (Polling)',
            inputs,
            outputs,
        }
        let pollingPayTrxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (pollingPayTrxDtl.err != false)
            return pollingPayTrxDtl;
        console.log('signed adm polling cost trx: ', utils.stringify(pollingPayTrxDtl));


        // push in buffer
        let res = await docBufferHandler.pushInAsync({ doc: admPollingDoc, DPCost: admPollingCost.cost });
        if (res.err != false)
            return res;
        res = await docBufferHandler.pushInAsync({ doc: pollingDoc, DPCost: pollingCost.cost });
        if (res.err != false)
            return res;
        res = await docBufferHandler.pushInAsync({ doc: admPollingDocTrxDtl.trx, DPCost: admPollingCost.cost });
        if (res.err != false)
            return res;
        res = await docBufferHandler.pushInAsync({ doc: pollingPayTrxDtl.trx, DPCost: pollingCost.cost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(admPollingDocTrxDtl.trx);
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(pollingPayTrxDtl.trx);

        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your administrative polling request is pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }

    static getSignMsgDAdmPolling(doc) {
        // as always ordering properties by alphabet
        let signables = {
            creationDate: doc.creationDate,
            creator: doc.creator,
            dClass: doc.dClass,
            dComment: doc.dComment,
            dVer: doc.dVer,
            dType: doc.dType,
            pSubject: doc.pSubject,
            pValues: doc.pValues,
        }
        clog.app.info(`RFRf(${doc.pSubject}) signables: ${utils.stringify(signables)}`);
        let signedHash = iutils.doHashObject(signables);
        let signMsg = signedHash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return { signedHash, signMsg };
    }

    static calcHashDAdmPollingDoc(doc) {
        // alphabetical order
        let hashables = {
            dExtHash: doc.dExtHash,
            dLen: doc.dLen
        }
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static calcAdmPollingDocCost(args) {
        let doc = args.doc;
        let stage = args.stage;
        let cDate = args.cDate;

        let dLen = parseInt(doc.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, cDate, dType: doc.dType, dClass: doc.dClass, dLen });

        if (stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: doc.dType,
                dClass: doc.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }


}

module.exports = PollingDocHandler;