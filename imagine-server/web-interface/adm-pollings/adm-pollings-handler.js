const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const GQLHandler = require('../../services/graphql/graphql-handler')
const docTmpHandler = require('../../services/tmp/tmp-handler');
const myContractsHandler = require('../../services/my-contracts/my-contracts-handler');
const crypto = require('../../crypto/crypto');
const DNAHandler = require('../../dna/dna-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');



class AdministrationPollingsHandler {

    static async createAPollingFor(args) {
        clog.app.info(`create A Polling For args ${utils.stringify(args)}`);
        let pSubject = args.pSubject;
        let doc;
        let { holdersByKey } = DNAHandler.getSharesInfo();
        args.votersCount = utils.objKeys(holdersByKey).length;
        args.pValues.longevity = pollHandler.normalizeVotingLongevity(args.pValues.longevity);
        let admRes = await this.docHandler.makeReqForAdmPolling(args);
        return admRes;
    }

    static loadAdmPollings(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        let admPollings = [
            {
                key: this.POLLING_TYPES.RFRfBasePrice,
                label: `Request for Refine charachter base pice of Data & Process costs(DPCost), currently is ${cnfHandler.getBasePricePerChar({ cDate })} micro PAI per Char`,
                pValues: {
                    pFee: cnfHandler.getBasePricePerChar({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfTxBPrice,
                label: `Request for Refine Transaction DPCost, currently is ${Math.trunc(cnfHandler.getBasicTxDPCost({ cDate, dLen: iConsts.TRANSACTION_MINIMUM_LENGTH }))} micro PAI per Char`,
                pValues: {
                    pFee: cnfHandler.getBasicTxDPCost({ cDate, dLen: iConsts.TRANSACTION_MINIMUM_LENGTH }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfBlockFixCost,
                label: `Request for Refine Block Fix Cost, currently is ${cnfHandler.getBlockFixCost({ cDate })} micro PAI per Block`,
                pValues: {
                    pFee: cnfHandler.getBlockFixCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfPollingPrice,
                label: `Request for Refine DPCost of Polling Document, currently is ${cnfHandler.getPollingDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getPollingDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfPLedgePrice,
                label: `Request for Refine DPCost of Pledge Document, currently is ${cnfHandler.getPledgeDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getPledgeDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfClPLedgePrice,
                label: `Request for Refine DPCost of Close a Pledged Account, currently is ${cnfHandler.getClosePledgeDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getClosePledgeDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfDNAPropPrice,
                label: `Request for Refine DPCost of offer a DNAProposal, currently is ${cnfHandler.getCloseDNAProposalDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getCloseDNAProposalDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfBallotPrice,
                label: `Request for Refine DPCost of Ballot, currently is ${cnfHandler.getBallotDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getBallotDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfINameRegPrice,
                label: `Request for Refine DPCost of register an iName, currently is ${cnfHandler.getINameRegDPCost({ cDate })} micro PAIs per unit `,
                pValues: {
                    pFee: cnfHandler.getINameRegDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfINameBndPGPPrice,
                label: `Request for Refine DPCost of Binding an iPGP key to an iName, currently is ${cnfHandler.getINameBindDPCost({ cDate })} micro PAIs per a pair-key `,
                pValues: {
                    pFee: cnfHandler.getINameBindDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfINameMsgPrice,
                label: `Request for Refine DPCost of a message via DAG, currently is ${cnfHandler.getINameMsgDPCost({ cDate })} micro PAIs per char. it refers to entire encrypted message and head & tail & ...`,
                pValues: {
                    pFee: cnfHandler.getINameMsgDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfCPostPrice,
                label: `Request for Refine DPCost of a Custom Post (including text, file, media...), currently is ${cnfHandler.getCPostDPCost({ cDate })} micro PAIs per char `,
                pValues: {
                    pFee: cnfHandler.getCPostDPCost({ cDate }),
                    longevity: iutils.getMinLongevity()
                }
            },



            {
                key: this.POLLING_TYPES.RFRfMinS2Wk,
                label: `Request for Refine Minimum Shares to be Allowed to participate in Wiki Activities, currently is ${utils.numberToString(cnfHandler.getMinShareToAllowedWiki({ cDate })).substr(0, 15)} Percent `,
                pValues: {
                    share: utils.numberToString(cnfHandler.getMinShareToAllowedWiki({ cDate })).substr(0, 15),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfMinS2DA,
                label: `Request for Refine Minimum Shares to be Allowed to participate in Demos Discussions, currently is ${utils.numberToString(cnfHandler.getMinShareToAllowedDemos({ cDate })).substr(0, 15)} Percent `,
                pValues: {
                    share: utils.numberToString(cnfHandler.getMinShareToAllowedDemos({ cDate })).substr(0, 15),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfMinS2V,
                label: `Request for Refine Minimum Shares to be Allowed to participate in ellections, currently is ${utils.numberToString(cnfHandler.getMinShareToAllowedVoting({ cDate })).substr(0, 15)} Percent `,
                pValues: {
                    share: utils.numberToString(cnfHandler.getMinShareToAllowedVoting({ cDate })).substr(0, 15),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfMinFSign,
                label: `Request for Refine Minimum Shares to be Allowed to Sign a Coinbase block, currently is ${utils.numberToString(cnfHandler.getMinShareToAllowedSignCoinbase({ cDate })).substr(0, 15)} Percent `,
                pValues: {
                    share: utils.numberToString(cnfHandler.getMinShareToAllowedSignCoinbase({ cDate })).substr(0, 15),
                    longevity: iutils.getMinLongevity()
                }
            },
            {
                key: this.POLLING_TYPES.RFRfMinFVote,
                label: `Request for Refine Minimum Shares to be Allowed to Issue a Floating Vote (either a block or an entry), currently is ${utils.numberToString(cnfHandler.getMinShareToAllowedIssueFVote({ cDate })).substr(0, 15)} Percent `,
                pValues: {
                    share: utils.numberToString(cnfHandler.getMinShareToAllowedIssueFVote({ cDate })).substr(0, 15),
                    longevity: iutils.getMinLongevity()
                }
            },




            {
                key: this.POLLING_TYPES.RFRlRsCoins,
                label: 'Request for release a Reserved Block',
            },

        ]

        return { err: false, records: admPollings }
    }

    /**
 * voting a proposal
 * 
 * @param {} args 
 * 
 */
    static async voteAdmPolling(args) {
        clog.app.info(`vote Adm Polling args: ${utils.stringify(args)}`);
        let votingRes = await pollHandler.ballotHandler.doVote({
            dTarget: _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER,
            ref: args.pllHash,
            vote: args.vote,
            voteComment: args.voteComment
        });
        return votingRes;
    }


}

AdministrationPollingsHandler.docHandler = require('./preparing-polling-doc');
AdministrationPollingsHandler.docHandler._super = AdministrationPollingsHandler;

AdministrationPollingsHandler.onChain = require('./onchain-handler');
AdministrationPollingsHandler.onChain._super = AdministrationPollingsHandler;

AdministrationPollingsHandler.POLLING_TYPES = {
    RFRlRsCoins: 'RFRlRsCoins',

    RFRfBasePrice: 'RFRfBasePrice',
    RFRfTxBPrice: 'RFRfTxBPrice',
    RFRfBlockFixCost: 'RFRfBlockFixCost',
    RFRfPollingPrice: 'RFRfPollingPrice',
    RFRfPLedgePrice: 'RFRfPLedgePrice',
    RFRfClPLedgePrice: 'RFRfClPLedgePrice',
    RFRfDNAPropPrice: 'RFRfDNAPropPrice',
    RFRfBallotPrice: 'RFRfBallotPrice',
    RFRfINameRegPrice: 'RFRfINameRegPrice',
    RFRfINameBndPGPPrice: 'RFRfINameBndPGPPrice',
    RFRfINameMsgPrice: 'RFRfINameMsgPrice',
    RFRfCPostPrice: 'RFRfCPostPrice',

    RFRfMinS2Wk: 'RFRfMinS2Wk',       // minimum shares to be qualified to participate in wiki activities(create a page or edit exisated pages)
    RFRfMinS2DA: 'RFRfMinS2DA',       // minimum shares to be qualified to participate in Demos/Agoras activities(create a new Agors, replay discussions)
    RFRfMinS2V: 'RFRfMinS2V',       // minimum shares to be qualified to participate in voting
    
    RFRfMinFSign: 'RFRfMinFSign',   // minimum shares to signing coinbase block
    RFRfMinFVote: 'RFRfMinFVote',   // minimum shares to voting about double-spending or collisions
}

module.exports = AdministrationPollingsHandler;