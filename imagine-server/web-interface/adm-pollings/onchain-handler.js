const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const crypto = require('../../crypto/crypto');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const machine = require('../../machine/machine-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const blockUtils = require('../../dag/block-utils');


const table = 'i_administrative_pollings';
const tblAdmRefinesHist = 'i_administrative_refines_history';

class OnchainHandler {

    static initAdministrativeConfigurationsHistory(args) {
        let RFRfPLedgePrice = ['RFRfPLedgePrice', 0];
        let RFRfPollingPrice = ['RFRfPollingPrice', 0];
        if (iConsts.TIME_GAIN != 1) {
            // devel
            RFRfPLedgePrice = ['RFRfPLedgePrice', 37];
            RFRfPollingPrice = ['RFRfPollingPrice', 37];
        }

        let params = [
            // ['RFRlRsCoins', {}],
            ['RFRfTxBPrice', 11],
            RFRfPollingPrice,// 57
            RFRfPLedgePrice,
            ['RFRfClPLedgePrice', 37],
            ['RFRfDNAPropPrice', 37],
            ['RFRfBallotPrice', 57],
            ['RFRfINameRegPrice', 71],
            ['RFRfINameBndPGPPrice', 41],
            ['RFRfINameMsgPrice', 11],
            ['RFRfCPostPrice', 17],
            ['RFRfBasePrice', 683],
            ['RFRfBlockFixCost', (iConsts.TIME_GAIN == 1) ? (100 * 2 * 1000000) : (1 * 1000)],  //minimum cost(100 trx * 2 PAI per trx * 1000000 micropPAI) for atleast 100 simple/light transaction

            ['RFRfMinS2Wk', '0.001'],
            ['RFRfMinS2DA', '0.0001'],
            ['RFRfMinS2V', '0.0000000001'],
            ['RFRfMinFSign', '0.0000000001'],
            ['RFRfMinFVote', '0.000000002'],
        ]
        for (let aSet of params) {
            model.sCreate({
                table: tblAdmRefinesHist,
                values: {
                    arh_hash: crypto.keccak256(`${iConsts.getLaunchDate()}-${aSet[0]}`),
                    arh_subject: aSet[0],
                    arh_value: aSet[1],
                    arh_apply_date: iConsts.getLaunchDate()
                }
            });
        }
    }

    /**
     * func retrieves the last date before given cDate in which the value is refined
     * @param {*} args 
     */
    static getAdmValue(args) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let pollingKey = args.pollingKey;
        let res = model.sRead({
            table: tblAdmRefinesHist,
            query: [
                ['arh_subject', pollingKey],
                ['arh_apply_date', ['<=', cDate]]
            ],
            order: [['arh_apply_date', 'DESC']],
            limit: 1
        });
        if (res.length == 0) {
            utils.exiter(`invalid arh_apply_date for ${pollingKey} on ${cDate}`, 125)
        }
        return res[0].arh_value;
    }

    static recordAnAdmPolling(args) {
        let msg;
        clog.app.info(`record A New Req for release reserved coins args: ${utils.stringify(args)}`);
        let block = args.block;
        let admPolling = args.doc;

        let dbl = model.sRead({
            table,
            query: [['apr_hash', admPolling.hash]]
        });
        if (dbl.length > 0) {
            msg = `try to double insert existed adm polling ${utils.hash6c(admPolling.hash)}`;
            clog.sec.error(msg);
            return { err: false, msg }
        }

        let apr_hash = utils.stripNonHex(admPolling.hash);
        let apr_creator = utils.stripNonAlphaNumeric(admPolling.creator);
        let apr_subject = utils.sanitizingContent(admPolling.pSubject);
        let apr_comment = utils.sanitizingContent(admPolling.dComment);
        let apr_values = blockUtils.wrapSafeObjectForDB({ obj: admPolling.pValues });
        let apr_creation_date = block.creationDate;
        let apr_conclude_date = '';
        let apr_approved = iConsts.CONSTS.NO;
        let apr_conclude_info = blockUtils.wrapSafeObjectForDB({ obj: {} });

        let values = {
            apr_hash,
            apr_creator,
            apr_subject,
            apr_values,
            apr_comment,
            apr_creation_date,
            apr_conclude_date,
            apr_approved,
            apr_conclude_info
        };
        console.log(`new admPolling is creates args: ${utils.stringify(values)}`);
        model.sCreate({
            table,
            values
        })
        clog.app.info(`new admPolling is creates args: ${utils.stringify(values)}`);
        return { err: false };
    }

    static async getOnchainAdmPollings(args = {}) {
        let voter = _.has(args, 'voter') ? args.voter : false;

        if (!voter) {
            let machineSettings = machine.getMProfileSettingsSync();
            voter = machineSettings.backerAddress;
        }

        // retrieve machine votes
        let votes = pollHandler.ballotHandler.searchInLocalBallot();
        let votesDict = {}
        for (let aVote of votes)
            votesDict[aVote.lbtpllHash] = aVote;
        // console.log('votes votes votes ', votes);
        // console.log('votesDict votes Dict ', votesDict);

        let cusQ = `
            SELECT ppr.ppr_name, ppr.ppr_perform_type, ppr.ppr_votes_counting_method, 

            ap.apr_hash, ap.apr_subject, ap.apr_comment, ap.apr_creator, ap.apr_values, 
            ap.apr_creation_date, ap.apr_conclude_info, 

            pll.pll_hash, pll.pll_start_date, pll.pll_longevity, pll.pll_status, pll.pll_ct_done,  
            pll.pll_y_count, pll.pll_n_count, pll.pll_a_count, 
            pll.pll_y_shares, pll.pll_n_shares, pll.pll_a_shares, 
            pll.pll_y_gain, pll.pll_n_gain, pll.pll_a_gain, 
            pll.pll_y_value, pll.pll_n_value, pll.pll_a_value 
            
            
            FROM i_pollings pll 
            JOIN i_polling_profiles ppr ON ppr.ppr_name=pll.pll_class 
            JOIN i_administrative_pollings ap ON ap.apr_hash = pll.pll_ref 
            
            ORDER BY pll.pll_start_date
        `;

        let res = model.sCustom({
            query: cusQ,
            values: []
        });
        for (let aRes of res) {
            aRes.pllEndDateYes = utils.minutesAfter(aRes.pll_longevity * 60, aRes.pll_start_date);
            aRes.creatorColor = `#${utils.hash6c(crypto.keccak256(aRes.apr_creator))}`;
            aRes.pllEndDateAbstainOrNo = utils.minutesAfter(utils.floor(aRes.pll_longevity * 60 * 1.5), aRes.pll_start_date);
            aRes.pllStatus = iConsts.CONSTS.STATUS_TO_LABEL[aRes.pll_status];
            aRes.machineBallot = _.has(votesDict, aRes.pll_hash) ? votesDict[aRes.pll_hash] : null;

            aRes.apr_values = blockUtils.openDBSafeObject(aRes.apr_values).content;
            aRes.aprStrValues = utils.objKeys(aRes.apr_values).map(x => [x, aRes.apr_values[x]].join(': ')).join(', ');
            aRes.apr_conclude_info = blockUtils.openDBSafeObject(aRes.apr_conclude_info).content;

            // calc potentiasl voter gains
            if (voter) {
                let diff = utils.timeDiff(aRes.pll_start_date).asMinutes;
                let vGain = pollHandler.calculateVoteGain(diff, diff, aRes.pll_longevity * 60);
                // console.log('diff', diff);
                // console.log('pll_longevity', aRes.pll_longevity * 60);
                // console.log('vGain', vGain);
                aRes.yourGainY = utils.customFloorFloat(vGain.gainYes * 100, 2);
                aRes.yourGainN = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);
                aRes.yourGainA = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);

            } else {
                aRes.yourGainY = 0.0;
                aRes.yourGainN = 0.0;
                aRes.yourGainA = 0.0;
            }
        }

        return { records: res, err: false };

    }

    static treatPollingWon(args) {
        let msg;
        clog.app.info(`treat Polling Won args: ${utils.stringify(args)}`);
        let polling = args.polling;
        let approveDate = args.approveDate;
        let admPollingHash = polling.pllRef;
        let admPolling = model.sRead({
            table,
            query: [
                ['apr_hash', admPollingHash]
            ]
        });
        if (admPolling.length != 1)
            return { err: true, msg: `invalid admPolling(${admPollingHash})` }
        admPolling = admPolling[0];
        admPolling.apr_values = blockUtils.openDBSafeObject(admPolling.apr_values).content;
        clog.app.info(`treat Polling Won admPolling: ${utils.stringify(admPolling)}`);
        let arh_apply_date = iutils.getACycleRange({
            forwardByCycle: 2,
            cDate: approveDate
        }).minCreationDate;

        switch (admPolling.apr_subject) {

            case this._super.POLLING_TYPES.RFRfMinS2V:
            case this._super.POLLING_TYPES.RFRfMinFSign:
            case this._super.POLLING_TYPES.RFRfMinFVote:
                this.logRefineDetail({
                    arh_hash: admPollingHash,
                    arh_subject: admPolling.apr_subject,
                    arh_value: admPolling.apr_values.share,
                    arh_apply_date,
                });
                break;

            case this._super.POLLING_TYPES.RFRfBasePrice:
            case this._super.POLLING_TYPES.RFRfTxBPrice:
            case this._super.POLLING_TYPES.RFRfBlockFixCost:
            case this._super.POLLING_TYPES.RFRfPollingPrice:
            case this._super.POLLING_TYPES.RFRfPLedgePrice:
            case this._super.POLLING_TYPES.RFRfClPLedgePrice:
            case this._super.POLLING_TYPES.RFRfDNAPropPrice:
            case this._super.POLLING_TYPES.RFRfBallotPrice:
            case this._super.POLLING_TYPES.RFRfINameRegPrice:
            case this._super.POLLING_TYPES.RFRfINameBndPGPPrice:
            case this._super.POLLING_TYPES.RFRfINameMsgPrice:
            case this._super.POLLING_TYPES.RFRfCPostPrice:
                this.logRefineDetail({
                    arh_hash: admPollingHash,
                    arh_subject: admPolling.apr_subject,
                    arh_value: admPolling.apr_values.pFee,
                    arh_apply_date,
                });
                break;

            default:
                msg = `Unknown apr_subject in "treat Polling Won" ${admPolling.apr_subject}`;
                clog.sec.error(msg);
                utils.exiter(msg, 987);

        }

        // update proposal 
        model.sUpdate({
            table,
            query: [
                ['apr_hash', admPollingHash]
            ],
            updates: {
                apr_conclude_date: approveDate,
                apr_approved: iConsts.CONSTS.YES
            }
        });
    }

    static logRefineDetail(args) {
        /**
         * FIXME: since "arh_apply_date" is calculted based on container block.creationDate
         * it is possible for a same pSubject having 2 or more different polling which the arh_apply_date will be sam(e.g they reside in same block or 2 different block by same creationdate which is heigly possible an adversor create them)
         * and at the end we have different result with same date to apply!
         * it must be fixed ASAP, meanwhile the comunity can simply futile the duplicated polling by negative votes
         */
        let exist = model.sRead({
            table: tblAdmRefinesHist,
            query: [['arh_hash', args.arh_hash]]
        });
        if (exist.length > 0) {
            return { err: true, msg: `duplicate refine hist (${args.arh_hash})` }
        }
        model.sCreate({
            table: tblAdmRefinesHist,
            values: {
                arh_hash: args.arh_hash,
                arh_subject: args.arh_subject,
                arh_value: args.arh_value,
                arh_apply_date: args.arh_apply_date
            }
        })
    }

    static concludeAdmPolling(args) {
        let polling = args.polling;
        let admPollingHash = polling.pllRef;

        // update proposal 
        model.sUpdate({
            table,
            query: [
                ['apr_hash', admPollingHash]
            ],
            updates: {
                apr_conclude_date: args.approveDate,
                apr_approved: iConsts.CONSTS.NO
            }
        });
        return { err: false }
    }

}

module.exports = OnchainHandler;
