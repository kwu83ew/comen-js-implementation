const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const GQLHandler = require('../../services/graphql/graphql-handler')
const docTmpHandler = require('../../services/tmp/tmp-handler');
const myContractsHandler = require('../../services/my-contracts/my-contracts-handler');
const crypto = require('../../crypto/crypto');
const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');
const pPledgeHandler = require('../../contracts/pledge-contract/proposal-pledge-contract');

class contractsHandler {

    static convertCPostToPLRBundle(docHash) {
        let msg;
        // retrieve proper document (which is a CPost)

        let posts = freeDocHandler.demos.posts.searchInPosts({
            query: [['ap_doc_hash', docHash]]
        });
        let post = posts[0];
        let ap_attrs = utils.parse(post.ap_attrs);
        let ap_opinion = post.ap_opinion;

        if (utils._nilEmptyFalse(ap_attrs)) {
            // it is a normal post, so render
            clog.sec.error(` wrong cPost type as proposal1`);
            return { err: true }
        }

        // mabe need special treatment befor insert to db
        if (!_.has(ap_attrs, 'isPreProposal') || (ap_attrs.isPreProposal != iConsts.CONSTS.YES)) {
            clog.sec.error(`wrong cPost type as proposal2`);
            return { err: true }
        }

        // convert opinion to base64 and save it in db
        let buffer = Buffer.from(ap_opinion, "base64");
        ap_opinion = buffer.toString("utf8");
        ap_opinion = utils.parse(ap_opinion);
        let proposal = ap_opinion.proposal;
        let pledge = ap_opinion.pledge;
        // let addrDtl = walletAddressHandler.getAddressesInfoSync([ap_opinion.pledge.pledger]);
        let dplValidateRes = pPledgeHandler.validatePledgerSignedRequest({
            proposal,
            pledge,
            stage: iConsts.STAGES.Creating
        });
        if (dplValidateRes.err != false) {
            clog.sec.error(`wrong cPost type as proposal3 ${utils.stringify(dplValidateRes)}`);
            return dplValidateRes;
        }

        // create a bundle & insert to DB
        let bundle = {
            sender: 'Agora',
            connection_type: "Public",
            receive_date: utils.getNow(),
            creationDate: utils.getNow(),
            dType: 'receivedPLR',
            dClass: 'receivedPLR',
            creationDate: utils.getNow(),
            payload: {
                cdType: "ProposalLoanRequest",
                cdVer: "0.0.4", //fixme: pick the right version number from right place

                proposal,
                pledgerSignedPledge: pledge
            }
        }
        bundle.hash = iutils.doHashObject(bundle);
        clog.app.info(`handle ReceivedProposalLoanRequest via cPost ${utils.stringify(bundle)}`);
        docTmpHandler.insertTmpDocSync({
            doc: bundle,
            doc_status: 'new'
        });
        return { err: false, msg: 'bundle created' }
    }

    static async getPLRBundles(args) {
        let ppts = await docTmpHandler.searchTmpDocsAsync({
            query: [['td_doc_type', 'receivedPLR']],
            order: [
                ['td_insert_date', 'ASC']
            ]
        });
        // console.log(ppts);
        if (ppts.length == 0)
            return [];

        for (let appt of ppts) {
            appt.body = utils.parse(appt.tdPayload);
            appt.body.payload.pledgerSignedPledge.arbiterColor = `#${utils.hash6c(crypto.keccak256(appt.body.payload.pledgerSignedPledge.arbiter))}`;
        }
        return ppts;
    }

    static async getPPTBundles(args) {
        let ppts = await docTmpHandler.searchTmpDocsAsync({
            query: [['td_doc_type', 'BundlePPT']],
            order: [
                ['td_insert_date', 'ASC']
            ]
        });
        // console.log(ppts);
        if (ppts.length == 0)
            return [];

        for (let appt of ppts) {
            appt.body = utils.parse(appt.tdPayload);
        }
        return ppts;
    }


    static async deleteBundle(args) {
        console.log('deleteBundle args ', args);
        return await docTmpHandler.removeFromTmpAsync({
            query: [['td_id', args.tdId]]
        });
    }

    static async deletePLR(args) {
        console.log('deletePLR args ', args);
        return await docTmpHandler.removeFromTmpAsync({
            query: [['td_id', args.tdId]]
        });
    }

    static signPLR(args) {
        console.log('signPLR args ', args);
        return docTmpHandler.unBundleAndSignPLR(args.tdId);
    }

    static doAction(args) {
        return myContractsHandler.doAction(args);
    }

    static getMyOnChains(args) {
        return myContractsHandler.searchInMyOnchainContracts();
    }

    static pushPPTToSendingQ(args) {
        console.log('pushPPTToSendingQ args ', args);
        return docTmpHandler.unBundleAndBroadcastPPT(args.tdId);
    }

}

module.exports = contractsHandler;
