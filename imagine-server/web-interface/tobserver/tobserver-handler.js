const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const model = require('../../models/interface');
const rejectedTrxHandler = require('../../dag/normal-block/rejected-transactions-handler');
const suspectTrxHandler = require('../../transaction/utxo/suspect-transactions-handler');
const superControlUntilCoinbaseMinting = require('../../dag/validate-inputs-all-the-way-back-to-cb');
const blockUtils = require('../../dag/block-utils');
const crypto = require('../../crypto/crypto')


class TransactionsObserver {

    static async getRejectedTrxsList(args) {
        let records = await rejectedTrxHandler.searchRejTrxAsync({
            order: [
                ['rt_insert_date', 'ASC'],
                ['rt_coin', 'ASC']
            ]
        });
        return { err: false, records }
    }

    static async getSusTrxsList(args) {
        let selectedOrderSet = args.selectedOrderSet;
        let order;
        switch (selectedOrderSet) {

            case 'orderSet1':
                order = [
                    ['st_vote_date', 'ASC'],
                    ['st_coin', 'ASC'],
                    ['st_receive_order', 'ASC'],
                ]
                break;

            case 'orderSet2':
                order = [
                    ['st_coin', 'ASC'],
                    ['st_receive_order', 'ASC'],
                    ['st_spender_block', 'ASC'],
                    ['st_spender_doc', 'ASC'],
                    ['st_vote_date', 'ASC'],
                ]
                break;


            case 'orderSet3':
                order = [
                    ['st_voter', 'ASC'],
                    ['st_coin', 'ASC'],
                    ['st_vote_date', 'ASC'],
                ]
                break;

            case 'orderSet4':
                order = [
                    ['st_logger_block', 'ASC'],
                    ['st_coin', 'ASC'],
                    ['st_voter', 'ASC'],
                ]
                break;


            default:
                order = [
                    ['st_vote_date', 'ASC'],
                    ['st_coin', 'ASC'],
                    ['st_receive_order', 'ASC'],
                ]
                break;

        }
        // TODO: implement a different priority for order option, e.g. order by coins, voteDate, receiveDate...
        let records = await suspectTrxHandler.retrieveSuspectTransactionsAsync({
            order
        });
        for (let aRec of records) {
            aRec.loggerColor = `#${utils.hash6c(crypto.keccak256(aRec.st_logger_block))}`;
            aRec.voterColor = `#${utils.hash6c(crypto.keccak256(aRec.st_voter))}`;
            aRec.coinColor = `#${utils.hash6c(crypto.keccak256(aRec.st_coin))}`;
            aRec.spenderColor = `#${utils.hash6c(crypto.keccak256(`${aRec.st_spender_block}:${aRec.st_spender_doc}`))}`;
        }
        return { err: false, records }
    }

    static async traceCoinInfo(args) {
        console.log(`trace CoinInfo args ${utils.stringify(args)}`);
        let selectedCoin = args.selectedCoin;
        if (utils._nilEmptyFalse(selectedCoin))
            return { err: true, msg: `Invalid selectedCoin in traceCoinInfo ${selectedCoin}` };

        let { docHash, outputIndex } = iutils.unpackCoinRef(selectedCoin);
        let hashesType = (docHash.length == 64) ? iConsts.CONSTS.COMPLETE : iConsts.CONSTS.SHORT;
        let { wBlocks, mapDocToBlock } = dagHandler.getWBlocksByDocHash({
            docsHashes: [docHash],
            hashesType
        });
        if (wBlocks.length == 0)
            return { err: false, msg: `given coin(${selectedCoin}) has no document!` };
        let block = blockUtils.openDBSafeObject(wBlocks[0].bBody).content;
        console.log('block', block);
        console.log('docHash', docHash);
        if (hashesType == iConsts.CONSTS.SHORT) {
            let longHashes = utils.objKeys(mapDocToBlock)
            console.log(`longHashes`, longHashes);
            longHashes.forEach(element => {
                if (element.startsWith(docHash))
                    docHash = element;
            });
        }
        console.log('docHash', docHash);
        let SCUUCM = superControlUntilCoinbaseMinting.trackingBackTheCoins({
            spendBlock: block,
            interestedDocs: [docHash]
        });
        if (SCUUCM.err != false)
            return SCUUCM;
        let coinTrack = SCUUCM.coinTrack.reverse().slice(1);
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        console.log(`SCUUCM.coinTrack`, SCUUCM.coinTrack);
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
        // console.log(`coinTrack`, coinTrack);


        // find double spendings (if exist)
        let order = [
            ['st_spender_block', 'ASC'],
            ['st_vote_date', 'ASC'],
            ['st_receive_order', 'ASC'],
        ]
        let susRecords = await suspectTrxHandler.retrieveSuspectTransactionsAsync({
            fields: [
                'st_voter',
                'st_vote_date',
                'st_coin',
                'st_spender_block',
                'st_spender_doc',
                'st_receive_order',
                'st_spend_date',
                `DATE_PART('Minutes', st_vote_date::time) - DATE_PART('Minutes', st_spend_date::time) AS _diff`
            ],
            query: [
                ['st_coin', iutils.packCoinRef(docHash, outputIndex)]
            ],
            order
        });
        let susGroupedByblockHashDocHash = {};
        let spenders = [];
        if (susRecords.length > 0) {
            for (let aSus of susRecords) {
                let BDKey = `${aSus.st_spender_block}:${aSus.st_spender_doc}`;
                spenders.push([aSus.st_spender_block, aSus.st_spender_doc, aSus.st_spend_date]);
                if (!_.has(susGroupedByblockHashDocHash, BDKey))
                    susGroupedByblockHashDocHash[BDKey] = {
                        votes: [],
                        summary: {}
                    };
                susGroupedByblockHashDocHash[BDKey].votes.push(aSus)


            }
        }

        // console.log('spenders', spenders);

        // prepare validity reports
        let spendersDict = {}
        for (let anSpender of spenders) {
            let spendReport;
            let susVotesForDoc = suspectTrxHandler.getSusInfoByDocHash({ invokedDocHash: anSpender[1] });
            console.log(`vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv`);
            console.log(`vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv`, anSpender[1]);
            console.log(`vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv`, susVotesForDoc);
            if (_.has(susVotesForDoc, 'allCoinsAreValid')) {
                let UIMCD2C = {}    // TODO it could be more complete (like analize a transaction coins input args)
                msg = `doc(${utils.hash6c(anSpender[1])}) of block(${utils.hash6c(block.blockHash)}) `
                msg += `is not sus (even if block is blockIsSusCase) allCoinsOfDocAreValid`
                clog.trx.info(msg);
                UIMCD2C.canImportNormally = true;
                UIMCD2C.transactionsDetection[anSpender[1]] = 'There is no sus vote for doc';
                AnalyzeTrxCoins.extractDocImportableUTXOs({
                    block,
                    doc
                });
                spendReport = UIMCD2C;

            } else {
                let { rawVotes } = suspectTrxHandler.retrieveVoterPercentages({ rawVotes: susVotesForDoc.rawVotes })
                spendReport = suspectTrxHandler.checkDocValidity({ doLog: false, invokedDocHash: anSpender[1], rawVotes });

            }

            let BDKey = `${anSpender[0]}:${anSpender[1]}`;
            spendersDict[BDKey] = {
                block: anSpender[0],
                doc: anSpender[1],
                blockCreationDate: anSpender[2],
                spenderColor: `#${utils.hash6c(crypto.keccak256(BDKey))}`,
                spendReport,
            };
        }
        console.log('\n\n>>>>>>>>spendReport', utils.stringify(spendersDict));

        // retrieve coin validity risk
        let coinSpendBlock = SCUUCM.coinTrack.reverse()[0].blockHash;
        let validityPercentage = dagHandler.walkThrough.getAllDescendents({
            blockHash: coinSpendBlock,
            retrieveValidityPercentage: true
        }).validityPercentage;


        return { err: false, coinTrack, susRecords, spendersDict, validityPercentage }

    }

}

module.exports = TransactionsObserver;
