const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const crypto = require('../../crypto/crypto')


// TODO: implement real graphQL either for nodes comunication or client/server comunication or wallet/clients comunication

class GQLHandler {

    static makeAPacket(args) {
        args.pType = _.has(args, 'pType') ? args.pType : iConsts.CONSTS.GQL;
        args.pVer = _.has(args, 'pVer') ? args.pVer : '0.0.3';
        args.creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let body = utils.stringify(args);
        let code = crypto.keccak256(body);
        return { code, body };
    }

}

GQLHandler.cardTypes = {
    ProposalLoanRequest: 'ProposalLoanRequest',
    // FUNC_ PROPOSAL_LOAN_REQUEST: "ProposalLoanRequest",
    FullDAGDownloadRequest: 'FullDAGDownloadRequest',
    FullDAGDownloadResponse: 'FullDAGDownloadResponse',
    BallotsReceiveDates: 'BallotsReceiveDates',
    NodeStatusScreenshot: 'NodeStatusScreenshot',

    pleaseRemoveMeFromYourNeighbors: 'pleaseRemoveMeFromYourNeighbors',
    directMsgToNeighbor: 'directMsgToNeighbor',
}

module.exports = GQLHandler;
