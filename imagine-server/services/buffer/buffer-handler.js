const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const model = require("../../models/interface");
const clog = require('../../loggers/console_logger');
const getMoment = require("../../startup/singleton").instance.getMoment
const listener = require('../../plugin-handler/plugin-handler');

const table = 'i_machine_buffer_documents';

class DocBufferHandler {

    static async getBufferContentsInfo(args={}) {
        let bufRecords = await this.searchBufferedDocsAsync({
            fields:['bd_id', 'bd_insert_date', 'bd_doc_hash', 'bd_doc_type', 'bd_doc_class', 'bd_dp_cost', 'bd_doc_len'],
            order: [
                ['bd_insert_date', 'ASC'],
                ['bd_dp_cost', 'DESC'],
            ]
        });
        // console.log('bufRecords', bufRecords);
        return bufRecords;
    }

    static async pushInAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        listener.doCallAsync('APSH_before_push_doc_to_buffer_async', args);

        let dblChk = await this.searchBufferedDocsAsync({
            query: [
                ['bd_mp_code', mpCode],
                ['bd_doc_hash', args.doc.hash]
            ]
        });
        if (dblChk.length > 0) {
            let msg = `tried to insert in buffer duplicated document ${args.doc.hash} `;
            clog.sec.error(msg);
            return { err: true, msg };
        }

        let payload = JSON.stringify(args.doc);
        let res = await model.aCreate({
            table,
            values: {
                bd_mp_code: mpCode,
                bd_insert_date: utils.getNow(),
                bd_doc_hash: args.doc.hash,
                bd_doc_type: args.doc.dType, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_doc_class: args.doc.dClass, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_payload: payload,
                bd_dp_cost: JSON.stringify(args.DPCost),
                bd_doc_len: payload.length,
            }
        });
        listener.doCallAsync('APSH_after_push_doc_to_buffer_async', args);
        return { err: false };
    }

    static pushInSync(args) {
        let msg;
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        listener.doCallSync('SPSH_before_push_doc_to_buffer_sync', args);

        let dblChk = this.searchBufferedDocsSync({
            query: [
                ['bd_mp_code', mpCode],
                ['bd_doc_hash', args.doc.hash]
            ]
        });
        if (dblChk.length > 0) {
            msg = `tried to insert in buffer duplicated document ${args.doc.hash} `;
            clog.sec.error(msg);
            return { err: true, msg };
        }

        let payload = JSON.stringify(args.doc);
        let res = model.sCreate({
            table,
            values: {
                bd_mp_code: mpCode,
                bd_insert_date: utils.getNow(),
                bd_doc_hash: args.doc.hash,
                bd_doc_type: args.doc.dType, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_doc_class: args.doc.dClass, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_payload: payload,
                bd_dp_cost: JSON.stringify(args.DPCost),
                bd_doc_len: payload.length,
            }
        });
        listener.doCallSync('SPSH_after_push_doc_to_buffer_sync', args);
        return { err: false };
    }

    static insertBufDocSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        listener.doCallSync('APSH_before_push_doc_to_buffer_sync', args);

        let dblChk = this.searchBufferedDocsSync({
            query: [
                ['bd_mp_code', mpCode],
                ['bd_doc_hash', args.doc.hash]
            ]
        });
        if (dblChk.length > 0) {
            clog.sec.error(`tried to insert in buffer duplicated trx ${args.doc.hash} `);
            return;
        }

        let payload = JSON.stringify(args.doc);
        let res = model.sCreate({
            table,
            values: {
                bd_mp_code: mpCode,
                bd_insert_date: utils.getNow(),
                bd_doc_hash: args.doc.hash,
                bd_doc_type: args.doc.dType, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_doc_class: args.doc.dClass, // iutils.mapDocCodeToDocType(args.doc.dType),
                bd_payload: payload,
                bd_dp_cost: JSON.stringify(args.DPCost),
                bd_doc_len: payload.length,
            }
        });
        listener.doCallAsync('APSH_after_push_doc_to_buffer_sync', args);
        return res;
    }

    static async removeFromBufferAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'bd_mp_code'))
            query.push(['bd_mp_code', mpCode]);

        let hashes = _.has(args, 'hashes') ? args.hashes : null;

        if (!utils._nilEmptyFalse(hashes))
            query.push(['bd_doc_hash', ['IN', hashes]]);

        return await model.aDelete({
            table,
            query
        })
    }

    static async removeFromBufferSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'bd_mp_code'))
            query.push(['bd_mp_code', mpCode]);

        let hashes = _.has(args, 'hashes') ? args.hashes : null;

        if (!utils._nilEmptyFalse(hashes))
            query.push(['bd_doc_hash', ['IN', hashes]]);

        return model.sDelete({
            table,
            query
        })
    }

    static async searchBufferedDocsAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'bd_mp_code'))
            query.push(['bd_mp_code', mpCode]);
        args.query = query;
        args.table = table;
        let res = await model.aRead(args);
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static searchBufferedDocsSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'bd_mp_code'))
            query.push(['bd_mp_code', mpCode]);
        args.query = query;
        args.table = table;
        let res = model.sRead(args);
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static async getProcessedTrxs(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let wTrxs = await DocBufferHandler.searchBufferedDocsAsync({
            query: [
                ['bd_mp_code', mpCode],
                ['bd_doc_type', ['IN', [iConsts.DOC_TYPES.BasicTx]]]
            ],
            order: [
                ['bd_dp_cost', 'DESC'],
                ['bd_insert_date', 'ASC']
            ]
        });
        let out = [];
        wTrxs.forEach(wTrx => {
            let trx = JSON.parse(wTrx.bdPayload);

            out.push({
                insert_date: wTrx.bdInsertDate,
                dp_cost: wTrx.bdDPCost,
                length: wTrx.bdDocLen, // to calculate minimum backer fee

                dVer: trx.dVer,
                dType: trx.dType,
                hash: utils.hash6c(trx.hash),
                inputCounts: trx.inputs.length,
                outputs: trx.outputs,
            });
        });

        return out
    }


    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'bd_id'))
            out.bdId = elm.bd_id;
        if (_.has(elm, 'bd_insert_date'))
            out.bdInsertDate = elm.bd_insert_date;
        if (_.has(elm, 'bd_doc_hash'))
            out.bdDocHash = elm.bd_doc_hash;
        if (_.has(elm, 'bd_doc_type'))
            out.bdDocType = elm.bd_doc_type;
        if (_.has(elm, 'bd_doc_class'))
            out.bdDocClass = elm.bd_doc_class;
        if (_.has(elm, 'bd_payload'))
            out.bdPayload = elm.bd_payload;
        if (_.has(elm, 'bd_dp_cost'))
            out.bdDPCost = iutils.convertBigIntToJSInt(elm.bd_dp_cost);
        if (_.has(elm, 'bd_doc_len'))
            out.bdDocLen = elm.bd_doc_len;
        return out;
    }

}

module.exports = DocBufferHandler;
