const _ = require('lodash');
const iConsts = require('../../config/constants');
const db = require('../../startup/db2')
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const listener = require('../../plugin-handler/plugin-handler');
const blockUtils = require('../../dag/block-utils');
const machine = require('../../machine/machine-handler');

const table = 'i_parsing_q';


class QueueUtils {

    static ancestorsConroll(args) {
        let msg;
        let block = args.block;
        let pq_type = args.pq_type;

        if (_.has(block, 'ancestors')) {
            let expectedBlocksHash = block.ancestors;
            let isAncValid = blockUtils.ifAncestorsAreValid(expectedBlocksHash);
            if (!isAncValid) {
                msg = `invalid ancestosr ${JSON.stringify(args)}`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true };
            }
            let existedWBlocks = dagHandler.searchInDAGSync({
                fields: ['b_hash', 'b_creation_date', 'b_type', 'b_utxo_imported'],
                query: [
                    ['b_hash', ['IN', expectedBlocksHash]]
                ]
            });
            let existedWBlocksHash = existedWBlocks.map(elm => elm.bHash);
            clog.app.info(`existedWBlocksHash ${JSON.stringify(existedWBlocksHash)}`);
            let missedBlocks = utils.arrayDiff(expectedBlocksHash, existedWBlocksHash);
            if (missedBlocks.length > 0) {
                clog.app.info(`in order to parsing block(${utils.hash6c(block.blockHash)}) machine needs these missed blocks(${missedBlocks.map(x => utils.hash6c(x))})`);

                QueueUtils.appendPrerequisites(block.blockHash, missedBlocks, pq_type);

                // check if the block already is in parsing queue? if not add it to missed blocks to invoke
                let missedHashInParsingQ = [];
                for (let hash of missedBlocks) {
                    let exists = QueueUtils.searchParsingQSync({
                        query: [
                            // ['pq_type', pq_type],
                            ['pq_code', hash],
                        ]
                    })
                    if (exists.length == 0)
                        missedHashInParsingQ.push(hash)
                };
                if (missedHashInParsingQ.length > 0) {
                    const missedBlocksHandler = require('../../dag/missed-blocks-handler');
                    clog.app.info(`Really missed Blocks, so push to invoking: ${JSON.stringify(missedHashInParsingQ)}`);
                    missedBlocksHandler.addMissedBlocksToInvoke(missedHashInParsingQ)
                }

                msg = `--- Break parsing block because of missed prerequisites block(${utils.hash6c(block.blockHash)}) > ${missedBlocks}`
                clog.trx.info(msg);
                return {
                    err: true,
                    msg,
                    shouldPurgeMessage: false
                }
            }

            let allAncestorsAreImported = true;
            let notImportedAncs = [];
            let oldestAncestorCreationDate = utils.getNow();
            for (let bk of existedWBlocks) {
                // controll ancestors creation date
                if (bk.creation_date > block.creationDate) {
                    msg = `Block(${utils.hash6c(block.blockHash)}) ${pq_type} creationDdate(${block.creationDate}) is before it's ancestors(${utils.hash6c(bk.bHash)}) creationDate(${bk.creation_date})`
                    clog.trx.error(msg);
                    res.msg = msg
                    res.err = true
                    res.shouldPurgeMessage = true
                    return res
                }

                // control import new coins
                if (([
                    iConsts.BLOCK_TYPES.Normal,
                    iConsts.BLOCK_TYPES.Coinbase,
                    iConsts.BLOCK_TYPES.RpBlock,
                    iConsts.BLOCK_TYPES.RlBlock
                ].includes(bk.bType)) && (bk.bUtxoImported != iConsts.CONSTS.YES)) {
                    allAncestorsAreImported = false;
                    notImportedAncs.push(bk.bHash);
                    if (oldestAncestorCreationDate > bk.bCreationDate)
                        oldestAncestorCreationDate = bk.bCreationDate;
                }
            }

            // if is in sync mode, control if ancestors's coins(if exist) are imported
            if (machine.isInSyncProcess() &&
                [iConsts.BLOCK_TYPES.Normal].includes(block.bType) &&   // in order to let adding FVote blocks to DAG, before importing uplinked Normal block
                !allAncestorsAreImported
                // && (block.creationDate < iutils.getACycleRange().minCreationDate)
            ) {
                if (utils.timeDiff(block.creationDate).asMinutes < iConsts.getCycleByMinutes() / 6) {
                    // if block is enoough new maybe machine is not in sync mode more
                    machine.isInSyncProcess({ forsToControlBasedOnDAGStatus: true });
                }
                // run this controll if the block creation date is not in current sycle
                // infact by passing lastSyncStatus when machine reached to almost leaves in real time  
                msg = `--- Break parsing block, because of not imported coins of ancestors block(${utils.hash6c(block.blockHash)}) > Ancestors(${notImportedAncs.map(x => utils.hash6c(x))})`
                clog.trx.info(msg);

                // manually calling import threads to import ancestors coins (if they are eligible)
                const transferUtxo = require('../../dag/normal-block/import-utxo-from-normal-blocks/import-utxo-from-normal-blocks');
                transferUtxo.doImportUTXOs({
                    cDate: block.creationDate
                });
                const coinbaseUtxo = require('../../dag/coinbase/import-utxo-from-coinbases');
                coinbaseUtxo.importCoinbasedUTXOs({
                    cDate: oldestAncestorCreationDate
                });

                return {
                    err: true,
                    msg,
                    shouldPurgeMessage: false
                }
            } else {
                return { err: false }
            }


        } else {
            if ([
                iConsts.BLOCK_TYPES.Normal,
                iConsts.BLOCK_TYPES.Coinbase,
                iConsts.BLOCK_TYPES.RpBlock,
                iConsts.BLOCK_TYPES.RlBlock,
                iConsts.BLOCK_TYPES.POW,
                iConsts.BLOCK_TYPES.FSign
            ].includes(pq_type)) {
                msg = `The ${pq_type} Block(${utils.hash6c(block.blockHash)}) MUST have ancestor!`;
                clog.trx.error(msg);
                clog.trx.error(block);
                return {
                    err: true,
                    msg,
                    shouldPurgeMessage: true
                }
            }
        }

        return { err: false }
    }



    // appends given Prerequisites for given block
    static appendPrerequisites(blockHash, prerequisites, pq_type = null) {
        if (prerequisites.length == 0)
            return;

        let msg;
        let query = [
            ['pq_code', blockHash]
        ]
        if (!utils._nilEmptyFalse(pq_type))
            query.push(['pq_type', pq_type])

        let res = QueueUtils.searchParsingQSync({
            fields: ["pq_type", "pq_code", "pq_prerequisites"],
            query: query
        });
        if (res.length == 0) {
            msg = `Wrong requeste to append requisities to a block(${pq_type} ${utils.hash6c(blockHash)}) which does not exiss in parsing q!`;
            clog.app.error(msg);
            return;
        }

        res = res[0];
        let currentPrereq = utils.unpackCommaSeperated(res.pqPrerequisites);
        clog.app.info(`block(${utils.hash6c(blockHash)}) adding new prerequisities(${prerequisites.map(x => utils.hash6c(x))}) to existed prerequisities(${currentPrereq.map(x => utils.hash6c(x))})`);
        currentPrereq = _.sortBy(currentPrereq.concat(prerequisites));
        clog.app.info(`block(${utils.hash6c(blockHash)}) final1 prerequisities(${currentPrereq.map(x => utils.hash6c(x))})`);
        currentPrereq = utils.packCommaSeperated(currentPrereq);
        clog.app.info(`block(${utils.hash6c(blockHash)}) final2 prerequisities(${currentPrereq})`);
        return model.sUpdate({
            table,
            query: [
                ['pq_code', blockHash]
            ],
            updates: {
                pq_prerequisites: currentPrereq,
                pq_last_modified: utils.getNow()
            }
        });

    }

    static findByPrerequisites(prerequisites) {
        return QueueUtils.searchParsingQSync({
            query: [
                ['pq_prerequisites', ['LIKE', `%${prerequisites}%`]],
            ]
        })
    }

    static searchParsingQSync(args = {}) {
        let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args)
        let records = model.sCustom({
            query: `SELECT ${_fields} FROM ${table} ${_clauses} ${_order} ${_limit}`,
            values: _values
        });
        if (records.length == 0)
            return [];

        let convertedRecords = [];
        for (let aRec of records) {
            aRec = QueueUtils.convertFields(aRec);
            if (_.has(aRec, 'pqPayload'))
                aRec.pqPayload = blockUtils.openDBSafeObject(aRec.pqPayload).content;
            convertedRecords.push(aRec);
        }
        return convertedRecords;
    }

    /**
     * 
     * @param {*} blockHash 
     * NOTE: the queue's prerequisities can be removen ONLY where the referenced block recorded in DAG.
     * in any other cases we must not remove block's prerequisities even the mentioned block already exist in queue
     */
    static removePrerequisitesSync(blockHash) {
        let records, prerequisites;
        records = model.sCustom({
            query: `SELECT pq_type, pq_code, pq_prerequisites FROM ${table} WHERE pq_prerequisites LIKE $1`,
            values: [`%${blockHash}%`],
            log: false
        });

        if (records.length == 0) {
            return;
        }

        records.forEach(aBlock => {
            prerequisites = aBlock.pq_prerequisites.replace(blockHash, '');
            prerequisites = utils.normalizeCommaSeperatedStr(prerequisites)
            model.sUpdate({
                table,
                query: [
                    ['pq_type', aBlock.pq_type],
                    ['pq_code', aBlock.pq_code]
                ],
                updates: {
                    pq_prerequisites: prerequisites
                },
                log: false
            });
        });
    }

    static async searchQAsync(args = {}) {
        let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args)
        return await model.aCustom({
            query: `SELECT ${_fields} FROM ${table} ${_clauses} ${_order} ${_limit}`,
            values: _values,
            log: false
        });
    }

    static searchQSync(args = {}) {
        args.table = table
        return model.sRead(args);
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'pq_id'))
            out.pqId = elm.pq_id;
        if (_.has(elm, 'pq_type'))
            out.pqType = elm.pq_type;
        if (_.has(elm, 'pq_code'))
            out.pqCode = elm.pq_code;
        if (_.has(elm, 'pq_sender'))
            out.pqSender = elm.pq_sender;
        if (_.has(elm, 'pq_connection_type'))
            out.pqConnectionType = elm.pq_connection_type;
        if (_.has(elm, 'pq_receive_date'))
            out.pqReceiveDate = elm.pq_receive_date;
        if (_.has(elm, 'pq_payload'))
            out.pqPayload = elm.pq_payload;
        if (_.has(elm, 'pq_prerequisites'))
            out.pqPrerequisites = elm.pq_prerequisites;
        if (_.has(elm, 'pq_parse_attempts'))
            out.pqParseAttempts = elm.pq_parse_attempts;
        if (_.has(elm, 'pq_v_status'))
            out.pqVStatus = elm.pq_v_status;
        if (_.has(elm, 'pq_creation_date'))
            out.pqCreationDate = elm.pq_creation_date;
        if (_.has(elm, 'pq_insert_date'))
            out.pqInsertDate = elm.pq_insert_date;
        if (_.has(elm, 'pq_last_modified'))
            out.pqLastModified = elm.pq_last_modified;
        return out;
    }
}

module.exports = QueueUtils;
