const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const clog = require('../../loggers/console_logger');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const MESSAGE_TYPES = require('../../messaging-protocol/message-types')
const listener = require('../../plugin-handler/plugin-handler');
const GQLHandler = require('../../services/graphql/graphql-handler')

class ParsingQHandler {

    static handlePulledPacket(args) {
        const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
        let msg = '';
        let res = {};

        listener.doCallAsync('APSH_before_handle_pulled_packet', args);

        clog.app.info(`handle Pulled Packet args: ${utils.shortenLog(utils.stringify(args))}`);

        let receive_date = _.has(args, 'pq_receive_date') ? args.pq_receive_date : utils.getNow()
        let pq_type = _.has(args, 'pq_type') ? args.pq_type : null;
        let pq_code = _.has(args, 'pq_code') ? args.pq_code : '';
        let sender = _.has(args, 'pq_sender') ? args.pq_sender : null;
        let connection_type = _.has(args, 'pq_connection_type') ? args.pq_connection_type : iConsts.CONSTS.PUBLIC;
        /**
         * payload could be a block, GQL or even old-style messages
         * TODO: optimizine to use BUFFER for bigger payloads
         */
        let payload = _.has(args, 'pq_payload') ? args.pq_payload : null;

        if (utils._nilEmptyFalse(sender) || utils._nilEmptyFalse(payload)) {
            res.err = true
            res.msg = 'missed sender or paylod toparse'
            return res
        }

        if (utils._nilEmptyFalse(pq_type)) {
            msg = `missed pq_type ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg };
        }

        if ([iConsts.BLOCK_TYPES.Normal,
        iConsts.BLOCK_TYPES.Coinbase,
        iConsts.BLOCK_TYPES.FSign,
        iConsts.BLOCK_TYPES.SusBlock,
        iConsts.BLOCK_TYPES.FVote,
        iConsts.BLOCK_TYPES.POW].includes(pq_type))
            return ParsingQHandler.parsePureBlock({
                sender,
                connection_type,
                receive_date,
                payload,
                pq_type
            });

        // ancestors controlls
        let anConrollRes = ParsingQHandler.qUtils.ancestorsConroll({
            block: payload,
            pq_type
        });
        if (anConrollRes.err != false)
            return anConrollRes

        let blockHash = _.has(payload, 'blockHash') ? payload.blockHash : '';
        clog.app.info(`\n\n--- parsing block(${utils.hash6c(blockHash)}) \ntype(${pq_type}) Block/Message \nfrom Q.${sender}`);
        switch (pq_type) {

            // GQL part
            case GQLHandler.cardTypes.ProposalLoanRequest:
                res = require("../../contracts/pledge-contract/proposal-pledge-contract").handleReceivedProposalLoanRequest({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            case GQLHandler.cardTypes.FullDAGDownloadRequest:
                res = require('../../messaging-protocol/dag/download-full-dag').prepareFullDAGDlResponse({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            // it is already parsed and broke down in dispacherfr file
            // case GQLHandler.cardTypes.FullDAGDownloadResponse:
            //     res = ParsingQHandler.parsePureBlock({
            //         sender,
            //         connection_type,
            //         receive_date,
            //         payload: payload.block,
            //         pq_type: payload.block.bType
            //     });
            //     break;

            case GQLHandler.cardTypes.pleaseRemoveMeFromYourNeighbors:
                res = require('../../machine/machine-handler').neighborHandler.doDeleteNeighbor({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;



            //comunications
            case MESSAGE_TYPES.DAG_INVOKE_BLOCK:
                res = dagMsgHandler.handleBlockInvokeReq({
                    sender,
                    payload,
                    connection_type: connection_type
                })
                break;

            case MESSAGE_TYPES.DAG_INVOKE_DESCENDENTS:
                res = dagMsgHandler.handleDescendentsInvokeReq({
                    sender,
                    payload,
                    connection_type: connection_type
                })
                break;

            default:
                msg = `Unknown packet in parsing Q! ${pq_type} ${pq_code} from ${sender}`
                clog.sec.error(msg);
                res = { err: true, msg: msg, shouldPurgeMessage: true }
                break;
        }

        return res

    }

    static parsePureBlock(args) {

        let res = {};

        let sender = args.sender;
        let connection_type = args.connection_type;
        let receive_date = args.receive_date;
        let payload = args.payload;
        let pq_type = args.pq_type;

        // general ancestors controlls
        let genAancConrollRes = ParsingQHandler.qUtils.ancestorsConroll({
            block: payload,
            pq_type
        });
        if (genAancConrollRes.err != false)
            return genAancConrollRes


        // DAG existance ancestors controlls
        let DAGAncConrollRes = dagHandler.searchInDAGSync({
            query: [
                ['b_hash', ['IN', payload.ancestors]],
            ]
        });
        if (DAGAncConrollRes.length == 0) {
            return {
                err: true,
                shouldPurgeMessage: false,
                msg: `in order to parse 0block(${utils.hash6c(payload.blockHash)}) machine needs blocks(${payload.ancestors.map(x => utils.hash6c(x))}) exist in DAG`
            }
        } else {
            let missedBlocks = utils.arrayDiff(payload.ancestors, DAGAncConrollRes.map(x => x.bHash))
            if (missedBlocks.length > 0)
                return {
                    err: true,
                    shouldPurgeMessage: false,
                    msg: `in order to parse +block(${utils.hash6c(payload.blockHash)}) machine needs blocks(${missedBlocks.map(x => utils.hash6c(x))}) exist in DAG`
                }
        }



        switch (pq_type) {

            case iConsts.BLOCK_TYPES.Normal:
                res = require('../../dag/normal-block/handle-received-block').handleReceivedNormalBlock({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            case iConsts.BLOCK_TYPES.RpBlock:
                res = require('../../dag/coinbase/repayments-handler').handleReceivedRepayBlock({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            case iConsts.BLOCK_TYPES.Coinbase:
                res = require('../../dag/coinbase/handle-received-coinbase').handleReceivedCoinbase({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            case iConsts.BLOCK_TYPES.FSign:
                res = require('../../dag/floating-signatures/floating-signatures').handleReceivedFSBlock({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            // case iConsts.BLOCK_TYPES.SusBlock:
            //     res = require('../../dag/normal-block/sus-block-handler').handleReceivedSusBlock({
            //         sender,
            //         payload,
            //         connection_type,
            //         receive_date
            //     });
            //     break;

            case iConsts.BLOCK_TYPES.FVote:
                res = require('../../dag/floating-vote/floating-vote-handler').handleReceivedFVoteBlock({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;

            case iConsts.BLOCK_TYPES.POW:
                res = require('../../dag/pow-block/handle-received-block').handleReceivedPOWBlock({
                    sender,
                    payload,
                    connection_type,
                    receive_date
                });
                break;
        }

        return res;
    }




}

ParsingQHandler.inOutHandler = require('./queue-in-out-handler');
ParsingQHandler.inOutHandler._super = ParsingQHandler;

ParsingQHandler.qPicker = require('./queue-picker');
ParsingQHandler.qPicker._super = ParsingQHandler;

ParsingQHandler.qUtils = require('./queue-utils');
ParsingQHandler.qUtils._super = ParsingQHandler;

module.exports = ParsingQHandler;
