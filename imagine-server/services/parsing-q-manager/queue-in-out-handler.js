const _ = require('lodash');
const iConsts = require('../../config/constants');
const db = require('../../startup/db2')
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const MESSAGE_TYPES = require('../../messaging-protocol/message-types')
const listener = require('../../plugin-handler/plugin-handler');
const GQLHandler = require('../../services/graphql/graphql-handler')
const blockUtils = require('../../dag/block-utils');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const kvHandler = require('../../models/kvalue');

const table = 'i_parsing_q';


class QueueInOutHandler {

    static pushToParsingQSync(args) {

        try {
            // check for duplicate entries
            let dbl = model.sRead({
                table,
                query: [
                    ['pq_type', args.type],
                    ['pq_code', args.code],
                ]
            });
            if (dbl.length > 0) {
                return { err: false, shouldPurgeMessage: true }
            }

            listener.doCallSync('SPSH_before_insert_packet_in_q', args);

            let invokedPrerequisites = _.has(args, 'prerequisites') ? args.prerequisites : '';

            // control if needs some initiative prerequisities
            if (_.has(args.message, 'ancestors') && (args.message.ancestors.length > 0)) {
                // check if ancestores exist in parsing q
                let queuedAncs = model.sRead({
                    table,
                    query: [
                        ['pq_code', ['IN', args.message.ancestors]],
                    ]
                });
                let missedAnc = [];
                if (queuedAncs.length == 0) {
                    missedAnc = args.message.ancestors;
                    clog.app.info(`xxxxxxxxxxxxxxxxxxxxx block(${args.code}) totaly missed ancs (${missedAnc})`);
                } else if (queuedAncs.length < args.message.ancestors) {
                    missedAnc = utils.arrayDiff(args.message.ancestors, queuedAncs.map(x => x.pq_code))
                    clog.app.info(`yyyyyyyyyyyyyyyyyyyyyyyyyyyy block(${args.code}) partially missed ancs (${missedAnc})`);
                }

                clog.app.info(`zzzzzzzzzzzzzzzzzzz block(${args.code}) before + missed ancs (${invokedPrerequisites}) + (${missedAnc})`);

                // control if missedAnc alredy exist in DAG?
                let DAGedAncs = dagHandler.searchInDAGSync({
                    query: [
                        ['b_hash', ['IN', missedAnc]],
                    ]
                })
                if (DAGedAncs.length > 0) {
                    let eixtDAG = DAGedAncs.map(x => x.bHash)
                    clog.app.info(`yyyyyyyyyyyyyy yyyyyyy some likly missed blocks(${missedAnc}) already recorded in DAG(${eixtDAG})`);
                    missedAnc = utils.arrayDiff(missedAnc, eixtDAG)
                }
                invokedPrerequisites = utils.arrayAdd(invokedPrerequisites, missedAnc)
            }

            /**
             * if blcok is FVote, maybe we need customized treatment, since generally in DAG later blocks are depend on early blocks and it is one way graph
             * but in case of vote blocks, they have effect on previous blocks (e.g accepting or rejecting a transaction)
             * so depends on voting type(bCat) for, we need proper treatment
             */
            if (args.message.bType == iConsts.BLOCK_TYPES.FVote) {

                switch (args.message.bCat) {

                    case iConsts.FLOAT_BLOCKS_CATEGORIES.Trx:
                        /**
                         * if blcok is FVote, so insert uplink block in SUS BLOCKS WHICH NEEDED VOTES TO BE IMPORTED AHAED(SusBlockWNVTBIA)
                         */
                        let uplinkBlock = args.message.ancestors[0];    // FVote blocks always have ONLY one ancestor for which Fvote is voting
                        let currentWNVTBIA = kvHandler.getValueSync('SusBlockWNVTBIA');
                        if (currentWNVTBIA == null) {
                            currentWNVTBIA = [uplinkBlock];
                        } else {
                            currentWNVTBIA = utils.parse(currentWNVTBIA);
                            currentWNVTBIA.push(uplinkBlock);
                            currentWNVTBIA = utils.arrayUnique(currentWNVTBIA);
                        }
                        currentWNVTBIA = utils.stringify(currentWNVTBIA);
                        kvHandler.upsertKValueSync('SusBlockWNVTBIA', currentWNVTBIA)
                        break;

                }

            }

            // TODO: security issue to control block (specially payload), before insert to db 
            // potentially attacks: sql injection, corrupted JSON object ...
            model.sCreate({
                table,
                values: {
                    pq_type: args.type,
                    pq_code: args.code,
                    pq_sender: args.sender,
                    pq_connection_type: args.connectionType,
                    pq_receive_date: utils.getNow(),
                    pq_payload: blockUtils.wrapSafeObjectForDB({ obj: args.message }),
                    pq_prerequisites: invokedPrerequisites,
                    pq_parse_attempts: 0,
                    pq_v_status: "new",
                    pq_creation_date: args.creationDate,
                    pq_insert_date: utils.getNow(),
                    pq_last_modified: utils.getNow()
                },
                log: false
            });

            listener.doCallSync('SPSH_after_insert_packet_in_q', args);

            if (iConsts.isDevelopMode())
                model.sCreate({
                    table: 'idev_parsing_q',
                    values: {
                        pq_type: args.type,
                        pq_code: args.code,
                        pq_sender: args.sender,
                        pq_connection_type: args.connectionType,
                        pq_receive_date: utils.getNow(),
                        pq_payload: JSON.stringify(args.message),
                        pq_prerequisites: invokedPrerequisites,
                        pq_parse_attempts: 0,
                        pq_v_status: "new",
                        pq_creation_date: args.creationDate,
                        pq_insert_date: utils.getNow(),
                        pq_last_modified: utils.getNow()
                    },
                    log: false
                });


            QueueInOutHandler.rmoveFromParsingQ({
                query: [
                    ['pq_parse_attempts', ['>', iConsts.MAX_PARSE_ATTEMPS_COUNT]],
                    ['pq_creation_date', ['<', utils.minutesBefore(iConsts.getCycleByMinutes())]],
                ]
            })

            return { err: false, shouldPurgeMessage: true }

        } catch (e) {
            e = new Error(e);
            clog.app.info(e);
            return { err: true, msg: e, shouldPurgeMessage: true }

        }
    }

    static rmoveFromParsingQ(args = {}) {
        model.sDelete({
            table,
            query: args.query
        });
    }

}

module.exports = QueueInOutHandler;
