const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const listener = require('../../plugin-handler/plugin-handler');
const GQLHandler = require('../../services/graphql/graphql-handler')
const blockUtils = require('../../dag/block-utils');
const machine = require('../../machine/machine-handler');

const table = 'i_parsing_q';


class QueuePicker {

    static launchSmartPullFromParsingQ() {
        // some preparations ...
        QueuePicker.recursiveSmartPullFromParsingQ();
    }

    static recursiveSmartPullFromParsingQ() {
        console.log('iConsts.getParsingQGap()', iConsts.getParsingQGap());
        setTimeout(QueuePicker.recursiveSmartPullFromParsingQ, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getParsingQGap());
        let res = QueuePicker.smartPullQSync();
        if (res.err != false) {
            // if (_.has(res, 'msg') && !res.msg.startsWith('--- Break parsing block '))
                clog.app.error(`QueuePicker.smart PullQSync res: ${utils.stringify(res)}`);
        } else {
            clog.app.info(`QueuePicker.smart PullQSync res: ${utils.stringify(res)}`);
        }
    }

    static smartPullQSync(args = {}) {
        let res = {}
        let { query, values } = QueuePicker.prepareSmartQuery({ limit: 1 })
        let packets = model.sCustom({
            query: query,
            values: values,
            log: false
        })

        if (packets.length == 0) {
            res.err = false,
                res.msg = 'No packet in parsing q'
            return res
        }

        // dummy async to pick next value a little later
        setTimeout(() => {
            QueuePicker.smartPullQSync()
        }, 20000);

        let packet = packets[0];
        packet.pq_payload = blockUtils.openDBSafeObject(packet.pq_payload).content;

        QueuePicker.increaseToparseAttempsCountSync(packet);

        res = QueuePicker._super.handlePulledPacket(packet);
        if (res.shouldPurgeMessage == false) {
            // clog.app.error(`why not purge1! ${packet.pq_type} block(${utils.hash6c(packet.pq_code)}) from (${packet.pq_sender})`);
            // clog.app.error(`why not purge2! ${utils.stringify(res)}`);

        } else {
            model.sDelete({
                table,
                query: [
                    ['pq_sender', packet.pq_sender],
                    ['pq_type', packet.pq_type],
                    ['pq_code', packet.pq_code],
                ],
                log: false
            })
        }
        return res;
    }

    static increaseToparseAttempsCountSync(packet) {
        try {
            return model.sUpdate({
                table,
                query: [
                    ['pq_type', packet.pq_type],
                    ['pq_code', packet.pq_code],
                    ['pq_sender', packet.pq_sender],
                ],
                updates: { pq_parse_attempts: packet.pq_parse_attempts + 1, pq_last_modified: utils.getNow() },
                log: false
            });

        } catch (e) {
            return (new Error(e))
        }
    }

    static prepareSmartQuery(args = {}) {
        let fields = ' * '
        if (_.has(args, 'fields')) {
            fields = args.fields.join()
        }
        let _limit = ''
        if (_.has(args, 'limit')) {
            _limit = ' LIMIT ' + args.limit
        }
        // TODO: make a more intelligence query
        let query;
        let queryNumber = Math.random() * 100;
        if (machine.isInSyncProcess()) {
            if (queryNumber < 60) {
                // since it is in sync phase, so maybe better order is based on creationdate(TODO: optimize it to prevent cheater to vector attack)
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                    ORDER BY pq_connection_type ASC, pq_creation_date ASC ${_limit}
                    `;

            } else if ((queryNumber > 60) && (queryNumber < 90)) {
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                    ORDER BY pq_connection_type ASC, pq_parse_attempts ASC, pq_receive_date ASC ${_limit}
                    `;


            } else {
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                     ${_limit}
                    `;

            }

        } else {
            if (queryNumber < 60) {
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                    ORDER BY pq_connection_type ASC, pq_parse_attempts ASC, pq_receive_date ASC ${_limit}
                    `;

            } else if ((queryNumber > 60) && (queryNumber < 90)) {
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                    ORDER BY pq_connection_type ASC, pq_creation_date ASC ${_limit}
                    `;


            } else {
                query = `
                    SELECT ${fields} FROM ${table} WHERE pq_prerequisites=$1 
                     ${_limit}
                    `;

            }

        }

        let values = [''];
        return { query, values }
    }

}

module.exports = QueuePicker;
