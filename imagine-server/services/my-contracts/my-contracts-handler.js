const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const model = require("../../models/interface");
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const pledgeCloseHandler = require('../../contracts/pledge-contract/pledge-close');
const pldegeHandler = require('../../contracts/pledge-contract/pledge-handler');


const table = 'i_machine_onchain_contracts';

class MyContractHandler {


    static addContractToDb(args) {
        let values = {
            lc_type: args.lcType,
            lc_class: args.lcClass,
            lc_ref_hash: args.lcRefHash,
            lc_descriptions: args.lcDescriptions,
            lc_body: utils.stringify(args.lcBody),
        };
        model.sCreate({
            table,
            values
        });
    }

    static doAction(args = {}) {
        clog.app.info(`doAction args: ${utils.stringify(args)}`);
        let res;
        switch (args.actSubjType) {
            case iConsts.DOC_TYPES.Pledge:
                res = this.doActionPledge(args);
                break;
        }
        return res;
    }

    static doActionPledge(args) {
        let res;
        let pledge = this.searchInMyOnchainContracts({
            query: [['lc_id', args.actSubjCode]]
        });
        if (pledge.length == 0)
            return { err: TreeWalker, msg: `doAction failed ${utils.stringify(args)}` };

        args.pledge = pledge[0].lcBody;
        switch (args.pledge.dClass) {
            case iConsts.PLEDGE_CLASSES.PledgeP:
                res = this.doActionPledgeP(args);
                break;
        }
        return res;
    }

    static doActionPledgeP(args) {
        let res;
        switch (args.actCode) {
            case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee:
            case iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter:
                res = pledgeCloseHandler.closeByPledgeeOrByArbiter({
                    pledge: args.pledge,
                });
                break;

            case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger:
                res = pledgeCloseHandler.closeByPledger();
                break;

        }
        return res;
    }

    static searchInMyOnchainContracts(args = {}) {
        args.table = table;
        let res = model.sRead(args);
        if (res.length == 0)
            return [];

        let maxDate = iutils.getACycleRange().maxCreationDate;

        let newRes = [];
        for (let aRes of res) {
            aRes = MyContractHandler.convertFields(aRes);
            aRes.lcBody = utils.parse(aRes.lcBody);

            let isItMine = false;
            if (aRes.lcType == iConsts.DOC_TYPES.Pledge) {
                let myType = pldegeHandler.recognizeSignerTypeInfo({ pledge: aRes.lcBody });
                if (myType.signerType == 'pledgee') {
                    isItMine = true;
                    aRes.actions = [
                        { actTitle: 'Unpledge By Pledgee', actCode: iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee, actSubjType: iConsts.DOC_TYPES.Pledge, actSubjCode: aRes.lcId }
                    ];
                } else if (myType.signerType == 'arbiter') {
                    isItMine = true;
                    aRes.actions = [
                        { actTitle: 'Unpledge By Arbiter', actCode: iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter, actSubjType: iConsts.DOC_TYPES.Pledge, actSubjCode: aRes.lcId }
                    ];
                } else if (myType.signerType == 'pledger') {
                    isItMine = true;
                    aRes.actions = [
                        { actTitle: 'Unpledge By Pledger', actCode: iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger, actSubjType: iConsts.DOC_TYPES.Pledge, actSubjCode: aRes.lcId }
                    ];
                }
            }
            if (isItMine) {
                // retrieve complete info about contract
                let pledge = pldegeHandler.searchInPledgedAccounts({
                    query: [['pgd_hash', aRes.lcBody.hash]
                    ]
                });
                console.log('pledge pledgepledge pledge ', pledge);
                if (pledge.length == 1) {
                    aRes.pledge = pledge[0];
                    let isActive = iConsts.CONSTS.NO;
                    if ((aRes.pledge.pgdStatus == iConsts.CONSTS.OPEN && aRes.pledge.pgdActivateDate < maxDate) ||
                        (aRes.pledge.pgdStatus == iConsts.CONSTS.CLOSE && aRes.pledge.pgdCloseDate > maxDate))
                        isActive = iConsts.CONSTS.YES;
                    let status;
                    if (isActive == iConsts.CONSTS.YES) {
                        status = 'Active';
                    } else {
                        if (aRes.pledge.pgdActivateDate > maxDate) {
                            status = 'Waiting to be active';
                        } else if ((utils.isValidDateForamt(aRes.pledge.pgdCloseDate)) && (aRes.pledge.pgdCloseDate < maxDate)) {
                            status = 'Closed';
                        }
                    }
                    let realCloseDate = (utils.isValidDateForamt(aRes.pledge.pgdCloseDate)) ? aRes.pledge.pgdCloseDate : '-';
                    aRes.complementaryInfo = [
                        { cmTitle: 'Status', cmValue: status },
                        { cmTitle: 'Real Activate Date', cmValue: aRes.pledge.pgdActivateDate },
                        { cmTitle: 'Real Close Date', cmValue: realCloseDate },
                    ]
                }
                newRes.push(aRes)
            }
        }
        return newRes;
    }


    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'lc_id'))
            out.lcId = elm.lc_id;
        if (_.has(elm, 'lc_type'))
            out.lcType = elm.lc_type;
        if (_.has(elm, 'lc_class'))
            out.lcClass = elm.lc_class;
        if (_.has(elm, 'lc_descriptions'))
            out.lcDescriptions = elm.lc_descriptions;
        if (_.has(elm, 'lc_ref_hash'))
            out.lcRefHash = elm.lc_ref_hash;
        if (_.has(elm, 'lc_body'))
            out.lcBody = elm.lc_body;

        return out;
    }

}

module.exports = MyContractHandler;