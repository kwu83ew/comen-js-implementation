const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const DNAHandler = require('../../dna/dna-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const vHandler = require('../../services/version-handler/version-handler');


const tableBallots = 'i_ballots';
const tableMachineBallots = 'i_machine_ballots';

let ballotDoc = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dType: iConsts.DOC_TYPES.Ballot,
    dClass: iConsts.BALLOT_CLASSES.Basic,
    dLen: "0000000",
    dComment: "",
    dVer: "0.0.2",
    ref: "0000000000000000000000000000000000000000000000000000000000000000",    // reference pollingHash
    creationDate: "", // voting time
    voter: "",    // the bech32 address of shareholder
    vote: 0,    // a number between -100 t0 +100
    dExtInfo: {},
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000"
};

class GeneralBallotHandler {

    static alreadyVoted(args) {
        let dbl = model.sRead({
            table: tableBallots,
            query: [
                ['ba_voter', args.voter],
                ['ba_pll_hash', args.ref],
            ]
        });
        if (dbl.length > 0)
            return true;
        return false;
    }



    static recordBallotInDB(args) {
        let baReceiveDate = _.has(args, 'baReceiveDate') ? args.baReceiveDate : utils.getNow();
        // duplicate check
        let alreadyVoted = GeneralBallotHandler.alreadyVoted({
            voter: args.baVoter,
            ref: args.baPollingRef
        });
        let voteAmendmentAllowed = iConsts.CONSTS.NO;   // TODO: it must be retreived from dynamc POLLING_PROFILE_CLASSES. implement it!
        if ((alreadyVoted) && (voteAmendmentAllowed == iConsts.CONSTS.NO)) {
            // TODO: make a black list and put all duplicated ballot in it and not counting in final response
            return { err: true, msg: `already voted polling(${utils.hash6c(args.baPollingRef)}) voter(${iutils.shortBech8(args.baVoter)}) ` }
        }

        let values = {
            ba_hash: args.baHash,
            ba_pll_hash: args.baPollingRef,
            ba_creation_date: args.baCreationDate,    // ballot creation date, extracted from container block.creationDate
            ba_receive_date: baReceiveDate,   // machine local time
            ba_voter: args.baVoter,
            ba_voter_shares: args.baVoterShares,
            ba_vote: args.baVote,
            ba_comment: args.baComment,
            ba_vote_c_diff: args.baCreationDateDiff,
            ba_vote_r_diff: args.baReceiveDateDiff
        };
        clog.app.info(`inserting a Ballot: ${utils.stringify(values)}`);
        let res = model.sCreate({
            table: tableBallots,
            values
        });
    }

    static removeBallot(ballotHash) {
        model.sDelete({
            table: tableBallots,
            query: [['ba_hash', ballotHash]]
        });
    }

    static calcBallotCost(args) {
        let ballot = args.ballot;
        let cDate = args.cDate;
        let dLen = parseInt(ballot.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: ballot.dType, dClass: ballot.dClass, dLen });

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: ballot.dType,
                dClass: ballot.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static async doVote(args) {

        let msg;
        clog.app.info(`do vote args: ${utils.stringify(args)}`);
        // in order to send vote to network, must create a documetn of type vote and pay vote fee too
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // create ballot doc
        let ballot = this.prepareABallotDoc({
            ref: args.ref,
            vote: args.vote,
            voteComment: args.voteComment
        });
        clog.app.info(`prepared ballot to send ${utils.stringify(ballot)}`);
        console.log(`prepared ballot to send ${utils.stringify(ballot)}`);

        // calculate ballot cost
        let ballotCost = this.calcBallotCost({
            cDate: utils.getNow(),
            ballot,
            stage: iConsts.STAGES.Creating
        });
        if (ballotCost.err != false)
            return ballotCost;

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: ballotCost.cost * 1.3  // an small portion bigger to support DPCosts
        });
        clog.trx.info(`retrieve spendable UTXOs to pay for a Ballot: ${utils.stringify(ballot)}\nspendables: ${utils.stringify(spendables)}`);
        if (spendables.err != false)
            return spendables;

        // create a transaction for payment
        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_BALLOT', ballotCost.cost]
        ];
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(ballotCost.cost * 1.3),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: ballot.hash,
            description: 'Pay for a Ballot cost',
            inputs,
            outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed Ballot cost trx: ', utils.stringify(trxDtl));

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

        // inser ballot in local db (as a voted polling);
        let res = this.insertALocalBallot(ballot);
        if (res.err != false)
            return res;

        // push trx & vote-ballot to Block buffer
        res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = await docBufferHandler.pushInAsync({ doc: ballot, DPCost: ballotCost.cost });
        if (res.err != false)
            return res;


        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your vote is pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        // return new Promise((resolve, reject))
        let r = await wBroadcastBlock.broadcastBlockAsync();
        return r;
    }

    static prepareABallotDoc(args) {
        let voteDate = _.has(args, 'voteDate') ? args.voteDate : utils.getNow();
        let voter = _.has(args, 'voter') ? args.voter : machine.getMProfileSettingsSync().backerAddress;
        let voteComment = _.has(args, 'voteComment') ? args.voteComment : '';
        voteComment = utils.sanitizingContent(voteComment);
        voteComment = utils.stripNonAscii(voteComment); // TODO remove this line ASAP in order to be able comment in multi-language
        let unlockIndex = _.has(args, 'unlockIndex') ? args.unlockIndex : 0;
        let blt = _.clone(ballotDoc);
        blt.ref = args.ref;
        blt.creationDate = voteDate;
        blt.voter = voter;
        blt.vote = args.vote;
        blt.dComment = voteComment;
        blt.dExtInfo = {};
        let { signedHash, signMsg } = GeneralBallotHandler.getSignMsgDBallot(blt);
        let { uSet, signatures } = machine.signByMachineKey({ unlockIndex, signMsg });
        blt.dExtInfo.uSet = uSet;
        blt.dExtInfo.signatures = signatures;
        blt.dExtHash = GeneralBallotHandler.calcBallotExtInfoHash(blt, signedHash);
        blt.dLen = iutils.paddingDocLength(utils.stringify(blt).length);
        blt.hash = GeneralBallotHandler.calcHashDBallot(blt);
        return blt;
    }

    static calcBallotExtInfoHash(blt, signedHash) {
        // alphabetical order
        let obj;
        if (blt.dVer == '0.0.0') {
            obj = {
                signatures: blt.signatures,
                uSet: blt.uSet,
            }
        } else {
            obj = {
                signatures: blt.dExtInfo.signatures,
                signedHash,
                uSet: blt.dExtInfo.uSet
            }
        }
        clog.app.info(`calc BallotExtInfoHashcalc BallotExtInfoHashcalc BallotExtInfoHash: ${utils.stringify(obj)}`);

        let hash = iutils.doHashObject(obj);
        return hash;
    }

    static getSignMsgDBallot(blt) {
        let signables = GeneralBallotHandler.extractSignablePartsOfBallot(blt);
        clog.app.info(`Ballot signables: ${utils.stringify(signables)}`);
        let signedHash = iutils.doHashObject(signables);
        let signMsg = signedHash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return { signedHash, signMsg };
    }

    static extractSignablePartsOfBallot(blt) {
        // as always ordering properties by alphabet
        let signable = {
            creationDate: blt.creationDate,
            dClass: blt.dClass,
            dType: blt.dType,
            dVer: blt.dVer,
            ref: blt.ref,
            vote: blt.vote,
            voter: blt.voter
        }
        return signable;
    }

    static calcHashDBallot(blt) {
        // alphabetical order
        let hashables = {
            dExtHash: blt.dExtHash,
            dLen: blt.dLen
        }
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static insertALocalBallot(blt) {
        let mpCode = iutils.getSelectedMProfile();
        let dbl = model.sRead({
            table: tableMachineBallots,
            query: [
                ['lbt_mp_code', mpCode],
                ['lbt_pll_hash', blt.ref],
                ['lbt_voter', blt.voter]
            ]
        });
        if (dbl.length > 0)
            return { err: true, msg: `account(${iutils.shortBech8(blt.voter)}) already voted for (${utils.hash6c(blt.ref)})` };

        let shareInfo = DNAHandler.getAnAddressShares(blt.voter, blt.creationDate);
        let values = {
            lbt_mp_code: mpCode,
            lbt_hash: blt.hash,
            lbt_pll_hash: blt.ref,
            lbt_creation_date: blt.creationDate,
            lbt_vote: blt.vote,
            lbt_voter: blt.voter,
            lbt_voter_shares: shareInfo.shares,
            lbt_voter_percent: shareInfo.percentage
        };
        clog.app.info(`insert a local ballot ${utils.stringify(values)}`);
        model.sCreate({
            table: tableMachineBallots,
            values
        });

        return { err: false }
    }

    static searchInOnchainBallots(args = {}) {
        let fields = _.has(args, 'fields') ? args.fields : ['*'];
        let query = _.has(args, 'query') ? args.query : [];

        let res = model.sRead({
            fields,
            table: tableBallots,
            query
        });
        if (res.length == 0)
            return [];
        res = res.map(x => GeneralBallotHandler.convertFieldsOnchainBallots(x));
        return res;
    }

    static searchInLocalBallot(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'lbt_mp_code'))
            query.push(['lbt_mp_code', mpCode]);

        let res = model.sRead({
            table: tableMachineBallots,
            query
        });
        if (res.length == 0)
            return [];
        res = res.map(x => GeneralBallotHandler.convertFieldsMachineBallots(x));
        return res;
    }

    static convertFieldsMachineBallots(elm) {
        let out = {};
        if (_.has(elm, 'lbt_id'))
            out.lbtId = elm.lbt_id;
        if (_.has(elm, 'lbt_mp_code'))
            out.lbtmpCode = elm.lbt_mp_code;
        if (_.has(elm, 'lbt_hash'))
            out.lbtHash = elm.lbt_hash;
        if (_.has(elm, 'lbt_pll_hash'))
            out.lbtpllHash = elm.lbt_pll_hash;
        if (_.has(elm, 'lbt_creation_date'))
            out.lbtCreationDate = elm.lbt_creation_date;
        if (_.has(elm, 'lbt_voter'))
            out.lbtVoter = elm.lbt_voter;
        if (_.has(elm, 'lbt_voter_shares'))
            out.lbtVoterShares = elm.lbt_voter_shares;
        if (_.has(elm, 'lbt_voter_percent'))
            out.lbtVoterPercent = elm.lbt_voter_percent;
        if (_.has(elm, 'lbt_vote'))
            out.lbtVote = elm.lbt_vote;
        return out;
    }

    static convertFieldsOnchainBallots(elm) {
        let out = {};
        if (_.has(elm, 'ba_hash'))
            out.baHash = elm.ba_hash;
        if (_.has(elm, 'ba_pll_hash'))
            out.bapllHash = elm.ba_pll_hash;
        if (_.has(elm, 'ba_creation_date'))
            out.baCreationDate = elm.ba_creation_date;
        if (_.has(elm, 'ba_receive_date'))
            out.baReceiveDate = elm.ba_receive_date;
        if (_.has(elm, 'ba_voter'))
            out.baVoter = elm.ba_voter;
        if (_.has(elm, 'ba_voter_shares'))
            out.baVoterShares = elm.ba_voter_shares;
        if (_.has(elm, 'ba_vote'))
            out.baVote = elm.ba_vote;
        if (_.has(elm, 'ba_vote_c_diff'))
            out.baVoteCDiff = elm.ba_vote_c_diff;
        if (_.has(elm, 'ba_vote_r_diff'))
            out.baVoteRDiff = elm.ba_vote_r_diff;
        return out;
    }




}

module.exports = GeneralBallotHandler;
