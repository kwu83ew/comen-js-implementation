const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const machine = require('../../machine/machine-handler');
const vHandler = require('../../services/version-handler/version-handler');

const tablePollingProfiles = 'i_polling_profiles';
const tablePollings = 'i_pollings';
const tableBallots = 'i_ballots';


let pollingDoc = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dType: iConsts.DOC_TYPES.Polling,
    dClass: iConsts.POLLING_PROFILE_CLASSES.Basic.ppName,
    dLen: "0000000",
    dVer: "0.0.9",
    dComment: "",
    ref: "0000000000000000000000000000000000000000000000000000000000000000",    // reference pollingHash
    creationDate: "", // voting time
    creator: "",    // the bech32 address of shareholder
    dExtInfo: {},
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000"
};

class GeneralPollHandler {

    static prepareNewPolling(args) {
        // TODO: add startDate(a date in future & superiore than one cycle) in which votting will be started, 
        // currently voting start date is equal the block in which placed request

        let polling = _.clone(pollingDoc);
        polling.dType = _.has(args, 'dType') ? args.dType : iConsts.DOC_TYPES.Polling;
        polling.dClass = _.has(args, 'pollingProfileClass') ? args.pollingProfileClass : iConsts.POLLING_PROFILE_CLASSES.Basic.ppName;
        polling.dVer = args.dVer;
        polling.ref = args.ref;
        polling.creationDate = args.creationDate;
        polling.creator = args.creator;
        polling.dComment = args.dComment;
        polling.refType = args.refType;
        polling.refClass = args.refClass;
        polling.votingLongevity = this.normalizeVotingLongevity(args.longevity);

        let { signedHash, signMsg } = this.getSignMsgDPolling(polling);
        let eInfo = machine.signByMachineKey({ signMsg });
        polling.dExtInfo = {
            signatures: eInfo.signatures,
            uSet: eInfo.uSet
        }
        let extHashObj;
        if (vHandler.isOlderThan(polling.dVer, '0.0.9')) {
            extHashObj = {
                signatures: eInfo.signatures,
                uSet: eInfo.uSet
            }
        } else {
            extHashObj = {
                signatures: eInfo.signatures,
                signedHash,
                uSet: eInfo.uSet
            }
        }
        polling.dExtHash = iutils.doHashObject(extHashObj);
        polling.dLen = iutils.paddingDocLength(utils.stringify(polling).length);;
        polling.hash = this.calcHashDPolling(polling);
        return polling;
    }

    static normalizeVotingLongevity(votingLongevity) {
        if (iConsts.TIME_GAIN == 1)
            return parseInt(votingLongevity);
        return Math.trunc(parseFloat(votingLongevity) * 100) / 100
    }

    static getSignMsgDPolling(polling) {
        // as always ordering properties by alphabet
        let signables = {
            creationDate: polling.creationDate,
            creator: polling.creator,
            dClass: polling.dClass,// polling profile class
            dComment: polling.dComment,
            dType: polling.dType,
            dVer: polling.dVer,
            ref: polling.ref,
            refType: polling.refType,
            refClass: polling.refClass,
            votingLongevity: polling.votingLongevity
        }
        clog.app.info(`polling signables: ${utils.stringify(signables)}`);
        let signedHash = iutils.doHashObject(signables);
        let signMsg = signedHash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return { signedHash, signMsg };
    }

    static calcHashDPolling(polling) {
        // as always alphabetical sort
        console.log(`-------------------------------------------------`);
        console.log(`-------------------------------------------------`);
        console.log(`-------------------------------------------------`);
        console.log(`-------------------------------------------------`);
        console.log(`-------------------------------------------------`);
        console.log(`-------------------------------------------------`);
        console.log(`calc HashDPolling polling ${utils.stringify(polling)}`);
        let hashables = {
            dExtHash: polling.dExtHash,
            dLen: polling.dLen
        }

        clog.app.info(`calc HashDPolling polling ${utils.stringify(polling)}`);
        clog.app.info(`calc HashDPolling hashables ${utils.stringify(hashables)}`);
        console.log(`calc HashDPolling hashables ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        console.log(`hash hash ${hash}`);
        console.log(`-------------------------------------------------`);
        return hash;
    }

    static calcPollingCost(args) {
        let polling = args.polling;
        let stage = args.stage;
        let cDate = args.cDate;
        let voters = args.voters;   // the number of share holders of that particular block

        if (utils._nilEmptyFalse(voters) || (voters != parseInt(voters)))
            return { err: true, msg: `the Voters count is invalid ${voters}` }

        let dLen = parseInt(polling.dLen);

        let theCost =
            voters *
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: polling.dType, dClass: polling.dClass, dLen });

        if (stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: polling.dType,
                dClass: polling.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static removePollingG(pollingHash) {
        model.sDelete({
            table: tablePollings,
            query: [
                ['pll_hash', pollingHash]
            ]
        });
        return { err: false }
    }

    static removePollingByRelatedProposal(proposalHash) {
        let msg;
        //sceptical test
        let exist = model.sRead({
            table: tablePollings,
            query: [
                ['pll_ref_type', iConsts.POLLING_REF_TYPE.Proposal],
                ['pll_ref', proposalHash]
            ]
        });
        if (exist.length != 1) {
            msg = `Try to delete polling strange result! ${utils.stringify(exist)}`;
            clog.sec.err(msg);
            return { err: true, msg }
        }

        model.sDelete({
            table: tablePollings,
            query: [
                ['pll_ref_type', iConsts.POLLING_REF_TYPE.Proposal],
                ['pll_ref', proposalHash]
            ]
        });
        return { err: false }
    }
    
    static removePollingByRelatedAdmPolling(admPollingDocHash) {
        let msg;
        //sceptical test
        let exist = model.sRead({
            table: tablePollings,
            query: [
                ['pll_ref_type', iConsts.POLLING_REF_TYPE.AdmPolling],
                ['pll_ref', admPollingDocHash]
            ]
        });
        if (exist.length != 1) {
            msg = `Try to delete polling${admPollingDocHash} strange result! ${utils.stringify(exist)}`;
            clog.sec.err(msg);
            return { err: true, msg }
        }

        model.sDelete({
            table: tablePollings,
            query: [
                ['pll_ref_type', iConsts.POLLING_REF_TYPE.AdmPolling],
                ['pll_ref', admPollingDocHash]
            ]
        });
        return { err: false }
    }


    static async maybeUpdateOpenPollingsStat() {

        let query = []
        /**
         * this query is because in synching process there is moment in which a polling is closed before all ballots beeing considered
         * so we re-processing ALL pollings
         */
        if (!machine.isInSyncProcess())
            query.push(['pll_status', iConsts.CONSTS.OPEN]);
        let openPollings = model.sRead({
            table: tablePollings,
            fields: ['pll_hash'],
            query
        });
        clog.app.info(`maybe Update OpenPollingsStat: ${openPollings.map(x => utils.hash6c(x.pll_hash))}`);
        for (let aPolling in openPollings) {
            this.maybeUpdatePollingStat(aPolling.pll_hash);
        }
        return { err: false };
    }

    /**
     * 
     * @param {*} pollingHash 
     * it re-calculate all Ballots and refreshes the polling final results
     */
    static maybeUpdatePollingStat(pollingHash) {
        let msg;
        let polling = GeneralPollHandler.getPollingInfo({ query: [['pll_hash', pollingHash]] });
        if (polling.length == 0) {
            msg = `polling(${utils.hash6c(pollingHash)}) does not exist in DAG`;
            return { err: true, msg }
        }
        polling = polling[0];
        clog.app.info(`retrieve polling(${utils.hash6c(pollingHash)}) info:${utils.stringify(polling)}`);

        // this control is commented because in synching process there is moment in which a polling is closed before all ballots beeing considered
        // TODO: FIXIT ASAP
        // // control if polling still is open & active
        // if (polling.pllStatus == iConsts.CONSTS.CLOSE)
        //     return { err: false, msg: `polling(${utils.hash6c(pollingHash)}) already closed` };


        let ballots = model.sRead({
            table: tableBallots,
            query: [
                ['ba_pll_hash', pollingHash],
            ]
        });
        let yesPeriod = polling.pllLongevity * 60;

        let vStatistics = {
            votesY: {
                count: 0,
                shares: 0,
                gain: 0,
                value: 0
            },
            votesA: {
                count: 0,
                shares: 0,
                gain: 0,
                value: 0
            },
            votesN: {
                count: 0,
                shares: 0,
                gain: 0,
                value: 0
            }
        }
        for (let aBall of ballots) {

            let voteGain = GeneralPollHandler.calculateVoteGain(aBall.ba_vote_c_diff, aBall.ba_vote_r_diff, yesPeriod);
            clog.app.info(`creationDif: ${aBall.ba_vote_c_diff}, receiveDiff: ${aBall.ba_vote_r_diff}, yesPeriod: ${yesPeriod}`);
            clog.app.info(`voteGain: ${utils.stringify(voteGain)}`);
            // let shares = (aBall.ba_voter_shares);
            let shares = iutils.convertBigIntToJSInt(aBall.ba_voter_shares);

            if (aBall.ba_vote > 0) {
                vStatistics.votesY.count++;
                vStatistics.votesY.shares += shares;
                vStatistics.votesY.gain += utils.floor(shares * voteGain.gainYes);
                vStatistics.votesY.value += utils.floor(vStatistics.votesY.gain * aBall.ba_vote);

            } else if (aBall.ba_vote == 0) {
                vStatistics.votesA.count++;
                vStatistics.votesA.shares += shares;
                vStatistics.votesA.gain += utils.floor(shares * voteGain.gainNoAbstain);
                vStatistics.votesA.value += 0;

            } else if (aBall.ba_vote < 0) {
                vStatistics.votesN.count++;
                vStatistics.votesN.shares += shares;
                vStatistics.votesN.gain += utils.floor(shares * voteGain.gainNoAbstain);
                vStatistics.votesN.value += utils.floor(vStatistics.votesN.gain * Math.abs(aBall.ba_vote));

            }
        }

        vStatistics.pllStatus = (utils.minutesAfter(polling.pllLongevity * 60 * 1.5, polling.pllStartDate) < utils.getNow()) ? iConsts.CONSTS.CLOSE : iConsts.CONSTS.OPEN;

        clog.app.info(`vStatistics: ${utils.stringify(vStatistics)}`);
        // update polling info
        model.sUpdate({
            table: tablePollings,
            query: [
                ['pll_hash', pollingHash]
            ],
            updates: {
                pll_status: vStatistics.pllStatus,

                pll_y_count: vStatistics.votesY.count,
                pll_y_shares: vStatistics.votesY.shares,
                pll_y_gain: vStatistics.votesY.gain,
                pll_y_value: vStatistics.votesY.value,

                pll_n_count: vStatistics.votesN.count,
                pll_n_shares: vStatistics.votesN.shares,
                pll_n_gain: vStatistics.votesN.gain,
                pll_n_value: vStatistics.votesN.value,

                pll_a_count: vStatistics.votesA.count,
                pll_a_shares: vStatistics.votesA.shares,
                pll_a_gain: vStatistics.votesA.gain,
                pll_a_value: vStatistics.votesA.value,
            }
        });
        return { err: false }
    }

    static updatePolling(args) {
        args.table = tablePollings;
        return model.sUpdate(args);
    }


    /**
 * 
 * @param {*} voteCreationTimeByMinute      : timeDiff by minutes (VotingDateRecordedInBlock.creationDate - dateOfStartVoting)
 * @param {*} voteReceiveingTimeByMinute    : timeDiff by minutes (receivingDate - dateOfStartVoting)
 * @param {*} votingLongevityByMinute 
 */
    static calculateVoteGain(voteCreationTimeByMinute, voteReceiveingTimeByMinute, votingLongevityByMinute = null) {
        if (votingLongevityByMinute == null)
            votingLongevityByMinute = iConsts.getCycleByMinutes() * 2;

        let offset = iConsts.getCycleByMinutes() / 4; // 3 hour(1/4 cycle) will be enough to entire network to be synched
        let minRange = iConsts.getCycleByMinutes() * 2; // 24 hour(2 cycle)
        let latenancyFactor = Math.log(voteReceiveingTimeByMinute - voteCreationTimeByMinute + minRange) / Math.log(minRange);

        let gainYes = 0;
        if (voteCreationTimeByMinute < votingLongevityByMinute) {
            gainYes = (Math.log(votingLongevityByMinute - voteCreationTimeByMinute + 1) / Math.log(votingLongevityByMinute + offset)) / latenancyFactor;
            gainYes = utils.customFloorFloat(gainYes, 2);
        }

        let gainNoAbstain = 0;
        if (voteCreationTimeByMinute < (votingLongevityByMinute * 1.5)) {
            gainNoAbstain = (Math.log((votingLongevityByMinute * 1.5) - voteCreationTimeByMinute + 1) / Math.log((votingLongevityByMinute * 1.5) + offset)) / latenancyFactor;
            gainNoAbstain = utils.customFloorFloat(gainNoAbstain, 2);
        }

        return { gainYes, gainNoAbstain, latenancyFactor };

        // primitive Sigmoidal Membership Function implementaition
        // TODO: improvement needed, based on network delay propogation and ...

    }



    static loadPollingProfiles() {
        let res = model.sRead({
            table: tablePollingProfiles,
            order: [['ppr_name', 'ASC']]
        });
        res = res.map(x => GeneralPollHandler.convertFieldsProfiles(x));
        return res;
    }

    static convertFieldsProfiles(elm) {
        let out = {};
        if (_.has(elm, 'ppr_name'))
            out.ppName = elm.ppr_name;
        if (_.has(elm, 'ppr_activated'))
            out.pprActivated = elm.ppr_activated;
        if (_.has(elm, 'ppr_perform_type'))
            out.pprPerformType = elm.ppr_perform_type;
        if (_.has(elm, 'ppr_votes_counting_method'))
            out.pprVotesCountingMethod = elm.ppr_votes_counting_method;
        if (_.has(elm, 'ppr_version'))
            out.pprVersion = elm.ppr_version;
        return out;
    }

    static initPollingProfiles() {
        let dbl = model.sRead({
            table: tablePollingProfiles,
            query: [['ppr_name', iConsts.POLLING_PROFILE_CLASSES.Basic.ppName]]
        });
        if (dbl.length > 0)
            return;

        for (let aPollingProfileKey of utils.objKeys(iConsts.POLLING_PROFILE_CLASSES)) {
            let values = {
                ppr_name: iConsts.POLLING_PROFILE_CLASSES[aPollingProfileKey].ppName,
                ppr_activated: iConsts.POLLING_PROFILE_CLASSES[aPollingProfileKey].activated,
                ppr_perform_type: iConsts.POLLING_PROFILE_CLASSES[aPollingProfileKey].performType,
                ppr_amendment_allowed: iConsts.POLLING_PROFILE_CLASSES[aPollingProfileKey].voteAmendmentAllowed,
                ppr_votes_counting_method: iConsts.POLLING_PROFILE_CLASSES[aPollingProfileKey].votesCountingMethod,
                ppr_version: '0.0.8'
            };
            // console.log('creating Basic polling profile', values);
            model.sCreate({
                table: tablePollingProfiles,
                values
            });
        }


        // let basciProfile = {
        //     performType: iConsts.POLL_PERFORMANCE_TYPES.Transparent, // currently it is ONLY Transparent, later we run more developed perform (e.g. ZKP)
        //     votesCountingMethod: iConsts.VOTE_COUNTING_METHODS.PluralityLog, // vote counting method(Plurality, ...)
        // }
        // let hash = iutils.doHashObject(basciProfile);

    }


    /**
     * 
     * @param {*} args 
     * the function records an onlchain polling doc(dType=Polling) to database, in order to query it easily
     */
    static recordPollingInDB(args) {
        let block = args.block;
        let polling = args.polling;

        clog.app.info(`record A general polling(${utils.stringify(polling)}) `);

        let values = {
            pll_hash: polling.hash,
            pll_creator: polling.creator,
            pll_type: polling.dType,
            pll_class: polling.dClass, // it is equal to pollingDoc.dClass

            pll_ref: polling.ref,  // referenced voting subject(could be proposalHash,...)
            pll_ref_type: polling.refType,  // referenced voting subject(could be proposalHash,...)
            pll_ref_class: polling.refClass,  // referenced voting subject(could be proposalHash,...)

            pll_start_date: block.creationDate, // TODO: improve to user can set different start date(at least one cycle later than block.creationDate)
            pll_longevity: polling.votingLongevity,
            pll_version: polling.dVer,
            pll_comment: polling.dComment,
            pll_y_count: 0,
            pll_y_shares: 0,
            pll_y_gain: 0,
            pll_y_value: 0,
            pll_n_count: 0,
            pll_n_shares: 0,
            pll_n_gain: 0,
            pll_n_value: 0,
            pll_a_count: 0,
            pll_a_shares: 0,
            pll_a_gain: 0,
            pll_a_value: 0,
            pll_status: _.has(args, 'status') ? args.status : iConsts.CONSTS.OPEN
        };

        console.log(`new general polling is creating by args: ${utils.stringify(values)}`);
        clog.app.info(`new general polling is creating by args: ${utils.stringify(values)}`);
        model.sCreate({
            table: tablePollings,
            values
        })
        return { err: false };
    }


    static autoCreatePollingForProposal(args) {
        console.log(`create NewPolling args: ${utils.stringify(args)}`);
        clog.app.info(`create NewPolling args: ${utils.stringify(args)}`);

        let pllVersion = _.has(args, 'pllVersion') ? args.pllVersion : '0.0.0';
        if (vHandler.isOlderThan(pllVersion, '0.0.8')) {
            pllVersion = '0.0.0';
        }
        let values = {
            pll_creator: args.creator,
            pll_type: args.dType,
            pll_class: args.dClass, // it is equal to pollingDoc.dClass

            pll_ref: args.ref,  // referenced voting subject(could be proposalHash,...)
            pll_ref_type: args.refType,  // referenced voting subject(could be proposalHash,...)
            pll_ref_class: args.refClass,  // referenced voting subject(could be proposalHash,...)

            pll_start_date: args.startDate,
            pll_longevity: args.longevity,
            pll_version: pllVersion,
            pll_comment: args.comment,
            pll_y_count: 0,
            pll_y_shares: 0,
            pll_y_gain: 0,
            pll_y_value: 0,
            pll_n_count: 0,
            pll_n_shares: 0,
            pll_n_gain: 0,
            pll_n_value: 0,
            pll_a_count: 0,
            pll_a_shares: 0,
            pll_a_gain: 0,
            pll_a_value: 0,
            pll_status: _.has(args, 'status') ? args.status : iConsts.CONSTS.OPEN
        };

        let tmpHashObj = {
            creationDate: args.startDate,
            creator: args.creator,

            dType: args.dType,
            dClass: args.dClass, //=iConsts.POLLING_PROFILE_CLASSES.Basic.ppName,

            dComment: args.comment,
            longevity: args.longevity,

            // referenced proposal
            ref: args.ref,
            refType: args.refType,
            refClass: args.refClass,

            dVer: pllVersion,
        }
        if (vHandler.isOlderThan(pllVersion, '0.0.8')) {
            tmpHashObj.dExtHash = 'noDExtHash';
        } else {
            tmpHashObj.dExtHash = iutils.doHashObject(tmpHashObj);
        }
        tmpHashObj.dLen = iutils.paddingDocLength(utils.stringify(tmpHashObj).length);
        values.pll_hash = GeneralPollHandler.calcHashDPolling(tmpHashObj);

        clog.app.info(`new polling for proposal is creating by args: ${utils.stringify(values)}`);
        model.sCreate({
            table: tablePollings,
            values
        })
        return { err: false };

    }

    static getPollingInfo(args) {
        args.table = tablePollings;
        let res = model.sRead(args);
        if (res.length == 0)
            return [];

        res = res.map(x => GeneralPollHandler.convertFieldsPollings(x));
        return res;
    }

    static convertFieldsPollings(elm) {
        let out = {};
        if (_.has(elm, 'pll_hash'))
            out.pllHash = elm.pll_hash;
        if (_.has(elm, 'pll_type'))
            out.pllType = elm.pll_type;
        if (_.has(elm, 'pll_class'))
            out.pllClass = elm.pll_class;   // ehich is equal to polling profile code
        if (_.has(elm, 'pll_ref_type'))
            out.pllRefType = elm.pll_ref_type;
        if (_.has(elm, 'pll_ref'))
            out.pllRef = elm.pll_ref;
        if (_.has(elm, 'pll_start_date'))
            out.pllStartDate = elm.pll_start_date;
        if (_.has(elm, 'pll_longevity'))
            out.pllLongevity = elm.pll_longevity;
        if (_.has(elm, 'pll_version'))
            out.pllVersion = elm.pll_version;

        if (_.has(elm, 'pll_y_count'))
            out.pllYCount = iutils.convertBigIntToJSInt(elm.pll_y_count);
        if (_.has(elm, 'pll_y_shares'))
            out.pllYShares = iutils.convertBigIntToJSInt(elm.pll_y_shares);
        if (_.has(elm, 'pll_y_gain'))
            out.pllYGain = iutils.convertBigIntToJSInt(elm.pll_y_gain);
        if (_.has(elm, 'pll_y_value'))
            out.pllYValue = iutils.convertBigIntToJSInt(elm.pll_y_value);

        if (_.has(elm, 'pll_n_count'))
            out.pllNCount = iutils.convertBigIntToJSInt(elm.pll_n_count);
        if (_.has(elm, 'pll_n_shares'))
            out.pllNShares = iutils.convertBigIntToJSInt(elm.pll_n_shares);
        if (_.has(elm, 'pll_n_gain'))
            out.pllNGain = iutils.convertBigIntToJSInt(elm.pll_n_gain);
        if (_.has(elm, 'pll_n_value'))
            out.pllNValue = iutils.convertBigIntToJSInt(elm.pll_n_value);

        if (_.has(elm, 'pll_a_count'))
            out.pllACount = iutils.convertBigIntToJSInt(elm.pll_a_count);
        if (_.has(elm, 'pll_a_shares'))
            out.pllAShares = iutils.convertBigIntToJSInt(elm.pll_a_shares);
        if (_.has(elm, 'pll_a_gain'))
            out.pllAGain = iutils.convertBigIntToJSInt(elm.pll_a_gain);
        if (_.has(elm, 'pll_a_value'))
            out.pllAValue = iutils.convertBigIntToJSInt(elm.pll_a_value);

        if (_.has(elm, 'pll_status'))
            out.pllStatus = elm.pll_status;
        if (_.has(elm, 'pll_ct_done'))
            out.pllCTDone = elm.pll_ct_done;
        return out;

    }
}
GeneralPollHandler.ballotHandler = require('./ballot-handler');
GeneralPollHandler.concludeHandler = require('./conclude-treatment-handler');
GeneralPollHandler.concludeHandler._super = GeneralPollHandler;

module.exports = GeneralPollHandler;
