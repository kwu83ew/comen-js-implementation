const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const listener = require('../../plugin-handler/plugin-handler');
const machine = require('../../machine/machine-handler');

const tablePollings = 'i_pollings';


class ConcludeHandler {

    static launchRecursiveConcludeTreatment() {
        ConcludeHandler.recursiveConcludeTreatment();
    }

    static recursiveConcludeTreatment() {
        // do recursive callbacks
        let gapByMinutes;
        if (iConsts.TIME_GAIN == 1) {
            // live
            if (machine.isInSyncProcess()) {
                // it is live setting, so every 1 minutes do callback
                gapByMinutes = 1;

            } else {
                // it is live setting, so every 31 minutes do callback
                gapByMinutes = 31;
            }

        } else {
            // devel
            if (machine.isInSyncProcess()) {
                // forteen times in each cycle do callback
                gapByMinutes = iConsts.TIME_GAIN / 14;
            } else {
                // four times in each cycle do callback
                gapByMinutes = iConsts.TIME_GAIN / 4;
            }
        }
        clog.app.info(`recursive Conclude Treatment, every ${gapByMinutes} minutes`);
        setTimeout(ConcludeHandler.recursiveConcludeTreatment, iConsts.ONE_MINUTE_BY_MILISECOND * gapByMinutes);
        ConcludeHandler.doPollingConcludeTreatment();
    }

    static doPollingConcludeTreatment(args = {}) {
        let forceToUpdate = _.has(args, 'forceToUpdate') ? args.forceToUpdate : false;
        if (machine.isInSyncProcess())
            forceToUpdate = true;

        // select the pollings which conclude treatment = No, and are closed in previous cycle
        let query = [];
        if (forceToUpdate) {
            /**
             * Add an start date limitation filter to not fetching all pollings 
             * But only after than 60 cycles (almost one month) ago
             * TODO: fixit for polling which duration is longer than one month and in synching/booting a node maybe makes problem
             **/
            if (machine.isInSyncProcess()) {
                const dagHandler = require('../../dag/graph-handler/dag-handler');
                let lastWBLock = dagHandler.walkThrough.getLatestBlock().wBlock;
                let oldestDate = iutils.getACycleRange({ backByCycle: 70, cDate: lastWBLock.bCreationDate }).minCreationDate;
                query.push(['pll_start_date', ['>=', oldestDate]]);
            }
        } else {
            query.push(['pll_ct_done', iConsts.CONSTS.NO]);
        }
        let pollings = ConcludeHandler._super.getPollingInfo({
            query
        });
        for (let aPolling of pollings) {
            args.aPolling = aPolling;
            this.doOnePollingConcludeTreatment(args);
        }
        return { err: false };
    }

    static doOnePollingConcludeTreatment(args) {
        clog.app.info(`conclude Treatment args: ${utils.stringify(args)}`);

        let aPolling = args.aPolling;
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let pollingEndDate = utils.minutesAfter(aPolling.pllLongevity * 60 * 1.5, aPolling.pllStartDate);
        let approveDate = utils.minutesAfter(iConsts.getCycleByMinutes() * 2, pollingEndDate);
        approveDate = iutils.getCoinbaseRange(approveDate).from;
        clog.app.info(`retrive for conclude Treatment polling(${utils.hash6c(aPolling.pllHash)}) pollingEndDate(${pollingEndDate})`);

        // generaly Close the pollings which are finished the voting time
        if (pollingEndDate < utils.getNow()) {
            ConcludeHandler._super.maybeUpdatePollingStat(aPolling.pllHash);
            // retrieve (maybe)updated info
            aPolling = ConcludeHandler._super.getPollingInfo({
                query: [
                    ['pll_hash', aPolling.pllHash],
                ]
            })[0];
        }

        // compareDate is the start date of last cycle, so ONLY pollings with close date before compare date can be consider and treat the conclude treatments
        let compareDate = iutils.getACycleRange({ backByCycle: 1, cDate }).minCreationDate;
        clog.app.info(`retrive for conclude Treatment polling(${utils.hash6c(aPolling.pllHash)}) compareDate(${compareDate})`);
        /**
         * TODO: implement a way to run a customized contract as a conclude treatment, at least as a plugin on some nodes. 
         * somethig like PledgeP conclude by Arbiters
         */
        clog.app.info(`controll if polling(${utils.hash6c(aPolling.pllHash)}) end date(${pollingEndDate}) is before the previous cycle end(${compareDate})`);
        if (pollingEndDate < compareDate) {
            clog.app.info(`comparing pllYValue(${aPolling.pllYValue}) & pllNValue(${aPolling.pllNValue})`);
            if (aPolling.pllYValue >= aPolling.pllNValue) {
                clog.app.info(`conclude Winer polling(${utils.hash6c(aPolling.pllHash)}) ${utils.stringify(aPolling)}`);
                this.treatPollingWon({
                    polling: aPolling,
                    approveDate
                });

            } else {
                clog.app.info(`conclude Missed polling(${utils.hash6c(aPolling.pllHash)}). ${utils.stringify(aPolling)}`);
                this.treatPollingMissed({
                    polling: aPolling,
                    approveDate
                });

            }

            // mark polling as conclude treatment done
            model.sUpdate({
                table: tablePollings,
                query: [
                    ['pll_hash', aPolling.pllHash]
                ],
                updates: { pll_ct_done: iConsts.CONSTS.YES }
            });
        }


        // custome polling-conclude-treatments
        if (pollingEndDate < compareDate) {
            if (aPolling.pllYValue >= aPolling.pllNValue) {
                listener.doCallSync('SASH_custom_polling_winer', { polling: aPolling });
            } else {
                listener.doCallSync('SASH_custom_polling_missed', { polling: aPolling });
            }
            listener.doCallSync('SASH_custom_polling_conclude_treatment_done', { polling: aPolling });
        }
    }

    static treatPollingWon(args) {
        let polling = args.polling;
        let approveDate = args.approveDate;
        /**
         * depends on pll_ref_type needs different treatment. there are a bunch of reserved pollings(e.g. Proposal, AdmPollings,...)
         */
        switch (polling.pllRefType) {

            case iConsts.POLLING_REF_TYPE.Proposal:
                const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
                proposalHandler.transformApprovedProposalToDNAShares({
                    polling,
                    approveDate
                });
                break;

            case iConsts.POLLING_REF_TYPE.AdmPolling:
                const admPollingsHandler = require('../../web-interface/adm-pollings/adm-pollings-handler');
                admPollingsHandler.onChain.treatPollingWon({
                    polling,
                    approveDate
                });
                break;


            // case iConsts.POLLING_REF_TYPE.ReqForRelRes:
            // TODO move it to up "admPollingsHandler.onChain.treatPollingWon"
            //     const reservedHandler = require('../../dag/coinbase/reserved-coins-handler');

            //     clog.app.info(`controll if ReqForRelRes polling(${utils.hash6c(polling.pllHash)}) end date(${pollingEndDate}) is before the previous cycle end(${compareDate})`);
            //     if (pollingEndDate < compareDate) {
            //         let cTRes = reservedHandler.doReqRelConcludeTreatment({ polling: polling, pollingEndDate, compareDate });
            //     }
            //     break;
        }
    }

    static treatPollingMissed(args) {
        let polling = args.polling;
        let approveDate = args.approveDate;
        /**
         * depends on pll_ref_type needs different treatment. there are a bunch of reserved pollings(e.g. Proposal, AdmPollings,...)
         */
        switch (polling.pllRefType) {

            case iConsts.POLLING_REF_TYPE.Proposal:
                const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
                proposalHandler.concludeProposal({
                    polling,
                    approveDate
                });
                break;

            case iConsts.POLLING_REF_TYPE.AdmPolling:
                const admPollingsHandler = require('../../web-interface/adm-pollings/adm-pollings-handler');
                admPollingsHandler.onChain.concludeAdmPolling({
                    polling,
                    approveDate
                });
                break;

        }

    }
}

module.exports = ConcludeHandler;
