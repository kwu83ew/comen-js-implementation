const _ = require('lodash');
const crypto = require('../../crypto/crypto');
const bdLogger = require('../../loggers/broadcast-logger')
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const iConsts = require('../../config/constants');
const networker = require('../../network-adapter/network-pusher');
const db = require('../../startup/db2')


const table = 'i_sending_q';


class SendingQHandler {

    static preparePacketsForNeighbors(args) {
        clog.app.info(`prepare PacketsForNeighbors args: ${utils.stringify(args)}`);
        const machine = require('../../machine/machine-handler');
        let msg;
        let denayDoubleSendCheck = _.has(args, 'denayDoubleSendCheck') ? args.denayDoubleSendCheck : false;
        let sqType = _.has(args, 'sqType') ? args.sqType : 'UnTp';
        let sqCode = _.has(args, 'sqCode') ? args.sqCode : 'UnCd';
        let sqTitle = _.has(args, 'sqTitle') ? args.sqTitle : 'newBlock';
        let sqReceivers = _.has(args, 'sqReceivers') ? args.sqReceivers : null;
        let noReceivers = _.has(args, 'noReceivers') ? args.noReceivers : null;

        if (sqReceivers != null) {
            clog.app.info(`targeted packet to ${sqReceivers}`);
        }
        if (noReceivers != null) {
            clog.app.info(`no targeted packet to ${noReceivers}`);
        }

        let neighbors = machine.neighborHandler.getActiveNeighborsSync();
        if (!utils._nilEmptyFalse(sqReceivers) && sqReceivers.length > 0) {
            // keep only requested neighbors
            let selectedNeighbors = [];
            for (let neighbor of neighbors) {
                if (sqReceivers.includes(neighbor.nEmail))
                    selectedNeighbors.push(neighbor)
            };
            neighbors = selectedNeighbors;
        }

        if (!utils._nilEmptyFalse(noReceivers) && noReceivers.length > 0) {
            // keep only requested neighbors
            let selectedNeighbors = [];
            for (let neighbor of neighbors) {
                if (!noReceivers.includes(neighbor.nEmail))
                    selectedNeighbors.push(neighbor)
            };
            neighbors = selectedNeighbors;
        }

        clog.app.info(`Finall Selected Neighbors=${neighbors.length}`);
        if (neighbors.length == 0) {
            clog.app.error('there is no neighbore to send prepare Packets For Neighbors');
            return []
        }


        let machineSettings = machine.getMProfileSettingsSync()
        // clog.app.info(machineSettings); 
        let machinePubEmail = machineSettings.pubEmail.address;
        let machinePubEmailPGPPrvKey = machineSettings.pubEmail.PGPPrvKey;
        let machinePrvEmail = machineSettings.pubEmail.address; // TODO: fix it to retreive right private email
        let machinePrvEmailPGPPrvKey = machineSettings.pubEmail.PGPPrvKey; // TODO: fix it to retreive right private email

        let alreadyBroadcasted = bdLogger.listSentBlocks({
            fields: ["lb_type", "lb_code", "lb_sender", "lb_receiver"]
        }).map(x => [x.lb_sender, x.lb_receiver, x.lb_type, x.lb_code].join());
        // console.log('alreadyBroadcasted', alreadyBroadcasted);
        // alreadyBroadcasted=[];

        clog.app.info(`neighbors to send ${utils.stringify(neighbors)}`);
        let packets = [];
        let neighbor;
        let emailBody, sender;
        for (let i = 0; i < neighbors.length; i++) {
            neighbor = neighbors[i]

            if (utils._nilEmptyFalse(neighbor.nPGPPublicKey) || (neighbor.nPGPPublicKey == ''))
                continue;

            let params = {
                shouldSign: true,
                shouldCompress: true,
                message: args.sqPayload,
                receiverPubKey: neighbor.nPGPPublicKey
            };
            if (neighbor.nConnectionType == iConsts.CONSTS.PRIVATE) {
                sender = machinePrvEmail;
                params["sendererPrvKey"] = machinePrvEmailPGPPrvKey;
            } else {
                sender = machinePubEmail;
                params["sendererPrvKey"] = machinePubEmailPGPPrvKey;
            }
            let key = [sender, neighbor.nEmail, sqType, sqCode].join()
            if (alreadyBroadcasted.includes(key)) {
                console.log(`already send packet! ${key}`);
                clog.app.error(`already send packet! ${key}`);
                if (!denayDoubleSendCheck)
                    continue;
            }

            emailBody = crypto.encryptPGP(params);
            emailBody = utils.breakByBR(emailBody);
            emailBody = crypto.wrapPGPEnvelope(emailBody)

            // control output size
            if (emailBody.length > iConsts.MAX_BLOCK_LENGTH_BY_CHAR) {
                msg = `excedded max packet size for packet type(${sqType}) code(${sqCode})`;
                clog.app.error(msg);
                continue;
            }

            packets.push({
                connection_type: neighbor.nConnectionType,
                sqTitle,
                sqType,
                sqCode,
                sqSender: sender,
                sqReceiver: neighbor.nEmail,
                sqPyload: emailBody
            });
            let args2 = {
                lb_type: sqType,
                lb_code: sqCode,
                lb_title: sqTitle,
                lb_sender: sender,
                lb_receiver: neighbor.nEmail,
                lb_connection_type: neighbor.nConnectionType
            }
            // console.log(`args ;;;;;;;;;;;;;;;;;;;;;; ${utils.stringify(args2)}`);
            bdLogger.addSentBlock(args2);
        }
        return packets;


        //TODO after successfull sending must save some part the result and change the email to confirmed
    }


    static pushIntoSendingQ(args) {
        let msg;
        // iPGP content
        let packets = this.preparePacketsForNeighbors(args);
        clog.app.info(`prepare PacketsForNeighbors res packets ${utils.stringify(packets)}`);

        for (let packet of packets) {
            clog.app.info(`inserting in "i_sending_q" ${packet.sqType}-${packet.sqCode} for ${packet.sqReceiver} ${packet.sqTitle}`);

            let values = this.prepareTosendValues(packet)
            let dblChk = model.sRead({
                table,
                query: [
                    ['sq_type', packet.sqType],
                    ['sq_code', packet.sqCode],
                    ['sq_sender', packet.sqSender],
                    ['sq_receiver', packet.sqReceiver],
                ]
            })
            clog.app.info(`packet pushed to send(${dblChk.length}) from ${packet.sqSender} to ${packet.sqReceiver} ${packet.sqType}(${packet.sqCode})`);

            if (dblChk.length == 0) {
                model.sCreate({
                    table,
                    values: values,
                    log: false
                });

                if (iConsts.isDevelopMode()) {
                    let dblChk = model.sRead({
                        table: 'idev_sending_q',
                        query: [
                            ['sq_type', packet.sqType],
                            ['sq_code', packet.sqCode],
                            ['sq_sender', packet.sqSender],
                            ['sq_receiver', packet.sqReceiver],
                        ]
                    });

                    if (dblChk.length == 0)
                        model.sCreate({
                            table: 'idev_sending_q',
                            values: values,
                            log: false
                        });
                }

            }
        }
        return { err: false, shouldPurgeMessage: true }

    }

    static prepareTosendValues(pkt) {
        return {
            sq_type: pkt.sqType,
            sq_code: pkt.sqCode,
            sq_title: pkt.sqTitle,
            sq_sender: pkt.sqSender,
            sq_receiver: pkt.sqReceiver,
            sq_connection_type: _.has(pkt, 'sqConnectionType') ? pkt.sqConnectionType : iConsts.CONSTS.PUBLIC,
            sq_payload: pkt.sqPyload,
            sq_send_attempts: 0,
            sq_creation_date: utils.getNow(),
            sq_last_modified: utils.getNow(),
        }
    }


    static rmoveFromSendingQ(args = {}) {
        model.sDelete({
            table,
            query: args.query
        });
    }


    static sendOutThePacket(args = {}) {
        let res = {}
        let pkt = this.fetchFromSendingQSync(args)
        if (pkt.length == 0) {
            res.err = false;
            res.msg = 'No packet in q to Send';
            return res;
        }

        // always pick the first pkt! TODO: maybe more intelligent solution needed
        pkt = pkt[0];
        let packet = {
            sender: pkt.sq_sender,
            target: pkt.sq_receiver,
            title: pkt.sq_title,
            message: pkt.sq_payload
        }
        let sendRes = networker.iPushSync(packet);

        // remove packet from to-send queue
        this.rmoveFromSendingQ({
            query: [
                ['sq_type', pkt.sq_type],
                ['sq_code', pkt.sq_code],
                ['sq_sender', pkt.sq_sender],
                ['sq_receiver', pkt.sq_receiver]
            ]
        });
    }

    static prepareSendingQuery(args = {}) {
        args.order = [
            ['sq_connection_type', 'DESC'],
            ['sq_send_attempts', 'ASC'],
            ['sq_creation_date', 'ASC']
        ]
        let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args)
        let _query = 'SELECT ' + _fields + ' FROM i_sending_q ' + _clauses + _order + _limit;
        return { _query, _values }
    }

    static async fetchFromSendingQAsync(args = {}) {

        let { _query, _values } = this.prepareSendingQuery(args)
        let block = await model.aCustom({
            query: _query,
            values: _values,
            log: args.log
        })
        return block
    }

    static fetchFromSendingQSync(args = {}) {
        let { _query, _values } = this.prepareSendingQuery(args)
        let pckt = model.sCustom({
            query: _query,
            values: _values
        });
        return pckt;
    }


    static recursivePullSendingQ() {
        setTimeout(SendingQHandler.recursivePullSendingQ, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getSendingQGap());
        return SendingQHandler.sendOutThePacket();
    }

    static launchPullSendingQ() {
        // some preparations ...
        this.recursivePullSendingQ();
    }


}
module.exports = SendingQHandler;
