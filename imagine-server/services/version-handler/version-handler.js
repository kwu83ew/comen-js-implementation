const _ = require('lodash');
const iConsts = require('../../config/constants');
const machine = require('../../machine/machine-handler');
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');



class VersionHandler {

    static convertVerToVal(version) {
        let versionSegments = version.split('.').map(x => parseInt(x));


        if ((versionSegments[0] < 0) || (versionSegments[1] < 0) || (versionSegments[2] < 0)) {
            utils.exiter(`invalid Version ${utils.stringify(versionSegments)}`, 409)
        }

        let versionSegments2 = version.split('.');
        if ((versionSegments[0].toString() != versionSegments2[0]) || (versionSegments[1].toString() != versionSegments2[1]) || (versionSegments[2].toString() != versionSegments2[2])) {
            console.log(`invalid Version `, versionSegments);
            utils.exiter(`invalid Version ${utils.stringify(versionSegments)}`, 423);
        }


        let versionValue = 1000000 * versionSegments[0] + 1000 * versionSegments[1] + versionSegments[2];
        return versionValue;
    }

    static isEqual(v1, vRef) {
        let v1Value = this.convertVerToVal(v1);
        let vRefValue = this.convertVerToVal(vRef);
        return (v1Value == vRefValue);
    }

    static isNewerThan(v1, vRef) {
        let v1Value = this.convertVerToVal(v1);
        let vRefValue = this.convertVerToVal(vRef);
        return (v1Value > vRefValue);
    }

    static isNewerOrEqualThan(v1, vRef) {
        let v1Value = this.convertVerToVal(v1);
        let vRefValue = this.convertVerToVal(vRef);
        return (v1Value >= vRefValue);
    }

    static isOlderThan(v1, vRef) {
        let v1Value = this.convertVerToVal(v1);
        let vRefValue = this.convertVerToVal(vRef);
        return (v1Value < vRefValue);
    }

}

module.exports = VersionHandler;