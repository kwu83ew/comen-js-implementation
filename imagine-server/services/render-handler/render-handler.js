const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');

const acceptableExtensions = [
    'png', 'gif', 'jpg', 'jpeg', 'jp2', 'webp', 'ppt', 'pdf', 'psd',
    'mp3', 'xls', 'xlsx', 'swf', 'doc', 'docx', 'odt', 'odc', 'odp',
    'odg', 'mpp',
    'zip', 'tar', 'gz'
];
const imageExtensions = ['jpg', 'jpeg', 'png', 'webp', 'gif', 'svg', 'tiff'];

class RenderHandler {

    static renderToHTML(content) {
        content = RenderHandler.renderLinks(content);
        content = RenderHandler.renderEndLines(content);
        return content;
    }

    static renderLinks(content) {
        if (utils._nilEmptyFalse(content))
            return '_nilEmptyFalse content!';

        let links = RenderHandler.extractLinks(content);
        // console.log('------------>---- content', content);
        // console.log('------------>---- links', links);
        if (utils._nilEmptyFalse(links))
            return content;

        if (Array.isArray(links) && (links.length > 0))
            for (let aLink of links) {
                let newLink = RenderHandler.renderLink(aLink);
                content = content.replace(aLink, newLink);
            }
        return content;
    }

    static extractLinks(inp) {
        let exp = /\[\[[^\]]*\]\]/gi;
        let regex = new RegExp(exp);
        let links = inp.match(regex);
        return links;
    }

    static renderLink(link) {
        link = link.replace('[[', '');
        link = link.replace(']]', '');
        let segments = link.split('|');
        let output = '';
        if (segments[0].startsWith('File')) {
            output = RenderHandler.renderFile(segments);
        } else {
            output = `<a href="${segments[0]}">`;
            if (segments.length > 1) {
                output += segments[1];
            } else {
                output += segments[0];
            }
            output += '</a>';

        }
        return output;
    }

    static renderFile(segments) {
        let segZero = segments.shift().substr(5); // remove "File:" prefix
        let caption = '';
        if (segments.length > 0)
            caption = segments.pop();
        caption = utils.sanitizingContent(caption);
        // console.log('caption', caption);
        let segZeroDtl = segZero.split('.');
        let extension = segZeroDtl[segZeroDtl.length - 1];
        let out = '';
        if (!acceptableExtensions.includes(extension))
            return `"Invalid extension(${utils.sanitizingContent(extension)}) for file: ${caption}"`;

        if (imageExtensions.includes(extension)) {
            // egZero segZero  cache-files/cache/142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg
            let fileFinalPath;
            if (segZero.includes('/')) {
                fileFinalPath = segZero;
                // probably is an external link
            } else {
                let iCache = iutils.getICachePath();
                fileFinalPath = `${iCache}/${segZero}`;
            }
            out = `<img src="${fileFinalPath}"`;
        }
        if (utils._nilEmptyFalse(caption)) {
            out += '>';
        } else {
            out += ` title="${caption}" `;
            // console.log('remained segments', segments);
            if (segments.length > 0) {
                let imgAttr = RenderHandler.renderImgAttrs(segments);
                out += imgAttr.attrsStr;
                out += '>';
                if (imgAttr.wrapper)
                    out = imgAttr.wrapper.start + out + imgAttr.wrapper.end;
            } else {
                out += '>';
            }
        }
        return out;
    }

    static renderImgAttrs(segments) {
        let response = {
            wrapper: null,
            attrsStr: ''
        };
        for (let aSeg of segments) {
            let segDtl = aSeg.split('=');
            switch (segDtl[0]) {
                case 'link':
                    response.wrapper = {
                        start: `<a href="${segDtl[1]}">`,
                        end: '</a>',
                    };
                    break;

                case 'alt':
                    response.attrsStr += ` alt="${utils.sanitizingContent(segDtl[1])}" `;
                    break;

            }
        }

        return response;
    }

    static renderEndLines(content){
        content = content.replace(/\n\n/g, "<br />");
        content = content.replace(/\n\r/g, "<br />");
        content = content.replace(/\r\r/g, "<br />");
        content = content.replace(/\r\n/g, "<br />");
        content = content.replace(/\n/g, "<br />");
        content = content.replace(/\r/g, "<br />");
        return content;
    }
}

module.exports = RenderHandler;

// var linkS = extractLinks('this is image[[File:earth.jpg|border|frame|thumb|left|20px|alt=the beautiful earth|myCustomizedCaption]]');
// console.log(linkS);
// var out = renderLink(linkS[0]);
// console.log(out);
// var linkS = extractLinks('this is image[[File:earth.jpg|border|frame|thumb|left|20px|link=mysite/home/earth|alt=the beautiful earth|myCustomizedCaption]]');
// console.log(linkS);
// var out = renderLink(linkS[0]);
// console.log(out);

