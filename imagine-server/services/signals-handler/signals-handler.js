const _ = require('lodash');
const iConsts = require('../../config/constants');
const machine = require('../../machine/machine-handler');
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');


const table = 'i_signals';

class SignalsHandler {

    static handleBlockSignals(args) {
        let backerAddress = args.backerAddress;
        let blockHash = args.blockHash;
        let creationDate = args.creationDate;
        let signals = args.signals;

        if (Array.isArray(signals))
            for (let aSignal of signals) {
                model.sCreate({
                    table,
                    values: {
                        sig_signaler: backerAddress,
                        sig_block_hash: blockHash,
                        sig_key: aSignal.sig,
                        sig_value: utils.stringify(aSignal),
                        sig_creation_date: creationDate,
                    }
                });
            }
    }

    static isClientSoftwareUptodate() {
        let report = SignalsHandler.getClientVersionSignalsReport();
        let latestVersion = report.latestVersion;
        let res = {
            isUpdate: (latestVersion <= iConsts.CLIENT_VERSION),
            machineVersion: iConsts.CLIENT_VERSION,
            latestVersion
        };
        return res;
    }

    static getClientVersionSignalsReport() {
        let records = model.sRead({
            table,
            query: [['sig_key', 'clientVersion']]
        });
        let groupedByVersion = {};
        // intial value can be machine settings, even if not created any block
        groupedByVersion[iConsts.CLIENT_VERSION] = [];
        groupedByVersion[iConsts.CLIENT_VERSION].pus({
            aRec: {
                sig_signaler: '',
                sig_block_hash: '',
                sig_key: 'sig_key',
                sig_value: '',
                sig_creation_date: ''
            },
            sigValue: {
                sig: 'clientVersion',
                ver: iConsts.CLIENT_VERSION,
            }
        });

        for (let aRec of records) {
            let sigValue = utils.parse(aRec.sisigV_value);
            let ver = sigValue.ver;
            if (!_.has(groupedByVersion, ver))
                groupedByVersion[ver] = []
            groupedByVersion[ver].push({ aRec, sigValue });
        }
        // TODO: implemet some detailed report e.g. percentage of versions & share percentage ...

        let report = {
            groupedByVersion,
            versions: utils.objKeys(groupedByVersion),
        };
        report['latestVersion'] = report.versions.sort().reverse()[0];
        return report;
    }

}

module.exports = SignalsHandler;