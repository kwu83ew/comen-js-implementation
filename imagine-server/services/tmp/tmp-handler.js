const _ = require('lodash');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const getMoment = require('../../startup/singleton').instance.getMoment
const listener = require('../../plugin-handler/plugin-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');


const table = 'i_machine_tmp_documents';

class docTmpHandler {

    static unBundleAndSignPLR(tdId) {
        let mpCode = iutils.getSelectedMProfile();
        let msg;
        let bundle = this.searchTmpDocsSync({
            query: [
                ['td_mp_code', mpCode],
                ['td_id', tdId],
                ['td_doc_status', 'new'],
            ]
        });
        clog.app.info(`going to sign bundle ${utils.stringify(bundle)}`);
        if (bundle.length != 1) {
            msg = `bundle ${tdId} is not new`;
            console.error(msg);
            return { err: true, msg };
        }
        bundle = bundle[0];

        // console.log(bundle);
        let args = utils.parse(bundle.tdPayload);
        const pPledgeHandler = require('../../contracts/pledge-contract/proposal-pledge-contract');
        let signRes = pPledgeHandler.pledgeeSignsProposalLoanRequestBundle(args);
        if (signRes.err != false)
            return signRes;

        // update bundle status
        model.sUpdate({
            table,
            query: [
                ['td_id', tdId]
            ],
            updates: { td_doc_status: 'signed' }
        });


        return { err: false, msg: 'Done' };
    }

    static unBundleAndBroadcastPPT(tdId) {
        let mpCode = iutils.getSelectedMProfile();
        let msg;
        console.log('unBundleAndBroadcastPPT tdId:', tdId);

        let bundle = this.searchTmpDocsSync({
            query: [
                ['td_mp_code', mpCode],
                ['td_id', tdId]
            ]
        });
        console.log('bundle', bundle);
        if (bundle.length != 1) {
            msg = `bundle ${tdId} does not exist`;
            console.error(msg);
            return { err: true, msg };
        }
        bundle = bundle[0];
        if (bundle.tdDocStatus != 'new') {
            msg = `bundle already sent`;
            console.error(msg);
            return { err: true, msg };
        }


        let body = utils.parse(bundle.tdPayload);
        let proposal = body.proposal;
        let pledge = body.pledge;
        let ProposalPayerTrx = body.ProposalPayerTrx;
        let pledgeDocPayerTrx = body.pledgeDocPayerTrx;

        // console.log(' ************* ************* ************* *************');
        // console.log('proposal', proposal);
        // console.log(' ************* ************* ************* *************');
        // console.log('pledge', pledge);
        // console.log(' ************* ************* ************* *************');
        // console.log('ProposalPayerTrx', ProposalPayerTrx);
        // console.log(' ************* ************* ************* *************');

        // push proposal to Block buffer
        docBufferHandler.insertBufDocSync({ doc: proposal, DPCost: pledge.redeemTerms.principal });

        // push pledge to Block buffer
        docBufferHandler.insertBufDocSync({ doc: pledge, DPCost: 1 }); // TODO: extract DPCost from ProposalPayerTrx outputs

        // push ProposalPayerTrx to Block buffer
        docBufferHandler.insertBufDocSync({
            doc: ProposalPayerTrx,
            DPCost: ProposalPayerTrx.outputs[ProposalPayerTrx.dPIs[0]][1]
        }); // TODO: extract DPCost from ProposalPayerTrx outputs

        // push ProposalPayerTrx to Block buffer
        docBufferHandler.insertBufDocSync({
            doc: pledgeDocPayerTrx,
            DPCost: pledgeDocPayerTrx.outputs[pledgeDocPayerTrx.dPIs[0]][1]
        }); // TODO: extract DPCost from pledgeDocPayerTrx outputs

        // update bundle status
        model.sUpdate({
            table,
            query: [
                ['td_id', tdId]
            ],
            updates: { td_doc_status: 'pushedToDocBuffer' }
        });
        return { msg: 'Done' };
    }

    static async pushInAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        listener.doCallAsync('APSH_before_push_doc_to_tmp_async', args);

        let dblChk = await this.searchAsync({
            query: [
                ['td_mp_code', mpCode],
                ['td_doc_hash', args.doc.hash]
            ]
        });
        if (dblChk.length > 0) {
            clog.sec.error(`tried to insert in tmp duplicated document ${args.doc.hash} `);
            return;
        }

        let payload = JSON.stringify(args.doc);
        let res = await model.aCreate({
            table,
            values: {
                td_mp_code: mpCode,
                td_insert_date: utils.getNow(),
                td_doc_hash: args.doc.hash,
                td_doc_type: args.doc.dType, // iutils.mapDocCodeToDocType(args.doc.dType),
                td_doc_class: args.doc.dClass, // iutils.mapDocCodeToDocType(args.doc.dType),
                td_payload: payload,
                td_dp_cost: JSON.stringify(args.DPCost),
                td_doc_len: payload.length,
            }
        });
        listener.doCallAsync('APSH_after_push_doc_to_tmp_async', args);
        return res;
    }

    static insertTmpDocSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        listener.doCallAsync('APSH_before_push_doc_to_tmp_sync', args);

        let dblChk = this.searchTmpDocsSync({
            query: [
                ['td_mp_code', mpCode],
                ['td_doc_hash', args.doc.hash]
            ]
        });
        if (dblChk.length > 0) {
            clog.sec.error(`tried to insert in tmp duplicated trx ${args.doc.hash} `);
            return;
        }

        let payload = utils.stringify(args.doc);
        let res = model.sCreate({
            table,
            values: {
                td_mp_code: mpCode,
                td_insert_date: utils.getNow(),
                td_doc_status: args.doc_status,
                td_doc_hash: args.doc.hash,
                td_doc_type: args.doc.dType, // iutils.mapDocCodeToDocType(args.doc.dType),
                td_doc_class: args.doc.dClass, // iutils.mapDocCodeToDocType(args.doc.dType),
                td_payload: payload
            }
        });
        listener.doCallAsync('APSH_after_push_doc_to_tmp_sync', args);
        return res;
    }

    static async removeFromTmpAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'td_mp_code'))
            query.push(['td_mp_code', mpCode]);

        let hashes = _.has(args, 'hashes') ? args.hashes : null;

        if (!utils._nilEmptyFalse(hashes))
            query.push(['td_doc_hash', ['IN', hashes]]);

        return await model.aDelete({
            table,
            query: query
        })
    }

    static async searchTmpDocsAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'td_mp_code'))
            query.push(['td_mp_code', mpCode]);
        args.query = query;

        args.table = table;
        let res = await model.aRead(args);
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFields(x));
        return res;
    }

    static searchTmpDocsSync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'td_mp_code'))
            query.push(['td_mp_code', mpCode]);
        args.query = query;

        console.log('search TmpDocsSync args', args);
        args.table = table;
        let res = model.sRead(args);
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFields(x));
        return res;
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'td_id'))
            out.tdId = elm.td_id;
        if (_.has(elm, 'td_insert_date'))
            out.tdInsertDate = elm.td_insert_date;
        if (_.has(elm, 'td_doc_status'))
            out.tdDocStatus = elm.td_doc_status;
        if (_.has(elm, 'td_doc_hash'))
            out.tdDocHash = elm.td_doc_hash;
        if (_.has(elm, 'td_doc_type'))
            out.tdDocType = elm.td_doc_type;
        if (_.has(elm, 'td_doc_class'))
            out.tdDocClass = elm.td_doc_class;
        if (_.has(elm, 'td_payload'))
            out.tdPayload = elm.td_payload;
        return out;
    }

}

module.exports = docTmpHandler;