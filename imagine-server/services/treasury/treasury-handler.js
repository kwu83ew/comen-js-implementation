const _ = require('lodash');
const iConsts = require('../../config/constants');
const model = require("../../models/interface");
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const clog = require('../../loggers/console_logger');
const getMoment = require("../../startup/singleton").instance.getMoment;
const Moment = require("../../startup/singleton").instance.Moment;
const dagHandler = require("../../dag/graph-handler/dag-handler");
const walletHandler = require("../../web-interface/wallet/wallet-handler");
const walletAddressHandler = require("../../web-interface/wallet/wallet-address-handler");
const machine = require("../../machine/machine-handler");
const trxHashHandler = require("../../transaction/hashable");

const table = 'i_treasury';

class TreasuryHandler {

    static donateTransactionInput(args) {

        // retrieve location refLoc is generated
        // let blocks = dagHandler.retrieveBlocksInWhichARefLocHaveBeenProduced(args.refLoc);
        // clog.trx.info(`donate Transaction Input. blocks by refLoc: ${JSON.stringify(blocks)}`);
        // let block = blocks[0];

        // big FIXME: for cloning transactions issue
        this.insertIncome({
            title: _.has(args, 'title') ? args.title : 'No Title!',
            cat: _.has(args, 'cat') ? args.cat : 'No Cat!',
            descriptions: args.descriptions,
            creationDate: args.creationDate,
            blockHash: args.blockHash,
            refLoc: args.refLoc,
            value: args.value
        });
    }

    static insertIncome(args) {
        let dbl = model.sRead({
            table,
            query: [['tr_ref_loc', args.refLoc]]
        })
        if (dbl.length > 0) {
            clog.trx.info(`duplicated treasury insertion: ${utils.stringify(args)}`);
            // update the descriptions
            model.sUpdate({
                table,
                query: [
                    ['tr_ref_loc', args.refLoc]
                ],
                updates: { tr_descriptions: dbl[0].tr_descriptions + ' ' + args.descriptions }
            })
            return;
        }
        clog.trx.info(`Treasury income: ${utils.microPAIToPAI6(args.value)} PAIs because of block(${utils.hash6c(args.blockHash)})`);

        model.sCreate({
            table,
            values: {
                tr_cat: args.cat,
                tr_title: args.title,
                tr_descriptions: args.descriptions,
                tr_creation_date: args.creationDate,
                tr_block_hash: args.blockHash,
                tr_ref_loc: args.refLoc,
                tr_value: args.value
            }
        });
    }

    static getWaitedIncomes(cDate = null) {
        cDate = this.getTreasureIncomesDateRange(cDate).maxCreationDate;
        let res = model.sRead({
            table,
            query: [
                ['tr_creation_date', ['>', cDate]]
            ]
        });
        let sum = 0;
        for (let income of res) {
            sum += iutils.convertBigIntToJSInt(income.tr_value)
        }
        return { sum };
    }

    static getTreasureIncomesDateRange(cDate) {
        return iutils.getACycleRange({
            cDate,
            backByCycle: iConsts.TREASURY_MATURATION_CYCLES
        });
    }


    static calcTreasuryIncomesSync(cDate = null) {
        if (utils._nilEmptyFalse(cDate))
            cDate = getMoment().format('YYYY-MM-DD HH:mm:ss');

        // retrieve the total treasury incomes in last cycle (same as share cycle calculation)
        let { minCreationDate, maxCreationDate } = this.getTreasureIncomesDateRange(cDate)

        let incomes = model.sCustom({
            query: 'SELECT SUM(tr_value) _incomes FROM i_treasury WHERE tr_creation_date between $1 AND $2',
            values: [minCreationDate, maxCreationDate]
        })
        clog.cb.info(`calc Treasury Incomes WHERE creation_date between (${minCreationDate}, ${maxCreationDate}) -> incomes(${utils.stringify(incomes)})`)
        if (utils._nilEmptyFalse(incomes[0]['_incomes'])) {
            incomes = 0;
        } else {
            incomes = iutils.convertBigIntToJSInt(incomes[0]['_incomes'])
        }
        return {
            fromDate: minCreationDate,
            toDate: maxCreationDate,
            incomes: incomes
        };
    }

    static createDPCostPaymentTrx(args) {
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let treasury = _.has(args, 'treasury') ? args.treasury : 0;
        let backerNetFee = _.has(args, 'backerNetFee') ? args.backerNetFee : 0;
        let trx = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.DPCostPay,
            dClass: "",
            dVer: "0.0.0",
            description: `Data & Process Cost Payment`,
            creationDate,
            outputs: [
                ['TP_DP', treasury],
                [machine.getMProfileSettingsSync().backerAddress, backerNetFee]
            ]
        };
        trx.hash = trxHashHandler.doHashTransaction(trx);
        clog.trx.info(`Data & Process Cost Payment transaction: ${JSON.stringify(trx)}`);
        return trx;
    }
}

module.exports = TreasuryHandler;
