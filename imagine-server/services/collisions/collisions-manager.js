const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const DNAHandler = require('../../dna/dna-handler');

const table = 'i_collisions';

/**
 * in general, because of the nature of distributed systems it suppposed to receive a piece of information to different nodes in different times.
 * and in cases in which the network have to know what package is arrived first? we need to accomulated entire networks opinion, and decide.
 * this module manages these stuffs
 * 
 */


class CollisionsHandler {


    static hasCollision(args) {
        clog.app.info(`has Collision args: ${utils.stringify(args)}`);
        console.log(`has Collision args: ${utils.stringify(args)}`);
        let clCollisionRef = args.clCollisionRef;
        let res = model.sRead({
            fields: ['cl_voter', 'cl_collision_ref', 'cl_block_hash', 'cl_doc_hash', 'cl_creation_date', 'cl_receive_date'],
            table,
            query: [
                ['cl_collision_ref', clCollisionRef]
            ],
            order: [['cl_receive_date', 'ASC']]
        });
        if (res.length == 0)
            return { hasCollision: false };

        res = res.map(x => CollisionsHandler.convertFields(x));
        return { hasCollision: true, records: res };
    }

    static getLoggedDocs(docHash) {
        let records = model.sRead({
            table,
            query: [
                ['cl_doc_hash', docHash]
            ]
        });
        return records;
    }

    static makeCollisionReport(args = {}) {
        // TODO: probably need to improve to more robust (e.g. like transaction doublespend solution)
        // do it ASAP
        let clCollisionRef = args.clCollisionRef;
        let records = CollisionsHandler.searchInCollisionLogs({
            query: [['cl_collision_ref', clCollisionRef]],
            order: [['cl_receive_date', 'DESC']]    // it is DESC to fore over-write early received on later arrived blocks
        });

        let votesDict = {}; // a dummy dictionary to make uniq voter uniq docHash results
        for (let aVote of records) {
            if (!_.has(votesDict, aVote.cl_voter))
                votesDict[aVote.cl_voter] = {
                    // cOrders: []     // collision receiving orders
                }


            let shares = DNAHandler.getAnAddressShares(aVote.cl_voter, aVote.cl_receive_date).percentage; // dna handler to calculate
            if (shares == 0) {
                clog.trx.info(`coll shares == 0 for (${iutils.shortBech(aVote.cl_voter)}): ${shares}`);
                shares = utils.iFloorFloat(iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);
                clog.trx.info(`coll shares == 0 for (${iutils.shortBech(aVote.cl_voter)}): ${shares}`);
            }

            votesDict[aVote.cl_voter][aVote.cl_doc_hash] = {
                docHash: aVote.cl_doc_hash,
                receiveDate: aVote.cl_receive_date,
                shares
            }
        }
        clog.app.info(`make CollisionReport votesDict: ${utils.stringify(votesDict)}`);

        let unifiedRecordsForAVoter = {};
        for (let aVoter of utils.objKeys(votesDict)) {

            let aVoterVotes = {}
            for (let aDocHash of utils.objKeys(votesDict[aVoter])) {
                let aRecord = votesDict[aVoter][aDocHash];
                let orderKey = [aRecord.receiveDate, aDocHash].join();
                aVoterVotes[orderKey] = aRecord;
            }
            clog.app.info(`aVoterVotes aVoterVotes: ${utils.stringify(aVoterVotes)}`);
            if (utils.objKeys(aVoterVotes).length > 0) {
                let firstDocForVoter = utils.objKeys(aVoterVotes).sort()[0];
                unifiedRecordsForAVoter[aVoter] = aVoterVotes[firstDocForVoter];
            }
        }
        clog.app.info(`make CollisionReport unifiedRecordsForAVoter: ${utils.stringify(unifiedRecordsForAVoter)}`);

        // score dict
        let scoresDict = {}
        for (let aVoter of utils.objKeys(unifiedRecordsForAVoter)) {
            if (!_.has(scoresDict, unifiedRecordsForAVoter[aVoter].docHash))
                scoresDict[unifiedRecordsForAVoter[aVoter].docHash] = 0
            scoresDict[unifiedRecordsForAVoter[aVoter].docHash] += unifiedRecordsForAVoter[aVoter].shares;
        }
        clog.app.info(`collision scoresDict: ${utils.stringify(scoresDict)}`);

        // find winer
        let winer = { score: 0 };
        for (let aDocHash of utils.objKeys(scoresDict)) {
            if (scoresDict[aDocHash] > winer.score) {
                winer.aDocHash = aDocHash;
                winer.score = scoresDict[aDocHash];
            }
        }
        clog.app.info(`collision winer: ${utils.stringify(winer)}`);
        return winer;
    }

    static searchInCollisionLogs(args = {}) {
        // let query = _.has(args, 'query') ? args.query : [];
        args.table = table;
        let records = model.sRead(args);
        // no conversion. records = records.map(x => CollisionsHandler.convertFields(x));
        return records;
    }

    static logACollision(args) {
        model.sCreate({
            table,
            values: {
                cl_voter: args.clVoter,
                cl_collision_ref: args.clCollisionRef,
                cl_block_hash: args.clBlockHash,
                cl_doc_hash: args.clDocHash,
                cl_creation_date: args.clCreationDate,
                cl_receive_date: args.clReceiveDate
            },
        });
    }

    static handleReceivedFloatingVote(args) {
        let msg;
        clog.app.info(`handle Received Floating Vote args ${utils.stringify(args.block.voteData)}`);
        let voteData = args.block.voteData;
        for (let aVote of voteData) {
            // control if already exist!
            let exists = CollisionsHandler.searchInCollisionLogs({
                query: [
                    ['cl_voter', aVote.clVoter],
                    ['cl_collision_ref', aVote.clCollisionRef],
                    ['cl_block_hash', aVote.clBlockHash],
                    ['cl_doc_hash', aVote.clDocHash]
                ]
            });
            if (exists.length > 0) {
                msg = `the collision alreay logged ref(${utils.hash6c(aVote.clCollisionRef)}) for doc(${utils.hash6c(aVote.clDocHash)}) ${utils.stringify(aVote)}`;
                clog.app.error(msg);
                return { err: false, msg };
            }

            // insert the collision log
            CollisionsHandler.logACollision({
                clVoter: aVote.clVoter,
                clCollisionRef: aVote.clCollisionRef,
                clBlockHash: aVote.clBlockHash,
                clDocHash: aVote.clDocHash,
                clCreationDate: aVote.clCreationDate,
                clReceiveDate: aVote.clReceiveDate
            })
        }
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'cl_id'))
            out.clId = elm.cl_id;
        if (_.has(elm, 'cl_voter'))
            out.clVoter = elm.cl_voter;
        if (_.has(elm, 'cl_collision_ref'))
            out.clCollisionRef = elm.cl_collision_ref;
        if (_.has(elm, 'cl_block_hash'))
            out.clBlockHash = elm.cl_block_hash;
        if (_.has(elm, 'cl_doc_hash'))
            out.clDocHash = elm.cl_doc_hash;
        if (_.has(elm, 'cl_creation_date'))
            out.clCreationDate = elm.cl_creation_date;
        if (_.has(elm, 'cl_receive_date'))
            out.clReceiveDate = elm.cl_receive_date;
        return out;

    }


}

module.exports = CollisionsHandler;
