const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const walletAddressHandler = require('../../../web-interface/wallet/wallet-address-handler');
const machine = require('../../../machine/machine-handler');
const blockUtils = require('../../../dag/block-utils');


const tableWikiPages = 'i_wiki_pages';
const tableWikiPageContent = 'i_wiki_contents';

class PagesHandler {

    static prepareNewWikiPageRegReq(args) {
        let msg;
        console.log(`prepare A new wiki Document args`, args);
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let wkCreatorAccount = _.has(args, 'wkCreatorAccount') ? args.wkCreatorAccount : null;
        let wkinHash = _.has(args, 'wkinHash') ? args.wkinHash : iutils.convertTitleToHash('imagine');   // the domain name of wiki
        let mpSelectedLang = _.has(args, 'mpSelectedLang') ? args.mpSelectedLang : iConsts.DEFAULT_LANG;
        /**
         * wkTitle is the name of page e.g. imagine/wiki/fruits or imagine/wiki/apple 
         * NOTE: wiki does not support more than one layer slashes e.g. imagine/wiki/fruits/apple is not a valid wiki URI
         */
        let wkUniqueHash = _.has(args, 'wkUniqueHash') ? args.wkUniqueHash : '';
        let wkTitle = _.has(args, 'wkTitle') ? args.wkTitle : '';
        let wkContent = _.has(args, 'wkContent') ? args.wkContent : ''; // the page content, formatted as mediawiki standards

        // sanWkTitle = utils.encode_utf8(wkTitle);
        let sanWkTitle = utils.encode_utf8(utils.sanitizingContent(wkTitle));
        let wkTitleHash = iutils.convertTitleToHash(sanWkTitle);
        let compressWkTitle = blockUtils.compressString({ content: wkTitle });

        let compressWkContent = blockUtils.compressString({ content: wkContent });

        if (utils._nilEmptyFalse(wkinHash)) {
            msg = `Invalid/Empty wiki URI Address!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        if (!utils._nilEmptyFalse(wkCreatorAccount) && !crypto.bech32_isValidAddress(wkCreatorAccount)) {
            msg = `Invalid wiki signer(${iutils.shortBech8(wkCreatorAccount)})!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        let uniqHashHandle = crypto.keccak256(`${wkinHash}/${wkTitleHash}`);

        let dClass;
        if (utils._nilEmptyFalse(wkUniqueHash)) {
            dClass = iConsts.CPOST_CLASSES.WK_CreatePage;
            let exist = model.sRead({
                table: tableWikiPages,
                query: [
                    ['wkp_unique_hash', uniqHashHandle],
                ]
            });
            if (exist.length > 0)
                return { err: true, msg: `The title(${sanWkTitle}) for desired address(${utils.hash6c(wkinHash)}) wiki is already registerd!` }
        } else {
            dClass = iConsts.CPOST_CLASSES.WK_EditPage;
            if (uniqHashHandle != wkUniqueHash) {
                msg = `desired wiki to update has not valid uniq hash, respect it's title & iName!`;
                clog.sec.error(msg);
                return { err: true, msg }
            }

        }

        if (utils._nilEmptyFalse(wkCreatorAccount)) {
            wkCreatorAccount = machine.getBackerAddress();
        }
        let fDoc = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.CPost,
            dClass,
            cLang: mpSelectedLang,
            dLen: "0000000",
            dVer: iConsts.DEFAULT_VERSION,
            fVer: iConsts.DEFAULT_VERSION,
            creationDate: creationDate,
            signer: wkCreatorAccount,
            title: compressWkTitle,
            content: compressWkContent,
            inHash: wkinHash,
            dExtInfo: "",
            dExtHash: "",
        }
        let signMsg = PagesHandler.getSignMsgDRegWikiPage(fDoc);
        let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: wkCreatorAccount });
        if (signatureInfo.err != false)
            return signatureInfo;
        fDoc.dExtInfo = {
            signatures: signatureInfo.signatures,
            uSet: signatureInfo.uSet
        }
        fDoc.dExtHash = PagesHandler.calcDExtHashWikiPageRegReq(fDoc.dExtInfo);

        fDoc.dLen = iutils.paddingDocLength(utils.stringify(fDoc).length);
        let custHashRes = PagesHandler.calcHashDWikiRegReq({ fDoc });
        if (custHashRes.err != false)
            return custHashRes;
        fDoc.hash = custHashRes.hash;

        return { err: false, fDoc }
    }

    static calcDExtHashWikiPageRegReq(dExtInfo) {
        return iutils.doHashObject({
            signatures: dExtInfo.signatures,
            uSet: dExtInfo.uSet
        });
    }

    static getSignMsgDRegWikiPage(fDoc) {
        // as always ordering properties by alphabet
        let signables = {
            content: fDoc.content,
            cLang: fDoc.cLang,
            creationDate: fDoc.creationDate,
            dClass: fDoc.dClass,
            dType: fDoc.dType,
            dVer: fDoc.dVer,
            fVer: fDoc.fVer,
            inHash: fDoc.inHash,
            title: fDoc.title
        }
        clog.app.info(`prepare reg req for Wiki signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static calcHashDWikiRegReq(args) {
        let fDoc = args.fDoc;
        // as always ordering properties by alphabet
        let hashables = {
            dExtHash: fDoc.dExtHash,
            dLen: fDoc.dLen,
            signer: fDoc.signer,
        }
        clog.app.info(`calc reg new wiki hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return { err: false, hash };
    }

    static searchInWikiPages(args = {}) {
        clog.app.info(`search In Wikis ${utils.stringify(args)}`);
        let msg;
        let needContentsToo = _.has(args, 'needContentsToo') ? args.needContentsToo : false;
        let query = _.has(args, 'query') ? args.query : [];

        let posts = model.sRead({
            table: tableWikiPages,
            query,
            order: [['wkp_title', 'ASC']]
        });
        if (!needContentsToo)
            return { err: false, records: posts };

        // retrieve contents
        let pgIds = posts.map(x => x.wkp_unique_hash);
        let contentsRes = PagesHandler.searchInWikiContents({
            query: [['wkc_unique_hash', ['IN', pgIds]]]
        });
        if (contentsRes.err != false) {
            clog.app.error(`contentsRes ${utils.stringify(contentsRes)}`);
            posts = posts.map(x => x.content = 'error in content reading!');
            return { err: false, records: posts };
        }
        let contentDict = {};
        for (let aContent of contentsRes.records) {
            contentDict[aContent.wkc_unique_hash] = aContent.wkc_content;
        }
        // add content to records
        for (let aPost of posts) {
            aPost.content = (_.has(contentDict, aPost.wkp_unique_hash)) ? contentDict[aPost.wkp_unique_hash] : 'The content not found!';
        }
        return { err: false, records: posts };
    }

    static searchInWikiContents(args = {}) {
        clog.app.info(`search In Wiki Contents ${utils.stringify(args)}`);
        console.log(`search In Wiki Contents ${utils.stringify(args)}`);
        let msg;
        let query = _.has(args, 'query') ? args.query : [];

        let contents = model.sRead({
            table: tableWikiPageContent,
            query
        });
        return { err: false, records: contents };
    }

    static validateWikiPage(args) {
        let msg;
        if (!iutils.isValidVersionNumber(args.fDoc.fVer)) {
            msg = `invalid formatting version in wiki page fDoc(${utils.hash6c(args.fDoc.hash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        // only signer validate!
        args.signMsg = PagesHandler.getSignMsgDRegWikiPage(args.fDoc);
        let vRes = PagesHandler._super._super.validatefDocSigner(args);
        if (vRes.err != false)
            return vRes;

        // validate safety wrape
        let titleValidateRes = blockUtils.decompressString(args.fDoc.title);
        if (titleValidateRes.err != false)
            return titleValidateRes;
        let contentValidateRes = blockUtils.decompressString(args.fDoc.content);
        if (contentValidateRes.err != false)
            return contentValidateRes;

        return { err: false }
    }

    static updateWkDoc(args) {
        let fDoc = args.fDoc;
        let block = args.block;
        let creationDate = block.creationDate;
        let decompTitle = blockUtils.decompressString(fDoc.title).content;
        let decompContent = blockUtils.decompressString(fDoc.content).content;
        clog.app.info(`updateWkDoc decompContent ${utils.stringify(decompContent)}`);

        return PagesHandler.updatePage({
            wkp_title: decompTitle,
            wkp_in_hash: fDoc.inHash,
            wkp_doc_hash: fDoc.hash,
            wkp_lang: fDoc.cLang,
            wkp_format_version: fDoc.fVer,
            wkp_last_modified: creationDate,
            wkp_creator: fDoc.signer,
            wkc_content: decompContent
        });
    }

    static insertRegDoc(args) {
        console.log(`insert Reg Doc args`, args);
        let fDoc = args.fDoc;
        let block = args.block;
        let creationDate = block.creationDate;
        let decompTitle = blockUtils.decompressString(fDoc.title).content;
        let decompContent = blockUtils.decompressString(fDoc.content).content;
        clog.app.info(`insertRegDoc decompContent ${utils.stringify(decompContent)}`);

        return PagesHandler.insertPage({
            wkp_title: decompTitle,
            wkp_in_hash: fDoc.inHash,
            wkp_doc_hash: fDoc.hash,
            wkp_lang: fDoc.cLang,
            wkp_format_version: fDoc.fVer,
            wkp_creation_date: creationDate,
            wkp_last_modified: creationDate,
            wkp_creator: fDoc.signer,
            wkc_content: decompContent
        });
    }

    static insertPage(args) {
        clog.app.info(`insert Wiki Page ${args.wkp_title}`);
        let msg;
        let wkp_title = _.has(args, 'wkp_title') ? args.wkp_title : '';
        let wkp_in_hash = _.has(args, 'wkp_in_hash') ? args.wkp_in_hash : '';
        let wkp_hash = _.has(args, 'wkp_hash') ? args.wkp_hash : '';
        let wkp_doc_hash = _.has(args, 'wkp_doc_hash') ? args.wkp_doc_hash : '';
        let wkp_unique_hash = _.has(args, 'wkp_unique_hash') ? args.wkp_unique_hash : '';
        let wkp_lang = _.has(args, 'wkp_lang') ? args.wkp_lang : iConsts.DEFAULT_LANG;
        let wkp_format_version = _.has(args, 'wkp_format_version') ? args.wkp_format_version : iConsts.DEFAULT_VERSION;
        let wkp_creation_date = _.has(args, 'wkp_creation_date') ? args.wkp_creation_date : utils.getNow();
        let wkp_last_modified = _.has(args, 'wkp_last_modified') ? args.wkp_last_modified : utils.getNow();
        let wkp_creator = _.has(args, 'wkp_creator') ? args.wkp_creator : '';
        let wkc_content = _.has(args, 'wkc_content') ? args.wkc_content : '';


        if (
            utils._nilEmptyFalse(wkp_title) ||
            utils._nilEmptyFalse(wkp_in_hash) ||
            utils._nilEmptyFalse(wkp_doc_hash) ||
            utils._nilEmptyFalse(wkp_creator) ||
            utils._nilEmptyFalse(wkc_content)
        ) {
            msg = `invalid wiki page info ${utils.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg }
        }


        wkp_title = utils.sanitizingContent(wkp_title);

        if (utils._nilEmptyFalse(wkp_hash)) {
            wkp_hash = iutils.convertTitleToHash(wkp_title);
        }
        if (utils._nilEmptyFalse(wkp_unique_hash)) {
            wkp_unique_hash = crypto.keccak256(`${wkp_in_hash}/${wkp_hash}`);
        }

        // check if exist newest version
        let exist = model.sRead({
            table: tableWikiPages,
            query: [
                ['wkp_unique_hash', wkp_unique_hash],
                ['wkp_last_modified', ['>=', wkp_last_modified]],
            ]
        });
        if (exist.length > 0)
            return { err: false, msg: `already a newer version of wikipage(${utils.hash6c(wkp_unique_hash)}) exist in db` };

        // check if same doc alredy recorded
        exist = model.sRead({
            table: tableWikiPages,
            query: [
                ['wkp_doc_hash', wkp_doc_hash],
            ]
        });
        if (exist.length > 0)
            return { err: false }

        let values = {
            wkp_title,
            wkp_in_hash,
            wkp_hash,
            wkp_doc_hash,
            wkp_unique_hash,
            wkp_lang,
            wkp_format_version,
            wkp_creation_date,
            wkp_last_modified,
            wkp_creator
        }
        // console.log(`inserting a wiki page: ${utils.stringify(values)}`);
        clog.app.info(`inserting a wiki page: ${utils.stringify(values)}`);

        model.sCreate({
            table: tableWikiPages,
            values
        });

        // insert contents too
        values = {
            wkc_unique_hash: wkp_unique_hash,
            wkc_content
        }
        // console.log(`inserting a wiki page: ${utils.stringify(values)}`);
        clog.app.info(`inserting a wiki page: ${utils.stringify(values)}`);

        model.sCreate({
            table: tableWikiPageContent,
            values
        });

    }

    static updatePage(args) {
        clog.app.info(`update Wiki Page ${utils.stringify(args)}`);
        console.log(`update Wiki Page ${utils.stringify(args)}`);
        let msg;
        let wkp_title = _.has(args, 'wkp_title') ? args.wkp_title : '';
        let wkp_in_hash = _.has(args, 'wkp_in_hash') ? args.wkp_in_hash : '';
        let wkp_hash = _.has(args, 'wkp_hash') ? args.wkp_hash : '';
        let wkp_doc_hash = _.has(args, 'wkp_doc_hash') ? args.wkp_doc_hash : '';
        let wkp_unique_hash = _.has(args, 'wkp_unique_hash') ? args.wkp_unique_hash : '';
        let wkp_lang = _.has(args, 'wkp_lang') ? args.wkp_lang : iConsts.DEFAULT_LANG;
        let wkp_format_version = _.has(args, 'wkp_format_version') ? args.wkp_format_version : iConsts.DEFAULT_VERSION;
        let wkp_last_modified = _.has(args, 'wkp_last_modified') ? args.wkp_last_modified : utils.getNow();
        let wkp_creator = _.has(args, 'wkp_creator') ? args.wkp_creator : '';
        let wkc_content = _.has(args, 'wkc_content') ? args.wkc_content : '';

        if (
            utils._nilEmptyFalse(wkp_title) ||
            utils._nilEmptyFalse(wkp_in_hash) ||
            utils._nilEmptyFalse(wkp_doc_hash) ||
            utils._nilEmptyFalse(wkp_creator) ||
            utils._nilEmptyFalse(wkc_content)
        ) {
            msg = `invalid update wiki page info ${utils.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        wkp_title = utils.sanitizingContent(wkp_title);

        if (utils._nilEmptyFalse(wkp_hash)) {
            wkp_hash = iutils.convertTitleToHash(wkp_title);
        }
        if (utils._nilEmptyFalse(wkp_unique_hash)) {
            wkp_unique_hash = crypto.keccak256(`${wkp_in_hash}/${wkp_hash}`);
        }

        // check if exist newest version
        let exist = model.sRead({
            table: tableWikiPages,
            query: [
                ['wkp_unique_hash', wkp_unique_hash],
                ['wkp_last_modified', ['>=', wkp_last_modified]],
            ]
        });
        if (exist.length > 0)
            return { err: false, msg: `already a newer version of wikipage(${utils.hash6c(wkp_unique_hash)}) exist in db` };

        // check if same doc alredy recorded
        exist = model.sRead({
            table: tableWikiPages,
            query: [
                ['wkp_doc_hash', wkp_doc_hash],
            ]
        });
        if (exist.length > 0)
            return { err: false, msg: `wikipage(${utils.hash6c(wkp_doc_hash)}) already recorded!` }

        let updates = {
            wkp_hash,
            wkp_doc_hash,

            wkp_lang,
            wkp_format_version,
            wkp_last_modified,
            wkp_creator
        }
        clog.app.info(`updating wiki page: ${utils.stringify(updates)}`);

        model.sUpdate({
            table: tableWikiPages,
            query: [['wkp_unique_hash', wkp_unique_hash]],
            updates
        });

        // update contents too
        updates = {
            wkc_content
        }
        clog.app.info(`updating wiki page content: ${utils.stringify(updates)}`);

        model.sUpdate({
            table: tableWikiPageContent,
            query: [['wkc_unique_hash', wkp_unique_hash]],
            updates
        });

        return { err: false }
    }


}

module.exports = PagesHandler;