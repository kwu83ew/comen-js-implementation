const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const walletAddressHandler = require('../../../web-interface/wallet/wallet-address-handler');



class WikiHandler {

    static recordInWikiDB(args) {
        let fDoc = args.fDoc;
        switch (fDoc.dClass) {

            case iConsts.CPOST_CLASSES.WK_CreatePage:
                return WikiHandler.pages.insertRegDoc(args);
                break;

            case iConsts.CPOST_CLASSES.WK_EditPage:
                return WikiHandler.pages.updateWkDoc(args);
                break;

            case iConsts.CPOST_CLASSES.WK_EditPage:
                break;
        }

        return { err: false }

    }



}

WikiHandler.init = require('./wiki-init-handler');
WikiHandler.init._super = WikiHandler;

WikiHandler.pages = require('./pages-handler');
WikiHandler.pages._super = WikiHandler;

module.exports = WikiHandler;