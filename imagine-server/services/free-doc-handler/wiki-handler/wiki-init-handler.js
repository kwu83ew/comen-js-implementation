const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const fileHandler = require('../../../hard-copy/file-handler');




class InitHandler {
    static initWikiPages() {
        const wikiPages = [
            'imagine', 'imagine network', 'home', 'faq', 'development', 'support languages', 'page formatting', 'prepare a proposal',
            'pledging and unpledging', 'mail faq', 'messenger', 'pollings', 'contributors', 'iips', 'todos', 'version control',
            'reserved coins'
        ];
        let creationDate = iConsts.getLaunchDate();
        let inHash = iutils.convertTitleToHash('imagine');
        // console.log('InitAgorasHandler.mapTitleToAgUniqueHash', InitAgorasHandler.mapTitleToAgUniqueHash);
        // read all contents and insert in proper Agora
        for (let aPageTitle of wikiPages) {
            let contentRes = fileHandler.packetFileReadSync({
                fileName: `${aPageTitle}.html`,
                appCloneId: 0,
                dirName: 'services/free-doc-handler/wiki-handler/init-contents'
            });
            if (contentRes.err != false) {
                utils.exiter(`could not create wiki page ${utils.stringify(contentRes)}`, 394);
            }
            let wkHash = iutils.convertTitleToHash(aPageTitle);
            let wkUniqueHash = crypto.keccak256(`${inHash}/${wkHash}`);
            InitHandler._super.pages.insertPage({
                wkp_title: aPageTitle,
                wkp_in_hash: inHash,
                wkp_hash: wkHash,
                wkp_doc_hash: wkHash,
                wkp_unique_hash: wkUniqueHash,
                wkp_lang: iConsts.DEFAULT_LANG,
                wkp_format_version: iConsts.DEFAULT_VERSION,
                wkp_creation_date: creationDate,
                wkp_last_modified: creationDate,
                wkp_creator: iConsts.HU_INAME_OWNER_ADDRESS,
                wkc_content: contentRes.content,
            });
        }
        return { err: false }
    }

}

module.exports = InitHandler;
