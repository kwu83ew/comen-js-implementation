const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const crypto = require('../../crypto/crypto');
const fileHandler = require('../../hard-copy/file-handler');
const machine = require('../../machine/machine-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const listener = require('../../plugin-handler/plugin-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const docBufferHandler = require('../buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');


/**
 * imagine accepts any type of information to be record on DAG
 * 
 */

const table = 'i_custom_posts';

let genericPostTpl = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dType: iConsts.DOC_TYPES.CPost,
    dClass: "",
    dVer: "0.0.0",
    creationDate: "",
    dLen: "0000000",

    dExtInfo: {},
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000",


    author: "", // requested name to register
    /**
     * related iName to which this post was sent
     * e.g. /demos or /userX
     */
    iNameHash: "0000000000000000000000000000000000000000000000000000000000000000",

    cTMode: 'wiki',  // content treatment mode
    cTMode: 'blog',  // content treatment mode
    cTMode: 'stream',  // content treatment mode
    cTMode: 'my custome mode',  // which specialized plugin can understand and trerat with.

    //if it is forum-like

    cPCmd: [    // cusaomt post command
        'create a forum, currently is simple but later , can be config to have send right for some group of users, ban user, encypted posts, ', 'close forum', 'a post on forum', 'a twit', 'a weblog post',
        'add member to forum', 'block member', 'pin post on top of forum', 'send a like/dislike to a post. like can contains some PAI as Tip',
        'create & register a url and its proper template '
    ],
    cPRefs: {
        // custom references such as
        uplink: '', // if the post is a reply to othre post or the agora is a sub category
        forumTopic: '',// the forum which the post is sending for

        //if it is weblog-like (also facebook/twitter/instagram all have same concept)
        weblogUrl: '',
        postId: '',

    },

    cPTags: [],
    cPMeta: {

    }, // an arbitrary dictionary of key/values


    content: `
        imagine follows the [https://mediawiki.org MediaWiki] formatting.
        surly it must be implementd completely! [[TODOs|here]] is a list of what we can do.


        [[media/cats.jpg|nice cats]]
    `

}

let WikiDefaultTemplate = `
    [pageTitle]
    [pageBody]
`;
class FreeDocumentHandler {

    static customValidate(args) {
        let fDoc = args.fDoc;
        // TODO: implemet a good factory design pattern and get rid of these switches
        switch (fDoc.dClass) {

            case iConsts.CPOST_CLASSES.File:
                return FreeDocumentHandler.fileDocHandler.validateFileDoc(args)
                break;

            case iConsts.CPOST_CLASSES.DMS_Post:
                return FreeDocumentHandler.demos.posts.validatePostToAgora(args)
                break;

            case iConsts.CPOST_CLASSES.DMS_RegAgora:
                return FreeDocumentHandler.demos.validateRegAgora(args)
                break;

            case iConsts.CPOST_CLASSES.WK_CreatePage:
            case iConsts.CPOST_CLASSES.WK_EditPage:
                return FreeDocumentHandler.wiki.pages.validateWikiPage(args)
                break;

            default:
                return listener.doCallSync('SASH_custom_validate_FreeDocs', args);
                break;
        }
    }

    static customHash(args) {
        let fDoc = args.fDoc;
        // TODO: implemet a good factory design pattern and get rid of these switches
        switch (fDoc.dClass) {

            case iConsts.CPOST_CLASSES.File:
                return FreeDocumentHandler.fileDocHandler.calcHashDCustomFile(args)
                break;

            case iConsts.CPOST_CLASSES.DMS_Post:
                return FreeDocumentHandler.demos.posts.calcHashDAgoraPostSpecial(args.fDoc)
                break;

            case iConsts.CPOST_CLASSES.DMS_RegAgora:
                return FreeDocumentHandler.demos.calcHashDAgoraRegReq(args)
                break;

            case iConsts.CPOST_CLASSES.WK_CreatePage:
            case iConsts.CPOST_CLASSES.WK_EditPage:
                return FreeDocumentHandler.wiki.pages.calcHashDWikiRegReq(args)
                break;

            default:
                return listener.doCallSync('SASH_custom_calc_hash_FreeDocs', args);
                break;
        }
    }

    static customTreatments(args) {
        let fDoc = args.fDoc;
        // TODO: implemet a good factory design pattern and get rid of these switches
        switch (fDoc.dClass) {

            case iConsts.CPOST_CLASSES.File:
                return FreeDocumentHandler.fileDocHandler.maybeWriteFileInLocal(args)
                break;

            case iConsts.CPOST_CLASSES.DMS_Post:
            case iConsts.CPOST_CLASSES.DMS_RegAgora:
                return FreeDocumentHandler.demos.recordInDemosDB(args)
                break;

            case iConsts.CPOST_CLASSES.WK_CreatePage:
            case iConsts.CPOST_CLASSES.WK_EditPage:
                return FreeDocumentHandler.wiki.recordInWikiDB(args)
                break;

            default:
                return listener.doCallSync('SASH_custom_treat_FreeDocs', args);
                break;
        }

    }

    static validatefDocSigner(args) {

        let fDoc = args.fDoc;
        let dExtInfo = args.dExtInfo;
        let signMsg = args.signMsg;

        // validate signer (if exist)
        let unlockSet, isValidUnlock;

        if (!crypto.bech32_isValidAddress(fDoc.signer)) {
            msg = `invalid owner(${iutils.shortBech8(fDoc.signer)})!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        unlockSet = dExtInfo.uSet;
        isValidUnlock = mOfNHandler.validateSigStruct({
            address: fDoc.signer,
            uSet: unlockSet
        });
        if (!isValidUnlock) {
            msg = `${stage}, Invalid! given freedoc(file sender signature) unlock structure for req, unlock:${utils.stringify({
                address: fDoc.signer,
                unlockSet
            })}`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // signature & permission validate check
        for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
            let aSignature = dExtInfo.signatures[signInx];
            try {
                let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                if (!verifyRes) {
                    msg = `Block creating, the FreeDoc(${utils.hash6c(fDoc.hash)}) has invalid signature`;
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            } catch (e) {
                msg = `error in verify FreeDoc Signature: ${utils.stringify(args)}`;
                clog.trx.error(msg);
                clog.trx.error(e);
                return { err: true, msg, shouldPurgeMessage: true };
            }
        }

        return { err: false }
    }

    static calcCostDCustomPost(args) {
        console.log(`calc cutom post args ${utils.stringify(args)}`);
        clog.app.info(`calc cutom post args ${utils.stringify(args)}`);
        let cPost = args.cPost;
        let cDate = args.cDate;

        let dLen = parseInt(cPost.dLen);
        clog.app.info(`calc cutom post dLen ${utils.stringify(dLen)}`);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: cPost.dType, dClass: cPost.dClass, dLen });

        clog.app.info(`calc cutom post theCost1 ${utils.stringify(theCost)}`);

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: cPost.dType,
                dClass: cPost.dClass,
                dLen
            });
        clog.app.info(`calc cutom post theCost2 ${utils.stringify(theCost)}`);

        return { err: false, cost: Math.floor(theCost) };
    }

    static payForFreeDocAndPushToBuffer(args) {

        let fDoc = args.fDoc;
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

        // calculate post cost
        let cPostCost = FreeDocumentHandler.calcCostDCustomPost({
            cDate: utils.getNow(),
            cPost: { dLen: fDoc.dLen, dType: fDoc.dType, dClass: fDoc.dClass },
            stage: iConsts.STAGES.Creating
        });
        clog.trx.info(`cPostCost Cost ${utils.stringify(cPostCost)}`);
        if ((cPostCost.err != false) || (cPostCost.cost == 0))
            return cPostCost;

        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_FDOC', cPostCost.cost]
        ];

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: cPostCost.cost * 1.5
        });
        if (spendables.err != false) {
            clog.trx.error('get Some Inputs res: ', utils.stringify(spendables));
            return spendables;
        }

        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(cPostCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: fDoc.hash,
            description: 'Pay for recording custom post on chain',
            inputs,
            outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        clog.trx.info('signed custom post costs trx: ', utils.stringify(trxDtl));


        // push trx & fDoc Req to Block buffer
        let res = docBufferHandler.pushInSync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = docBufferHandler.pushInSync({ doc: fDoc, DPCost: cPostCost.cost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);


        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your Doc was pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }

}

FreeDocumentHandler.MIMETYPES = {
    TEXT_PLAIN: 'text/plain',
    TEXT_HTML: 'text/html',
    IMAGE_JPEG: 'image/jpeg',
    IMAGE_PNG: 'image/png',
    IMAGE_SVG_XML: 'image/svg+xml',
    VIDEO_MP4: 'video/mp4',
    AUDIO_MPEG: 'audio/mpeg'
}


FreeDocumentHandler.fileDocHandler = require('./file-doc-handler');
FreeDocumentHandler.fileDocHandler._super = FreeDocumentHandler;
FreeDocumentHandler.demos = require('./demos-handler/demos-handler');
FreeDocumentHandler.demos._super = FreeDocumentHandler;
FreeDocumentHandler.wiki = require('./wiki-handler/wiki-handler');
FreeDocumentHandler.wiki._super = FreeDocumentHandler;

module.exports = FreeDocumentHandler;
