const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const machine = require('../../../machine/machine-handler');
const walletAddressHandler = require('../../../web-interface/wallet/wallet-address-handler');

const tableAgoras = 'i_demos_agoras';
const tableAgoraPosts = 'i_demos_posts';

// TODO add pinedPost posibility to change first posts to better ones

class PostsHandler {

    static prepareNewPost(args) {
        let msg;
        console.log(`send New Post= args`, args);
        clog.app.info(`send New Post= args`, args);
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let napCostPayMode = _.has(args, 'napCostPayMode') ? args.napCostPayMode : 'normal';
        let signer = _.has(args, 'signer') ? args.signer : null;
        let napAgUqHash = _.has(args, 'napAgUqHash') ? args.napAgUqHash : '';  // related Agora
        let dAttrs = _.has(args, 'dAttrs') ? args.dAttrs : '';
        let napReply = _.has(args, 'napReply') ? args.napReply : '';
        let napReplyPoint = _.has(args, 'napReplyPoint') ? args.napReplyPoint : 0;
        let napOpinion = _.has(args, 'napOpinion') ? args.napOpinion : '';


        if (utils._nilEmptyFalse(napAgUqHash)) {
            msg = `invalid Agora hash`;
            return { err: true, msg };
        }

        let fDoc = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.CPost,
            dClass: iConsts.CPOST_CLASSES.DMS_Post,
            /**
             * if post has special attributes to consoder. e.g. {isPreProposal:'Y'} means the opinion is a preproposal
             * plugins can use this feature to customizing post and adding new features to system
             */
            dAttrs: {},
            dLen: "0000000",
            dVer: "0.0.0",
            fVer: "0.0.0",  // formating version
            creationDate: creationDate,
            signer,
            agHash: napAgUqHash, // hash of proper Agora
            opinion: napOpinion,
            dExtInfo: "",
            dExtHash: ""
        }

        if (!utils._nilEmptyFalse(dAttrs)) {
            fDoc.dAttrs = dAttrs;
        }
        if (!utils._nilEmptyFalse(napReply)) {
            fDoc.reply = napReply;
            fDoc.replyPoint = napReplyPoint;
        }

        let signMsg = PostsHandler.getSignMsgDNewPost(fDoc);

        if (utils._nilEmptyFalse(signer)) {
            let signatureInfo = machine.signByMachineKey({ signMsg });
            fDoc.signer = signatureInfo.backer;
            fDoc.dExtInfo = {
                signatures: signatureInfo.signatures,
                uSet: signatureInfo.uSet
            }
        } else {
            let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: signer });
            if (signatureInfo.err != false)
                return signatureInfo;

            fDoc.dExtInfo = {
                signatures: signatureInfo.signatures,
                uSet: signatureInfo.uSet
            }
        }
        fDoc.dExtHash = iutils.doHashObject({
            signatures: fDoc.dExtInfo.signatures,
            uSet: fDoc.dExtInfo.uSet
        });

        fDoc.dLen = iutils.paddingDocLength(utils.stringify(fDoc).length);
        let custHashRes = PostsHandler.calcHashDAgoraPostSpecial(fDoc);
        if (custHashRes.err != false)
            return custHashRes;
        fDoc.hash = custHashRes.hash;

        return { err: false, fDoc }
    }

    static calcHashDAgoraPostSpecial(fDoc) {
        // as always ordering properties by alphabet
        let hashables = {
            dExtHash: fDoc.dExtHash,
            dLen: fDoc.dLen,
            signer: fDoc.signer,
        }
        clog.app.info(`calc post Agora fDocHash hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return { err: false, hash };
    }

    static getSignMsgDNewPost(fDoc) {
        // as always ordering properties by alphabet
        let signables = {
            creationDate: fDoc.creationDate,
            dClass: fDoc.dClass,
            opinion: fDoc.opinion,
            dType: fDoc.dType,
            dVer: fDoc.dVer
        }
        if (!utils._nilEmptyFalse(fDoc.napReply)) {
            signables.reply = fDoc.reply;
            signables.replyPoint = fDoc.replyPoint;
        }
        if (!utils._nilEmptyFalse(fDoc.dAttrs)) {
            signables.dAttrs = fDoc.dAttrs;
        }
        clog.app.info(`prepare new post Agora signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static validatePostToAgora(args) {
        let msg;
        if (!iutils.isValidVersionNumber(args.fDoc.fVer)) {
            msg = `invalid formatting version in Agora post fDoc(${utils.hash6c(args.fDoc.hash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        // only signer validate!
        args.signMsg = PostsHandler.getSignMsgDNewPost(args.fDoc);
        let vRes = PostsHandler._super._super.validatefDocSigner(args);
        if (vRes.err != false)
            return vRes;
        return { err: false }
    }

    static searchInPosts(args = {}) {
        args.table = tableAgoraPosts;
        let posts = model.sRead(args);
        return posts;
    }

    static insertDemosDocPost(args) {
        clog.app.info(`insert Demos Doc Post ${utils.stringify(args)}`);
        let block = args.block;
        let creationDate = block.creationDate;
        let fDoc = args.fDoc;
        let opinion = fDoc.opinion;

        let reply = _.has(fDoc, 'reply') ? fDoc.reply : '';
        let replyPoint = _.has(fDoc, 'replyPoint') ? fDoc.replyPoint : 0;
        let dAttrs = _.has(fDoc, 'dAttrs') ? fDoc.dAttrs : null;



        if (utils._nilEmptyFalse(dAttrs)) {
            // it is normal html post
            opinion = utils.sanitizingContent(opinion);

        } else {
            // mabe need special treatment befor insert to db
            if (_.has(dAttrs, 'isPreProposal') && (dAttrs.isPreProposal == iConsts.CONSTS.YES)) {
                // convert opinion to base64 and save it in db
                opinion = utils.stringify(opinion);
                let b = Buffer.alloc(opinion.length, opinion);
                let base64Encoded = b.toString('base64');
                opinion = base64Encoded;
            }
            
            dAttrs = utils.stringify(dAttrs);
        }

        return PostsHandler.insertDemosPost({
            ap_ag_unique_hash: fDoc.agHash,
            ap_doc_hash: fDoc.hash,
            ap_opinion: opinion,
            ap_format_version: fDoc.fVer,
            ap_reply: reply,
            ap_reply_point: replyPoint,
            ap_attrs: dAttrs,
            ap_creation_date: creationDate,
            ap_creator: fDoc.signer,
        });
    }

    static insertDemosPost(args) {
        clog.app.info(`insert Demos Post ${utils.stringify(args)}`);
        let msg;
        let ap_ag_unique_hash = _.has(args, 'ap_ag_unique_hash') ? args.ap_ag_unique_hash : '';
        let ap_format_version = _.has(args, 'ap_format_version') ? args.ap_format_version : '0.0.0';
        let ap_opinion = _.has(args, 'ap_opinion') ? args.ap_opinion : '';
        let ap_reply = _.has(args, 'ap_reply') ? args.ap_reply : '';
        let ap_reply_point = _.has(args, 'ap_reply_point') ? args.ap_reply_point : 0;
        let ap_creation_date = _.has(args, 'ap_creation_date') ? args.ap_creation_date : utils.getNow();
        let ap_creator = _.has(args, 'ap_creator') ? args.ap_creator : null;
        let ap_attrs = _.has(args, 'ap_attrs') ? args.ap_attrs : null;

        if (utils._nilEmptyFalse(ap_ag_unique_hash)) {
            msg = `empty ap_ag_unique_hash`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (utils._nilEmptyFalse(ap_creator)) {
            msg = `empty ap_creator`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let exist = model.sRead({
            table: tableAgoraPosts,
            query: [
                ['ap_doc_hash', args.ap_doc_hash],
            ]
        });
        if (exist.length > 0)
            return { err: false }

        clog.app.info(`ap_opinion, ${utils.stringify(ap_opinion)}`);
        let values = {
            ap_ag_unique_hash,
            ap_doc_hash: args.ap_doc_hash,
            ap_opinion,
            ap_attrs,
            ap_format_version: ap_format_version,
            ap_reply,
            ap_reply_point,
            ap_creation_date,
            ap_creator
        }
        clog.app.info(`inserting a post to Agora: ${utils.stringify(values)}`);

        model.sCreate({
            table: tableAgoraPosts,
            values
        });
    }


}

module.exports = PostsHandler;