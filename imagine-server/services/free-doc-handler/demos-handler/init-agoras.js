const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const fileHandler = require('../../../hard-copy/file-handler');

const tableAgoras = 'i_demos_agoras';


class InitAgorasHandler {

    static initAgoras() {
        InitAgorasHandler.mapTitleToAgUniqueHash = {};

        let defAgoras = [
            {
                title: 'Beginners',
                description: '',
                tags: '',
            },
            {
                title: 'Development',
                description: '',
                tags: '',
            },
            {
                title: 'Economy',
                description: '',
                tags: '',
            },
            {
                title: 'Translation',
                description: '',
                tags: '',
            },

        ];


        let inHash = iutils.convertTitleToHash('imagine');
        let creationDate = iConsts.getLaunchDate();

        for (let anAg of defAgoras) {
            let ag_doc_hash = iutils.convertTitleToHash(anAg.title);   // a dummy doc-hash for initializing
            let ag_hash = iutils.convertTitleToHash(anAg.title);
            let ag_unique_hash = crypto.keccak256(`${inHash}/${ag_hash}`);
            let insRes = InitAgorasHandler._super.insertAgoraInDB({
                ag_title: anAg.title,
                ag_doc_hash,
                ag_hash,
                ag_in_hash: inHash,
                ag_unique_hash,
                ag_parent: '',
                ag_description: anAg.description,
                ag_creation_date: creationDate,
                ag_last_modified: creationDate,
                ag_creator: iConsts.HU_INAME_OWNER_ADDRESS,
            });
            InitAgorasHandler.mapTitleToAgUniqueHash[`${anAg.title}`] = insRes.ag_unique_hash;
        }


        // insert sub-category
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Beginners',
            defAgoras: [
                {
                    title: 'Before Start',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Contribute, Proposals, Earn And Shares',
                    description: '',
                    tags: '',
                }
            ]
        });
        // let 'Interface Develope (Angular)' = 'Interface Develope (Angular)';
        // insert sub-category
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Development',
            defAgoras: [
                {
                    title: 'Pre Proposals and Discussions',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Security',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Core develope',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Interface Develope (Angular)',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Mobile Wallet',
                    description: '',
                    tags: '',
                },
                {
                    title: '3rd Party development',
                    description: '',
                    tags: '',
                },
            ]
        });
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: '3rd Party development',
            defAgoras: [
                {
                    title: 'Machine Buffer watcher',
                    description: '',
                    tags: '',
                },
            ]
        });

        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Economy',
            defAgoras: [
                {
                    title: 'Inflationary or Deflationary Money',
                    description: '',
                    tags: '',
                },
                {
                    title: 'How to determine coins value',
                    description: '',
                    tags: '',
                },

            ]
        });

        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Core develope',
            defAgoras: [
                {
                    title: 'Comunication & Encryption Enhancement',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Plugins',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Wallet API',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Messenger improvement',
                    description: '',
                    tags: '',
                },
            ]
        });
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Plugins',
            defAgoras: [
                {
                    title: 'Plugin handler improvements',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Distributed Version Control Plugin implementation',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Distributed Wiki Plugin implementation',
                    description: '',
                    tags: '',
                },
            ]
        });
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Interface Develope (Angular)',
            defAgoras: [
                {
                    title: 'Design & Theme Improvement',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Multilingual',
                    description: '',
                    tags: '',
                },
                {
                    title: 'Wallet Enhancement',
                    description: '',
                    tags: '',
                },
            ]
        });

        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Design & Theme Improvement',
            defAgoras: [
                {
                    title: 'Logos, either imagine or PAI',
                    description: '',
                    tags: '',
                },

            ]
        });
        InitAgorasHandler.insertSubCat({
            inHash,
            parentName: 'Comunication & Encryption Enhancement',
            defAgoras: [
                {
                    title: 'iPGP improvement',
                    description: '',
                    tags: '',
                },

            ]
        });

        // insert initial content of Agoras
        return InitAgorasHandler.initPosts();
    }

    static insertSubCat(args) {
        // console.log('insertSubCat args', args);

        let inHash = args.inHash;
        let parentName = args.parentName;
        let defAgoras = args.defAgoras;
        let creationDate = iConsts.getLaunchDate();

        let parentInfo = InitAgorasHandler._super.searchInAgoras({
            query: [
                ['ag_in_hash', inHash],
                ['ag_title', parentName],
            ]
        });
        // console.log('parentInfo', parentInfo);
        let parentUniqHash = parentInfo[0].ag_unique_hash;
        for (let anAg of defAgoras) {
            let ag_doc_hash = iutils.convertTitleToHash(anAg.title);   // a dummy doc-hash for initializing
            let ag_hash = iutils.convertTitleToHash(anAg.title);
            let ag_unique_hash = crypto.keccak256(`${inHash}/${ag_hash}`);

            let insRes = InitAgorasHandler._super.insertAgoraInDB({
                ag_title: anAg.title,
                ag_doc_hash,
                ag_hash,
                ag_in_hash: inHash,
                ag_unique_hash,
                ag_parent: parentUniqHash,
                ag_description: anAg.description,
                ag_creation_date: creationDate,
                ag_last_modified: creationDate,
                ag_creator: iConsts.HU_INAME_OWNER_ADDRESS,
            });
            InitAgorasHandler.mapTitleToAgUniqueHash[`${parentName}-${anAg.title}`] = insRes.ag_unique_hash;
        }
    }

    static initPosts() {
        let creationDate = iConsts.getLaunchDate();
        // console.log('InitAgorasHandler.mapTitleToAgUniqueHash', InitAgorasHandler.mapTitleToAgUniqueHash);
        // read all contents and insert in proper Agora
        for (let aFile of utils.objKeys(InitAgorasHandler.mapTitleToAgUniqueHash)) {
            // console.log('InitAgorasHandler.aFile', aFile);
            let contentRes = fileHandler.packetFileReadSync({
                fileName: `${aFile}.html`,
                appCloneId: 0,
                dirName: 'services/free-doc-handler/demos-handler/init-contents'
            });
            // console.log('InitAgorasHandler.contentRes', contentRes);

            if (contentRes.err != false) {
                utils.exiter(`could not create Agoras ${contentRes}`, 970);
            }

            InitAgorasHandler._super.posts.insertDemosPost({
                ap_ag_unique_hash: InitAgorasHandler.mapTitleToAgUniqueHash[aFile],
                ap_doc_hash: crypto.keccak256(contentRes.content),
                ap_opinion: contentRes.content,
                ap_reply: '',
                ap_reply_point: 0,
                ap_creation_date: creationDate,
                ap_creator: iConsts.HU_INAME_OWNER_ADDRESS

            });

        }
        return { err: false }
    }
}

module.exports = InitAgorasHandler;