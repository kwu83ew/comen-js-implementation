const _ = require('lodash');
const iConsts = require('../../../config/constants');
const clog = require('../../../loggers/console_logger');
const model = require('../../../models/interface');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const machine = require('../../../machine/machine-handler');
const walletAddressHandler = require('../../../web-interface/wallet/wallet-address-handler');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');

const tableAgoras = 'i_demos_agoras';

class DemosHandler {

    static prepareNewAgoraRegisterDoc(args) {
        let msg;
        console.log(`prepare A build new Agora Document args`, args);
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let agCreatorAccount = _.has(args, 'agCreatorAccount') ? args.agCreatorAccount : '';
        let aginHash = _.has(args, 'aginHash') ? args.aginHash : iutils.convertTitleToHash('imagine');   // the domain name of forum
        let agParent = _.has(args, 'agParent') ? args.agParent : '';
        let agTitle = _.has(args, 'agTitle') ? args.agTitle : '';
        let agDescription = _.has(args, 'agDescription') ? args.agDescription : '';
        let agTags = _.has(args, 'agTags') ? args.agTags : '';
        let agSelectedLang = _.has(args, 'agSelectedLang') ? args.agSelectedLang : iConsts.CONSTS.DEFAULT_LANG;

        agTitle = utils.sanitizingContent(agTitle);
        agTitle = utils.utf8Decode(agTitle);
        agDescription = utils.sanitizingContent(agDescription);
        agTags = utils.sanitizingContent(agTags);
        // validate signer (if exist)

        if (utils._nilEmptyFalse(aginHash)) {
            msg = `Invalid/Empty Agora Address!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        if (utils._nilEmptyFalse(agCreatorAccount)) {
            msg = `Invalid/Empty Agor signer(${iutils.shortBech8(agCreatorAccount)})!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        if (!crypto.bech32_isValidAddress(agCreatorAccount)) {
            msg = `Invalid Agor signer(${iutils.shortBech8(agCreatorAccount)})!`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        let agHash = iutils.convertTitleToHash(agTitle);
        let uniqHashHandle = crypto.keccak256(`${aginHash}/${agHash}`);
        let exist = model.sRead({
            table: tableAgoras,
            query: [
                ['ag_unique_hash', uniqHashHandle],
            ]
        });
        if (exist.length > 0)
            return { err: true, msg: `The title(${agTitle}) for desired address(${utils.hash6c(aginHash)}) for Agora is already registerd!` }

        let fDoc = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.CPost,
            dClass: iConsts.CPOST_CLASSES.DMS_RegAgora,
            dLen: "0000000",
            dVer: iConsts.DEFAULT_VERSION,
            fVer: iConsts.DEFAULT_VERSION,
            creationDate: creationDate,
            parent: agParent,   // super category(if exist)
            signer: agCreatorAccount,
            title: agTitle,
            cLang: agSelectedLang,
            description: agDescription,
            tags: agTags,
            inHash: aginHash,
            dExtInfo: "",
            dExtHash: "",
        }

        // TODO: implement a language detection sub-module to automatically sujjest the language or adjust it

        let signMsg = DemosHandler.getSignMsgDRegAgora(fDoc);
        let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: agCreatorAccount });
        if (signatureInfo.err != false)
            return signatureInfo;

        fDoc.dExtInfo = {
            signatures: signatureInfo.signatures,
            uSet: signatureInfo.uSet
        }
        fDoc.dExtHash = iutils.doHashObject({
            signatures: fDoc.dExtInfo.signatures,
            uSet: fDoc.dExtInfo.uSet
        });

        fDoc.dLen = iutils.paddingDocLength(utils.stringify(fDoc).length);
        let custHashRes = DemosHandler.calcHashDAgoraRegReq({ fDoc });
        if (custHashRes.err != false)
            return custHashRes;
        fDoc.hash = custHashRes.hash;

        return { err: false, fDoc }
    }



    static getSignMsgDRegAgora(fDoc) {
        // as always ordering properties by alphabet
        let signables = {
            cLang: fDoc.cLang,
            creationDate: fDoc.creationDate,
            dClass: fDoc.dClass,
            description: fDoc.description,
            dType: fDoc.dType,
            dVer: fDoc.dVer,
            fVer: fDoc.fVer,
            inHash: fDoc.inHash,
            parent: fDoc.parent,
            tags: fDoc.tags,
            title: fDoc.title
        }
        clog.app.info(`prepare reg req for Agora signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static calcHashDAgoraRegReq(args) {
        let fDoc = args.fDoc;
        // as always ordering properties by alphabet
        let hashables = {
            dExtHash: fDoc.dExtHash,
            dLen: fDoc.dLen,
            signer: fDoc.signer,
        }
        clog.app.info(`calc reg Agora fDocHash hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return { err: false, hash };
    }

    static validateRegAgora(args) {
        let msg;
        if (!iutils.isValidVersionNumber(args.fDoc.fVer)) {
            msg = `invalid formatting version in Build Agora fDoc(${utils.hash6c(args.fDoc.hash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        if (!iutils.isValidLanguageCode(args.fDoc.cLang)) {
            msg = `invalid content language in Build Agora fDoc(${utils.hash6c(args.fDoc.hash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }

        // only signer validate!
        args.signMsg = DemosHandler.getSignMsgDRegAgora(args.fDoc);
        let vRes = DemosHandler._super.validatefDocSigner(args);
        if (vRes.err != false)
            return vRes;
        return { err: false }
    }


    static recordInDemosDB(args) {
        let fDoc = args.fDoc;
        switch (fDoc.dClass) {

            case iConsts.CPOST_CLASSES.DMS_Post:
                return DemosHandler.posts.insertDemosDocPost(args);
                break;

            case iConsts.CPOST_CLASSES.DMS_RegAgora:
                return DemosHandler.insertAgoraInDB(args);
                break;
        }

        return { err: false }
    }

    static searchInAgoras(args = {}) {
        args.table = tableAgoras;
        return model.sRead(args);
    }

    static insertAgoraInDB(args) {
        let msg;
        let ag_title = _.has(args, 'ag_title') ? args.ag_title : null;
        let ag_unique_hash = _.has(args, 'ag_unique_hash') ? args.ag_unique_hash : null;
        let ag_creator = _.has(args, 'ag_creator') ? args.ag_creator : null;
        let ag_lang = _.has(args, 'ag_lang') ? args.ag_lang : iConsts.DEFAULT_LANG; // content language
        let ag_description = _.has(args, 'ag_description') ? args.ag_description : '';
        let ag_tags = _.has(args, 'ag_tags') ? args.ag_tags : '';
        let ag_format_version = _.has(args, 'ag_format_version') ? args.ag_format_version : '0.0.0';
        let ag_creation_date = _.has(args, 'ag_creation_date') ? args.ag_creation_date : utils.getNow();
        let ag_last_modified = _.has(args, 'ag_last_modified') ? args.ag_last_modified : utils.getNow();

        if (utils._nilEmptyFalse(ag_title)) {
            msg = `ag_title is empty`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (utils._nilEmptyFalse(ag_creator)) {
            msg = `ag_creator is empty`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (utils._nilEmptyFalse(ag_unique_hash)) {
            ag_unique_hash = crypto.keccak256(`${args.ag_in_hash}/${args.ag_hash}`);
        }


        let exist = model.sRead({
            table: tableAgoras,
            query: [
                ['ag_unique_hash', ag_unique_hash],
            ]
        });
        if (exist.length > 0)
            return { err: false, msg: `Agora ag_unique_hash(${ag_unique_hash}) already exist in DB` }

        let controlledByMachine = iConsts.CONSTS.NO;
        let mpCode = '';
        let walletControlledAddresses = walletAddressHandler.searchWalletAdress({ query: [['wa_address', ag_creator]] });
        if (walletControlledAddresses.length > 0) {
            controlledByMachine = iConsts.CONSTS.YES;
            mpCode = walletControlledAddresses[0].wampCode;
        }
        let values = {
            ag_title: ag_title,
            ag_doc_hash: args.ag_doc_hash,
            ag_hash: args.ag_hash,
            ag_in_hash: args.ag_in_hash,    // the related domain
            ag_unique_hash: ag_unique_hash,
            ag_parent: args.ag_parent,
            ag_lang,
            ag_description,
            ag_format_version,
            ag_tags,
            ag_creation_date,
            ag_last_modified,
            ag_creator,
            ag_controlled_by_machine: controlledByMachine,
            ag_mp_code: mpCode
        }

        clog.app.info(`inserting an Agora reg req ${utils.stringify(values)}`);
        model.sCreate({
            table: tableAgoras,
            values
        });

        return { err: false, ag_unique_hash };
    }
}

DemosHandler.initAg = require('./init-agoras');
DemosHandler.initAg._super = DemosHandler;

DemosHandler.posts = require('./posts-handler');
DemosHandler.posts._super = DemosHandler;

// DemosHandler.initAg.initAgoras();
module.exports = DemosHandler;