const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const crypto = require('../../crypto/crypto');
const fileHandler = require('../../hard-copy/file-handler');
const machine = require('../../machine/machine-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');

const table = 'i_machine_posted_files';


class FileDocHandler {

    static prepareAfileDocument(args) {
        let msg;
        console.log(`prepare A file Document args`, args);
        let shouldSignBySender = _.has(args, 'shouldSignBySender') ? args.shouldSignBySender : true;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let cPFileLabel = _.has(args, 'cPFileLabel') ? args.cPFileLabel : '';
        let fileName = args.fileName;
        let extension = args.extension;
        let fileDoc = {
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            dType: iConsts.DOC_TYPES.CPost,
            dClass: iConsts.CPOST_CLASSES.File,
            dLen: "0000000",
            dVer: "0.0.0",
            creationDate: "",
            signer: "", //signer can be empty, means it is not signed
            cPFileLabel: "",
            cPFileMimetype: "",
            cPFileName: "",
            cPFileContent: "",
            dExtInfo: "NOE",
            dExtHash: "NOE",

        }
        fileDoc.creationDate = creationDate;

        let cPFileContent = fileHandler.fReadBinarySync({
            appCloneId: 0,
            dirName: 'tmp/',
            fileName
        });
        fileDoc.cPFileName = `${crypto.keccak256(cPFileContent)}.${extension}`;
        fileDoc.cPFileLabel = cPFileLabel;
        fileDoc.cPFileContent = cPFileContent;
        fileDoc.cPFileMimetype = args.mimetype;

        if (shouldSignBySender) {
            let signMsg = FileDocHandler.getSignMsgDCFile(fileDoc);
            let { backer, uSet, signatures } = machine.signByMachineKey({ signMsg });
            fileDoc.signer = backer;
            fileDoc.dExtInfo = { signatures, uSet }
            fileDoc.dExtHash = iutils.doHashObject({ signatures, uSet });
        }

        fileDoc.dLen = iutils.paddingDocLength(utils.stringify(fileDoc).length);
        let custHashRes = FileDocHandler.calcHashDCustomFile({ fDoc: fileDoc });
        if (custHashRes.err != false)
            return custHashRes;
        fileDoc.hash = custHashRes.hash;

        return { err: false, fileDoc }
    }

    static getSignMsgDCFile(fileDoc) {
        // as always ordering properties by alphabet
        let signables = {
            cPLable: fileDoc.cPLable,
            cPMimetype: fileDoc.cPMimetype,
            cPFileName: fileDoc.cPFileName,
            cPFileContent: fileDoc.cPFileContent,
            creationDate: fileDoc.creationDate,
            dClass: fileDoc.dClass,
            dType: fileDoc.dType,
            dVer: fileDoc.dVer,
        }
        clog.app.info(`prepare cust file signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static calcHashDCustomFile(args) {
        let fileDoc = args.fDoc;
        // as always ordering properties by alphabet
        let hashables = {
            dExtHash: fileDoc.dExtHash,
            dLen: fileDoc.dLen,
            signer: fileDoc.signer
        }
        clog.app.info(`calcfileDocHash hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return { err: false, hash };
    }

    static validateFileDoc(args) {
        let fDoc = args.fDoc;
        if (!utils._nilEmptyFalse(fDoc.signer)) {
            args.signMsg = FileDocHandler.getSignMsgDCFile(fDoc);
            let vRes = FileDocHandler._super.validatefDocSigner(args);
            if (vRes.err != false)
                return vRes;
        }
        return { err: false }
    }

    static getMyOnchainFiles(args) {
        clog.app.info(`get MyOnchainFiles args: ${utils.stringify(args)}`);
        let query = _.has(args, 'query') ? args.query : [];
        // let needURL = _.has(args, 'needURL') ? args.needURL : false;
        let iCache = iutils.getICachePath();
        let files = model.sRead({
            table,
            query
        });
        if (files.length == 0)
            return { err: false, records: files, iCache, msg: 'You did not send any file' }

        return { err: false, records: files, iCache }

    }

    static maybeWriteFileInLocal(args) {
        // depends on machine security level & file type and sender/signer shares in imagine DNA each machine can decide to decrypt file or not
        // TODO: improve it to support more comprehensive reputation system beside the file controll by Oracles and society ranking (like, dislike etc..)

        if (iConsts.DECODE_ALL_FREE_POST_FILES) {
            let cPost = args.fDoc;
            let cPFileContent = cPost.cPFileContent;

            // write file on hard
            fileHandler.fCreateBinarySync({
                dirName: iConsts.HD_PATHES.I_CACHE_FILES,
                fileName: cPost.cPFileName,
                fileContent: cPFileContent,
                overWrite: true
            });

            if (!utils._nilEmptyFalse(cPost.signer)) {
                let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
                // insert in DB too(if its mine), it will be used to administrator reasons
                let walletControlledAddresses = walletAddressHandler.searchWalletAdress({ query: [['wa_address', cPost.signer]] });
                if (walletControlledAddresses.length > 0) {
                    let exist = model.sRead({
                        table,
                        query: [
                            ['mpf_mp_code', mpCode],
                            ['mpf_name', cPost.cPFileName],
                        ]
                    });
                    if (exist.length == 0) {
                        let values = {
                            mpf_mp_code: mpCode,
                            mpf_name: cPost.cPFileName,
                            mpf_doc_hash: cPost.hash,
                            mpf_mime: cPost.cPFileMimetype,
                            mpf_creation_date: cPost.creationDate,
                            mpf_signer: cPost.signer
                        }
                        clog.app.info(`inserting a free file ${utils.stringify(values)}`);
                        model.sCreate({
                            table,
                            values
                        });
                    }
                }

            }
        }
        return { err: false };
    }


}

module.exports = FileDocHandler;