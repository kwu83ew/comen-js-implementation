const _ = require('lodash');
const iConsts = require('../../config/constants');
const db = require('../../startup/db2')
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const MESSAGE_TYPES = require('../../messaging-protocol/message-types')
const listener = require('../../plugin-handler/plugin-handler');
const GQLHandler = require('../../services/graphql/graphql-handler')
const blockUtils = require('../../dag/block-utils');
const fileHandler = require('../../hard-copy/file-handler');
const kvHandler = require('../../models/kvalue');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const machine = require('../../machine/machine-handler');
const crypto = require('../../crypto/crypto');
const pollHandler = require('../../services/polling-handler/general-poll-handler');

class ScreenshotGenerator {

    static async generateNodeScreenShot(args) {

        // refresh pollings status
        await pollHandler.maybeUpdateOpenPollingsStat();

        let finalReport = {};
        let report;

        report = await this.readBlocks()
        finalReport['i_blocks'] = report.records;

        report = await this.readBlockDocsMap()
        finalReport['i_docs_blocks_map'] = report.records;

        report = await this.readBlockExtInfos()
        finalReport['i_block_extinfos'] = report.records;

        report = await this.readProposals()
        finalReport['i_proposals'] = report.records;

        report = await this.readPollingProfiles()
        finalReport['i_polling_profiles'] = report.records;

        report = await this.readPollings()
        finalReport['i_pollings'] = report.records;

        report = await this.readBallots()
        finalReport['i_ballots'] = report.records;

        report = await this.readDNA()
        finalReport['i_dna_shares'] = report.records;

        report = await this.readTreasury()
        finalReport['i_treasury'] = report.records;

        report = await this.readTrxSpend()
        finalReport['i_trx_spend'] = report.records;

        report = await this.readTrxUTXOs()
        finalReport['i_trx_utxos'] = report.records;

        report = await this.readPledgedAccounts()
        finalReport['i_pledged_accounts'] = report.records;

        report = await this.readSignals()
        finalReport['i_signals'] = report.records;

        report = await this.readDemosAgoras()
        finalReport['i_demos_agoras'] = report.records;

        report = await this.readDemosPosts()
        finalReport['i_demos_posts'] = report.records;

        report = await this.readWikiPages()
        finalReport['i_wiki_pages'] = report.records;

        report = await this.readWikiContents()
        finalReport['i_wiki_contents'] = report.records;

        report = await this.readINameRecords()
        finalReport['i_iname_records'] = report.records;

        report = await this.readINameBindings()
        finalReport['i_iname_bindings'] = report.records;

        report = await this.readAdmPollings()
        finalReport['i_administrative_pollings'] = report.records;

        report = await this.readAdmRefinesHist()
        finalReport['i_administrative_refines_history'] = report.records;

        report = await this.readAdministrativeCurrentValues()
        finalReport['administrativeCurrentValues'] = report.records;

        report = await this.readSusTrxs()
        finalReport['i_trx_suspect_transactions'] = report.records;

        report = await this.readRejectedTrxs()
        finalReport['i_trx_rejected_transactions'] = report.records;


        return { err: false, finalReport }
    }



    static async readBlocks() {
        /**
         *     
         * SELECT b_creation_date, b_backer, b_hash, b_type, b_cycle, b_confidence, b_docs_root_hash, b_ext_root_hash, b_ancestors, b_utxo_imported 
           FROM i_blocks ORDER BY b_creation_date, b_backer, b_hash, b_type, b_cycle, b_confidence, b_docs_root_hash, b_ext_root_hash, b_ancestors, b_utxo_imported;
         */
        let records = await model.aRead({
            table: 'i_blocks',
            fields: ['b_creation_date', 'b_backer', 'b_hash', 'b_type', 'b_cycle', 'b_confidence',
                'b_docs_root_hash', 'b_ext_root_hash', 'b_ancestors', 'b_utxo_imported'],
            order: [
                ['b_creation_date', 'ASC'],
                ['b_backer', 'ASC'],
                ['b_hash', 'ASC'],
                ['b_type', 'ASC'],
                ['b_cycle', 'ASC'],
                ['b_confidence', 'ASC'],
                ['b_docs_root_hash', 'ASC'],
                ['b_ext_root_hash', 'ASC'],
                ['b_ancestors', 'ASC'],
                ['b_utxo_imported', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readBlockDocsMap() {
        /**
         *     
            SELECT dbm_block_hash, dbm_doc_index, dbm_doc_hash  
            FROM i_docs_blocks_map
            ORDER BY dbm_block_hash, dbm_doc_index, dbm_doc_hash;
         */
        let records = await model.aRead({
            table: 'i_docs_blocks_map',
            fields: ['dbm_block_hash', 'dbm_doc_index', 'dbm_doc_hash'],
            order: [
                ['dbm_block_hash', 'ASC'],
                ['dbm_doc_index', 'ASC'],
                ['dbm_doc_hash', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readProposals() {
        /**
         *     
            SELECT pr_hash, pr_type, pr_class, pr_version, md5(pr_title) AS _title, md5(pr_description) AS _description, 
            md5(pr_tags) AS _tags, pr_project_id, pr_help_hours, pr_help_level, pr_voting_longevity, 
            pr_polling_profile, pr_cotributer_account, pr_start_voting_date, pr_conclude_date, pr_approved 
            FROM i_proposals 
            ORDER BY pr_hash, pr_type, pr_class, pr_version, md5(pr_title), md5(pr_description), 
            md5(pr_tags), pr_project_id, pr_help_hours, pr_help_level, pr_voting_longevity, 
            pr_polling_profile, pr_cotributer_account, pr_start_voting_date, pr_conclude_date, pr_approved; 
         */
        let records = await model.aRead({
            table: 'i_proposals',
            fields: ['pr_hash', 'pr_type', 'pr_class', 'pr_version', 'md5(pr_title) AS _title', 'md5(pr_description) AS _description',
                'md5(pr_tags) AS _tags', 'pr_project_id', 'pr_help_hours', 'pr_help_level', 'pr_voting_longevity',
                'pr_polling_profile', 'pr_cotributer_account', 'pr_start_voting_date', 'pr_conclude_date',
                'pr_approved'],
            order: [
                ['pr_hash', 'ASC'],
                ['pr_type', 'ASC'],
                ['pr_class', 'ASC'],
                ['pr_version', 'ASC'],
                ['md5(pr_title)', 'ASC'],
                ['md5(pr_description)', 'ASC'],
                ['md5(pr_tags)', 'ASC'],
                ['pr_project_id', 'ASC'],
                ['pr_help_hours', 'ASC'],
                ['pr_help_level', 'ASC'],
                ['pr_voting_longevity', 'ASC'],
                ['pr_polling_profile', 'ASC'],
                ['pr_cotributer_account', 'ASC'],
                ['pr_start_voting_date', 'ASC'],
                ['pr_conclude_date', 'ASC'],
                ['pr_approved', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readPollingProfiles() {
        /**
         *     
            SELECT ppr_name, ppr_activated, ppr_perform_type, ppr_amendment_allowed, ppr_votes_counting_method, ppr_version 
            FROM i_polling_profiles 
            ORDER BY ppr_name, ppr_activated, ppr_perform_type, ppr_amendment_allowed, ppr_votes_counting_method, ppr_version;
         */
        let records = await model.aRead({
            table: 'i_polling_profiles',
            fields: ['ppr_name', 'ppr_activated', 'ppr_perform_type', 'ppr_amendment_allowed', 'ppr_votes_counting_method', 'ppr_version'],
            order: [
                ['ppr_name', 'ASC'],
                ['ppr_activated', 'ASC'],
                ['ppr_perform_type', 'ASC'],
                ['ppr_amendment_allowed', 'ASC'],
                ['ppr_votes_counting_method', 'ASC'],
                ['ppr_version', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readPollings() {
        /**
         *     
            SELECT pll_start_date, pll_hash, pll_creator, pll_type, pll_class, pll_ref, pll_ref_type, pll_ref_class,  
            pll_longevity, pll_version, md5(pll_comment) _comment, pll_y_count, pll_y_shares, pll_y_gain, pll_y_value,
            pll_n_count, pll_n_shares, pll_n_gain, pll_n_value, pll_a_count, pll_a_shares, pll_a_gain, pll_a_value,
            pll_status, pll_ct_done   
            FROM i_pollings 
            ORDER BY pll_start_date, pll_hash, pll_creator, pll_type, pll_class, pll_ref, pll_ref_type, pll_ref_class,  
            pll_longevity, pll_version, md5(pll_comment), pll_y_count, pll_y_shares, pll_y_gain, pll_y_value,
            pll_n_count, pll_n_shares, pll_n_gain, pll_n_value, pll_a_count, pll_a_shares, pll_a_gain, pll_a_value,
            pll_status, pll_ct_done;
         */
        let records = await model.aRead({
            table: 'i_pollings',
            fields: ['pll_start_date', 'pll_hash', 'pll_creator', 'pll_type', 'pll_class', 'pll_ref',
                'pll_ref_type', 'pll_ref_class', 'pll_longevity', 'pll_version', 'md5(pll_comment) _comment',
                'pll_y_count', 'pll_y_shares', 'pll_y_gain', // 'pll_y_value',
                'pll_n_count', 'pll_n_shares', 'pll_n_gain', 'pll_n_value',
                'pll_a_count', 'pll_a_shares', 'pll_a_gain', 'pll_a_value',
                'pll_status', 'pll_ct_done'],
            order: [
                ['pll_start_date', 'ASC'],
                ['pll_hash', 'ASC'],
                ['pll_creator', 'ASC'],
                ['pll_type', 'ASC'],
                ['pll_class', 'ASC'],
                ['pll_ref', 'ASC'],
                ['pll_ref_type', 'ASC'],
                ['pll_ref_class', 'ASC'],
                ['pll_longevity', 'ASC'],
                ['pll_version', 'ASC'],
                ['md5(pll_comment)', 'ASC'],
                ['pll_y_count', 'ASC'],
                ['pll_y_shares', 'ASC'],
                ['pll_y_gain', 'ASC'],
                // ['pll_y_value', 'ASC'],
                ['pll_n_count', 'ASC'],
                ['pll_n_shares', 'ASC'],
                ['pll_n_gain', 'ASC'],
                ['pll_n_value', 'ASC'],
                ['pll_a_count', 'ASC'],
                ['pll_a_shares', 'ASC'],
                ['pll_a_gain', 'ASC'],
                ['pll_a_value', 'ASC'],
                ['pll_status', 'ASC'],
                ['pll_ct_done', 'ASC'],
            ]
        });
        return { err: false, records };
    }

    static async readBallots() {
        /**
         *     
        SELECT ba_hash, ba_pll_hash, ba_voter, ba_voter_shares, ba_vote, ba_creation_date, ba_vote_c_diff, ba_receive_date, ba_vote_r_diff    
        FROM i_ballots ORDER BY ba_hash, ba_pll_hash, ba_voter, ba_voter_shares, ba_vote, ba_creation_date, ba_vote_c_diff, ba_receive_date, ba_vote_r_diff;

         */
        let records = await model.aRead({
            table: 'i_ballots',
            fields: ['ba_hash', 'ba_pll_hash', 'ba_voter', 'ba_voter_shares', 'ba_vote', 'ba_creation_date',
                'ba_vote_c_diff', 'ba_receive_date', 'ba_vote_r_diff'],
            order: [
                ['ba_hash', 'ASC'],
                ['ba_pll_hash', 'ASC'],
                ['ba_voter', 'ASC'],
                ['ba_voter_shares', 'ASC'],
                ['ba_vote', 'ASC'],
                ['ba_creation_date', 'ASC'],
                ['ba_vote_c_diff', 'ASC'],
                ['ba_receive_date', 'ASC'],
                ['ba_vote_r_diff', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readDNA() {
        /**
         *     
            SELECT dn_creation_date, dn_project_hash, dn_shareholder, dn_doc_hash, dn_help_hours, dn_help_level, dn_shares,
            dn_votes_y, dn_votes_a, dn_votes_n, md5(dn_title) as _title, md5(dn_descriptions) as _descriptions, md5(dn_tags) as _tags 
            FROM i_dna_shares 
            ORDER BY dn_creation_date, dn_project_hash, dn_shareholder, dn_doc_hash, dn_help_hours, dn_help_level, dn_shares,
            dn_votes_y, dn_votes_a, dn_votes_n, md5(dn_title), md5(dn_descriptions), md5(dn_tags) ;
         */
        let records = await model.aRead({
            table: 'i_dna_shares',
            fields: ['dn_creation_date', 'dn_project_hash', 'dn_shareholder', 'dn_doc_hash', 'dn_help_hours',
                'dn_help_level', 'dn_votes_y', 'dn_votes_a', 'dn_votes_n', 'md5(dn_title) _title',
                'md5(dn_descriptions) _descriptions', 'md5(dn_tags) _tags'],
            order: [
                ['dn_creation_date', 'ASC'],
                ['dn_project_hash', 'ASC'],
                ['dn_shareholder', 'ASC'],
                ['dn_doc_hash', 'ASC'],
                ['dn_help_hours', 'ASC'],
                ['dn_help_level', 'ASC'],
                ['dn_votes_y', 'ASC'],
                ['dn_votes_a', 'ASC'],
                ['dn_votes_n', 'ASC'],
                ['md5(dn_title)', 'ASC'],
                ['md5(dn_descriptions)', 'ASC'],
                ['md5(dn_tags)', 'ASC'],
            ]
        });
        return { err: false, records };
    }

    static async readTreasury() {
        /**
         *     
            SELECT tr_ref_loc, tr_block_hash, tr_creation_date, tr_cat, tr_value, tr_title  
            FROM i_treasury ORDER BY tr_ref_loc, tr_block_hash, tr_creation_date, tr_cat, tr_value, tr_title;
         */
        let records = await model.aRead({
            table: 'i_treasury',
            fields: ['tr_ref_loc', 'tr_block_hash', 'tr_creation_date', 'tr_cat', 'tr_value',
                'tr_title'],
            order: [
                ['tr_ref_loc', 'ASC'],
                ['tr_block_hash', 'ASC'],
                ['tr_creation_date', 'ASC'],
                ['tr_cat', 'ASC'],
                ['tr_value', 'ASC'],
                ['tr_title', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readTrxSpend() {
        /**
         *     
            SELECT sp_coin, sp_spend_loc, sp_spend_date FROM i_trx_spend 
            ORDER BY sp_coin, sp_spend_loc, sp_spend_date;
         */
        let records = await model.aRead({
            table: 'i_trx_spend',
            fields: ['sp_coin', 'sp_spend_loc', 'sp_spend_date'],
            order: [
                ['sp_coin', 'ASC'],
                ['sp_spend_loc', 'ASC'],
                ['sp_spend_date', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readTrxUTXOs() {
        /**
         *     
            SELECT DISTINCT ut_coin, ut_ref_creation_date, ut_o_address, ut_o_value 
            FROM i_trx_utxos
            ORDER BY ut_ref_creation_date, ut_coin, ut_o_address, ut_o_value; 
         */
        let records = await model.aRead({
            table: 'i_trx_utxos',
            fields: ['DISTINCT ut_coin', 'ut_ref_creation_date', 'ut_o_address', 'ut_o_value'],
            order: [
                ['ut_ref_creation_date', 'ASC'],
                ['ut_coin', 'ASC'],
                ['ut_o_address', 'ASC'],
                ['ut_o_value', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readPledgedAccounts() {
        /**
         *     
            SELECT pgd_hash, pgd_type, pgd_class, pgd_version, pgd_pledger_sign_date, pgd_pledgee_sign_date, 
            pgd_arbiter_sign_date, pgd_activate_date, pgd_close_date, pgd_pledger, pgd_pledgee, pgd_arbiter, 
            pgd_principal, pgd_annual_interest, pgd_repayment_offset, pgd_repayment_amount, 
            pgd_repayment_schedule, pgd_status 
            FROM i_pledged_accounts 
            ORDER BY pgd_hash, pgd_type, pgd_class, pgd_version, pgd_pledger_sign_date, pgd_pledgee_sign_date, 
            pgd_arbiter_sign_date, pgd_activate_date, pgd_close_date, pgd_pledger, pgd_pledgee, pgd_arbiter, 
            pgd_principal, pgd_annual_interest, pgd_repayment_offset, pgd_repayment_amount, 
            pgd_repayment_schedule, pgd_status;
         */
        let records = await model.aRead({
            table: 'i_pledged_accounts',
            fields: ['pgd_hash', 'pgd_type', 'pgd_class', 'pgd_version',
                'pgd_pledger_sign_date', 'pgd_pledgee_sign_date', 'pgd_arbiter_sign_date',
                'pgd_activate_date', 'pgd_close_date',
                'pgd_pledger', 'pgd_pledgee', 'pgd_arbiter',
                'pgd_principal', 'pgd_annual_interest', 'pgd_repayment_offset',
                'pgd_repayment_amount', 'pgd_repayment_schedule', 'pgd_status'],
            order: [
                ['pgd_hash', 'ASC'],
                ['pgd_type', 'ASC'],
                ['pgd_class', 'ASC'],
                ['pgd_version', 'ASC'],
                ['pgd_pledger_sign_date', 'ASC'],
                ['pgd_pledgee_sign_date', 'ASC'],
                ['pgd_arbiter_sign_date', 'ASC'],
                ['pgd_activate_date', 'ASC'],
                ['pgd_close_date', 'ASC'],
                ['pgd_pledger', 'ASC'],
                ['pgd_pledgee', 'ASC'],
                ['pgd_arbiter', 'ASC'],
                ['pgd_principal', 'ASC'],
                ['pgd_annual_interest', 'ASC'],
                ['pgd_repayment_offset', 'ASC'],
                ['pgd_repayment_amount', 'ASC'],
                ['pgd_repayment_schedule', 'ASC'],
                ['pgd_status', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readBlockExtInfos() {
        /**
         *     
            SELECT x_creation_date, x_block_hash, md5(x_detail) FROM i_block_extinfos 
            ORDER BY x_creation_date, x_block_hash;
         */
        let records = await model.aRead({
            table: 'i_block_extinfos',
            fields: ['x_creation_date', 'x_block_hash', 'md5(x_detail) AS _detail'],
            order: [
                ['x_creation_date', 'ASC'],
                ['x_block_hash', 'ASC'],
                ['md5(x_detail)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readSignals() {
        /**
         *     
            SELECT sig_creation_date, sig_block_hash, sig_signaler, sig_key, sig_value FROM i_signals 
            ORDER BY sig_creation_date, sig_block_hash, sig_signaler, sig_key, sig_value;
         */
        let records = await model.aRead({
            table: 'i_signals',
            fields: ['sig_creation_date', 'sig_block_hash', 'sig_signaler', 'sig_key', 'sig_value'],
            order: [
                ['sig_creation_date', 'ASC'],
                ['sig_block_hash', 'ASC'],
                ['sig_signaler', 'ASC'],
                ['sig_key', 'ASC'],
                ['sig_value', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readDemosAgoras() {
        /**
         *     
            SELECT ag_parent, ag_in_hash, ag_hash, ag_unique_hash, ag_lang, md5(ag_title) _title, 
            ag_doc_hash, ag_creation_date, ag_creator 
            FROM i_demos_agoras 
            ORDER BY ag_parent, ag_in_hash, ag_hash, ag_unique_hash, ag_lang, md5(ag_title), 
            ag_doc_hash, ag_creation_date, ag_creator;
         */
        let records = await model.aRead({
            table: 'i_demos_agoras',
            fields: ['ag_parent', 'ag_in_hash', 'ag_hash', 'ag_unique_hash', 'ag_lang', 'md5(ag_title) _title'],
            order: [
                ['ag_parent', 'ASC'],
                ['ag_in_hash', 'ASC'],
                ['ag_hash', 'ASC'],
                ['ag_unique_hash', 'ASC'],
                ['ag_lang', 'ASC'],
                ['md5(ag_title)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readDemosPosts() {
        /**
         *     
            SELECT ap_creation_date, ap_ag_unique_hash, ap_doc_hash, ap_creator, 
            ap_reply, ap_reply_point, ap_format_version, md5(ap_opinion), md5(ap_attrs) FROM i_demos_posts 
            ORDER BY ap_creation_date, ap_ag_unique_hash, ap_doc_hash, ap_creator, 
            ap_reply, ap_reply_point, ap_format_version, md5(ap_opinion), md5(ap_attrs);
         */
        let records = await model.aRead({
            table: 'i_demos_posts',
            fields: [
                'ap_creation_date', 'ap_ag_unique_hash', 'ap_doc_hash', 'ap_creator', 'ap_reply', 'ap_reply_point',
                'ap_format_version', 'md5(ap_opinion) _opinion', 'md5(ap_attrs) _attrs'
            ],
            order: [
                ['ap_creation_date', 'ASC'],
                ['ap_ag_unique_hash', 'ASC'],
                ['ap_doc_hash', 'ASC'],
                ['ap_creator', 'ASC'],
                ['ap_reply', 'ASC'],
                ['ap_reply_point', 'ASC'],
                ['ap_format_version', 'ASC'],
                ['md5(ap_opinion)', 'ASC'],
                ['md5(ap_attrs)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readWikiPages() {
        /**
         *     
            SELECT wkp_creation_date, wkp_unique_hash, wkp_in_hash, wkp_hash, wkp_doc_hash, wkp_lang, 
            wkp_format_version, wkp_last_modified, wkp_creator, md5(wkp_title) _title 
            FROM i_wiki_pages 
            ORDER BY wkp_creation_date, wkp_unique_hash, wkp_in_hash, wkp_hash, wkp_doc_hash, wkp_lang, 
            wkp_format_version, wkp_last_modified, wkp_creator, md5(wkp_title);
         */
        let records = await model.aRead({
            table: 'i_wiki_pages',
            fields: [
                'wkp_creation_date', 'wkp_unique_hash', 'wkp_in_hash', 'wkp_hash', 'wkp_doc_hash', 'wkp_lang',
                'wkp_format_version', 'wkp_creator', 'md5(wkp_title) _title'
            ],
            order: [
                ['wkp_creation_date', 'ASC'],
                ['wkp_unique_hash', 'ASC'],
                ['wkp_in_hash', 'ASC'],
                ['wkp_hash', 'ASC'],
                ['wkp_doc_hash', 'ASC'],
                ['wkp_lang', 'ASC'],
                ['wkp_format_version', 'ASC'],
                ['wkp_creator', 'ASC'],
                ['md5(wkp_title)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readWikiContents() {
        /**
         *     
            SELECT wkc_unique_hash, md5(wkc_content) AS _content 
            FROM i_wiki_contents ORDER BY wkc_unique_hash, md5(wkc_content);
         */
        let records = await model.aRead({
            table: 'i_wiki_contents',
            fields: ['wkc_unique_hash', 'md5(wkc_content) AS _content'],
            order: [
                ['wkc_unique_hash', 'ASC'],
                ['md5(wkc_content)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readINameRecords() {
        /**
         *     
            SELECT in_register_date, in_hash, in_name, in_owner, in_doc_hash, in_is_settled 
            FROM i_iname_records 
            ORDER BY in_register_date, in_hash, in_name, in_owner, in_doc_hash, in_is_settled;
         */
        let records = await model.aRead({
            table: 'i_iname_records',
            fields: ['in_register_date', 'in_hash', 'in_name', 'in_owner', 'in_doc_hash', 'in_is_settled'],
            order: [
                ['in_register_date', 'ASC'],
                ['in_hash', 'ASC'],
                ['in_name', 'ASC'],
                ['in_owner', 'ASC'],
                ['in_doc_hash', 'ASC'],
                ['in_is_settled', 'ASC'],
            ]
        });
        return { err: false, records };
    }

    static async readINameBindings() {
        /**
         *     
            SELECT nb_creation_date, nb_in_hash, nb_doc_hash, nb_bind_type, md5(nb_conf_info) AS _conf_info, 
            md5(nb_title) AS _title, md5(nb_comment) AS _comment, nb_status 
            FROM i_iname_bindings 
            ORDER BY nb_creation_date, nb_in_hash, nb_doc_hash, nb_bind_type, md5(nb_conf_info), 
            md5(nb_title), md5(nb_comment), nb_status;
         */
        let records = await model.aRead({
            table: 'i_iname_bindings',
            fields: [
                'nb_creation_date', 'nb_in_hash', 'nb_doc_hash', 'nb_bind_type', 'md5(nb_conf_info) AS _conf_info',
                'md5(nb_title) AS _title', 'md5(nb_comment) AS _comment', 'nb_status'],
            order: [
                ['nb_creation_date', 'ASC'],
                ['nb_in_hash', 'ASC'],
                ['nb_doc_hash', 'ASC'],
                ['nb_bind_type', 'ASC'],
                ['md5(nb_conf_info)', 'ASC'],
                ['md5(nb_title)', 'ASC'],
                ['md5(nb_comment)', 'ASC'],
                ['nb_status', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readAdmPollings() {
        /**
         *     
            SELECT apr_creation_date, apr_conclude_date, apr_approved, apr_hash, apr_creator, apr_subject, 
            md5(apr_values) AS _values, md5(apr_comment) AS _comment, md5(apr_conclude_info) AS _conclude_info
            FROM i_administrative_pollings 
            ORDER BY apr_creation_date, apr_conclude_date, apr_approved, apr_hash, apr_creator, apr_subject, 
            md5(apr_values), md5(apr_comment), md5(apr_conclude_info);
         */
        let records = await model.aRead({
            table: 'i_administrative_pollings',
            fields: [
                'apr_creation_date', 'apr_conclude_date', 'apr_approved', 'apr_hash', 'apr_creator', 'apr_subject',
                'md5(apr_values) AS _values',
                'md5(apr_comment) AS _comment', 'md5(apr_conclude_info) AS _conclude_info'],
            order: [
                ['apr_creation_date', 'ASC'],
                ['apr_conclude_date', 'ASC'],
                ['apr_approved', 'ASC'],
                ['apr_hash', 'ASC'],
                ['apr_creator', 'ASC'],
                ['apr_subject', 'ASC'],
                ['md5(apr_values)', 'ASC'],
                ['md5(apr_comment)', 'ASC'],
                ['md5(apr_conclude_info)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readAdmRefinesHist() {
        /**
         *     
            SELECT arh_apply_date, arh_hash, arh_subject, md5(arh_value) AS _value 
            FROM i_administrative_refines_history 
            ORDER BY arh_apply_date, arh_hash, arh_subject, md5(arh_value);
         */
        let records = await model.aRead({
            table: 'i_administrative_refines_history',
            fields: [
                'arh_apply_date', 'arh_hash', 'arh_subject', 'md5(arh_value) AS _value'],
            order: [
                ['arh_apply_date', 'ASC'],
                ['arh_hash', 'ASC'],
                ['arh_subject', 'ASC'],
                ['md5(arh_value)', 'ASC']
            ]
        });
        return { err: false, records };
    }

    static async readAdministrativeCurrentValues() {
        /**
         * instead of fetching from a single table, tha values are retrived from proper functions in ConfParamsHandler
         */
        const cnfHandler = require('../../config/conf-params');
        let cDate = utils.getNow();
        let records = [{
            cycleStartDate: iutils.getACycleRange().minCreationDate,
            getTransactionMinimumFee: cnfHandler.getTransactionMinimumFee({ cDate }),
            prepareDocExpenseDict: cnfHandler.prepareDocExpenseDict({ cDate, dLen: iConsts.TRANSACTION_MINIMUM_LENGTH }),
            getBasePricePerChar: cnfHandler.getBasePricePerChar({ cDate }),
            getBlockFixCost: cnfHandler.getBlockFixCost({ cDate }),

            getMinShareToAllowedIssueFVote: cnfHandler.getMinShareToAllowedIssueFVote({ cDate }),
            getMinShareToAllowedVoting: cnfHandler.getMinShareToAllowedVoting({ cDate }),
            getMinShareToAllowedSignCoinbase: cnfHandler.getMinShareToAllowedSignCoinbase({ cDate }),
        }];

        return { err: false, records };
    }

    static async readSusTrxs() {
        /**
         *     
            SELECT st_voter, st_vote_date, st_coin, st_logger_block, st_spender_block, st_spender_doc,  
            st_receive_order, st_spend_date 
            FROM i_trx_suspect_transactions 
            ORDER BY st_voter, st_vote_date, st_coin, st_logger_block, st_spender_block, st_spender_doc,  
            st_receive_order, st_spend_date;
         */
        let records = await model.aRead({
            table: 'i_trx_suspect_transactions',
            fields: [
                'st_voter', 'st_vote_date', 'st_coin', 'st_logger_block', 'st_spender_block',
                'st_spender_doc', 'st_receive_order', 'st_spend_date',
                `md5(st_voter||st_vote_date||st_coin||st_logger_block||st_spender_block||st_spender_doc||st_receive_order||st_spend_date) AS _uniq_hash `
            ],
            order: [
                ['st_voter', 'ASC'],
                ['st_vote_date', 'ASC'],
                ['st_coin', 'ASC'],
                ['st_logger_block', 'ASC'],
                ['st_spender_block', 'ASC'],
                ['st_spender_doc', 'ASC'],
                ['st_receive_order', 'ASC'],
                ['st_spend_date', 'ASC'],
            ]
        });
        return { err: false, records };
    }

    static async readRejectedTrxs() {
        /**
         *     
            SELECT rt_block_hash, rt_doc_hash, rt_coin, rt_insert_date 
            FROM i_trx_rejected_transactions 
            ORDER BY rt_block_hash, rt_doc_hash, rt_coin, rt_insert_date;
         */
        let records = await model.aRead({
            table: 'i_trx_rejected_transactions',
            fields: [
                'rt_block_hash', 'rt_doc_hash', 'rt_coin', `md5(rt_block_hash||rt_doc_hash||rt_coin) AS _uniq_hash `],
            order: [
                ['rt_block_hash', 'ASC'],
                ['rt_doc_hash', 'ASC'],
                ['rt_coin', 'ASC']
            ]
        });
        return { err: false, records };
    }



}

module.exports = ScreenshotGenerator
