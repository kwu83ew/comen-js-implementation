const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const MESSAGE_TYPES = require('../../messaging-protocol/message-types')
const listener = require('../../plugin-handler/plugin-handler');
const crypto = require('../../crypto/crypto');
const pollHandler = require('../../services/polling-handler/general-poll-handler');

const table = 'i_nodes_screenshots';

VALIDITY_CONTROL_LEVEL = {
    STRICT: 'strict',
    ACCEPTABLE: 'acceptable',
    ROUGH: 'rough',

}

class CompareHandler {

    static async compareScreenShot(args) {
        console.log(`compare ScreenShot args ${utils.stringify(args)}`);

        // refresh pollings status
        await pollHandler.maybeUpdateOpenPollingsStat();

        let nss_id = args.nss_id
        let compareLevel = _.has(args, 'compareLevel') ? args.compareLevel : VALIDITY_CONTROL_LEVEL.STRICT;
        compareLevel = VALIDITY_CONTROL_LEVEL.ACCEPTABLE; //later remove it

        let remoteReport = await model.aRead({
            table,
            query: [['nss_id', nss_id]]
        });
        remoteReport = remoteReport[0].nss_content;
        remoteReport = utils.parse(remoteReport);
        let remoteKeys = utils.objKeys(remoteReport);

        let localReport = await CompareHandler._super.screenshotGenerator.generateNodeScreenShot();
        localReport = localReport.finalReport;
        let localKeys = utils.objKeys(localReport);
        console.log(`remote has ${remoteKeys.length} keys == local has ${localKeys.length} keys`);
        console.log(`remote ${remoteKeys} \nlocal ${localKeys}`);

        let keyDif = utils.arrayDiff(localKeys, remoteKeys);
        if (keyDif > 0)
            return { err: true, msg: `discrepency in tables count` }
        keyDif = utils.arrayDiff(remoteKeys, localKeys);
        if (keyDif > 0)
            return { err: true, msg: `discrepency in tables count` }


        CompareHandler.nodesDiscreps = [];
        CompareHandler.areChecked = [];
        // comparing all tables of locl with a given remote
        for (let aTable of localKeys) {
            let localData = _.has(localReport, aTable) ? localReport[aTable] : null;
            let remoteData = _.has(remoteReport, aTable) ? remoteReport[aTable] : null;
            if (localData == null) {
                CompareHandler.nodesDiscreps.push({
                    msg: `Missed table(${aTable}) info for local`
                });
                continue;
            }

            if (remoteData == null) {
                CompareHandler.nodesDiscreps.push({
                    msg: `Missed table(${aTable}) info for remote`
                });
                continue;
            }

            if (localData.length != remoteData.length)
                CompareHandler.nodesDiscreps.push({
                    msg: `table elements not matched: ${aTable} local(${localData.length}) remote(${remoteData.length})`
                });

            let dummyJumpTests = false;
            switch (aTable) {

                case 'i_blocks':
                    this.compare_Blocks({ localData, remoteData, compareLevel });
                    break;

                case 'i_docs_blocks_map':
                    this.compare_BlocksDocsMap({ localData, remoteData, compareLevel });
                    break;

                case 'i_block_extinfos':
                    this.compare_bExtInfos({ localData, remoteData, compareLevel });
                    break;

                case 'i_proposals':
                    if (!dummyJumpTests)
                        this.compare_proposals({ localData, remoteData, compareLevel });
                    break;

                case 'i_polling_profiles':
                    this.compare_pollingProfiles({ localData, remoteData, compareLevel });
                    break;

                case 'i_pollings':
                    this.compare_pollings({ localData, remoteData, compareLevel });
                    break;

                case 'i_ballots':
                    this.compare_ballots({ localData, remoteData, compareLevel });
                    break;

                case 'i_dna_shares':
                    if (!dummyJumpTests)
                        this.compare_DNAShares({ localData, remoteData, compareLevel });
                    break;

                case 'i_treasury':
                    this.compare_treasury({ localData, remoteData, compareLevel });
                    break;

                case 'i_trx_spend':
                    this.compare_trxSpend({ localData, remoteData, compareLevel });
                    break;

                case 'i_trx_utxos':
                    if (!dummyJumpTests)
                        this.compare_trxUTXOs({ localData, remoteData, compareLevel });
                    break;

                case 'i_pledged_accounts':
                    this.compare_i_pledgedAccounts({ localData, remoteData, compareLevel });
                    break;

                case 'i_signals':
                    this.compare_signals({ localData, remoteData, compareLevel });
                    break;

                case 'i_demos_agoras':
                    this.compare_demosAgoras({ localData, remoteData, compareLevel });
                    break;

                case 'i_demos_posts':
                    if (!dummyJumpTests)
                        this.compare_demosPosts({ localData, remoteData, compareLevel });
                    break;

                case 'i_wiki_pages':
                    if (!dummyJumpTests)
                        this.compare_wikiPages({ localData, remoteData, compareLevel });
                    break;

                case 'i_wiki_contents':
                    if (!dummyJumpTests)
                        this.compare_wikiContents({ localData, remoteData, compareLevel });
                    break;

                case 'i_iname_records':
                    this.compare_iNameRecords({ localData, remoteData, compareLevel });
                    break;

                case 'i_iname_bindings':
                    this.compare_iNameBindings({ localData, remoteData, compareLevel });
                    break;

                case 'i_administrative_pollings':
                    this.compare_administrativePollings({ localData, remoteData, compareLevel });
                    break;

                case 'i_administrative_refines_history':
                    this.compare_administrativeRefinesHist({ localData, remoteData, compareLevel });
                    break;

                case 'administrativeCurrentValues':
                    this.compare_administrativeCurrentValues({ localData, remoteData, compareLevel });
                    break;

                case 'i_trx_suspect_transactions':
                    this.compare_suspectTransactions({ localData, remoteData, compareLevel });
                    break;

                case 'i_trx_rejected_transactions':
                    this.compare_rejectedTransactions({ localData, remoteData, compareLevel });
                    break;

            }
        }
        console.log(`\n\nYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY\n${CompareHandler.areChecked.join(`\n`)}\n\n\n`);


        if (CompareHandler.nodesDiscreps.length > 0) {
            // print report on console
            let consoleMsg = CompareHandler.nodesDiscreps.map(x => x.msg).join(`\n`);
            console.log(`\n\n=======================================\n${consoleMsg}\n\n\n`);

            return { err: true, msg: `general discrepency`, nodesDiscreps: CompareHandler.nodesDiscreps }
        }
        return { err: false }
    }

    static compare_Blocks(args) {
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;

        function makeObj(aRecord) {
            return {
                b_hash: aRecord.b_hash,
                b_creation_date: aRecord.b_creation_date,
                b_ancestors: aRecord.b_ancestors,
                b_type: aRecord.b_type,
                b_cycle: aRecord.b_cycle,
                b_confidence: aRecord.b_confidence,
                b_docs_root_hash: aRecord.b_docs_root_hash,
                b_ext_root_hash: aRecord.b_ext_root_hash,
                b_utxo_imported: aRecord.b_utxo_imported
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Blocks';
        args.hashField = 'b_hash';
        args.testStepNumber = '1';
        let genCompareRes = this.doGeneralComapres(args);

        // let localHashes = [];
        // let localDict = [];
        // for (let aLocal of localData) {
        //     localHashes.push(aLocal.b_hash)
        //     localDict[aLocal.b_hash] = {
        //         b_creation_date: aLocal.b_creation_date,
        //         b_ancestors: aLocal.b_ancestors,
        //         // b_backer: aLocal.b_backer,   backers for the same RpBlocks are different
        //         b_type: aLocal.b_type,
        //         b_cycle: aLocal.b_cycle,
        //         b_confidence: aLocal.b_confidence,
        //         b_docs_root_hash: aLocal.b_docs_root_hash,
        //         b_ext_root_hash: aLocal.b_ext_root_hash,
        //         b_utxo_imported: aLocal.b_utxo_imported
        //     }
        //     localDict[aLocal.b_hash]['recordHash'] = crypto.sha256(utils.stringify(localDict[aLocal.b_hash]));
        // }

        // let remoteHashes = [];
        // let remoteDict = [];
        // for (let aRemote of remoteData) {
        //     remoteHashes.push(aRemote.b_hash)
        //     remoteDict[aRemote.b_hash] = {
        //         b_creation_date: aRemote.b_creation_date,
        //         b_ancestors: aRemote.b_ancestors,
        //         // b_backer: aRemote.b_backer,
        //         b_type: aRemote.b_type,
        //         b_cycle: aRemote.b_cycle,
        //         b_confidence: aRemote.b_confidence,
        //         b_docs_root_hash: aRemote.b_docs_root_hash,
        //         b_ext_root_hash: aRemote.b_ext_root_hash,
        //         b_utxo_imported: aRemote.b_utxo_imported
        //     }
        //     remoteDict[aRemote.b_hash]['recordHash'] = crypto.sha256(utils.stringify(remoteDict[aRemote.b_hash]));
        // }


        // let localMissedRecords = utils.arrayDiff(remoteHashes, localHashes)
        // if (localMissedRecords.length > 0)
        //     CompareHandler.nodesDiscreps.push({
        //         msg: `local missed blocks: (${localMissedRecords.map(x => utils.hash6c(x))})`
        //     });

        // let localHasMoreRecords = utils.arrayDiff(localHashes, remoteHashes)
        // if (localHasMoreRecords.length > 0)
        //     CompareHandler.nodesDiscreps.push({
        //         msg: `local extra blocks: (${localHasMoreRecords.map(x => utils.hash6c(x))})`
        //     });

        // for (let aHash of localHashes) {
        //     if (_.has(localDict, aHash) && _.has(remoteDict, aHash)) {
        //         if (localDict[aHash].recordHash != remoteDict[aHash].recordHash)
        //             CompareHandler.nodesDiscreps.push({
        //                 msg: `discrepency block content for \n\tlocal: ${utils.stringify(localDict[aHash])} \n\tremote: ${utils.stringify(remoteDict[aHash])} `
        //             });
        //     }
        // }

        return;

    }

    static compare_BlocksDocsMap(args) {
        function makeObj(aRecord) {
            return {
                dbm_block_hash: aRecord.dbm_block_hash,
                dbm_doc_index: aRecord.dbm_doc_index,
                dbm_doc_hash: aRecord.dbm_doc_hash
            }
        }
        args.keyRef = 'Docs map';
        args.hashField = 'dbm_block_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '2';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_bExtInfos(args) {
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;

        function makeObj(aRecord) {
            return {
                x_block_hash: aRecord.x_block_hash,
                x_creation_date: aRecord.x_creation_date,
                _detail: aRecord._detail
            }
        }
        args.keyRef = 'bExtInfos';
        args.hashField = 'x_block_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '3';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }


    static compare_proposals(args) {
        function makeObj(aRecord) {
            return {
                pr_hash: aRecord.pr_hash,
                pr_type: aRecord.pr_type,
                pr_class: aRecord.pr_class,
                pr_version: aRecord.pr_version,
                _title: aRecord._title,
                _description: aRecord._description,
                _tags: aRecord._tags,
                pr_project_id: aRecord.pr_project_id,
                pr_help_hours: aRecord.pr_help_hours,
                pr_help_level: aRecord.pr_help_level,
                pr_voting_longevity: aRecord.pr_voting_longevity,
                pr_polling_profile: aRecord.pr_polling_profile,
                pr_cotributer_account: aRecord.pr_cotributer_account,
                pr_start_voting_date: aRecord.pr_start_voting_date,
                pr_conclude_date: aRecord.pr_conclude_date,
                pr_approved: aRecord.pr_approved
            }
        }

        args.keyRef = 'Proposals';
        args.hashField = 'pr_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '4';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_pollingProfiles(args) {
        function makeObj(aRecord) {
            return {
                ppr_name: aRecord.ppr_name,
                ppr_activated: aRecord.ppr_activated,
                ppr_perform_type: aRecord.ppr_perform_type,
                ppr_amendment_allowed: aRecord.ppr_amendment_allowed,
                ppr_votes_counting_method: aRecord.ppr_votes_counting_method
            }
        }

        args.keyRef = 'Proposal Profiles';
        args.hashField = 'ppr_name';
        args.makeObj = makeObj;
        args.testStepNumber = '5';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_pollings(args) {
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;

        function makeObj(aRecord) {
            return {
                pll_hash: aRecord.pll_hash,
                pll_start_date: aRecord.pll_start_date,
                pll_creator: aRecord.pll_creator,
                pll_type: aRecord.pll_type,
                pll_class: aRecord.pll_class,
                pll_ref: aRecord.pll_ref,
                pll_ref_type: aRecord.pll_ref_type,
                pll_ref_class: aRecord.pll_ref_class,
                pll_longevity: aRecord.pll_longevity,
                pll_version: aRecord.pll_version,
                _comment: aRecord._comment,
                pll_y_count: aRecord.pll_y_count,
                pll_y_shares: aRecord.pll_y_shares,
                pll_y_gain: aRecord.pll_y_gain,
                // pll_y_value: aRecord.pll_y_value,
                pll_n_count: aRecord.pll_n_count,
                pll_n_shares: aRecord.pll_n_shares,
                pll_n_gain: aRecord.pll_n_gain,
                pll_n_value: aRecord.pll_n_value,
                pll_a_count: aRecord.pll_a_count,
                pll_a_count: aRecord.pll_a_count,
                pll_a_shares: aRecord.pll_a_shares,
                pll_a_gain: aRecord.pll_a_gain,
                pll_a_value: aRecord.pll_a_value,
                pll_status: aRecord.pll_status,
                pll_ct_done: aRecord.pll_ct_done,
            }
        }

        args.keyRef = 'Pollings';
        args.hashField = 'pll_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '6';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_ballots(args) {
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;

        function makeObj1(aRecord) {
            return {
                ba_hash: aRecord.ba_hash,
                ba_creation_date: aRecord.ba_creation_date,
                ba_pll_hash: aRecord.ba_pll_hash,
                ba_voter: aRecord.ba_voter,
                ba_voter_shares: aRecord.ba_voter_shares,
                ba_vote: aRecord.ba_vote,
                ba_vote_c_diff: aRecord.ba_vote_c_diff
            }
        }
        args.keyRef = 'Ballots essentials';
        args.hashField = 'ba_hash';
        args.makeObj = makeObj1;
        args.testStepNumber = '7a';
        let genCompareRes = this.doGeneralComapres(args);

        function makeObj2(aRecord) {
            return {
                ba_hash: aRecord.ba_hash,
                ba_creation_date: aRecord.ba_creation_date,
                ba_pll_hash: aRecord.ba_pll_hash,
                ba_voter: aRecord.ba_voter,
                ba_voter_shares: aRecord.ba_voter_shares,
                ba_vote: aRecord.ba_vote,
                ba_vote_c_diff: aRecord.ba_vote_c_diff,
                ba_receive_date: aRecord.ba_receive_date,
                ba_vote_r_diff: aRecord.ba_vote_r_diff
            }
        }
        args.keyRef = 'Ballots Reception time';
        args.hashField = 'ba_hash';
        args.makeObj = makeObj2;
        if (compareLevel == VALIDITY_CONTROL_LEVEL.STRICT) {
            args.testStepNumber = '7b';
            genCompareRes = this.doGeneralComapres(args);
        }
        return;
    }

    static compare_DNAShares(args) {
        function makeObj(aRecord) {
            return {
                dn_doc_hash: aRecord.dn_doc_hash,
                dn_creation_date: aRecord.dn_creation_date,
                dn_project_hash: aRecord.dn_project_hash,
                dn_shareholder: aRecord.dn_shareholder,
                dn_help_hours: aRecord.dn_help_hours,
                dn_help_level: aRecord.dn_help_level,
                dn_votes_y: aRecord.dn_votes_y,
                dn_votes_a: aRecord.dn_votes_a,
                dn_votes_n: aRecord.dn_votes_n,
                _title: aRecord._title,
                _descriptions: aRecord._descriptions,
                _tags: aRecord._tags
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'DNA Shares';
        args.hashField = 'dn_doc_hash';
        args.testStepNumber = '8';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_treasury(args) {
        function makeObj(aRecord) {
            return {
                tr_ref_loc: aRecord.tr_ref_loc,
                tr_block_hash: aRecord.tr_block_hash,
                tr_creation_date: aRecord.tr_creation_date,
                tr_cat: aRecord.tr_cat,
                tr_value: aRecord.tr_value,
                tr_title: aRecord.tr_title
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Treasury';
        args.hashField = 'tr_ref_loc';
        args.testStepNumber = '9';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_trxSpend(args) {
        function makeObj(aRecord) {
            return {
                sp_coin: aRecord.sp_coin,
                sp_spend_loc: aRecord.sp_spend_loc,
                sp_spend_date: aRecord.sp_spend_date
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Trx spend';
        args.hashField = 'sp_coin';
        args.testStepNumber = '10';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_trxUTXOs(args) {
        function makeObj(aRecord) {
            return {
                ut_coin: aRecord.ut_coin,
                ut_ref_creation_date: aRecord.ut_ref_creation_date,
                ut_o_address: aRecord.ut_o_address,
                ut_o_value: aRecord.ut_o_value
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Trx UTXOs';
        args.hashField = 'ut_coin';
        args.shorteningRefs = false;
        args.testStepNumber = '11';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_i_pledgedAccounts(args) {
        function makeObj(aRecord) {
            return {
                pgd_hash: aRecord.pgd_hash,
                pgd_type: aRecord.pgd_type,
                pgd_class: aRecord.pgd_class,
                pgd_version: aRecord.pgd_version,
                pgd_pledger_sign_date: aRecord.pgd_pledger_sign_date,
                pgd_pledgee_sign_date: aRecord.pgd_pledgee_sign_date,
                pgd_arbiter_sign_date: aRecord.pgd_arbiter_sign_date,
                pgd_activate_date: aRecord.pgd_activate_date,
                pgd_close_date: aRecord.pgd_close_date,
                pgd_pledger: aRecord.pgd_pledger,
                pgd_pledgee: aRecord.pgd_pledgee,
                pgd_arbiter: aRecord.pgd_arbiter,
                pgd_principal: aRecord.pgd_principal,
                pgd_annual_interest: aRecord.pgd_annual_interest,
                pgd_repayment_offset: aRecord.pgd_repayment_offset,
                pgd_repayment_amount: aRecord.pgd_repayment_amount,
                pgd_repayment_schedule: aRecord.pgd_repayment_schedule,
                pgd_status: aRecord.pgd_status
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Pledged Accounts';
        args.hashField = 'pgd_hash';
        args.shorteningRefs = false;
        args.testStepNumber = '12';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_signals(args) {
        function makeObj(aRecord) {
            return {
                sig_block_hash: aRecord.sig_block_hash,
                sig_creation_date: aRecord.sig_creation_date,
                sig_signaler: aRecord.sig_signaler,
                sig_key: aRecord.sig_key,
                sig_value: aRecord.sig_value
            }
        }
        args.makeObj = makeObj;
        args.keyRef = 'Signals';
        args.hashField = 'sig_block_hash';
        args.testStepNumber = '13';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_demosAgoras(args) {
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;

        function makeObj(aRecord) {
            return {
                ag_unique_hash: aRecord.ag_unique_hash,
                ag_parent: aRecord.ag_parent,
                ag_in_hash: aRecord.ag_in_hash,
                ag_hash: aRecord.ag_hash,
                ag_lang: aRecord.ag_lang,
                _title: aRecord._title,
                ag_doc_hash: aRecord.ag_doc_hash,
                ag_creation_date: aRecord.ag_creation_date,
                ag_creator: aRecord.ag_creator
            }
        }

        args.keyRef = 'Agoras';
        args.hashField = 'ag_unique_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '14';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_demosPosts(args) {
        function makeObj(aRecord) {
            return {
                ap_ag_unique_hash: aRecord.ap_ag_unique_hash,
                ap_creation_date: aRecord.ap_creation_date,
                ap_doc_hash: aRecord.ap_doc_hash,
                ap_creator: aRecord.ap_creator,
                ap_reply: aRecord.ap_reply,
                ap_reply_point: aRecord.ap_reply_point,
                ap_format_version: aRecord.ap_format_version,
                _opinion: aRecord._opinion,
                _attrs: aRecord._attrs
            }
        }
        args.keyRef = 'Demos Posts';
        args.hashField = 'ap_ag_unique_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '15';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_wikiPages(args) {
        function makeObj(aRecord) {
            return {
                wkp_unique_hash: aRecord.wkp_unique_hash,
                wkp_creation_date: aRecord.wkp_creation_date,
                wkp_in_hash: aRecord.wkp_in_hash,
                wkp_hash: aRecord.wkp_hash,
                wkp_doc_hash: aRecord.wkp_doc_hash,
                wkp_lang: aRecord.wkp_lang,
                wkp_format_version: aRecord.wkp_format_version,
                wkp_creator: aRecord.wkp_creator,
                _title: aRecord._title
            }
        }
        args.keyRef = 'Wiki Pages';
        args.hashField = 'wkp_unique_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '16';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_wikiContents(args) {
        function makeObj(aRecord) {
            return {
                wkc_unique_hash: aRecord.wkc_unique_hash,
                _content: aRecord._content
            }
        }
        args.keyRef = 'Wiki Contents';
        args.hashField = 'wkc_unique_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '17';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_iNameRecords(args) {
        function makeObj(aRecord) {
            return {
                in_hash: aRecord.in_hash,
                in_register_date: aRecord.in_register_date,
                in_name: aRecord.in_name,
                in_owner: aRecord.in_owner,
                in_doc_hash: aRecord.in_doc_hash,
                in_is_settled: aRecord.in_is_settled
            }
        }
        args.keyRef = 'iName Records';
        args.hashField = 'in_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '18';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_iNameBindings(args) {
        function makeObj(aRecord) {
            return {
                nb_doc_hash: aRecord.nb_doc_hash,
                nb_creation_date: aRecord.nb_creation_date,
                nb_in_hash: aRecord.nb_in_hash,
                nb_bind_type: aRecord.nb_bind_type,
                _conf_info: aRecord._conf_info,
                _title: aRecord._title,
                _comment: aRecord._comment,
                nb_status: aRecord.nb_status,
            }
        }
        args.keyRef = 'iName Bindings';
        args.hashField = 'nb_doc_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '19';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_administrativePollings(args) {
        function makeObj(aRecord) {
            return {
                apr_hash: aRecord.apr_hash,
                apr_creation_date: aRecord.apr_creation_date,
                apr_conclude_date: aRecord.apr_conclude_date,
                apr_approved: aRecord.apr_approved,
                apr_creator: aRecord.apr_creator,
                apr_subject: aRecord.apr_subject,
                _values: aRecord._values,
                _comment: aRecord._comment,
                _conclude_info: aRecord._conclude_info,
            }
        }
        args.keyRef = 'Adm Pollings';
        args.hashField = 'apr_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '20';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_administrativeRefinesHist(args) {
        function makeObj(aRecord) {
            return {
                arh_hash: aRecord.arh_hash,
                arh_apply_date: aRecord.arh_apply_date,
                arh_subject: aRecord.arh_subject,
                _value: aRecord._value,
            }
        }
        args.keyRef = 'Adm Refines Hist';
        args.hashField = 'arh_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '21';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_administrativeCurrentValues(args) {
        function makeObj(aRecord) {
            return {
                cycleStartDate: aRecord.cycleStartDate,
                getTransactionMinimumFee: aRecord.getTransactionMinimumFee,
                getBasePricePerChar: aRecord.getBasePricePerChar,
                getBlockFixCost: aRecord.getBlockFixCost,
                getMinShareToAllowedIssueFVote: aRecord.getMinShareToAllowedIssueFVote,
                getMinShareToAllowedVoting: aRecord.getMinShareToAllowedVoting,
                getMinShareToAllowedSignCoinbase: aRecord.getMinShareToAllowedSignCoinbase,
            }
        }
        args.keyRef = 'Adm Params Current Values';
        args.hashField = 'cycleStartDate';
        args.makeObj = makeObj;
        args.testStepNumber = '22';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static doGeneralComapres(args) {
        let testStepNumber = _.has(args, 'testStepNumber') ? args.testStepNumber : 0;
        let localData = args.localData;
        let remoteData = args.remoteData;
        let compareLevel = args.compareLevel;
        let makeObj = args.makeObj;
        let keyRef = args.keyRef;
        let shorteningRefs = _.has(args, 'shorteningRefs') ? args.shorteningRefs : true;
        let hashField = args.hashField;
        let msg = '';

        console.log(`\n\n${keyRef}`);
        let localHashes = [];
        let localDict = [];
        for (let aLocal of localData) {
            localHashes.push(aLocal[hashField])
            localDict[aLocal[hashField]] = makeObj(aLocal);
            localDict[aLocal[hashField]]['recordHash'] = crypto.sha256(utils.stringify(localDict[aLocal[hashField]]));
        }

        let remoteHashes = [];
        let remoteDict = [];
        for (let aRemote of remoteData) {
            remoteHashes.push(aRemote[hashField])
            remoteDict[aRemote[hashField]] = makeObj(aRemote);
            remoteDict[aRemote[hashField]]['recordHash'] = crypto.sha256(utils.stringify(remoteDict[aRemote[hashField]]));
        }

        let hasCountDiscrepency = false;
        let localMissedRecords = utils.arrayDiff(remoteHashes, localHashes)
        if (localMissedRecords.length > 0) {
            hasCountDiscrepency = true;

            if (shorteningRefs) {
                msg = `local missed ${keyRef}: (${utils.arrayUnique(localMissedRecords).map(x => utils.hash16c(x))})`
            } else {
                msg = `local missed ${keyRef}: (${utils.arrayUnique(localMissedRecords)})`
            }
            CompareHandler.nodesDiscreps.push({ msg });
        }

        let localHasMoreRecords = utils.arrayDiff(localHashes, remoteHashes)
        if (localHasMoreRecords.length > 0) {
            hasCountDiscrepency = true;
            if (shorteningRefs) {
                msg = `local extra ${keyRef}: (${utils.arrayUnique(localHasMoreRecords).map(x => utils.hash16c(x))})`
            } else {
                msg = `local extra ${keyRef}: (${utils.arrayUnique(localHasMoreRecords)})`
            }
            CompareHandler.nodesDiscreps.push({ msg });
        }

        if (!hasCountDiscrepency)
            CompareHandler.areChecked.push(`${testStepNumber} ${keyRef} are same in count`);

        let discrepencyInContent = [];
        for (let aHash of localHashes) {
            if (_.has(localDict, aHash) && _.has(remoteDict, aHash)) {
                if (localDict[aHash].recordHash != remoteDict[aHash].recordHash) {
                    discrepencyInContent.push(aHash);
                    if (shorteningRefs) {
                        msg = `${keyRef} discrepency in content for \n\tlocal: ${utils.stringify(localDict[aHash])} \n\tremote: ${utils.stringify(remoteDict[aHash])}`
                    } else {
                        msg = `${keyRef} discrepency in content for \n\tlocal: ${utils.stringify(localDict[aHash])} \n\tremote: ${utils.stringify(remoteDict[aHash])}`
                    }
                    CompareHandler.nodesDiscreps.push({ msg });
                }
            }
        }
        if (discrepencyInContent.length == 0)
            CompareHandler.areChecked.push(`${testStepNumber} ${keyRef} are same in contents`);

        return {
            localMissedRecords: utils.arrayUnique(localMissedRecords),
            localHasMoreRecords: utils.arrayUnique(localHasMoreRecords),
            discrepencyInContent: utils.arrayUnique(discrepencyInContent)
        }
    }

    static compare_suspectTransactions(args) {
        function makeObj(aRecord) {
            return {
                _uniq_hash: aRecord._uniq_hash,
                st_voter: aRecord.st_voter,
                st_vote_date: aRecord.st_vote_date,
                st_coin: aRecord.st_coin,
                st_logger_block: aRecord.st_logger_block,
                st_spender_block: aRecord.st_spender_block,
                st_spender_doc: aRecord.st_spender_doc,
                st_receive_order: aRecord.st_receive_order,
                st_spend_date: aRecord.st_spend_date
            }
        }
        args.keyRef = 'Suspicious Trx';
        args.hashField = '_uniq_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '23';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

    static compare_rejectedTransactions(args) {
        function makeObj(aRecord) {
            return {
                _uniq_hash: aRecord._uniq_hash,
                rt_doc_hash: aRecord.rt_doc_hash,
                rt_coin: aRecord.rt_coin
            }
        }
        args.keyRef = 'Rejected Trx';
        args.hashField = '_uniq_hash';
        args.makeObj = makeObj;
        args.testStepNumber = '24';
        let genCompareRes = this.doGeneralComapres(args);
        return;
    }

}

module.exports = CompareHandler;
