const _ = require('lodash');
const iConsts = require('../../config/constants');
const db = require('../../startup/db2')
const utils = require('../../utils/utils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const MESSAGE_TYPES = require('../../messaging-protocol/message-types')
const listener = require('../../plugin-handler/plugin-handler');
const GQLHandler = require('../../services/graphql/graphql-handler')
const blockUtils = require('../../dag/block-utils');
const fileHandler = require('../../hard-copy/file-handler');
const kvHandler = require('../../models/kvalue');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const machine = require('../../machine/machine-handler');

const table = 'i_nodes_screenshots';

class ScrenShotHandler {

    static async dlNodeScreenShot(args) {
        let dTarget = args.dTarget;
        let dNeighbor = args.dNeighbor;
        return new Promise(async (resolve, reject) => {
            try {

                let repRes = await this.screenshotGenerator.generateNodeScreenShot()


                switch (dTarget) {

                    case 'toHard':
                        // write on Hard Disk(backup-dag folder)
                        let res = fileHandler.packetFileCreateSync({
                            dirName: iConsts.HD_PATHES.hd_backup_dag,
                            fileName: `${utils.getNow()}_nodeScreenShot.txt`,
                            fileContent: utils.stringify(repRes.finalReport)
                        });
                        break;

                    case 'toNeighbor':
                        console.log(`sending screenshot to ${dNeighbor}`);
                        let machineSettings = machine.getMProfileSettingsSync();
                        let machineEmail = machineSettings.pubEmail.address;

                        let { code, body } = GQLHandler.makeAPacket({
                            cards: [
                                {
                                    cdType: GQLHandler.cardTypes.NodeStatusScreenshot,
                                    cdVer: '0.0.1',
                                    creationDate: utils.getNow(),
                                    sender: machineEmail,
                                    finalReport: repRes.finalReport,
                                }
                            ]
                        });

                        let pushRes = sendingQ.pushIntoSendingQ({
                            sqType: iConsts.CONSTS.GQL,
                            sqCode: code,
                            sqPayload: body,
                            sqTitle: `Node screenshot to(${dNeighbor})`,
                            sqReceivers: [dNeighbor]
                        });

                        break;

                }

                return resolve({ err: false });
            } catch (e) {
                return reject(e);

            }
        });
    }



    static async deleteSS(args) {
        console.log(`deleteSS args ${utils.stringify(args)}`);
        let nss_id = args.nss_id
        await model.aDelete({
            table,
            query: [['nss_id', nss_id]]
        });
        return { err: false }
    }

    static async loadScreenshotsList() {
        let records = await model.aRead({
            table,
            order: [['nss_id', 'ASC']]
        })
        return { err: false, records }
    }



    static async pushReportToDB(args) {
        let scOwner = _.has(args, 'scOwner') ? args.scOwner : 'unknown';

        let values = {
            nss_label: `${utils.getNow()}-${scOwner}`,
            nss_content: utils.stringify(args.content),
            nss_creation_date: utils.getNow()
        };
        model.aCreate({
            table,
            values
        });

        return { err: false }
    }

}

ScrenShotHandler.compareHandler = require('./compare-handler');
ScrenShotHandler.compareHandler._super = ScrenShotHandler;

ScrenShotHandler.screenshotGenerator = require('./prepare-screenshot');
ScrenShotHandler.screenshotGenerator._super = ScrenShotHandler;

module.exports = ScrenShotHandler;
