const iConsts = require('../config/constants');
const secp256k1 = require('./secp256k1');
class CryptoHandler {

    static convertToSignMsg(obj) {
        let signMsg = keccak.keccak256(JSON.stringify(obj)).substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static b64Encode(content) {
        let b = Buffer.alloc(content.length, content);
        content = b.toString('base64');
        return content;
    }

    static b64Decode(content) {
        let buffer = Buffer.from(content, "base64");
        content = buffer.toString("utf8");
        return content;
    }
}

CryptoHandler.rand = secp256k1.rand;
CryptoHandler.generatePrivateKey = secp256k1.generatePrivateKey;
CryptoHandler.drivePublicKey = secp256k1.drivePublicKey;
CryptoHandler.signMsg = secp256k1.signMsg;
CryptoHandler.verifySignature = secp256k1.verifySignature;
CryptoHandler.generatePairKey = secp256k1.generatePairKey;
CryptoHandler.encodeWithPubKey = secp256k1.encodeWithPubKey;
CryptoHandler.decodeWithPrvKey = secp256k1.decodeWithPrvKey;

CryptoHandler.sha256 = require('./sha256');

const bech32 = require('./bech32');
CryptoHandler.bech32_isValidAddress = bech32.isValidAddress;
CryptoHandler.bech32_encode = bech32.encode;
CryptoHandler.bech32_decode = bech32.decode;
CryptoHandler.bech32_encodePub = bech32.encodePubKey;
CryptoHandler.bech32_decodeAddress = bech32.decodeAddress;
CryptoHandler.bech32_MAX_LEN_ERR = bech32.bech32_MAX_LEN_ERR;
CryptoHandler.bech32_LOWERCASE_ERR = bech32.bech32_LOWERCASE_ERR;

const keccak = require('./keccak');
CryptoHandler.keccak256 = keccak.keccak256;
CryptoHandler.keccak256Dbl = keccak.keccak256Dbl;

const aes = require('./aes');
CryptoHandler.aesEncrypt = aes.aesEncrypt;
CryptoHandler.aesDecrypt = aes.aesDecrypt;

const native = require('./native-crypto');
CryptoHandler.nativeGenerateKeyPairSync = native.nativeGenerateKeyPairSync
CryptoHandler.nativeSign = native.nativeSign
CryptoHandler.nativeVerifySignature = native.nativeVerifySignature
CryptoHandler.nativeEncryptWithPublicKey = native.nativeEncryptWithPublicKey
CryptoHandler.nativeDecryptWithPrivateKey = native.nativeDecryptWithPrivateKey

const pgp = require('./pgp/interface');
CryptoHandler.encryptPGP = pgp.encryptPGP;
CryptoHandler.decryptPGP = pgp.decryptPGP;
CryptoHandler.wrapPGPEnvelope = pgp.wrapPGPEnvelope;

const merkle = require("./merkle/merkle")
CryptoHandler.merkleNoHash = merkle.noHash;
CryptoHandler.merkleAliasHash = merkle.aliasHash;
CryptoHandler.merkleGenerate = merkle.merkle;
CryptoHandler.merklePresenter = merkle.merklePresenter;
CryptoHandler.merkleGetRootByAProve = merkle.getRootByAProve;

module.exports = CryptoHandler;