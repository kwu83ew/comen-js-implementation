const _ = require('lodash');
const keccak256 = require('../../crypto/keccak').keccak256;
const utils = require('../../utils/utils')

class MerkleHandler {
    static merklePresenter(m) {
        let out = `Root: ${m.root} \nLeaves: ${m.levels} \nLevels: ${m.leaves} \nProofs:`;

        let inx = 0;
        _.forOwn(m.proofs, function (value, key) {
            out += `\n\tleave(${inx}): ${key} => ` + ((utils._notNil(value.lHash)) ? `\n\t\tleftHsh: ${value.lHash}` : ``)
            out += `\n\t\thashes: `
            value.hashes.forEach(element => {
                out += element + ` `
            });
            inx += 1;
        });
        return out;
    }

    static getRootByAProve(leave, proveHashes, lHash, inputType = 'hashed', hashAlgorithm = keccak256) {
        if (inputType == 'string') {
            leave = hashAlgorithm(leave);
        }
        let proof = '';
        if (utils._notNil(lHash)) {
            proof = hashAlgorithm(lHash + leave);
        } else {
            proof = leave;
        }

        let val, pos;
        if (utils._notNil(proveHashes))
            proveHashes.forEach(element => {
                pos = element.substring(0, 1)
                val = element.substring(2)
                if (pos == 'r') {
                    proof = hashAlgorithm(proof + val);
                } else {
                    proof = hashAlgorithm(val + proof);
                }
            });

        return proof;
    }

    static merkle(elms, inputType = 'hashed', hashAlgorithm = keccak256) {
        if (elms.length == 0)
            return {
                root: '',
                levels: 0,
                proofs: null,
                leaves: 0,
                version: "0.0.0"
            }

        if (elms.length == 1)
            return {
                root: (inputType == 'hashed') ? elms[0] : hashAlgorithm(elms[0]),
                levels: 1,
                proofs: null,
                leaves: 1,
                version: "0.0.0"
            }

        let m = MerkleHandler._merkle(elms, inputType, hashAlgorithm);
        let res = {
            root: m.root,
            levels: m.levels,
            leaves: m.leaves,
            proofs: {},
            version: "0.0.0"
        }
        // let proofs;
        _.forOwn(m.proofs, function (value, key) {
            res.proofs[key.substring(2)] = {
                hashes: value.proofKeys,
                lHash: value.lHash
            };
        });
        return res;
    };

    static _merkle(elms, inputType = 'hashed', hashAlgorithm = keccak256) {
        if (inputType == 'string') {
            let hashedElms = []
            elms.forEach(element => {
                hashedElms.push(hashAlgorithm(element))
            });
            elms = hashedElms
        }

        let proofs = {};
        let leaves = elms.length
        let level = 0
        let parent;
        let lKey, rKey;
        let lChild, rChild;
        while (true) {
            level += 1
            if (elms.length == 1)
                return { root: elms[0], levels: level, proofs: proofs, leaves: leaves }

            if (elms.length % 2 == 1) {
                elms.push(elms[elms.length - 1])
            }

            let chunks = _.chunk(elms, 2)
            elms = []
            chunks.forEach(chunk => {
                parent = hashAlgorithm(chunk[0] + chunk[1]);
                elms.push(parent);
                if (level == 1) {
                    lKey = 'l_' + chunk[0].toString();
                    rKey = 'r_' + chunk[1].toString();
                    proofs[lKey] = { proofKeys: [], parent: parent, lHash: null };  // left hash
                    proofs[rKey] = { proofKeys: [], parent: parent, lHash: chunk[0] };
                    proofs[lKey].proofKeys.push('r.' + chunk[1])

                } else {
                    // find alter parent cild
                    _.forOwn(proofs, function (value, key) {
                        if (chunk[0] == value.parent) {
                            proofs[key].proofKeys.push('r.' + chunk[1]);
                            proofs[key].parent = parent;
                        }
                        if (chunk[1] == value.parent) {
                            proofs[key].proofKeys.push('l.' + chunk[0]);
                            proofs[key].parent = parent;
                        }
                    });
                    lChild = _.find(proofs, { parent: chunk[0] })
                }
            });
        }
    }

    static noHash(s) {
        return s
    }

    static aliasHash(s) {
        return 'h(' + s + ')'
    }

}

module.exports = MerkleHandler;