const bech32 = require('bech32'); // https://www.npmjs.com/package/bech32
const utils = require("../utils/utils");
const bech32_MAX_LEN_ERR = 'bech32 must has up to 90 char length';
const bech32_LOWERCASE_ERR = 'bech32 addresses must be in lowercase';
const sha256 = require('./sha256');
const iConsts = require('../config/constants');

//TODO write some unit tests

function encode(input, prefix = 'im') {
    //prefix could be
    // im: imagine main net
    // it: imagine test net(default)
    if (input.length > 90) {
        return { err: bech32_MAX_LEN_ERR, encoded: null };
    }

    let words = bech32.toWords(Buffer.from(input, 'utf8'));
    let enc = bech32.encode(prefix, words);
    return { err: false, input: input, encoded: enc };
};

function isValidAddress(addr) {
    if (addr.toLowerCase() != addr) {
        return false;
    }

    if (addr.length > 90) {
        return false;
    }

    try {
        let dec = bech32.decode(addr);
        if (utils._nilEmptyFalse(dec.prefix) || utils._nilEmptyFalse(dec.words))
            return false;
    } catch (e) {
        console.log(`\n isValidAddress ${utils.stringify(e)}`);
        return false;
    }

    return true;
}

function decode(encoded_str) {

    if (encoded_str.toLowerCase() != encoded_str) {
        return { err: bech32_LOWERCASE_ERR, decoded: null };
    }

    if (encoded_str.length > 90) {
        return { err: bech32_MAX_LEN_ERR, decoded: null };
    }

    try {
        let dec = bech32.decode(encoded_str);
        let decoded = '';
        bech32.fromWords(dec.words).forEach(c => { // TODO: probably there is more intelligent solution to generate decoded string
            decoded += String.fromCharCode(c);
        });
        return { err: false, prefix: dec.prefix, decoded };
    } catch (e) {
        return { err: `bech32 addresses is Invalid ${e}`, decoded: null };
    }
};

function encodePubKey(pubKey) {
    pubKey = iConsts.BECH32_ADDRESS_VER + pubKey;
    pubKey = pubKey.substr(0, iConsts.TRUNCATE_FOR_BECH32_ADDRESS);
    let enc = encode(pubKey, 'im');
    return enc;
};

function decodeAddress(address) {
    let dec = decode(address);
    return dec;
};


module.exports.isValidAddress = isValidAddress;
module.exports.bech32_MAX_LEN_ERR = bech32_MAX_LEN_ERR;
module.exports.bech32_LOWERCASE_ERR = bech32_LOWERCASE_ERR;
module.exports.decode = decode;
module.exports.encode = encode;
module.exports.encodePubKey = encodePubKey;
module.exports.decodeAddress = decodeAddress;