const { randomBytes } = require('crypto');
const secp256k1 = require('secp256k1');
const _ = require('lodash');

rand = (options) => {
    // generate a random number
    let r = randomBytes(32);

    if (options && options.full) {
        // Array of bytes as decimal integers
        return {
            binString: r.toString('binary'),
            hex: r.toString('hex'),
            bytes: r
        };

    } else if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return r;

    } else if (options && options.asBinString) {
        // Binary string
        return r.toString('binary');

    } else {
        // String of hex characters
        return r.toString('hex');

    }

};

generatePrivateKey = (options) => {
    // generate prvKey
    let prvKey;
    do {
        prvKey = rand({ asBytes: true });
    } while (!secp256k1.privateKeyVerify(prvKey));

    if (options && options.full) {
        // Array of bytes as decimal integers
        return {
            binString: prvKey.toString('binary'),
            hex: prvKey.toString('hex'),
            bytes: prvKey
        };

    } else if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return prvKey;

    } else if (options && options.asBinString) {
        // Binary string
        return prvKey.toString('binary');

    } else {
        // String of hex characters
        return prvKey.toString('hex');

    }

};

drivePublicKey = (prvKey, options) => {
    let pubKey = secp256k1.publicKeyCreate(prvKey); // prvKey must be in Bytes Buffer

    if (options && options.full) {
        // Array of bytes as decimal integers
        return {
            binString: pubKey.toString('binary'),
            hex: pubKey.toString('hex'),
            bytes: pubKey
        };

    } else if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return pubKey;

    } else if (options && options.asBinString) {
        // Binary string
        return pubKey.toString('binary');

    } else {
        // String of hex characters
        return pubKey.toString('hex');

    }

};

generatePairKey = (options) => {
    let prvKey;
    do {
        prvKey = rand({ asBytes: true });
    } while (!secp256k1.privateKeyVerify(prvKey));
    let pubKey = secp256k1.publicKeyCreate(prvKey);

    if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return { prvKey: prvKey, pubKey: pubKey };

    } else if (options && options.asBinString) {
        // Binary string
        return { prvKey: prvKey.toString('binary'), pubKey: pubKey.toString('binary') };

    } else {
        // String of hex characters
        return { prvKey: prvKey.toString('hex'), pubKey: pubKey.toString('hex') };

    }
};

signMsg = (msg, prvKey, options = {}) => {

    if (typeof msg === 'string' || msg instanceof String) {
        msg = Buffer.from(msg)
    }

    // TODO apply limitation to msg length
    if (msg.length < 8)
        throw new Error('Too short message to signing');

    if (msg.length > 32)
        throw new Error('Too long message to signing');

    if (typeof prvKey === 'string' || prvKey instanceof String) {
        prvKey = Buffer.from(prvKey, "hex")
    }
    let res = secp256k1.sign(msg, prvKey);

    if (options && options.full) {
        // Array of bytes as decimal integers
        return {
            binString: null,
            hex: res.signature.toString('hex'),
            recovery: res.recovery,
            bytes: res.signature
        };

    } else if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return res.signature;

    } else if (options && options.asBinString) {
        // Binary string
        return null

    } else {
        // String of hex characters
        return res.signature.toString('hex');

    }


};

verifySignature = (msg, signature, pubKey) => {
    signature = _.has(signature, "signature") ? signature.signature : signature;
    // TODO: maybe using recovery?
    if (typeof msg === 'string' || msg instanceof String) {
        msg = Buffer.from(msg)
    }
    if (typeof signature === 'string' || signature instanceof String) {
        signature = Buffer.from(signature, "hex")
    }
    if (typeof pubKey === 'string' || pubKey instanceof String) {
        pubKey = Buffer.from(pubKey, "hex")
    }
    return secp256k1.verify(msg, signature, pubKey);
};

encodeWithPubKey = (msg, pubKey) => {
    // sign the message
    return secp256k1.sign(Buffer.from(msg), Buffer.from(pubKey, "hex"));
};

decodeWithPrvKey = (msg, sigObj, prvKey) => {
    return secp256k1.verify(Buffer.from(msg), sigObj.signature, Buffer.from(prvKey, "hex"));
};

module.exports.rand = rand;
module.exports.generatePrivateKey = generatePrivateKey;
module.exports.drivePublicKey = drivePublicKey;
module.exports.signMsg = signMsg;
module.exports.verifySignature = verifySignature;
module.exports.generatePairKey = generatePairKey;
module.exports.encodeWithPubKey = encodeWithPubKey;
module.exports.decodeWithPrvKey = decodeWithPrvKey;