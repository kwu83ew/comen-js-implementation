const _ = require('lodash');
const iConsts = require('../../config/constants');
const compress = require('../../utils/compressor-adapter/compressor');
const utils = require("../../utils/utils");
const clog = require('../../loggers/console_logger');

function decryptPGP(args = {}) {
    const crypto = require('../crypto');
    let finalDec = {}
    prvKey = args.prvKey
    let senderPubKey = _.has(args, 'senderPubKey') ? args.senderPubKey : null;

    message = args.message.trim()
    if (message.indexOf(iConsts.msgTags.iPGPStartEnvelope) > -1) {
        message = message.split(iConsts.msgTags.iPGPStartEnvelope)[1]
    }
    if (message.indexOf(iConsts.msgTags.iPGPEndEnvelope) > -1) {
        message = message.split(iConsts.msgTags.iPGPEndEnvelope)[0]
    }

    // decode base64
    let buffer = Buffer.from(message, "base64");
    message = buffer.toString("utf8");
    base64Decoded = JSON.parse(message)
    finalDec.isAuthenticated = base64Decoded.isAuthenticated
    finalDec.iPGPVersion = base64Decoded.iPGPVersion
    let encryptedSecretKey = base64Decoded.secretKey;

    message = JSON.parse(base64Decoded.message);

    finalDec.aesVersion = _.has(message, 'aesVersion') ? message.aesVersion : 'Unknown!';

    let AESdecrypted
    if (encryptedSecretKey == iConsts.msgTags.NO_ENCRYPTION) {
        AESdecrypted = message
        finalDec.encryptedSecretKey = encryptedSecretKey

    } else {
        let AESencryptedMsg = message.encrypted
        // decrypt secret key
        let decryptedSecretKey;
        try {
            decryptedSecretKey = crypto.nativeDecryptWithPrivateKey(prvKey, encryptedSecretKey)
            finalDec.decryptedSecretKey = decryptedSecretKey
        } catch (e) {
            console.error(new Error(e))
            finalDec.err = true;
            finalDec.verified = false;
            finalDec.msg = 'wrong prvKey or encryptedSecretKey';
            return finalDec
        }

        // decrypt message body
        try {
            AESdecrypted = crypto.aesDecrypt({
                aesVersion: finalDec.aesVersion,
                secretKey: decryptedSecretKey,
                message: AESencryptedMsg,
            })
            AESdecrypted = JSON.parse(AESdecrypted)
        } catch (e) {
            clog.app.error(new Error(e))
            finalDec.err = true;
            finalDec.verified = false;
            finalDec.msg = 'aesDecrypt failed';
            return finalDec
        }
    }
    finalDec.isCompressed = AESdecrypted.isCompressed;
    finalDec.zipVersion = _.has(AESdecrypted, 'zipVersion') ? AESdecrypted.zipVersion : 'Unknown!';
    finalDec.zipAlgorithm = _.has(AESdecrypted, 'algorithm') ? AESdecrypted.zipAlgorithm : 'Unknown!';

    message = AESdecrypted.message

    // decompress it if it is compressed
    if (finalDec.isCompressed) {
        // decompress message
        message = compress.conventionalDecompress(message)
    }
    message = JSON.parse(message)

    // control signature if is signed
    finalDec.isSigned = message.isSigned
    finalDec.verified = false
    if (finalDec.isSigned) {
        if (utils._nilEmptyFalse(senderPubKey)) {
            finalDec.err = true;
            finalDec.verified = false;
            finalDec.msg = '_nilEmptyFalse pubKey';
            return finalDec;
        }
        finalDec.sigVersion = message.sigVersion
        signature = message.signature
        let hash = crypto.keccak256(message.message);
        finalDec.verified = crypto.nativeVerifySignature(senderPubKey, hash, signature)
    }
    // decode base64
    let buffer2 = Buffer.from(message.message, "base64");
    message = buffer2.toString("utf8");

    finalDec.err = false;
    finalDec.message = message;
    return finalDec;
}
module.exports = decryptPGP;
