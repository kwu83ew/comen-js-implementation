const _ = require('lodash');
const iConsts = require('../../config/constants');
const compress = require('../../utils/compressor-adapter/compressor');
const utils = require('../../utils/utils');
const clog = require('../../loggers/console_logger');

function encryptPGP(args = {}) {
    const crypto = require('../crypto');

    let iPGPVersion = '0.0.1';

    let message = _.has(args, 'message') ? args.message : null;
    if (message == null) {
        message = '';
    }

    let receiverPubKey = _.has(args, 'receiverPubKey') ? args.receiverPubKey : null;

    // base64 encoding
    let b = Buffer.alloc(message.length, message);
    let base64Encoded = b.toString('base64');

    let signature = '';
    let shouldSign = _.has(args, 'shouldSign') ? args.shouldSign : false;
    if (shouldSign) {
        let hash = crypto.keccak256(base64Encoded);
        signature = crypto.nativeSign(args.sendererPrvKey, hash);
    }

    let msg_plus_signature = {
        sigVersion: '0.0.2', // this version denotes to used hashing and signature 
        isSigned: shouldSign,
        message: base64Encoded,
        signature: signature
    };
    msg_plus_signature = JSON.stringify(msg_plus_signature)


    let compressed;
    let shouldCompress = _.has(args, 'shouldCompress') ? args.shouldCompress : false;
    if (shouldCompress) {
        compressed = {
            zipVersion: '0.0.0', // compression algorthm version
            zipAlgorithm: 'zip...',
            isCompressed: true,
            message: compress.conventionalCompress(msg_plus_signature) // TODO: implementing compress function
        }
    } else {
        compressed = {
            isCompressed: false,
            message: msg_plus_signature
        }

    }
    compressed = JSON.stringify(compressed);


    let pgpRes;
    if (utils._nilEmptyFalse(receiverPubKey)) {
        // there is no pub key, so ther is no sence to encryption
        pgpRes = {
            iPGPVersion,
            secretKey: iConsts.msgTags.NO_ENCRYPTION,
            message: compressed,
            isAuthenticated: false,
        }
        pgpRes = JSON.stringify(pgpRes)

    } else {
        let secretKey = _.has(args, 'secretKey') ? args.secretKey : null;
        if (secretKey != null) {
            secretKey = secretKey;
        } else {
            secretKey = crypto.rand();
        }
        secretKey = secretKey.slice(0, 32)
            // conventional symmetric encryption
        let aesEncoded = crypto.aesEncrypt({
            secretKey: secretKey,
            message: compressed,
        });

        let asyncEncryptedSecretKey = crypto.nativeEncryptWithPublicKey(receiverPubKey, secretKey)

        pgpRes = {
            iPGPVersion,
            secretKey: asyncEncryptedSecretKey,
            message: aesEncoded,
            isAuthenticated: true,
        }
        pgpRes = JSON.stringify(pgpRes)

    }
    // base64 encoding
    let b2 = Buffer.alloc(pgpRes.length, pgpRes);
    pgpRes = b2.toString('base64');

    return pgpRes;
}



module.exports = encryptPGP;
