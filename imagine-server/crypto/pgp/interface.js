
// TODO: it needs also revoke implementation, 
// TODO: change it to be compatible with openPGP & GPG & web of trust publick keys
// TODO: implement also fingerprint sending & confimation (something like verySign confirmed and green check on top of users page)
const iConsts = require('../../config/constants');



class PGPHandler {

    static wrapPGPEnvelope(body) {
        return iConsts.msgTags.iPGPStartEnvelope + body + iConsts.msgTags.iPGPEndEnvelope
    }
}

PGPHandler.encryptPGP = require('./encrypt-pgp');
PGPHandler.decryptPGP = require('./decrypt-pgp');

module.exports = PGPHandler;