// const sha3_512 = require('js-sha3').sha3_512;
// const sha3_384 = require('js-sha3').sha3_384;
// const sha3_256 = require('js-sha3').sha3_256;
// const sha3_224 = require('js-sha3').sha3_224;
// const keccak512 = require('js-sha3').keccak512;
// const keccak384 = require('js-sha3').keccak384;
// const keccak224 = require('js-sha3').keccak224;
// const shake128 = require('js-sha3').shake128;
// const shake256 = require('js-sha3').shake256;
// const cshake128 = require('js-sha3').cshake128;
// const cshake256 = require('js-sha3').cshake256;
// const kmac128 = require('js-sha3').kmac128;
// const kmac256 = require('js-sha3').kmac256;
const keccak256 = require('js-sha3').keccak256;

// TODO: needs too many test and investigattion to be robust & secure for each type of outputs
function keccak256W(inp, options) {

    if (options && options.full) {
        // Array of bytes as decimal integers
        return {
            binString: keccak256(inp).toString('binary'),
            hex: keccak256(inp),
            bytes: keccak256.arrayBuffer(inp)
        };

    } else if (options && options.asBytes) {
        // Array of bytes as decimal integers
        return keccak256.arrayBuffer(inp);

    } else if (options && options.asBinString) {
        // Binary string
        return keccak256(inp).toString('binary');

    } else {
        // String of hex characters
        return keccak256(inp)

    }

}

function keccak256Dbl(inp) {
    return keccak256W(keccak256W(inp));
}
module.exports.keccak256 = keccak256W;
module.exports.keccak256Dbl = keccak256Dbl;