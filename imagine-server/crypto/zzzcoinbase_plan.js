// const BigInteger = require('bigi'); // http://cryptocoinjs.com/modules/misc/bigi/   https://github.com/cryptocoinjs/bigi

// const num_2 = BigInteger.fromHex("000000000000000000000000000000000000000000000002");
// const num_7 = BigInteger.fromHex("000000000000000000000000000000000000000000000007");
// const num_12 = BigInteger.fromHex("00000000000000000000000000000000000000000000000c");
// const num_24 = BigInteger.fromHex("000000000000000000000000000000000000000000000018");
// const num_30 = BigInteger.fromHex("00000000000000000000000000000000000000000000001e");
// const hoursPerYear = BigInteger.fromHex("000000000000000000000000000000000000000000002238"); //24 * 365
// const startDate = { year: 2019, month: 7, day: 12 };
// const e = 27; // initial coinbase for start is 2 pow 23 per 12 hours 


// module.exports = {
//     e: e,
//     num_2: num_2,
//     num_7: num_7,
//     num_12: num_12,
//     num_24: num_24,
//     num_30: num_30,
//     hoursPerYear: hoursPerYear,

//     generateTable: function() {
//         clog.app.info(`Years  \t\t|PaiPer12Hour \t|PaiPerMonth \t\t|PaiPerYear \t\t|PaiPer7Year`);
//         clog.app.info('-------------------------------------------------------------------------------------------------');
//         for (var i = this.e; i > -1; i--) {
//             var cycle = 2019 + ((this.e - i) * 7);
//             var paiPer12Hour = this.calcPotentialMicroPaiPerOneCycle(cycle);
//             // var paiPer12Hour = this.num_2.pow(i);
//             var PaiPerMonth = paiPer12Hour.multiply(this.num_2).multiply(this.num_30);
//             var allYear = PaiPerMonth.multiply(this.num_12);
//             var all7Year = allYear.multiply(this.num_7);
//             clog.app.info(`${cycle}-${cycle+6} \t|${this.microPAIToPAI(paiPer12Hour.intValue())} \t|${this.microPAIToPAI(PaiPerMonth.intValue())} \t\t|${this.microPAIToPAI(allYear.intValue())} \t\t|${this.microPAIToPAI(all7Year.intValue())}`);
//         }
//         clog.app.info('-------------------------------------------------------------------------------------------------\n\n');
//     },





//     gcalcInfoForAnSpecialYear: function(year) {
//         let paiPer12Hour = this.calcPotentialMicroPaiPerOneCycle(year);
//         let paiPerDay = paiPer12Hour.multiply(this.num_2);
//         let paiPerMonth = paiPerDay.multiply(this.num_30);
//         var allYear = paiPerMonth.multiply(this.num_12);
//         var all7Year = allYear.multiply(this.num_7);
//         return {
//             paiPer12Hour: paiPer12Hour.intValue(),
//             paiPerDay: paiPerDay.intValue(),
//             paiPerMonth: paiPerMonth.intValue(),
//             allYear: allYear.intValue(),
//             all7Year: all7Year.intValue(),
//         };
//     }
// };