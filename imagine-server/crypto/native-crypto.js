const crypto = require("crypto");

function nativeGenerateKeyPairSync() {
    const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
        modulusLength: 4096,
        namedCurve: 'secp256k1',
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
                // cipher: 'aes-256-cbc',
                // passphrase: 'abcd'
        }
    });
    return {
        prvKey: privateKey,
        pubKey: publicKey
    }
};

var nativeSign = function(prvKey, message) {
    const signer = crypto.createSign('sha256');
    signer.update(message);
    signer.end();
    let signature = signer.sign(prvKey)
    signature = signature.toString('base64')
    return signature
};

var nativeVerifySignature = function(pubKey, message, signature) {
    signature = Buffer.from(signature, 'base64')
    const verifier = crypto.createVerify('sha256');
    verifier.update(message);
    verifier.end();
    const verified = verifier.verify(pubKey, signature);
    return verified;
};

var encryptStringWithPublicKey = function(publicKey, toEncrypt) {
    // var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    // var publicKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = Buffer.from(toEncrypt);
    var encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
};

var decryptStringWithPrivateKey = function(privateKey, toDecrypt) {
    // var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    // var privateKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("utf8");
};

module.exports = {
    nativeGenerateKeyPairSync: nativeGenerateKeyPairSync,
    nativeSign: nativeSign,
    nativeVerifySignature: nativeVerifySignature,
    nativeEncryptWithPublicKey: encryptStringWithPublicKey,
    nativeDecryptWithPrivateKey: decryptStringWithPrivateKey
}