// const AES = require('aes');
const cCipher = require('crypto');

function aesEncrypt(args = {}) {

    let aesVersion = '0.0.0';
    let algorithm = "aes-256-cbc"
    let message = args.message;

    let key = Buffer.from(args.secretKey) // secretKey.legth must be 32

    const msg_chunk_size = 16
    let chunk = ''
    let iv = Buffer.alloc(msg_chunk_size, 'utf8')
    let cipher = cCipher.createCipheriv(algorithm, key, iv)
    let crypted
    let cryptedArr = [];
    for (let i = 0; i < parseInt(message.length / msg_chunk_size) + 1; i++) {
        chunk = message.substr(i * msg_chunk_size, msg_chunk_size)
        chunk = chunk.padEnd(msg_chunk_size, ' ')
        crypted = cipher.update(chunk, 'utf8', 'base64')
        cryptedArr.push(crypted)
    }
    crypted = cipher.final('base64')
    cryptedArr.push(crypted)
        // clog.app.info('########### enc ended ##########')
        // clog.app.info(cryptedArr)
    finalEnc = JSON.stringify({
        aesVersion: aesVersion,
        encrypted: cryptedArr
    })
    return finalEnc
}

function aesDecrypt(args = {}) {
    let aesVersion = args.aesVersion;
    if (aesVersion == '0.0.0') {
        let algorithm = "aes-256-cbc"
        let message = args.message
        let key = Buffer.from(args.secretKey) // secretKey.legth must be 32
        const msg_chunk_size = 16
        let iv = Buffer.alloc(msg_chunk_size, 'utf8')

        // let enc = JSON.parse(finalEnc)
        //     // clog.app.info('---------- decrypt  --------')
        // let message = enc.enc


        var decipher = cCipher.createDecipheriv(algorithm, key, iv)
        let dec = ''
        message.forEach(chunk => {
            dec += decipher.update(chunk, 'base64', 'utf8')
        });
        dec += decipher.final('utf8')
        dec = dec.trim()
            // clog.app.info(`dec: ${dec}`);
            // clog.app.info('---------- decrypt  end --------')
        dec = dec.trim()
        return dec

        // secretKey = args.secretKey;
        // enc = args.enc;
        // let dec = cCipher.createDecipheriv(algorithm, secretKey).update(enc, "hex", "utf8");
        // return dec;

    } else {
        return 'aes version to decryption does not supported!';

    }

}

module.exports.aesEncrypt = aesEncrypt;
module.exports.aesDecrypt = aesDecrypt;