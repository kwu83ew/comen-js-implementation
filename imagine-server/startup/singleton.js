// https://derickbailey.com/2016/03/09/creating-a-true-singleton-in-node-js-with-es6-symbols/
const _ = require('lodash')
const IMG_SINGLETON = Symbol.for("imagine.singleton");
// const db = require('./db')
const Moment = require('moment-timezone');

function getMoment() {
    return Moment().tz('Greenwich')
}

var globalSymbols = Object.getOwnPropertySymbols(global);
var hasFoo = (globalSymbols.indexOf(IMG_SINGLETON) > -1);
if (!hasFoo) {
    global[IMG_SINGLETON] = {
        // db: db,
        getMoment: getMoment,
        Moment: Moment,
    };
}



var singleton = {};

Object.defineProperty(singleton, "instance", {
    get: function() {
        return global[IMG_SINGLETON];
    }
});

// ensure the API is never changed
// -------------------------------

Object.freeze(singleton);

// export the singleton API only
// -----------------------------

module.exports = singleton;