// this is for testing usages
const _ = require('lodash');
const config = require("config");

// app IDs
// 0: normal nodes
// 1: hu node
// 2: another test neighbor

module.exports = {
    appId: undefined,

    getAppCloneId: function() {
        // clog.app.info('#################################### 1');
        // clog.app.info(appId);

        // if (!_.isUndefined(appId))
        //     return this.appId;

        // if (config.get("dbPostfix") == '_test') {
        //     this.appId = 0;

        // } else {
        // clog.app.info('#################################### 2');
        // clog.app.info(process.argv);
        // clog.app.info(process.argv[2]);
        appId = process.argv[2];
        // clog.app.info(`appId ${appId}`);
        if (_.isUndefined(appId) || (parseInt(appId) === NaN)) {
            appId = 0
        } else {
            appId = parseInt(appId)
        }
        // clog.app.info(`appIddddddddddddd ${appId}`)
        // }
        // clog.app.info(`appId ${appId}`);
        return appId;
    },

    // setAppCloneId: function(i) {
    //     const db = require('./db2')
    //     appId = i
    //     db.selectProperDB()
    // }
};