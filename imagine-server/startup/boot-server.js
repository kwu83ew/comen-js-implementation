const _ = require('lodash')
const iConsts = require('../config/constants')
const db = require('../startup/db2')
const clog = require('../loggers/console_logger');
const utils = require('../utils/utils');
const model = require('../models/interface')
const psqlSetter = require('../db/psql.setting')
const initContentSetting = require('./init-content/initial-content-setting');
const machine = require('../machine/machine-handler');
const sampleMachines = require('./set-sample-machines');

class BootHandler {

    static bootServer() {
        clog.app.info('--- Booting Server')
        // control if the database is initialized in proper mode?
        this.initializeMachine(3);

        // time setting for sql server
        clog.app.info('--- UTC time setting for PSQL server');
        psqlSetter.maybeSetServerTime();

    }

    static initializeMachine(caller = null) {
        let appCloneId = db.getAppCloneId();
        // clog.app.info(`calerrrrrrrrrrrrrrrrrr: ${caller},  appCloneId: ${appCloneId}`);
        machine.initMachine();

        machine.neighborHandler.addSeedNeighbors()

        // initialize document & content
        let initRes = initContentSetting.doInit();
        if (initRes.err != false)
            return initRes;
        let doesSafelyInitialized = initContentSetting.doesSafelyInitialized();
        if (doesSafelyInitialized.err != false) {
            utils.exiter(`doesSafelyInitialized ${doesSafelyInitialized}`, 609);
        }

        return { err: false, msg: 'The machine fully initilized' };
    }


}

module.exports = BootHandler;