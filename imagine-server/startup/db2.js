const _ = require('lodash');
const config = require("config");
const clog = require('../loggers/console_logger');
// const { Pool } = require('pg');
const pg = require('pg');
const utils = require("../utils/utils");
const NativeClient = require('pg-native');

const DB_NAME = 'imgdblive';
const DB_USER = 'imgusrlive';
const DB_PASS = 'imgpwdlive';
const DB_PORT = 5432;

let start, duration, rowCount;

const typesBuiltins = {
    BOOL: 16,
    BYTEA: 17,
    CHAR: 18,
    INT8: 20,
    INT2: 21,
    INT4: 23,
    REGPROC: 24,
    TEXT: 25,
    OID: 26,
    TID: 27,
    XID: 28,
    CID: 29,
    JSON: 114,
    XML: 142,
    PG_NODE_TREE: 194,
    SMGR: 210,
    PATH: 602,
    POLYGON: 604,
    CIDR: 650,
    FLOAT4: 700,
    FLOAT8: 701,
    ABSTIME: 702,
    RELTIME: 703,
    TINTERVAL: 704,
    CIRCLE: 718,
    MACADDR8: 774,
    MONEY: 790,
    MACADDR: 829,
    INET: 869,
    ACLITEM: 1033,
    BPCHAR: 1042,
    VARCHAR: 1043,
    DATE: 1082,
    TIME: 1083,
    TIMESTAMP: 1114,
    TIMESTAMPTZ: 1184,
    INTERVAL: 1186,
    TIMETZ: 1266,
    BIT: 1560,
    VARBIT: 1562,
    NUMERIC: 1700,
    REFCURSOR: 1790,
    REGPROCEDURE: 2202,
    REGOPER: 2203,
    REGOPERATOR: 2204,
    REGCLASS: 2205,
    REGTYPE: 2206,
    UUID: 2950,
    TXID_SNAPSHOT: 2970,
    PG_LSN: 3220,
    PG_NDISTINCT: 3361,
    PG_DEPENDENCIES: 3402,
    TSVECTOR: 3614,
    TSQUERY: 3615,
    GTSVECTOR: 3642,
    REGCONFIG: 3734,
    REGDICTIONARY: 3769,
    JSONB: 3802,
    REGNAMESPACE: 4089,
    REGROLE: 4096
};


// TODO: some improvment on connections needed

class DB {

    // app IDs
    // 0: normal nodes
    // 1: hu node
    // 2: another test neighbor
    // appId: undefined,

    static getAppCloneId() {

        if (_.isUndefined(this.dbDictAsync))
            this.dbDictAsync = {};
        if (_.isUndefined(this.dbDictSync))
            this.dbDictSync = {};

        if (!_.isUndefined(this.appId))
            return this.appId;

        if (config.get("dbPostfix") == '_test') {
            this.appId = 0;

        } else {
            this.appId = process.argv[2];
            if (_.isUndefined(this.appId) || (parseInt(this.appId) === NaN)) {
                this.appId = 0
            } else {
                this.appId = parseInt(this.appId)
            }
            clog.app.info(`this.appIddddddddddddd ${this.appId}`);
        }
        this.selectProperDB()
        return this.appId;
    }

    static setAppCloneId(i) {
        this.appId = i;
        this.selectProperDB()
    }

    static selectProperDB() {

        if (_.has(this, 'dbDictAsync') && _.has(this.dbDictAsync, this.getDBName())) {
            this.selectedAsyncDB = this.dbDictAsync[this.getDBName()]
        } else {

            // no time stamp
            pg.types.setTypeParser(1114, function (stringValue) {
                return stringValue;
            });

            pg.types.setTypeParser(typesBuiltins.INT8, (value) => {
                return parseInt(value);
            });

            pg.types.setTypeParser(typesBuiltins.FLOAT8, (value) => {
                return parseFloat(value);
            });

            pg.types.setTypeParser(typesBuiltins.NUMERIC, (value) => {
                return parseFloat(value);
            });

            this.selectedAsyncDB = new pg.Pool({
                host: 'localhost',
                database: this.getDBName(),
                port: DB_PORT,
                user: DB_USER,
                password: DB_PASS,
            });
            this.dbDictAsync[this.getDBName()] = this.selectedAsyncDB
        }


        if (_.has(this, 'dbDictSync') && _.has(this.dbDictSync, this.getDBName())) {
            this.selectedSyncDB = this.dbDictSync[this.getDBName()]
        } else {
            this.selectedSyncDB = new NativeClient();
            this.selectedSyncDB.connectSync(`host=localhost port=${DB_PORT} dbname=${this.getDBName()} user=${DB_USER} password=${DB_PASS} connect_timeout=10`);
            this.dbDictSync[this.getDBName()] = this.selectedSyncDB
        }

    }

    static getDBName() {
        let database = DB_NAME;
        let appCloneId = this.getAppCloneId();
        if (parseInt(appCloneId) > 0)
            database += parseInt(appCloneId);

        let dbPostfix;
        try {
            dbPostfix = config.get("dbPostfix");
        } catch (e) {
            dbPostfix = "";
        }
        database += dbPostfix;
        return database
    }

    static getCurrentPoolName() {
        return this.selectedDBName;
    }

    static query(text, params, callback) {
        try {
            let start = Date.now();
            return selectedAsyncDB.query(text, params, (err, res) => {
                let duration = Date.now() - start;
                try {
                    let rowCount = _.has(res, 'rowCount') ? res.rowCount : 0
                    clog.sql.info('executed query1 ' + text + ' ' + params.join(', ') + ' duration:' + duration + ' rows:' + rowCount);
                } catch (e) {
                    clog.sql.error(e);
                }
                callback(err, res);
            })
        } catch (e) {
            throw new Error(e)
        }
    }

    static async aQuery(text, params, log = true) {
        try {
            return new Promise((resolve, reject) => {
                let start = Date.now();
                this.selectedAsyncDB.query(text, params, (err, res) => {
                    if (err)
                        return reject(err);

                    let duration = Date.now() - start;
                    let rowCount = _.has(res, 'rowCount') ? res.rowCount : 0;
                    if (log)
                        clog.sql.info('executed query2 ' + text + ' ' + params.join(', ') + ' duration:' + duration + ' rows:' + rowCount);
                    if (_.has(res, 'rows') && _.has(res.rows, 'length') && (res.rows.length > 0))
                        return resolve(res.rows);

                    return resolve([]);

                });
            });
        } catch (e) {
            throw new Error(e)
        }
    }


    // sync 
    static sQuery(query, params, lockDb = false, log = true) {
        start = Date.now();
        let res;
        if (lockDb) {
            try {
                this.selectedSyncDB.querySync(`BEGIN;`);
                res = this.selectedSyncDB.querySync(query, params);
                this.selectedSyncDB.querySync(`COMMIT;`);
            } catch (e) {
                console.log('sQuery Exception: ', e);
                this.selectedSyncDB.querySync(`ROLLBACK;`);
                clog.sql.error('\nsQuery Exception: ' + 'sQuery ' + query + ' [' + params.join(', ') + ']');
            }
        } else {
            res = this.selectedSyncDB.querySync(query, params);
        }
        let duration = Date.now() - start;
        if (log) {
            let tmpQ = `sQuery ${query} [${params.join(', ')}] (${duration} seconds)`;
            if (lockDb)
                tmpQ = `BEGIN; ${tmpQ}; COMMIT;`;
            tmpQ = `\n${tmpQ}`;
            clog.sql.info(tmpQ);
        }
        if (res === undefined) {
            // is not initialized so,
            clog.sql.error('--- undefined query result! ');
            return null;

        } else {
            if (log)
                clog.sql.info(' rows:' + res.length);
            return res;
        }

    }

    static sEmptyDB() {
        try {
            let start = Date.now();
            let res = this.selectedSyncDB.querySync('SELECT emptydb()', []);
            let duration = Date.now() - start;
            clog.sql.info('empty DB duration: ' + duration);
            return res;
        } catch (e) {
            throw new Error(e)
        }
    }

    // query generating

    static aEmptyDB() {
        let start = Date.now();
        return new Promise((resolve, reject) => {
            this.selectedAsyncDB.query('SELECT emptydb()', [], (err, res) => {
                let duration = Date.now() - start;
                try {
                    clog.sql.info('empty DB duration: ' + duration);
                    return resolve(res)
                } catch (e) {
                    throw new Error(e)
                }
                return reject(err);
            });
        })
    }

    // TODO wirte some unittests
    static updateQueryGenerator(args) {
        try {
            let query = _.has(args, 'query') ? args.query : null;
            let updates = _.has(args, 'updates') ? args.updates : null;
            let table = _.has(args, 'table') ? args.table : null;

            if (utils._nil(updates))
                return null

            let inx = 0
            let key, value;
            let sets = []
            let _values = []
            // let clauses = []
            let clauseQueryGeneratorRes = { _clauses: '', _values: [] }
            _.forOwn(updates, (value, key) => {
                inx++
                sets.push(key + '=$' + inx)
                _values.push(value)
            })
            sets = " SET " + sets.join(", ")

            if (utils._notNil(query)) {
                // let { _clauses, _values } = this.clauseQueryGenerator({
                clauseQueryGeneratorRes = this.clauseQueryGenerator({
                    inx: inx + 1,
                    query: args.query
                });
                // console.log('clauseQueryGeneratorRes', clauseQueryGeneratorRes);
            }
            let q = `UPDATE ${table} ${sets} ${clauseQueryGeneratorRes._clauses}`;
            for (let aVal of clauseQueryGeneratorRes._values) {
                _values.push(aVal);
            }
            return {
                _query: q,
                _values: _values
            }
        } catch (e) {
            throw new Error(e)
        }
    }

    // TODO wirte some unittests
    static upsertQueryGenerator(args) {
        try {
            let updates = _.has(args, 'updates') ? args.updates : null;
            let table = _.has(args, 'table') ? args.table : null;
            let idField = _.has(args, 'idField') ? args.idField : null;
            let idValue = _.has(args, 'idValue') ? args.idValue : null;

            if (utils._nil(updates))
                return null;

            let updateAssignments = []
            let _values = []
            let clauses = []
            let insertFieldNames = []
            let insertPlaceholders = []

            let inx = 1;
            insertFieldNames.push(idField)
            insertPlaceholders.push('$' + inx)
            _values.push(idValue)

            _.forOwn(updates, (value, key) => {
                inx++
                insertFieldNames.push(key)
                insertPlaceholders.push('$' + inx)
                _values.push(value)
            });
            _.forOwn(updates, (value, key) => {
                inx++
                updateAssignments.push(key + '=$' + inx)
                _values.push(value)
            });

            updateAssignments = " SET " + updateAssignments.join(", ")
            clauses = " WHERE " + clauses.join(" AND ");
            let q = 'INSERT INTO ' + table + ` (${insertFieldNames.join(', ')}) VALUES (${insertPlaceholders.join(', ')}) 
        ON CONFLICT(${idField}) DO UPDATE ` + updateAssignments;
            return {
                _query: q,
                _values: _values
            }
        } catch (e) {
            throw new Error(e)
        }
    }

    // TODO wirte some unittests
    static clauseQueryGenerator(args = {}) {
        let key, value;
        let inx = _.has(args, 'inx') ? args.inx - 1 : 0;
        let _values = []
        let _clauses = '';
        let valArr;
        if (_.has(args, 'query') && !utils._nilEmptyFalse(args.query)) {
            let tmp = []
            args.query.forEach(aClauseTuple => {
                key = aClauseTuple[0];
                value = aClauseTuple[1];
                inx++
                if (Array.isArray(value)) {
                    switch (value[0].toUpperCase()) {
                        case 'IN':
                        case 'NOT IN':
                            let placeholders = [];
                            valArr = value[1];
                            if (!Array.isArray(valArr))
                                valArr = [valArr];
                            valArr.forEach(val => {
                                placeholders.push('$' + inx);
                                _values.push(val)
                                inx++;
                            });
                            tmp.push(' (' + key + ' ' + value[0].toUpperCase() + ' (' + placeholders.join(', ') + ')) ')
                            inx--;
                            break;

                        case 'LIKE:OR':
                            let orTmp = [];
                            valArr = value[1];
                            if (!Array.isArray(valArr))
                                valArr = [valArr];
                            valArr.forEach(val => {
                                _values.push(val);
                                orTmp.push(' (' + key + ' LIKE $' + inx + ')')
                                inx++;
                            });
                            tmp.push('(' + orTmp.join(' OR ') + ')')
                            inx--;
                            break;

                        default:
                            tmp.push(' (' + key + ' ' + value[0] + ' $' + inx + ') ')
                            _values.push(value[1])
                    }

                } else {
                    tmp.push(' (' + key + '=$' + inx + ') ')
                    _values.push(value)

                }
            });

            if ((tmp.length > 0) && (_values.length > 0)) {
                _clauses = " WHERE " + tmp.join(' AND ')
            }
        }

        let _fields = ' * '
        if (_.has(args, 'fields')) {
            _fields = args.fields.join(', ')
        }
        let _order = ''
        if (_.has(args, 'order')) {
            let tmp = []
            args.order.forEach(ord => {
                tmp.push(` ${ord[0]} ${ord[1]}`)
            });
            if (tmp.length > 0)
                _order = ' ORDER BY ' + tmp.join(', ') + ' '
        }
        let _limit = ''
        if (_.has(args, 'limit')) {
            _limit = ' LIMIT ' + args.limit + ' '
        }


        return {
            _fields: _fields,
            _clauses: _clauses,
            _values: _values,
            _order: _order,
            _limit: _limit,
        }

    }

    // TODO wirte some unittests
    static insertQueryGenerator(args = {}) {
        try {
            let inx = 0
            let _values = []
            let _fields = []
            let _fieldsVar = []
            _.forOwn(args, (value, key) => {
                inx++
                _fields.push(key)
                _fieldsVar.push('$' + inx)
                _values.push(value)
            })

            if ((_fields.length > 0) && (_values.length > 0)) {
                _fields = " (" + _fields.join(', ') + ") VALUES (" + _fieldsVar.join(', ') + ") "
            } else {
                _fields = ""
            }

            return {
                _fields: _fields,
                _values: _values
            }
        } catch (e) {
            throw new Error(e)
        }
    }
}

DB.selectedDBName = DB.getDBName();

module.exports = DB
