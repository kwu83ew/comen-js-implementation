const express = require('express');
const contracts = require('../routes/contracts/contracts');
const contribute = require('../routes/contribute/contribute');
const demos = require('../routes/demos/demos');
const freePosts = require('../routes/free-posts/free-posts');
const iname = require('../routes/iname/iname');
const machine = require('../routes/machine/machine');
const messenger = require('../routes/messenger/messenger');
const misc = require('../routes/misc/misc');
const plugins = require('../routes/plugins/plugins');
const settings = require('../routes/settings/settings');
const tobserver = require('../routes/tobserver/tobserver');
const utils = require('../routes/utils/utils');
const wallet = require('../routes/wallet/wallet');
const watcher = require('../routes/watcher/watcher');
const wiki = require('../routes/wiki/wiki');
const admPollings = require('../routes/adm-pollings/adm-pollings');
const icache = require('../routes/i-cache-files/icache');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
// const emailPoper = require('../services/dummyEmailHandler');

const IS_DEVEL = ((process.env.NODE_ENV || 'development') === 'development') ? true : false;


//TODO: implementing jwtPrivateKey
module.exports = function (app) {
    app.all('/*', function (req, res, next) {
        let otherHeadersSeparatedByComma = '';
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers',
            'Content-Type,X-Requested-With,cache-control,pragma' +
            otherHeadersSeparatedByComma);
        res.header('access-control-allow-methods', 'GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS');

        next();
    });
    app.use(express.json());
    app.use(express.urlencoded({ extended: true })); //if client sends data via classic old post
    
    app.use(fileUpload({
        // debug: true,
        useTempFiles: true,
        tempFileDir: '../tmp/'
    }));

    if (IS_DEVEL)
        app.use(morgan('tiny')); // logging

    app.use('/api/utils', utils);
    app.use('/api/wiki', wiki);
    app.use('/api/adm-pollings', admPollings);
    app.use('/api/demos', demos);
    app.use('/api/wallet', wallet);
    app.use('/api/machine', machine);
    app.use('/api/free-posts', freePosts);
    app.use('/api/watcher', watcher);
    app.use('/api/plugins', plugins);
    app.use('/api/contribute', contribute);
    app.use('/api/settings', settings);
    app.use('/api/contracts', contracts);
    app.use('/api/misc', misc);
    app.use('/api/iname', iname);
    app.use('/api/messenger', messenger);
    app.use('/api/tobserver', tobserver);
    app.use('/api/icache', icache);
};
