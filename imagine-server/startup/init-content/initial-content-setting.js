const _ = require('lodash');
const iConsts = require('../../config/constants');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface')
const blockHandler = require('../../dag/interface');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const DNAHandler = require('../../dna/dna-handler');
const flensHandler = require('../../contracts/flens-contract/flens-handler');
const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
const wikiHandler = require('../../services/free-doc-handler/wiki-handler/wiki-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const Moment = require('../../startup/singleton').instance.Moment;

const startVotingDate = Moment(iConsts.getLaunchDate(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': 5 * iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");

let Genesis = {
    net: "im",
    descriptions: "Imagine all the people sharing all the world",
    blockLength: "0000000",
    blockHash: "0000000000000000000000000000000000000000000000000000000000000000",
    bType: iConsts.BLOCK_TYPES.Genesis,
    bVer: "0.0.0",
    ancestors: [],
    signals: [],

    backer: "", // the address which is used to collect transaction-fees
    confidence: 100.00, // the sahre percentage of block backer
    creationDate: iConsts.getLaunchDate(),

    docsRootHash: "",
    docs: [],
    fVotes: [], // floating votes(e.g. coinbase confirmations, floating Votes, cloneTransaction...)

};

let dnaProposalDoc;

class InitialContentSetting {

    static doInit() {
        let res = model.sRead({
            table: 'i_blocks',
            query: [['b_type', iConsts.BLOCK_TYPES.Genesis]]
        });
        if (res.length != 0)
            return { err: false }; // it already initialized

        res = this.initGenesis();
        this.initShares();
        this.initAdministrativeConfigurationsHistory();
        this.initPollingProfiles();

        res = this.initAgoras();
        if (res.err != false)
            return res;

        this.initTmpWikiContent();

        return { err: false }
    }

    static doesSafelyInitialized() {
        // TODO implement it to controll if all intial document are inserted properly?
        return { err: false }
    }

    static initGenesis() {
        let block = Genesis;
        block.creationDate = iConsts.getLaunchDate();
        block.signals = [];

        block.docs = [];
        dnaProposalDoc = DNAHandler.getInitialDNADoc(); // add initialise DNA to genesis block
        dnaProposalDoc.shares = dnaProposalDoc.helpHours * dnaProposalDoc.helpLevel;
        dnaProposalDoc.votingLongevity = 24;
        dnaProposalDoc.pollingProfile = 'Basic';
        block.docs.push(dnaProposalDoc);

        block.docsRootHash = blockHandler.calculateDocsRootHash(block);
        block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);
        block.blockHash = blockHandler.hashBlock(block);
        dagHandler.addBH.addBlockToDAG({ block });
    }

    static initShares() {
        // update proposal status in DB
        proposalHandler.updateProposal({
            query: [
                ['pr_hash', dnaProposalDoc.hash]
            ],
            updates: {
                pr_start_voting_date: startVotingDate,
                pr_conclude_date: iConsts.getLaunchDate(),
                pr_approved: iConsts.CONSTS.YES
            }
        });

        // update polling status in DB
        pollHandler.updatePolling({
            query: [
                ['pll_ref', dnaProposalDoc.hash]
            ],
            updates: {
                pll_start_date: startVotingDate,
                pll_status: iConsts.CONSTS.CLOSE,
                pll_ct_done: iConsts.CONSTS.YES
            }
        });

        // also insert in db DNA initiate shares
        DNAHandler.insertAShare(dnaProposalDoc);


    }

    static initAdministrativeConfigurationsHistory() {
        const admPollingsHandler = require('../../web-interface/adm-pollings/adm-pollings-handler');
        admPollingsHandler.onChain.initAdministrativeConfigurationsHistory();
    }

    static initPollingProfiles() {
        const pollHandler = require('../../services/polling-handler/general-poll-handler');
        pollHandler.initPollingProfiles();
    }

    static initAgoras() {
        const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');
        let res = freeDocHandler.demos.initAg.initAgoras();
        if (res.err != false) {
            utils.exiter(`Machine couln't initialize Agoras`, 678);
        }
        return res;
    }

    static initTmpWikiContent() {
        // register iName imagine & hu
        flensHandler.register.initINames({ creationDate: iConsts.getLaunchDate() });

        // creating init tmpWiki Pages
        wikiHandler.init.initWikiPages();


        // delegate permission to ALL to post on imagine domain
        // TODO:
    }
}

module.exports = InitialContentSetting;
