const _ = require('lodash')
const db = require('../startup/db2')
const clog = require('../loggers/console_logger');
const utils = require('../utils/utils');
const model = require('../models/interface')
const iConsts = require('../config/constants')
const machine = require('../machine/machine-handler');
const Moment = require('../startup/singleton').instance.Moment;
const DNAHandler = require('../dna/dna-handler');

const SHOULD_SET_SAMPLE_DNA_SHARES = false;

class DevelopeMachines {


    static setSampleDnaShares(args) {

        if (!SHOULD_SET_SAMPLE_DNA_SHARES)
            return;

        const initShareDate = Moment(iConsts.getLaunchDate(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': 2 * iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");

        let { neighborMachineSettings, aliceMachineSettings, bobMachineSettings } = args;

        let dnaProposalDoc, msg;
        let onePercent = Math.floor((61355 * 6) / 100);

        // init 1 hour shares of Neighbor
        dnaProposalDoc = DNAHandler.getDNATmpShareDoc(); // add initialise DNA to genesis block
        dnaProposalDoc.title = `init 1 hour shares of Neighbor`;
        dnaProposalDoc.descriptions = `init 1 hour shares of Neighbor`;
        dnaProposalDoc.tags = `init 1 hour shares of Neighbor`;
        dnaProposalDoc.helpHours = 1;
        dnaProposalDoc.helpLevel = 1;
        dnaProposalDoc.shares = dnaProposalDoc.helpHours * dnaProposalDoc.helpLevel;
        dnaProposalDoc.votesY = 10000;
        dnaProposalDoc.votesN = 1;
        dnaProposalDoc.votesA = 2;
        dnaProposalDoc.creationDate = initShareDate;
        dnaProposalDoc.approvalDate = initShareDate;
        dnaProposalDoc.shareholder = neighborMachineSettings.backerAddress;
        dnaProposalDoc.hash = DNAHandler.hashDNARegDoc(dnaProposalDoc);
        DNAHandler.insertAShare(dnaProposalDoc);
        msg = `Init 1 shares for Neighbor ${utils.microPAIToPAI(onePercent)} = 1 Hour level 1 `;


        // Raising almost 7% shares of Alice
        dnaProposalDoc = DNAHandler.getDNATmpShareDoc(); // add initialise DNA to genesis block
        dnaProposalDoc.title = `Raising almost 7% shares of Alice`;
        dnaProposalDoc.descriptions = `Raising almost 7% shares of Alice`;
        dnaProposalDoc.tags = `Raising almost 7% shares of Alice`;
        dnaProposalDoc.helpHours = Math.floor(onePercent * 7 / 5);
        dnaProposalDoc.helpLevel = 5;
        dnaProposalDoc.shares = dnaProposalDoc.helpHours * dnaProposalDoc.helpLevel;
        dnaProposalDoc.votesY = 10000;
        dnaProposalDoc.votesN = 1;
        dnaProposalDoc.creationDate = initShareDate;
        dnaProposalDoc.approvalDate = initShareDate;
        dnaProposalDoc.shareholder = aliceMachineSettings.backerAddress;
        dnaProposalDoc.hash = DNAHandler.hashDNARegDoc(dnaProposalDoc);
        DNAHandler.insertAShare(dnaProposalDoc);
        msg = `Raising 7% shares for Alice ${utils.microPAIToPAI(onePercent * 7)} = %1 `;


        // Raising almost 3% shares of Bob
        dnaProposalDoc = DNAHandler.getDNATmpShareDoc(); // add initialise DNA to genesis block
        dnaProposalDoc.title = `Raising almost 3% shares of Bob`;
        dnaProposalDoc.descriptions = `Raising almost 3% shares of Bob`;
        dnaProposalDoc.tags = `Raising almost 3% shares of Bob`;
        dnaProposalDoc.helpHours = Math.floor(onePercent * 3 / 5);
        dnaProposalDoc.helpLevel = 5;
        dnaProposalDoc.shares = dnaProposalDoc.helpHours * dnaProposalDoc.helpLevel;
        dnaProposalDoc.votesY = 10000;
        dnaProposalDoc.votesN = 2;
        dnaProposalDoc.votesA = 1;
        dnaProposalDoc.creationDate = initShareDate;
        dnaProposalDoc.approvalDate = initShareDate;
        dnaProposalDoc.shareholder = bobMachineSettings.backerAddress;
        dnaProposalDoc.hash = DNAHandler.hashDNARegDoc(dnaProposalDoc);
        DNAHandler.insertAShare(dnaProposalDoc);
        msg = `Raising 3% shares for Bob ${utils.microPAIToPAI(onePercent * 3)} = %3 `;





    }

    static setSampleMachines() {
        const iutils = require('../utils/iutils');
        let mpCode = iutils.getSelectedMProfile();
        let appCloneId = db.getAppCloneId();

        let SetEveToo = false;

        let neighborMachineSettings, hu1MachineSettings, aliceMachineSettings, bobMachineSettings, eveMachineSettings;
        clog.app.info(`pppppppppppppppp<<<<<<<<< in setSampleMachines appCloneId: ${appCloneId}`);
        if (appCloneId == 1) {

            //reading machines setting
            try {
                db.setAppCloneId(0);
                neighborMachineSettings = machine.getMProfileSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT });
                console.log(`\n\n\n\nneighborMachineSettings neighborMachineSettingsneighborMachineSettings ${utils.stringify(neighborMachineSettings)}`);
                clog.app.info(`\n\n\n\nneighborMachineSettings neighborMachineSettingsneighborMachineSettings ${utils.stringify(neighborMachineSettings)}`);
                neighborMachineSettings.pubEmail.address = 'neighbor@imagine.com';
                machine.updateMachineEmailSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT, params: neighborMachineSettings.pubEmail, emailType: iConsts.CONSTS.PUBLIC });
            } catch (e) {
                console.log(e);
            }
            try {
                db.setAppCloneId(1);
                hu1MachineSettings = machine.getMProfileSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT });
                hu1MachineSettings.pubEmail.address = 'hu1@imagine.com';
                machine.updateMachineEmailSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT, params: hu1MachineSettings.pubEmail, emailType: iConsts.CONSTS.PUBLIC });
            } catch (e) {
                console.log(e);
            }
            try {
                db.setAppCloneId(2);
                aliceMachineSettings = machine.getMProfileSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT });
                aliceMachineSettings.pubEmail.address = 'alice@imagine.com';
                machine.updateMachineEmailSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT, params: aliceMachineSettings.pubEmail, emailType: iConsts.CONSTS.PUBLIC });
            } catch (e) {
                console.log(e);
            }
            try {
                db.setAppCloneId(3);
                bobMachineSettings = machine.getMProfileSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT });
                bobMachineSettings.pubEmail.address = 'bob@imagine.com';
                machine.updateMachineEmailSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT, params: bobMachineSettings.pubEmail, emailType: iConsts.CONSTS.PUBLIC });
            } catch (e) {
                console.log(e);
            }
            if (SetEveToo) {
                try {
                    db.setAppCloneId(4);
                    eveMachineSettings = machine.getMProfileSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT });
                    eveMachineSettings.pubEmail.address = 'eve@imagine.com';
                    machine.updateMachineEmailSettingsSync({ mpCode: iConsts.CONSTS.DEFAULT, params: eveMachineSettings.pubEmail, emailType: iConsts.CONSTS.PUBLIC });
                } catch (e) {
                    console.log(e);
                }
            }

            db.setAppCloneId(1);
            // add HU INAME_OWNER_ADDRESS to wallet
            const huINamesAddressdetail = require('../../hu-not-versionate-it/prepare-and-init-hu-inames');
            model.sCreate({
                table: 'i_machine_wallet_addresses',
                values: {
                    wa_mp_code: mpCode,
                    wa_address:iConsts.HU_INAME_OWNER_ADDRESS,
                    wa_title: 'HU_INAME_OWNER_ADDRESS',
                    wa_creation_date: utils.getNow(),
                    wa_detail: utils.stringify(huINamesAddressdetail)
                }
            });

            // add HU DNA_SHARE_ADDRESS to wallet & profiles
            const huDNAAddressdetail = require('../../hu-not-versionate-it/prepare-and-init-hu-dna');
            model.sCreate({
                table: 'i_machine_wallet_addresses',
                values: {
                    wa_mp_code: mpCode,
                    wa_address:iConsts.HU_DNA_SHARE_ADDRESS,
                    wa_title: 'HU_DNA_SHARE_ADDRESS',
                    wa_creation_date: utils.getNow(),
                    wa_detail: utils.stringify(huDNAAddressdetail)
                }
            });
            // reset machine backer address
            hu1MachineSettings.machineAlias = 'huMachine';
            hu1MachineSettings.backerAddress = iConsts.HU_DNA_SHARE_ADDRESS
            hu1MachineSettings.backerDetail = huDNAAddressdetail
            // TODO update backer pub, prv keys
            machine.updateMachineSettings({ machineSettings: hu1MachineSettings })

            try {
                db.setAppCloneId(0);
                this.setSampleDnaShares({ neighborMachineSettings, aliceMachineSettings, bobMachineSettings });
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu2@imagine.com']
                    ]
                })
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu1@imagine.com']
                    ]
                })
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'hu1@imagine.com',
                    nPGPPubKey: hu1MachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'alice@imagine.com',
                    nPGPPubKey: aliceMachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
            } catch (e) {
                console.log(e);
            }

            try {
                db.setAppCloneId(1);
                this.setSampleDnaShares({ neighborMachineSettings, aliceMachineSettings, bobMachineSettings });
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'neighbor@imagine.com',
                    nPGPPubKey: neighborMachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
                if (SetEveToo) {
                    machine.neighborHandler.addANeighborSync({
                        nEmail: 'eve@imagine.com',
                        nPGPPubKey: eveMachineSettings.pubEmail.PGPPubKey,
                        nConnectionType: iConsts.CONSTS.PUBLIC,
                    });
                }
            } catch (e) {
                console.log(e);
            }

            try {
                db.setAppCloneId(2);
                this.setSampleDnaShares({ neighborMachineSettings, aliceMachineSettings, bobMachineSettings });
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu1@imagine.com']
                    ]
                })
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu2@imagine.com']
                    ]
                })
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'neighbor@imagine.com',
                    nPGPPubKey: neighborMachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'bob@imagine.com',
                    nPGPPubKey: bobMachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
                if (SetEveToo) {
                    machine.neighborHandler.addANeighborSync({
                        nEmail: 'eve@imagine.com',
                        nPGPPubKey: eveMachineSettings.pubEmail.PGPPubKey,
                        nConnectionType: iConsts.CONSTS.PUBLIC,
                    });
                }
            } catch (e) {
                console.log(e);
            }

            try {
                db.setAppCloneId(3);
                this.setSampleDnaShares({ neighborMachineSettings, aliceMachineSettings, bobMachineSettings });
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu1@imagine.com']
                    ]
                })
                machine.neighborHandler.deleteNeighborSync({
                    query: [
                        ['n_email', 'hu2@imagine.com']
                    ]
                })
                machine.neighborHandler.addANeighborSync({
                    nEmail: 'alice@imagine.com',
                    nPGPPubKey: aliceMachineSettings.pubEmail.PGPPubKey,
                    nConnectionType: iConsts.CONSTS.PUBLIC,
                });
                if (SetEveToo) {
                    machine.neighborHandler.addANeighborSync({
                        nEmail: 'eve@imagine.com',
                        nPGPPubKey: eveMachineSettings.pubEmail.PGPPubKey,
                        nConnectionType: iConsts.CONSTS.PUBLIC,
                    });
                }
            } catch (e) {
                console.log(e);
            }

            if (SetEveToo) {
                try {
                    db.setAppCloneId(4);
                    this.setSampleDnaShares({ neighborMachineSettings, aliceMachineSettings, bobMachineSettings });
                    machine.neighborHandler.deleteNeighborSync({
                        query: [
                            ['n_email', 'hu2@imagine.com']
                        ]
                    })
                    machine.neighborHandler.addANeighborSync({
                        nEmail: 'bob@imagine.com',
                        nPGPPubKey: bobMachineSettings.pubEmail.PGPPubKey,
                        nConnectionType: iConsts.CONSTS.PUBLIC,
                    });
                    machine.neighborHandler.deleteNeighborSync({
                        query: [
                            ['n_email', 'hu1@imagine.com']
                        ]
                    })
                    machine.neighborHandler.addANeighborSync({
                        nEmail: 'hu1@imagine.com',
                        nPGPPubKey: hu1MachineSettings.pubEmail.PGPPubKey,
                        nConnectionType: iConsts.CONSTS.PUBLIC,
                    });
                } catch (e) {
                    console.log(e);
                }
            }

            db.setAppCloneId(1);
        }

        db.setAppCloneId(1);
        model.sCreate({
            table: 'i_kvalue',
            values: {
                kv_key: 'setSampleMachines',
                kv_value: 'done',
                kv_last_modified: utils.getNow(),
            }
        });


    }
}

module.exports = DevelopeMachines;
