const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const networkListener = require('../../network-adapter/network-listener');
const coinbaseCreator = require('../../dag/coinbase/create-coinbase-block');
const flSign = require('../../dag/floating-signatures/floating-signatures');
const coinbaseUtxo = require('../../dag/coinbase/import-utxo-from-coinbases');
const transferUtxo = require('../../dag/normal-block/import-utxo-from-normal-blocks/import-utxo-from-normal-blocks');
const parsingQHandler = require('../../services/parsing-q-manager/parsing-q-handler');
const sendingQManager = require('../../services/sending-q-manager/sending-q-manager');
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const flensHandler = require('../../contracts/flens-contract/flens-handler');
const machine = require('../../machine/machine-handler');

// pop email and copy it to hardcopy file on hard disc
if (iConsts.EMAIL_IS_ACTIVE)
    networkListener.launchEmailPoper();

// read hardcopy file from hard disc and insert to ..._parsing_q table
networkListener.launchHardCopyReading();

// read from i_parsing_q and parse block
parsingQHandler.qPicker.launchSmartPullFromParsingQ();

// read from i_packet_tosend and if DO_HARDCOPY_OUTPUT_EMAILS create proper file on hard disc/outbox folder otherwise send email directly
sendingQManager.launchPullSendingQ();

// control "maybe_ask_for_latest_blocks" flag and invoke missed information
setTimeout(dagMsgHandler.launchInvokeLeaves, 15000);

// control "maybe_ask_for_descendents" flag and invoke missed information
setTimeout(dagMsgHandler.launchInvokeDescendents, 30000);
setTimeout(dagMsgHandler.launchDummyPrerequisitiesRemover, 2000);


// coinbase check. the 1 minute delay is critical in first runing the system to give time to init Genesis block in table
setTimeout(coinbaseCreator.launchCreateCB, 8000);

// coinbase signing check
setTimeout(flSign.launchSignCB, 8000);

setTimeout(dagMsgHandler.launchMissedBlocksInvoker, 20000);

// manage Coinbase/DNA incomes (insert to UTXOs)
setTimeout(coinbaseUtxo.launchImportUTXOsFromCoinbaseBlocks, 8000);


// manage transfering PAI incomes (insert to UTXOs)
setTimeout(transferUtxo.launchImportUTXOsFromNormalBlocks, 8000);

// controll recently closed pollings and do proper reaction
setTimeout(pollHandler.concludeHandler.launchRecursiveConcludeTreatment, 1000);

// controll iNAme registration settelment
setTimeout(flensHandler.register.iNameSettlement, 1000);


// if machine is sync mode, refresh more frequantly missed blocks table
setTimeout(() => {
    if(machine.isInSyncProcess()){
        const missedBlocksHandler = require('../../dag/missed-blocks-handler');
        missedBlocksHandler.recursiveMissedlocksManager();
        // force to invoke download full blockgraph 
        const dagMessagingHandler = require('../../messaging-protocol/dag/dag-msg-handler');
        dagMessagingHandler.invokeDescendents()
    }
}, 1000);


