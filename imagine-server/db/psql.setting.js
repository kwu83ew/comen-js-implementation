// http://blog.untrod.com/2016/08/actually-understanding-timezones-in-postgresql.html

const _ = require('lodash');
const db = require('../startup/db2')
const getMoment = require('../startup/singleton').instance.getMoment
const clog = require('../loggers/console_logger');

let utils;
function getServerTime() {
    return new Promise((resolve, reject) => {

        db.query('Select NOW();', [], (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res.rows[0].now);
        });
    });
}

function setServerTime() {
    return new Promise((resolve, reject) => {

        db.query("SET TIME ZONE 'UTC';", [], (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(true);
        });
    });
}

async function maybeSetServerTime() {
    try {
        let psqlTime = await setServerTime()
        if (!psqlTime) {
            utils.exiter('machine could not set psql timezone to UTC!!', 17);
        }
        psqlTime = await getServerTime()
        clog.sql.info(`PSQL timezone: ${psqlTime}`)
        psqlTime = parseInt(new Date(psqlTime).getTime() / (1000 * 60));
        let momentUTC = parseInt(new Date(getMoment()).getTime() / (1000 * 60));
        if (psqlTime != momentUTC) {
            if (utils == null)
                utils = require('../utils/utils');
            utils.exiter('machine timezone(moment) is not same as psql timezone!!', 18);
        }

    } catch (e) {
        return (new Error(e))
    }
}

module.exports.getServerTime = getServerTime
module.exports.maybeSetServerTime = maybeSetServerTime