
-- drop table idev_inbox_logs;
CREATE TABLE idev_inbox_logs (
    il_id bigserial primary key,           
    il_title text NOT NULL,               
    il_creation_date varchar(32) NOT NULL 
);

-- drop table idev_parsing_q;
CREATE TABLE idev_parsing_q (
    pq_id bigserial primary key,  
    pq_type varchar(32) NOT NULL,   
    pq_code varchar(256) NOT NULL,   
    pq_sender varchar(256) NOT NULL,   -- the sender's email         
    pq_connection_type varchar(256) NOT NULL,   -- public or private
    pq_receive_date varchar(32) NOT NULL, -- receiving time in local node but utc-timezone
    pq_payload TEXT NOT NULL,   -- stringified body of block
    pq_prerequisites TEXT NULL,   -- stringified array of block hash which are needed to validate this block
    pq_parse_attempts INT NULL,  -- parse attempts, to avoid blocking on one block
    pq_v_status varchar(64) NULL,   -- the validation status of block
    pq_creation_date varchar(32) NOT NULL, -- the block creation date
    pq_insert_date varchar(32) NOT NULL, 
    pq_last_modified varchar(32) NOT NULL
);

-- drop table idev_sending_q;
CREATE TABLE idev_sending_q (
    sq_id bigserial primary key,  
    sq_type varchar(32) NOT NULL,
    sq_code varchar(256) NOT NULL,
    sq_title varchar(256) NOT NULL,            
    sq_sender varchar(256) NOT NULL,   -- the sender's email         
    sq_receiver varchar(256) NOT NULL,   -- the receiver's email         
    sq_connection_type varchar(256) NOT NULL,   -- public or private
    sq_payload TEXT NOT NULL,   -- stringified body of block
    sq_send_attempts INT NULL,  -- send attempts, to avoid blocking on one block
    sq_creation_date varchar(32) NOT NULL,
    sq_last_modified varchar(32) NOT NULL
);
ALTER TABLE idev_sending_q ADD CONSTRAINT idev_sending_q_bsr UNIQUE (sq_type, sq_code, sq_sender, sq_receiver);


-- drop table idev_logs_trx_utxos;
CREATE TABLE idev_logs_trx_utxos
(
    ul_id bigserial primary key,
    ul_action varchar(16) NOT NULL,    -- added/deleted to/from table
    ul_timestamp timestamp without time zone default (now() at time zone 'utc'),    -- creation date of the block which mentioned in visible_by field
    ul_creation_date varchar(32) NOT NULL,    -- creation date of the block which mentioned in visible_by field
    -- ul_clone_code varchar(256) NOT NULL,    -- group code to avoid duplicate inputs
    ul_ref_loc varchar(512) NOT NULL,    -- ReferenceTransactionHash : ParseInt(OutputIndexNumber).toSting() // may be not usefull->: OutputAddress : ParseInt(OutputValue).toSting()
    ul_o_address varchar(256) NOT NULL,    -- owner's address
    ul_o_value BIGINT NOT NULL,    -- spendable value
    ul_visible_by varchar(256) NOT NULL,    -- the Block which has this transaction in it's history
    ul_ref_creation_date varchar(32) NOT NULL    -- the date the local machin insert it
);
CREATE INDEX index_logs_trx_utxos_ul_action ON idev_logs_trx_utxos(ul_action);
-- CREATE INDEX index_logs_trx_utxos_ul_clone_code ON idev_logs_trx_utxos(ul_clone_code);
CREATE INDEX index_logs_trx_utxos_ul_creation_date ON idev_logs_trx_utxos(ul_creation_date);
CREATE INDEX index_logs_trx_utxos_ul_ref_loc ON idev_logs_trx_utxos(ul_ref_loc);
CREATE INDEX index_logs_trx_utxos_ul_visible_by ON idev_logs_trx_utxos(ul_visible_by);
CREATE INDEX index_logs_trx_utxos_ul_o_address ON idev_logs_trx_utxos(ul_o_address);


-- trigger to add log
CREATE OR REPLACE FUNCTION added_utxo() RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO
        idev_logs_trx_utxos(ul_action, ul_creation_date, ul_ref_loc, ul_o_address, ul_o_value, ul_visible_by, ul_ref_creation_date)
        VALUES('add', new.ut_creation_date, new.ut_coin, new.ut_o_address, new.ut_o_value, new.ut_visible_by, new.ut_ref_creation_date);
           RETURN new;
END;
$BODY$
language plpgsql;
DROP TRIGGER IF EXISTS trigger_added_utxo ON i_trx_utxos;
CREATE TRIGGER trigger_added_utxo  AFTER INSERT ON i_trx_utxos FOR EACH ROW EXECUTE PROCEDURE added_utxo();
CREATE OR REPLACE FUNCTION deleted_utxo() RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO
        idev_logs_trx_utxos(ul_action, ul_creation_date, ul_ref_loc, ul_o_address, ul_o_value, ul_visible_by, ul_ref_creation_date)
        VALUES('del', old.ut_creation_date, old.ut_coin, old.ut_o_address, old.ut_o_value, old.ut_visible_by, old.ut_ref_creation_date);
           RETURN old;
END;
$BODY$
language plpgsql;
DROP TRIGGER IF EXISTS  trigger_deleted_utxo ON i_trx_utxos;
CREATE TRIGGER trigger_deleted_utxo BEFORE DELETE ON i_trx_utxos FOR EACH ROW EXECUTE PROCEDURE deleted_utxo();

-- insert into i_trx_utxos (ut_creation_date,ut_coin,ut_o_address,ut_o_value,ut_visible_by,ut_ref_creation_date)
-- values ('2019-01-14 09:45:34', 'refLoc', 'oAdd', '1212', 'utVis32', 'utRef');



-- CREATE TABLE idv_fake_inbox (
--     id bigserial primary key,               -- TODO: maybe un-necessary id and remove it later
--     title varchar(256) NULL,  -- block root hash
--     sender varchar(256) NULL,  -- block root hash
--     target varchar(256) NULL,  -- block root hash
--     message text NULL,                -- comma seperated ancestors root hash + 2 comma in start and end of atring
--     creation_date varchar(32) NOT NULL       -- the block creation date which stated by block creator
-- );



-- alter table i_kvalue alter column kv_value type text;
--alter table idev_logs_trx_utxos alter column ul_clone_code drop not null;
-- ALTER TABLE IF EXISTS i_machine_used_coins RENAME TO i_machine_used_coins;
-- ALTER TABLE i_machine_draft_proposals RENAME COLUMN pd_descriptions TO pd_description;
-- ALTER TABLE i_iname_records ADD COLUMN in_is_settled varchar(1) DEFAULT 'N';
-- ALTER TABLE i_trx_suspect_transactions ADD COLUMN st_logger_doc bigserial primary key;
-- ALTER TABLE i_trx_suspect_transactions ADD COLUMN st_logger_doc varchar(256) NOT NULL;
-- ALTER TABLE i_trx_spend RENAME COLUMN sp_ref_loc to sp_coin;



CREATE OR REPLACE FUNCTION emptydb() RETURNS void AS
$$ 
DELETE FROM i_administrative_pollings;
DELETE FROM i_administrative_refines_history;
DELETE FROM i_ballots;
DELETE FROM i_blocks;
-- alter table i_machine_buffer_documents alter column dp_cost type BIGINT;
DELETE FROM i_block_extinfos;
DELETE FROM i_collisions;
DELETE FROM i_custom_posts;
DELETE FROM i_demos_agoras;
DELETE FROM i_demos_posts;
DELETE FROM i_dna_shares;
DELETE FROM i_docs_blocks_map;
DELETE FROM i_pledged_accounts;
DELETE FROM i_iname_bindings;
DELETE FROM i_iname_records;
DELETE FROM i_kvalue;
DELETE FROM i_logs_block_import_report;
DELETE FROM i_logs_broadcast;
DELETE FROM i_logs_suspect_transactions;
DELETE FROM i_logs_time_locked;
DELETE FROM i_machine_ballots;
DELETE FROM i_machine_buffer_documents;
DELETE FROM i_machine_direct_messages;
DELETE FROM i_machine_draft_pledges;
DELETE FROM i_machine_draft_proposals;
DELETE FROM i_machine_iname_messages;
DELETE FROM i_machine_iname_bindings;
DELETE FROM i_machine_iname_records;
DELETE FROM i_machine_neighbors;
DELETE FROM i_machine_onchain_contracts;
DELETE FROM i_machine_posted_files;
DELETE FROM i_machine_profiles;
DELETE FROM i_machine_tmp_documents;
DELETE FROM i_machine_used_coins;
DELETE FROM i_machine_wallet_addresses;
DELETE FROM i_machine_wallet_funds;
DELETE FROM i_message_ticketing;
DELETE FROM i_missed_blocks;
DELETE FROM i_nodes_screenshots;
DELETE FROM i_parsing_q;
DELETE FROM i_pollings;
DELETE FROM i_polling_profiles;
DELETE FROM i_proposals;
DELETE FROM i_sending_q;
DELETE FROM i_signals;
DELETE FROM i_released_reserves;
-- DELETE FROM i_requests_for_release_reserved_coins;
DELETE FROM i_treasury;
DELETE FROM i_trx_rejected_transactions;
DELETE FROM i_trx_spend;
DELETE FROM i_trx_suspect_transactions;
DELETE FROM i_trx_output_time_locked;
DELETE FROM i_trx_utxos;
DELETE FROM i_wiki_contents;
DELETE FROM i_wiki_pages;
DELETE FROM idev_inbox_logs;
DELETE FROM idev_logs_trx_utxos;
DELETE FROM idev_parsing_q;
DELETE FROM idev_sending_q;
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dev_drop_img_tables() RETURNS void AS
$$ 
DROP TRIGGER trigger_added_utxo ON i_trx_utxos;
DROP TRIGGER trigger_i_dna_shares_calc_shares ON i_dna_shares;
DROP TABLE i_administrative_pollings;
DROP TABLE i_administrative_refines_history;
DROP TABLE i_ballots;
DROP TABLE i_collisions;
DROP TABLE i_custom_posts;
DROP TABLE i_block_extinfos;
DROP TABLE i_blocks;
DROP TABLE i_demos_agoras;
DROP TABLE i_demos_posts;
DROP TABLE i_dna_shares;
DROP TABLE i_docs_blocks_map;
DROP TABLE i_pledged_accounts;
DROP TABLE i_iname_bindings;
DROP TABLE i_iname_records;
DROP TABLE i_kvalue;
DROP TABLE i_logs_block_import_report;
DROP TABLE i_logs_broadcast;
DROP TABLE i_logs_suspect_transactions;
DROP TABLE i_logs_time_locked;
DROP TABLE i_machine_ballots;
DROP TABLE i_machine_buffer_documents;
DROP TABLE i_machine_direct_messages;
DROP TABLE i_machine_draft_pledges;
DROP TABLE i_machine_draft_proposals;
DROP TABLE i_machine_iname_messages;
DROP TABLE i_machine_iname_bindings;
DROP TABLE i_machine_iname_records;
DROP TABLE i_machine_neighbors;
DROP TABLE i_machine_onchain_contracts;
DROP TABLE i_machine_posted_files;
DROP TABLE i_machine_profiles;
DROP TABLE i_machine_tmp_documents;
DROP TABLE i_machine_used_coins;
DROP TABLE i_machine_wallet_addresses;
DROP TABLE i_machine_wallet_funds;
DROP TABLE i_message_ticketing;
DROP TABLE i_missed_blocks;
DROP TABLE i_nodes_screenshots;
DROP TABLE i_parsing_q;
DROP TABLE i_pollings;
DROP TABLE i_polling_profiles;
DROP TABLE i_proposals;
DROP TABLE i_released_reserves;
-- DROP TABLE i_requests_for_release_reserved_coins;
DROP TABLE i_sending_q;
DROP TABLE i_signals;
DROP TABLE i_treasury;
DROP TABLE i_trx_rejected_transactions;
DROP TABLE i_trx_spend;
DROP TABLE i_trx_suspect_transactions;
DROP TABLE i_trx_output_time_locked;
DROP TABLE i_trx_utxos;
DROP TABLE i_wiki_contents;
DROP TABLE i_wiki_pages;
DROP TABLE idev_inbox_logs;
DROP TABLE idev_logs_trx_utxos;
DROP TABLE idev_parsing_q;
DROP TABLE idev_sending_q;
$$ LANGUAGE sql;


CREATE OR REPLACE FUNCTION nodev_drop_img_tables() RETURNS void AS
$$ 
DROP TABLE i_administrative_pollings;
DROP TABLE i_administrative_refines_history;
DROP TABLE i_ballots;
DROP TABLE i_collisions;
DROP TABLE i_custom_posts;
DROP TABLE i_block_extinfos;
DROP TABLE i_blocks;
DROP TABLE i_demos_agoras;
DROP TABLE i_demos_posts;
DROP TABLE i_dna_shares;
DROP TABLE i_docs_blocks_map;
DROP TABLE i_pledged_accounts;
DROP TABLE i_iname_bindings;
DROP TABLE i_iname_records;
DROP TABLE i_kvalue;
DROP TABLE i_logs_block_import_report;
DROP TABLE i_logs_broadcast;
DROP TABLE i_logs_suspect_transactions;
DROP TABLE i_logs_time_locked;
DROP TABLE i_machine_ballots;
DROP TABLE i_machine_buffer_documents;
DROP TABLE i_machine_direct_messages;
DROP TABLE i_machine_draft_pledges;
DROP TABLE i_machine_draft_proposals;
DROP TABLE i_machine_iname_messages;
DROP TABLE i_machine_iname_bindings;
DROP TABLE i_machine_iname_records;
DROP TABLE i_machine_neighbors;
DROP TABLE i_machine_onchain_contracts;
DROP TABLE i_machine_posted_files;
DROP TABLE i_machine_profiles;
DROP TABLE i_machine_tmp_documents;
DROP TABLE i_machine_used_coins;
DROP TABLE i_machine_wallet_addresses;
DROP TABLE i_machine_wallet_funds;
DROP TABLE i_message_ticketing;
DROP TABLE i_missed_blocks;
DROP TABLE i_nodes_screenshots;
DROP TABLE i_parsing_q;
DROP TABLE i_pollings;
DROP TABLE i_polling_profiles;
DROP TABLE i_proposals;
DROP TABLE i_released_reserves;
DROP TABLE i_sending_q;
DROP TABLE i_signals;
DROP TABLE i_treasury;
DROP TABLE i_trx_rejected_transactions;
DROP TABLE i_trx_spend;
DROP TABLE i_trx_suspect_transactions;
DROP TABLE i_trx_output_time_locked;
DROP TABLE i_trx_utxos;
DROP TABLE i_wiki_contents;
DROP TABLE i_wiki_pages;
$$ LANGUAGE sql;
