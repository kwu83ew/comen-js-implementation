const _ = require('lodash')
const iutils = require('../utils/iutils')
const utils = require('../utils/utils')
const model = require('../models/interface')
const iConsts = require('../config/constants')
const clog = require('../loggers/console_logger');

const table = 'i_logs_broadcast';

// to log broadcasted packets to avoid duplication, improve performance...
class BroadcastLogger {

    static listSentBlocks(args = {}) {
        const machine = require('../machine/machine-handler');
        try {
            let fields = _.has(args, 'fields') ? args.fields : ' * '
            let afterThat = _.has(args, 'afterThat') ? args.afterThat : null;
            if (afterThat == null) {
                if (machine.isInSyncProcess()) {
                    afterThat = utils.minutesBefore(iConsts.getCycleByMinutes() / 80);
                } else {
                    afterThat = utils.minutesBefore(iConsts.getCycleByMinutes() / 20);
                }
            }
            return model.sRead({
                table,
                fields,
                query: [
                    ['lb_send_date', ['>=', afterThat]]
                ]
            })
        } catch (e) {
            throw new Error(e);
        }

    }

    static isBroadcasted(key) {
        let broadcastedBlocks = this.listSentBlocks({
            fields: ["lb_type", "lb_code", "lb_sender", "lb_receiver"]
        })
        let alreadyBroadcasted = []
        broadcastedBlocks.forEach(blk => {
            alreadyBroadcasted.push([blk.pb_type, blk.pb_code, blk.sender, blk.receiver].join())
        });
        // clog.app.info(`alreadyBroadcasted: ${utils.stringify(alreadyBroadcasted)}`);
        return (alreadyBroadcasted.includes(key))
    }

    static addSentBlock(values) {

        try {
            values.lb_send_date = _.has(values, 'lb_send_date') ? values.lb_send_date : utils.getNow();
            clog.app.info(`add SentBlock: ${utils.stringify(values)}`);
            model.sCreate({
                table,
                values
            });

        } catch (e) {
            throw new Error(e);
        }
    }

    static async dropBroadcastLogs(values) {
        model.aDelete({
            table,
            values
        });
        return { err: false, msg:'The Broadcast Logs is Empty' }
    }

}

module.exports = BroadcastLogger
