const app = require('debug')('app');
const http = require('debug')('http');
const sql = require('debug')('sql');
const sec = require('debug')('sec'); // security issues
const iConsts = require('../config/constants');



// const winston = require('winston')
const { createLogger, format, transports } = require('winston');

class WLogger {
    // static levels = ['error', 'warn', 'info', 'verbose', 'debug', 'silly'];

    constructor(errLogger) {
        this.errLogger = errLogger;
    }

    // supper logs
    static getLoggerSuperLog() {
        if (this.loggerSuperLog == undefined) {
            this.setLoggerSuperLog()
        }
        return this.logObjCreate(this.loggerSuperLog, 'isSuperLog')
    }

    static setLoggerSuperLog() {
        let filePostfix = this.getFilePostfix()
        this.loggerSuperLog = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                //
                // - Write to all logs with level `info` and below to `combined.log` 
                // - Write all logs error (and below) to `error.log`.
                //
                // new transports.File({ filename: `../../iserver-error${filePostfix}.log`, level: 'error' }),
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/superlog${filePostfix}` })
            ]
        });
    }

    // supper errors logs
    static getLoggerSuperError() {
        if (this.loggerSuperError == undefined) {
            this.setLoggerSuperError()
        }
        return this.logObjCreate(this.loggerSuperError, 'isSUperError')
    }

    static setLoggerSuperError() {
        let filePostfix = this.getFilePostfix()
        this.loggerSuperError = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                //
                // - Write to all logs with level `info` and below to `combined.log` 
                // - Write all logs error (and below) to `error.log`.
                //
                // new transports.File({ filename: `../../iserver-error${filePostfix}.log`, level: 'error' }),
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/supererror${filePostfix}` })
            ]
        });
    }

    // application logs
    static getLoggerApp() {
        if (this.loggerApp == undefined) {
            this.setLoggerApp()
        }
        return this.logObjCreate(this.loggerApp, 'App')
    }

    static setLoggerApp() {
        let filePostfix = this.getFilePostfix()
        this.loggerApp = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                //
                // - Write to all logs with level `info` and below to `combined.log` 
                // - Write all logs error (and below) to `error.log`.
                //
                // new transports.File({ filename: `../../iserver-error${filePostfix}.log`, level: 'error' }),
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/app${filePostfix}` })
            ]
        });
        // this.loggerApp.add(new transports.Console({
        //     format: format.simple()
        // }));
    }

    // sql logs
    static getLoggerSQL() {
        if (this.loggerSQL == undefined) {
            this.setLoggerSQL()
        }
        return this.logObjCreate(this.loggerSQL, 'SQL')
    }

    static setLoggerSQL() {
        let filePostfix = this.getFilePostfix()
        this.loggerSQL = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/sql${filePostfix}` })
            ]
        });
    }

    // HTTP log
    static getLoggerHTTP() {
        if (this.loggerHTTP == undefined) {
            this.setLoggerHTTP()
        }
        return this.logObjCreate(this.loggerHTTP, 'HTTP')
    }

    static setLoggerHTTP() {
        let filePostfix = this.getFilePostfix()
        this.loggerHTTP = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/http${filePostfix}` })
            ]
        });
    }

    // CoinBase log
    static getLoggerCB() {
        if (this.loggerCB == undefined) {
            this.setLoggerCB()
        }
        return this.logObjCreate(this.loggerCB, 'CB')
    }

    static setLoggerCB() {
        let filePostfix = this.getFilePostfix()
        this.loggerCB = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/cb${filePostfix}` })
            ]
        });
    }

    // Transaction log
    static getLoggerTrx() {
        if (this.loggerTrx == undefined) {
            this.setLoggerTrx()
        }
        return this.logObjCreate(this.loggerTrx, 'Trx')
    }

    static setLoggerTrx() {
        let filePostfix = this.getFilePostfix()
        this.loggerTrx = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/trx${filePostfix}` })
            ]
        });
    }

    // secusrity logs
    static getLoggerSec() {
        if (this.loggerSec == undefined) {
            this.setLoggerSec()
        }
        return this.logObjCreate(this.loggerSec, 'Sec')
    }

    static setLoggerSec() {
        let filePostfix = this.getFilePostfix()
        this.loggerSec = createLogger({
            level: 'verbose',
            // format: format.simple(),
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({ filename: `${iConsts.HD_PATHES.logs_path}/sec${filePostfix}` })
            ]
        });
    }



    static logObjCreate(lObj, caller = '') {
        return {
            error: function (msg) {
                lObj.log('error', msg);
                if ((caller != 'isSUperError') && (caller != 'isSuperLog'))
                    WLogger.getLoggerSuperError().error(msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().error(caller + ' ' + msg);
            },
            warn: function (msg) {
                lObj.log('warn', msg);
                if ((caller != 'isSUperError') && (caller != 'isSuperLog'))
                    WLogger.getLoggerSuperError().warn(msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().warn(caller + ' ' + msg);
            },
            info: function (msg) {
                lObj.log('info', msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().info(caller + ' ' + msg);
            },
            verbose: function (msg) {
                lObj.log('verbose', msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().verbose(caller + ' ' + msg);
            },
            debug: function (msg) {
                lObj.log('debug', msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().debug(caller + ' ' + msg);
            },
            // dummy wrapper
            log: function (msg) {
                lObj.log('debug', msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().log(caller + ' ' + msg);
            },
            silly: function (msg) {
                lObj.log('silly', msg);
                if (caller != 'isSuperLog')
                    WLogger.getLoggerSuperLog().silly(caller + ' ' + msg);
            }
        }
    }

    static getAppClone() {
        if (this.acim === undefined)
            this.acim = require("../startup/app-clone");
        return this.acim.getAppCloneId()
    }

    static getFilePostfix() {
        let filePostfix = this.getAppClone().toString()
        if (filePostfix == '0')
            filePostfix = ''
        else {
            filePostfix = '-' + filePostfix
        }
        return filePostfix
    }
}

let logToFile = true;

module.exports = {
    app: logToFile ? WLogger.getLoggerApp() : app,
    cb: logToFile ? WLogger.getLoggerCB() : app,
    trx: logToFile ? WLogger.getLoggerTrx() : app,
    http: logToFile ? WLogger.getLoggerHTTP() : http,
    sql: logToFile ? WLogger.getLoggerSQL() : sql,
    sec: logToFile ? WLogger.getLoggerSec() : sec,
};
//DEBUG=http,sql,err nodemon index.js