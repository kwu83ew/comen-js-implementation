const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const model = require('../models/interface');
const clog = require('../loggers/console_logger');
const machine = require('../machine/machine-handler');
const getMoment = require('../startup/singleton').instance.getMoment;
const Moment = require('../startup/singleton').instance.Moment;
const crypto = require('../crypto/crypto');

// the ONLY rule
// shares are creating/increasing by proposals. every body can send a proposal and the current share holders 
// vote to approve or invalidate the proposal.
// who (or a group of person) has any percentage of shares (valid in last 12 hours) can vote for a proposal 
// in voting-valid-dates.
// after finish voting time-window, the nodes calculate the proposal votes and it finishs with accept proposal 
// and create/increase a shareholders stock or denay the proposal

const table = 'i_dna_shares';
const initShareDate = Moment(iConsts.getLaunchDate(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': 2 * iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");



class DNAHandler {

    static getDNATmpShareDoc() {
        let dnaShareDoc = {
            dType: iConsts.DOC_TYPES.DNAProposal,
            dClass: iConsts.PROPOSAL_CLASESS.General,
            dVer: "0.0.7",
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            title: "",
            descriptions: "",
            tags: "",
            projectHash: iutils.convertTitleToHash('imagine'), // it must be sha3(imagine) intended for imagine main dna, later we can use this ground for public use
            creationDate: "", // this is proposal creation time
            approvalDate: "", // the approval time, when the proposal approved, will be insert both to table i_dna_shares
            shareholder: "",    // the bech32 address of shareholder
            helpHours: 0,
            helpLevel: 0, // how important/usefull was this help? 1-12
            shares: 0, // shares = helpHours * helpLevel
            votesY: 0,
            votesN: 0,
            votesA: 0
        };
        return _.cloneDeep(dnaShareDoc);
    }

    static getInitialDNADoc() {
        const dnaShareDoc = {
            dType: iConsts.DOC_TYPES.DNAProposal,
            dClass: iConsts.PROPOSAL_CLASESS.General,
            dVer: "0.0.7",
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            title: "fair effort, fair gain, win win win",
            descriptions: "imagine's contributors time & effort is recorded here",
            tags: "initialize, DNA",
            projectHash: iutils.convertTitleToHash('imagine'), // it must be sha3(imagine) intended for imagine main dna, later we can use this ground for public use
            creationDate: initShareDate, // this is proposal creation time, the approval time, when the proposal approved, will be insert both to table i_dna_shares
            approvalDate: initShareDate, // this is proposal creation time, the approval time, when the proposal approved, will be insert both to table i_dna_shares
            shareholder: iConsts.HU_DNA_SHARE_ADDRESS,
            helpHours: 1000000,
            helpLevel: 1, // how important/usefull was this help? 1-12
            shares: 1000000, // shares = helpHours * helpLevel
            votesY: 1000000,
            votesN: 0,
            votesA: 0
        };
        let doc = _.cloneDeep(dnaShareDoc);
        doc.shares = doc.helpHours * doc.helpLevel;
        doc.hash = DNAHandler.hashDNARegDoc(doc);
        return doc;
    }

    static DNATruncateToBroadcast(dna) {
        //each node independentely must canculate/extract these information, so we do not broadcast it
        try { delete dna.approvalDate; } catch (e) { }
        try { delete dna.votesY; } catch (e) { }
        try { delete dna.votesN; } catch (e) { }
        try { delete dna.votesA; } catch (e) { }
        return block;
    }

    /**
     * 
     * @param {*} _t 
     * given time(DNA proposal approing time), it returns the range in which a share is valid
     * the active period starts from 7 years ago and finishes right at the end of previous cycle time
     * it means if your proposal have been approved in 2017-01-01 00:00:00, the owner can gain from 2017-01-01 12:00:00 to 2024-01-01 00:00:00
     */
    static getDNAActiveDateRange(cDate) {
        let {
            minCreationDate,
            maxCreationDate
        } = iutils.getACycleRange({
            cDate,
            backByCycle: iConsts.SHARE_MATURITY_CYCLE
        });
        if (iConsts.TIME_GAIN == 1) {
            minCreationDate = utils.yearsBefore(iConsts.CONTRIBUTION_APPRECIATING_PERIOD, minCreationDate)
        } else {
            minCreationDate = utils.minutesBefore(100 * iConsts.getCycleByMinutes(), minCreationDate)
        }
        // console.log('minCreationDate, maxCreationDate', minCreationDate, maxCreationDate);
        return { minCreationDate, maxCreationDate };
    }

    static DNAProposalPresenter(doc, indetns = '') {
        let out = '';
        out += '\n' + indetns + 'Title: ' + doc.title
        out += '\n' + indetns + 'Hash: ' + doc.hash
        out += '\n' + indetns + 'Type: ' + doc.dType + ' \tVersion: ' + doc.version;
        out += '\n' + indetns + 'Descriptions: ' + doc.descriptions
        out += '\n' + indetns + 'Tags: ' + doc.tags
        out += '\n' + indetns + 'CreationDate: ' + doc.creationDate
        out += '\n' + indetns + 'contributor Address: ' + doc.shareholder
        out += '\n' + indetns + utils.microPAIToPAI(doc.helpHours) + ' (Hours) * ' + doc.helpLevel + ' (Usefullness level) => ' + utils.microPAIToPAI(doc.shares) + ' shares';
        return out;
    }


    static extractDNARegDocHashableParts(doc) {
        let hashable = {
            creationDate: doc.creationDate,
            descriptions: doc.descriptions,
            dType: doc.dType,
            dVer: doc.dVer,
            helpHours: doc.helpHours,
            helpLevel: doc.helpLevel,
            net: doc.net,
            projectHash: doc.projectHash,
            shareholder: doc.shareholder,
            tags: doc.tags,
            title: doc.title,
        };
        hashable.shares = hashable.helpHours * hashable.helpLevel;
        return hashable;
    }

    static hashDNARegDoc(doc) {
        let hashable = DNAHandler.extractDNARegDocHashableParts(doc);
        return crypto.keccak256(JSON.stringify(hashable));
    }

    static prepareQuery(dna) {
        utils.exitIfGreaterThanNow(dna.creationDate);
        utils.exitIfGreaterThanNow(dna.approvalDate);
        dna.helpLevel = Math.trunc(dna.helpLevel);

        let args = {
            dn_doc_hash: dna.hash,
            dn_shareholder: dna.shareholder,
            dn_project_hash: dna.projectHash,
            dn_help_hours: dna.helpHours,
            dn_help_level: dna.helpLevel,
            dn_shares: dna.helpHours * dna.helpLevel,
            dn_votes_y: dna.votesY,
            dn_votes_n: dna.votesN,
            dn_votes_a: dna.votesA,
            dn_title: dna.title,
            dn_descriptions: dna.descriptions,
            dn_tags: dna.tags,
            dn_creation_date: dna.creationDate
        };
        return args;
    }

    static searchInDNA(args = {}) {
        args.table = table;
        let res = model.sRead(args);
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static updateDNAVotes(args) {
        clog.app.info(`update DNA Votes args ${utils.stringify(args)}`);
        model.sUpdate({
            table,
            query: args.query,
            updates: args.updates
        });
    }

    static insertAShare(dna) {
        let values = DNAHandler.prepareQuery(dna);
        let exist = model.sRead({
            table,
            query: [['dn_doc_hash', dna.hash]]      // dna.hash is equal to proposal.hash (if the proposal get approved)
        });
        if (exist.length > 0) {
            // maybe some updates
            return { err: true, msg: `the DNA document (${utils.hash6c(dna.hash)}) is already recorded` }
        }
        model.sCreate({
            table,
            values
        });
        return { err: false }
    }

    static getAnAddressShares(address, cDate = null) {
        let { sumShares, holdersByKey } = DNAHandler.getSharesInfo(cDate);
        let shares = _.has(holdersByKey, address) ? holdersByKey[address] : 0.0;
        let percentage = _.has(holdersByKey, address) ? ((shares * 100) / sumShares) : 0.0;
        percentage = utils.iFloorFloat(percentage);
        return { shares, percentage };
    }

    static getMachineShares(cDate = null) {
        let { sumShares, holdersByKey } = DNAHandler.getSharesInfo(cDate);
        let backerAddress = machine.getMProfileSettingsSync().backerAddress;
        let shares = _.has(holdersByKey, backerAddress) ? holdersByKey[backerAddress] : 0;
        let percentage = utils.iFloorFloat((shares / sumShares) * 100);
        return { backer: backerAddress, shares, percentage }
    }


    // TODO: since shares are counting for before 2 last cycles, so implementing a caching system will be much helpfull
    static getSharesInfo(cDate = null) {
        // retrieve the total shares in last 24 hours, means -36 to -24 based on greenwich time 
        // (Note: it is not the machine local time)
        // for examplie if a node runs this command on 7 May at 14 (in greenwich time) 
        // the result will be the final state of DNA at 11:59:59 of 6 May.
        // it means the node calculate all shares which the creation date are less than 11:59:59  of 6 May
        // -------------< 11:59 of 6 May |         --- 24 hours ---        |12:00 of 7 May     --- 2 hours ---     14:00 of 7 May

        if (cDate === null)
            cDate = utils.getNow();
        clog.app.info(`getShare Sync: calc shares for date ${cDate} `);

        let { minCreationDate, maxCreationDate } = DNAHandler.getDNAActiveDateRange(cDate);
        let query = `
            SELECT dn_shareholder _shareholder, SUM(dn_shares) _share FROM i_dna_shares 
            WHERE dn_creation_date between $1 AND $2 GROUP BY _shareholder ORDER BY _share DESC`;
        clog.app.info(`Retrieve shares for range ${cDate} -> (${minCreationDate} ${maxCreationDate})`);
        // let msg = `Retrieve shares: SELECT shareholder _shareholder, SUM(shares) _share FROM i_dna_shares WHERE creation_date between '${minCreationDate}' AND '${maxCreationDate}' GROUP BY _shareholder ORDER BY _share DESC `;
        let shares = model.sCustom({
            query,
            values: [minCreationDate, maxCreationDate]
        });

        // let shares = await model.getShares(cDate);
        let sumShares = 0;
        let holdersOrderByShares = [];
        let holdersByKey = {}
        for (let aSHare of shares) {
            sumShares += parseInt(aSHare._share);
            holdersByKey[aSHare._shareholder] = parseInt(aSHare._share);
            holdersOrderByShares.push({
                account: aSHare._shareholder,
                shares: holdersByKey[aSHare._shareholder]
            })
        }
        return { sumShares, holdersByKey, holdersOrderByShares };
    }

    //////////////////// interface methods//////////////////

    static async getShareAsync(cDate = null) {
        // retrieve the total shares in last 24 hours, means -36 to -24 based on greenwich time (Note: it is not the machine local time)
        // for examplie if a node runs this command on 7 May at 14 (in greenwich time) the result will be the final state of DNA at 11:59:59 of 6 May.
        // it means the node calculate all shares which the creation date are less than 11:59:59  of 6 May
        // -------------< 11:59 of 6 May |         --- 24 hours ---        |12:00 of 7 May     --- 2 hours ---     14:00 of 7 May
        try {

            if (cDate === null)
                cDate = getMoment().format('YYYY-MM-DD HH:mm:ss');
            clog.app.info(`getShare Async: calc shares for date ${cDate} `);

            let { minCreationDate, maxCreationDate } = DNAHandler.getDNAActiveDateRange(cDate);
            let query = 'SELECT dn_shareholder _shareholder, SUM(dn_shares) _share FROM i_dna_shares WHERE dn_creation_date between $1 AND $2 GROUP BY _shareholder ORDER BY _share DESC';
            clog.app.info('>>>>>>>>>sssssssssssss>>>>>>>>>>>>>>>>>>>>>');
            clog.app.info(query, minCreationDate, maxCreationDate);
            let shares;
            shares = await model.aCustom({ query: query, values: [minCreationDate, maxCreationDate] });

            // let shares = await model.getShares(cDate);
            let sumShares = 0;
            let holdersByKey = {}
            for (let aSHare in shares) {
                // clog.app.info('================');
                // clog.app.info(aSHare);
                sumShares += parseInt(aSHare._share);
                holdersByKey[aSHare._shareholder] = parseInt(aSHare._share);
            };
            // clog.app.info(`+++++++++++++++++sumShares: ${sumShares}`);
            return { sumShares, holdersByKey };
        } catch (e) {
            throw new Error(e);
        }

    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'dn_doc_hash'))
            out.dnDocHash = elm.dn_doc_hash;
        if (_.has(elm, 'dn_shareholder'))
            out.dnShareholder = elm.dn_shareholder;
        if (_.has(elm, 'dn_project_hash'))
            out.dnProjectHash = elm.dn_project_hash;
        if (_.has(elm, 'dn_help_hours'))
            out.dnHelpHours = elm.dn_help_hours;
        if (_.has(elm, 'dn_help_level'))
            out.dnHelpLevel = elm.dn_help_level;
        if (_.has(elm, 'dn_shares'))
            out.dnShares = elm.dn_shares;
        if (_.has(elm, 'dn_title'))
            out.dnTitle = elm.dn_title;
        if (_.has(elm, 'dn_descriptions'))
            out.dnDescriptions = elm.dn_descriptions;
        if (_.has(elm, 'dn_tags'))
            out.dnTags = elm.dn_tags;
        if (_.has(elm, 'dn_votes_y'))
            out.dnVotesY = elm.dn_votes_y;
        if (_.has(elm, 'dn_votes_n'))
            out.dnVotesN = elm.dn_votes_n;
        if (_.has(elm, 'dn_votes_a'))
            out.dnVotesA = elm.dn_votes_a;
        if (_.has(elm, 'dn_creation_date'))
            out.dnCreationDate = elm.dn_creation_date;
        return out;
    }

}

module.exports = DNAHandler;
