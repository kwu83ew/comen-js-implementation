const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const DNAHandler = require('../../dna/dna-handler');
const pollHandler = require('../../services/polling-handler/general-poll-handler');
const machine = require('../../machine/machine-handler');

const tableDrafts = 'i_machine_draft_proposals';
const tableOnchainProposals = 'i_proposals';


class ProposalHandler {

    static concludeProposal(args) {
        let polling = args.polling;
        let proposalHash = polling.pllRef;

        // update proposal 
        model.sUpdate({
            table: tableOnchainProposals,
            query: [
                ['pr_hash', proposalHash]
            ],
            updates: {
                pr_conclude_date: args.approveDate,
                pr_approved: iConsts.CONSTS.NO
            }
        });
        return { err: false }
    }

    static transformApprovedProposalToDNAShares(args) {
        let msg;
        let dnProjectHash = _.has(args, 'dnProjectHash') ? args.dnProjectHash : iutils.convertTitleToHash('imagine');
        let polling = args.polling;
        let proposalHash = polling.pllRef;

        let proposal = this.searchInProposals({ query: [['pr_hash', proposalHash]] });
        if (proposal.length == 0) {
            msg = `proposal(${utils.hash6c(proposalHash)}) doesn't exisat in recorded proposals`;
            clog.app.error(msg)
            return { err: true, msg }
        }
        proposal = proposal[0];

        let nowT = utils.getNow();

        proposal.prHelpLevel = Math.trunc(proposal.prHelpLevel);
        // insert in DNA

        let dnaDoc = {
            hash: proposal.prHash,
            shareholder: proposal.prCotributerAccount,
            projectHash: dnProjectHash,
            helpHours: proposal.prHelpHours,
            helpLevel: proposal.prHelpLevel,
            shares: Math.floor(proposal.prHelpHours * proposal.prHelpLevel),
            title: proposal.prTitle,
            descriptions: proposal.prDescription,
            tags: proposal.prTags,
            votesY: polling.pllYValue,
            votesA: polling.pllAValue,
            votesN: polling.pllNValue,
            creationDate: args.approveDate
        };
        clog.app.info(`adding shares because of proposal(${utils.hash6c(proposal.prHash)}): ${utils.stringify(dnaDoc)}`);

        if (machine.isInSyncProcess()) {
            let exist = DNAHandler.searchInDNA({
                query: [['dn_doc_hash', proposal.prHash]]
            });
            if (exist.length == 0) {
                DNAHandler.insertAShare(dnaDoc);
            } else {
                // update votes
                DNAHandler.updateDNAVotes({
                    query: [['dn_doc_hash', proposal.prHash]],
                    updates: {
                        dn_votes_y: polling.pllYValue,
                        dn_votes_a: polling.pllAValue,
                        dn_votes_n: polling.pllNValue
                    }
                });
            }

        } else {
            DNAHandler.insertAShare(dnaDoc);
        }

        // update proposal too
        model.sUpdate({
            table: tableOnchainProposals,
            query: [
                ['pr_hash', proposal.prHash]
            ],
            updates: {
                pr_conclude_date: args.approveDate,
                pr_approved: iConsts.CONSTS.YES
            }
        });

    }

    static updateProposal(args) {
        return model.sUpdate({
            table: tableOnchainProposals,
            query: args.query,
            updates: args.updates
        });
    }

    static searchInProposals(args) {
        args.table = tableOnchainProposals;
        let res = model.sRead(args);
        res = res.map(x => this.convertFieldsOnchainRecords(x));
        return res;
    }

    /**
     * 
     * @param {*} args 
     * creates a draftproposal and save it i_
     * 
     */
    static async prepareDraftProposal(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let pd_type = _.has(args, 'pdType') ? args.pdType : iConsts.DOC_TYPES.DNAProposal;
        let pd_class = _.has(args, 'pdClass') ? args.pdClass : iConsts.PROPOSAL_CLASESS.General;
        let pd_version = _.has(args, 'pdVersion') ? args.pdVersion : '0.0.8';
        let pd_title = _.has(args, 'pdTitle') ? args.pdTitle : 'Un-titled';
        let pd_description = _.has(args, 'pdDescription') ? args.pdDescription : '';
        let pd_tags = _.has(args, 'pdTags') ? args.pdTags : '';
        let pd_project_hash = _.has(args, 'pdProjectHash') ? args.pdProjectHash : iutils.convertTitleToHash('imagine');
        let pd_creation_date = _.has(args, 'pdCreationDate') ? args.pdCreationDate : utils.getNow();
        let pd_help_hours = _.has(args, 'pdHelpHours') ? args.pdHelpHours : 0;
        let pd_help_level = _.has(args, 'pdHelpLevel') ? args.pdHelpLevel : 0;
        let pd_voting_longevity = _.has(args, 'pdVotingLongevity') ? args.pdVotingLongevity : 0;
        if (iConsts.TIME_GAIN == 1)
            pd_voting_longevity = Math.trunc(pd_voting_longevity);
        let pd_polling_profile = _.has(args, 'pdPollingProfile') ? args.pdPollingProfile : 'Unknown';
        let pd_cotributer_account = _.has(args, 'pdCotributerAccount') ? args.pdCotributerAccount : '';

        let draft = this.getDraftTpl();
        draft.dType = pd_type;
        draft.dClass = pd_class;
        draft.dVer = pd_version;
        draft.title = utils.sanitizingContent(pd_title);
        draft.descriptions = utils.sanitizingContent(pd_description);
        draft.tags = utils.sanitizingContent(pd_tags);
        draft.projectHash = pd_project_hash;
        draft.creationDate = pd_creation_date;
        draft.helpHours = pd_help_hours;
        draft.helpLevel = pd_help_level;
        draft.votingLongevity = pd_voting_longevity;
        draft.pollingProfile = pd_polling_profile;
        draft.pollingVersion = '0.0.8';
        draft.shareholder = pd_cotributer_account;
        draft.hash = "0000000000000000000000000000000000000000000000000000000000000000";
        draft.dLen = iutils.paddingDocLength(utils.stringify(draft).length);
        draft.hash = this.doHashProposal(draft);
        let pd_hash = draft.hash;

        model.sCreate({
            table: tableDrafts,
            values: {
                pd_mp_code: mpCode,
                pd_hash,
                pd_type,
                pd_class,
                pd_version,
                pd_title,
                pd_description,
                pd_tags,
                pd_project_hash,
                pd_help_hours,
                pd_help_level,
                pd_polling_profile,
                pd_voting_longevity,
                pd_cotributer_account,
                pd_body: JSON.stringify(draft),
                pd_creation_date
            },
        })
        clog.app.info(`prepare Draft Proposal2 args: ${utils.stringify(args)}`);
        return { err: false };
    }

    static deleteDraftProposal(pdId) {
        let mpCode = iutils.getSelectedMProfile();
        model.sDelete({
            table: tableDrafts,
            query: [
                ['pd_mp_code', mpCode],
                ['pd_id', pdId],
            ]
        })
    }

    static calcProposalDocumetnCost(args) {
        let proposal = args.proposal;
        let cDate = args.cDate;

        let dLen = parseInt(proposal.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: proposal.dType, dClass: proposal.dClass, dLen });

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: proposal.dType,
                dClass: proposal.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };

    }

    static calcProposalApplyCost(args) {
        clog.app.info(`calc ProposalApplyCost args ${utils.stringify(args)}`);
        let cCDate = (_.has(args, 'cCDate')) ? args.cCDate : null;    // the creation date of the block in which contribute is recorded (start date)

        let theContribute = args.helpHours * args.helpLevel;
        let incomeInfo = iutils.predictFutureIncomes({
            cCDate,
            months: 1,
            annualContributeGrowthRate: 100,
            theContribute
        });
        clog.app.info(`iutils.predictFutureIncomes res incomeInfo ${utils.stringify(incomeInfo)}`);

        let oneCycleIncome = incomeInfo.monthlyIncomes[0].oneCycleIncome;
        let applyCost = incomeInfo.monthlyIncomes[0].incomePerMonth * iConsts.PROPOSAL_APPLY_COST_SCALE; // 3 * first month incom
        return { oneCycleIncome, applyCost };
    }

    static extractDraftByHash(dHash) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let res = model.sRead({
            table: tableDrafts,
            query: [
                ['pd_mp_code', mpCode],
                ['pd_hash', dHash]
            ]
        });
        if (res.length == 1)
            return res[0];
        return null;
    }

    static getDraftTpl() {
        let dnaDraftProposal = {
            dType: iConsts.DOC_TYPES.DNAProposal,
            dClass: iConsts.PROPOSAL_CLASESS.General,
            dLen: "0000000",
            dVer: "0.0.8",
            hash: "0000000000000000000000000000000000000000000000000000000000000000",
            title: "",
            descriptions: "",
            tags: "",
            projectHash: iutils.convertTitleToHash('imagine'), // it must be sha3(imagine) intended for imagine main dna, later we can use this ground for public use
            creationDate: "", // this is proposal creation time
            helpHours: 0,
            helpLevel: 0, // how important/usefull was this help? 1-12
            shareholder: '',
            votingLongevity: iConsts.getMinVotingLongevity(),
            pollingProfile: iConsts.BASIC_POLLING_PROFILE_HASH,
            dExtInfo: "NOE",
            dExtHash: "NOE"
        };
        dnaDraftProposal.dVer = '0.0.8';
        return _.cloneDeep(dnaDraftProposal);
    }

    static doHashProposal(draft) {
        let hashableDraft = this.extractHashableParts(draft);
        return iutils.doHashObject(hashableDraft);
    }

    static extractHashableParts(draft) {
        // as always ordering properties by alphabet
        return {
            creationDate: draft.creationDate,
            descriptions: draft.descriptions,
            dClass: draft.dClass,
            dLen: draft.dLen,
            dType: draft.dType,
            helpHours: draft.helpHours,
            helpLevel: draft.helpLevel,
            projectHash: draft.projectHash,
            tags: draft.tags,
            shareholder: draft.shareholder,
            title: draft.title,
            dVer: draft.dVer,
            votingLongevity: draft.votingLongevity,
            pollingProfile: draft.pollingProfile
        }
    }

    static async loadMyDraftProposals(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let res = model.sRead({
            table: tableDrafts,
            query: [['pd_mp_code', mpCode]],
            order: [['pd_creation_date', 'ASC']]
        });
        if (res.length == 0)
            return [];

        let { sumShares, holdersByKey } = DNAHandler.getSharesInfo();
        // console.log('sumShares', sumShares);
        // console.log('holdersByKey', holdersByKey);
        const pledgeHandler = require("../../contracts/pledge-contract/pledge-handler");

        let finalRows = [];
        for (let aPropos of res) {
            aPropos.sumShares = sumShares;
            if (_.has(holdersByKey, aPropos.pd_cotributer_account)) {
                aPropos.currentShares = holdersByKey[aPropos.pd_cotributer_account];
            } else {
                aPropos.currentShares = 0;
            }
            aPropos.type = 'proposal';
            finalRows.push(aPropos);


            let draftPledges = pledgeHandler.searchInDraftPledges({
                dpl_proposal_ref: aPropos.pd_hash
            });
            if (draftPledges.length > 0)
                for (let aPlg of draftPledges) {
                    aPlg.type = 'pledgeReq';
                    aPlg.dplBody = utils.parse(aPlg.dplBody);
                    // console.log('aPlg.dplPledgee', aPlg.dplPledgee, `#${utils.hash6c(crypto.keccak256(aPlg.dplPledgee))}`);
                    aPlg.dplPledgeeColor = `#${utils.hash6c(crypto.keccak256(aPlg.dplPledgee))}`;
                    finalRows.push(aPlg);
                }
        }
        // console.log(' loadMyDraftProposals res', finalRows);
        return finalRows;
    }

    static searchInDraftProposal(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'pd_mp_code'))
            query.push(['pd_mp_code', mpCode]);
        let res = model.sRead({
            table: tableDrafts,
            query
        });
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFieldsDraftTbl(x));
        return res;
    }






    // recorded proposals part
    static recordANewProposal(args) {
        let msg;
        clog.app.info(`record A New Proposal args: ${utils.stringify(args)}`);
        let block = args.block;
        let proposal = args.proposal;

        let dbl = model.sRead({
            table: tableOnchainProposals,
            query: [['pr_hash', proposal.hash]]
        });
        if (dbl.length > 0) {
            msg = `try to double insert existed proposal ${utils.hash6c(proposal.hash)}`;
            clog.sec.error(msg);
            return { err: false, msg }
        }
        proposal.helpLevel = Math.trunc(proposal.helpLevel);

        let pr_hash = proposal.hash;
        let pr_type = proposal.dType;
        let pr_class = proposal.dClass;
        let pr_version = proposal.dVer;
        let pr_title = proposal.title;
        let pr_description = proposal.descriptions;
        let pr_tags = proposal.tags;
        let pr_project_id = proposal.projectHash;
        let pr_help_hours = proposal.helpHours;
        let pr_help_level = proposal.helpLevel;
        let pr_voting_longevity = proposal.votingLongevity;
        let pr_polling_profile = proposal.pollingProfile;
        let pr_cotributer_account = proposal.shareholder;
        let pr_start_voting_date = block.creationDate;
        let pr_conclude_date = '';
        let pr_approved = iConsts.CONSTS.NO;

        let values = {
            pr_hash,
            pr_type,
            pr_class,
            pr_version,
            pr_title,
            pr_description,
            pr_tags,
            pr_project_id,
            pr_help_hours,
            pr_help_level,
            pr_voting_longevity,
            pr_polling_profile,
            pr_cotributer_account,
            pr_start_voting_date,
            pr_conclude_date,
            pr_approved
        };
        console.log(`new proposal is creates args: ${utils.stringify(values)}`);
        model.sCreate({
            table: tableOnchainProposals,
            values
        })
        clog.app.info(`new proposal is creates args: ${utils.stringify(values)}`);
        return;
    }

    static getOnchainProposalsList(args) {
        let voter = _.has(args, 'voter') ? args.voter : false;

        // retrieve machine votes
        let votes = pollHandler.ballotHandler.searchInLocalBallot();
        let votesDict = {}
        for (let aVote of votes)
            votesDict[aVote.lbtpllHash] = aVote;
        // console.log('votes votes votes ', votes);
        // console.log('votesDict votes Dict ', votesDict);

        let cusQ = `
            SELECT ppr.ppr_name, ppr.ppr_perform_type, ppr.ppr_votes_counting_method, 

            pr.pr_hash, pr.pr_title, pr.pr_description, pr.pr_tags, pr.pr_help_hours, pr.pr_help_level, 
            pr.pr_cotributer_account, 

            pll.pll_hash, pll.pll_start_date, pll.pll_longevity, pll.pll_status, pll.pll_ct_done,  
            pll.pll_y_count, pll.pll_n_count, pll.pll_a_count, 
            pll.pll_y_shares, pll.pll_n_shares, pll.pll_a_shares, 
            pll.pll_y_gain, pll.pll_n_gain, pll.pll_a_gain, 
            pll.pll_y_value, pll.pll_n_value, pll.pll_a_value 
            
            
            FROM i_pollings pll 
            JOIN i_polling_profiles ppr ON ppr.ppr_name=pll.pll_class 
            JOIN i_proposals pr ON pr.pr_hash = pll.pll_ref 
            
            WHERE pll.pll_ref_type=$1 AND ppr.ppr_name=$2 
            ORDER BY pll.pll_start_date
        `;

        let res = model.sCustom({
            query: cusQ,
            values: [iConsts.POLLING_REF_TYPE.Proposal, iConsts.POLLING_PROFILE_CLASSES.Basic.ppName]
        });
        for (let aRes of res) {
            aRes.pllEndDateYes = utils.minutesAfter(aRes.pll_longevity * 60, aRes.pll_start_date);
            aRes.cotributerColor = `#${utils.hash6c(crypto.keccak256(aRes.pr_cotributer_account))}`;
            aRes.pllEndDateAbstainOrNo = utils.minutesAfter(utils.floor(aRes.pll_longevity * 60 * 1.5), aRes.pll_start_date);
            aRes.pllStatus = iConsts.CONSTS.STATUS_TO_LABEL[aRes.pll_status];
            aRes.machineBallot = _.has(votesDict, aRes.pll_hash) ? votesDict[aRes.pll_hash] : null;

            // calc potentiasl voter gains
            if (voter) {
                let diff = utils.timeDiff(aRes.pll_start_date).asMinutes;
                let vGain = pollHandler.calculateVoteGain(diff, diff, aRes.pll_longevity * 60);
                // console.log('diff', diff);
                // console.log('pll_longevity', aRes.pll_longevity * 60);
                // console.log('vGain', vGain);
                aRes.yourGainY = utils.customFloorFloat(vGain.gainYes * 100, 2);
                aRes.yourGainN = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);
                aRes.yourGainA = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);

            } else {
                aRes.yourGainY = 0.0;
                aRes.yourGainN = 0.0;
                aRes.yourGainA = 0.0;
            }
        }
        return { records: res, err: false };
    }

    static removeProposal(proposalHash) {
        let msg;
        //sceptical test
        let exist = model.sRead({
            table: tableOnchainProposals,
            query: [['pr_hash', proposalHash]]
        });
        if (exist.length != 1) {
            msg = `Try to delete proposal strange result! ${utils.stringify(exist)}`;
            clog.sec.err(msg);
            return { err: true, msg }
        }

        model.sDelete({
            table: tableOnchainProposals,
            query: [['pr_hash', proposalHash]]
        });
        return { err: false }
    }

    /**
     * it used in Demos to present a PoW/Normal proposal in Agora
     */
    static renderProposalDocumentToHTML(proposal) {
        return `
        <table>
            <tr>
                <td>Proposal Title:</td>
                <td>${proposal.title}</td>
            </tr>
            
            <tr>
                <td>Proposal Description:</td>
                <td>${proposal.descriptions}</td>
            </tr>
            
            <tr>
                <td>Proposal Tags:</td>
                <td>${proposal.tags}</td>
            </tr>
            
            <tr>
                <td>Proposal Hash:</td>
                <td>${proposal.hash}</td>
            </tr>

            <tr>
                <td>Share Holder:</td>
                <td>${proposal.shareholder}</td>
            </tr>

            <tr>
                <td>Help Hours:</td>
                <td>${proposal.helpHours}</td>
            </tr>

            
            <tr>
                <td>Help Level:</td>
                <td>${proposal.helpLevel}</td>
            </tr>

            <tr>
                <td>Gained Shares:</td>
                <td>${Math.floor(proposal.helpHours * proposal.helpLevel)}</td>
            </tr>

            <tr>
                <td>Creation Date:</td>
                <td>${proposal.creationDate}</td>
            </tr>
        </table>
        `;
    }

    static convertFieldsOnchainRecords(elm) {
        let out = {};
        if (_.has(elm, 'pr_hash'))
            out.prHash = elm.pr_hash;
        if (_.has(elm, 'pr_type'))
            out.prType = elm.pr_type;
        if (_.has(elm, 'pr_class'))
            out.prClass = elm.pr_class;
        if (_.has(elm, 'pr_version'))
            out.prVersion = elm.pr_version;
        if (_.has(elm, 'pr_title'))
            out.prTitle = elm.pr_title;
        if (_.has(elm, 'pr_description'))
            out.prDescription = elm.pr_description;
        if (_.has(elm, 'pr_tags'))
            out.prTags = elm.pr_tags;
        if (_.has(elm, 'pr_project_id'))
            out.prProjectId = elm.pr_project_id;
        if (_.has(elm, 'pr_help_hours'))
            out.prHelpHours = elm.pr_help_hours;
        if (_.has(elm, 'pr_help_level'))
            out.prHelpLevel = elm.pr_help_level;
        if (_.has(elm, 'pr_voting_longevity'))
            out.prVotingLongevity = elm.pr_voting_longevity;
        if (_.has(elm, 'pr_polling_profile'))
            out.prPollingProfile = elm.pr_polling_profile;
        if (_.has(elm, 'pr_cotributer_account'))
            out.prCotributerAccount = elm.pr_cotributer_account;
        if (_.has(elm, 'pr_start_voting_date'))
            out.prStartVotingDate = elm.pr_start_voting_date;
        if (_.has(elm, 'pr_conclude_date'))
            out.prConcludeDate = elm.pr_conclude_date;
        if (_.has(elm, 'pr_approved'))
            out.prApproved = elm.pr_approved;
        return out;
    }

    static convertFieldsDraftTbl(elm) {
        let out = {};
        if (_.has(elm, 'pd_id'))
            out.pdId = elm.pd_id;
        if (_.has(elm, 'pd_mp_code'))
            out.pdmpCode = elm.pd_mp_code;
        if (_.has(elm, 'pd_hash'))
            out.pdHash = elm.pd_hash;
        if (_.has(elm, 'pd_type'))
            out.pdType = elm.pd_type;
        if (_.has(elm, 'pd_class'))
            out.pdClass = elm.pd_class;
        if (_.has(elm, 'pd_version'))
            out.pdVersion = elm.pd_version;
        if (_.has(elm, 'pd_title'))
            out.pdTitle = elm.pd_title;
        if (_.has(elm, 'pd_description'))
            out.pdDescription = elm.pd_description;
        if (_.has(elm, 'pd_tags'))
            out.pdTags = elm.pd_tags;
        if (_.has(elm, 'pd_project_hash'))
            out.pdProjectHash = elm.pd_project_hash;
        if (_.has(elm, 'pd_help_hours'))
            out.pdHelpHours = elm.pd_help_hours;
        if (_.has(elm, 'pd_help_level'))
            out.pdHelpLevel = elm.pd_help_level;
        if (_.has(elm, 'pd_voting_longevity'))
            out.pdVotingLongevity = elm.pd_voting_longevity;
        if (_.has(elm, 'pd_polling_profile'))
            out.pdpdPollingProfile = elm.pd_polling_profile;
        if (_.has(elm, 'pd_cotributer_account'))
            out.pdCotributerAccount = elm.pd_cotributer_account;
        if (_.has(elm, 'pd_body'))
            out.pdBody = elm.pd_body;
        if (_.has(elm, 'pd_creation_date'))
            out.pdCreationDate = elm.pd_creation_date;

        return out;
    }


}

module.exports = ProposalHandler;
