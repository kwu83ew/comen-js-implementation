const _ = require('lodash');
const config = require("config");

// dummy solution to cloning app for loacl-tests
let acim = require("./startup/app-clone");
let appCloneId = acim.getAppCloneId();
// acim.setAppCloneId(1);
// appCloneId = acim.getAppCloneId();
let cliProt = 1717 + appCloneId;
const imgServerPort = cliProt || process.env.PORT;
const clog = require('./loggers/console_logger');
// const Joi = require('joi');
const app = require('express')();
require("./startup/boot-server").bootServer();

require("./startup/routes")(app);

// launching threads (if is not test)
if (config.get("dbPostfix") != '_test')
    require("./startup/threads/threads");

if (appCloneId > 0) {
    clog.app.info(`--------------- imagine clone -------------`)
    clog.app.info(`clone: ${appCloneId}`)
    clog.app.info(`port: ${cliProt}`)
    clog.app.info('--------------- imagine ------------------')
}

const server = app.listen(imgServerPort, () => {
    clog.app.info(`imagine app is runing on Greenwich Time (UTC): ${require('./startup/singleton').instance.getMoment().format()}`);
    clog.app.info(`listening on port ${imgServerPort} `);
});
server.timeout = 100000;

const iConsts = require("./config/constants");
if (iConsts.DSP_CONSTANTS) {
    clog.app.info(require("./config/dsp-constants").dspConstants());
}

module.exports = server