// const {User, validate} = require("../models/user");
const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const walletAddressHandler = require("../../web-interface/wallet/wallet-address-handler");
const walletHandler = require("../../web-interface/wallet/wallet-handler");
const docBufferHandler = require("../../services/buffer/buffer-handler");
const blockHandler = require("../../dag/normal-block/normal-block-handler/normal-block-handler");
const walletHandlerLocalUTXOs = require("../../web-interface/wallet/wallet-handler-local-utxos");
const machine = require('../../machine/machine-handler');


router.put("/new-address", async (req, res) => {
    console.log(`/new-address ${utils.stringify(req.body)}`);
    let r = await walletAddressHandler.createNewAddressAsync(req.body);
    res.send(JSON.stringify(r));
});

router.get("/getWalletAddresses", async (req, res) => {
    let r = await walletAddressHandler.getAddressesListAsync({ sum: true, order:[['wa_creation_date', 'ASC']] });
    res.send(JSON.stringify(r));
});

router.get("/getWalletBalance", async (req, res) => {
    let r = await walletHandler.getFundsList(req, res);
    console.log('/getWalletBalance', JSON.stringify(r));
    res.send(JSON.stringify(r));
});
router.get("/restorUnUsedUTXOs", async (req, res) => {
    let r = await walletHandlerLocalUTXOs.restorUnUsedUTXOs();
    console.log('/restorUnUsedUTXOs', JSON.stringify(r));
    res.send(JSON.stringify(r));
});

router.get("/trxList", async (req, res) => {
    let r = await walletHandler.getTrxList(req);
    res.send(JSON.stringify(r));
});

router.put("/sendPai", async (req, res) => {
    let r = await walletHandler.walletSigner(req.body);
    res.send(JSON.stringify(r));
});

router.get("/buffered", async (req, res) => {
    // TODO: replace this function with more complete version "docBufferHandler.getBufferContentsInfo()"
    let r = await docBufferHandler.getProcessedTrxs();
    res.send(JSON.stringify(r));
});

router.get("/push-trx", async (req, res) => {
    let r = await blockHandler.createANormalBlock();
    if (r.err != false)
        console.log(`\n\npush-trx ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.get("/", async (req, res) => {
    // const { error } = validate(req.body);
    // if (error) return res.status(400).send(error.detail[0].message);

    // some validating

    // // insert new user
    // user = new User(_.pick(req.body, [preferedLanguage, mnemonic, password]));
    // await user.save();
    // res.send(JSON.stringify(_.pick(user, ["preferedLanguage"])));

});


// validate user access
// const token = req.header("x-aut-token");
// if (error) return res.status(401).send(error.detail[0].message);


module.exports = router;
