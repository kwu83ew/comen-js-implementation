// const {User, validate} = require("../models/user");
const _ = require('lodash');
const express = require('express');
const iConsts = require('../../config/constants');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const parseQHandler = require('../../services/parsing-q-manager/parsing-q-handler');

router.put("/getMyOnchainFiles", async (req, res) => {
    console.log(`/getMyOnchainFiles: ${utils.stringify(req.body)}`);
    const freeDocHandler = require('../../services/free-doc-handler/free-doc-handler');
    let r = freeDocHandler.fileDocHandler.getMyOnchainFiles();
    res.send(r);
});

router.put("/refreshPollingStatus", async (req, res) => {
    console.log(`/refreshPollingStatus: ${utils.stringify(req.body)}`);
    const pollHandler = require('../../services/polling-handler/general-poll-handler');
    let r = pollHandler.maybeUpdatePollingStat(req.body.pllHash);
    res.send(r);
});

router.put("/getDNASharesInfo", async (req, res) => {
    console.log(`/getDNASharesInfo: ${utils.stringify(req.body)}`);
    const DNAHandler = require("../../dna/dna-handler");
    let { sumShares, holdersByKey, holdersOrderByShares } = DNAHandler.getSharesInfo();
    res.send({ sumShares, holdersByKey, holdersOrderByShares });
});

router.put("/flushBufferedDocs", async (req, res) => {
    console.log(`/flushBufferedDocs: ${utils.stringify(req.body)}`);
    const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
    let r = await wBroadcastBlock.broadcastBlockAsync(req.body);
    res.send(r);
});

router.put("/getClientVersion", async (req, res) => {
    // console.log(`/getClientVersion: ${utils.stringify(req.body)}`);
    // let r = utils.getClientVersion(req.body);
    // console.log(`r ${r}`);
    res.send({ imgClientVersion: iConsts.CLIENT_VERSION });
});

router.put("/getMachineSignals", async (req, res) => {
    console.log(`/getMachineSignals: ${utils.stringify(req.body)}`);
    let r = iutils.getMachineSignals(req.body);
    console.log(`r ${r}`);
    res.send(r);
});
router.put("/getNetworkSignals", async (req, res) => {
    console.log(`/getNetworkSignals: ${utils.stringify(req.body)}`);
    let r = iutils.getNetworkSignals(req.body);
    console.log(`r ${r}`);
    res.send(r);
});

router.put("/calcLoanRepayments", async (req, res) => {
    console.log(`/calcLoanRepayments: ${utils.stringify(req.body)}`);
    let loanHandler = require('../../contracts/loan-contract/loan-contract');
    let r = loanHandler.calcLoanRepayments(req.body);
    // console.log(`r ${r}`);
    res.send(r);
});

router.put("/loadRecordedINames", async (req, res) => {
    console.log(`/loadRecordedINames: ${utils.stringify(req.body)}`);
    const flensHandler = require('../../contracts/flens-contract/flens-handler');
    let r = flensHandler.register.searchRegisteredINames({ needBindingsInfoToo: true });
    console.log(`searchRegisteredINames res ${utils.stringify(r.bindingDict)}`);
    res.send(r);
});

router.put("/predictFutureCoinIssuance", async (req, res) => {
    // console.log(`/predictFutureCoinIssuance: ${utils.stringify(req.body)}`);
    let r = await iutils.predictFutureCoinIssuance(req.body);
    console.log(`r ${r}`);
    res.send(r);
});

router.put("/getRegisteredINames", async (req, res) => {
    console.log(`/getRegisteredINames: ${utils.stringify(req.body)}`);
    let r = await iutils.getRegisteredINames(req.body);
    res.send(r);
});

router.put("/getReservesDetails", async (req, res) => {
    const crypto = require("../../crypto/crypto");
    console.log(`/getReservesDetails: ${utils.stringify(req.body)}`);
    let { err, reserves, orderedBlockHashes } = iutils.getReservesDetails(req.body);

    // arrange shareholders
    for (let blockHash of orderedBlockHashes) {
        reserves[blockHash].holders = [];
        for (let anOut of reserves[blockHash].shareHolders) {
            reserves[blockHash].holders.push({
                holder: anOut[0],
                value: anOut[1],
                percentage: utils.customFloorFloat((anOut[1] / reserves[blockHash].releasableCoins) * 100, 4),
                holderColor: `#${utils.hash6c(crypto.keccak256(anOut[0]))}`
            });
        }
        delete reserves[blockHash].shareHolders;
    }
    // console.log('get Reserves Details res', utils.stringify(reserves));
    res.send({ err, orderedBlockHashes, reserves });
});

router.put("/loadPollingProfiles", async (req, res) => {
    console.log(`/loadPollingProfiles: ${utils.stringify(req.body)}`);
    const pollHandler = require("../../services/polling-handler/general-poll-handler");
    let r = pollHandler.loadPollingProfiles(req.body);
    res.send(r);
});

router.put("/getMachineAccountInfo", async (req, res) => {
    let r = await machine.getMProfileSettingsAsyncSecure(req.body);

    // also retrieve inames which controlled by this machine
    const flensHandler = require('../../contracts/flens-contract/flens-handler');
    let machineINames = flensHandler.searchForINamesControlledByWallet();
    r.machineINames = machineINames.records
    console.log(`/getMachineAccountInfo: ${utils.stringify(r)}`);
    res.send(r);
});


router.put("/predictReleaseableMicroPAIsPerOneCycle", async (req, res) => {
    console.log(`/predictReleaseableMicroPAIsPerOneCycle: ${utils.stringify(req.body)}`);
    let r = await iutils.predictReleaseableMicroPAIsPerOneCycle(req.body);
    res.send(JSON.stringify(r));
});

router.put("/fetchInboxManually", async (req, res) => {
    const networkListener = require('../../network-adapter/network-listener');
    console.log(`/fetchInboxManually args: ${utils.stringify(req.body)}`);
    let r;
    if (req.body.emailType == iConsts.CONSTS.PRIVATE) {
        r = await networkListener.fetchPrvEmailAndWriteOnHardDisk();
    } else if (req.body.emailType == iConsts.CONSTS.PUBLIC) {
        r = await networkListener.fetchPubEmailAndWriteOnHardDisk();
    }
    console.log(`/fetchInboxManually args: ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/readingHarddiskManually", async (req, res) => {
    const networkListener = require('../../network-adapter/network-listener');
    console.log(`/readingHarddiskManually args: ${utils.stringify(req.body)}`);
    let r;
    r = await networkListener.recursive_reading_harddisk();
    console.log(`/readingHarddiskManually args: ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/manInvokeLeavesFromNeighbors", async (req, res) => {
    const dagMessagingHandler = require('../../messaging-protocol/dag/dag-msg-handler');
    console.log(`/man InvokeLeavesFromNeighbors args: ${utils.stringify(req.body)}`);
    let r;
    r = dagMessagingHandler.invokeDescendents({ denayDoubleSendCheck: true });
    console.log(`/man InvokeLeavesFromNeighbors args: ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/dropBroadcastLogs", async (req, res) => {
    const bdLogger = require('../../loggers/broadcast-logger')
    console.log(`/empty BroadcastLogs args: ${utils.stringify(req.body)}`);
    let r;
    r = await bdLogger.dropBroadcastLogs();
    res.send(r);
});

router.put("/pullFromQeueManually", async (req, res) => {
    const networkListener = require('../../network-adapter/network-listener');
    console.log(`/pullFromQeueManually args: ${utils.stringify(req.body)}`);
    let r;
    r = parseQHandler.qPicker.smartPullQSync();
    console.log(`/pullFromQeueManually args: ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/sendOutThePacketManually", async (req, res) => {
    const sendingQManager = require('../../services/sending-q-manager/sending-q-manager');
    console.log(`/sendOutThePacketManually args: ${utils.stringify(req.body)}`);
    let r;
    r = sendingQManager.sendOutThePacket();
    console.log(`/sendOutThePacketManually args: ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/dlDAGBundle", async (req, res) => {
    console.log(`/dlDAGBundle: ${utils.stringify(req.body)}`);
    let r = await require('../../messaging-protocol/dag/download-full-dag').dlDAGBundle(req.body);
    // console.log(`r ${r}`);
    res.send(r);
});

router.put("/dlNodeScreenShot", async (req, res) => {
    console.log(`/dlNodeScreenShot: ${utils.stringify(req.body)}`);
    const nodeScHandler = require('../../services/node-screen-shot/node-screen-shot-handler');
    let r = await nodeScHandler.dlNodeScreenShot(req.body);
    // console.log(`r ${r}`);
    res.send(r);
});

router.put("/loadScreenshotsList", async (req, res) => {
    console.log(`/loadScreenshotsList: ${utils.stringify(req.body)}`);
    const nodeScHandler = require('../../services/node-screen-shot/node-screen-shot-handler');
    let r = await nodeScHandler.loadScreenshotsList(req.body);
    // console.log(`r ${r}`);
    res.send(r);
});

router.put("/compareScreenShot", async (req, res) => {
    console.log(`/compareScreenShot: ${utils.stringify(req.body)}`);
    const nodeScHandler = require('../../services/node-screen-shot/node-screen-shot-handler');
    let rCompareRes = await nodeScHandler.compareHandler.compareScreenShot(req.body);
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`compare Screen Shot res: ${utils.stringify(rCompareRes)}`);
    res.send(rCompareRes);
});

router.put("/generateTestData", async (req, res) => {
    console.log(`/generateTestData: ${utils.stringify(req.body)}`);
    let r;
    const testDatasetGeneratorHandler = require('../../tests/unit/transaction/importing-validations/prepare-data/prepare-test-data');
    switch (req.body.datasetNumber) {
        case 'prepareDataset1':
            r = await testDatasetGeneratorHandler.prepareGroup1();
            break;

    }
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||`);
    console.log(`prepare Data set res for ${req.body.datasetNumber}: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/deleteSS", async (req, res) => {
    console.log(`/deleteSS: ${utils.stringify(req.body)}`);
    const nodeScHandler = require('../../services/node-screen-shot/node-screen-shot-handler');
    let r = await nodeScHandler.deleteSS(req.body);
    // console.log(`r ${r}`);
    res.send(r);
});


router.post("/uploadFile", async (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    let theFile = req.files.theFile;
    let extension = theFile.name.split('.')[theFile.name.split('.').length - 1];
    let fileName = `temp-file.${extension}`; //${crypto.rand()} DO NOT name uploaded file with rand func, since recordFile was not able to read the file (which is unfinished and still is open)
    theFile.mv(`tmp/${fileName}`, (error) => {
        if (error) {
            return res.send({ err: false, msg: 'Error un upload' });
        }
        return res.send({
            err: false,
            msg: 'Done',
            fileName,
            mimetype: theFile.mimetype,
            extension
        });
    });
});

router.put("/recordFile", async (req, res) => {
    const fileHandler = require('../../hard-copy/file-handler');
    // TODO: FIXME, move this part to a generic module, dedicated to manage upload file... for the vary reasons
    console.log('req.body, req.body', req.body);
    console.log('req.body, cPFileLabel', req.body.cPFileLabel);
    /**
    req.body {
        dTarget: 'machineRestoreInfo',
        uploadRes: {
            err: false,
            msg: 'Done',
            fileName: 'temp-file.txt',
            mimetype: 'text/plain',
            extension: 'txt'
        }
    }

     */
    let r, readRes;
    let uploadRes = req.body.uploadRes;
    switch (req.body.dTarget) {

        case 'machineRestoreInfo':
            // read the file content
            readRes = fileHandler.packetFileReadSync({
                fileName: uploadRes.fileName,
                appCloneId: 0,//db.getAppCloneId(),
                dirName: `tmp`,
            });
            r = await machine.backupHandler.restoreMaWa({
                reqBody: req.body,
                content: readRes.content
            });
            break;

        case 'cFileToBuffer':
        case 'cFileToNetwork':
            const freePostHandler = require('../../web-interface/free-posts/free-posts-handler');
            r = freePostHandler.recordFile(req.body);
            if (r.res != false)
                console.log(r);
            break;

        case 'nodeScreenShot':
            // read the file content
            readRes = fileHandler.packetFileReadSync({
                fileName: uploadRes.fileName,
                appCloneId: 0,//db.getAppCloneId(),
                dirName: `tmp`,
            });
            const nodeScHandler = require('../../services/node-screen-shot/node-screen-shot-handler');
            r = await nodeScHandler.pushReportToDB({
                scOwner: req.body.cPFileLabel,
                content: utils.parse(readRes.content)
            });
            break;

        default:
    }
    res.send(r);
});

router.get("/", async (req, res) => {

});


module.exports = router;
