const _ = require('lodash');
const clog = require('../../loggers/console_logger');
const express = require('express');
const router = express.Router();
const kvHandler = require("../../models/kvalue");

router.put("/getKValue", async (req, res) => {
    clog.http.info(`put: plugins/getKValue: `);
    console.log(`put: plugins/getKValue: ------------------- `, req.body);
    let current = await kvHandler.getValueAsync(req.body.kv_key);
    if (current == null) {
        current = {}
    } else {
        current = JSON.parse(current);
    }
    res.send(current);
});


module.exports = router;