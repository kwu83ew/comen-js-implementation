const Joi = require("joi");
// const {User} = require("../models/user");
const express = require('express');
const router = express.Router();
const _ = require('lodash');
// const bcrypt = require("bcrypt");


router.post("/", async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.detail[0].message);

    // does user exist?
    let user = await user.findOne({email: req.body.email});
    if (!user) return res.status(400).send("User does not exist");

    //validate password
    const isValidPwd = bcrypt.compare(req.body.password, user.password);
    if (!isValidPwd) res.status(400).send("Invalid email or password");

    // some validating

    jwtPrivateKey = config.get("jwtPrivateKey");
    if (!config.get("jwtPrivateKey")){
        console.error("ERORR: imagine_jwtPrivateKey is not defined, use export imagine_jwtPrivateKey='your private key' ");
        jwtPrivateKey = "I am the only user :) ";
    }
    const token = jwt.sign({_id: "dummyId", name: "dummyName"}, jwtPrivateKey);
    res.header("x-auth-token", token).send("html");

});


function validate(req){
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema);
}
module.exports = router;