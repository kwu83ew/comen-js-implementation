// const {User, validate} = require("../models/user");
const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto')
const contractsHandler = require('../../web-interface/contracts/contracts-handler');


router.put("/doAction", (req, res) => {
    let r = contractsHandler.doAction(req.body);
    res.send(r);
});

router.put("/getPLRBundles", async (req, res) => {
    let r = await contractsHandler.getPLRBundles(req.body);
    for (let aProp of r) {
        aProp.body.payload.proposal.shareholderShort = iutils.shortBech16(aProp.body.payload.proposal.shareholder);
        aProp.body.payload.proposal.shareholderColor = `#${utils.hash6c(crypto.keccak256(aProp.body.payload.proposal.shareholder))}`;
    }
    res.send(r);
});

router.put("/getMyOnChains", async (req, res) => {
    let r = await contractsHandler.getMyOnChains(req.body);
    console.log('getMyOnChains', r);
    res.send(r);
});

router.put("/getPPTBundles", async (req, res) => {
    let r = await contractsHandler.getPPTBundles(req.body);
    res.send(r);
});

router.put("/deletePLR", async (req, res) => {
    let r = await contractsHandler.deletePLR(req.body);
    res.send(r);
});

router.put("/signPLR", async (req, res) => {
    let r = await contractsHandler.signPLR(req.body);
    if (r.err != false)
        clog.http.error(r);
    res.send(r);
});

router.put("/deleteBundle", async (req, res) => {
    let r = await contractsHandler.deleteBundle(req.body);
    res.send(r);
});

router.put("/pushPPTToSendingQ", async (req, res) => {
    let r = await contractsHandler.pushPPTToSendingQ(req.body);
    res.send(r);
});

// router.put("/predicdtFutureIncomes", async (req, res) => {
//     let r = await contractsHandler.predicdtFutureIncomes(req.body);
//     // console.log(JSON.stringify(r));
//     res.send(r);
// });


router.get("/", async (req, res) => {
});

module.exports = router;