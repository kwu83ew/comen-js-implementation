const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const router = express.Router();
const DAGHandler = require("../../dag/graph-handler/dag-handler");
const dagMsgHandler = require("../../messaging-protocol/dag/dag-msg-handler");
const db = require('../../startup/db2')
const toParse = require("../../services/parsing-q-manager/parsing-q-handler");
const toSend = require("../../services/sending-q-manager/sending-q-manager");
const utxoHandler = require("../../transaction/utxo/utxo-handler");
const crypto = require("../../crypto/crypto");
const leavesHandler = require("../../dag/leaves-handler");
const coinbaseUTXOs = require('../../dag/coinbase/import-utxo-from-coinbases');
const machine = require("../../machine/machine-handler");
const Moment = require("../../startup/singleton").instance.Moment;
const DNAHandler = require("../../dna/dna-handler");
const cbIC = require('../../dag/coinbase/control-coinbase-issuance-criteria')

function getBackerInfo(req) {
    clog.http.info('get: watcher/getBackerInfo');
    let { backer, shares, percentage } = DNAHandler.getMachineShares();
    let { cycle, machineEmail, machineKey, emailsHashDict } = cbIC.makeEmailHashDict();

    let res = {
        backerAddress:backer, shares, percentage,
        cycle, machineEmail, machineKey, emailsHashDict
    };
    return res;
}
module.exports = getBackerInfo