const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const treasuryHandler = require('../../services/treasury/treasury-handler');
const machine = require('../../machine/machine-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const crypto = require('../../crypto/crypto');
const blockUtils = require('../../dag/block-utils');


class MachineBalanceHandler {


    /**
     * 
     * @param {*} args 
     * given block short hash, trace back all the way back and returs the block which are not in given block's history
     */
    static async notInHistory(args) {
        let msg;
        console.log(`notInHistory args: ${utils.stringify(args)}`);
        //{"leaveShortHash":"76a69e"}
        let leaveShortHash = args.leaveShortHash
        let blocks = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_creation_date'],
            query:
                [
                    ['b_hash', ['LIKE', `${leaveShortHash}%`]]
                ],
        });
        console.log('blocks', blocks);
        if (blocks.length == 0) {
            msg = `The block(${leaveShortHash}) does not exist in DAG!`
            return { err: true, msg }
        }
        if (blocks.length > 1) {
            msg = `The block(${leaveShortHash}) COLLISION!`
            return { err: true, msg }
        }
        let leaveBlock = blocks[0];
        let leaveBlockHash = leaveBlock.bHash;

        // find all blocks already recorded in DAG
        blocks = dagHandler.searchInDAGSync({
            fields: ['b_hash'],
            query:
                [
                    ['b_creation_date', ['<=', leaveBlock.bCreationDate]]
                ],
        });
        blocks = blocks.map(x => x.bHash);
        // console.log(`entire blocks older than leave ${utils.stringify(blocks)}`);

        let allAncestorsOfAYoungerBlock = dagHandler.walkThrough.returnAncestorsYoungerThan({
            blockHashes: [leaveBlockHash],
            byDate: utils.minutesBefore(10, iConsts.getLaunchDate())    // to cover Genesis block too
        });
        allAncestorsOfAYoungerBlock.push(leaveBlockHash);
        allAncestorsOfAYoungerBlock = utils.arrayUnique(allAncestorsOfAYoungerBlock);
        // console.log(`all AncestorsOfAYoungerBlock ${utils.stringify(allAncestorsOfAYoungerBlock)}`);
        let missedBlocks = utils.arrayDiff(blocks, allAncestorsOfAYoungerBlock);
        console.log(`Not in Hhistory ${utils.stringify(missedBlocks)}`);

    }

    static async getWAncestors(args) {
        let msg;
        console.log(`getAncestors args: ${utils.stringify(args)}`);
        //{"leaveShortHash":"76a69e"}
        let leaveShortHash = args.leaveShortHash
        let blocks = dagHandler.searchInDAGSync({
            fields: ['b_hash', 'b_creation_date'],
            query:
                [
                    ['b_hash', ['LIKE', `${leaveShortHash}%`]]
                ],
        });
        console.log('blocks', blocks);
        if (blocks.length == 0) {
            msg = `The block(${leaveShortHash}) does not exist in DAG!`
            return { err: true, msg }
        }
        if (blocks.length > 1) {
            msg = `The block(${leaveShortHash}) COLLISION!`
            return { err: true, msg }
        }
        let leaveBlock = blocks[0];
        let leaveBlockHash = leaveBlock.bHash;

        let ancestors = dagHandler.walkThrough.getAncestors([leaveBlockHash], args.stepBack);
        console.log('ancestors', ancestors);
        return { err: false, ancestors }

    }



    static async blockGenReport() {

        console.log(`blockGenReport `);

        let machineSettings = machine.getMProfileSettingsSync();
        let backerAddress = machineSettings.backerAddress;


        let res = await dagHandler.searchInDAGAsync({
            fields: ['b_hash', 'b_creation_date', 'b_type'],
            query:
                [
                    ['b_backer', backerAddress]
                ],
            order: [['b_creation_date', 'DESC']],
            limit: 50
        });

        res.map(x => x.bHash = utils.hash6c(x.bHash));
        res.map(x => x.bCreationDate = x.bCreationDate.split(' ')[1]);
        console.log(`res.reverse() ${res.reverse()}`);
        return {};
    }

    static async machineBalanceChk() {

        let DAGAgeAsMinutes = utils.timeDiff(iConsts.getLaunchDate()).asMinutes
        let cycleCountsFromBegan = Math.floor(DAGAgeAsMinutes / iConsts.getCycleByMinutes()) + 1;
        let paiPerCycle = iutils.calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore().oneCycleIssued;
        let distinctCoinbases = dagHandler.searchInDAGSync({
            fields: ["DISTINCT b_cycle"],
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Coinbase]
            ]
        }).length;

        // also retrieve treasury unspendables(if exist)
        let waitedTreasuryIncomes = treasuryHandler.getWaitedIncomes().sum;
        console.log(`waitedIncomes: ${waitedTreasuryIncomes}`);


        let spendableUTXOs = utxoHandler.getSpendablesInfo();
        // console.log('spendableUTXOs by table trx_utxos', spendableUTXOs);
        let waitedCoinbases = dagHandler.watcher.getNotImportedCoinbaseBlocks();
        let waitedNormals = dagHandler.watcher.getNotImportedNormalBlock();
        // console.log('waitedNormals---------', utils.stringify(waitedNormals));


        let bMPAIs = dagHandler.watcher.getCBBlocksStat();
        console.log('bMPAIs.mintedMicroPAIs,', bMPAIs.mintedMicroPAIs);
        console.log('gggggggggggggggggggggggg');
        console.log(`bMPAIs `, utils.stringify(bMPAIs));
        console.log('gggggggggggggggggggggggg');
        // console.log('floorishMicroPAIs', bMPAIs, utils.microPAIToPAI6(bMPAIs.floorishMicroPAIs));
        let finalBalance = bMPAIs.mintedMicroPAIs
            - spendableUTXOs.sum
            - waitedCoinbases.sum
            - waitedNormals.sum
            - bMPAIs.floorishMicroPAIs // the pais lost because of Math.floor
            - waitedTreasuryIncomes;

        let repOut = `\n --------------- \n\nminted PAIs\t${bMPAIs.mintedMicroPAIs} \t-${bMPAIs.mintedMicroPAIs}`;
        repOut += `\n-spendableUTXOs\t-${spendableUTXOs.sum}    \t${spendableUTXOs.sum}`;
        repOut += `\n-waited coinbases To Be Spendable\t-${waitedCoinbases.sum}    \t${waitedCoinbases.sum}`;
        repOut += `\n-waited Normals To Be Spendable\t-${waitedNormals.sum}   \t${waitedNormals.sum}`;
        repOut += `\n-floorish\t-${bMPAIs.floorishMicroPAIs}    \t${bMPAIs.floorishMicroPAIs}`;
        repOut += `\n-waited Treasury Incomes\t-${waitedTreasuryIncomes}     \t${waitedTreasuryIncomes}`;
        repOut += `\n-final Balance\t${finalBalance}     \t-${finalBalance}`;
        console.log(repOut);



        // control trnsactions refLocs are realy moved from UTXOs?
        let usedRefLocs = [];
        let normalWBlocs = dagHandler.searchInDAGSync({
            query: [
                ['b_type', iConsts.BLOCK_TYPES.Normal]
            ]
        });
        for (let wBlock of normalWBlocs) {
            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            if (_.has(block, 'docs') && (block.docs.length > 0))
                for (let doc of block.docs) {
                    if (_.has(doc, 'inputs') && (doc.inputs.length > 0))
                        for (let input of doc.inputs) {
                            usedRefLocs.push(iutils.packCoinRef(input[0], input[1]));
                        }
                }
        }
        let refLocsExistance = await utxoHandler.getCoinsInfoASync({ coins: usedRefLocs });
        // console.log(`refLocsExistance: ${refLocsExistance}`);

        // retrieve node wealth
        let nodeSpendableUTXOs = await walletHandler.retrieveSpendableUTXOsAsync(); //will return valid(or invalid in case of doublespend tests) inputs
        let nodeWealth = {
            details: [],
            value: 0
        };
        // console.log('nodeSpendableUTXOs nodeSpendableUTXOs', nodeSpendableUTXOs);

        for (let anUtxo of nodeSpendableUTXOs) {
            nodeWealth.value += anUtxo.utOValue
            nodeWealth.details.push({
                refLoc: iutils.shortCoinRef(anUtxo.utCoin),
                address: iutils.shortBech(anUtxo.utOAddress),
                value: utils.microPAIToPAI(anUtxo.utOValue),
                refCreationDate: anUtxo.utRefCreationDate.split(' ')[1]
            });
        }
        let tmpDtl = [];
        for (let i = 0; i < nodeWealth.details.length; i++) {
            let x = nodeWealth.details[i];
            tmpDtl.push([i + 1, x.refLoc, x.address, x.refCreationDate, x.value].join(' '))
        }
        nodeWealth.details = tmpDtl.join(`\n`);

        return {
            paiPerCycle,
            cycleCountsFromBegan,
            cycleCountsFromBeganColor: `#${utils.hash6c(crypto.keccak256(cycleCountsFromBegan.toString()))}`,
            distinctCoinbases,

            sumCoinbase: bMPAIs.mintedMicroPAIs,
            sumCoinbaseColor: `#${utils.hash6c(iutils.doHashObject(bMPAIs.mintedMicroPAIs))}`,

            spendables: spendableUTXOs.sum,
            spendablesColor: `#${utils.hash6c(iutils.doHashObject(spendableUTXOs.sum))}`,

            waitedCoinbases,
            waitedCoinbasesColor: `#${utils.hash6c(iutils.doHashObject(waitedCoinbases))}`,
            waitedNormals: waitedNormals,
            waitedNormalsColor: `#${utils.hash6c(iutils.doHashObject(waitedNormals))}`,
            waitedIncomes: waitedTreasuryIncomes,
            waitedIncomesColor: `#${utils.hash6c(iutils.doHashObject(waitedTreasuryIncomes))}`,

            floorishMicroPAIs: bMPAIs.floorishMicroPAIs,
            floorishMicroPAIsColor: `#${utils.hash6c(iutils.doHashObject(bMPAIs))}`,

            finalBalance,
            finalBalanceColor: `#${utils.hash6c(iutils.doHashObject(finalBalance))}`,

            nodeWealth,
            nodeWealthColor: `#${utils.hash6c(iutils.doHashObject(nodeWealth))}`,
            refLocsExistance
        };
    }
}

MachineBalanceHandler.excelReporter = require('./excel-reporter');

module.exports = MachineBalanceHandler;
