const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const blockUtils = require('../../dag/block-utils');
const db = require('../../startup/db2');

const CSV_TRX_HEADRS = ['', 'Account', 'Total Minted', 'Minted', 'Treasury', 'Inputs', 'Outputs', 'Balance', '.', 'Description', 'Used Coins', 'new Coins']; // headers

class ExcelReporter {

    static async dlTrxList(args) {
        console.log(`dlTrxList args: ${utils.stringify(args)}`);

        this.usedRefLocs = [];
        this.doubleSpendsSum = 0;
        // Require library
        let xl = require('excel4node');
        // Create a new instance of a Workbook class
        this.wb = new xl.Workbook();
        // Add Worksheets to the workbook
        this.ws = this.wb.addWorksheet('Sheet 1');
        this.initStyles();

        let wBlocks = await dagHandler.searchInDAGAsync({
            fields: ["DISTINCT b_cycle, b_hash, b_type, b_creation_date, b_body"],
            query: [
                ['b_type', ['NOT IN', [iConsts.BLOCK_TYPES.Genesis, iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote]]]
            ],
            order: [['b_creation_date', 'ASC']]
        });


        let totalMinted = 0;
        let spendasbleMinted_ByBlockClaim = 0;
        let waitedMinted_ByBlockClaim = 0;
        let waitedTreasury_ByBlockClaim = 0;
        let waitedUTXOs_ByBlockClaim = 0;
        let bBalance = 0;
        let AllUTXOsDict = {};
        let blockInx = 0;
        this.rowNum = 0;

        console.log('getCbUTXOsDateRange', iutils.getCbUTXOsDateRange());
        let { maxCreationDate } = iutils.getCbUTXOsDateRange();
        let aCycleBefore = utils.minutesBefore(iConsts.getCycleByMinutes());

        this.addHeader();
        for (let wBlock of wBlocks) {
            blockInx++;


            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;


            this.addBlockHd1({ blockInx, bType: block.bType, date: block.creationDate });
            this.addBlockHd2({ blockHash: block.blockHash, bType: block.bType });
            this.addHeader();

            let floorish = 0;
            if (block.bType == iConsts.BLOCK_TYPES.Coinbase) {
                // insert minted coins & treasury incomes(if exist)
                let mintedCoins = block.docs[0].mintedCoins;
                totalMinted += mintedCoins;
                floorish += mintedCoins;
                bBalance += mintedCoins;
                // console.log('block.creationDate', block.creationDate);
                // console.log('maxCreationDate', maxCreationDate);
                if (block.creationDate < maxCreationDate) {
                    spendasbleMinted_ByBlockClaim += mintedCoins;
                } else {
                    waitedMinted_ByBlockClaim += mintedCoins;
                }
                this.prepareARow({
                    dType: block.docs[0].dType,
                    date: block.creationDate,
                    account: 'Minting',
                    totalMinted,
                    minted: mintedCoins,
                    bBalance,
                    desc: `Minting PAIs by block(${utils.hash6c(block.blockHash)})`
                });
                let treasuryIncomes = block.docs[0].treasuryIncomes;
                bBalance += treasuryIncomes;
                floorish += treasuryIncomes;
                this.prepareARow({
                    dType: block.docs[0].dType,
                    date: block.creationDate,
                    account: 'Treasury Incomes',
                    treasury: treasuryIncomes,
                    bBalance,
                    desc: `Treasury Incomes PAIs by block(${utils.hash6c(block.blockHash)})`
                });
                this.prepareARow({});

                // divide between shreholders
                for (let outInx = 0; outInx < block.docs[0].outputs.length; outInx++) {
                    let anOut = block.docs[0].outputs[outInx];
                    let outValue = anOut[1];
                    AllUTXOsDict[iutils.packCoinRef(block.docs[0].hash, outInx)] = [anOut[0], outValue];
                    bBalance -= outValue;
                    floorish -= outValue;
                    this.prepareARow({
                        dType: block.docs[0].dType,
                        date: block.creationDate,
                        account: anOut[0],
                        output: -outValue,
                        bBalance,
                        desc: `Share dividend by block(${utils.hash6c(block.blockHash)})`,
                        newRef: iutils.packCoinRef(block.docs[0].hash, outInx)
                    });
                }


            } else if ([iConsts.BLOCK_TYPES.Normal, iConsts.BLOCK_TYPES.RpBlock].includes(block.bType)) {
                for (let aDoc of block.docs) {
                    if (_.has(aDoc, 'inputs')) {
                        for (let anInput of aDoc.inputs) {
                            let refLoc = iutils.packCoinRef(anInput[0], anInput[1]);
                            let refLocInfo = AllUTXOsDict[refLoc];
                            bBalance += refLocInfo[1];
                            floorish += refLocInfo[1];
                            this.prepareARow({
                                dType: aDoc.dType,
                                date: block.creationDate,
                                account: refLocInfo[0],
                                input: refLocInfo[1],
                                bBalance,
                                desc: `RefLoc input block(${utils.hash6c(block.blockHash)})`,
                                refLoc
                            });
                        }
                    }

                    if (_.has(aDoc, 'outputs')) {
                        if (aDoc.dType == iConsts.DOC_TYPES.DPCostPay) {
                            for (let outInx = 0; outInx < aDoc.outputs.length; outInx++) {
                                let anOut = aDoc.outputs[outInx];
                                let outValue = anOut[1];
                                AllUTXOsDict[iutils.packCoinRef(aDoc.hash, outInx)] = [anOut[0], outValue];

                                bBalance -= outValue;
                                floorish -= outValue;
                                if (anOut[0] == 'TP_DP') {
                                    waitedTreasury_ByBlockClaim += outValue;
                                    this.prepareARow({
                                        dType: aDoc.dType,
                                        date: block.creationDate,
                                        account: anOut[0],
                                        output: -outValue,
                                        bBalance,
                                        desc: `TP_DP payment block(${utils.hash6c(block.blockHash)})`,
                                        newRef: iutils.packCoinRef(aDoc.hash, outInx)
                                    });

                                } else {
                                    waitedUTXOs_ByBlockClaim += outValue;
                                    this.prepareARow({
                                        dType: aDoc.dType,
                                        date: block.creationDate,
                                        account: anOut[0],
                                        output: -outValue,
                                        bBalance,
                                        desc: `Backer fee payment block(${utils.hash6c(block.blockHash)})`,
                                        newRef: iutils.packCoinRef(aDoc.hash, outInx)
                                    });

                                }
                            }
                        } else if ([iConsts.DOC_TYPES.BasicTx, iConsts.DOC_TYPES.RpDoc].includes(aDoc.dType)) {
                            for (let outInx = 0; outInx < aDoc.outputs.length; outInx++) {

                                let anOut = aDoc.outputs[outInx];
                                let outValue = anOut[1];
                                AllUTXOsDict[iutils.packCoinRef(aDoc.hash, outInx)] = [anOut[0], outValue];


                                // not calculating backer fee, becaue it is in TP_DP transaaction
                                if (outInx == aDoc.dPIs) {
                                    this.prepareADPIsRow({
                                        dType: aDoc.dType,
                                        date: block.creationDate,
                                        account: anOut[0],
                                        output: +outValue,
                                        desc: `dPIs cost`,
                                        newRef: iutils.packCoinRef(aDoc.hash, outInx)
                                    });
                                    continue;
                                }


                                if (block.creationDate < aCycleBefore) {
                                    // do somrthing
                                } else {
                                    waitedUTXOs_ByBlockClaim += outValue;
                                }
                                bBalance -= outValue;
                                floorish -= outValue;
                                this.prepareARow({
                                    dType: aDoc.dType,
                                    date: block.creationDate,
                                    account: anOut[0],
                                    output: -outValue,
                                    bBalance,
                                    desc: `trx paying block(${utils.hash6c(block.blockHash)})`,
                                    newRef: iutils.packCoinRef(aDoc.hash, outInx)
                                });
                            }
                        }
                    }
                    this.prepareARow({});
                }
            }

            bBalance -= floorish;
            if (floorish != 0) {
                this.prepareARow({
                    dType: block.docs[0].dType,
                    date: block.creationDate,
                    account: 'floorish',
                    output: -floorish,
                    bBalance,
                    desc: `lost by floorish block(${block.docs[0].cycle} -> ${utils.hash6c(block.blockHash)})`,
                });
            }
//

            // balance at the end of block
            // this.prepareARow({ bBalance, desc: 'Block Balance' });

            // gap between  blocks
            this.prepareARow({});
            this.prepareARow({});


        }


        // footer numbers 
        this.prepareARow({
            date: utils.getNow(),
            account: 'Spendasble Minted PAIs By Block Claim',
            totalMinted: spendasbleMinted_ByBlockClaim,
            minted: -spendasbleMinted_ByBlockClaim,
            bBalance: 0,
            desc: `Spendasble Minted PAIs By Block Claim`
        });
        this.prepareARow({
            date: utils.getNow(),
            account: 'Waited Minted PAIs By Block Claim',
            totalMinted: waitedMinted_ByBlockClaim,
            minted: -waitedMinted_ByBlockClaim,
            bBalance: 0,
            desc: `Waited Minted PAIs By Block Claim`
        });
        this.prepareARow({
            date: utils.getNow(),
            account: 'Waited UTXOs By block claim',
            totalMinted: waitedUTXOs_ByBlockClaim,
            minted: -waitedUTXOs_ByBlockClaim,
            bBalance: 0,
            desc: `Waited UTXOs By block claim`
        });
        this.prepareARow({
            date: utils.getNow(),
            account: 'Waited Treasury By block claim',
            totalMinted: waitedTreasury_ByBlockClaim,
            minted: -waitedTreasury_ByBlockClaim,
            bBalance: 0,
            desc: `Waited Treasury By block claim`
        });
        let spendableUTXOs = utxoHandler.getSpendablesInfo();
        // console.log('spendableUTXOs', spendableUTXOs);
        this.prepareARow({
            date: utils.getNow(),
            account: 'Total spendables retrieved from trx_utxos table',
            totalMinted: spendableUTXOs.sum,
            minted: -spendableUTXOs.sum,
            bBalance: 0,
            desc: `Total spendables retrieved from trx_utxos table`
        });
        this.prepareARow({
            date: utils.getNow(),
            account: 'Double spends Sum',
            totalMinted: this.doubleSpendsSum,
            minted: -this.doubleSpendsSum,
            bBalance: 0,
            desc: `Double spends Sum`
        });


        // prepare a list of unique UTXOs in table trx_utxos
        this.rowNum++;
        this.rowNum++;
        this.prepareAUTXORow({
            refLoc: 'RefLoc',
            outValue: 0,
            creationDate: 'Creation Date',
            visibleBy: 'Visible By',
        });
        let inx = 0;
        for (let aUT of spendableUTXOs.utxos) {
            inx++;
            this.prepareAUTXORow({
                inx,
                refLoc: aUT.refLoc,
                outValue: aUT.outValue,
                creationDate: aUT.refLocCreationDate,
                visibleBy: utils.stringify(spendableUTXOs.utxosDICT[aUT.refLoc].visibleBy.map(x => utils.hash6c(x)).sort()),
            });
        }

        let outbox = iConsts.HD_PATHES.hd_backup_dag;
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0)
            outbox = outbox + appCloneId.toString();
        outbox = `${outbox}/${utils.getNowSSS()}_trx-bBalances.xlsx`;

        this.wb.write(outbox);
        console.log(`############## excel reporter Done! ######### ${outbox}`);
    }



    static prepareAUTXORow(args) {
        this.rowNum++;
        let rowNum = this.rowNum;

        if (_.has(args, 'inx')) {
            this.ws.cell(rowNum, 1).string(_.has(args, 'inx') ? args.inx.toString() : '').style(this.BechAdd);
        }

        if (_.has(args, 'refLoc')) {
            this.ws.cell(rowNum, 2).string(_.has(args, 'refLoc') ? args.refLoc : '').style(this.BechAdd);
        }

        if (_.has(args, 'outValue')) {
            this.ws.cell(rowNum, 3).number(iutils.convertBigIntToJSInt(args.outValue)).style(this.numbers2);
        }

        if (_.has(args, 'creationDate')) {
            this.ws.cell(rowNum, 4).string(args.creationDate).style(this.BechAdd);
        }

        if (_.has(args, 'visibleBy')) {
            this.ws.cell(rowNum, 5).string(args.visibleBy).style(this.BechAdd);
        }
    }



    static prepareARow(args) {

        let dType = _.has(args, 'dType') ? args.dType : null;
        this.rowNum++;
        let rowNum = this.rowNum;

        // this.ws.cell(rowNum, 1).string(_.has(args, 'date') ? args.date.toString() : '').style(this.bhdStyle);

        if (_.has(args, 'account')) {
            let acc = args.account.toString();
            if (iConsts.TREASURY_PAYMENTS.includes(acc)) {
                this.ws.cell(rowNum, 2).string(_.has(args, 'account') ? args.account.toString() : '').style(this.TPIncome);

            } else {
                this.ws.cell(rowNum, 2).string(_.has(args, 'account') ? args.account.toString() : '').style(this.BechAdd);

            }
        }
        if (_.has(args, 'totalMinted')) {
            this.ws.cell(rowNum, 3).number(args.totalMinted).style(this.numbers2);
        }
        if (_.has(args, 'minted')) {
            this.ws.cell(rowNum, 4).number(args.minted).style(this.mainNumbersIn);
        }
        if (_.has(args, 'treasury')) {
            this.ws.cell(rowNum, 5).number(args.treasury).style(this.mainNumbersIn);
        }
        if (_.has(args, 'input')) {
            this.ws.cell(rowNum, 6).number(args.input).style(this.mainNumbersIn);
        }
        if (_.has(args, 'output')) {
            this.ws.cell(rowNum, 7).number(args.output).style(this.mainNumbersOut);
        }
        if (_.has(args, 'bBalance')) {
            let bBalance = args.bBalance;
            if (parseInt(bBalance) > 0) {
                this.ws.cell(rowNum, 8).number(bBalance).style(this.numbers3);
            } else {
                this.ws.cell(rowNum, 8).number(bBalance).style(this.numbers30);
            }
        }
        this.ws.cell(rowNum, 9).string(_.has(args, 'dummy') ? args.dummy.toString() : '');
        this.ws.cell(rowNum, 10).string(_.has(args, 'desc') ? args.desc.toString() : '').style(this.BechAdd);
        if (_.has(args, 'refLoc')) {
            let refLoc = args.refLoc.toString();
            let style = this.BechAdd;

            if (this.usedRefLocs.includes(refLoc) && (dType != iConsts.DOC_TYPES.RpDoc)) {
                // the input is already spend
                style = this.BechAddRed;
                this.doubleSpendsSum += args.input;
            }
            this.ws.cell(rowNum, 11).string(refLoc).style(style);
            this.usedRefLocs.push(refLoc);
        }
        this.ws.cell(rowNum, 12).string(_.has(args, 'newRef') ? args.newRef.toString() : '').style(this.BechAdd);
    }

    static prepareADPIsRow(args) {

        this.rowNum++;
        let rowNum = this.rowNum;

        if (_.has(args, 'output')) {
            this.ws.cell(rowNum, 8).number(args.output).style(this.dpiNumbersOut);
        }
        this.ws.cell(rowNum, 10).string(_.has(args, 'desc') ? args.desc.toString() : '').style(this.BechAdd);
        this.ws.cell(rowNum, 12).string(_.has(args, 'newRef') ? args.newRef.toString() : '').style(this.BechAdd);
    }


    static addHeader() {
        this.rowNum++;
        let rowNum = this.rowNum;

        let inx = 0;
        for (let aHd of CSV_TRX_HEADRS) {
            inx++;
            this.ws.cell(rowNum, inx).string(aHd).style(this.hdStyle);
        }
    }

    static addBlockHd1(args) {
        this.rowNum++;
        let rowNum = this.rowNum;

        let bStyle = this.hdStyle;
        switch (args.bType) {
            case iConsts.BLOCK_TYPES.Coinbase:
                bStyle = this.bhdCoinBase;
                break;

            case iConsts.BLOCK_TYPES.Normal:
                bStyle = this.bhdNromBlock;
                break;

            case iConsts.BLOCK_TYPES.RpBlock:
                bStyle = this.bhdRpBlock;
                break;
        }
        this.ws.cell(rowNum, 1).number(args.blockInx).style(bStyle);
        this.ws.cell(rowNum, 2).string(`${args.bType} ${args.date}`).style(bStyle);
    }

    static addBlockHd2(args) {
        this.rowNum++;
        let rowNum = this.rowNum;
        let bStyle = this.hdStyle;
        switch (args.bType) {
            case iConsts.BLOCK_TYPES.Coinbase:
                bStyle = this.bhdCoinBase;
                break;

            case iConsts.BLOCK_TYPES.Normal:
                bStyle = this.bhdNromBlock;
                break;

            case iConsts.BLOCK_TYPES.RpBlock:
                bStyle = this.bhdRpBlock;
                break;
        }

        this.ws.cell(rowNum, 2).string(`${args.blockHash}`).style(bStyle);
    }

    static initStyles() {
        this.ws.row(1).freeze();
        this.ws.column(1).setWidth(3);
        this.ws.column(2).setWidth(40);
        this.ws.column(3).setWidth(14);
        this.ws.column(4).setWidth(14);
        this.ws.column(5).setWidth(14);
        this.ws.column(6).setWidth(14);
        this.ws.column(7).setWidth(14);
        this.ws.column(8).setWidth(14);
        this.ws.column(9).setWidth(1);
        this.ws.column(10).setWidth(19);
        this.ws.column(11).setWidth(40);
        this.ws.column(12).setWidth(37);
        // const bigi = require("big-integer");
        // Create a reusable style
        this.hdStyle = this.wb.createStyle({
            font: {
                color: '#1F0800',
                size: 10,
            }
        });
        this.bhdCoinBase = this.wb.createStyle({
            font: {
                color: '#d45806',
                size: 10,
            }
        });
        this.bhdNromBlock = this.wb.createStyle({
            font: {
                color: '#1245fc',
                size: 10,
            }
        });
        this.bhdRpBlock = this.wb.createStyle({
            font: {
                color: '#790680',
                size: 10,
            }
        });
        this.bhdStyle = this.wb.createStyle({
            font: {
                color: '#1F2860',
                size: 9,
            }
        });
        this.BechAdd = this.wb.createStyle({
            font: {
                color: '#2F3820',
                size: 7,
            }
        });
        this.BechAddRed = this.wb.createStyle({
            font: {
                color: '#FF2817',
                size: 8,
            }
        });
        this.TPIncome = this.wb.createStyle({
            font: {
                color: '#3E7171',
                size: 7,
            }
        });
        this.nromalStyle = this.wb.createStyle({
            font: {
                color: '#000000',
                size: 8,
            },
            // numberFormat: '$#,##0.00; ($#,##0.00); -',
        });
        this.numbers2 = this.wb.createStyle({
            font: {
                color: '#303030',
                size: 8,
            },
            numberFormat: '#,##0; -#,##0; ',
        });
        this.numbers3 = this.wb.createStyle({
            font: {
                color: '#afb8ab',
                size: 8,
            },
            numberFormat: '#,##0; -#,##0; ',
        });
        this.numbers30 = this.wb.createStyle({
            font: {
                color: '#afb8ab',
                size: 8,
            },
        });
        this.mainNumbersIn = this.wb.createStyle({
            font: {
                color: '#0e4000',
                size: 8,
            },
            numberFormat: '#,##0; -#,##0; ',
        });
        this.mainNumbersOut = this.wb.createStyle({
            font: {
                color: '#222090',
                size: 8,
            },
            numberFormat: '#,##0; -#,##0; ',
        });
        this.dpiNumbersOut = this.wb.createStyle({
            font: {
                color: '#3280C1',
                size: 8,
                fillColor: '3280C1',
                bgColor: '3280C1',
                bgcolor: '3280C1',
            },
            numberFormat: '#,##0; -#,##0; ',
        });

        this.redStyle = this.wb.createStyle({
            font: {
                color: '#FF0800',
                size: 9,
            },
            numberFormat: '$#,##0.00; ($#,##0.00); -',
        });
        // ws.cell(1, 1).number(100).style(style);
        // ws.cell(2, 1).string('string').style(style);
        // ws.cell(3, 1).bool(true).style(style).style({ font: { size: 14 } });
        // ws.cell(3, 2).string('ooo').style(style2).style({ font: { size: 14 } });
    }
}

module.exports = ExcelReporter;
