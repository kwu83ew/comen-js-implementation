const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
// const express = require('express');
// const router = express.Router();
const docBufferHandler = require("../../services/buffer/buffer-handler");
const walletHandler = require("../../web-interface/wallet/wallet-handler");
const walletHandlerLocalUTXOs = require("../../web-interface/wallet/wallet-handler-local-utxos");
const kvHandler = require("../../models/kvalue");

const MINIMUM_INPUTS_COUNT = 1;
const MAXIMUM_INPUTS_COUNT = 3;

async function makeTrx(args) {
    let msg;
    clog.http.info(`get: watcher/makeTrx -> ${utils.stringify(args)}`);
    console.log(`get: watcher/makeTrx -> ${utils.stringify(args)}`);

    let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
    let receivers = _.has(args, 'receivers') ? args.receivers : [];
    let unEqua = _.has(args, 'unEqua') ? args.unEqua : 0;
    let createDateType = _.has(args, 'createDateType') ? args.createDateType : 0;


    msg = `try to make a transaction(${mode})  `;
    clog.trx.info(msg);
    console.log(`msg ${msg}`);

    let trxDtl;
    let trxNeededInfo;
    let maxDPCost;

    if (mode == 'change') {
        msg = `change an input to 10 powered outputs `;
        clog.trx.info(msg);
        console.log(`msg ${msg}`);
        maxDPCost = 99000000;
        trxDtl = await walletHandler.changePAIs({
            minValue: 12345678901,
            maxDPCost
        });
        if (trxDtl.err != false)
            return trxDtl;

    } else {

        maxDPCost = 120000000;
        trxNeededInfo = await walletHandler.creatARandomTrx({
            unEqua,
            mode,
            maxDPCost,
            paiReceivers: receivers,
            minInpCount: MINIMUM_INPUTS_COUNT,
            maxInpCount: MAXIMUM_INPUTS_COUNT,
        });

        console.log(':::::::::::::::');
        console.log(':::::::::::::::');
        console.log(':::::::::::::::');
        console.log(':::::::::::::::');
        console.log(':::::::::::::::trxNeededInfo', utils.stringify(trxNeededInfo));
        console.log(':::::::::::::::');
        console.log(':::::::::::::::');
        console.log(':::::::::::::::');
        if (trxNeededInfo.err != false)
            return trxNeededInfo;

        let newArgs = {
            unEqua,
            maxDPCost,
            DPCostChangeIndex: trxNeededInfo.outputs.length - 1, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,

            description: '',
            inputs: trxNeededInfo.inputs,
            outputs: trxNeededInfo.outputs,

        }
        if ((mode == 'dbl') || (createDateType == 'cheat')) {
            newArgs['creationDate'] = trxNeededInfo.creationDate;
        }
        trxDtl = walletHandler.makeATransaction(newArgs);
        if (trxDtl.err != false)
            return trxDtl;

    }

    clog.trx.info(`creatARandomTrx. trxDtl: ${JSON.stringify(trxDtl)}`);

    // push transaction to Block buffer
    await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost, maxDPCost });
    walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

    if (mode == 'dbl') {
        // save cheating creationDate & ancestors
        await kvHandler.setAsync('cheatingCreationDate', trxNeededInfo.creationDate);
        await kvHandler.setAsync('cheatingAncestors', utils.stringify(trxNeededInfo.ancestors));
    }

    return {};
}



module.exports = makeTrx;
