const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const DAGHandler = require('../../dag/graph-handler/dag-handler');
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const db = require('../../startup/db2')
const fileHandler = require('../../hard-copy/file-handler')
const parsingQHandler = require('../../services/parsing-q-manager/parsing-q-handler');
const toSend = require('../../services/sending-q-manager/sending-q-manager');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const WATCHING_BLOCKS_COUNT = 25;
const leavesHandler = require('../../dag/leaves-handler');
const DNAHandler = require('../../dna/dna-handler');
const cbIC = require('../../dag/coinbase/control-coinbase-issuance-criteria')
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const lib = require('./lib');
const missedBlocksHandler = require('../../dag/missed-blocks-handler');
const blockUtils = require('../../dag/block-utils');
const docBufferHandler = require('../../services/buffer/buffer-handler');

async function getWatcherMainInfo(req) {
    clog.http.info('get: watcher/getWatcherMainInfo');
    let reqCloneId = req.body.cloneId;
    let machineInfo = {};
    let backerAddressDict = {}

    // hard disc
    let inbox = iConsts.HD_PATHES.inbox;
    let outbox = iConsts.HD_PATHES.outbox;
    let appCloneId = db.getAppCloneId();
    if (appCloneId > 0) {
        inbox = inbox + appCloneId.toString();
        outbox = outbox + appCloneId.toString();
    }
    inbox = fileHandler.emailFileHandler.fileList({ dirName: inbox });
    machineInfo['inbox'] = _.orderBy(inbox, 'creation_date', 'asc');
    outbox = fileHandler.emailFileHandler.fileList({ dirName: outbox });
    machineInfo['outbox'] = _.orderBy(outbox, 'creation_date', 'asc');


    let { backer, percentage } = DNAHandler.getMachineShares();
    machineInfo['backer'] = backer;
    machineInfo['percentage'] = Math.round(percentage * 100) / 100;
    machineInfo['percentage'] = percentage;

    hybridInfo = await lib.loadHybridDags(WATCHING_BLOCKS_COUNT);
    hybridInfo = _.orderBy(hybridInfo, 'bCreationDate', 'asc');
    machineInfo.blocks = [];
    hybridInfo.forEach(wBlk => {
        let body = blockUtils.openDBSafeObject(wBlk.bBody).content;
        machineInfo.blocks.push({
            blockHash: body.blockHash,
            docRHash: ((_.has(body, 'docsRootHash')) ? utils.hash6c(body.docsRootHash) : 'uU'),
            ancestors: body.ancestors,
            ancestorsCount: body.ancestors.length,
            bType: body.bType,
            cycle: wBlk.bCycle,
            confidence: Math.round(wBlk.bConfidence * 100) / 100,
            backers: 1,
            backerAddress: wBlk.bBacker,
            creationDate: body.creationDate,
            receiveDate: wBlk.bReceiveDate,
            confirmDate: wBlk.bConfirmDate,
        });
    });
    let dd = await lib.calcDagsSShot();
    machineInfo.DAGShoot = dd.DAGShoot
    machineInfo.histBlocks = dd.histBlocks

    let parsingQ = await parsingQHandler.qUtils.searchQAsync({
        log: false,
        fields: ['pq_code', 'pq_type', 'pq_sender', 'pq_parse_attempts', 'pq_prerequisites'],
        order: [
            ['pq_code', 'ASC']
        ]
    });
    parsingQ.forEach(element => {
        element.prerequisites = utils.unpackCommaSeperated(element.prerequisites)
    });
    machineInfo.parsingQ = parsingQ

    machineInfo.parsingQCount = await parsingQHandler.qUtils.searchQAsync({
        log: false,
        fields: ['COUNT(*) as _count']
    });

    machineInfo.sendingQ = await toSend.fetchFromSendingQAsync({
        log: false,
        fields: ['sq_title', 'sq_sender', 'sq_receiver', 'sq_type', 'sq_code']
    });

    let UTXOs = await utxoHandler.getUTXOReportAsync();
    let totalUTXOs = []
    let UTXOSum = 0
    UTXOs.forEach(elm => {
        let hash = `#${utils.hash6c(elm.ref_loc)}`;
        UTXOSum += iutils.convertBigIntToJSInt(elm.o_value);
        totalUTXOs.push(`${hash}\t${utils.microPAIToPAI(elm.o_value)}`);
    });
    machineInfo.totalUtxosSum = UTXOSum
    machineInfo.totalUtxosCount = totalUTXOs.length
    machineInfo.totalUtxosHint = totalUTXOs.join(`\n`)


    let { cycle, machineEmail, machineKey, emailsHashDict } = cbIC.makeEmailHashDict();
    clog.app.info(`emails Hash Dict ${utils.stringify(emailsHashDict)}`);
    machineInfo.machineCBKey = machineKey;
    machineInfo.emailsHashDict = emailsHashDict;
    machineInfo.machineCoinbasePriority = utils.objKeys(emailsHashDict).sort().indexOf(machineKey);
    let priorityOrder = utils.objKeys(emailsHashDict).sort();
    machineInfo.coinbaseOrder = [];
    for (let aNodeHash of priorityOrder) {
        machineInfo.coinbaseOrder.push(emailsHashDict[aNodeHash]);
    }

    machineInfo.leaves = getDagLeaves();
    machineInfo.freashLeaves = getFreshLeaves();

    machineInfo.askForLeavesInfo = dagMsgHandler.getMaybeAskForLatestBlocksFlag()
    machineInfo.dgaHealth = DAGHandler.watcher.analyzeDAGHealth({ shouldUpdateDescendants: true });
    // let cbBuffer = cbBufferHandler.searchCBBuffer({
    //     // fields: ['block_hash', 'backer_signs', 'confidence', 'ancestors'],
    //     order: [
    //         ['confidence', 'DESC'],
    //         ['backer_signs', 'DESC'],
    //         ['block_hash', 'ASC'],
    //     ]
    // })


    let { minCreationDate, maxCreationDate } = iutils.getCbUTXOsDateRange();
    machineInfo.coinbaseRange = `${minCreationDate.split(' ')[0]} (${minCreationDate.split(' ')[1]} ${maxCreationDate.split(' ')[1]})`

    let missedBlockHashes = utils.unpackCommaSeperated(missedBlocksHandler.getMissedBlocksToInvoke()).map(x => (x)).sort();
    machineInfo.missedBlocks = missedBlockHashes.map(x => `#cc${x}ccccccccc`).sort();

    let existedMissed = await parsingQHandler.qUtils.searchQAsync({
        log: false,
        fields: ['pq_code'],
        query: [
            ['pq_code', ['IN', missedBlockHashes]]
        ]
    })
    machineInfo.notExistMissed = utils.arrayDiff(missedBlockHashes, existedMissed.map(x => x.pq_code)).map(x => `#${x}`).sort();

    // machineInfo.utxo12h = utxo12Handler.get12hShot();
    let { visShoot, UTXOShoot, UTXOsInfo, UTXOsCount } = utxoHandler.getUTXOSHoot();
    machineInfo.visShoot = visShoot;
    machineInfo.UTXOShoot = UTXOShoot;
    machineInfo.UTXOsInfo = UTXOsInfo;
    machineInfo.UTXOsCount = UTXOsCount;

    machineInfo.nodeBuffer = await docBufferHandler.getBufferContentsInfo()

    lastBLockInfo = dagMsgHandler.getLastReceivedBlockTimestamp();
    lastBLockInfo.bType = lastBLockInfo.bType;
    lastBLockInfo.receiveDate = lastBLockInfo.receiveDate.split(' ')[1];
    lastBLockInfo.hash = '#' + utils.hash6c(lastBLockInfo.hash);
    machineInfo.lastBLockInfo = lastBLockInfo;


    let walletAddresses = await walletAddressHandler.getAddressesListAsync({ mpCode: iConsts.CONSTS.ALL, sum: false, fields: ['wa_address'] });
    machineInfo.walletAddresses = walletAddresses.map(x => x.waAddress);
    // res.forEach(element => {
    // });

    let cycleRange = iutils.getCoinbaseRange();
    let imgInfo = {
        cycleNumber: iutils.getCoinbaseCycleStamp().split(' ')[1],
        cycleFrom: cycleRange.from.split(' ')[1],
        cycleTo: cycleRange.to.split(' ')[1],
        machineTime: utils.getNow()
    }

    let res = { imgInfo, machineInfo };
    // console.log(JSON.stringify(res));
    return res;

}

function getDagLeaves() {
    let leaves = leavesHandler.getLeaveBlocks();
    let newRes = [];
    let hashes = utils.objKeys(leaves).sort()
    hashes.forEach(hash => {
        //border: 1px dashed #9f2f9f; 
        newRes.push({
            hash: '#' + utils.hash6c(hash),
            date: leaves[hash].date.split(' ')[1],
            bType: leaves[hash].bType

        });
    });
    return newRes;
}


function getFreshLeaves() {
    let leaves = leavesHandler.getFreshLeaves();
    let newRes = [];
    let hashes = utils.objKeys(leaves).sort()
    hashes.forEach(hash => {
        //border: 1px dashed #9f2f9f; 
        newRes.push({
            hash: '#' + utils.hash6c(hash),
            date: leaves[hash].date.split(' ')[1],
            bType: leaves[hash].bType

        });
    });
    return newRes;
}



module.exports = getWatcherMainInfo;
