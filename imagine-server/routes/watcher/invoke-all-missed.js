const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const router = express.Router();
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const missedBlocksHandler = require('../../dag/missed-blocks-handler');
const model = require('../../models/interface');
const parsingQHandler = require('../../services/parsing-q-manager/parsing-q-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');


class MissedInvokHandler {

    static invokeAllMissed(req) {
        clog.http.info('get: watcher/invokeAllMissed');
        missedBlocksHandler.invokeAllMissed();
        return {};
    }

    static refreshMissedBlock() {
        clog.http.info('get: watcher/refreshMissedBlock');
        missedBlocksHandler.refreshMissedBlock();
    }
}
module.exports = MissedInvokHandler

