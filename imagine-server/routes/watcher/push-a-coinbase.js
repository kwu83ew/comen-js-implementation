const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager')
const machine = require('../../machine/machine-handler');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const leavesHandler = require('../../dag/leaves-handler');
const blockHasher = require('../../dag/hashable');
const blockUtils = require('../../dag/block-utils');

// const cbIC = require('../../dag/coinbase/control-coinbase-issuance-criteria')

// theorically the coinbase block can be created by any one, 
// and the root hash of the block could be different(because of adifferent ancesters).
function pushCoinbase(args) {
    let cbInfo = iutils.getCoinbaseInfo({ cDate: utils.getNow() })

    clog.cb.info(`$$$$$$$$$$ Try to Create Coinbase manually for Range ${cbInfo.from.split(' ')[0]} from:${cbInfo.fromHour}`);

    cycle = iutils.getCoinbaseCycleStamp();
    cbInfo = iutils.getCoinbaseInfo({
        cycle: cycle
    })
    let block = require("../../dag/coinbase/do-create-coinbase-block").createCBCore({ cycle });

    // connecting to existed leaves as ancestors
    let leaves = leavesHandler.getLeaveBlocks({
        maxCreationDate: cbInfo.from
    });
    leaves = utils.objKeys(leaves);
    clog.cb.info(`manually cb do GenerateCoinbaseBlock retrieved leaves from kv: ${JSON.stringify(leaves)} cycle: ${cycle} from: ${cbInfo.from}`);
    // clog.cb.info(`do GenerateCoinbaseBlock block ancestors: ${currentAncestors.join()} `);
    // currentAncestors = utils.arrayAdd(currentAncestors, leaves).sort();
    clog.cb.info(`do GenerateCoinbaseBlock aggrigated ancestors: ${leaves.sort().join()} `);

    let floatingSignatures = dagHandler.fSigs.aggrigateFloatingSignatures();
    clog.cb.info(`manually cb locally created block's confidence: ${JSON.stringify(floatingSignatures)}`);
    block.confidence = floatingSignatures.confidence
    leaves = utils.arrayUnique(utils.arrayAdd(leaves, floatingSignatures.block_hashes));


    // if machine is in current cycle and hasn't fresh leaves, so can not generate a CB block
    if ((leaves.length == 0) && (cycle == iutils.getCoinbaseCycleStamp())) {
        clog.app.error(`creating local CB faild! no leaves exist for current cycle!`);
        return null;
    }
    block.ancestors = leaves.sort();
    // dummy duplicating random ancestors to change block hash
    block.ancestors.push(block.ancestors[parseInt(Math.random() * block.ancestors.length)]);
    block.ancestors = blockUtils.normalizeAncestors(block.ancestors);
    clog.cb.info(`manually cb do GenerateCoinbaseBlock block.ancestors: ${block.ancestors} `);

    block.blockLength = iutils.offsettingLength(JSON.stringify(block).length);
    block.blockHash = blockHasher.hashBlock(block);








    clog.cb.info(`manually cb local manually coinbase for cycle ${block.cycle} fromHour:${cbInfo.fromHour}) block=> ${JSON.stringify(block)}`)

    if (utils._nilEmptyFalse(block)) {
        clog.cb.error(`Due to an errore, can not create a coinbase block`);
        return null
    }
    let tmpLocalConfidence = block.confidence;

    // if local machine can create a coinbase block with more confidence or ancestors, broadcast it
    let mostConfidenceInDAG = dagHandler.getMostConfidenceCoinbaseBlockFromDAG();
    let DAGHasNotCB = utils._nilEmptyFalse(mostConfidenceInDAG);
    let tmpDAGConfidence, tmpDAGAncestors;
    if (DAGHasNotCB) {
        clog.cb.info(`manually cb DAG hasn't cb for cycle ${block.cycle} fromHour:${cbInfo.fromHour})`)
        tmpDAGConfidence = 0.0
        tmpDAGAncestors = 0
    } else {
        clog.cb.info(`manually cb mostConfidenceInDAG for cycle ${block.cycle} fromHour:${cbInfo.fromHour}): ${JSON.stringify(mostConfidenceInDAG)}`)
        tmpDAGConfidence = _.has(mostConfidenceInDAG, 'bConfidence') ? mostConfidenceInDAG.bConfidence : 0.0
        tmpDAGAncestors = _.has(mostConfidenceInDAG, 'bAncestors') ? utils.parse(mostConfidenceInDAG.bAncestors).length : 0
    }

    let localHasMoreConfidenceThanDAG = (tmpDAGConfidence < tmpLocalConfidence);
    if (localHasMoreConfidenceThanDAG)
        clog.cb.info(`manually cb local coinbase(${utils.hash6c(block.blockHash)}) has more confidence(${tmpLocalConfidence}) than DAG(${tmpDAGConfidence}) cycle:${block.cycle} from: ${cbInfo.fromHour}`)


    let tmpLocalAncestors = block.ancestors.length
    let localHasMoreAncestorsThanDAG = (tmpDAGAncestors < tmpLocalAncestors)
    if (localHasMoreAncestorsThanDAG)
        clog.cb.info(`manually cb local coinbase(${utils.hash6c(block.blockHash)}) has more ancestors(${tmpLocalAncestors}) than recordedCB in DAG(${tmpDAGAncestors}) for ${block.cycle} fromHour:${cbInfo.fromHour}`)

    let machineSettings = machine.getMProfileSettingsSync();
    // let block = blockLib.blockTruncateToBroadcast(block);
    clog.cb.info(`manually cb block: ${JSON.stringify(block)}`);

    // if (localHasMoreConfidenceThanDAG || localHasMoreAncestorsThanDAG) {

    // broadcast coin base
    if (iutils.isInCurrentCycle(block.creationDate)) {
        sendingQ.pushIntoSendingQ({
            sqType: iConsts.BLOCK_TYPES.Coinbase,
            sqCode: block.blockHash,
            sqPayload: utils.stringify(block),
            sqTitle: `manually created coinbase block(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `,
        });
        clog.cb.info(`\n Broadcasted manually Coinbase block(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour}`)
        // canGenCB.coinbasePushedTosend = true
        // return canGenCB
    }

    // } else if (iutils.passedCertainTimeOfCycleToRecordInDAG() && DAGHasNotCB) {
    //     // another psudo random emulatore
    //     // if already passed more than 1/4 of cycle and still no coinbase block recorded in DAG, 
    //     // so the machine has to create one
    //     if (cbIC.haveIFirstHashedEmail('desc')) {
    //         sendingQ.pushIntoSendingQ({
    //             sq_type: iConsts.BLOCK_TYPES.Coinbase,
    //             sq_code: utils.hash6c(block.blockHash),
    //             payload: JSON.stringify(block),
    //             title: `coinbase block(${utils.hash6c(block.blockHash)}) issuance cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour} `,
    //         });
    //         clog.cb.info(`\nCoinbase(${utils.hash6c(block.blockHash)}) issued by ${machineSettings.pubEmail.address} ${iutils.getCoinbaseCycleStamp()} because of 1/4 of cycle passed`)
    //         canGenCB.coinbasePushedTosend = true
    //         return canGenCB
    //     }

    // } else {
    //     clog.cb.info(`\nCoinbase(${utils.hash6c(block.blockHash)}) can be issued by ${machineSettings.pubEmail.address} cycle: ${cbInfo.cycleStamp} fromHour: ${cbInfo.fromHour}) 
    //     but local hasn't neither more confidence nor more ancestors and still not riched to 1/4 of cycle time`)

    // }

    // canGenCB.coinbasePushedTosend = false
    // return canGenCB
    return {};
}


module.exports = pushCoinbase;
