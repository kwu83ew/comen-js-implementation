const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const router = express.Router();
const dagMsgHandler = require("../../messaging-protocol/dag/dag-msg-handler");



function doInvokeBlock(req) {
    clog.http.info('get: watcher/doInvokeBlock');
    let blockShortHash = req.body.blockShortHash;
    let res = dagMsgHandler.invokeBlock(blockShortHash);
    return {res};
}
module.exports = doInvokeBlock

