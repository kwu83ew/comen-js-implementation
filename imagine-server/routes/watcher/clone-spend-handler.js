const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require("../../utils/utils");
const iutils = require('../../utils/iutils');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const trxHashHandler = require('../../transaction/hashable');
const signingInputOutputs = require('../../transaction/input-output-signer').signingInputOutputs;
// const trxHandler = require('../../transaction/interface');
const machine = require('../../machine/machine-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const blockHandler = require('../../dag/normal-block/normal-block-handler/normal-block-handler');

const MAX_UTXO_PER_NODE = 5;
const MAX_INPUT_IN_TRX = 15;
const MIN_DP_COST = 1;

class CloneSpendHandler {

    static async broadcastClonedTrx(args) {
        console.log('+++ broadcastClonedTrx');
        // delete from  i_machine_buffer_documents;
        let finalCreationDate = utils.getNow();
        let ancestors = [];

        let trx = args.trx;
        let backersDict = {};
        trx.outputs.forEach(out => {
            backersDict[out[0]] = out[1];
        });
        const machineSettings = await machine.getMProfileSettingsAsync();
        let DPCost = _.has(backersDict, machineSettings.backerAddress) ? backersDict[machineSettings.backerAddress] : 0;
        // push transaction to Block buffer
        await docBufferHandler.pushInAsync({ doc: trx, DPCost });
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trx);


        // create another random trx
        let createMoreTrx = false;
        if (createMoreTrx) {
            let trxDtl = await walletHandler.creatARandomTrx();
            await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
            walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);
            finalCreationDate = trxDtl.finalCreationDate;
            ancestors = trxDtl.ancestors;
        }

        let mbr = blockHandler.createANormalBlock({
            creationDate: finalCreationDate,
            ancestors
        });
        if (mbr.err != false) {
            console.log(`mbr ${utils.stringify(mbr)}`);
            return mbr;
        }
        return { err: false };

    }

    /**
     * 
     * @param {*} args 
     * cloned trasaction is a type of normasl transaction in which the wallet signs and sends same transaction for more than one backer in order to be sure and fast confirm of transaction
     * obviously the wallet for each backer has to pay DPCost
     */
    static async signClonedTrx(args) {
        console.log('sign Cloned Trx args', args);
        let msg;
        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl

        const machineSettings = await machine.getMProfileSettingsAsync();


        let cheatingBlockCreateDate;
        let finalCreationDate = (mode == 'dbl') ? cheatingBlockCreateDate : utils.getNow();


        let candidBackers = args.candidBackers;
        let backersCount = parseInt(Math.random() * (utils.objKeys(candidBackers).length - 1)) + 2;
        let selectedBackers = [machineSettings.backerAddress]; // the first fee earner can be()but not necessarily the very machine
        for (let i = 0; i < backersCount; i++)
            selectedBackers.push(utils.objKeys(candidBackers)[parseInt(Math.random() * utils.objKeys(candidBackers).length)]);
        selectedBackers = utils.arrayUnique(selectedBackers);
        console.log('+++++++++ selectedBackers', selectedBackers);


        let maxDPCost = 120000000;

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: maxDPCost
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;
        let inputs = spendables.selectedCoins;


        let changeAddress = await walletHandler.getAnOutputAddressAsync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });

        let outputs = [
            [changeAddress, 1]  // a new address for change back
        ];

        let trxNeededArgs = {

            maxDPCost,
            DPCostChangeIndex: 0, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,

            description: 'clone trx',
            inputs,
            outputs: outputs,

            backersAddresses: selectedBackers

        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        let trx = trxDtl.trx;
        // console.log('signed clone trx: ', utils.stringify(trxDtl));


        if (mode != 'dbl') {
            let doesUTXOUsed = await walletHandlerLocalUTXOs.doesUsedUTXO(trx);
            if (doesUTXOUsed) {
                msg = `clone: One or more funds are already used and you must not use it in next 12 hours in order to to avoid double spending.`;
                console.log(`msg ${msg}`);
                clog.sec.error(msg);
                clog.sec.error(utils.stringify(trx));
                return;
            }
        }

        let res = { trx, candidBackers, selectedBackers, DPCost: trxDtl.DPCost };
        console.log(`signClonedTrx res: ${utils.stringify(res)}`);
        clog.app.info(`signClonedTrx res: ${utils.stringify(res)}`);
        return res;

    }

    static async getLocalBackerAddress(args) {
        const machineSettings = await machine.getMProfileSettingsAsync();
        return { backerAddress: machineSettings.backerAddress };
    }

    static async getSomeSpendableRefs() {
        let spendableUTXOs = await walletHandler.retrieveSpendableUTXOsAsync(); //will return valid(or invalid in case of doublespend tests) inputs
        if (spendableUTXOs.length == 0) {
            console.error('there is no spendableUTXOs');
        }
        let maxUtxo = MAX_UTXO_PER_NODE;
        if (spendableUTXOs.length < MAX_UTXO_PER_NODE)
            maxUtxo = spendableUTXOs.length;
        let selectedUTXOs = [];
        for (let inx = 0; inx < maxUtxo; inx++) {
            selectedUTXOs.push(spendableUTXOs[parseInt(Math.random() * maxUtxo)]);
        }

        let addressesDict = {};
        let walletAddresses = await walletAddressHandler.getAddressesListAsync({ sum: false, fields: ['wa_address'] });
        walletAddresses = walletAddresses.map(x => x.waAddress);
        let addressesInfo = walletAddressHandler.getAddressesInfoSync(walletAddresses);
        addressesInfo.forEach(element => {
            addressesDict[element.waAddress] = utils.parse(element.waDetail);
        });

        let utxoInfo = [];
        for (let utxo of selectedUTXOs) {
            // let unlockerIndex = _.has(utxo, 'unlockerIndex') ? utxo.unlockerIndex : 0; // if client didn't mention, chose the first unlock struct FIXME =0
            utxoInfo.push({
                refLoc: utxo.utCoin,
                address: utxo.utOAddress,
                value: iutils.convertBigIntToJSInt(utxo.utOValue),
                unlock: addressesDict[utxo.utOAddress]
            });
        };



        const machineSettings = await machine.getMProfileSettingsAsync();

        let backerInfo = {
            address: machineSettings.backerAddress,
            unlock: addressesDict[machineSettings.backerAddress]
        }


        let res = { utxoInfo, backerInfo }
        console.log('res', res);
        return res;
    }


}


module.exports = CloneSpendHandler;
