const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const router = express.Router();
const dagMsgHandler = require('../../messaging-protocol/dag/dag-msg-handler');
const db = require('../../startup/db2')
const toParse = require('../../services/parsing-q-manager/parsing-q-handler');
const toSend = require('../../services/sending-q-manager/sending-q-manager');
const utxoHandler = require('../../transaction/utxo/utxo-handler');
const crypto = require('../../crypto/crypto');
const WATCHING_BLOCKS_DTL_COUNT = 100;
const leavesHandler = require('../../dag/leaves-handler');
const coinbaseUTXOs = require('../../dag/coinbase/import-utxo-from-coinbases');
const machine = require('../../machine/machine-handler');
const Moment = require('../../startup/singleton').instance.Moment;
const dnaHandler = require('../../dna/dna-handler');
const cbIC = require('../../dag/coinbase/control-coinbase-issuance-criteria')
const lib = require('./lib');
const blockUtils = require('../../dag/block-utils');

async function detailedDag(req) {
    clog.http.info('get: watcher/detailedDag');
    let machineInfo = {};

    hybridInfo = await lib.loadHybridDags(WATCHING_BLOCKS_DTL_COUNT);
    hybridInfo = _.orderBy(hybridInfo, 'bCreationDate', 'asc');
    machineInfo.blocks = [];
    hybridInfo.forEach(wBlk => {
        let body = blockUtils.openDBSafeObject(wBlk.bBody).content;
        machineInfo.blocks.push({
            blockHash: body.blockHash,
            docRHash: ((_.has(body, 'docsRootHash')) ? utils.hash6c(body.docsRootHash) : 'uU'),
            ancestors: body.ancestors,
            ancestorsCount: body.ancestors.length,
            bType: body.bType,
            cycle: wBlk.bCycle,
            confidence: Math.round(wBlk.bConfidence * 100) / 100,
            backerAddress: wBlk.bBacker,
            creationDate: body.creationDate,
            receiveDate: wBlk.bReceiveDate,
            confirmDate: wBlk.bConfirmDate,
        });
    });
    let dd = await lib.calcDagsSShot();
    machineInfo.DAGShoot = dd.DAGShoot
    machineInfo.histBlocks = dd.histBlocks

    let cycleRange = iutils.getCoinbaseRange();
    let imgInfo = {
        cycleNumber: iutils.getCoinbaseCycleStamp().split(' ')[1],
        cycleFrom: cycleRange.from.split(' ')[1],
        cycleTo: cycleRange.to.split(' ')[1],
        machineTime: utils.getNow()
    }

    let res = { imgInfo, machineInfo };
    return res;

}
module.exports = detailedDag;
