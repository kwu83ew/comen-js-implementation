const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const router = express.Router();
const getMachineInfo = require('./main-info');
const detailedDag = require('./detailed-dag');
const getBackerInfo = require('./backer-info');
const getCheatingInfo = require('./cheating-info');
const doInvokeBlock = require('./do-invoke-block');
const invokeMissedHandler = require('./invoke-all-missed');
const makeTrx = require('./make-trx');
const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
const watcherShareHandler = require('./watcher-share-handler');
const pushCoinbase = require('./push-a-coinbase');
const cloneSpendHandler = require('./clone-spend-handler');
const p4PHandler = require('./p4p-handler');
const machineBalanceHandler = require('./machine-balance-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const DAGHandler = require('../../dag/graph-handler/dag-handler');
const blockUtils = require('../../dag/block-utils');
const DNAHandler = require('../../dna/dna-handler');

router.put("/makeTrx", async (req, res) => {
    clog.http.info(`put: watcher/makeTrx: `);
    let r = await makeTrx(req.body);
    console.log(`put: watcher/makeTrx: ------------------- ${utils.stringify(r)}`);
    res.send(r);
});

router.get("/wGetShares", async (req, res) => {
    clog.http.info(`get: watcher/getShares: ${req.body.cloneId}`);
    console.log(`get: watcher/getShares: ------------------- ${req.body.cloneId}`);
    let r = await watcherShareHandler.wGetShares();
    res.send(r);
});
router.put("/wRaiseShares", async (req, res) => {
    clog.http.info(`get: watcher/wRaiseShares: ${req.body.cloneId}`);
    console.log(`get: watcher/wRaiseShares: ------------------- ${req.body.cloneId}`);
    let r = await watcherShareHandler.wRaiseShares(req.body);
    res.send(r);
});

router.put("/wBroadcastBlock", async (req, res) => {
    clog.http.info(`get: watcher/wBroadcastBlock: ${req.body.cloneId}`);
    console.log(`get: watcher/wBroadcastBlock: ------------------- ${req.body.cloneId}`);
    let r = await wBroadcastBlock.broadcastBlockAsync(req.body);
    res.send(r);
});

router.put("/pushCoinbase", async (req, res) => {
    clog.http.info(`get: watcher/pushCoinbase: ${req.body.cloneId}`);
    console.log(`get: watcher/pushCoinbase: ------------------- ${req.body.cloneId}`);
    let r = pushCoinbase(req.body);
    res.send(r);
});

router.put("/machineBalanceChk", async (req, res) => {
    clog.http.info(`get: watcher/machineBalanceChk: ${req.body.cloneId}`);
    console.log(`get: watcher/machineBalanceChk: ------------------- ${req.body.cloneId}`);
    let r = await machineBalanceHandler.machineBalanceChk(req.body);
    res.send(r);
});

router.put("/blockGenReport", async (req, res) => {
    clog.http.info(`get: watcher/blockGenReport: ${req.body.cloneId}`);
    console.log(`get: watcher/blockGenReport: ------------------- ${req.body.cloneId}`);
    let r = await machineBalanceHandler.blockGenReport(req.body);
    res.send(r);
});
router.put("/dlTrxList", async (req, res) => {
    let r = await machineBalanceHandler.excelReporter.dlTrxList(req.body);
    res.send(r);
});
router.put("/notInHistory", async (req, res) => {
    let r = await machineBalanceHandler.notInHistory(req.body);
    res.send(r);
});
router.put("/getWAncestors", async (req, res) => {
    let r = await machineBalanceHandler.getWAncestors(req.body);
    res.send(r);
});
router.put("/getBlockInfo", async (req, res) => {
    console.log(`getBlockInfo args: `, req.body);
    let blockInfo = await DAGHandler.searchInDAGAsync({ query: [['b_hash', ['LIKE', `${req.body.bShortHash}%`]]] });
    blockInfo = blockInfo[0]
    blockInfo.bBody = blockUtils.openDBSafeObject(blockInfo.bBody).content;

    let trxStatusDict = {};
    // retrieve import logs (if exist)
    const logNormalBlockUTXOsImport = require('../../dag/normal-block/log-import-utxo-from-normal-blocks');
    let importLogs = await logNormalBlockUTXOsImport.getImportLogs({ blockHash: blockInfo.bBody.blockHash });
    blockInfo.importLogs = {};
    let tmpLogsDict = {};
    for (let aLog of importLogs) {
        let tmpRep = utils.parse(aLog.li_report)
        if (aLog.li_title == 'transactions Validity Check') {
            for (let aTrxHash of utils.objKeys(tmpRep)) {
                if (_.has(tmpRep[aTrxHash], 'cloned') && (tmpRep[aTrxHash]['cloned'] == 'cloned'))
                    tmpLogsDict[aTrxHash] = 'Cloned Transaction';
            }
        }

        aLog.li_report = `<pre>${utils.syntaxHighlight(tmpRep)}</pre>`;
        blockInfo.importLogs[aLog.li_title] = aLog;

    }

    if (_.has(blockInfo.bBody, 'docs')) {
        for (let aDoc of blockInfo.bBody.docs) {
            let key = `trx(${utils.hash6c(aDoc.hash)})`;

            let mayCloned = null;
            if (_.has(tmpLogsDict, key)) {
                mayCloned = tmpLogsDict[key];
            } else if (_.has(tmpLogsDict, aDoc.hash)) {
                mayCloned = tmpLogsDict[aDoc.hash];
            }

            if (mayCloned != null) {
                trxStatusDict[aDoc.hash] = mayCloned;
            }
        }
    }

    // retrieve sus Info (if exist)
    if (blockInfo.bBody.bType == iConsts.BLOCK_TYPES.FVote) {
        blockInfo.bBody.voteData = `<pre>${utils.syntaxHighlight(blockInfo.bBody.voteData)}</pre>`;
        // retrtieve voter confidence
        blockInfo.voterShares = DNAHandler.getAnAddressShares(blockInfo.bBody.backer, blockInfo.bBody.creationDate);
    }

    // controll if block uses rejected coins?
    const rejectedTrxHandler = require('../../dag/normal-block/rejected-transactions-handler');
    let rejTrxs = await rejectedTrxHandler.searchRejTrxAsync({
        query: [
            ['rt_block_hash', blockInfo.bBody.blockHash]
        ]
    });
    blockInfo.rejCoins = [];
    for (let aRej of rejTrxs) {
        blockInfo.rejCoins.push(aRej.rt_coin);
        trxStatusDict[aRej.rt_doc_hash] = 'Rejected Transaction';
    }

    blockInfo.trxStatusDict = trxStatusDict;

    // console.log(`getBlockInfo res: `, blockInfo.bBody.docs[2]);
    res.send(blockInfo);
});


// router.put("/getSomeSpendableRefs", async(req, res) => {
//     clog.http.info(`get: watcher/getSomeSpendableRefs: ${req.body.cloneId}`);
//     console.log(`get: watcher/getSomeSpendableRefs: ------------------- ${req.body.cloneId}`);
//     let r = await cloneSpendHandler.getSomeSpendableRefs(req.body);
//     res.send(r);
// });
router.put("/getLocalBackerAddress", async (req, res) => {
    clog.http.info(`get: watcher/getLocalBackerAddress`);
    console.log(`get: watcher/getLocalBackerAddress`);
    let r = await cloneSpendHandler.getLocalBackerAddress(req.body);
    res.send(r);
});
router.put("/signClonedTrx", async (req, res) => {
    clog.http.info(`get: watcher/signClonedTrx`);
    console.log(`get: watcher/signClonedTrx`);
    let r = await cloneSpendHandler.signClonedTrx(req.body);
    res.send(r);
});
router.put("/broadcastClonedTrx", async (req, res) => {
    clog.http.info(`get: watcher/broadcastClonedTrx`);
    console.log(`get: watcher/broadcastClonedTrx`);
    let r = await cloneSpendHandler.broadcastClonedTrx(req.body);
    res.send(r);
});

router.put("/signP4PTrx", async (req, res) => {
    clog.http.info(`get: watcher/signP4PTrx`);
    console.log(`get: watcher/signP4PTrx`);
    let r = await p4PHandler.signP4PTrx(req.body);
    res.send(r);
});
router.put("/sendP4PWrapper", async (req, res) => {
    clog.http.info(`get: watcher/sendP4PWrapper`);
    console.log(`get: watcher/sendP4PWrapper`);
    let r = await p4PHandler.sendP4PWrapper(req.body);
    res.send(r);
});
router.put("/sendP4POrg", async (req, res) => {
    clog.http.info(`get: watcher/sendP4POrg`);
    console.log(`get: watcher/sendP4POrg`);
    let r = await p4PHandler.sendP4POrg(req.body);
    res.send(r);
});

// router.put("/aMachineStat", async (req, res) => {
//     let r = await aMachine(req);
//     return res.send(r);
// });



router.get("/getMachineInfo", async (req, res) => {
    clog.http.info(`get: watcher/getMachineInfo`);
    let r = await getMachineInfo(req)
    res.send(r);
});

router.get("/detailedDag", async (req, res) => {
    let r = await detailedDag(req)
    res.send(r);
});

router.get("/getBackerInfo", async (req, res) => {
    let r = getBackerInfo(req)
    res.send(r);
});

router.get("/getCheatingInfo", async (req, res) => {
    let r = await getCheatingInfo(req)
    res.send(r);
});

router.put("/doInvokeBlock", async (req, res) => {
    let r = doInvokeBlock(req)
    res.send(r);
});

router.put("/rmvFromBuffer", async (req, res) => {
    let r = await docBufferHandler.removeFromBufferAsync({ hashes: [req.body.bufHash] })
    res.send(r);
});

router.put("/invokeAllMissed", async (req, res) => {
    let r = invokeMissedHandler.invokeAllMissed(req)
    res.send(r);
});
router.get("/refreshMissedBlock", async (req, res) => {
    let r = invokeMissedHandler.refreshMissedBlock(req)
    res.send(r);
});



module.exports = router;
