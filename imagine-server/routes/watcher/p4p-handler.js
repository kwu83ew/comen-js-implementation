const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const machine = require('../../machine/machine-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const blockHandler = require('../../dag/normal-block/normal-block-handler/normal-block-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');


/** imagine you signed a transaction and sent it to a node to put it in  next block.
 * what if the node do not record your trx in block? and asks bribe?
 * you have not your money and if you try to spend the inputs in other trx, the node will release your previously signe trx 
 * and finished to double spending.
 * solution:
 * there is another transaction type which called pay4pay and it is used wrap an already signed transaction in order to record it in blockchain.
 * 
 */

 
const MINIMUM_INPUTS_COUNT = 1;
const MAXIMUM_INPUTS_COUNT = 3;

class P4PdHandler {

    static async signP4PTrx(args) {
        let msg;
        clog.http.info(`get: watcher/signP4PTrx`);
        console.log(`get: watcher/signP4PTrx`);
        console.log(`args ${args}`);

        let testCase = _.has(args, 'testCase') ? args.testCase : null;  // could be normal or dbl
        if (!utils._nilEmptyFalse(testCase))
            return await this.signDefP4PTrx(args);

        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
        let receivers = _.has(args, 'candidBackers') ? utils.objKeys(args.candidBackers) : [];
        let unEqua = _.has(args, 'unEqua') ? args.unEqua : 0;


        msg = `try to make a "${mode}" P4P trx `;
        clog.trx.info(msg);
        console.log(`msg ${msg}`);

        const machineSettings = await machine.getMProfileSettingsAsync();

        let maxDPCost = 120000000;
        let trxNeededInfo = await walletHandler.creatARandomTrx({
            unEqua,
            mode,
            maxDPCost,
            paiReceivers: receivers,
            minInpCount: MINIMUM_INPUTS_COUNT,
            maxInpCount: MAXIMUM_INPUTS_COUNT,
        });
        console.log('trxNeededInfo', utils.stringify(trxNeededInfo));

        let trxNeededArgs = {

            maxDPCost,
            DPCostChangeIndex: trxNeededInfo.outputs.length - 1, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,

            description: 'ceased Normal Trx by Backer',
            inputs: trxNeededInfo.inputs,
            outputs: trxNeededInfo.outputs,

        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;

        console.log('::::::::::::::::::::::::::::::::::::::::::::::::::');
        trxDtl['orgBacker'] = machineSettings.backerAddress;
        clog.trx.info(`signP4PTrx. Ceased Trx. trxDtl: ${JSON.stringify(trxDtl)}`);
        console.log(`signP4PTrx. Ceased Trx. trxDtl: ${JSON.stringify(trxDtl)}`);
        return { trxDtl };

    }

    static async signDefP4PTrx(args) {
        let msg;
        clog.http.info(`get: watcher/signDefP4PTrx`);
        console.log(`get: watcher/signDefP4PTrx`);
        console.log(`args ${args}`);

        let testCase = _.has(args, 'testCase') ? args.testCase : null;
        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
        let receivers = _.has(args, 'candidBackers') ? utils.objKeys(args.candidBackers) : [];
        let unEqua = _.has(args, 'unEqua') ? args.unEqua : 0;


        msg = `try to make a definit test "${testCase}" P4P trx `;
        clog.trx.info(msg);
        console.log(`msg ${msg}`);


        let maxDPCost = 120000000;

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: maxDPCost
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        let inputs = spendables.selectedCoins;
        console.log(`inputs: ${utils.stringify(inputs)}`);

        let targetAddress = inputs[utils.objKeys(inputs)[0]]
        console.log(`targetAddress: ${utils.stringify(targetAddress)}`);

        let transferredValue = 500;
        let outputs = [
            [targetAddress.address, 1],  // a new address for change back
            [targetAddress.address, transferredValue],  // transferred value
        ];

        let trxNeededArgs = {

            maxDPCost,
            DPCostChangeIndex: 0, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,

            description: `ceased Trx by Backer test(${testCase})`,
            inputs: inputs,
            outputs: outputs

        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;

        trxDtl.testCase = testCase;
        trxDtl.targetAddress = targetAddress.address;
        trxDtl.inputSum = targetAddress.value;
        trxDtl.transferredValue = transferredValue;
        trxDtl.changeBack = trxDtl.inputSum - transferredValue - trxDtl.DPCost;
        const machineSettings = await machine.getMProfileSettingsAsync();
        trxDtl.orgBacker = machineSettings.backerAddress;
        console.log('signed ceased trx: ', utils.stringify(trxDtl));
        return trxDtl;

    }

    static async sendP4POrg(args) {
        let msg;
        clog.http.info(`get: watcher/sendP4POrg`);
        console.log(`get: watcher/sendP4POrg`);
        console.log(`args ${args}`);
        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
        msg = `try to make a Org P4P "${mode}" trx `;
        clog.trx.info(msg);
        console.log(`msg ${msg}`);
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(args.trx);

        // push transaction to Block buffer
        await docBufferHandler.pushInAsync({ doc: args.trx, DPCost: args.DPCost });

        let p4pMBR = blockHandler.createANormalBlock({
            creationDate: args.finalCreationDate,
            ancestors: args.ancestors
        });
        if (p4pMBR.err != false)
            console.log(`p4pMBR ${utils.stringify(p4pMBR)}`);

        return {};
    }

    static async sendP4PWrapper(args) {
        let msg;
        clog.http.info(`get: watcher/sendP4PWrapper`);
        console.log(`get: watcher/sendP4PWrapper`);
        console.log('sendP4PWrapper args', JSON.stringify(args));

        let mode = _.has(args, 'mode') ? args.mode : 'normal';  // could be normal or dbl
        let testCase = _.has(args, 'testCase') ? args.testCase : null;

        let orgTrx = args.trx;

        let maxDPCost = 120000000;

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: maxDPCost
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;
        let inputs = spendables.selectedCoins;

        if (utils._nilEmptyFalse(inputs)) {
            console.log('there is no enough input to make P4P Trx');
            return null;
        }

        // console.log(' inputs inputs inputs inputs inputs inputs inputs ');
        // console.log(inputs);
        // console.log(' inputs inputs inputs inputs inputs inputs inputs ');

        let changeAddress = await walletHandler.getAnOutputAddressAsync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });

        let outputs = [
            [changeAddress, 1]  // a new address for change back
        ];


        let trxNeededArgs = {

            maxDPCost,
            DPCostChangeIndex: 0, // to change back

            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.P4P,

            description: 'P4p Warpper',
            inputs: inputs,
            outputs: outputs,

            ref: orgTrx.hash,
            supportedP4PTrxLength: orgTrx.length

        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        let p4pTrx = trxDtl.trx
        clog.trx.info(`P4PInfo: ${utils.stringify(trxDtl)}`);
        console.log(`...... P4PInfo: ${utils.stringify(trxDtl)}`);

        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(orgTrx);
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(p4pTrx);
        let trxs = [
            [utils.hash6c(orgTrx.hash), 'org'],
            [utils.hash6c(p4pTrx.hash), 'P4P'],
        ];

        if (utils._nilEmptyFalse(testCase)) {
            // make some random trx
            for (let i = 0; i < Math.floor(Math.random() * 2); i++) {
                let rndTrxDtl = await walletHandler.creatARandomTrxWrapper('rndTrxDtl 1');
                if (rndTrxDtl.err != false)
                    return rndTrxDtl;
                console.log(`rndTrxDtl 1: ${utils.stringify(rndTrxDtl)}`);
                walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(rndTrxDtl.trx);
                await docBufferHandler.pushInAsync({ doc: rndTrxDtl.trx, DPCost: rndTrxDtl.DPCost });
                trxs.push([rndTrxDtl.trx, 'rnd1'])
            }
        }
        // push org transaction to Block buffer
        await docBufferHandler.pushInAsync({ doc: orgTrx, DPCost: args.DPCost });

        if (utils._nilEmptyFalse(testCase)) {
            // make some random trx
            for (let i = 0; i < Math.floor(Math.random() * 2); i++) {
                let rndTrxDtl = await walletHandler.creatARandomTrxWrapper('rndTrxDtl 2');
                if (rndTrxDtl.err != false)
                    return rndTrxDtl;
                console.log(`rndTrxDtl 2: ${utils.stringify(rndTrxDtl)}`);
                walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(rndTrxDtl.trx);
                await docBufferHandler.pushInAsync({ doc: rndTrxDtl.trx, DPCost: rndTrxDtl.DPCost });
                trxs.push([rndTrxDtl.trx, 'rnd2'])
            }
        }

        // push wrapper transaction to Block buffer
        await docBufferHandler.pushInAsync({ doc: p4pTrx, DPCost: trxDtl.DPCost });

        if (utils._nilEmptyFalse(testCase)) {
            // make some random trx
            for (let i = 0; i < Math.floor(Math.random() * 2); i++) {
                let rndTrxDtl = await walletHandler.creatARandomTrxWrapper('rndTrxDtl 3');
                if (rndTrxDtl.err != false)
                    return rndTrxDtl;
                console.log(`rndTrxDtl 3: ${utils.stringify(rndTrxDtl)}`);
                walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(rndTrxDtl.trx);
                await docBufferHandler.pushInAsync({ doc: rndTrxDtl.trx, DPCost: rndTrxDtl.DPCost });
                trxs.push([rndTrxDtl.trx, 'rnd3'])
            }
        }

        msg = `create a block included ${trxs.length} Trxs ${trxs.map(x => [x[1], x[0]].join(':')).join(' ')} `;
        clog.trx.info(msg);
        console.log(`msg ${msg}`);

        let p4pMBR2 = blockHandler.createANormalBlock({
            creationDate: utils.getNow()
        });
        if (p4pMBR2.err != false)
            console.log(`p4pMBR2 ${utils.stringify(p4pMBR2)}`);

        return {};

    }




}

module.exports = P4PdHandler