const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const clog = require('../../loggers/console_logger');
const kvHandler = require("../../models/kvalue");

async function getCheatingInfo(req) {
    clog.http.info('get: watcher/getCheatingInfo');
    let cheatingCreationDate = await kvHandler.getValueAsync('cheatingCreationDate');
    let cheatingAncestors = await kvHandler.getValueAsync('cheatingAncestors');
    if (!utils._nilEmptyFalse(cheatingCreationDate))
        console.log('cheatingCreationDate', cheatingCreationDate);
    if (Array.isArray(cheatingAncestors))
        cheatingAncestors = utils.parse(cheatingAncestors).map(x => utils.hash6c(x));
    return { cheatingCreationDate, cheatingAncestors };
}
module.exports = getCheatingInfo