const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const express = require('express');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const router = express.Router();
const DAGHandler = require("../../dag/graph-handler/dag-handler");
const crypto = require("../../crypto/crypto");
const WATCHING_BLOCKS_COUNT = 10;
const leavesHandler = require("../../dag/leaves-handler");

class wLib {
    static async loadHybridDags(blocksCount = WATCHING_BLOCKS_COUNT, order = null) {
        order = (utils._nilEmptyFalse(order)) ? [['b_creation_date', 'DESC']] : order;
        return await DAGHandler.searchInDAGAsync({
            fields: ['b_hash', 'b_confidence', 'b_cycle', 'b_ancestors', 'b_creation_date', 'b_receive_date', 'b_confirm_date', 'b_backer', 'b_body'],
            order,
            limit: blocksCount
        });
    }

    static async calcDagsSShot(cycleNumber = 200) {
        clog.app.info(`calcDagsSShot cycleNumber(${cycleNumber}) `);
        // let cDate = iConsts.getCycleByMinutes() * cycleNumber - 1
        // cDate = Moment(utils.getNow(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': cDate }).format("YYYY-MM-DD HH:mm:ss");
        // cDate = iutils.getCoinbaseRange(cDate).from
        let cDate = iConsts.getLaunchDate();

        let dummyList = []
        // finding leave block info
        let wBlocks = await DAGHandler.searchInDAGAsync({
            fields: ['b_hash', 'b_creation_date', 'b_type', 'b_utxo_imported'],
            query: [
                ['b_creation_date', ['>=', cDate]]
            ],
            order: [
                ['b_creation_date', 'ASC'],
                ['b_hash', 'ASC']
            ]
        });
        if (wBlocks.length == 0)
            return { DAGShoot: '#000', histBlocks: [] };

        // going back in hirarchy for this leave
        for (let wBlock of wBlocks) {
            dummyList.push([wBlock.bHash, wBlock.bCreationDate, wBlock.bType, wBlock.bUtxoImported])
        }

        let DAGShoot = [] // = '#' + utils.hash6c(crypto.keccak256(dummyList.map(x=>x.join()).join()));
        let histBlocks = []
        for (let b of dummyList) {
            DAGShoot.push(b[0])
            histBlocks.push({
                hash: '#' + utils.hash6c(b[0]),
                date: b[1],
                bType: b[2],
                imported: b[3],
            });
        }
        DAGShoot = '#' + utils.hash6c(crypto.keccak256(DAGShoot.join()));
        return { DAGShoot, histBlocks };

    }


    static async calcDagsSShotOld(hirarchyLevel = 17) {
        // return '23421';
        try {
            let dummyList = []
            let leaves = leavesHandler.getLeaveBlocks();
            let dadBlocks = [];
            leaves = utils.objKeys(leaves).sort();
            // console.log(leaves);
            for (let aLeave of leaves) {
                // leaves.forEach(async (aLeave) => {
                dummyList.push(aLeave);
                // finding leave block info
                let child = await DAGHandler.searchInDAGAsync({
                    fields: ['b_hash', 'b_ancestors'],
                    query: [
                        ['b_hash', aLeave]
                    ],
                    order: [
                        ['b_creation_date', 'DESC']
                    ]
                });

                // going back in hirarchy for this leave
                for (let i = 0; i < hirarchyLevel; i++) {
                    dadBlocks = [];
                    child.forEach(async (aChild) => {
                        dummyList.push(aChild.bHash)
                        let dads = await DAGHandler.searchInDAGAsync({
                            query: [
                                ['b_hash', ['in', utils.parse(aChild.bAncestors)]]
                            ],
                            fields: ['b_hash', 'b_ancestors'],
                            order: [
                                ['b_hash', 'asc']
                            ]
                        });
                        dads.forEach(dad => {
                            dummyList.push(dad.bHash)
                            dadBlocks.push(dad.bHash)

                        });
                    });
                    child = dadBlocks;
                }
            }


            return '#' + utils.hash6c(crypto.keccak256(dummyList.join()));

        } catch (e) {

            clog.app.error(e)
        }
    }
}

module.exports = wLib
