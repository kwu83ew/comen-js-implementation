const express = require('express');
const router = express.Router();
const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const DNAHandler = require("../../dna/dna-handler");



async function wGetShares(cloneId) {
    let { sumShares, holdersByKey } = DNAHandler.getSharesInfo();
    let { backer, shares, percentage } = DNAHandler.getMachineShares();
    return { backer, sumShares };
}

async function wRaiseShares(shareInfo) {
    let newShares = Math.floor(shareInfo.sumShares / 100);
    // const initShareDate = Moment(iConsts.getLaunchDate(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': 2 * iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");
    let tNow = utils.getNow();
    let dnaDoc = DNAHandler.getDNATmpShareDoc(); // add initialise DNA to genesis block
    dnaDoc.title = 'Raising almost share 1%'
    dnaDoc.descriptions = 'Raising share 1%'
    dnaDoc.tags = 'Raising share 1%'
    dnaDoc.helpHours = Math.floor(newShares / 5);
    dnaDoc.helpLevel = 5;
    dnaDoc.shares = dnaDoc.helpHours * dnaDoc.helpLevel;
    dnaDoc.votesY = 10000;
    dnaDoc.votesN = 1;
    dnaDoc.votesA = 1;
    dnaDoc.creationDate = tNow;
    dnaDoc.approvalDate = tNow;
    dnaDoc.shareholder = shareInfo.backer;
    dnaDoc.hash = DNAHandler.hashDNARegDoc(dnaDoc);
    
    DNAHandler.insertAShare(dnaDoc);
    let msg = `Raising shares for ${iutils.shortBech(shareInfo.backer)} ${utils.microPAIToPAI(dnaDoc.shares)} = %1 `;
    clog.app.info(msg);
    console.log(`msg ${msg}`);

    return {};
}

module.exports.wRaiseShares = wRaiseShares;
module.exports.wGetShares = wGetShares;