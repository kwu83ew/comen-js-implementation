const _ = require('lodash');
const express = require('express');
const router = express.Router();
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const miscHandler = require('../../web-interface/misc/misc-handler');
const reservedHandler = require('../../dag/coinbase/reserved-coins-handler');

router.put("/reqForRelRes", async (req, res) => {
    console.log(`req ForRelRes: ${utils.stringify(req.body)}`);
    let r = await reservedHandler.makeReqForRelRes(req.body);
    console.log('req ForRelRes', r);
    res.send(r);
});

router.put("/getCoinsReleaseReq", async (req, res) => {
    let r = reservedHandler.getCoinsReleaseReq(req.body);
    if (r.err != false)
        clog.http.error(`get OnchainProposalsList res ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/voteReleaseReseve", async (req, res) => {
    console.log(`/voteReleaseReseve: ${utils.stringify(req.body)}`);
    let r = await reservedHandler.voteReleaseReseve(req.body);
    if (r.err != false)
        clog.http.error(`vote ReleaseReseve res ${utils.stringify(r)}`);
    res.send(r);
});


router.put("/registreIName", async (req, res) => {
    let r;
    if (req.body.mod == 'register') {
        // do register
        r = await miscHandler.registreIName(req.body);

    } else {
        // calc register cost
        r = await miscHandler.estimateINameRegReqCost(req.body);
        if (r.err == false) {
            r = {
                err: false,
                iNameRegCost: r.cost,
                iName: req.body.iName,
                msg: `iName register cost for "${req.body.iName}" almost is ${utils.microPAIToPAI(r.cost + 7000000)} PAIs`
            }
        }
    }
    r.mod = req.body.mod;
    console.log('registreIName2', r);
    res.send(r);
});




router.get("/", async (req, res) => {
});

module.exports = router;
