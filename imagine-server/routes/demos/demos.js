const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const demosHandler = require("../../web-interface/demos/demos-handler");


router.put("/buildANewAgora", async (req, res) => {
    let r = demosHandler.buildANewAgora(req.body);
    if (r.err != false)
        console.log(`build A New Agora res: `, r);
    res.send(r);
});

router.put("/getOnchainAgoras", async (req, res) => {
    let r = await demosHandler.getOnchainAgoras(req.body);
    if (r.err != false)
        console.log(`get onchain Agora res: `, r);
    res.send(r);
});

router.put("/retrieveAgorasInfo", async (req, res) => {
    let r = demosHandler.retrieveAgorasInfo(req.body);
    if (r.err != false)
        console.log(`retrieve Agoras Info res: `, r);
    res.send(r);
});

router.put("/sendNewPost", async (req, res) => {
    let r = demosHandler.sendNewPost(req.body);
    if (r.err != false)
        console.log(`retrieve send NewPost res: `, r);
    res.send(r);
});

router.put("/convertCPostToPLRBundle", async (req, res) => {
    let r = demosHandler.convertCPostToPLRBundle(req.body);
    if (r.err != false)
        console.log(`retrieve send NewPost res: `, r);
    res.send(r);
});



router.get("/", async (req, res) => { });

module.exports = router;