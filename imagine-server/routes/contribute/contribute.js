// const {User, validate} = require("../models/user");
const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const contributeHandler = require("../../web-interface/contribute/contribute-handler");


// router.put("/estimatePAIIncome", async (req, res) => {
//     let r = await contributeHandler.estimatePAIIncome(req.body);
//     res.send(JSON.stringify(r));
// });

router.put("/voteProposal", async (req, res) => {
    let r = await contributeHandler.voteProposal(req.body);
    res.send(r);
});

router.put("/reviewBallotAndComments", async (req, res) => {
    let r = await contributeHandler.reviewBallotAndComments(req.body);
    res.send(r);
});

router.put("/sendLoanRequest", async (req, res) => {
    let r = contributeHandler.sendLoanRequest(req.body);
    res.send(r);
});

router.put("/sendLReqToAgora", async (req, res) => {
    let r = contributeHandler.sendLReqToAgora(req.body);
    clog.app.info(`send LReq To Agora, RES: ${utils.stringify(r)}`);
    console.log(`send LReq To Agora, RES: ${utils.stringify(r)}`);

    if (r.err != false)
        console.log(`send LReq To Agora, RES: ${utils.stringify(r)}`);

    res.send(r);
});

router.put("/deleteDraftPledgeReq", async (req, res) => {
    let r = contributeHandler.deleteDraftPledgeReq(req.body);
    res.send(r);
});

router.put("/bindProposalLoanPledge", async (req, res) => {
    let r = contributeHandler.bindProposalLoanPledge(req.body);
    res.send(r);
});

router.put("/signAndPayProposalCosts", async (req, res) => {
    let r = await contributeHandler.signAndPayProposalCosts(req.body);
    if (r.err != false)
        console.log(r);
    res.send(r);
});

router.put("/fillLoanRequestForm", async (req, res) => {
    let r = contributeHandler.fillLoanRequestForm(req.body);
    // console.log(JSON.stringify(r));
    res.send(r);
});

router.put("/predictFutureIncomes", async (req, res) => {
    let r = await contributeHandler.predictFutureIncomes(req.body);
    // console.log(JSON.stringify(r));
    res.send(r);
});

router.put("/prepareDraftProposal", async (req, res) => {
    let r = await contributeHandler.prepareDraftProposal(req.body);
    res.send(JSON.stringify(r));
});

router.put("/deleteDraftProposal", async (req, res) => {
    let r = await contributeHandler.deleteDraftProposal(req.body);
    res.send({});
});


router.put("/loadMyDraftProposals", async (req, res) => {
    let r = await contributeHandler.loadMyDraftProposals(req.body);
    res.send(r);
});

router.put("/getOnchainProposalsList", async (req, res) => {
    let r = contributeHandler.getOnchainProposalsList(req.body);
    if (r.err != false)
        clog.http.error(`get OnchainProposalsList res ${utils.stringify(r)}`);
    res.send(r);
});




router.get("/", async (req, res) => {
});

module.exports = router;
