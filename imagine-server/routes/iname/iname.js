const _ = require('lodash');
const iConsts = require('../../config/constants');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const flensHandler = require('../../contracts/flens-contract/flens-handler');


router.put("/getUserINames", (req, res) => {
    let r;
    r = flensHandler.getMachineControlledINames();
    console.log(`get MachineControlledINames `, utils.stringify(r));
    res.send(r);
});

router.put("/createAndBindIPGP", async (req, res) => {
    let r;
    r = await flensHandler.binder.createAndBindIPGP(req.body);
    res.send(r);
});




router.get("/", async (req, res) => {
});

module.exports = router;