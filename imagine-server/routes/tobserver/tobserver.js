// const {User, validate} = require("../models/user");
const _ = require('lodash');
const iConsts = require('../../config/constants');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto')
const machine = require('../../machine/machine-handler');
const tobserver = require('../../web-interface/tobserver/tobserver-handler');



router.put("/getRejectedTrxsList", async (req, res) => {
    console.log('get RejectedTrxsList', req.body);
    let r = await tobserver.getRejectedTrxsList(req.body);
    res.send(r);
});

router.put("/getSusTrxsList", async (req, res) => {
    console.log('get SusTrxsList', req.body);
    let r = await tobserver.getSusTrxsList(req.body);
    res.send(r);
});

router.put("/traceCoinInfo", async (req, res) => {
    console.log('trace Coin Info', req.body);
    let r = await tobserver.traceCoinInfo(req.body);
    res.send(r);
});


router.get("/", async (req, res) => {
});

module.exports = router;
