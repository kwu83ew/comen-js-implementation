// const {User, validate} = require("../models/user");
const _ = require('lodash');
const iConsts = require('../../config/constants');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto')
const machine = require('../../machine/machine-handler');
const msngrHandler = require('../../web-interface/messenger/messenger-handler');
const directmsgHandler = require('../../messaging-protocol/direct-msg-handler');



router.put("/loadMessages", async (req, res) => {
    console.log('loadMessage', req.body);
    let r = await msngrHandler.loadTextMessages(req.body);
    res.send(r);
    console.log('------------------------------------');
    console.log('------------------------------------');
    console.log('------------------------------------');
    console.log('------------------------------------');
    console.log('------------------------------------');
    console.log('------------------------------------');
    console.log('loadMessage', r);
    console.log('------------------------------------');
});

router.put("/listMyIPGPBindings", async (req, res) => {
    let r = msngrHandler.listMyIPGPBindings(req.body);
    let newR = [];
    for (let aRes of r) {
        newR.push({
            bindedIName: aRes.in_name,
            bindedTitle: aRes.mcb_label,
            bindedDummyKey: utils.hash6c(iutils.doHashObject(aRes))
        });
    }
    res.send(newR);
});


router.put("/sendPlainTextMessage", async (req, res) => {
    let r = await msngrHandler.sendPlainTextMessage(req.body);
    res.send(r);
});


router.put("/listMyNeighbors", async (req, res) => {
    let r = await machine.neighborHandler.getActiveNeighborsAsync();
    console.log(`list My Neighbors`, r);
    res.send(r);
});

router.put("/sendDirectMsgToNeighbor", async (req, res) => {
    console.log(`send Direct Msg To Neighbor`, req.body);
    let pushRes = directmsgHandler.sendAMsg({
        receiver: req.body.selectedNeighbor,
        directMsgBody: req.body.directMsgBody,
    });
    res.send(pushRes);
});

router.put("/getMyDirectMsgs", async (req, res) => {
    let r = await directmsgHandler.getMyDirectMsgs();
    res.send(r);
});

router.put("/deleteDirectMsg", async (req, res) => {
    let r = await directmsgHandler.deleteDirectMsg(req.body);
    res.send(r);
});

router.put("/refreshMyDirectMsgs", async (req, res) => {
    let r = await directmsgHandler.refreshMyDirectMsgs(req.body);
    res.send(r);
});



router.get("/", async (req, res) => {
});

module.exports = router;