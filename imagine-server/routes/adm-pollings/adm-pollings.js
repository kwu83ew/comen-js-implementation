const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const admPollingsHandler = require('../../web-interface/adm-pollings/adm-pollings-handler');


router.put("/loadAdmPollings", async (req, res) => {
    let r = admPollingsHandler.loadAdmPollings(req.body);
    if (r.err != false)
        console.log(`load adm Pollings res: `, r);
    res.send(r);
});

router.put("/createAPollingFor", async (req, res) => {
    let r = await admPollingsHandler.createAPollingFor(req.body);
    if (r.err != false)
        console.log(`create A Polling For res: `, r);
    res.send(r);
});

router.put("/getOnchainAdmPollings", async (req, res) => {
    let r = await admPollingsHandler.onChain.getOnchainAdmPollings(req.body);
    if (r.err != false)
        console.log(`get Onchain Adm Pollings res: `, r);
    res.send(r);
});

router.put("/voteAdmPolling", async (req, res) => {
    let r = await admPollingsHandler.voteAdmPolling(req.body);
    if (r.err != false)
        console.log(`get Onchain Adm Pollings res: `, r);
    res.send(r);
});

router.get("/", async (req, res) => { });

module.exports = router;
