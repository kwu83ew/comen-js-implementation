const _ = require('lodash');
const express = require('express');
const router = express.Router();
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const wikiHandler = require('../../web-interface/wiki/wiki-handler');


router.put("/createNewWikiPage", async (req, res) => {
    let r = wikiHandler.createNewWikiPage(req.body);
    if (r.err != false)
        console.log(`build A New wiki page res: `, r);
    res.send(r);
});

router.put("/editAWikiPage", async (req, res) => {
    let r = wikiHandler.createNewWikiPage(req.body);
    if (r.err != false)
        console.log(`edit a wiki page res: `, r);
    res.send(r);
});

router.put("/getOnchainWkPages", async (req, res) => {
    let r = await wikiHandler.getOnchainWkPages(req.body);
    if (r.err != false)
        console.log(`get onchain wikis res: `, r);
    res.send(r);
});

router.put("/getWkPageInfo", async (req, res) => {
    let r = await wikiHandler.getWkPageInfo(req.body);
    if (r.err != false)
        console.log(`get Wk Page Info res: `, r);
    res.send(r);
});


router.get("/", async (req, res) => { });

module.exports = router;
