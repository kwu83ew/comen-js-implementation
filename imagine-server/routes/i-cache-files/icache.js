
const _ = require('lodash');
const iConsts = require('../../config/constants');
const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');

var mimes = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};

router.get('*', function (req, res) {
    let file = path.join(iConsts.HD_PATHES.I_CACHE_FILES, req.path);
    if (file.indexOf(iConsts.HD_PATHES.I_CACHE_FILES + path.sep) !== 0) {
        return res.status(403).end('Forbidden');
    }
    let type = mimes[path.extname(file).slice(1)] || 'text/plain';
    let s = fs.createReadStream(file);
    s.on('open', function () {
        res.set('Content-Type', type);
        s.pipe(res);
    });
    s.on('error', function () {
        res.set('Content-Type', 'text/plain');
        res.status(404).end('Not found');
    });
});

module.exports = router;