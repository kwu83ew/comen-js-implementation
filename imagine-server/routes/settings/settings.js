const _ = require('lodash');
const express = require('express');
const router = express.Router();
const clog = require('../../loggers/console_logger');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const machine = require('../../machine/machine-handler');
const settingsHandler = require('../../web-interface/settings/settings-handler');
const crypto = require('../../crypto/crypto')
const kvHandler = require('../../models/kvalue');

router.put("/loadMachineNeighbors", async (req, res) => {
    clog.http.info('put: settings/loadMachineNeighbors', req.body);
    let r = await settingsHandler.loadMachineNeighbors(req.body); // the response by itself is stringifyed
    // console.log(`loadMachineNeighbors: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/loadMachineProfile", (req, res) => {
    clog.http.info('put: settings/loadMachineProfile', req.body);
    let r = settingsHandler.loadMachineProfile(req.body); // the response by itself is stringifyed
    console.log(`load Machine Profile: ${utils.stringify(r)}`);
    // convert iPGP Public keys to base64 for presentation & transfer manually
    r.selectedMP.pubEmail.PGPPubKey = crypto.b64Encode(r.selectedMP.pubEmail.PGPPubKey);
    r.selectedMP.prvEmail.PGPPubKey = crypto.b64Encode(r.selectedMP.prvEmail.PGPPubKey);
    res.send(r);
});

router.put("/loadMachineProfileG", (req, res) => {
    clog.http.info('put: settings/loadMachineProfileG', req.body);
    let selectedMPCode = settingsHandler.getActiveMP().activeMP;
    let r = settingsHandler.loadMachineProfile({ selectedMPCode }); // the response by itself is stringifyed

    // console.log(`load Machine ProfileG: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/getActiveMP", (req, res) => {
    clog.http.info('put: settings/getActiveMP');
    let r = settingsHandler.getActiveMP(req.body); // the response by itself is stringifyed
    res.send(r);
});
router.put("/setActiveMP", (req, res) => {
    clog.http.info('put: settings/setActiveMP');
    let r = settingsHandler.setActiveMP(req.body); // the response by itself is stringifyed
    res.send(r);
});

router.put("/getMProfilesList", async (req, res) => {
    clog.http.info('put: settings/getMProfilesList');
    let r = await settingsHandler.getMProfilesList(req.body); // the response by itself is stringifyed
    // console.log(`getMProfilesList: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/addNewMProfile", async (req, res) => {
    clog.http.info('put: settings/addNewMProfile');
    let r = await settingsHandler.addNewMProfile(req.body); // the response by itself is stringifyed
    res.send(r);
});

router.put("/saveEmailsSettings", async (req, res) => {
    clog.http.info('put: settings/saveEmailsSettings');
    let r = await settingsHandler.saveEmailsSettings(req.body); // the response by itself is stringifyed
    res.send(r);
});

router.put("/testEmailServerIncomeConnection", async (req, res) => {
    clog.http.info('put: settings/testEmailServerIncomeConnection');
    let r = await settingsHandler.testEmailServerIncomeConnection(req.body); // the response by itself is stringifyed
    console.log(`test EmailServerIncomeConnection res`, r);
    res.send(r);
});
router.put("/testEmailServerOutgoingConnection", async (req, res) => {
    clog.http.info('put: settings/testEmailServerOutgoingConnection');
    let r = await settingsHandler.testEmailServerOutgoingConnection(req.body); // the response by itself is stringifyed
    console.log(`test EmailServerOutgoingConnection res`, r);
    res.send(r);
});

router.put("/addNeighbor", (req, res) => {
    console.log(`put: settings/addNeighbor: ${utils.stringify(req.body)}`);
    clog.http.info(`put: settings/addNeighbor: ${utils.stringify(req.body)}`);
    let args = {
        nConnectionType: req.body.nConnectionType,
        nEmail: req.body.nEmail.trim(),
        nPGPPubKey: req.body.nPubKey
    }
    let r = settingsHandler.addNeighborS(args); // the response by itself is stringifyed
    res.send(r);
});

router.put("/cutNeighbor", (req, res) => {
    clog.http.info('put: settings/cutNeighbor');
    let r = settingsHandler.cutNeighbor(req.body); // the response by itself is stringifyed
    res.send(r);
});

router.put("/handshakeNeighbor", async (req, res) => {
    clog.http.info('put: settings/handshakeNeighbor', req.body);
    let r = await settingsHandler.handshakeNeighbor(req.body); // the response by itself is stringifyed
    if (r.err != false)
        clog.app.error(`handshake Neighbor res: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/presentEmailToNeighbors", async (req, res) => {
    console.log('put: settings/flood Email To Neighbors', req.body);
    clog.http.info('put: settings/flood Email To Neighbors', req.body);
    let r = settingsHandler.presentEmailToNeighbors(req.body); // the response by itself is stringifyed
    if (r.err != false)
        clog.app.error(`present Email To Neighbor res: ${utils.stringify(r)}`);
    res.send(r);
});

router.put("/getLanguagesList", async (req, res) => {
    clog.http.info('put: settings/getLanguagesList');
    let r = settingsHandler.getLanguagesList(); // the response by itself is stringifyed
    res.send(r);
});

router.put("/deleteMProfile", async (req, res) => {
    clog.http.info('put: settings/deleteMProfile');

    // first change selected profile(if it is the delete candidate profile)
    letselected_profile = await kvHandler.getValueAsync('selected_profile');
    let mpCode = req.body.mpCode;
    if (letselected_profile == mpCode) {
        let allProfiles = await settingsHandler.getMProfilesList();
        allProfiles = allProfiles.map(x => x.mpCode);
        allProfiles = utils.arrayDiff(allProfiles, [mpCode]).sort();
        await kvHandler.setAsync('selected_profile', allProfiles[0]);
    }
    
    let r = settingsHandler.deleteMProfile(req.body);
    res.send(r);
});

router.put("/backupMaWa", async (req, res) => {
    let r = await machine.backupHandler.backupMaWa();
    if (r.err != false)
        console.log(`\n\nbackupMaWa ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/controllMaWa", async (req, res) => {
    let r = await machine.backupHandler.controllMaWa();
    if (r.err != false)
        console.log(`\n\ncontrollMaWa ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

router.put("/restoreMaWa", async (req, res) => {
    let r = await machine.backupHandler.restoreMaWa();
    if (r.err != false)
        console.log(`\n\nrestoreMaWa ${utils.stringify(r)}`);
    res.send(JSON.stringify(r));
});

module.exports = router;
