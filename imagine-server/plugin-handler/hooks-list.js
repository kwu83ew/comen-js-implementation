module.exports = {
    'APSH_before_push_doc_to_buffer_async': { hType: 'APSH' }, // Async Passive Server Hook
    'APSH_after_push_doc_to_buffer_async': { hType: 'APSH' },
    'APSH_create_coinbase_block': { hType: 'APSH' },
    'APSH_control_if_missed_block': { hType: 'APSH' },
    'APSH_before_handle_pulled_packet': { hType: 'APSH' },


    'SPSH_failed_on_validate_NBlock': { hType: 'SPSH' },// Synchronous Passive Server Hook
    'SPSH_failed_on_validate_FSBlock': { hType: 'SPSH' },
    'SPSH_block_has_double_spend_input': { hType: 'SPSH' },
    
    'SASH_before_validate_normal_block': { hType: 'SASH' },   // Synchronous Active Server Hook
    'SASH_calc_service_price': { hType: 'SPSH' },
    'SASH_validate_normal_block': { hType: 'SASH' },   // Synchronous Active Server Hook
    'SPSH_after_parse_packet': { hType: 'SASH' },
    'SASH_signals': { hType: 'SASH' },
}
