const _ = require('lodash');
const utils = require("../utils/utils");
const iutils = require("../utils/iutils");
const iConsts = require('../config/constants');
const model = require("../models/interface");
const validHooksList = require("./hooks-list");
const validHooksName = utils.objKeys(validHooksList);
const pluginsList = require("../plugins/plugins-list");
const clog = require('../loggers/console_logger');


let plugins = null;
let registeredHooks = null;


class PluginHanlder {

    static loadPlugins() {
        if (registeredHooks == null) {
            registeredHooks = {};
            for (let plugin of utils.objKeys(pluginsList)) {
                if (pluginsList[plugin].status == 'enabled') {
                    pluginsList[plugin].pluginCode = plugin;
                    let pluginPath = `../plugins/${pluginsList[plugin].path}/index`;
                    pluginsList[plugin].pluginPath = pluginPath;
                    console.log(`load plugin: ${plugin}(${pluginPath})  `);
                    let plg = require(pluginPath);
                    let hooks = plg.getHooks();
                    // console.log('hooks', utils.getAllMethodNames(hooks));
                    for (let aHook of utils.getAllMethodNames(hooks)) {
                        if (validHooksName.includes(aHook)) {
                            console.log('registering Hook: ', aHook);
                            if (!_.has(registeredHooks, aHook))
                                registeredHooks[aHook] = [];
                            registeredHooks[aHook].push(pluginsList[plugin]);
                        }
                    }
                    // console.log(`${plugin} hooks: `, hooks);
                }
            }
            console.log('registeredHooks', registeredHooks);
        }
    }

    static validateCustomDocs(block) {
        let msg = `Custom plugin confirmed block(${utils.hash6c(block.blockHash)})`;
        clog.app.info(msg);
        return { err: false, msg, shouldPurgeMessage: true };
    }

    static doCallAsync(func, args) {
    }

    static doCallSync(func, args) {
        if (plugins == null)
            plugins = this.loadPlugins();

        // console.log('func', func);
        let callbacksRes;
        if (_.has(registeredHooks, func)) {
            if (validHooksName.includes(func)) {
                callbacksRes = _.clone(args);
                callbacksRes.imApp = this.getApp();
                for (let aCallBack of registeredHooks[func]) {
                    try {
                        if (['SASH', 'SPSH'].includes(validHooksList[func].hType)) {
                            // calling just synchronuos callbacks
                            // console.log(`calling plugin ${aCallBack.pluginCode}.${func}`);
                            // console.log(`calling plugin ${aCallBack.pluginCode}.${func} args:${utils.stringify(callbacksRes)}`);
                            let plg = require(aCallBack.pluginPath);
                            callbacksRes = plg.getHooks()[func](callbacksRes);
                            // console.log(`calling plugin res:${utils.stringify(callbacksRes)}`);
                        }
                    } catch (e) {
                        console.log('Hook calling error', e);
                    }
                }
            }
        }
        return callbacksRes;
    }

    static getApp() {
        return {
            kvHandler: require("../models/kvalue"),
        }
    }
}



module.exports = PluginHanlder;