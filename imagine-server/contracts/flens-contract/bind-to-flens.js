const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const crypto = require('../../crypto/crypto')
const machine = require('../../machine/machine-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const blockHasher = require('../../dag/hashable');
const fVHandler = require('../../dag/floating-vote/floating-vote-handler');
const collisionsHandler = require('../../services/collisions/collisions-manager');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');



const tableBindings = 'i_iname_bindings';
const tableMachineBindings = 'i_machine_iname_bindings';

let bindToINameReqTpl = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dLen: "0000000",
    dType: iConsts.DOC_TYPES.INameBind,
    dClass: iConsts.GENERAL_CLASESS.Basic,
    dVer: "0.0.0",
    nbTitle: "", // e.g. use PUB for work
    nbComment: "", // e.g. use this key only for bussiness/friendly chat...
    nbType: "",
    nbInfo: "",
    dExtInfo: {
        signatures: [],
        uSet: {}
    },
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000",
    creationDate: ""
};


/**
 * the owner of iName can bind public PGP key to iName in order to receive encrypted messages
 */
class FleNSBinder {

    static async createAndBindIPGP(args = {}) {
        let msg;
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;
        let isHotIName = _.has(args, 'isHotIName') ? args.isHotIName : false;   // the Hot IName is the iName which is currently created by user and atill is not setteled

        const walletHandler = require('../../web-interface/wallet/wallet-handler');
        clog.app.info(`create And BindIPGP args: ${utils.stringify(args)}`);
        let nbINameHash = args.nbINameHash;
        let iPLabel = args.iPLabel;
        let iPComment = args.iPComment;

        // retrieve proper iName
        let iPNOwner;
        if (isHotIName) {
            iPNOwner = args.hotInameOwner;
        } else {
            let iNameInfo = FleNSBinder._super.getMachineControlledINames({ query: [['imn_hash', nbINameHash]] });
            if (iNameInfo.records.length != 1)
                return { err: true, msg: `the iNameHash(${utils.hash6c(nbINameHash)}) does not exist or not controlled by your Wallet/Profile` }
            iNameInfo = iNameInfo.records[0];
            iPNOwner = iNameInfo.imn_owner;
        }

        // prepare iPGP pair key
        let nbInfo = {
            iPNHash: nbINameHash,
            iPNOwner,
            iPLabel,
            iPComment
        };
        let PGPpairKey = crypto.nativeGenerateKeyPairSync();
        nbInfo['iPVersion'] = '0.0.0';
        nbInfo['iPPrvKey'] = PGPpairKey.prvKey;
        nbInfo['iPPubKey'] = PGPpairKey.pubKey;
        // console.log(`nbInfo: ${utils.stringify(nbInfo)}`);


        // TODO: implement a draft table and mechanisem to avoid misconception of accepted bindings
        // for now directy insert to machine name_bindings table
        let cRes = FleNSBinder.createABinding({
            mcb_in_hash: nbINameHash,
            mcb_bind_type: iConsts.BINDING_TYPES.IPGP,
            mcb_conf_info: utils.stringify(nbInfo),
            mcb_label: iPLabel,
            mcb_comment: iPComment,
            mcb_creation_date: utils.getNow()
        });
        if (cRes.err != false)
            return cRes;

        // prepare request documetn
        delete nbInfo['iPPrvKey'];
        let prepareRes = FleNSBinder.prepareReqToBinding({
            nbInfo,
            nbType: iConsts.BINDING_TYPES.IPGP,

        });
        if (prepareRes.err != false)
            return prepareRes;
        clog.app.info(`bind Request Doc: ${utils.stringify(prepareRes)}`);

        let bReqDoc = prepareRes.bReqDoc;
        // prepare payment doc too
        // calculate bind cost
        let bindCost = this.calcINameBindCost({
            cDate: utils.getNow(),
            bReqDoc,
            stage: iConsts.STAGES.Creating
        });
        console.log(`bindCost Cost ${utils.stringify(bindCost)}`);
        if (bindCost.err != false)
            return bindCost;

        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_INAME_BIND', bindCost.cost]
        ];

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: bindCost.cost * 2
        });
        if (spendables.err != false)
            return spendables;

        // return { err: false, msg: 'binding doc createsd' }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(bindCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: bReqDoc.hash,
            description: 'Pay for request for Bind an entity to registered an iName',
            inputs: inputs,
            outputs: outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed iName-bind costs trx: ', utils.stringify(trxDtl));


        // push trx & bReqDoc Req to Block buffer
        let res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = await docBufferHandler.pushInAsync({ doc: bReqDoc, DPCost: bindCost.cost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your Binding request is pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;


    }

    static calcINameBindCost(args) {
        console.log(`calc IName Bind Cost args ${utils.stringify(args)}`);
        clog.app.info(`calc IName Bind Cost args ${utils.stringify(args)}`);
        let bReqDoc = args.bReqDoc;

        let dLen = parseInt(bReqDoc.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate: args.cDate }) *
            cnfHandler.getDocExpense({ cDate: args.cDate, dType: bReqDoc.dType, dClass: bReqDoc.dClass, dLen });



        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: bReqDoc.dType,
                dClass: bReqDoc.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static prepareBindingsDict(args) {
        let query = _.has(args, 'query') ? args.query : [];
        let needpgpKey = _.has(args, 'needpgpKey') ? args.needpgpKey : false;

        let bindings = model.sRead({
            table: tableBindings,
            query
        });
        if (bindings.length == 0)
            return { err: false, bindingDict: {} };

        let bindingDict = {};
        for (let aBind of bindings) {
            let bnd = FleNSBinder.convertOnchainBindings(aBind);
            bnd.nbConfInfo = utils.parse(bnd.nbConfInfo);

            // lightening response
            if (bnd.nbBindType == iConsts.BINDING_TYPES.IPGP) {
                bnd.iPGPHandle = iutils.doHashObject([bnd.nbConfInfo.iPLabel, bnd.nbCreationDate]);
                if (!needpgpKey) {
                    delete (bnd.nbConfInfo.iPPubKey);
                }
            }

            if (!_.has(bindingDict, bnd.nbinHash))
                bindingDict[bnd.nbinHash] = {};

            if (!_.has(bindingDict[bnd.nbinHash], bnd.nbBindType))
                bindingDict[bnd.nbinHash][bnd.nbBindType] = [];
            bindingDict[bnd.nbinHash][bnd.nbBindType].push(bnd);

        }
        return { err: false, bindingDict }
    }

    static convertMachineBindings(elm) {
        let out = {};
        if (_.has(elm, 'mcb_mp_code'))
            out.mcbmpCode = elm.mcb_mp_code;
        if (_.has(elm, 'mcb_in_hash'))
            out.mcbinHash = elm.mcb_in_hash;
        if (_.has(elm, 'mcb_bind_type'))
            out.mcbBindType = elm.mcb_bind_type;
        if (_.has(elm, 'mcb_conf_info'))
            out.mcbConfInfo = elm.mcb_conf_info;
        if (_.has(elm, 'mcb_label'))
            out.mcbLabel = elm.mcb_label;
        if (_.has(elm, 'mcb_comment'))
            out.mcbComment = elm.mcb_comment;
        if (_.has(elm, 'mcb_status'))
            out.mcbStatus = elm.mcb_status;
        if (_.has(elm, 'mcb_creation_date'))
            out.mcbCreationDate = elm.mcb_creation_date;
        return out;
    }

    static convertOnchainBindings(elm) {
        let out = {};
        if (_.has(elm, 'nb_doc_hash'))
            out.nbDocHash = elm.nb_doc_hash;
        if (_.has(elm, 'nb_in_hash'))
            out.nbinHash = elm.nb_in_hash;
        if (_.has(elm, 'nb_bind_type'))
            out.nbBindType = elm.nb_bind_type;
        if (_.has(elm, 'nb_conf_info'))
            out.nbConfInfo = elm.nb_conf_info;
        if (_.has(elm, 'nb_title'))
            out.nbTitle = elm.nb_title;
        if (_.has(elm, 'nb_comment'))
            out.nbComment = elm.nb_comment;
        if (_.has(elm, 'nb_status'))
            out.nbStatus = elm.nb_status;
        if (_.has(elm, 'nb_creation_date'))
            out.nbCreationDate = elm.nb_creation_date;
        return out;
    }


    /**
     * 
     * @param {*} args 
     * records a newly created binding in local machine user for current profile
     */
    static createABinding(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let dbl = model.sRead({
            table: tableMachineBindings,
            query: [
                ['mcb_mp_code', mpCode],
                ['mcb_in_hash', args.mcb_in_hash],
                ['mcb_bind_type', args.mcb_bind_type],
                ['mcb_label', args.mcb_label],
            ]
        });
        if (dbl.length > 0)
            return { err: true, msg: `the binding label(${args.mcb_label}) for(${utils.hash6c(args.mcb_in_hash)}) is already exist` };

        let values = {
            mcb_mp_code: mpCode,
            mcb_in_hash: args.mcb_in_hash,
            mcb_bind_type: args.mcb_bind_type,
            mcb_conf_info: args.mcb_conf_info,
            mcb_comment: args.mcb_comment,
            mcb_label: args.mcb_label,
            mcb_creation_date: args.mcb_creation_date,
        };
        clog.app.info(`create A Binding ${utils.stringify(values)}`);

        model.sCreate({
            table: tableMachineBindings,
            values
        });

        return { err: false }
    }

    // TODO: implement it
    static revokeBinding(args = {}) {

    }


    /**
     * 
     * @param {*} args 
     * records an onchain binded entity
     */
    static recordAbindDoc(args) {
        let bindDoc = args.bindDoc;

        let dbl = model.sRead({
            table: tableBindings,
            query: [
                ['nb_doc_hash', bindDoc.hash],
                ['nb_in_hash', bindDoc.nbInfo.iPNHash],
            ]
        });
        if (dbl.length > 0)
            return { err: false, msg: `the binding doc(${utils.hash6c(bindDoc.hash)} for(${utils.hash6c(bindDoc.nbInfo.iPNHash)}) already recorded in DAG` };

        let values = {
            nb_doc_hash: bindDoc.hash,
            nb_in_hash: bindDoc.nbInfo.iPNHash,
            nb_bind_type: bindDoc.nbType,
            nb_conf_info: utils.stringify(bindDoc.nbInfo),
            nb_title: bindDoc.nbTitle,
            nb_comment: bindDoc.nbComment,
            nb_status: iConsts.CONSTS.VALID,
            nb_creation_date: bindDoc.creationDate,
        };
        clog.app.info(`create A Binding ${utils.stringify(values)}`);


        dbl = model.sRead({
            table: tableBindings,
            query: [
                ['nb_title', bindDoc.nbTitle],
                ['nb_in_hash', bindDoc.nbInfo.iPNHash],
            ]
        });
        if (dbl.length > 0) {
            model.sUpdate({
                table: tableBindings,
                query: [
                    ['nb_title', bindDoc.nbTitle],
                    ['nb_in_hash', bindDoc.nbInfo.iPNHash],
                ],
                updates: values
            });
        } else {
            model.sCreate({
                table: tableBindings,
                values
            });

        }

        return { err: false }
    }

    static prepareReqToBinding(args) {
        clog.app.info(`prepare ReqToBinding args: ${utils.stringify(args)}`);

        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();

        let bReqDoc = _.clone(bindToINameReqTpl);
        bReqDoc.creationDate = creationDate;
        bReqDoc.nbType = args.nbType;
        bReqDoc.nbInfo = args.nbInfo;
        bReqDoc.nbTitle = args.nbInfo.iPLabel;
        bReqDoc.nbComment = args.nbInfo.iPComment;

        // do sign doc by proper key
        let signMsg = FleNSBinder.getSignMsgDBindReq(bReqDoc);
        let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: args.nbInfo.iPNOwner });
        if (signatureInfo.err != false)
            return signatureInfo;

        bReqDoc.dExtInfo = {
            signatures: signatureInfo.signatures,
            uSet: signatureInfo.uSet
        }
        bReqDoc.dExtHash = iutils.doHashObject({
            signatures: bReqDoc.dExtInfo.signatures,
            uSet: bReqDoc.dExtInfo.uSet
        });

        bReqDoc.dLen = iutils.paddingDocLength(utils.stringify(bReqDoc).length);

        bReqDoc.hash = FleNSBinder.calcHashBBindReqDoc(bReqDoc);
        return { err: false, bReqDoc };
    }

    static getSignMsgDBindReq(bReqDoc) {
        // as always ordering properties by alphabet
        let signables = {
            nbInfo: bReqDoc.nbInfo,
            nbType: bReqDoc.nbType,
            creationDate: bReqDoc.creationDate,
            dClass: bReqDoc.dClass,
            nbComment: bReqDoc.nbComment,
            nbTitle: bReqDoc.nbTitle,
            dType: bReqDoc.dType,
            dVer: bReqDoc.dVer,
        }
        clog.app.info(`prepare ReqToBinding signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static calcHashBBindReqDoc(bReqDoc) {
        // as always ordering properties by alphabet
        let hashables = {
            dExtHash: bReqDoc.dExtHash,
            dLen: bReqDoc.dLen
        }
        clog.app.info(`calcHashBBindReqDoc hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static removeBind(bindDocHash) {
        model.sDelete({
            table: tableBindings,
            query: [['nb_doc_hash', bindDocHash]]
        });
    }

    static listMyIPGPBindings(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = `
            SELECT inm.in_name, mb.mcb_label FROM i_machine_iname_bindings mb 
            JOIN i_iname_records inm ON inm.in_hash=mb.mcb_in_hash 
            WHERE mb.mcb_mp_code=$1 AND mb.mcb_bind_type=$2
            ORDER BY mcb_label, mcb_creation_date`;
        let res = model.sCustom({ query, values: [mpCode, iConsts.BINDING_TYPES.IPGP] });
        return res;
    }

    static getBindInfoByINameAndBindTitle(args) {
        let res = model.sRead({
            table: tableBindings,
            query: [
                ['nb_bind_type', iConsts.BINDING_TYPES.IPGP],
                ['nb_title', args.nbTitle],
                ['nb_in_hash', args.nbinHash],
            ]
        });
        res = res.map(x => FleNSBinder.convertOnchainBindings(x));
        return res;
    }

    static getMachineIPGPBindInfoByINameAndBindTitle(args) {
        let res = model.sRead({
            table: tableMachineBindings,
            query: [
                ['mcb_bind_type', iConsts.BINDING_TYPES.IPGP],
                ['mcb_label', args.mcbLabel],
                ['mcb_in_hash', args.mcbinHash],
            ]
        });
        res = res.map(x => FleNSBinder.convertMachineBindings(x));
        return res;
    }

    /**
     * 
     * @param {*} args 
     * actually can not realy cut the pgp key registered on0chain. but at leaset the future senders will be aware of revoked keys and their app will not send new messages
     */
    static revokePGPKeys(args) {
        // TODO: implement it

    }

}

module.exports = FleNSBinder;
