const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const crypto = require('../../crypto/crypto');
const machine = require('../../machine/machine-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const fVHandler = require('../../dag/floating-vote/floating-vote-handler');
const collisionsHandler = require('../../services/collisions/collisions-manager');

// flexible & extensible name service
// iNames are imagine accoun name which has functionality of all all username, nickname, domain-name, aplication-account, email-address, website-address concepts in one place
// iNames also can be used like bank account to receive payments

const tableINames = 'i_iname_records';
const tableMachineINames = 'i_machine_iname_records';


class FleNSHandler {

    /**
     * retrueve the pages which are created under tha iName controlled by mahine wallet.
     * TODO: not necssarily all keys of a domain are living on backer machine.
     * what about mobile?
     * the keys & registered iNames must be kept safe in user wallet
     * 
     */
    static getMyPages() {
        // retrieve user registerd iNames and proper information
        let machineDomains = FleNSHandler.getMachineControlledINames();
        let domainsDict = {};
        for (let aName of machineDomains) {
            domainsDict[aName.in_hash] = aName;
        }

        // retrieve pages
        let myPages = model.sRead({
            table,
            query: [
                ['wl_in_hash', ['IN', utils.objKeys(domainsDict)]]
            ],
            order: [
                ['wl_url', 'ASC']
            ]
        });
        return myPages;
    }

    static generateINameHash(iName) {
        let hash = crypto.keccak256(FleNSHandler.register.normalizeIName(iName));
        return hash;
    }

    static searchForINamesControlledByWallet(args = {}) {

        let wArgs = {};
        // if (utils.queryHas(query, 'mpCode'))
        if (_.has(args, 'mpCode'))
            wArgs.mpCode = args.mpCode;
        let walletAddresses = walletAddressHandler.getAddressesListSync(wArgs);
        let searchAdds = [];
        let mapAddressToProfile = {};
        for (let anAdd of walletAddresses) {
            searchAdds.push(anAdd.waAddress);
            mapAddressToProfile[anAdd.waAddress] = anAdd.wampCode;
        }

        let sQuery = [];
        sQuery.push(['in_owner', ['IN', searchAdds]]);
        let res = FleNSHandler.register.searchRegisteredINames({
            needBindingsInfoToo: _.has(args, 'needBindingsInfoToo') ? args.needBindingsInfoToo : false,
            query: sQuery,
        });
        // console.log('mapAddressToProfile mapAddressToProfile mapAddressToProfile', mapAddressToProfile);
        res.mapAddressToProfile = mapAddressToProfile;
        return res;
    }

    static getMachineControlledINames(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        let needBindingsInfoToo = _.has(args, 'needBindingsInfoToo') ? args.needBindingsInfoToo : false;
        let needpgpKey = _.has(args, 'needpgpKey') ? args.needpgpKey : false;

        if (mpCode != iConsts.CONSTS.ALL)
            query.push(['imn_mp_code', mpCode]);
        let records = model.sRead({
            table: tableMachineINames,
            query,
            order: [['imn_register_date', 'ASC']]
        });
        if (records.length == 0)
            return { err: false, msg: `There is no iName-Addresses controlled by this machine`, records: [] };

        if (!needBindingsInfoToo)
            return { err: false, records };

        let bindingsInfo = FleNSHandler.binder.prepareBindingsDict({ needpgpKey });
        if (bindingsInfo.err != false)
            return bindingsInfo;

        return {
            err: false,
            records,
            bindingDict: bindingsInfo.bindingDict
        };
    }


    static createFleNSVoteBlock(args) {
        clog.app.info(`create FleNSVoteBlock args: ${utils.stringify(args)}`);
        console.log(`create FleNSVoteBlock args: ${utils.stringify(args)}`);
        // let collisionBlockContainer = args.block;
        let clCollisionRef = args.clCollisionRef;
        let collisions = collisionsHandler.hasCollision({ clCollisionRef });
        clog.app.info(`collisionsHandler.has Collision, res : ${utils.stringify(collisions)}`);
        if (!collisions.hasCollision)
            return { err: false, msg: `there is no collision for FleNS(${utils.hash6c(clCollisionRef)})` }


        let fVBlock = fVHandler.createFVoteBlock({
            uplink: args.blockHash,
            bCat: iConsts.FLOAT_BLOCKS_CATEGORIES.FleNS,
            voteData: collisions.records
        });
        clog.app.info(`creatingfVBlock res: ${utils.stringify(fVBlock)}`);
        return fVBlock;

    }

    static estimateINameRegReqCost(args) {
        let iName = args.iName;
        let cDate = args.cDate;
        console.log(`calc INameRegReqCost args ${utils.stringify(args)}`);
        clog.app.info(`calc INameRegReqCost args ${utils.stringify(args)}`);
        if (!FleNSHandler.register.isAcceptableIName(iName)) {
            return { err: true, msg: `The inames(${iName}) is not acceptable` }
        }

        let dLen = 2000; // adummy price for document itself
        let pureCost = FleNSHandler.register.getPureINameRegCost({ iName });
        console.log('pureCost', pureCost);
        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: args.dType, dClass: args.dClass, dLen });

        theCost += pureCost.cost;

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: args.dType,
                dClass: args.dClass,
                dLen
            });
        return { err: false, cost: Math.floor(theCost) };
    }

    static removeIName(args) {
        let msg;
        //sceptical test
        let exist = model.sRead({
            table: tableINames,
            query: [
                ['in_hash', FleNSHandler.generateINameHash(iName)],
                ['in_name', iName],
            ]
        });
        if (exist.length != 1) {
            msg = `Try to delete iName strange result! ${utils.stringify(exist)}`;
            clog.sec.err(msg);
            return { err: true, msg }
        }

        clog.app.info(`Removing iName(${iName}) because of faild in payment`)
        let iName = args.iName;
        model.sDelete({
            table: tableINames,
            query: [
                ['in_hash', FleNSHandler.generateINameHash(iName)],
                ['in_name', iName],
            ]
        });
        return { err: false }
    }


}

FleNSHandler.register = require('./register-an-iname');
FleNSHandler.register._super = FleNSHandler;

FleNSHandler.binder = require('./bind-to-flens');
FleNSHandler.binder._super = FleNSHandler;

FleNSHandler.msg = require('./msg-to-flens');
FleNSHandler.msg._super = FleNSHandler;

// FleNSHandler.register._refreshMachineControlledINames();
module.exports = FleNSHandler;
