const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const machine = require('../../machine/machine-handler');
const walletHandler = require('../../web-interface/wallet/wallet-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const kvHandler = require('../../models/kvalue');
const model = require('../../models/interface');
const blockUtils = require('../../dag/block-utils');
const extinfosHandler = require('../../dag/extinfos-handler');


const tableLocalMsg = 'i_machine_iname_messages';

let msgDocTpl = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dLen: "0000000",
    dType: iConsts.DOC_TYPES.INameMsgTo,
    dClass: iConsts.GENERAL_CLASESS.Basic,
    dVer: "0.0.0",
    sINHash: "",
    sINBTitle: "",
    rINHash: "", // receiver's iName hash
    rINPBTitle: "", // receiver iPGP bind title
    dExtInfo: {},
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000",
    creationDate: ""
};

// since the messages are used for different purposes(e.g. message, public-key changing, pre-signed-transaction-comunication...)
// so must be structured
let msgBodyStruct = {
    msgType: iConsts.MESSAGES_TYPES.Plain,
    msgPayload: ""
}

class FleNSMsgHandler {

    static loadTextMessages(args = {}) {
        args.mimTypeClause = iConsts.MESSAGES_TYPES.Plain;
        let res = FleNSMsgHandler.loadMessages(args);
        console.log(res);
        if (res.err != false)
            return res;
        for (let aRec of res.records) {
            aRec.mim_message = utils.parse(aRec.mim_message).msgPayload;
        }
        return res;
    }

    static loadMessages(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let mimTypeClause = _.has(args, 'mimTypeClause') ? args.mimTypeClause : '';
        let senderIPNHash = args.senderIPNHash;
        let senderIPLabel = args.senderIPLabel;
        let receiverIPNHash = crypto.keccak256(args.receiverIPNHash);
        let receiverIPLabel = args.receiverIPLabel;

        if (!utils._nilEmptyFalse(mimTypeClause))
            mimTypeClause = `AND mim_type='${mimTypeClause}'`;

        let senderBindInfo = FleNSMsgHandler._super.binder.getBindInfoByINameAndBindTitle({
            nbinHash: senderIPNHash,
            nbTitle: senderIPLabel
        });
        clog.app.info(`load msgs of senderBindInfo: ${utils.stringify(senderBindInfo)}`);
        if (senderBindInfo.length != 1)
            return { err: true, msg: `load msgs, There is no iPGP binding info for sender(${senderIPLabel}/${utils.hash6c(senderIPNHash)})` }
        senderBindInfo = senderBindInfo[0];
        senderBindInfo.nbConfInfo = utils.parse(senderBindInfo.nbConfInfo);
        let query = ` 
            SELECT mim_direction, mim_message, mim_status, mim_receive_date 
            FROM ${tableLocalMsg} 
            WHERE ((mim_sender_in_hash=$1 AND mim_sender_key_label=$2) OR 
            (mim_receiver_in_hash=$1 AND mim_receiver_key_label=$2) OR
            (mim_sender_in_hash=$3 AND mim_sender_key_label=$4) OR 
            (mim_receiver_in_hash=$3 AND mim_receiver_key_label=$4)) ${mimTypeClause}
            ORDER BY mim_receive_date`;

        // if (mpCode != iConsts.CONSTS.ALL)
        //     query.push(['mim_mp_code', mpCode]);
        let values = [senderIPNHash, senderIPLabel, receiverIPNHash, receiverIPLabel];
        let records = model.sCustom({
            query, values
        });
        if (records.length == 0)
            return { err: false, msg: `No message exist!`, records: [] };
        return { err: false, records };
    }

    static async sendPublicKey(args = {}) {
        args.msgType = iConsts.MESSAGES_TYPES.PublicKey;
        return await FleNSMsgHandler.sendMessage(args);
    }

    static async sendBech32(args = {}) {
        args.msgType = iConsts.MESSAGES_TYPES.Bech32;
        return await FleNSMsgHandler.sendMessage(args);
    }

    static async sendPlainTextMessage(args = {}) {
        let msgBody = utils.sanitizingContent(args.msgBody);
        let msgStruct = _.clone(msgBodyStruct);
        args.msgType = iConsts.MESSAGES_TYPES.Plain;
        msgStruct.msgType = args.msgType;
        msgStruct.msgPayload = msgBody;
        args.msgStruct = msgStruct;
        return await FleNSMsgHandler.sendMessage(args);
    }

    static async sendMessage(args = {}) {
        let msg;
        console.log(`send Message args: `, args);
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;
        let msgType = _.has(args, 'msgType') ? args.msgType : iConsts.MESSAGES_TYPES.Plain;
        let msgStruct = _.has(args, 'msgStruct') ? args.msgStruct : null;

        if (utils._nilEmptyFalse(msgStruct)) {
            msg = `Empty msgStruct!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let receiverBindInfo = FleNSMsgHandler._super.binder.getBindInfoByINameAndBindTitle({
            nbinHash: args.receiverIPNHash,
            nbTitle: args.receiverIPLabel
        });
        if (receiverBindInfo.length != 1)
            return { err: true, msg: `There is no binfding for (${args.iPLabel}/${utils.hash6c(args.iPNHash)})` }
        receiverBindInfo = receiverBindInfo[0];
        receiverBindInfo.nbConfInfo = utils.parse(receiverBindInfo.nbConfInfo);
        console.log(`receiverBindInfo: `, receiverBindInfo);

        let senderBindInfo = FleNSMsgHandler._super.binder.getMachineIPGPBindInfoByINameAndBindTitle({
            mcbinHash: crypto.keccak256(args.senderverBbindedIName),
            mcbLabel: args.senderverBbindedLabel
        });
        if (senderBindInfo.length != 1)
            return { err: true, msg: `There is no binfding for (${args.iPLabel}/${utils.hash6c(args.iPNHash)})` }
        senderBindInfo = senderBindInfo[0];
        senderBindInfo.mcbConfInfo = utils.parse(senderBindInfo.mcbConfInfo);
        console.log(`senderBindInfo: `, senderBindInfo);

        let msgBody = crypto.encryptPGP({
            shouldSign: true, // in real world you can not sign an email in negotiation step in which the receiver has not your pgp public key
            shouldCompress: true,
            message: utils.stringify(msgStruct),
            sendererPrvKey: senderBindInfo.mcbConfInfo.iPPrvKey,
            receiverPubKey: receiverBindInfo.nbConfInfo.iPPubKey
        });


        let msgDoc = FleNSMsgHandler.prepareMsgDoc({
            sINHash: senderBindInfo.mcbConfInfo.iPNHash,    // sender iName hash
            sINBTitle: senderBindInfo.mcbConfInfo.iPLabel,    // sender iPGPbind (used to replay)
            msgBody,
            rINHash: receiverBindInfo.nbConfInfo.iPNHash,
            rINPBTitle: receiverBindInfo.nbConfInfo.iPLabel,
        });

        // pay for message
        // calculate bind cost
        let msgCost = this.calcMsgCost({
            cDate: utils.getNow(),
            msgDoc,
            stage: iConsts.STAGES.Creating
        });
        console.log(`msgCost Cost ${utils.stringify(msgCost)}`);
        if (msgCost.err != false)
            return msgCost;

        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_INAME_MSG', msgCost.cost]
        ];

        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: msgCost.cost * 2
        });
        if (spendables.err != false)
            return spendables;

        // return { err: false, msg: 'binding doc createsd' }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(msgCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: msgDoc.hash,
            description: 'Pay for msg to iName',
            inputs: inputs,
            outputs: outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed msg-to-iName costs trx: ', utils.stringify(trxDtl));


        // push trx & msgDoc Req to Block buffer
        let res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = await docBufferHandler.pushInAsync({ doc: msgDoc, DPCost: msgCost.cost });
        if (res.err != false)
            return res;

        await kvHandler.upsertKValueAsync(`tmp_msg_${msgDoc.hash}`, utils.stringify(msgStruct));

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your Message is pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;
    }

    static calcMsgCost(args) {
        // console.log(`calc message Cost args ${utils.stringify(args)}`);
        clog.app.info(`calc message Cost args ${utils.stringify(args)}`);
        let msgDoc = args.msgDoc;
        let cDate = args.cDate;

        let dLen = parseInt(msgDoc.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: msgDoc.dType, dClass: msgDoc.dClass, dLen });

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: msgDoc.dType,
                dClass: msgDoc.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static prepareMsgDoc(args) {
        let msg;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();

        let msgDoc = _.clone(msgDocTpl);
        msgDoc.sINHash = args.sINHash;
        msgDoc.sINBTitle = args.sINBTitle;
        msgDoc.creationDate = creationDate;
        msgDoc.dExtInfo.msgBody = args.msgBody;
        msgDoc.dExtHash = iutils.doHashObject(msgDoc.dExtInfo);
        msgDoc.rINHash = args.rINHash;    // receiver's iName hash
        msgDoc.rINPBTitle = args.rINPBTitle;   // receiver iName iPGP bind title
        msgDoc.dLen = iutils.paddingDocLength(utils.stringify(msgDoc).length);
        msgDoc.hash = FleNSMsgHandler.calcMsgDocHash(msgDoc);
        console.log('msgDoc', msgDoc);

        return msgDoc;
    }

    static calcMsgDocHash(msgDoc) {
        // as always ordering properties by alphabet
        let hashables = {
            creationDate: msgDoc.creationDate,
            dClass: msgDoc.dClass,
            dExtHash: msgDoc.dExtHash,
            dLen: msgDoc.dLen,
            dType: msgDoc.dType,
            sINBTitle: msgDoc.sINBTitle,
            sINHash: msgDoc.sINHash,
            rINHash: msgDoc.rINHash,
            rINPBTitle: msgDoc.rINPBTitle,
            dVer: msgDoc.dVer,
        }
        clog.app.info(`calcMsgDocHash hashables: ${utils.stringify(hashables)}`);
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static removeMsg(docHash) {
        // TODO: implement it
    }


    /**
    * 
    * @param {*} args 
    * records a local entity on machine
    */
    static recordAmsgDocInLocal(args) {
        let msg;
        clog.app.info(`record AmsgDocInLocal args: ${utils.stringify(args)}`);
        let block = args.block;
        let msgDoc = args.msgDoc;
        let msgDirection = null;

        let machineBindInfo = FleNSMsgHandler._super.binder.getMachineIPGPBindInfoByINameAndBindTitle({
            mcbinHash: msgDoc.rINHash,
            mcbLabel: msgDoc.rINPBTitle
        });
        clog.app.info(`record AmsgDocInLocal machineBindInfo(receiver): ${utils.stringify(machineBindInfo)}`);
        if (machineBindInfo.length == 1) {
            machineBindInfo = machineBindInfo[0];
            machineBindInfo.mcbConfInfo = utils.parse(machineBindInfo.mcbConfInfo);
            msgDirection = iConsts.CONSTS.RECEIVED;

        } else {
            machineBindInfo = FleNSMsgHandler._super.binder.getMachineIPGPBindInfoByINameAndBindTitle({
                mcbinHash: msgDoc.sINHash,
                mcbLabel: msgDoc.sINBTitle
            });
            clog.app.info(`record AmsgDocInLocal machineBindInfo(sender): ${utils.stringify(machineBindInfo)}`);
            if (machineBindInfo.length == 1) {
                machineBindInfo = machineBindInfo[0];
                machineBindInfo.mcbConfInfo = utils.parse(machineBindInfo.mcbConfInfo);
                msgDirection = iConsts.CONSTS.SENT;
            }
        }

        let receiverBindInfo;
        let values = {
            mim_direction: msgDirection,
            mim_mp_code: machineBindInfo.mcbmpCode,
            mim_doc_hash: msgDoc.hash,
            mim_sender_in_hash: msgDoc.sINHash,
            mim_sender_key_label: msgDoc.sINBTitle,
            // mim_message: msgBody,
            mim_receiver_in_hash: msgDoc.rINHash,
            mim_receiver_key_label: msgDoc.sINBTitle,
            mim_status: iConsts.CONSTS.UNREAD,
            mim_receive_date: utils.getNow(),
        };

        let dExtInfo;
        if (_.has(msgDoc, 'dExtInfo')) {
            dExtInfo = msgDoc.dExtInfo;
        } else {
            if (!_.has(block, 'bExtInfo')) {
                let bExtInfoRes = extinfosHandler.getBlockExtInfos(block.blockHash);
                if (bExtInfoRes.bExtInfo == null) {
                    msg = `missed bExtInfo1 (${utils.hash16c(block.blockHash)})`;
                    clog.sec.error(msg);
                    return { err: true, msg }
                }
                block.bExtInfo = bExtInfoRes.bExtInfo
            }
            let docInx = blockUtils.pickDocByHash(block, msgDoc.hash).docInx;
            dExtInfo = block.bExtInfo[docInx];
        }

        let msgBody = dExtInfo.msgBody;
        if (msgDirection == iConsts.CONSTS.RECEIVED) {
            let senderBindInfo = FleNSMsgHandler._super.binder.getBindInfoByINameAndBindTitle({
                nbinHash: msgDoc.sINHash,
                nbTitle: msgDoc.sINBTitle
            });
            clog.app.info(`record AmsgDocInLocal senderBindInfo: ${utils.stringify(senderBindInfo)}`);
            if (senderBindInfo.length != 1) {
                msg = `msg(${utils.hash6c(msgDoc.hash)}) was for me but machine hasn't nbinHash/nbTitle`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            senderBindInfo = senderBindInfo[0];
            senderBindInfo.nbConfInfo = utils.parse(senderBindInfo.nbConfInfo);

            // decrypt & insert in machine table if receiver iNames(rINHash) is controlled by machine
            let decryptedMessage = crypto.decryptPGP({
                prvKey: machineBindInfo.mcbConfInfo.iPPrvKey,
                senderPubKey: senderBindInfo.nbConfInfo.iPPubKey,
                message: msgBody
            });
            clog.app.info(`record AmsgDocInLocal received decryptedMessage: ${utils.stringify(decryptedMessage)}`);
            if (decryptedMessage.err != false) {
                msg = `FleNS Decryption failed msg from iName(${utils.hash6c(msgDoc.sINHash)}) doc(${utils.hash6c(msgDoc.hash)})`;
                clog.sec.warn(msg);
                return { err: true, msg };
            }
            if (decryptedMessage.isSigned && !decryptedMessage.verified) {
                msg = `Wrong signature for msg from iName(${utils.hash6c(msgDoc.sINHash)}) doc(${utils.hash6c(msgDoc.hash)})`;
                clog.sec.error(msg);
                return { err: true, msg };
            }
            if (utils._nilEmptyFalse(decryptedMessage.message)) {
                msg = `empty body for msg from iName(${utils.hash6c(msgDoc.sINHash)}) doc(${utils.hash6c(msgDoc.hash)})`;
                clog.sec.error(msg);
                return { err: true, msg };
            }

            let msgStruct = decryptedMessage.message;
            clog.app.info(`received message: ${msgStruct}`);
            values.mim_message = msgStruct;
            let tmpMsg = utils.parse(msgStruct);
            values.mim_type = tmpMsg.msgType;

        } else if (msgDirection == iConsts.CONSTS.SENT) {
            let msgKey = `tmp_msg_${msgDoc.hash}`;
            let msgStruct = kvHandler.getValueSync(msgKey);
            clog.app.info(`record AmsgDocInLocal received msgStruct: ${msgStruct}`);

            if (utils._nilEmptyFalse(msgStruct))
                return { err: false, msg: `Doc(${utils.hash6c(msgDoc.hash)}) is an empty message!` };

            values.mim_message = msgStruct;
            let tmpMsg = utils.parse(msgStruct);
            values.mim_type = tmpMsg.msgType;
            // remove message from kValue table
            kvHandler.deleteSync(msgKey)

        } else {
            return { err: false, msg: `the msg wasn't neither machine sender nor receiver` }
        }

        let dbl = model.sRead({
            table: tableLocalMsg,
            query: [
                ['mim_doc_hash', msgDoc.hash]
            ]
        });
        if (dbl.length > 0)
            return { err: false, msg: `the message doc(${utils.hash6c(msgDoc.hash)}) already existed in machine table` };

        // insert to DB
        clog.app.info(`create insert a message to local ${utils.stringify(values)}`);
        model.sCreate({
            table: tableLocalMsg,
            values
        });

        return { err: false }

        //     machineBindInfo = FleNSMsgHandler._super.binder.getMachineIPGPBindInfoByINameAndBindTitle({
        //         mcbinHash: msgDoc.rINHash,
        //         mcbLabel: msgDoc.rINPBTitle
        //     });
        //     clog.app.info(`record AmsgDocInLocal machineBindInfo: ${utils.stringify(machineBindInfo)}`);
        //     if (machineBindInfo.length != 1)
        //         return { err: true, msg: `There is no iPGP binding info for receiver(${msgDoc.rINHash}/${utils.hash6c(msgDoc.rINPBTitle)})` }
        //     machineBindInfo = machineBindInfo[0];
        //     machineBindInfo.mcbConfInfo = utils.parse(machineBindInfo.mcbConfInfo);
        //     // it is a sent message
        //     msgDirection = iConsts.CONSTS.SENT;
        //     receiverBindInfo = FleNSMsgHandler._super.binder.getBindInfoByINameAndBindTitle({
        //         nbinHash: msgDoc.sINHash,
        //         nbTitle: msgDoc.sINBTitle

    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'mim_mp_code'))
            out.mimmpCode = elm.mim_mp_code;
        if (_.has(elm, 'mim_type'))
            out.mimType = elm.mim_type;
        if (_.has(elm, 'mim_direction'))
            out.mimDirection = elm.mim_direction;
        if (_.has(elm, 'mim_doc_hash'))
            out.mimdocHash = elm.mim_doc_hash;
        if (_.has(elm, 'mim_sender_in_hash'))
            out.mimSenderINHash = elm.mim_sender_in_hash;
        if (_.has(elm, 'mim_sender_key_label'))
            out.mimSenderKeyLabel = elm.mim_sender_key_label;
        if (_.has(elm, 'mim_message'))
            out.mimMessage = elm.mim_message;
        if (_.has(elm, 'mim_receiver_in_hash'))
            out.mimReceiverINHash = elm.mim_receiver_in_hash;
        if (_.has(elm, 'mim_receiver_key_label'))
            out.mimReceiverKeyLabel = elm.mim_receiver_key_label;
        if (_.has(elm, 'mim_status'))
            out.mimStatus = elm.mim_status;
        if (_.has(elm, 'mim_receive_date'))
            out.mimReceiveDate = elm.mim_receive_date;
        return out;
    }

}

module.exports = FleNSMsgHandler;
