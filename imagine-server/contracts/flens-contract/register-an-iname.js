const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const model = require('../../models/interface');
const crypto = require('../../crypto/crypto');
const machine = require('../../machine/machine-handler');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const collisionsHandler = require('../../services/collisions/collisions-manager');
const DNAHandler = require('../../dna/dna-handler');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const kvHandler = require('../../models/kvalue');


const tableINames = 'i_iname_records';
const tableMachineINames = 'i_machine_iname_records';

let flensTpl = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dLen: "0000000",
    dType: iConsts.DOC_TYPES.INameReg,
    dClass: iConsts.INAME_CLASESS.NoDomain,
    dVer: "0.0.0",
    iName: "", // requested name to register
    iNOwner: "",    // the bech32 address of iName owner
    dExtInfo: {
        signatures: [],
        uSet: {}
    },
    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000",
    creationDate: ""
};

class iNameRegisterHandler {

    static initINames(block) {
        // register iName imagine & hu
        let reservedINames = utils.arrayUnique(require('./reserved-inames'));
        for (let aName of reservedINames) {
            let insertRes = iNameRegisterHandler._recordINameInDAG({
                block,
                flens: {
                    hash: iNameRegisterHandler._super.generateINameHash(aName),
                    iName: aName,
                    iNOwner: iConsts.HU_INAME_OWNER_ADDRESS,
                },
                inIsSettled: iConsts.CONSTS.YES
            });

        }
    }

    static _recordINameInDAG(args) {
        let inIsSettled = _.has(args, 'inIsSettled') ? args.inIsSettled : iConsts.CONSTS.NO;
        let block = args.block;
        let flens = args.flens;
        let iName = iNameRegisterHandler.normalizeIName(flens.iName);
        let iNameHash = iNameRegisterHandler._super.generateINameHash(iName);
        let values = {
            in_doc_hash: flens.hash,
            in_name: iName,
            in_hash: iNameHash,
            in_owner: flens.iNOwner,
            in_register_date: block.creationDate,
            in_is_settled: inIsSettled
        };
        clog.app.info(`Registering iName(${iName}) in iName DAG values:${utils.stringify(values)}`)
        model.sCreate({
            table: tableINames,
            values
        });

        return iNameRegisterHandler._addToMachineControlled({
            iNameHash,
            flens,
            iName,
            block
        });
    }

    static _refreshMachineControlledINames() {
        let records = model.sRead({
            table: tableINames,
            order: [['in_register_date', 'ASC']]
        });
        for (let anIname of records) {
            iNameRegisterHandler._addToMachineControlled({
                iName: anIname.in_name,
                iNameHash: anIname.in_hash,
                flens: {
                    hash: anIname.in_doc_hash,
                    iNOwner: anIname.in_owner,
                },
                block: {
                    creationDate: anIname.in_register_date,
                }
            });
        }

        return;
    }

    static _addToMachineControlled(args) {
        let iNameHash = args.iNameHash;
        let iName = args.iName;
        let flens = args.flens;
        let block = args.block;

        // if the iName is controlled by machine-wallet mProfiles, add it to local db too
        let machineINames = iNameRegisterHandler._super.searchForINamesControlledByWallet({ mpCode: iConsts.CONSTS.ALL });
        clog.app.info(`machine INames: ${utils.stringify(machineINames)}`);
        let inHashes = machineINames.records.map(x => x.inHash);
        if (inHashes.includes(iNameHash)) {
            let dbl = model.sRead({
                table: tableMachineINames,
                query: [['imn_hash', iNameHash]]
            });
            if (dbl.length == 0) {
                // insert
                let values = {
                    imn_mp_code: machineINames.mapAddressToProfile[flens.iNOwner],
                    imn_doc_hash: flens.hash,
                    imn_name: iName,
                    imn_hash: iNameHash,
                    imn_owner: flens.iNOwner,
                    imn_info: utils.stringify({}),
                    imn_register_date: block.creationDate,
                }
                clog.app.info(`_record IName In local machine: ${utils.stringify(values)}`);
                model.sCreate({
                    table: tableMachineINames,
                    values
                });
            }
        }
        return { err: false }
    }


    static iNameSettlement() {
        let msg;
        const dagHandler = require('../../dag/graph-handler/dag-handler');

        // do recursive callbacks
        if (iConsts.TIME_GAIN == 1) {
            // it is live setting, so every 31 minutes do callback
            setTimeout(iNameRegisterHandler.iNameSettlement, 1000 * 60 * 31);
        } else {
            // two times in each cycle do callback
            setTimeout(iNameRegisterHandler.iNameSettlement, 1000 * 60 * iConsts.TIME_GAIN / 4);
        }

        // find unsettled iNames which is registered more than 12 hours ago
        let toBeSetteled = iNameRegisterHandler.searchRegisteredINames({
            query: [
                ['in_is_settled', iConsts.CONSTS.NO],
                ['in_register_date', ['<', utils.minutesBefore(iConsts.getCycleByMinutes())]],
            ]
        });
        if (toBeSetteled.err != false)
            return toBeSetteled;
        if (toBeSetteled.records.length > 0) {
            for (let aRec of toBeSetteled.records) {
                clog.app.info(`going to settle ${utils.stringify(aRec)}`);
                let collisionReport = collisionsHandler.makeCollisionReport({ clCollisionRef: aRec.inHash });
                clog.app.info(`going to collisionReport ${utils.stringify(collisionReport)}`);
                if (_.has(collisionReport, 'aDocHash') && !utils._nilEmptyFalse(collisionReport.aDocHash)) {
                    // do settle
                    // retrieve winer doc proper info
                    let theDoc = dagHandler.retrieveDocByDocHash({ docHash: collisionReport.aDocHash });
                    clog.app.info(`iNameSettlement retrieve document ${utils.stringify(theDoc)}`);
                    if (theDoc.err != false) {
                        msg = `iNameSettlement couldn't find doc(${utils.hash6c(alreadyRecordedDoc.inDocHash)})`;
                        clog.sec.error(msg);
                        continue;
                    }
                    if (!_.has(theDoc, 'document') || utils._nilEmptyFalse(theDoc.document)) {
                        msg = `iNameSettlement the doc(${utils.hash6c(alreadyRecordedDoc.inDocHash)}) was empty`;
                        clog.sec.error(msg);
                        continue;
                    }

                    theDoc = theDoc.document
                    let updates = {
                        in_is_settled: iConsts.CONSTS.YES,
                        in_owner: theDoc.iNOwner,
                        in_doc_hash: theDoc.hash,
                    }
                    clog.app.info(`iNameSettlement update values ${utils.stringify(updates)}`);

                    model.sUpdate({
                        table: tableINames,
                        query: [
                            ['in_name', theDoc.iName],
                            ['in_hash', iNameRegisterHandler._super.generateINameHash(theDoc.iName)],
                        ],
                        updates
                    });
                }
            }
        }


    }

    static calcCostDINameRegReq(args) {
        console.log(`calc INameRegReqCost args ${utils.stringify(args)}`);
        clog.app.info(`calc INameRegReqCost args ${utils.stringify(args)}`);
        let flensDoc = args.flens;
        let cDate = args.cDate;

        let dLen = parseInt(flensDoc.dLen);
        let iName = flensDoc.iName;

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: flensDoc.dType, dClass: flensDoc.dClass, dLen });

        let pureCost = iNameRegisterHandler.getPureINameRegCost({ iName });
        if (pureCost.err != false)
            return pureCost;
        theCost += pureCost.cost;

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: flensDoc.dType,
                dClass: flensDoc.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static isAcceptableIName(iName) {
        /**
         * dummy simple way to avoid registering general domain names (e.g. google.com)
         * later, after finding a way to authorize canonical domain name owner can activate it
         * the ":" strictly prohobited, because used for port number seperating also / \ and |
         */

        if (iName.includes(':') || (iName.includes('.') && !iName.includes('@')))
            return false;

        return true;
    }

    static getSignMsgDFleNS(flens) {
        // as always ordering properties by alphabet
        let iName = iNameRegisterHandler.normalizeIName(flens.iName);
        let signables = {
            creationDate: flens.creationDate,
            dClass: flens.dClass,
            dType: flens.dType,
            iName,
            iNOwner: flens.iNOwner,
            dVer: flens.dVer,
        }
        clog.app.info(`FLENS signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }


    static normalizeIName(inp) {
        // it is tooo strict. later can be a more free even unicode and smiley domains
        return inp.replace(/([^a-z0-9-_@]+)/gi, '-').toLowerCase();
    }


    static getPureINameRegCost(args) {
        console.log(`calc IName RegReqCost args ${utils.stringify(args)}`);
        clog.app.info(`calc IName RegReqCost args ${utils.stringify(args)}`);

        let iName = args.iName;
        let theCost = 0;
        if (iName.includes('@') && (utils.isAValidEmailFormat(iName))) {
            // the email addresses are most cheaper than simple names
            theCost += iConsts.INAME_UNIT_PRICE_EMAIL;
        } else {
            theCost += iConsts.INAME_UNIT_PRICE_NO_EMAIL;
        }
        console.log('iConsts.INAME_UNIT_PRICE_NO_EMAIL', iConsts.INAME_UNIT_PRICE_NO_EMAIL);
        console.log('theCost', theCost);

        let lenFactor = utils.calcLog(iName.length, iConsts.INAME_THRESHOLD_LENGTH, 1).gain;
        if (lenFactor < 1)
            lenFactor = 1;
        theCost *= lenFactor;

        return { err: false, cost: Math.floor(theCost) };
    }


    static async iNameRegReq(args) {
        let msg;
        const walletHandler = require('../../web-interface/wallet/wallet-handler');
        clog.app.info(`iName Reg Req args: ${utils.stringify(args)}`);
        // in order to send a FLENS() request to network, must create a documetn of type flens and pay flens fee too

        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;
        let iName = args.iName;
        let iNameOwner = args.iNameOwner;

        // create flens doc
        let flensRes = iNameRegisterHandler.prepareINameRegRequest({
            iName,
            iNameOwner
        });
        if (flensRes.err != false)
            return flensRes;

        let flens = flensRes.flens;
        clog.app.info(`prepared flens to send ${utils.stringify(flens)}`);
        console.log(`prepared flens to send ${utils.stringify(flens)}`);

        // calculate flens cost
        let iNameCost = iNameRegisterHandler.calcCostDINameRegReq({
            cDate: utils.getNow(),
            flens,
            stage: iConsts.STAGES.Creating
        });
        console.log(`iName Cost ${utils.stringify(iNameCost)}`);
        if (iNameCost.err != false)
            return iNameCost;

        // create a transaction for payment
        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_INAME_REG', iNameCost.cost]
        ];
        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: iNameCost.cost * 1.3  // an small portion bigger to support DPCosts
        });
        if (spendables.err != false)
            return spendables;

        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(iNameCost.cost * 1.3),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: flens.hash,
            description: 'Pay for request for register an iName(FleNS)',
            inputs: inputs,
            outputs: outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed iName costs trx: ', utils.stringify(trxDtl));


        // push trx & flens-iName Req to Block buffer
        let res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = await docBufferHandler.pushInAsync({ doc: flens, DPCost: iNameCost.cost });
        if (res.err != false)
            return res;

        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

        // also binding a default iPGP pair key to domain name
        let bidRes = await iNameRegisterHandler._super.binder.createAndBindIPGP({
            isHotIName: true,
            hotInameOwner: iNameOwner,
            dTarget: iConsts.CONSTS.TO_BUFFER,
            nbINameHash: iNameRegisterHandler._super.generateINameHash(iName),
            iPLabel: iConsts.CONSTS.DEFAULT,
            iPComment: 'Always PGP',
        });

        if (bidRes.err)
            return bidRes;

        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your FleNs(iName Register Request) equest is pushed to Block buffer ` + bidRes.msg };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }


    static prepareINameRegRequest(args) {
        let msg;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let iName = iNameRegisterHandler.normalizeIName(args.iName);   // TODO: improve it to accept unicode domain

        let iNameOwner = args.iNameOwner;
        let flens = _.clone(flensTpl);

        let iNameRegInfo = iNameRegisterHandler.ownerRegisteredRecords(iNameOwner);
        clog.app.info(`owner registered iNames: ${utils.stringify(iNameRegInfo)}`);
        if (iNameRegInfo.available < 1) {
            msg = `owner(${iutils.shortBech8(iNameOwner)}) has ${iNameRegInfo.shares} shares, `;
            msg += `so can register ${iNameRegInfo.available} iNames. (each iName needs ${iConsts.SHARES_PER_INAME} shares)`;
            if (iNameRegInfo.alreadyUsed > 0)
                msg += `you alresdy registered ${iNameRegInfo.alreadyUsed} iNames.`;
            return { err: true, msg };
        }
        let isRegistred = iNameRegisterHandler.alreadyRegistered(iName).isRegistred;
        if (isRegistred)
            return { err: true, msg: `iName (${iName}) already registerd!` };

        let preRecordedINames = kvHandler.getValueSync('preRecordedINames');
        if (!utils._nilEmptyFalse(preRecordedINames))
            preRecordedINames = utils.parse(preRecordedINames);
        if (utils._nilEmptyFalse(preRecordedINames))
            preRecordedINames = [];
        if (preRecordedINames.includes(iName)) {
            return { err: true, msg: `iName (${iName}) already registerd by you & placed in buffer to broadcast!` };
        }
        preRecordedINames.push(iName);
        kvHandler.upsertKValueSync('preRecordedINames', utils.stringify(preRecordedINames));

        flens.iName = iName;
        flens.iNOwner = iNameOwner;
        flens.creationDate = creationDate;
        flens = iNameRegisterHandler.signINameRegReq(flens);

        flens.dExtHash = iutils.doHashObject({
            signatures: flens.dExtInfo.signatures,
            uSet: flens.dExtInfo.uSet
        });

        flens.dLen = iutils.paddingDocLength(utils.stringify(flens).length);
        flens.hash = iNameRegisterHandler.calcHashDINameRegReqS(flens);
        return { err: false, flens };

    }

    static calcHashDINameRegReqS(flens) {
        // alphabetical order
        let hashables = {
            dLen: flens.dLen,
            dExtHash: flens.dExtHash,
        };
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static signINameRegReq(flens) {
        let msg;

        let addrDtl = walletAddressHandler.getAddressesInfoSync([flens.iNOwner]);
        if (addrDtl.length == 0)
            return { err: true, msg: `the owner address ${flens.iNOwner} is not controlled by wallet` };

        addrDtl = utils.parse(addrDtl[0].waDetail);
        let unlockerIndex = 0;  // TODO: the unlocker should be customizable
        let unlockSet = addrDtl.uSets[unlockerIndex]

        let signatures = [];
        // if already signed exit

        console.log('unlockSet', unlockSet);
        console.log('sSets', unlockSet.sSets);

        flens.dExtInfo.uSet = unlockSet;
        let signMsg = iNameRegisterHandler._super.register.getSignMsgDFleNS(flens);
        for (let inx = 0; inx < unlockSet.sSets.length; inx++) {
            let signature = crypto.signMsg(signMsg, addrDtl.privContainer[unlockSet.salt][inx]);
            signatures.push(signature);
        }
        if (signatures.length == 0) {
            msg = `The FleNS couldn't be signed`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        flens.dExtInfo.signatures = signatures;

        return flens;
    }


    /**
     * 
     * @param {*} address 
     * takes address and based on holder shares and the number of already registered iNames returns result
     */
    static ownerRegisteredRecords(address) {

        let { holdersByKey } = DNAHandler.getSharesInfo();
        if (!_.has(holdersByKey, address))
            return { err: true, msg: `The account ${iutils.shortBech16(address)} has nothing shares!` }

        let shares = holdersByKey[address];

        let alreadyUsed = model.sRead({
            table: tableINames,
            query: [['in_owner', address]]
        }).length;

        // this formula will not work well after 7 years when the share going to amortization!
        // TODO: enhance the formul, or even better after 7 years we do not need at all this limitation
        let allowed = utils.floor(shares / iConsts.SHARES_PER_INAME);

        return {
            shares,
            allowed,
            alreadyUsed,
            available: allowed - alreadyUsed
        }
    }


    static alreadyRegistered(iName) {

        let res = model.sRead({
            table: tableINames,
            query: [
                ['in_name', ['ILIKE', iNameRegisterHandler.normalizeIName(iName)]]
            ]
        });
        if (res.length == 0)
            return { isRegistred: false };

        res = iNameRegisterHandler.convertFieldsRecordsIName(res[0]);
        res.isRegistred = true;
        return res;
    }


    static searchRegisteredINames(args = {}) {
        let needBindingsInfoToo = _.has(args, 'needBindingsInfoToo') ? args.needBindingsInfoToo : false;
        args.table = tableINames;
        args.order = [['in_name', 'ASC']];
        let res = model.sRead(args);
        if (res.length == 0)
            return { err: false, msg: 'There is no registered iName!', records: [] };

        res = res.map(x => iNameRegisterHandler.convertFieldsRecordsIName(x));
        if (!needBindingsInfoToo)
            return { err: false, msg: `There are ${res.length} registered iName`, records: res };

        let bindings = iNameRegisterHandler._super.binder.prepareBindingsDict({ needpgpKey: false });
        if (bindings.err != false)
            return bindings;
        res = {
            err: false,
            msg: `There are ${res.length} registered iName`,
            records: res,
            bindingDict: bindings.bindingDict
        };
        return res;
    }


    static convertFieldsRecordsIName(elm) {
        let out = {};
        if (_.has(elm, 'in_doc_hash'))
            out.inDocHash = elm.in_doc_hash;
        if (_.has(elm, 'in_name'))
            out.iName = elm.in_name;
        if (_.has(elm, 'in_hash'))
            out.inHash = elm.in_hash;
        if (_.has(elm, 'in_owner'))
            out.inOwner = elm.in_owner;
        if (_.has(elm, 'in_is_settled'))
            out.inIsSettled = elm.in_is_settled;
        if (_.has(elm, 'in_register_date'))
            out.inRegisterDate = elm.in_register_date;
        return out;
    }

}

module.exports = iNameRegisterHandler;
