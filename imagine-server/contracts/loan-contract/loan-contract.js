const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');


class LoanContractHandler {

    static pledgeAddress(args) {
        let address = _.has(args, 'address') ? args.address : null;

    }

    static calcLoanRepayments(args) {
        let msg;
        clog.app.info(`calc LoanRepayments args: ${utils.stringify(args)}`);

        let orgPrincipal = args.principal;    // the PAIs are loaned
        if (!(orgPrincipal > 0)) {
            msg = `The loan principal(${orgPrincipal}) is invalid`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg };
        }

        let annualInterest = args.annualInterest / 100;
        if (!(annualInterest > 0)) {
            msg = `The loan annual interest(${annualInterest}) is invalid`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg };
        }

        let repaymentAmount = args.repaymentAmount;
        if (!(repaymentAmount > 0)) {
            msg = `The loan repayment amount(${repaymentAmount}) is invalid`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg };
        }

        // how many times do repayment in a year in imagine = 365*2 shich means 2 time in day
        let repaymentSchedule = _.has(args, 'repaymentSchedule') ? args.repaymentSchedule : 365 * 2;
        if (!(repaymentSchedule > 0)) {
            msg = `The loan repayment schedule(${repaymentSchedule}) is invalid`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg };
        }

        let maxRepaymentsCount = _.has(args, 'maxRepaymentsCount') ? args.maxRepaymentsCount : iConsts.DEFAULT_MAX_REPAYMENTS;
        if (!(maxRepaymentsCount > 0)) {
            msg = `The loan max Repayments Count(${maxRepaymentsCount}) is invalid`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg };
        }

        let percision = _.has(args, 'percision') ? args.percision : 0;

        let inx = 1;
        let repayments = [];
        // console.log(`Balance \tRepayment \tInterest Paid \tPrincipal Paid \tNew Balance`);
        let principal = orgPrincipal;
        let newPrincipal;
        let periodInterest;
        let totalRePaymentAmount = 0;
        let interestGain = (annualInterest / repaymentSchedule);
        while ((principal > repaymentAmount) && (inx < maxRepaymentsCount)) {
            periodInterest = utils.customFloorFloat(interestGain * principal, percision);
            newPrincipal = utils.customFloorFloat(principal - repaymentAmount + periodInterest, percision);
            // console.log(`${inx}.${newPrincipal} \t${repaymentAmount} \t${periodInterest} \t${repaymentAmount - periodInterest} \t${tmpPrincipal - repaymentAmount + periodInterest}`);
            totalRePaymentAmount += repaymentAmount;
            repayments.push({
                repaymentNumber: inx,
                balance: principal,
                repaymentAmount,
                paidInterest: periodInterest,
                paidPrincipal: repaymentAmount - periodInterest,
                newBalance: newPrincipal
            });
            principal = newPrincipal;
            inx++;
        }

        if (principal > repaymentAmount) {
            msg = `By Repayment(${utils.microPAIToPAI(repaymentAmount)}) PAIs per Cycle, you can not finish your ${utils.microPAIToPAI(orgPrincipal)} PAIs dept in ${iConsts.DEFAULT_MAX_REPAYMENTS} Repayments!`;
            console.log(`msg: ${msg}`);
            clog.app.error(msg);
            return { err: true, msg, interestGain, principal: orgPrincipal };
        }

        // add the last repayment
        periodInterest = utils.customFloorFloat(interestGain * principal, percision);
        let LastRepaymentAmount = principal + periodInterest;
        totalRePaymentAmount += LastRepaymentAmount;
        newPrincipal = utils.customFloorFloat(principal - LastRepaymentAmount + periodInterest);
        if (newPrincipal < 0)
            newPrincipal = 0;
        repayments.push({
            repaymentNumber: inx,
            balance: principal,
            repaymentAmount: LastRepaymentAmount,
            paidInterest: periodInterest,
            paidPrincipal: LastRepaymentAmount - periodInterest,
            newBalance: newPrincipal
        });


        return {
            err: false,
            repayments,
            interestGain,
            principal: orgPrincipal,
            totalRePaymentAmount,
            repaymentsNumber: repayments.length
        };
    }

}

module.exports = LoanContractHandler;