const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');

/**
 * reputation-system will be start to be used as an optional plugin in imagine ecosystem.
 * and later depends on usefulness of this sub-system, in the future we can add this plugin to imagine core
 * 
 */

/**
 * some predictions,
 * the prediction/statement/condition MUST be some CLEAR STATEMENT, that everybody get SAME MEAN.
 * e.g. MBA will win the league 2021
 * clearness of statements(which created by a certain Actor) is also one of parameters which has effect on an Acor reputation
 *  
 * the reponse of a prediction can be bolean or analogous(between -100 to 100). and every body can freely brodcast her opinion about predition and the status of prediction in her POV
 * at the time she publish her opinion.
 */

 /**
  * every two parties can make a "future contract", they can put a certain percentage of contract as a guaranty in form of blocked PAIs
  * or even a part of future incomes because of they sharea in imagine's DNA.
  * BTW the contract reffers to an statement doc as a contract condition
  * and at the end there are arbiters/oracles. the person who have knowledge & right to tell her/his opinion about a certain statement.
  * obviously EVERY BODY can broadcast their opinion about EVERY statements.
  * but the contract cosiders the one mentioned in contract.
  * contract can mention a few arbiter/oracles or hundred or thousands of them. 
  * of course adding more arbiter/oracle means paying more fee either to arbiter or imagine Treasury
  * contract can specified the metodology of counting opinions also all features of Polling-sub-system which is exist in imagine eccosystem
  * contract can specified the vote gain of each arbiter/oracle/voter too.
  * 
  */

let statementDoc = {
    hash: "",
    condition: "in 2020-10-11 at Market..., Gold price is heigher than 574$",
    dueDate: "",    // the date, oracle can not tell his opinion before that
    opinionType: 'Boolean/Analouge'
};

/**
 * every body can be an Oracle,
 * as time passes the reputation of an oracle growth. or miss
 * it is all up to her statements 
 * 
 */
let actorOpinion = {
    actorAccount: "im1pq...",   //  the BECH32 address of oracle
    statementDocHash: "",
    isSatisfied: iConsts.CONSTS.YES,   //  Y/N/A the oracle states the clause is satisfied or not or can abstain to tell his opinion   (e.g. in 2020-10-11 at Market..., Gold price is heigher than 574$)
    signatures: []
};


let reputationContract = {
    engagedAddress: "",      // Bech32 address which has income from treasury dividence, and used as a guaranty for well treatment
    engagementType: "FIX",      // can be FIX or PERCENTAGE
    engagementValue: 5, // in case of bad reputation, maximum x percent/PAI of income of the account will be donated to treasury(or charity, individuals... if mentioned)
    penalReceiver: "",  // in case of bad reputation, the engagement will go to treasury(TP_REP_FAILD), charity accounts or individuals account. depends on contract settings

}

/**
 * 3rd party algorythms(e.g. some AI) based on actors history can calculate some reputation gain for each actor and offer risk rate and other usefull info 
 * for decision making
 * 
 */

class ReputationHandler {

    /**
     * given reputation contract, returns how much is trustable
     * the Actor can be an Oracle, Arbiter or parties which are engaged in a predition
     */
    static getActorReputationConfidence(reptContractHash) {



        return {
            trustDtl: [
                { form: 0, to: iConsts.THOUSAND_PAI, score: 99.99 },
                { form: iConsts.THOUSAND_PAI, to: 100 * iConsts.THOUSAND_PAI, score: 93 },
                { form: 100 * iConsts.THOUSAND_PAI, to: 500 * iConsts.THOUSAND_PAI, score: 71 },
                { form: 500 * iConsts.THOUSAND_PAI, to: iConsts.MILLION_PAI, score: 59 },
                { form: iConsts.MILLION_PAI, score: 37 },
            ]
        }

    }
}
module.exports = ReputationHandler;


reputation system: after a gudgement on a case, if the 90% of gudges confirm "YES" the gudges which said "NO" will lose PAIs in stack/pledged account/or only get bad reputation which means lost nothing except reputation
the treashold percent can be adjusted between 60 to 100. less treashold means hight risk for users to trust this jury. and for jury less treshold dossiers in history means less confidence
let concludeStatementAffectOnJudges = {
failThreshold:0,	// a number between 0 to 100 to demonstrate the limit of votes for accepting final result, it also represents the 
cashBail:0,	// it could be a number which represents the PAIs value which is bailed in contract
cashPrize:0,	// it could be a number which lost percentage if the actor lose
cashPenal:0,	// it could be a number which benefit percentage if the actor win

accountBail:0,	// it could be a number which represents the PAIs value which is bailed in contract
accountPrize:0,	// it could be a number which lost percentage if the actor lose
accountPenal:0,	// it could be a number which benefit percentage if the actor win

reputationBail:0,	// it could be a number which represents the PAIs value which is bailed in contract
reputationPrize:0,	// it could be a number which lost percentage if the actor lose
reputationPenal:0,	// it could be a number which benefit percentage if the actor win

};
