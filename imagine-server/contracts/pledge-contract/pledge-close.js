const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');
const machine = require('../../machine/machine-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const docBufferHandler = require('../../services/buffer/buffer-handler');
const pldegeHandler = require('./pledge-handler');


const tableDAG = 'i_pledged_accounts';



let closePledgeTpl = {

    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dType: iConsts.DOC_TYPES.ClosePledge,
    dLen: "0000000",
    dVer: '0.0.3',
    concluder: "",
    closeMsg: '',
    ref: '',    // the contract which is going to be closed


    creationDate: "",
    closeMsg: 'Closed because of finished repayments'// closed because plegee remits remained repayments
    // clog.app.info(`pledgee contract signables: ${utils.stringify(signables)}`);
    // let hash = iutils.doHashObject(signables);
    // let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
    // return signMsg;
}

class PledgeCloser {


    static closeByPledger(args) {
        // TODO: implement it 
        // the calculation to determainif contract concludable or not is almost same as conclude ByNetwork
        // moreover control entire history payment to validate request
        return { err: true, msg: `This feature still doedn't work` };
    }

    /**
     * 
     * @param {pledgeHash} args 
     * prepares a close contract or a given pledgeHash
     */
    static closeByPledgeeOrByArbiter(args) {
        let msg;
        clog.app.info(`close ByPledgeeOrByArbiter args: ${utils.stringify(args)}`);
        const walletHandler = require('../../web-interface/wallet/wallet-handler');

        let unlockerIndex = _.has(args, 'unlockerIndex') ? args.unlockerIndex : 0;
        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let pledge = _.has(args, 'pledge') ? args.pledge : null;
        let pledgeHash = _.has(args, 'pledgeHash') ? args.pledgeHash : null;
        if (pledge == null) {
            // retrieve pledge by hash
            const pPledgeHandler = require('./proposal-pledge-contract');
            pledge = pPledgeHandler.retrievePledgeInfo(pledgeHash).pledge;
        }
        let signerInfo = pldegeHandler.recognizeSignerTypeInfo({ pledge });
        if (signerInfo.err != false)
            return signerInfo;

        let closePledgeDoc = this.prepareCloseDocByPledgeeOrByArbiter({
            pledge,
            concluder: signerInfo.byType,
            description: `Closed because of ${signerInfo.signerType} request`
        });
        clog.app.info(`closePledgeDoc: ${utils.stringify(closePledgeDoc)}`);


        // now sign a transaction to pay close-pledge's costs
        let closingCost = this.calcClosePldgCost({
            cDate: utils.getNow(),
            stage: iConsts.STAGES.Creating,
            closePledgeDoc,
            byType: signerInfo.byType
        });
        if (closingCost.err != false)
            return closingCost;

        // create a transaction for payment
        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_PLEDGE_CLOSE', closingCost.cost]
        ];
        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(closingCost.cost * 1.2) // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(closingCost.cost * 3),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: closePledgeDoc.hash,
            description: 'Pay for applying closePledgeDoc',
            inputs,
            outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed closePledgeDoc cost trx: ', utils.stringify(trxDtl));
        pledge.trx = trxDtl.trx.hash
        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);



        // push trx & closePledge to Block buffer
        let res = docBufferHandler.pushInSync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
        if (res.err != false)
            return res;

        res = docBufferHandler.pushInSync({ doc: closePledgeDoc, DPCost: closingCost.cost });
        if (res.err != false)
            return res;


        if (dTarget == iConsts.CONSTS.TO_BUFFER)
            return { err: false, msg: `Your Pledge close doc is pushed to Block buffer` };

        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
        // return new Promise((resolve, reject))
        let r = wBroadcastBlock.broadcastBlock();
        return r;

    }

    static calcClosePldgCost(args) {
        let closePledgeDoc = args.closePledgeDoc;
        let byType = args.byType;
        let stage = args.stage;
        let cDate = args.cDate;

        let dLen = parseInt(closePledgeDoc.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: closePledgeDoc.dType, dClass: closePledgeDoc.dClass, dLen });

        // if pledger asks for closing contract, entire network must audit entire payments(maybe for years) and decide about closing. this process s TOO costly
        // TODO: implement, machine calc cost based on repayments count.
        if (byType == iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger)
            theCost *= 1000;


        if (stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: closePledgeDoc.dType,
                dClass: closePledgeDoc.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static prepareCloseDocByPledgeeOrByArbiter(args) {
        clog.app.info(`prepare Close ByPledgeeOrByArbiter args: ${utils.stringify(args)}`);
        let msg;
        let pledge = _.has(args, 'pledge') ? args.pledge : null;
        let pledgeHash = _.has(args, 'pledgeHash') ? args.pledgeHash : null;
        let unlockerIndex = _.has(args, 'unlockerIndex') ? args.unlockerIndex : 0;
        let concluder = _.has(args, 'concluder') ? args.concluder : null;
        let dType = _.has(args, 'dType') ? args.dType : iConsts.DOC_TYPES.ClosePledge;
        let dClass = _.has(args, 'dClass') ? args.dClass : iConsts.PLEDGE_CLOSE_CLASESS.General;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let version = _.has(args, 'version') ? args.version : '0.0.0.';
        let description = _.has(args, 'description') ? args.description : 'Closed because of finish repayments';  // closed because plegee remits remained repayments

        if (pledge == null) {
            // retrieve pledge by hash
            const pPledgeHandler = require('./proposal-pledge-contract');
            pledge = pPledgeHandler.retrievePledgeInfo(pledgeHash).pledge;
        }
        if (utils._nilEmptyFalse(concluder) || ![iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee, iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter].includes(concluder)) {
            msg = `Can not close pledge since missed concluder ${concluder}`;
            return { err: true, msg }
        }

        let unlockSet, isValidUnlock, signMsg;
        let theSigner;
        switch (concluder) {
            case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee:
                theSigner = pledge.pledgee;
                break;

            case iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter:
                theSigner = pledge.arbiter;
                break;

            case iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger:
                theSigner = pledge.pledger;
                break;

        }

        if (utils._nilEmptyFalse(theSigner) || !crypto.bech32_isValidAddress(theSigner)) {
            msg = `Can not close pledge since missed Pledgee ${theSigner}`;
            return { err: true, msg }
        }

        let addrDtl = walletAddressHandler.getAddressesInfoSync([theSigner]);
        if (addrDtl.length == 0)
            return { err: true, msg: `the conclude-signer address ${iutils.shortBech(theSigner)} is not controlled by wallet` };

        addrDtl = utils.parse(addrDtl[0].waDetail);
        unlockSet = addrDtl.uSets[unlockerIndex]

        let closeContract = _.clone(closePledgeTpl);
        closeContract.ref = pledge.hash;
        closeContract.closeMsg = description;
        closeContract.dType = dType;
        closeContract.dClass = dClass;
        closeContract.dVer = version;
        closeContract.concludeType = concluder;
        closeContract.concluder = theSigner;
        closeContract.creationDate = creationDate;
        signMsg = this.getSignMsgDForCloseContract(closeContract)

        let signatures = [];
        closeContract.dExtInfo = { uSet: unlockSet };
        let permitedToUnPledge = iConsts.CONSTS.NO;
        for (let inx = 0; inx < unlockSet.sSets.length; inx++) {
            if (unlockSet.sSets[inx].pPledge == iConsts.CONSTS.YES)
                permitedToUnPledge = iConsts.CONSTS.YES;
            let signature = crypto.signMsg(signMsg, addrDtl.privContainer[unlockSet.salt][inx]);
            signatures.push(signature);
        }
        if (permitedToUnPledge != iConsts.CONSTS.YES) {
            msg = `Close contract, the signer(${concluder}) of close to pledgeContract(${utils.hash6c(pledgeHash)}) (${theSigner}) has not permited to UnPledgeing`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        if (signatures.length == 0) {
            msg = `The closeContract couldn't be signed`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        closeContract.dExtInfo.signatures = signatures;
        closeContract.dExtHash = iutils.doHashObject({
            signatures: closeContract.dExtInfo.signatures,
            uSet: closeContract.dExtInfo.uSet
        });
        closeContract.dLen = iutils.paddingDocLength(utils.stringify(closeContract).length);
        closeContract.hash = this.calcHashDPledgeClose(closeContract);
        return closeContract
    }

    static getSignMsgDForCloseContract(closeContract) {
        // alphabetical order
        let signables = {
            concluder: closeContract.concluder,
            creationDate: closeContract.creationDate,
            description: closeContract.description, //'Closed because of finishe repayments'// closed because plegee remits remained repayments
            dType: closeContract.dType,
            dVer: closeContract.dVer,
            ref: closeContract.ref
        }
        clog.app.info(`pledgee contract to close signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static calcHashDPledgeClose(closeContract) {
        // alphabetical order
        let hashables = {
            dLen: closeContract.dLen,
            dExtHash: closeContract.dExtHash,
        };
        let hash = iutils.doHashObject(hashables);
        return hash;
    }

    static validatePledgeeClosesPledge(args) {
        let msg;
        let pledgeHash = args.pledgeHash;
        let signatures = args.signatures;

        let { pledge, dExtInfo } = this.retrievePledgeInfo(pledgeHash)

        let unlockSet, isValidUnlock, signMsg;
        unlockSet = dExtInfo.pledgeeUSet;
        isValidUnlock = mOfNHandler.validateSigStruct({
            address: pledge.pledgee,
            uSet: unlockSet
        });
        if (isValidUnlock != true) {
            msg = `pledgee can not close contract, Invalid! given pledgee unlock structure for close req, unlock pledgeContract(${utils.hash6c(pledgeHash)}): ${utils.stringify({
                address: pledge.pledgee,
                sSets: unlockSet.sSets,
                lHash: unlockSet.lHash,
                proofs: unlockSet.proofs,
                salt: unlockSet.salt
            })}`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        // pledgee signature & permission validate check
        signMsg = this.getSignMsgDForCloseContract(aPledge);
        let permitedToPledge = iConsts.CONSTS.NO;
        for (let signInx = 0; signInx < dExtInfo.pledgeeSignatures.length; signInx++) {
            let aSignature = dExtInfo.pledgeeSignatures[signInx];
            try {
                let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                if (!verifyRes) {
                    msg = `Close contract, the Pledge to pledgeepledgeContract(${utils.hash6c(pledgeHash)}) (${pledge.pledgee}) has invalid pledgee signature`;
                    return { err: true, msg }
                }
            } catch (e) {
                msg = `error in verify pledgee Signature pledgeContract(${utils.hash6c(pledgeHash)}): ${utils.stringify(args)}`;
                clog.trx.error(msg);
                clog.trx.error(e);
                return { err: true, msg };
            }
            if (unlockSet.sSets[signInx].pPledge == iConsts.CONSTS.YES)
                permitedToPledge = iConsts.CONSTS.YES;

        }
        if (permitedToPledge != iConsts.CONSTS.YES) {
            msg = `Close contract, the signer of close to pledgeContract(${utils.hash6c(pledgeHash)}) (${pledge.pledgee}) has not permited to UnPledgeing`;
            clog.sec.error(msg);
            return { err: true, msg: msg }
        }

        // // controll pledgee signature
        // unlockSet = dExtInfo.pledgeeUSet;
        // if (isValidUnlock != true) {
        //     msg = `Block creating, Invalid! given unlock pledgee structure for pledge req, unlock:${utils.stringify({
        //         address: pledge.pledgee,
        //         sSets: unlockSet.sSets,
        //         lHash: unlockSet.lHash,
        //         proofs: unlockSet.proofs,
        //         salt: unlockSet.salt
        //     })}`;
        //     clog.sec.error(msg);
        //     return { err: true, msg }
        // }
        // // pledger signature
        // signMsg = pledgeHandler.getSignMsgAsPledgee(aPledge);
        // for (let signInx = 0; signInx < dExtInfo.pledgeeSignatures.length; signInx++) {
        //     let aSignature = dExtInfo.pledgeeSignatures[signInx];
        //     try {
        //         let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
        //         if (!verifyRes) {
        //             msg = `Block creating, the Pledge to pledgee(${pledge.pledgee}) has invalid pledgee signature`;
        //             return { err: true, msg }
        //         }
        //     } catch (e) {
        //         msg = `error in verify pledgee Signature: ${utils.stringify(args)}`;
        //         clog.trx.error(msg);
        //         clog.trx.error(e);
        //         return { err: true, msg };
        //     }
        // }


    }

    static doApplyClosingPledge(args) {
        /**
         * NOTE: if pgd_status=Open then pgd_close_date is a indicating date
         *       if pgd_status=Close then pgd_close_date is a definitive close date
         */
        clog.app.info(`do Apply PledgeClosing args: ${utils.stringify(args)}`);
        // change pledge status in table i_pledged_accounts
        let realCloseDate = iutils.getACycleRange({
            cDate: args.blockCreationDate
        }).minCreationDate;

        model.sUpdate({
            table: tableDAG,
            query: [
                ['pgd_hash', args.pgdHash]
            ],
            updates: {
                pgd_status: iConsts.CONSTS.CLOSE,
                pgd_close_date: realCloseDate
            }
        });
        return { err: false }
    }

    static reOpenPledgeBecauseOfPaymentsFail(args) {
        clog.app.info(`do Apply reopen PledgeClosing args: ${utils.stringify(args)}`);
        // change pledge status in table i_pledged_accounts
        model.sUpdate({
            table: tableDAG,
            query: [
                ['pgd_hash', args.pgdHash]
            ],
            updates: { pgd_status: iConsts.CONSTS.OPEN, pgd_close_date: '' }
        });
        return { err: false }
    }

}


module.exports = PledgeCloser;
