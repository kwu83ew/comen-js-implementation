const _ = require('lodash');
const iConsts = require('../../config/constants');
const cnfHandler = require('../../config/conf-params');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const mOfNHandler = require('../../transaction/signature-structure-handler/general-m-of-n-handler');
const machine = require('../../machine/machine-handler');
const docTmpHandler = require('../../services/tmp/tmp-handler');
const proposalHandler = require('../../dna/proposal-handler/proposal-handler');
const extinfosHandler = require('../../dag/extinfos-handler');
const walletHandlerLocalUTXOs = require('../../web-interface/wallet/wallet-handler-local-utxos');
const pldegeHandler = require('./pledge-handler');


let loanHandler = null;

const tableDraft = 'i_machine_draft_pledges';
const tableOnchainPledgedAccounts = 'i_pledged_accounts';



class ProposalPledgeHandler {



    static insertAPledge(args) {

        clog.app.info(`create A Pledge args: ${utils.stringify(args)}`);


        let block = args.block;
        let pledge = args.pledge;
        let realActivateDate = iutils.getACycleRange({
            cDate: block.creationDate,
            // forwardByCycle: iConsts.PLDEGE_ACTIVATE_OR_DEACTIVATE_MATURATION_CYCLE_COUNT
        }).minCreationDate;
        let values = {
            pgd_hash: pledge.hash,
            pgd_type: pledge.dType,
            pgd_class: pledge.dClass,
            pgd_version: pledge.dVer,
            pgd_pledger_sign_date: pledge.pledgerSignDate,
            pgd_pledgee_sign_date: pledge.pledgeeSignDate,
            pgd_arbiter_sign_date: pledge.arbiterSignDate,
            pgd_activate_date: realActivateDate,
            pgd_pledger: pledge.pledger,
            pgd_pledgee: pledge.pledgee,
            pgd_arbiter: pledge.arbiter,
            pgd_principal: pledge.redeemTerms.principal,
            pgd_annual_interest: pledge.redeemTerms.annualInterest,
            pgd_repayment_offset: pledge.redeemTerms.repaymentOffset,
            pgd_repayment_amount: pledge.redeemTerms.repaymentAmount,
            pgd_repayment_schedule: pledge.redeemTerms.repaymentSchedule,
            pgd_status: iConsts.CONSTS.OPEN // by default by inserting a pledge is initialy open and active 
        };
        clog.app.info(`inserting new onchain pledge ${utils.stringify(values)}`);
        model.sCreate({
            table: tableOnchainPledgedAccounts,
            values
        });
        return { err: false }
    }

    /**
     * 
     * @param {*} pledgeHash 
     * if pleger/plegee does not pay the costs by a valid transaction, so the pledge is not valid!
     * even proposal costs are payed properly?
     * 
     */
    static removePledgeBecauseOfPaymentsFail(pledgeHash) {
        let msg;
        //sceptical test
        let exist = model.sRead({
            table: tableOnchainPledgedAccounts,
            query: [['pgd_hash', pledgeHash]]
        });
        if (exist.length != 1) {
            msg = `Try to delete pledge strange result! ${utils.stringify(exist)}`;
            clog.sec.err(msg);
            return { err: true, msg }
        }

        model.sDelete({
            table: tableOnchainPledgedAccounts,
            query: [['pgd_hash', pledgeHash]]
        });
        return { err: false }
    }


    static retrievePledgeInfo(pledgeHash) {
        const dagHandler = require('../../dag/graph-handler/dag-handler');
        let msg;
        let pledge = pldegeHandler.searchInPledgedAccounts({
            query: [['pgd_hash', pledgeHash]]
        });
        pledge = pledge[0];

        // let blockExtInfos = extinfosHandler.get BlockExtInfos(pledge.pgdBlockHash);
        let bExtInfoRes = extinfosHandler.getBlockExtInfos(pledge.pgdBlockHash);
        if (bExtInfoRes.bExtInfo == null) {
            msg = `missed bExtInfo2 (${utils.hash16c(pledge.pgdBlockHash)})`;
            clog.sec.error(msg);
            return { err: true, msg }
        }
        let blockExtInfos = bExtInfoRes.bExtInfo

        let { blockHashes, mapDocToBlock } = dagHandler.getBlockHashesByDocHashes({ docsHashes: [pledgeHash] });
        let docInx = null;
        for (let aBlk of mapDocToBlock[pledgeHash]) {
            if (aBlk.blockHash == pledge.pgdBlockHash) {
                docInx = aBlk.docIndex;
            }
        }
        if (docInx == null) {
            msg = `Can not pledgee ClosesPledge simce pledgeHash(${utils.hash6c(pledgeHash)}) is not registered in DAG!`;
            clog.app.error(msg);
            return { err: true, msg };
        }

        let dExtInfo = blockExtInfos[docInx];

        return { pledge, dExtInfo };
    }

    static extractHashableParts(pledge) {
        // as always ordering properties by alphabet
        let hashables = {
            creationDate: pledge.creationDate,
            arbiter: pledge.arbiter,
            arbiterSignDate: pledge.arbiterSignDate,
            description: pledge.description,
            dClass: pledge.dClass,
            dLen: pledge.dLen,
            dType: pledge.dType,
            dExtHash: pledge.dExtHash,
            pledgee: pledge.pledgee,
            pledgeeSignDate: pledge.pledgeeSignDate,
            pledger: pledge.pledger,
            pledgerSignDate: pledge.pledgerSignDate,
            redeemTerms: {
                annualInterest: pledge.redeemTerms.annualInterest,
                principal: pledge.redeemTerms.principal,
                repaymentAmount: pledge.redeemTerms.repaymentAmount,
                repaymentOffset: pledge.redeemTerms.repaymentOffset,
                repaymentSchedule: pledge.redeemTerms.repaymentSchedule
            },
            dVer: pledge.dVer,
        }
        if (_.has(pledge, 'proposalRef'))
            hashables.proposalRef = pledge.proposalRef;
        return hashables;
    }

    static calcHashDPledge(pledge) {
        let hashables = this.extractHashableParts(pledge);
        return iutils.doHashObject(hashables);
    }

    static calcExtRoot(pledge) {
        return iutils.doHashObject(pledge.dExtInfo);
    }


    static doPledgeAddress(args) {
        clog.app.info(`do PledgeAddress args: ${utils.stringify(args)}`)
        let msg;
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        let pledgerSignDate = _.has(args, 'pledgerSignDate') ? args.pledgerSignDate : utils.getNow();

        let pledger = _.has(args, 'pledger') ? args.pledger : null;
        if (utils._nilEmptyFalse(pledger)) {
            msg = `The pledger ${pledger} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let pledgee = _.has(args, 'pledgee') ? args.pledgee : null;
        if (utils._nilEmptyFalse(pledgee)) {
            msg = `The pledgee ${pledgee} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let arbiter = _.has(args, 'arbiter') ? args.arbiter : "";

        let dType = _.has(args, 'dType') ? args.dType : null;
        let dClass = _.has(args, 'dClass') ? args.dClass : null;
        let proposalRef = _.has(args, 'proposalRef') ? args.proposalRef : null;
        if ((dClass == iConsts.PLEDGE_CLASSES.PledgeP) && utils._nilEmptyFalse(proposalRef)) {
            msg = `The proposalRef ${proposalRef} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // the PAIs are loaned
        let principal = _.has(args, 'principal') ? args.principal : null;
        if (utils._nilEmptyFalse(principal) || !(parseInt(principal) > 0)) {
            msg = `The principal ${principal} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let annualInterest = _.has(args, 'annualInterest') ? args.annualInterest : null;
        if (utils._nilEmptyFalse(annualInterest) || !(parseInt(annualInterest) >= 0)) {
            msg = `The annualInterest ${annualInterest} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // the amount is cutting from income and payed to Pledgee
        let repaymentAmount = _.has(args, 'repaymentAmount') ? args.repaymentAmount : null;
        if (utils._nilEmptyFalse(repaymentAmount) || !(parseInt(repaymentAmount) > 0)) {
            msg = `The Repayment Amount ${repaymentAmount} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // the amount is cutting from income and payed to Pledgee
        let repaymentsNumber = _.has(args, 'repaymentsNumber') ? args.repaymentsNumber : null;
        if (utils._nilEmptyFalse(repaymentsNumber) || !(parseInt(repaymentsNumber) > 0)) {
            msg = `The Repayments Number ${repaymentsNumber} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // how many times do repayment in a year
        let repaymentSchedule = _.has(args, 'repaymentSchedule') ? args.repaymentSchedule : iConsts.DEFAULT_REPAYMENT_SCHEDULE;
        if (utils._nilEmptyFalse(repaymentSchedule) || !(parseInt(repaymentSchedule) >= 0)) {
            msg = `The repaymentSchedule ${repaymentSchedule} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // starting to pay the first repayment after n hours
        // how many minutes is offset. in other word, the first repayment can started after x minutes from approving pledge and giving loan
        // TODO: offset bigger than zero must be implemented 
        let repaymentOffset = _.has(args, 'repaymentOffset') ? args.repaymentOffset : iConsts.DEFAULT_REPAYMENT_SCHEDULE;
        console.log('repaymentOffset', repaymentOffset);
        console.log('repaymentOffset', repaymentOffset);
        console.log('repaymentOffset', repaymentOffset);
        console.log('(parseInt(repaymentOffset) > 0)', (parseInt(repaymentOffset) > 0));
        console.log('utils._nilEmptyFalse(repaymentOffset)', utils._nilEmptyFalse(repaymentOffset));
        if (utils._empty(repaymentOffset) || (parseInt(repaymentOffset) > 0)) {
            msg = `The repaymentOffset ${repaymentOffset} is invalid!`;
            clog.app.error(msg);
            return { err: true, msg }
        }


        // validating interest and repayments number and principal are in equation?
        if (loanHandler == null)
            loanHandler = require('../loan-contract/loan-contract');
        let loanDtl = loanHandler.calcLoanRepayments({
            principal,
            annualInterest,
            repaymentAmount,
            repaymentSchedule
        });
        if (loanDtl.err != false) {
            return loanDtl;
        }


        let pledgeContract = pldegeHandler.getPledgeTpl();
        pledgeContract.creationDate = creationDate;
        pledgeContract.dType = dType;
        pledgeContract.dClass = dClass;
        pledgeContract.pledgerSignDate = pledgerSignDate;
        pledgeContract.pledger = pledger;
        pledgeContract.pledgee = pledgee;
        pledgeContract.arbiter = arbiter;
        pledgeContract.proposalRef = proposalRef;
        pledgeContract.redeemTerms = {
            principal,
            annualInterest,
            repaymentOffset,
            repaymentAmount,
            repaymentSchedule,
            repaymentsNumber
        }
        let pledgerSignRes = this.pledgerSignsPledge({ pledgeContract });
        if (pledgerSignRes.err != false) {
            return pledgerSignRes;
        }
        let signedPledge = pledgerSignRes.pledgeContract;
        signedPledge.dLen = iutils.paddingDocLength(utils.stringify(signedPledge).length);
        clog.app.info(`signedPledge: ${utils.stringify(signedPledge)}`);

        return { err: false, signedPledge }
    }

    static pledgerSignsPledge(args) {
        let msg;
        let pledgeContract = _.has(args, 'pledgeContract') ? args.pledgeContract : null;
        let pledger = _.has(pledgeContract, 'pledger') ? pledgeContract.pledger : null;
        if (utils._nilEmptyFalse(pledger)) {
            msg = `The pledger ${pledger} is null`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(pledger)) {
            msg = `The pledger ${pledger} is not a valid Bech32 address!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let pledgerAddInfo = walletAddressHandler.getAddressesInfoSync([pledger]);
        if (pledgerAddInfo.length != 1) {
            msg = `The Invoice Address ${lrAccountAddress} is not controlled by your wallet!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let addrDtl = utils.parse(pledgerAddInfo[0].waDetail);
        let signatures = [];
        for (let anUnlockSet of addrDtl.uSets) {
            // if already signed exit
            if (signatures.length > 0)
                continue;
            console.log('anUnlockSet', anUnlockSet);
            console.log('sSets', anUnlockSet.sSets);
            let pPledge = iConsts.CONSTS.NO;
            for (let aSign of anUnlockSet.sSets) {
                if (aSign.pPledge == iConsts.CONSTS.YES)
                    pPledge = iConsts.CONSTS.YES;
            }
            if (pPledge == iConsts.CONSTS.YES) {
                pledgeContract.dExtInfo.pledgerUSet = anUnlockSet;
                let signMsg = this.getSignMsgAsPledger({ pledge: pledgeContract, dExtInfo: pledgeContract.dExtInfo });
                for (let inx = 0; inx < anUnlockSet.sSets.length; inx++) {
                    let signature = crypto.signMsg(signMsg, addrDtl.privContainer[anUnlockSet.salt][inx]);
                    console.log('signature', signature);
                    signatures.push(signature);
                }
            }
        }
        if (signatures.length == 0) {
            msg = `The pledgerSignsPledge couldn't sign it`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        pledgeContract.dExtInfo.pledgerSignatures = signatures;
        clog.app.info(`pledgeContract: ${utils.stringify(pledgeContract)}`);

        return { err: false, pledgeContract }
    }

    static getSignMsgAsPledger(args) {
        let signables = this.extractSignablePartsForPledger(args);
        clog.app.info(`pledger contract signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static getSignMsgAsPledgee(args) {
        let signables = this.extractSignablePartsForPledgee(args);
        clog.app.info(`pledgee contract signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static getSignMsgAsArbiter(args) {
        let signables = this.extractSignablePartsForArbiter(args);
        clog.app.info(`arbiter contract signables: ${utils.stringify(signables)}`);
        let hash = iutils.doHashObject(signables);
        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
        return signMsg;
    }

    static extractSignablePartsForArbiter(args) {
        // as always ordering properties by alphabet
        let dExtInfo = args.dExtInfo;
        let signable = this.extractSignablePartsForPledgee(args);
        signable.pledgeeSignatures = dExtInfo.pledgeeSignatures;
        signable.arbiterSignDate = dExtInfo.arbiterSignDate;
        signable.arbiterUSet = dExtInfo.arbiterUSet;
        return signable;
    }

    static extractSignablePartsForPledgee(args) {
        // as always ordering properties by alphabet
        let dExtInfo = args.dExtInfo;
        let signable = this.extractSignablePartsForPledger(args);
        signable.pledgerSignatures = dExtInfo.pledgerSignatures;
        signable.pledgeeSignDate = dExtInfo.pledgeeSignDate;
        signable.pledgeeUSet = dExtInfo.pledgeeUSet;
        signable.trx = args.pledge.trx;
        return signable;
    }

    static extractSignablePartsForPledger(args) {
        let pledge = args.pledge;
        let dExtInfo = args.dExtInfo;
        // as always ordering properties by alphabet
        let signable = {
            arbiter: pledge.arbiter,
            dClass: pledge.dClass,
            dType: pledge.dType,
            pledgee: pledge.pledgee,
            pledger: pledge.pledger,
            pledgerSignDate: pledge.pledgerSignDate,
            pledgerUSet: dExtInfo.pledgerUSet,
            redeemTerms: {
                annualInterest: pledge.redeemTerms.annualInterest,
                principal: pledge.redeemTerms.principal,
                repaymentAmount: pledge.redeemTerms.repaymentAmount,
                repaymentOffset: pledge.redeemTerms.repaymentOffset,
                repaymentSchedule: pledge.redeemTerms.repaymentSchedule
            }
        }
        if (pledge.dType == iConsts.PLEDGE_CLASSES.PledgeP)
            signable.proposalRef = pledge.proposalRef;

        return signable;
    }

    static savepldgDraft(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let draft = args.draft;
        let values = {
            dpl_mp_code: mpCode,
            dpl_type: draft.dType,
            dpl_class: draft.dClass,
            dpl_version: draft.dVer,
            dpl_descriptions: draft.description,
            dpl_pledger: draft.pledger,
            dpl_pledgee: draft.pledgee,
            dpl_arbiter: draft.arbiter,
            dpl_proposal_ref: draft.proposalRef,
            dpl_body: utils.stringify(draft),
            dpl_req_date: draft.pledgerSignDate,
        }
        model.sCreate({
            table: tableDraft,
            values
        })
    }

    static removeDraftPledge(args) {
        let query = _.has(args, 'query') ? args.query : [];
        if (_.has(args, 'dpl_proposal_ref'))
            query.push(['dpl_proposal_ref', ['IN', args.dpl_proposal_ref]]);

        if (_.has(args, 'dpl_pledger'))
            query.push(['dpl_pledger', ['IN', args.dpl_pledger]]);

        if (query.length == 0)
            return;

        let res = model.sDelete({
            table: tableDraft,
            query
        });

        return res;
    }

    static calcPldgeDocumentCost(args) {
        console.log(`------calc Pldge Document Cost args `, args);
        let pledge = args.pledge;
        let repaymentsNumber = args.repaymentsNumber;
        let cDate = args.cDate;

        let dLen = parseInt(pledge.dLen);

        let theCost =
            dLen *
            cnfHandler.getBasePricePerChar({ cDate }) *
            cnfHandler.getDocExpense({ cDate, dType: pledge.dType, dClass: pledge.dClass, dLen }) *
            repaymentsNumber;

        if (args.stage == iConsts.STAGES.Creating)
            theCost = theCost * machine.getMachineServiceInterests({
                dType: args.dType,
                dClass: args.dClass,
                dLen
            });

        return { err: false, cost: Math.floor(theCost) };
    }

    static handleReceivedProposalLoanRequest(args) {
        let msg;
        let payload = args.payload;
        let sender = payload.sender;
        let proposal = payload.proposal;
        let pledge = payload.pledgerSignedPledge;

        let cdVer = _.has(payload, 'cdVer') ? payload.cdVer : null;
        if (utils._nilEmptyFalse(cdVer) || !iutils.isValidVersionNumber(cdVer)) {
            msg = `missed cdVer gql ${JSON.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }


        // do a bunch of control on pledgeRequest
        let dplValidateRes = this.validatePledgerSignedRequest({ proposal, pledge, stage: iConsts.STAGES.Creating });
        if (dplValidateRes.err != false)
            return dplValidateRes;

        clog.app.info(`handle ReceivedProposalLoanRequest ${utils.stringify(args)}`);
        // push contract to broadcast buffer
        args.creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        args.dType = 'receivedPLR';
        args.dClass = 'receivedPLR';
        args.hash = iutils.doHashObject(args);
        docTmpHandler.insertTmpDocSync({
            doc: args,
            doc_status: 'new'
        });
        return { err: false, shouldPurgeMessage: true }
    }

    static pledgeeSignsProposalLoanRequestBundle(args) {
        let msg;
        clog.app.info(`sign ProposalLoanRequest args: ${utils.stringify(args)} `);
        const walletHandler = require('../../web-interface/wallet/wallet-handler');

        let sender = args.sender;
        let proposal = args.payload.proposal;
        let pledge = args.payload.pledgerSignedPledge;
        // do a bunch of control on pledgeRequest
        let dplValidateRes = this.validatePledgerSignedRequest({ proposal, pledge, stage: iConsts.STAGES.Creating });
        if (dplValidateRes.err != false)
            return dplValidateRes;



        // (START) dummy pledge siging in order to calculate pledge length after signature
        pledge.trx = "0000000000000000000000000000000000000000000000000000000000000000";
        let pledgeeSignRes = this.pledgeeSignsPledge(pledge);
        if (pledgeeSignRes.err != false) {
            return pledgeeSignRes;
        }
        pledge = pledgeeSignRes.pledge;
        pledge.dExtHash = "0000000000000000000000000000000000000000000000000000000000000000";
        pledge.dLen = iutils.paddingDocLength(utils.stringify(pledge).length);
        // (END) dummy pledge siging in order to calculate pledge length after signature


        let pldgeContractCost = this.calcPldgeDocumentCost({
            cDate: utils.getNow(),
            stage: iConsts.STAGES.Creating,
            pledge,
            repaymentsNumber: dplValidateRes.repaymentsNumber
        });
        if (pldgeContractCost.err != false)
            return pldgeContractCost;

        // create a transaction for payment
        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
        let outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_PROPOSAL', pledge.redeemTerms.principal],
            // ['TP_PLEDGE', pldgeContractCost.cost] 
        ];
        let spendables = walletHandler.coinPicker.getSomeCoins({
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(pledge.redeemTerms.principal * 1.2)  // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        let inputs = spendables.selectedCoins;
        let trxNeededArgs = {
            maxDPCost: utils.floor(pledge.redeemTerms.principal * 0.7),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: proposal.hash,
            description: 'Payed(by Pledgee) for applying proposal to Vote process',
            inputs,
            outputs,
        }
        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed proposal cost trx: ', utils.stringify(trxDtl));
        pledge.trx = trxDtl.trx.hash
        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);
        let ProposalPayerTrx = trxDtl.trx;

        pledgeeSignRes = this.pledgeeSignsPledge(pledge);
        if (pledgeeSignRes.err != false) {
            return pledgeeSignRes;
        }
        pledge = pledgeeSignRes.pledge;

        pledge.dExtHash = this.calcExtRoot(pledge);
        if (parseInt(pledge.dLen) != utils.stringify(pledge).length) {
            msg = `Worng pldge contract length(${utils.stringify(pledge).length})calculation: ${utils.stringify(pledge)}`;
            clog.app.info(msg);
            return { err: true, msg };
        }

        pledge.hash = this.calcHashDPledge(pledge);

        console.log('completed pledge contract', utils.stringify(pledge));


        // pay for pledge doc too
        outputs = [
            [changeAddress, 1],  // a new address for change back
            ['TP_PLEDGE', pldgeContractCost.cost]
        ];
        spendables = walletHandler.coinPicker.getSomeCoins({
            excludeRefLocs: utils.objKeys(inputs),  // avoid double spending inputs
            selectionMethod: 'precise',
            minimumSpendable: utils.floor(pldgeContractCost.cost * 2)   // an small portion bigger to support DPCosts
        });
        console.log('spendables: ', spendables);
        if (spendables.err != false)
            return spendables;

        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
            msg = `Wallet couldn't find! proper UTXOs to spend to pay pledge doc costs`;
            clog.app.info(msg);
            return { err: true, msg };
        }
        inputs = spendables.selectedCoins;
        trxNeededArgs = {
            maxDPCost: utils.floor(pldgeContractCost.cost * 2),
            DPCostChangeIndex: 0, // to change back
            dType: iConsts.DOC_TYPES.BasicTx,
            dClass: iConsts.TRX_CLASSES.SimpleTx,
            ref: pledge.hash,
            description: 'Payed(by Pledgee) for PledgeP document cost',
            inputs,
            outputs,
        }
        trxDtl = walletHandler.makeATransaction(trxNeededArgs);
        if (trxDtl.err != false)
            return trxDtl;
        console.log('signed pledge-doc cost trx: ', utils.stringify(trxDtl));
        // mark UTXOs as used in local machine
        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);
        let pledgeDocPayerTrx = trxDtl.trx;

        // push whole Proposal, Pledge & transaction all in a bundle in 
        let bundleProposalPledgeTrx = this.createBundlePPT({
            proposal,
            pledge,
            ProposalPayerTrx,
            pledgeDocPayerTrx
        });
        docTmpHandler.insertTmpDocSync({
            doc: bundleProposalPledgeTrx,
            doc_status: 'new'
        });


        return { err: false, shouldPurgeMessage: true }
    }

    static createBundlePPT(args) {
        // push contract to broadcast buffer
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();

        let bundle = {
            creationDate: creationDate,
            dType: 'BundlePPT',
            dClass: 'Basic',
            proposal: args.proposal,
            pledge: args.pledge,
            ProposalPayerTrx: args.ProposalPayerTrx,
            pledgeDocPayerTrx: args.pledgeDocPayerTrx,
        }
        bundle.hash = iutils.doHashObject(bundle);
        return bundle;
    }

    static pledgeeSignsPledge(pledge) {
        let msg;
        let pledgee = _.has(pledge, 'pledgee') ? pledge.pledgee : null;
        let pledgeeSignDate = _.has(pledge, 'pledgeeSignDate') ? pledge.pledgeeSignDate : utils.getNow();
        if (utils._nilEmptyFalse(pledgeeSignDate))
            pledgeeSignDate = utils.getNow();
        if (utils._nilEmptyFalse(pledgee)) {
            msg = `The pledgee ${pledgee} is null`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(pledgee)) {
            msg = `The pledgee ${pledgee} is not a valid Bech32 address!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        let pledgeeAddInfo = walletAddressHandler.getAddressesInfoSync([pledgee]);
        // console.log('pledgee Add Info', pledgeeAddInfo);


        if (pledgeeAddInfo.length != 1) {
            msg = `The pledgee Address(${iutils.shortBech8(pledgee)}) is not controlled by your wallet!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let addrDtl = utils.parse(pledgeeAddInfo[0].waDetail);
        let signatures = [];
        for (let anUnlockSet of addrDtl.uSets) {
            // if already signed exit
            if (signatures.length > 0)
                continue;

            console.log('anUnlockSet', anUnlockSet);
            console.log('sSets', anUnlockSet.sSets);
            pledge.dExtInfo.pledgeeUSet = anUnlockSet;
            let signMsg = this.getSignMsgAsPledgee({ pledge, dExtInfo: pledge.dExtInfo });
            for (let inx = 0; inx < anUnlockSet.sSets.length; inx++) {
                let signature = crypto.signMsg(signMsg, addrDtl.privContainer[anUnlockSet.salt][inx]);
                // console.log('the pledgee signature', signature);
                signatures.push(signature);
            }
        }
        if (signatures.length == 0) {
            msg = `The pledgee SignsPledge couldn't sign it`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        pledge.dExtInfo.pledgeeSignatures = signatures;
        pledge.pledgeeSignDate = pledgeeSignDate;
        clog.app.info(`THe pledgee-signed pledge Contract: ${utils.stringify(pledge)}`);

        return { err: false, pledge }
    }

    static arbiterSignsPledge(pledge) {
        let msg;
        let arbiter = _.has(pledge, 'arbiter') ? pledge.arbiter : null;
        let arbiterSignDate = _.has(pledge, 'arbiterSignDate') ? pledge.arbiterSignDate : utils.getNow();
        if (utils._nilEmptyFalse(arbiterSignDate))
            arbiterSignDate = utils.getNow();
        if (utils._nilEmptyFalse(arbiter)) {
            msg = `The arbiter ${arbiter} is null`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        if (!crypto.bech32_isValidAddress(arbiter)) {
            msg = `The arbiter ${arbiter} is not a valid Bech32 address!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }

        // TODO: not necessarily the arbiter private info exist in local machine. change it to send pledge to arbiter(s) (probably via iNames) 
        // and get back signed pledge-contract
        let arbiterAddInfo = walletAddressHandler.getAddressesInfoSync([arbiter]);
        console.log('arbiterAddInfo', arbiterAddInfo);


        if (arbiterAddInfo.length != 1) {
            msg = `The arbiter Address(${iutils.shortBech8(arbiter)}) is not controlled by your wallet!!`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let addrDtl = utils.parse(arbiterAddInfo[0].waDetail);
        let signatures = [];
        for (let anUnlockSet of addrDtl.uSets) {
            // if already signed exit
            if (signatures.length > 0)
                continue;

            console.log('anUnlockSet', anUnlockSet);
            console.log('sSets', anUnlockSet.sSets);
            pledge.dExtInfo.arbiterUSet = anUnlockSet;
            let signMsg = this.getSignMsgAsArbiter({ pledge, dExtInfo: pledge.dExtInfo });
            for (let inx = 0; inx < anUnlockSet.sSets.length; inx++) {
                let signature = crypto.signMsg(signMsg, addrDtl.privContainer[anUnlockSet.salt][inx]);
                console.log('signature', signature);
                signatures.push(signature);
            }
        }
        if (signatures.length == 0) {
            msg = `The arbiter SignsPledge couldn't sign it`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        pledge.dExtInfo.arbiterSignatures = signatures;
        pledge.arbiterSignDate = arbiterSignDate;
        clog.app.info(`THe arbiter-signed pledge Contract: ${utils.stringify(pledge)}`);

        return { err: false, pledge }
    }

    static validatePledgerSignedRequest(args) {
        let msg;
        let blockCreationDate = args.blockCreationDate;
        let proposal = args.proposal;
        let pledge = args.pledge;
        let stage = args.stage;
        let dExtInfo;
        if (_.has(pledge, 'dExtInfo')) {
            dExtInfo = pledge.dExtInfo;
        } else if (_.has(args, 'dExtInfo')) {
            dExtInfo = args.dExtInfo;
        }

        // does truly referenced the proposal
        if (proposal.hash != pledge.proposalRef) {
            msg = `The proposal(${utils.hash6c(proposal.hash)}) is not the Refered one in pledge(${utils.hash6c(pledge.proposalRef)}) req`
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // pledger singature structure check
        let unlockSet = dExtInfo.pledgerUSet;
        let isValidUnlock = mOfNHandler.validateSigStruct({
            address: pledge.pledger,
            uSet: unlockSet
        });
        if (isValidUnlock != true) {
            msg = `Invalid! given unlock structure for pledge req, unlock:${utils.stringify({
                address: pledge.pledger,
                unlockSet
            })}`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        // pledger signature & permission validate check
        let signMsg = this.getSignMsgAsPledger({ pledge, dExtInfo: dExtInfo });
        let permitedToPledge = iConsts.CONSTS.NO;
        for (let signInx = 0; signInx < dExtInfo.pledgerSignatures.length; signInx++) {
            let aSignature = dExtInfo.pledgerSignatures[signInx];
            try {
                let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
                if (!verifyRes) {
                    msg = `The Pledge to proposal(${proposal.hash}) has invalid signature`;
                    return { err: true, msg, shouldPurgeMessage: true }
                }
            } catch (e) {
                clog.trx.error(`error in verify pledge Signature: ${utils.stringify(args)}`);
                clog.trx.error(e);
                return false;
            }
            if (unlockSet.sSets[signInx].pPledge == iConsts.CONSTS.YES)
                permitedToPledge = iConsts.CONSTS.YES;
        }
        if (permitedToPledge != iConsts.CONSTS.YES) {
            msg = `The pledger of Pledge to proposal(${proposal.hash}) has not permited to pledge`;
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // control proposal cost
        let proposalDocumetnCost = proposalHandler.calcProposalDocumetnCost({
            cDate: pledge.creationDate,
            proposal,
            stage
        });
        if (proposalDocumetnCost.err != false)
            return proposalDocumetnCost;

        let { oneCycleIncome, applyCost } = proposalHandler.calcProposalApplyCost({
            cCDate: blockCreationDate, // the creation date of the block in which contribute is recorded (start date)
            helpHours: proposal.helpHours,
            helpLevel: proposal.helpLevel
        });
        if (pledge.redeemTerms.principal < proposalDocumetnCost.cost + applyCost) {
            msg = `Proposal costs(${applyCost}) is more than requested loan(${pledge.redeemTerms.principal})!`;
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }
        if (pledge.redeemTerms.repaymentAmount > oneCycleIncome) {
            msg = `Repayment value(${utils.microPAIToPAI6(pledge.redeemTerms.repaymentAmount)}) is bigger than one cycle income(${utils.microPAIToPAI6(oneCycleIncome)})!`;
            clog.sec.error(msg);
            return { err: true, msg: msg, shouldPurgeMessage: true }
        }

        // redeem terms control
        if (loanHandler == null)
            loanHandler = require('../loan-contract/loan-contract');
        let loanDtl = loanHandler.calcLoanRepayments({
            principal: pledge.redeemTerms.principal,
            annualInterest: pledge.redeemTerms.annualInterest,
            repaymentAmount: pledge.redeemTerms.repaymentAmount,
            repaymentSchedule: pledge.redeemTerms.repaymentSchedule,
        });
        if (loanDtl.err != false) {
            loanDtl.shouldPurgeMessage = true;
            return loanDtl;
        }

        // so it seems a valid pledge request
        return { err: false, repaymentsNumber: loanDtl.repaymentsNumber }

    }



}

module.exports = ProposalPledgeHandler;
