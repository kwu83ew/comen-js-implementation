const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
const machine = require('../../machine/machine-handler');

const tableDraft = 'i_machine_draft_pledges';
const tableOnchainPledgedAccounts = 'i_pledged_accounts';



let pledgeStructure = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dLen: "0000000",
    dType: iConsts.DOC_TYPES.Pledge,
    dClass: iConsts.PLEDGE_CLASSES.PledgeP,
    dVer: '0.0.2',
    description: '',
    creationDate: '',
    pledgerSignDate: '',  // pledge Request Date
    pledgeeSignDate: '',  // pledge Accept Date
    arbiterSignDate: '',  // arbiter Accept Date(if exist)

    // bech32 address which is pledged  (give as security on a loan.)
    pledger: '',

    // bech32 address of pledgee    (a person to whom a pledge is given.)
    pledgee: '',

    // bech32 address of arbiter(s). the arbiter has same right of pledgee
    arbiter: '',

    // these references are dependent of dClass
    // proposalRef: '', // reference of loan/lend/...

    redeemTerms: {
        principal: 0,    // the PAIs are loaned
        annualInterest: 0,
        repaymentOffset: 0, // starting to pay the first repayment after n hours
        repaymentAmount: 0, // the amount is cutting from income and payed to Pledgee
        repaymentSchedule: 365 * 2,   // how many times do repayment in a year
    },
    dExtInfo: {
        // used for 2 reason, 
        // 1. proving awnership and perission to pledge the account 
        // 2. proving acceptance of pledge clauses
        // 3. proving the pledger accept arbiter(s)
        pledgerSignatures: '',
        pledgerUSet: {},

        // used for 2 reason. 
        // 1. proving ownership of account in which the funds will go
        // 2. proving acceptance of unpledge clauses(which is same as pledge clauses)
        // 3. proving the pledgee accept arbiter(s)
        pledgeeSignatures: '',
        pledgeeUSet: {},

        // used for 2 reason. 
        // 1. proving ownership of account which make decision
        // 2. proving acceptance of unpledge clauses(which is same as pledge clauses)
        // 3. proving arbiter(s) know pledger, pledge and redeem terms
        arbiterSignatures: '',
        arbiterUSet: {}
    }
};

class GeneralPledgeHandler {

    static getPledgeTpl() {
        return _.clone(pledgeStructure);
    }

    static recognizeSignerTypeInfo(args) {
        let signerAddress = _.has(args, 'signerAddress') ? args.signerAddress : null;
        let pledge = args.pledge;
        let byType = null;
        let signerType = null;
        clog.app.info(`recognize SignerTypeInfo ${[pledge.pledger, pledge.pledgee, pledge.arbiter]}`);

        let addDict = {};
        if (signerAddress == null) {
            let addInfo = walletAddressHandler.getAddressesInfoSync([pledge.pledger, pledge.pledgee, pledge.arbiter]);
            if (addInfo.length == 0)
                return { err: true, masg: `non of these 3 address not controlled by machine wallet ${[pledge.pledger, pledge.pledgee, pledge.arbiter]} ` }    // non of these 3 address not controlled by machine wallet

            for (let anAdd of addInfo) {
                addDict[anAdd.waAddress] = anAdd;
            }
        } else {
            addDict[signerAddress] = true;
        }

        if (_.has(addDict, pledge.pledgee)) {
            byType = iConsts.PLEDGE_CONCLUDER_TYPES.ByPledgee;
            signerType = 'pledgee';

        } else if (_.has(addDict, pledge.arbiter)) {
            byType = iConsts.PLEDGE_CONCLUDER_TYPES.ByArbiter;
            signerType = 'arbiter';

        } else if (_.has(addDict, pledge.pledger)) {
            byType = iConsts.PLEDGE_CONCLUDER_TYPES.ByPledger;
            signerType = 'pledger';
        }
        if ((signerType == null) || (byType == null))
            return { err: true, msg: `either signerType(${signerType}) or byType(${byType}) is invalid` };

        return { err: false, signerType, byType };
    }

    static searchInPledgedAccounts(args) {
        let query = _.has(args, 'query') ? args.query : [];

        let res = model.sRead({
            table: tableOnchainPledgedAccounts,
            query
        });
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFieldsPledgedAccounts(x));
        return res;
    }

    static searchInDraftPledges(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'dpl_mp_code'))
            query.push(['dpl_mp_code', mpCode]);

        if (_.has(args, 'dpl_proposal_ref'))
            query.push(['dpl_proposal_ref', ['IN', args.dpl_proposal_ref]]);

        if (_.has(args, 'dpl_pledger'))
            query.push(['dpl_pledger', ['IN', args.dpl_pledger]]);

        let res = model.sRead({
            table: tableDraft,
            query
        });
        if (!res || res.length == 0)
            return [];

        res = res.map(x => this.convertFieldsDraft(x));
        return res;
    }

    /**
     * 
     */
    static getPledgedAccounts(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let onlyActives = _.has(args, 'onlyActives') ? args.onlyActives : false;

        // retriev activated befor 2 last cycle
        cDate = iutils.getCbUTXOsDateRange(cDate).maxCreationDate;

        /**
         * 
         *  effective date |<----- 12Hours ---->|  evaluating point  |
         *                .|                    |  .                 |        
         *  pledge windows |                    |  .                 |         
         *                 |                    |                    |        Active Pledges
         *  <+++++++++++++.|++                  |  .                 |        Open  (unknown end date)
         *  <+++++++++++++.|++++++++++++++++++++|++.++++             |        Open (indicating end date)
         *  <+++++++++++++.|++>                 |  .                 |        Closed (definit end date)
         *                 |                    |                    |
         *                 |                    |                    |        Inctive Pledges
         *                .|<+++++++++++        |  .                 |        Open (unknown end date)
         *                .| <++++++++++>       |  .                 |        Open (indicating end date)
         *  <++++++++++++>.|                    |  .                 |        Closed (definit end date)
         *  <+++++++++++> .|                    |  .                 |        Closed 
         * 
         * The real calculateDate is end of 2 cycle before the given cDate.
         * so:
         * query on cDate=2020-03-02 12:00:01 => 
         *     SELECT * WHERE 
         *     (status='Open' AND activate_date<'2020-03-01 23:59:59') OR 
         *     (pgd_status='Close' AND close_date>'2020-03-01 23:59:59');
         * which returns active pledges on given cDate, even Open or Close.
         * active means the accounts which they must do repayback.
         * 
         * while on same date to retrieve take-placed pledges (actives or inactives)
         * query on 2020-03-02 12:00:01 => SELECT * WHERE 
         * (status='Open' AND activate_date<'2020-03-01 23:59:59') OR (pgd_status='Close' AND close_date>'2020-03-01 23:59:59');
         * 
         * Note: the only indicator for pledge status is "pgd_status" and not start_date or close_date
         */
        let query, values;
        query = `
            SELECT * FROM ${tableOnchainPledgedAccounts} 
            WHERE (pgd_status=$1 AND pgd_activate_date<$2) OR (pgd_status=$3 AND pgd_close_date>$4) `
        values = [iConsts.CONSTS.OPEN, cDate, iConsts.CONSTS.CLOSE, cDate];

        let res = model.sCustom({
            query,
            values
        });
        if (res.length == 0)
            return {};

        res = res.map(x => this.convertFieldsPledgedAccounts(x));

        let outDict = {}
        for (let aRes of res) {
            if (!_.has(outDict, aRes.pgdPledger))
                outDict[aRes.pgdPledger] = [];  // it is possible an account pledged multi time because of different pledge contracts
            outDict[aRes.pgdPledger].push(aRes);
        }
        return outDict
    }

    static renderPledgeDocumentToHTML(pledge) {
        return `
        <table>
            <tr>
                <td>Pledge Class:</td>
                <td>${pledge.dClass}</td>
            </tr>
            
            <tr>
                <td>Pledge Description:</td>
                <td>${pledge.description}</td>
            </tr>
            
            <tr>
                <td>Pledger:</td>
                <td>${pledge.pledger}</td>
            </tr>
            <tr>
                <td>Pledgee:</td>
                <td>${pledge.pledgee}</td>
            </tr>
            <tr>
                <td>arbiter:</td>
                <td>${pledge.arbiter}</td>
            </tr>
            
            <tr>
                <td>Redeem Terms:</td>
                <td></td>
            </tr>

            <tr>
                <td>Principal:</td>
                <td title="${utils.sepNum(pledge.redeemTerms.principal)} &micro;PAI">
                    ${utils.microPAIToPAI(pledge.redeemTerms.principal)} PAI
                </td>
            </tr>
            <tr>
                <td>Annual Interest:</td>
                <td>${pledge.redeemTerms.annualInterest} %</td>
            </tr>
            <tr>
                <td>Repayment Offset:</td>
                <td>${pledge.redeemTerms.repaymentOffset}</td>
            </tr>
            <tr>
                <td>Repayment Amount:</td>
                <td title="${utils.sepNum(pledge.redeemTerms.repaymentAmount)} &micro;PAI">
                    ${utils.microPAIToPAI(pledge.redeemTerms.repaymentAmount)} PAI
                </td>
            </tr>
            <tr>
                <td>Repayment Schedule:</td>
                <td>${pledge.redeemTerms.repaymentSchedule} times (2 time in day)</td>
            </tr>

            <tr>
                <td>Creation Date:</td>
                <td>${pledge.creationDate}</td>
            </tr>

            <tr>
                <td>Pledger Sign Date:</td>
                <td>${pledge.pledgerSignDate}</td>
            </tr>
        </table>
        `;
    }

    static convertFieldsPledgedAccounts(elm) {
        let out = {};
        if (_.has(elm, 'pgd_hash'))
            out.pgdHash = elm.pgd_hash;
        if (_.has(elm, 'pgd_type'))
            out.pgdType = elm.pgd_type;
        if (_.has(elm, 'pgd_class'))
            out.pgdClass = elm.pgd_class;
        if (_.has(elm, 'pgd_version'))
            out.pgdVersion = elm.pgd_version;
        if (_.has(elm, 'pgd_pledger_sign_date'))
            out.pgdPledgerSignDate = elm.pgd_pledger_sign_date;
        if (_.has(elm, 'pgd_pledgee_sign_date'))
            out.pgdPledgeeSignDate = elm.pgd_pledgee_sign_date;
        if (_.has(elm, 'pgd_activate_date'))
            out.pgdActivateDate = elm.pgd_activate_date;
        if (_.has(elm, 'pgd_close_date'))
            out.pgdCloseDate = elm.pgd_close_date;
        if (_.has(elm, 'pgd_pledger'))
            out.pgdPledger = elm.pgd_pledger;
        if (_.has(elm, 'pgd_pledgee'))
            out.pgdPledgee = elm.pgd_pledgee;
        if (_.has(elm, 'pgd_arbiter'))
            out.pgdArbiter = elm.pgd_arbiter;
        if (_.has(elm, 'pgd_principal'))
            out.pgdPrincipal = elm.pgd_principal;
        if (_.has(elm, 'pgd_annual_interest'))
            out.pgdAnnualInterest = elm.pgd_annual_interest;
        if (_.has(elm, 'pgd_repayment_offset'))
            out.pgdRepaymentOffset = elm.pgd_repayment_offset;
        if (_.has(elm, 'pgd_repayment_amount'))
            out.pgdRepaymentAmount = elm.pgd_repayment_amount;
        if (_.has(elm, 'pgd_repayment_schedule'))
            out.pgdRepaymentSchedule = elm.pgd_repayment_schedule;
        if (_.has(elm, 'pgd_status'))
            out.pgdStatus = elm.pgd_status;
        return out;
    }

    static convertFieldsDraft(elm) {
        let out = {};
        if (_.has(elm, 'dpl_id'))
            out.dplId = elm.dpl_id;
        if (_.has(elm, 'dpl_mp_code'))
            out.dplmpCode = elm.dpl_mp_code;
        if (_.has(elm, 'dpl_type'))
            out.dplType = elm.dpl_type;
        if (_.has(elm, 'dpl_version'))
            out.dplVersion = elm.dpl_version;
        if (_.has(elm, 'dpl_descriptions'))
            out.dplDescriptions = elm.dpl_descriptions;
        if (_.has(elm, 'dpl_pledger'))
            out.dplPledger = elm.dpl_pledger;
        if (_.has(elm, 'dpl_pledgee'))
            out.dplPledgee = elm.dpl_pledgee;
        if (_.has(elm, 'dpl_proposal_ref'))
            out.dplProposalRef = elm.dpl_proposal_ref;
        if (_.has(elm, 'dpl_body'))
            out.dplBody = elm.dpl_body;
        if (_.has(elm, 'dpl_req_date'))
            out.dplReqDate = elm.dpl_req_date;

        return out;
    }
}


module.exports = GeneralPledgeHandler;
