const _ = require('lodash');
const iConsts = require("../../../config/constants");
const utils = require("../../../utils/utils");
// const coinbase = require("../../../dag/coinbase");
const getDNAActiveDateRange = require("../../../dna/dna-handler").getDNAActiveDateRange
const calculateVoteGain = require("../../../services/polling-handler/general-poll-handler").calculateVoteGain;
const fs = require('fs');

describe('Coinbase ', () => {
    it('Should control if for active shares date range is valid (12 hours per cycle)', () => {
        let t;
        //TODO: need more tests for corner cases!
        if (iConsts.TIME_GAIN == 1) {
            t = getDNAActiveDateRange('2024-05-09 00:00:01');
            expect(t.minCreationDate).toEqual('2017-05-08 00:00:00');
            expect(t.maxCreationDate).toEqual('2024-05-08 11:59:59');

            t = getDNAActiveDateRange('2024-05-09 23:59:59');
            expect(t.minCreationDate).toEqual('2017-05-08 12:00:00');
            expect(t.maxCreationDate).toEqual('2024-05-08 23:59:59');

            t = getDNAActiveDateRange('2019-05-07 11:20:01');
            expect(t.minCreationDate).toEqual('2012-05-06 00:00:00');
            expect(t.maxCreationDate).toEqual('2019-05-06 11:59:59');

            t = getDNAActiveDateRange('2019-05-07 12:00:01');
            expect(t.minCreationDate).toEqual('2012-05-06 12:00:00');
            expect(t.maxCreationDate).toEqual('2019-05-06 23:59:59');

            t = getDNAActiveDateRange('2019-05-07 13:00:01');
            expect(t.minCreationDate).toEqual('2012-05-06 12:00:00');
            expect(t.maxCreationDate).toEqual('2019-05-06 23:59:59');

            t = getDNAActiveDateRange('2019-06-01 00:00:00');
            expect(t.minCreationDate).toEqual('2012-05-31 00:00:00');
            expect(t.maxCreationDate).toEqual('2019-05-31 11:59:59');

        }
    });

    it('Should control if for active shares date range is valid (5 minutes per cycle)', () => {
        let _t;
        if (iConsts.getCycleByMinutes() == 5) {
            _t = getDNAActiveDateRange('2019-06-20 00:10:00');
            expect(_t.minCreationDate).toEqual('2019-06-19 00:10:00');  // 2019-06-19 15:40:00
            expect(_t.maxCreationDate).toEqual('2019-06-20 00:04:59');

            _t = getDNAActiveDateRange('2019-06-20 00:12:00');
            expect(_t.minCreationDate).toEqual('2019-06-19 00:10:00');
            // expect(_t.maxCreationDate).toEqual('2019-06-20 -0:-1:59');

            _t = getDNAActiveDateRange('2019-06-20 00:16:00');
            console.log(_t);
            expect(_t.minCreationDate).toEqual('2019-06-19 00:15:00');
            if (iConsts.TREASURY_MATURATION_CYCLES == 2) {
                expect(_t.maxCreationDate).toEqual('2019-06-20 00:09:59');

            } else if (iConsts.TREASURY_MATURATION_CYCLES == 4) {
                expect(_t.maxCreationDate).toEqual('2019-06-20 00:09:59');
            }
        }
    });

    it('Should control vote gain of voters based on time of receiveing vote to a local machine', () => {
        return;
        let gainYes, gainNoAbstain;
        let cases = [
            { voteCreationTimeByMinute: 0, voteReceiveingTimeByMinute: 0 },
            { voteCreationTimeByMinute: 1, voteReceiveingTimeByMinute: 1 },
            { voteCreationTimeByMinute: 360, voteReceiveingTimeByMinute: 360 }, //  6 hour
            { voteCreationTimeByMinute: 720, voteReceiveingTimeByMinute: 720 }, //  12 hour
            { voteCreationTimeByMinute: 1080, voteReceiveingTimeByMinute: 1080 }, // 18 hour  
            { voteCreationTimeByMinute: 1440, voteReceiveingTimeByMinute: 1440 }, // 24 hour  
            { voteCreationTimeByMinute: 2880, voteReceiveingTimeByMinute: 2880 }, // 48 hour
            { voteCreationTimeByMinute: 4320, voteReceiveingTimeByMinute: 4320 }, // 72 hour
            { voteCreationTimeByMinute: 5040, voteReceiveingTimeByMinute: 5040 }, // 84 hour
        ]
        let out = '\nCreation \tReceive \tDelta \tConfirm \t Cancel';
        let createDate, receiveDate, delta;
        for (let elm of cases) {
            delta = elm.voteReceiveingTimeByMinute - elm.voteCreationTimeByMinute;
            delta = (delta / 60) + ':' + (delta % 60)
            let { gainYes, gainNoAbstain } = calculateVoteGain(elm.voteCreationTimeByMinute, elm.voteReceiveingTimeByMinute);
            createDate = parseInt(elm.voteCreationTimeByMinute / 60) + ':' + parseInt(elm.voteCreationTimeByMinute % 60)
            receiveDate = parseInt(elm.voteReceiveingTimeByMinute / 60) + ':' + parseInt(elm.voteReceiveingTimeByMinute % 60)
            out += `\n${_.padStart(createDate, 8, ' ')} \t${_.padStart(receiveDate, 7, ' ')} \t${_.padStart(delta.toFixed(3), 5, ' ')} \t${_.padStart(gainYes, 7, ' ')} \t${_.padStart(gainNoAbstain, 7, ' ')}`;
        }


        // expect().toEqual(1);
        // expect(calculateVoteGain(0)).toEqual(1);
        // expect(calculateVoteGain(720)).toEqual(0);

    });

    it('Should control vote gain of voters based on time of receiveing vote to a local machine', () => {
        let gainYes, gainNoAbstain;
        if (iConsts.TIME_GAIN == 5) {
            let cases = [
                { voteReceiveingTimeByMinute: 0, latenancy: 1 },
                { voteReceiveingTimeByMinute: 1, latenancy:  1.041392685158225 },
                { voteReceiveingTimeByMinute: 2, latenancy:  1.0791812460476247 },
                { voteReceiveingTimeByMinute: 3, latenancy:  1.1139433523068367 },
                { voteReceiveingTimeByMinute: 4, latenancy:  1.1461280356782377 },
                { voteReceiveingTimeByMinute: 5, latenancy:  1.1760912590556811 },
                { voteReceiveingTimeByMinute: 6, latenancy:  1.2041199826559246 },
                { voteReceiveingTimeByMinute: 7, latenancy:  1.2304489213782739 },
                { voteReceiveingTimeByMinute: 8, latenancy:  1.2552725051033058 },
                { voteReceiveingTimeByMinute: 10, latenancy: 1.301029995663981 },
                { voteReceiveingTimeByMinute: 11, latenancy: 1.322219294733919 },
                { voteReceiveingTimeByMinute: 12, latenancy: 1.3424226808222062 },
                { voteReceiveingTimeByMinute: 13, latenancy: 1.3617278360175928 },
                { voteReceiveingTimeByMinute: 14, latenancy: 1.380211241711606 },
                { voteReceiveingTimeByMinute: 15, latenancy: 1.3979400086720375 },
                { voteReceiveingTimeByMinute: 16, latenancy: 1.414973347970818 },
            ]
            let out = '\nDelta \tLatenancy';
            let createDate, receiveDate, delta;
            for (let elm of cases) {
                delta = elm.voteReceiveingTimeByMinute;
                delta = utils.floor(delta / 60) + ':' + utils.customFloorFloat(delta % 60, 2)
                let { gainYes, gainNoAbstain, latenancyFactor } = calculateVoteGain(0, elm.voteReceiveingTimeByMinute);
                out += `\n\t${_.padStart(delta, 5, ' ')} \t${_.padStart(latenancyFactor, 7, ' ')}`;
                expect(elm.latenancy).toEqual(latenancyFactor);
            }
            // console.log(out);

            // expect(calculateVoteGain(0)).toEqual(1);
            // expect(calculateVoteGain(720)).toEqual(0);
        }
    });

    it('Should control vote gain of voters based on time of receiveing vote to a local machine', () => {

        let gainYes, gainNoAbstain;
        let out = '\nCreation \tReceive \tDelta \tConfirm \t Cancel';
        let createDate, receiveDate, delta, newLine;

        let shouldCreateFile = false
        let fileName = 'temporary/dna-proposal.txt';
        let fp;
        if (shouldCreateFile)
            fp = fs.openSync(fileName, 'a', '0777');

        let endCreateTime = 5040;
        let endReceiveTime;
        for (let x = 0; x < endCreateTime; x += 10) { // create time
            newLine = '\n\nCreation \tReceive \tDelta \tConfirm \t Cancel';
            if (shouldCreateFile)
                fs.writeFileSync(fp, newLine, 'utf8');
            // out += newLine;
            endReceiveTime = 2 * x;
            if (endReceiveTime > endCreateTime + 2880)
                endReceiveTime = endCreateTime + 2880; // maximum until 48 hour after finishing voting

            for (let y = x; y < endReceiveTime; y += 10) { // recevieng time
                delta = y - x;
                delta = parseInt(delta / 60) + ':' + parseInt(delta % 60)
                let { gainYes, gainNoAbstain } = calculateVoteGain(x, y);
                createDate = parseInt(x / 60) + ':' + parseInt(x % 60)
                receiveDate = parseInt(y / 60) + ':' + parseInt(y % 60)
                newLine = `\n${_.padStart(createDate, 8, ' ')} \t${_.padStart(receiveDate, 7, ' ')} \t${_.padStart(delta, 5, ' ')} \t${_.padStart(gainYes, 7, ' ')} \t${_.padStart(gainNoAbstain, 7, ' ')}`;
                // out += newLine

                // writing to file
                if (shouldCreateFile)
                    fs.writeFileSync(fp, newLine, 'utf8');

            }

        }
        let cases = [
            { voteCreationTimeByMinute: 0, voteReceiveingTimeByMinute: 0 },
            { voteCreationTimeByMinute: 1, voteReceiveingTimeByMinute: 1 },
            { voteCreationTimeByMinute: 360, voteReceiveingTimeByMinute: 360 }, //  6 hour
            { voteCreationTimeByMinute: 720, voteReceiveingTimeByMinute: 720 }, //  12 hour
            { voteCreationTimeByMinute: 1080, voteReceiveingTimeByMinute: 1080 }, // 18 hour  
            { voteCreationTimeByMinute: 1440, voteReceiveingTimeByMinute: 1440 }, // 24 hour  
            { voteCreationTimeByMinute: 2160, voteReceiveingTimeByMinute: 2160 }, // 36 hour
            { voteCreationTimeByMinute: 2880, voteReceiveingTimeByMinute: 2880 }, // 48 hour
            { voteCreationTimeByMinute: 4320, voteReceiveingTimeByMinute: 4320 }, // 72 hour
            { voteCreationTimeByMinute: 5040, voteReceiveingTimeByMinute: 5040 }, // 84 hour
        ]





        // expect().toEqual(1);
        // expect(calculateVoteGain(0)).toEqual(1);
        // expect(calculateVoteGain(720)).toEqual(0);

    });


});
