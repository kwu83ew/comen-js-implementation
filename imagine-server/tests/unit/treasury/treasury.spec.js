const iConsts = require('../../../config/constants');
const treasuryHandler = require('../../../services/treasury/treasury-handler');

describe('Treasury ', () => {
    it('Should control if income date range is valid (12 hours cycle)', () => {
        let t;
        if (iConsts.getCycleByMinutes() == iConsts.STANDARD_CYCLE_BY_MINUTES) {
            
            t = treasuryHandler.getTreasureIncomesDateRange('2020-03-29 12:00:00');
            expect(t.minCreationDate).toEqual('2020-03-27 12:00:00');
            expect(t.maxCreationDate).toEqual('2020-03-27 23:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 00:00:00');
            expect(t.minCreationDate).toEqual('2018-05-05 00:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-05 11:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 11:00:01');
            expect(t.minCreationDate).toEqual('2018-05-05 00:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-05 11:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 12:00:01');
            expect(t.minCreationDate).toEqual('2018-05-05 12:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-05 23:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2017-05-07 14:00:11');
            expect(t.minCreationDate).toEqual('2017-05-05 12:00:00');
            expect(t.maxCreationDate).toEqual('2017-05-05 23:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2016-01-08 00:00:01');
            expect(t.minCreationDate).toEqual('2016-01-06 00:00:00');
            expect(t.maxCreationDate).toEqual('2016-01-06 11:59:59');
        }
    });

    it('Should control if income date range is valid (60 minutes cycle)', () => {
        let t;
        if (iConsts.getCycleByMinutes() == 60) {
            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 00:00:01');
            expect(t.minCreationDate).toEqual('2018-05-06 20:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-06 20:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 11:00:01');
            expect(t.minCreationDate).toEqual('2018-05-07 07:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 07:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 12:00:01');
            expect(t.minCreationDate).toEqual('2018-05-07 08:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 08:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2016-05-08 20:00:01');
            expect(t.minCreationDate).toEqual('2016-05-08 16:00:00');
            expect(t.maxCreationDate).toEqual('2016-05-08 16:59:59');
        }
    });

    it('Should control if income date range is valid (30 minutes cycle)', () => {
        let t;
        if (iConsts.getCycleByMinutes() == 30) {
            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 00:00:01');
            expect(t.minCreationDate).toEqual('2018-05-06 22:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-06 22:29:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 11:00:01');
            expect(t.minCreationDate).toEqual('2018-05-07 09:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 09:29:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 11:45:01');
            expect(t.minCreationDate).toEqual('2018-05-07 09:30:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 09:59:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 12:00:01');
            expect(t.minCreationDate).toEqual('2018-05-07 10:00:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 10:29:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2017-05-07 14:00:11');
            expect(t.minCreationDate).toEqual('2017-05-07 12:00:00');
            expect(t.maxCreationDate).toEqual('2017-05-07 12:29:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2016-05-08 00:00:01');
            expect(t.minCreationDate).toEqual('2016-05-07 22:00:00');
            expect(t.maxCreationDate).toEqual('2016-05-07 22:29:59');
        }
    });

    it('Should control if income date range is valid (30 minutes cycle)', () => {
        let t;
        if (iConsts.TIME_GAIN == 5) {
            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 10:50:00');
            expect(t.minCreationDate).toEqual('2018-05-07 10:30:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 10:34:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2018-05-07 11:00:01');
            expect(t.minCreationDate).toEqual('2018-05-07 10:40:00');
            expect(t.maxCreationDate).toEqual('2018-05-07 10:44:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2019-09-21 23:00:00');
            expect(t.minCreationDate).toEqual('2019-09-21 22:40:00');
            expect(t.maxCreationDate).toEqual('2019-09-21 22:44:59');

            t = treasuryHandler.getTreasureIncomesDateRange('2019-09-21 23:20:00');
            expect(t.minCreationDate).toEqual('2019-09-21 23:00:00');
            expect(t.maxCreationDate).toEqual('2019-09-21 23:04:59');

        }
    });

});
