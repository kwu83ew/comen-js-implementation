const vHandler = require('../../../services/version-handler/version-handler')

describe('vHandler.isNewerEqualThan', () => {
    it('Should return true for newer or equal version', () => {
        expect(vHandler.isNewerOrEqualThan('0.0.1', '0.0.0')).toEqual(true)
        expect(vHandler.isNewerOrEqualThan('0.2.0', '0.2.1')).toEqual(false)
        expect(vHandler.isNewerOrEqualThan('0.0.2', '0.0.1')).toEqual(true)
        expect(vHandler.isNewerOrEqualThan('1.0.0', '0.90.91')).toEqual(true)
    });
    
    it('Should return true for ONLY newer version', () => {
        expect(vHandler.isNewerThan('0.0.1', '0.0.0')).toEqual(true)
        expect(vHandler.isNewerThan('0.0.0', '0.0.0')).toEqual(false)
        expect(vHandler.isNewerThan('0.0.0', '0.1.0')).toEqual(false)
        expect(vHandler.isNewerThan('0.2.0', '0.1.0')).toEqual(true)
        expect(vHandler.isNewerThan('0.2.0', '0.0.90')).toEqual(true)
        expect(vHandler.isNewerThan('1.2.0', '0.90.90')).toEqual(true)
    });
    
    it('Should return true for ONLY older version', () => {
        expect(vHandler.isOlderThan('0.0.0', '0.0.1')).toEqual(true)
        expect(vHandler.isOlderThan('0.0.1', '0.0.0')).toEqual(false)
        expect(vHandler.isOlderThan('0.0.0', '0.0.0')).toEqual(false)
        expect(vHandler.isOlderThan('0.0.0', '0.1.0')).toEqual(true)
        expect(vHandler.isOlderThan('0.2.0', '0.1.0')).toEqual(false)
        expect(vHandler.isOlderThan('0.2.0', '0.0.90')).toEqual(false)
        expect(vHandler.isOlderThan('1.2.0', '0.90.90')).toEqual(false)
    });

});

