
const crypto = require('../../../crypto/crypto');
const CONSTANTS = require('../../../config/constants');
const complexAddressHandler = require('../../../address/complex-address-handler');

describe('bech32', () => {
    it('Should encode then decod and return same text', () => {
        let address = complexAddressHandler.createAddress({
            pubToPrvMap: getPubToPrvMap(),
            rawSignSets: getRawSignSets(),
            mSignaturesCount: 2,
            nSignaturesCount: 3,



            signatureType: 'complex',
            customBechUnlocker: 'im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf',
            walletAddresses: [
                "im1xq6rvvfevgexyd3j8qmrxvnpvs6rxerrxqengde389jrjwf4vsmn2flg38h",
                "im1xpjngwph8q6rqep5x5crge3kxcexvdp5x4snyeryxvmxyvph8yukg5sv0gp",
                "im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf",
                "im1xqckvwrxx5cnwen9xuerver9xvcnxwpnvvmnycekx9nrvwfcve3rgd4fsrd"
            ],
            addressesDict: getAddressesDict(),


        });
    });

});


function getPubToPrvMap() {
    return {
        "3d7f50c111d983c0b6f0ef0d2e642b9155788d4abb80669be1f61e112f8eb621": {
            "merkleVersion": "0.0.0",
            "merkleRoot": "e5503076f4e3530010dc66fdfe549cba23ca7176a3eb8194988373b8a5b23f77",
            "accountAddress": "im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf",
            "uSets": [
                {
                    "sSets": [
                        [
                            "032a6bee6373cfad85a4488768b379f85695989bb246f4f0b85df64b6815dd09fa"
                        ],
                        [
                            "0326a27da8825bb1ba7fcc59142263b260749915c614c3a6703634ad1d3360204e"
                        ]
                    ],
                    "proofs": [
                        "r.1805776278a3e2ee752e4e699aec52ff0bad61a966d4092fcae2774ba76125be",
                        "r.b01d627bd8d34b5cdd6486b93cca4bf0838ab0a5a0c60f8a53adcb5e83ef680a"
                    ],
                    "lHash": null,
                    "salt": "2f2b08c5a89e9093"
                },
                {
                    "sSets": [
                        [
                            "02b2806b3b59f08b6dfc494b141ad9cf6d30208493ae0e43d621d5b82ac664c87b"
                        ],
                        [
                            "032a6bee6373cfad85a4488768b379f85695989bb246f4f0b85df64b6815dd09fa"
                        ]
                    ],
                    "proofs": [
                        "r.b01d627bd8d34b5cdd6486b93cca4bf0838ab0a5a0c60f8a53adcb5e83ef680a"
                    ],
                    "lHash": "0dda9e4175ca83f1c406bfe66b0667465a4620533b4db808b33319b09e4bd962",
                    "salt": "f2065056fd18d21b"
                },
                {
                    "sSets": [
                        [
                            "0326a27da8825bb1ba7fcc59142263b260749915c614c3a6703634ad1d3360204e"
                        ],
                        [
                            "02b2806b3b59f08b6dfc494b141ad9cf6d30208493ae0e43d621d5b82ac664c87b"
                        ]
                    ],
                    "proofs": [
                        "l.2f9f1256efe432b846239a7858be08b31fcf5578f509af45b2a83ce3231da19a"
                    ],
                    "lHash": "073c848071c1439d0239ab3cfde786c7c8a39ed7b804e67947fa416cdcd4a953",
                    "salt": "8e04f7423de6943e"
                }
            ],
            "privContainer": {
                "2f2b08c5a89e9093": {
                    "uSets": [
                        "cf7876e5ee8182dd86d44586e469355e155be943cef7052ec8e82859d7901621",
                        "3c4938561b537f9c779efdb40b38d34173f2d9ba18dc58c5663ab09a11ee4d25"
                    ],
                    "privContainer": {}
                },
                "f2065056fd18d21b": {
                    "uSets": [
                        "2e964ce46ae3d0b3d8db8cd02e3ff8161a806586634dafb881d2a65744708f88",
                        "cf7876e5ee8182dd86d44586e469355e155be943cef7052ec8e82859d7901621"
                    ],
                    "privContainer": {}
                },
                "8e04f7423de6943e": {
                    "uSets": [
                        "3c4938561b537f9c779efdb40b38d34173f2d9ba18dc58c5663ab09a11ee4d25",
                        "2e964ce46ae3d0b3d8db8cd02e3ff8161a806586634dafb881d2a65744708f88"
                    ],
                    "privContainer": {}
                }
            }
        },
        "958270644848f65afa54a19ee5434873b34be541a67915914575e94692ecc290": "1c2cbba0ce853ac5207fbf26d77a13243917366420eb66668829dc226efcd773",
        "3f38cd9369e6749509b9c7a9d143dbb9b17a79ece067c7e71523ad52108c3077": {
            "merkleVersion": "0.0.0",
            "merkleRoot": "0f9b64a046fec0a38a0248f3c5be71d2d6c19243a681bf38f928ab602cd34582",
            "accountAddress": "im1xpjngwph8q6rqep5x5crge3kxcexvdp5x4snyeryxvmxyvph8yukg5sv0gp",
            "uSets": [
                {
                    "sSets": [
                        [
                            "03669a35317c1efdbe7262ce5a8734ecd2f987ef4a852ba501ddb23aa233d4ad76"
                        ]
                    ],
                    "proofs": null,
                    "lHash": null,
                    "salt": "e0256dba47bd81a6"
                }
            ],
            "privContainer": {
                "e0256dba47bd81a6": {
                    "uSets": [
                        "f6bde9fe404e4837aedf736436278b4a29ed5519c3e86b99eee50e7a7056b362"
                    ],
                    "privContainer": {}
                }
            }
        }
    }
}

function getRawSignSets() {
    return [
        [
            "3d7f50c111d983c0b6f0ef0d2e642b9155788d4abb80669be1f61e112f8eb621",
            [
                "im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf",
                0,
                0,
                [],
                0,
                0
            ]
        ],
        [
            "958270644848f65afa54a19ee5434873b34be541a67915914575e94692ecc290",
            [
                "035726114fd5d208850e5cea63724c31edb73770dc7cbe85430f8bbcfd22c8298f",
                2,
                0,
                [],
                0,
                0
            ]
        ],
        [
            "3f38cd9369e6749509b9c7a9d143dbb9b17a79ece067c7e71523ad52108c3077",
            [
                "im1xpjngwph8q6rqep5x5crge3kxcexvdp5x4snyeryxvmxyvph8yukg5sv0gp",
                0,
                0,
                [],
                0,
                0
            ]
        ]
    ]
}





















function getAddressesDict() {
    return {
        "im1xq6rvvfevgexyd3j8qmrxvnpvs6rxerrxqengde389jrjwf4vsmn2flg38h": {
            "title": "2/3",
            "detail": {
                "merkleVersion": "0.0.0",
                "merkleRoot": "066664917c429bb6d2cc5bf007d4062427493f33a87493c2e34f8c0985031521",
                "accountAddress": "im1xq6rvvfevgexyd3j8qmrxvnpvs6rxerrxqengde389jrjwf4vsmn2flg38h",
                "uSets": [
                    {
                        "sSets": [
                            [
                                "0285fbd1dad48479191d88449766056efbbe67426e3f90070a78eb1479f6c2d56c"
                            ],
                            [
                                "037b6e9523310a3b28b8de45fa6a843c65a37de6f46554f6287ff081abe56f8310"
                            ]
                        ],
                        "proofs": [
                            "r.4010b140ada0cb5478f5a235a0b0ea43cebdc8d812dde559570002b8e017a61b",
                            "r.4f6923a1b0b644a18acae7ec0a99083703dc7c0a0d29b7c7da6ce25797bc173e"
                        ],
                        "lHash": null,
                        "salt": "d9e113267a41bf8e"
                    },
                    {
                        "sSets": [
                            [
                                "03d5513caca97e45fae3371f32309e633bc29f51ed0c89278f4bbf728289318c06"
                            ],
                            [
                                "0285fbd1dad48479191d88449766056efbbe67426e3f90070a78eb1479f6c2d56c"
                            ]
                        ],
                        "proofs": [
                            "r.4f6923a1b0b644a18acae7ec0a99083703dc7c0a0d29b7c7da6ce25797bc173e"
                        ],
                        "lHash": "afbd00bf22e5ec6d05a7faf798601b2814dbfa3f4e7bd28bf250160ade31e97f",
                        "salt": "9b0c3f3ac39aad49"
                    },
                    {
                        "sSets": [
                            [
                                "037b6e9523310a3b28b8de45fa6a843c65a37de6f46554f6287ff081abe56f8310"
                            ],
                            [
                                "03d5513caca97e45fae3371f32309e633bc29f51ed0c89278f4bbf728289318c06"
                            ]
                        ],
                        "proofs": [
                            "l.6a99d676a44237660aba4d237df3afbed77ccffc76ec2a14b7c85fd7386b46f9"
                        ],
                        "lHash": "1e252686b0e2a9fc9d838884b72b309bc3b0d0755eb228dd9a9d4683bbf1e536",
                        "salt": "ed8d5e06c80dd15b"
                    }
                ],
                "privContainer": {
                    "d9e113267a41bf8e": {
                        "uSets": [
                            "6f9c3e8a834521f426fa94a5ddfc4983d5ef0691b3031888d04c0c89f695f3fb",
                            "ac5e887f21fd4fb94f523ab70717a4937bace1365a348a8b2d0c69579704d40d"
                        ],
                        "privContainer": {}
                    },
                    "9b0c3f3ac39aad49": {
                        "uSets": [
                            "ae54098fc663d119aaa32980b06a1d6b0a30a1cb6a4ad48a31cd86dddcb3b9bd",
                            "6f9c3e8a834521f426fa94a5ddfc4983d5ef0691b3031888d04c0c89f695f3fb"
                        ],
                        "privContainer": {}
                    },
                    "ed8d5e06c80dd15b": {
                        "uSets": [
                            "ac5e887f21fd4fb94f523ab70717a4937bace1365a348a8b2d0c69579704d40d",
                            "ae54098fc663d119aaa32980b06a1d6b0a30a1cb6a4ad48a31cd86dddcb3b9bd"
                        ],
                        "privContainer": {}
                    }
                }
            }
        },
        "im1xpjngwph8q6rqep5x5crge3kxcexvdp5x4snyeryxvmxyvph8yukg5sv0gp": {
            "title": "1/1",
            "detail": {
                "merkleVersion": "0.0.0",
                "merkleRoot": "0f9b64a046fec0a38a0248f3c5be71d2d6c19243a681bf38f928ab602cd34582",
                "accountAddress": "im1xpjngwph8q6rqep5x5crge3kxcexvdp5x4snyeryxvmxyvph8yukg5sv0gp",
                "uSets": [
                    {
                        "sSets": [
                            [
                                "03669a35317c1efdbe7262ce5a8734ecd2f987ef4a852ba501ddb23aa233d4ad76"
                            ]
                        ],
                        "proofs": null,
                        "lHash": null,
                        "salt": "e0256dba47bd81a6"
                    }
                ],
                "privContainer": {
                    "e0256dba47bd81a6": {
                        "uSets": [
                            "f6bde9fe404e4837aedf736436278b4a29ed5519c3e86b99eee50e7a7056b362"
                        ],
                        "privContainer": {}
                    }
                }
            }
        },
        "im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf": {
            "title": "2/3",
            "detail": {
                "merkleVersion": "0.0.0",
                "merkleRoot": "e5503076f4e3530010dc66fdfe549cba23ca7176a3eb8194988373b8a5b23f77",
                "accountAddress": "im1xpjnxvfex5cnvdrzx3snger9vccxxcfhv9nxydm9v93xzetrxqexxefkrpf",
                "uSets": [
                    {
                        "sSets": [
                            [
                                "032a6bee6373cfad85a4488768b379f85695989bb246f4f0b85df64b6815dd09fa"
                            ],
                            [
                                "0326a27da8825bb1ba7fcc59142263b260749915c614c3a6703634ad1d3360204e"
                            ]
                        ],
                        "proofs": [
                            "r.1805776278a3e2ee752e4e699aec52ff0bad61a966d4092fcae2774ba76125be",
                            "r.b01d627bd8d34b5cdd6486b93cca4bf0838ab0a5a0c60f8a53adcb5e83ef680a"
                        ],
                        "lHash": null,
                        "salt": "2f2b08c5a89e9093"
                    },
                    {
                        "sSets": [
                            [
                                "02b2806b3b59f08b6dfc494b141ad9cf6d30208493ae0e43d621d5b82ac664c87b"
                            ],
                            [
                                "032a6bee6373cfad85a4488768b379f85695989bb246f4f0b85df64b6815dd09fa"
                            ]
                        ],
                        "proofs": [
                            "r.b01d627bd8d34b5cdd6486b93cca4bf0838ab0a5a0c60f8a53adcb5e83ef680a"
                        ],
                        "lHash": "0dda9e4175ca83f1c406bfe66b0667465a4620533b4db808b33319b09e4bd962",
                        "salt": "f2065056fd18d21b"
                    },
                    {
                        "sSets": [
                            [
                                "0326a27da8825bb1ba7fcc59142263b260749915c614c3a6703634ad1d3360204e"
                            ],
                            [
                                "02b2806b3b59f08b6dfc494b141ad9cf6d30208493ae0e43d621d5b82ac664c87b"
                            ]
                        ],
                        "proofs": [
                            "l.2f9f1256efe432b846239a7858be08b31fcf5578f509af45b2a83ce3231da19a"
                        ],
                        "lHash": "073c848071c1439d0239ab3cfde786c7c8a39ed7b804e67947fa416cdcd4a953",
                        "salt": "8e04f7423de6943e"
                    }
                ],
                "privContainer": {
                    "2f2b08c5a89e9093": {
                        "uSets": [
                            "cf7876e5ee8182dd86d44586e469355e155be943cef7052ec8e82859d7901621",
                            "3c4938561b537f9c779efdb40b38d34173f2d9ba18dc58c5663ab09a11ee4d25"
                        ],
                        "privContainer": {}
                    },
                    "f2065056fd18d21b": {
                        "uSets": [
                            "2e964ce46ae3d0b3d8db8cd02e3ff8161a806586634dafb881d2a65744708f88",
                            "cf7876e5ee8182dd86d44586e469355e155be943cef7052ec8e82859d7901621"
                        ],
                        "privContainer": {}
                    },
                    "8e04f7423de6943e": {
                        "uSets": [
                            "3c4938561b537f9c779efdb40b38d34173f2d9ba18dc58c5663ab09a11ee4d25",
                            "2e964ce46ae3d0b3d8db8cd02e3ff8161a806586634dafb881d2a65744708f88"
                        ],
                        "privContainer": {}
                    }
                }
            }
        }
    }
}
