const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.isOlderThanSomeCycles', () => {
    if (iConsts.TIME_GAIN == 1) {
        it('Should calc isOlderThanSomeCycles ', () => {

        });
    }


    if (iConsts.TIME_GAIN == 5) {

        it('Should calc isOlderThanSomeCycles', () => {
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:10:00', 1, '2028-09-05 07:10:00')).toEqual(false);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:05:00', 1, '2028-09-05 07:10:00')).toEqual(false);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:04:59', 1, '2028-09-05 07:10:00')).toEqual(true);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:00:00', 1, '2028-09-05 07:10:00')).toEqual(true);

            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:10:00', 2, '2028-09-05 07:10:00')).toEqual(false);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:05:00', 2, '2028-09-05 07:10:00')).toEqual(false);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 07:00:00', 2, '2028-09-05 07:10:00')).toEqual(false);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 06:59:59', 2, '2028-09-05 07:10:00')).toEqual(true);
            expect(iutils.isOlderThanSomeCycles('2028-09-05 06:55:00', 2, '2028-09-05 07:10:00')).toEqual(true);

        });
    }

});