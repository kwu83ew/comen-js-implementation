
const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const POWblockHandler = require('../../../dag/pow-block/pow-block-handler');
const cnfHandler = require('../../../config/conf-params');

function hasherFunc(doc) {
    return { err: false, hash: iutils.doHashObject(doc) };
}

describe('iUtils.arrayRemoveByIndex', () => {
    it('Should calculate arrayRemoveByIndex', () => {
        let document = {
            hash: '0000000000000000000000000000000000000000000000000000000000000000',
            dLen: '0000000',
        };
        let solution = POWblockHandler.calcNonce({
            document,
            hasherFunc,
            difficulty: cnfHandler.getPoWDifficulty()
        });
        console.log('solution', solution);
        // expect(utils.arrayRemoveByIndex(arr, 1)).toEqual([1, 3, 'a', 5, 6]);
    });



});


