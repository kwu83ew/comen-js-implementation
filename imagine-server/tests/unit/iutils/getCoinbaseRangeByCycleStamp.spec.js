const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.getCoinbaseRangeByCycleStamp', () => {
    if (iConsts.TIME_GAIN == 1) {
        it('Should calc coinbase range ', () => {
            let range;
            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 am')
            expect(range.from).toEqual('2018-08-10 00:00:00')
            expect(range.to).toEqual('2018-08-10 11:59:59')

            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 pm')
            expect(range.from).toEqual('2018-08-10 12:00:00')
            expect(range.to).toEqual('2018-08-10 23:59:59')

        });
    }


    if (iConsts.TIME_GAIN == 5) {

        it('Should calc coinbase range ', () => {
            let range;
            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 0')
            expect(range.from).toEqual('2018-08-10 00:00:00')
            expect(range.to).toEqual('2018-08-10 00:04:59')

            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 1')
            expect(range.from).toEqual('2018-08-10 00:05:00')
            expect(range.to).toEqual('2018-08-10 00:09:59')

            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 02')
            expect(range.from).toEqual('2018-08-10 00:10:00')
            expect(range.to).toEqual('2018-08-10 00:14:59')

            range = iutils.getCoinbaseRangeByCycleStamp('2018-08-10 287')
            expect(range.from).toEqual('2018-08-10 23:55:00')
            expect(range.to).toEqual('2018-08-10 23:59:59')

        });
    }

});