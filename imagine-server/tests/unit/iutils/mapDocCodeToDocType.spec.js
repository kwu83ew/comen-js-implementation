const iutils = require('../../../utils/iutils')


describe('iutils.mapDocCodeToDocType', () => {
    it('Should mapDocCodeToDocType ', () => {
        expect(iutils.mapDocCodeToDocType('Coinbase')).toEqual('Coinbase');
        expect(iutils.mapDocCodeToDocType('DNAProposal')).toEqual('DNAProposal');
    });
});