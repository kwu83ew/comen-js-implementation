

const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.getCoinbaseCheckGap', () => {
    it('Should be 30 minute gap for 3 first hours of coinbase period', () => {
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 00:00:00'))).toEqual(30);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 03:00:00'))).toEqual(60);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 06:00:00'))).toEqual(120);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 10:00:00'))).toEqual(60);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 11:00:00'))).toEqual(30);

        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 12:00:00'))).toEqual(30);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 15:00:00'))).toEqual(60);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 18:00:00'))).toEqual(120);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 22:00:00'))).toEqual(60);
        expect(parseInt(iutils.getCoinbaseCheckGap('2010-01-04 23:00:00'))).toEqual(30);


    });
});

