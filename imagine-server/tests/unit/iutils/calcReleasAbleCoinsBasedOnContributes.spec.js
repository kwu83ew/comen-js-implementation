

const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.calculateReleasableCoinsBasedOnContributesVolume', () => {
    it('Should calc ReleasAble Coins Based On Contributes ', () => {
        let worldPop = iConsts.WORLD_POPULATION;
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(0)).toEqual(1);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 2))).toEqual(1);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 3))).toEqual(1);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 3) + .01)).toEqual(2);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 5))).toEqual(3);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 8))).toEqual(5);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 13))).toEqual(8);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 21))).toEqual(13);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 34))).toEqual(21);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 55))).toEqual(34);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 89) + .01)).toEqual(55);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop)).toEqual(55);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 144))).toEqual(89);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 233))).toEqual(100);
        expect(iutils.calculateReleasableCoinsBasedOnContributesVolume(worldPop / (100 / 1233))).toEqual(100);
    });
});

