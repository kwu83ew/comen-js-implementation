const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.getPrevCoinbaseInfo', () => {
    if (iConsts.TIME_GAIN == 1) {
        it('Should calc coinbase cbInfo ', () => {
            let cbInfo;
            cbInfo = iutils.getPrevCoinbaseInfo('2018-08-10 am')
            expect(cbInfo.from).toEqual('2018-08-10 00:00:00')
            expect(cbInfo.to).toEqual('2018-08-10 11:59:59')

            cbInfo = iutils.getPrevCoinbaseInfo('2018-08-10 pm')
            expect(cbInfo.from).toEqual('2018-08-10 12:00:00')
            expect(cbInfo.to).toEqual('2018-08-10 23:59:59')

        });
    }


    if (iConsts.TIME_GAIN == 5) {

        it('Should calc coinbase cbInfo ', () => {
            let _t, cbInfo;
            _t = '2018-08-10 01:15:00'
            cbInfo = iutils.getPrevCoinbaseInfo(_t);
            expect(cbInfo.cycleStamp).toEqual('2018-08-10 014')
            expect(cbInfo.from).toEqual('2018-08-10 01:10:00')
            expect(cbInfo.to).toEqual('2018-08-10 01:14:59')

            _t = '2018-08-10 01:15:01'
            cbInfo = iutils.getPrevCoinbaseInfo(_t);
            expect(cbInfo.cycleStamp).toEqual('2018-08-10 014')
            expect(cbInfo.from).toEqual('2018-08-10 01:10:00')
            expect(cbInfo.to).toEqual('2018-08-10 01:14:59')

        });
    }

});