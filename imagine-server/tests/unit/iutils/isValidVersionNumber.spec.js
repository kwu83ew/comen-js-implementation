const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('iutils.isValidVersionNumber', () => {

    it('Should calc isValidVersionNumber', () => {
        expect(iutils.isValidVersionNumber('0.0.0')).toEqual(true);
        expect(iutils.isValidVersionNumber('0.0.10')).toEqual(true);
        expect(iutils.isValidVersionNumber('05.0.10')).toEqual(true);
        
        expect(iutils.isValidVersionNumber('v0.0.0')).toEqual(false);
        expect(iutils.isValidVersionNumber('v.0-0-0')).toEqual(false);
        expect(iutils.isValidVersionNumber('0.0.0-0')).toEqual(false);
        expect(iutils.isValidVersionNumber('0.0.0-9')).toEqual(false);
        expect(iutils.isValidVersionNumber('05.0.10d')).toEqual(false);

    });

});


