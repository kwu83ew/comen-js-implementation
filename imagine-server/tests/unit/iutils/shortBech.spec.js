const iutils = require('../../../utils/iutils')


describe('iutils.shortBech', () => {
    it('Should shortBech ', () => {
        expect(iutils.shortBech('im1xp3xgdfcxe3rqdm9xdskyvpsvs6xvc35xqurvd3cxdnrxwfhxgerysm5d0r')).toEqual('im1.5d0r');
    });
});