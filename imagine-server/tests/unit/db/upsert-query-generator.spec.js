const db = require("../../../startup/db2")
const iConsts = require("../../../config/constants")
const getMoment = require("../../../startup/singleton").instance.getMoment;

// TODO more unittests

describe('clauseQueryGenerator ', () => {
    it('Should proper params for clauseQueryGenerator', () => {
        let upsertQ;
        upsertQ = db.upsertQueryGenerator({
            table: 'i_machine_wallet_addresses',
            idField: 'wa_address',
            idValue: iConsts.HU_DNA_SHARE_ADDRESS,
            updates: { wa_title: 'HU_DNA_SHARE_ADDRESS' }
        });

        expect([
            'im1xp3nqvmp8qmrgvmr8pnxxcf38quxgcnrxgunydnrvgukgdpjvgeky0pzdye',
            'HU_DNA_SHARE_ADDRESS',
            'HU_DNA_SHARE_ADDRESS',
        ]).toEqual(upsertQ._values);

    });

});