const db2 = require('../../../startup/db2');
const utils = require('../../../utils/utils');

// TODO more unittests

describe('clauseQueryGenerator ', () => {

    it('Should proper params for only table in query', () => {
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({ table: 'a_table' });
        let _query = 'DELETE FROM a_table' + _clauses;
        expect(_query).toEqual('DELETE FROM a_table');
        expect(_values).toEqual([]);
    });


    it('Should proper params for clauseQueryGenerator', () => {
        let startsFrom = utils.getNow();
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({
            table: 'a_table',
            query: [
                ['send_date', startsFrom]
            ],
            order: [
                ['send_date', 'DESC']
            ]
        });
        expect(_fields).toEqual(' * ');
        expect(_clauses).toEqual(' WHERE  (send_date=$1) ');
        expect(_values).toEqual([startsFrom]);
        expect(_order).toEqual(' ORDER BY  send_date DESC ');
        expect(_limit).toEqual('');
    });

    it('Should proper params for clauseQueryGenerator', () => {
        let startsFrom = utils.getNow();
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({
            table: 'a_table',
            query: [
                ['send_date', ['>=', startsFrom]]
            ],
            order: [
                ['send_date', 'DESC'],
                ['block_hash', 'ASC'],
            ],
            limit: 11
        });
        expect(_fields).toEqual(' * ');
        expect(_clauses).toEqual(' WHERE  (send_date >= $1) ');
        expect(_values).toEqual([startsFrom]);
        expect(_order).toEqual(' ORDER BY  send_date DESC,  block_hash ASC ');
        expect(_limit).toEqual(' LIMIT 11 ');
    });

    it('Should proper params for clauseQueryGenerator', () => {
        let startsFrom = utils.getNow();
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({
            table: 'a_table',
            query: [
                ['send_date', ['>=', startsFrom]],
                ['send_date', ['<', startsFrom]]
            ],
            order: [
                ['send_date', 'DESC'],
                ['block_hash', 'ASC'],
            ],
            limit: 11
        });
        expect(_fields).toEqual(' * ');
        expect(_clauses).toEqual(' WHERE  (send_date >= $1)  AND  (send_date < $2) ');
        expect(_values).toEqual([startsFrom, startsFrom]);
        expect(_order).toEqual(' ORDER BY  send_date DESC,  block_hash ASC ');
        expect(_limit).toEqual(' LIMIT 11 ');
    });



    it('Should proper params for clauseQueryGenerator for "IN" ', () => {
        let startsFrom = utils.getNow();
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({
            table: 'a_table',
            query: [
                ['send_date', ['>=', startsFrom]],
                ['name', ['in', ['Alice']]],
                ['send_date', ['<', startsFrom]]
            ],
            order: [
                ['send_date', 'DESC'],
                ['block_hash', 'ASC'],
            ],
            limit: 11
        });
        expect(_fields).toEqual(' * ');
        // expect(_clauses).toEqual(' WHERE  (send_date >= $1)  AND  (name in ($2))  AND  (send_date < $3) ');
        expect(_clauses).toEqual(" WHERE  (send_date >= $1)  AND  (name IN ($2))  AND  (send_date < $3) ");

        expect(_values).toEqual([startsFrom, 'Alice', startsFrom]);
        expect(_order).toEqual(' ORDER BY  send_date DESC,  block_hash ASC ');
        expect(_limit).toEqual(' LIMIT 11 ');
    });

    it('Should proper params for clauseQueryGenerator for "IN" ', () => {
        let startsFrom = utils.getNow();
        let { _fields, _clauses, _values, _order, _limit } = db2.clauseQueryGenerator({
            table: 'a_table',
            query: [
                ['send_date', ['>=', startsFrom]],
                ['name', ['in', ['Alice', 'Bob']]],
                ['send_date', ['<', startsFrom]]
            ],
            order: [
                ['send_date', 'DESC'],
                ['block_hash', 'ASC'],
            ],
            limit: 11
        });
        expect(_fields).toEqual(' * ');
        // expect(_clauses).toEqual(' WHERE  (send_date >= $1)  AND  (name in ($2, $3))  AND  (send_date < $4) ');
        expect(_clauses).toEqual(" WHERE  (send_date >= $1)  AND  (name IN ($2, $3))  AND  (send_date < $4) ");

        expect(_values).toEqual([startsFrom, 'Alice', 'Bob', startsFrom]);
        expect(_order).toEqual(' ORDER BY  send_date DESC,  block_hash ASC ');
        expect(_limit).toEqual(' LIMIT 11 ');
    });

    it('Should proper params for updateQueryGenerator for "IN" ', () => {
        let { _query, _values } = db2.updateQueryGenerator({
            table: 'a_table',
            query: [
                ['date1', ['>=', 'date1']],
                ['name', ['in', ['Alice', 'Bob']]],
                ['date2', ['<', 'date2']]
            ],
            updates: {
                updFiled1: 'updValue1',
            }
        });
        // console.log('_query', _query);
        // console.log('_values', _values);
        expect(_query).toEqual('UPDATE a_table  SET updFiled1=$1  WHERE  (date1 >= $2)  AND  (name IN ($3, $4))  AND  (date2 < $5) ');
        expect(_values).toEqual(['updValue1', 'date1', 'Alice', 'Bob', 'date2']);

    });





});