const iConsts = require('../../../config/constants');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')
const rendererHandler = require('../../../services/render-handler/render-handler');


describe('iutils.renderToHTML', () => {
    let content, rendered;
    it('Should render the wiki-format-links to normal html tag a', () => {
        content = 'general [[mySite/wiki/faq|FAQ]] and write your[[comments]]';
        rendered = 'general <a href="mySite/wiki/faq">FAQ</a> and write your<a href="comments">comments</a>';
        expect(rendererHandler.renderToHTML(content)).toEqual(rendered);
    });

    it('Should render the wiki-format-links for img', () => {
        content = 'this is [[File:earth.jpg]]';
        rendered = 'this is <img src="earth.jpg">';
        expect(rendererHandler.renderToHTML(content)).toEqual(rendered);

        content = 'this is [[File:earth.jpg|mother earth]]';
        rendered = 'this is <img src="earth.jpg" title="mother earth" >';
        expect(rendererHandler.renderToHTML(content)).toEqual(rendered);

        content = 'this is image[[File:earth.jpg|border|frame|thumb|left|20px|link=mysite/|alt=the beautiful earth|myCustomizedCaption]]';
        rendered = 'this is image<a href="mysite/"><img src="earth.jpg" title="myCustomizedCaption"  alt="the beautiful earth" ></a>';
        expect(rendererHandler.renderToHTML(content)).toEqual(rendered);
    });

});