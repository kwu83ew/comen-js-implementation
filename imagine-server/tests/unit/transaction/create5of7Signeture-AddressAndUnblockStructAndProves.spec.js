const crypto = require("../../../crypto/crypto")
const mOfNHandler = require("../../../transaction/signature-structure-handler/general-m-of-n-handler");
const mofnPresenter = require("../../../transaction/trx-utils").mofnPresenter;
const utils = require('../../../utils/utils')

// https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t
describe('Genesis', () => {
    it('Should create a 2 of 3 classic transaction', () => {
        let keyPairs = []
        for (let i = 0; i < 7; i++) {
            keyPairs[i] = crypto.generatePairKey()
        }
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: [
                ['1', [keyPairs[0].pubKey]],
                ['2', [keyPairs[1].pubKey]],
                ['3', [keyPairs[2].pubKey]],
                ['4', [keyPairs[3].pubKey]],
                ['5', [keyPairs[4].pubKey]],
                ['6', [keyPairs[5].pubKey]],
                ['7', [keyPairs[6].pubKey]]
            ], neccessarySignaturesCount: 5
        });

        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });

    });
});