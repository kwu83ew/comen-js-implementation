const crypto = require("../../../crypto/crypto")
const mOfNHandler = require("../../../transaction/signature-structure-handler/general-m-of-n-handler");
const mofnPresenter = require("../../../transaction/trx-utils").mofnPresenter;
const addressHandler = require('../../../address/address-handler');

// https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t
describe('m of n ', () => {
    it('Should create a trx unlock structur for 5 signatures', () => {
        let keyPairs = []
        for (let i = 0; i < 5; i++) {
            keyPairs[i] = crypto.generatePairKey()
        }

        let privContainer = {}
        keyPairs.forEach(pairs => {
            // privContainer[pairs.pubKey] = pairs.prvKey
            privContainer = addressHandler.addToDictContainer(privContainer, pairs.pubKey, pairs.prvKey);
        });

        let uSets = [
            [
                [keyPairs[0].pubKey, '10080']
            ],
            [
                [keyPairs[3].pubKey, '0'],
                [keyPairs[1].pubKey, '0'],
                [keyPairs[2].pubKey, '0']
            ],
            [
                [keyPairs[0].pubKey, '0'],
                [keyPairs[1].pubKey, '0'],
                [keyPairs[2].pubKey, '0'],
                [keyPairs[4].pubKey, '0']
            ], // for security use this one

        ];

        let trxUnlockMerkle = mOfNHandler.createMOfNMerkle({ uSets });
        expect(trxUnlockMerkle).not.toEqual('');

        // validate unlock structure
        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });
    });


    it('Should create a 1 of 2 ', () => {
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: [
                ['pub1', '0'],
                ['pub2', '0']
            ],
            neccessarySignaturesCount: 1
        })

        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });
    });


    it('Should create a 2 of 2 ', () => {
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: [
                ['000', ['pub1']],
                ['111', ['pub2']]
            ],
            neccessarySignaturesCount: 2
        })

        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });
    });

    it('Should create a 2 of 3 classic transaction', () => {
        let keyPairs = []
        for (let i = 0; i < 3; i++) {
            keyPairs[i] = crypto.generatePairKey()
        }
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: [
                ['0', [keyPairs[0].pubKey]],
                ['1', [keyPairs[1].pubKey]],
                ['2', [keyPairs[2].pubKey]]
            ],
            neccessarySignaturesCount: 2
        })

        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });
    });

    it('Should create a 3 of 3 classic transaction', () => {
        let keyPairs = []
        for (let i = 0; i < 3; i++) {
            keyPairs[i] = crypto.generatePairKey()
        }
        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            sSets: [
                [keyPairs[0].pubKey, '0'],
                [keyPairs[1].pubKey, '0'],
                [keyPairs[2].pubKey, '0']
            ],
            neccessarySignaturesCount: 3
        })

        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                uSet: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                }
            });
            expect(isValidUnblock).toEqual(true);
        });
    });

});