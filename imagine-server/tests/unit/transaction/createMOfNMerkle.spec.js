const crypto = require("../../../crypto/crypto")
const mOfNHandler = require("../../../transaction/signature-structure-handler/general-m-of-n-handler");
const mofnPresenter = require("../../../transaction/trx-utils").mofnPresenter;
const aliasHash = require('../../../crypto/merkle/merkle').aliasHash;

describe('m of n ', () => {
    it('Should create a trx unlock structur for 2 of 2 signatures', () => {

        let uSets = [
            [
                ["AlicePubKey", '0'],
                ["BobPubKey", '0']
            ],

        ];

        let trxUnlockMerkle = mOfNHandler.createMOfNMerkle({
            uSets,
            inputType: 'hashed',
            hashAlgorithm: aliasHash
        });
        // clog.app.info(mofnPresenter(trxUnlockMerkle));
        expect(trxUnlockMerkle).not.toEqual('');

        // validate unlock structure
        trxUnlockMerkle.uSets.forEach(aSignatureInfo => {
            let isValidUnblock = mOfNHandler.validateStructureOfAnUnlockMOfN({
                unlock: {
                    address: trxUnlockMerkle.accountAddress,
                    sType: aSignatureInfo.sType,
                    sVer: aSignatureInfo.sVer,
                    sSets: aSignatureInfo.sSets,
                    lHash: aSignatureInfo.lHash,
                    proofs: aSignatureInfo.proofs,
                    salt: aSignatureInfo.salt
                },
                inputType: 'hashed',
                hashAlgoritm: aliasHash
            });
            expect(isValidUnblock).toEqual(true);
        });
    });



});