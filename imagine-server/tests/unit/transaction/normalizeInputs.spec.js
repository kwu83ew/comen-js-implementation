// const c = require('../../crypto/crypto');
const trxSamples = require('../../../docs-structure/transaction/simple-transaction-v0.0.0');
const trxHashHandler = require("../../../transaction/hashable");

describe('normalizeInputs', () => {
    it('Should normalize Inputs of a transaction', () => {
        let trx = trxSamples.t1;
        trx.inputs = [
            ['b', 2],
            ['c', 7],
            ['b', 0],
            ['a', 0],
        ];
        trx.outputs = [
            ['im1xqenjep5vccxvwfjxscrxcf4vyerqvf3vy6rzcfcx5ergvfhxaskgue52h3', 7],
            ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 2],
            ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 0],
            ['im1xp3nzvtz8qenyc3jvejnvwp4vsurxep3x9snvvtzx4jrywpk8qcry6p349r', 0],
        ];
        // console.log(trx);
        let inpNroms = trxHashHandler.normalizeInputs(trx.inputs, true);
        expect(inpNroms.normalizedInputs).toEqual([
            ['a', 0],
            ['b', 0],
            ['b', 2],
            ['c', 7]
        ]);

        // let outNroms = trxHashHandler.normalizeOutputs(trx.outputs, true);
        // // console.log(outNroms);
        // expect(outNroms).toEqual([
        //     ['im1xp3nzvtz8qenyc3jvejnvwp4vsurxep3x9snvvtzx4jrywpk8qcry6p349r', 0],
        //     ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 0],
        //     ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 2],
        //     ['im1xqenjep5vccxvwfjxscrxcf4vyerqvf3vy6rzcfcx5ergvfhxaskgue52h3', 7],
        // ]);


    });

    it('Should normalize Outputs of a transaction', () => {
        let trx = trxSamples.t1;
        trx.inputs = [
            ['b', 2],
            ['c', 7],
            ['b', 0],
            ['a', 0],
        ];
        trx.outputs = [
            ['im1xqenjep5vccxvwfjxscrxcf4vyerqvf3vy6rzcfcx5ergvfhxaskgue52h3', 7],
            ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 2],
            ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 0],
            ['im1xp3nzvtz8qenyc3jvejnvwp4vsurxep3x9snvvtzx4jrywpk8qcry6p349r', 0],
        ];
        // console.log(trx);
        let inpNroms = trxHashHandler.normalizeInputs(trx.inputs, true);
        expect(inpNroms.normalizedInputs).toEqual([
            ['a', 0],
            ['b', 0],
            ['b', 2],
            ['c', 7]
        ]);

        // let outNroms = trxHashHandler.normalizeOutputs(trx.outputs, true);
        // // console.log(outNroms);
        // expect(outNroms).toEqual([
        //     ['im1xp3nzvtz8qenyc3jvejnvwp4vsurxep3x9snvvtzx4jrywpk8qcry6p349r', 0],
        //     ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 0],
        //     ['im1xqekzwf5xqmkvd3nx9jkxvr9xycrzde5xgexzdfhx5cxyvn9v4snvk764yc', 2],
        //     ['im1xqenjep5vccxvwfjxscrxcf4vyerqvf3vy6rzcfcx5ergvfhxaskgue52h3', 7],
        // ]);


    });

    it('Should normalize Outputs of a transaction', () => {
        let outputs = [
            [
                "im1xpjrgdf4vd3njvpcxy6xgenpx56rycesxf3kyenxxesnyet9xpjnyuup298",
                3725512915
            ],
            [
                "im1xqunvcnx8y6rzetxvgexxdrzx4sn2vp5vscnwwp4vccrqvrrx33rxxrcw9k",
                25197064674
            ],
            [
                "im1xp3rqdnpxdskywp4v33nqvnyxy6ryd3c8qmkxvesv56xgvpnvejrqfnwv23",
                97614985560
            ],
            [
                "im1xqcrqwtrxajrqdpjxanx2wtz8yerzcf5xcurxdmzx9nrwwr9xu6rjazuh5x",
                16806581
            ],
            [
                "im1xpjrgdf4vd3njvpcxy6xgenpx56rycesxf3kyenxxesnyet9xpjnyuup298",
                31881430762
            ],
        ];

        let outNorm = trxHashHandler.normalizeOutputs(outputs, true);
        console.log(outNorm);
        expect(outNorm).toEqual([
            [
                "im1xp3rqdnpxdskywp4v33nqvnyxy6ryd3c8qmkxvesv56xgvpnvejrqfnwv23",
                97614985560
            ],
            [
                "im1xpjrgdf4vd3njvpcxy6xgenpx56rycesxf3kyenxxesnyet9xpjnyuup298",
                3725512915
            ],
            [
                "im1xpjrgdf4vd3njvpcxy6xgenpx56rycesxf3kyenxxesnyet9xpjnyuup298",
                31881430762
            ],
            [
                "im1xqcrqwtrxajrqdpjxanx2wtz8yerzcf5xcurxdmzx9nrwwr9xu6rjazuh5x",
                16806581
            ],
            [
                "im1xqunvcnx8y6rzetxvgexxdrzx4sn2vp5vscnwwp4vccrqvrrx33rxxrcw9k",
                25197064674
            ]
        ]);


    });

});

