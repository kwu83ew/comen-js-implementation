

const iConsts = require('../../../config/constants');
const cnfHandler = require('../../../config/conf-params');
const utils = require('../../../utils/utils')
const iutils = require('../../../utils/iutils')


describe('cnfHandler.getDocExpense', () => {
    it('Should calc getDocExpense ', () => {
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 1000 })).toBeGreaterThan(11);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 1000 })).toBeLessThan(12);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 10000 })).toBeGreaterThan(12);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 10000 })).toBeLessThan(13.5);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 20000 })).toBeGreaterThan(16);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 20000 })).toBeLessThan(16.5);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 50000 })).toBeGreaterThan(38);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 50000 })).toBeLessThan(39);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 80000 })).toBeGreaterThan(223);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 80000 })).toBeLessThan(224);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 90000 })).toBeGreaterThan(954);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 90000 })).toBeLessThan(955);

        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 99999 })).toEqual(null);
        expect(cnfHandler.getDocExpense({ dType: iConsts.DOC_TYPES.BasicTx, dLen: 99999 })).toEqual(null);

    });



});
