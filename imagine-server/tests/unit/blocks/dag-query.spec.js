const prepareDAGQuery = require('../../../dag/graph-handler/dag-handler').prepareDAGQuery;
const utils = require('../../../utils/utils');


describe('prepareDAGQuery', () => {
    it('Empty args, Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({});
        expect(_query.trim()).toBe("SELECT  *  FROM i_blocks");
    });

    it('limit, Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            limit: 11
        });
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT  *  FROM i_blocks  LIMIT 11 "));
    });

    it('limit & order, Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            limit: 11,
            order: [
                ['b_send_date', 'DESC'],
                ['b_hash', 'ASC'],
            ]
        });
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT  *  FROM i_blocks  ORDER BY  b_send_date DESC,  b_hash ASC  LIMIT 11 "));
    });

    it('filelds, Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            fields: ["b_hash", "ancestors", "body", "b_creation_date", "receive_date", "confirm_date"]
        });
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT b_hash, ancestors, body, b_creation_date, receive_date, confirm_date FROM i_blocks "));
    });

    it('custom fields(b_hash list), Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            fields: ["b_hash", "ancestors", "body"],
            query: [
                ['b_hash', ['IN', ["hash1", "hash2"]]]
            ]
        });
        // expect(_query).toBe("SELECT b_hash, ancestors, body FROM i_blocks  WHERE  b_hash IN ($1,$2) ");
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT b_hash, ancestors, body FROM i_blocks  WHERE  (b_hash IN ($1, $2)) "));
        expect(_values.length).toBe(2);
        expect(_values[0]).toBe("hash1");
        expect(_values[1]).toBe("hash2");
    });

    it('custom operators(<, >, <=, >=), Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            fields: ["b_hash", "ancestors", "b_creation_date"],
            query: [
                ['b_creation_date', ["<", 'today']]
            ]
        });
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT b_hash, ancestors, b_creation_date FROM i_blocks  WHERE  (b_creation_date < $1) "));
        expect(_values.length).toBe(1);
        expect(_values[0]).toBe("today");
    });

    it('custom fields & custom operators(<, >, <=, >=), Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            fields: ["b_hash", "ancestors", "b_creation_date"],
            query: [
                ['b_hash', ['IN', ["hash1", "hash2"]]],
                ['b_creation_date', ["<", 'today']],
            ]
        });
        // expect(_query).toBe("SELECT b_hash, ancestors, b_creation_date FROM i_blocks  WHERE  (b_creation_date < $1)  AND  b_hash IN ($2,$3) ");
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT b_hash, ancestors, b_creation_date FROM i_blocks  WHERE  (b_hash IN ($1, $2))  AND  (b_creation_date < $3) "));
        expect(_values.length).toBe(3);
        expect(_values[0]).toBe("hash1");
        expect(_values[1]).toBe("hash2");
        expect(_values[2]).toBe("today");
    });

    // same above with diffrent query order
    it('custom fields & custom operators(<, >, <=, >=), Should prepare query for search in dag', () => {
        let { _query, _values } = prepareDAGQuery({
            fields: ["b_hash", "ancestors", "b_creation_date"],
            query: [
                ['b_creation_date', ["<", 'today']],
                ['b_hash', ['in', ["hash1", "hash2"]]],
            ]
        });
        // expect(_query).toBe("SELECT b_hash, ancestors, b_creation_date FROM i_blocks  WHERE  (b_creation_date < $1)  AND  b_hash IN ($2,$3) ");
        expect(utils.normalizeSpaces(_query)).toBe(utils.normalizeSpaces("SELECT b_hash, ancestors, b_creation_date FROM i_blocks  WHERE  (b_creation_date < $1)  AND  (b_hash IN ($2, $3)) "));
        expect(_values.length).toBe(3);
        expect(_values[0]).toBe("today");
        expect(_values[1]).toBe("hash1");
        expect(_values[2]).toBe("hash2");
    });

})