const iConsts = require("../../../config/constants");
const loanContractHandler = require("../../../contracts/loan-contract");



let loan;
describe('loanContractHandler', () => {
    it('Should calcLoanRepayments 0% interest', () => {
        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 0,
            repaymentAmount: 50,
            repaymentSchedule: 365 * 2
        });
        expect(loan.repaymentsNumber).toBe(2);
        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[1].balance).toBe(50);
        expect(loan.repayments[1].newBalance).toBe(0);

        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 0,
            repaymentAmount: 60,
            repaymentSchedule: 365 * 2
        });
        expect(loan.repaymentsNumber).toBe(2);
        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[1].balance).toBe(40);
        expect(loan.repayments[1].newBalance).toBe(0);


        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 0,
            repaymentAmount: 30,
            repaymentSchedule: 365 * 2
        });
        expect(loan.repaymentsNumber).toBe(4);
        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[1].balance).toBe(70);
        expect(loan.repayments[2].balance).toBe(40);
        expect(loan.repayments[3].balance).toBe(10);
        expect(loan.repayments[3].newBalance).toBe(0);

    });

    it('Should calcLoanRepayments 10% interest', () => {
        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 10,
            repaymentAmount: 50,
            repaymentSchedule: 12,
            percision: 2
        });
        expect(loan.repaymentsNumber).toBe(3);
        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[0].paidInterest).toBe(0.83);
        expect(loan.repayments[0].paidPrincipal).toBe(49.17);
        expect(loan.repayments[0].repaymentAmount).toBe(50);

        expect(loan.repayments[1].balance).toBe(50.83);
        expect(loan.repayments[1].paidPrincipal).toBe(49.58);
        expect(loan.repayments[1].paidInterest).toBe(0.42);

        expect(loan.repayments[2].paidInterest).toBe(0.01);
        expect(loan.repayments[2].paidPrincipal).toBe(1.24);
        expect(loan.repayments[2].repaymentAmount).toBe(1.25);
        expect(loan.repayments[2].newBalance).toBe(0);

    });

    it('Should calcLoanRepayments 10% interest long term', () => {
        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 10,
            repaymentAmount: 10,
            repaymentSchedule: 12,
            percision: 2
        });
        expect(loan.repaymentsNumber).toBe(11);
        expect(loan.totalRePaymentAmount).toBe(104.78);

        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[0].paidInterest).toBe(0.83);
        expect(loan.repayments[0].paidPrincipal).toBe(9.17);
        expect(loan.repayments[0].repaymentAmount).toBe(10);

        expect(loan.repayments[1].balance).toBe(90.83);
        expect(loan.repayments[1].paidInterest).toBe(0.75);
        expect(loan.repayments[1].paidPrincipal).toBe(9.25);

        expect(loan.repayments[2].balance).toBe(81.58);
        expect(loan.repayments[2].paidInterest).toBe(0.67);
        expect(loan.repayments[2].paidPrincipal).toBe(9.33);
        
        expect(loan.repayments[10].balance).toBe(4.75);
        expect(loan.repayments[10].paidInterest).toBe(0.03);
        expect(loan.repayments[10].paidPrincipal).toBe(4.75);
        expect(loan.repayments[10].repaymentAmount).toBe(4.78);
        expect(loan.repayments[10].newBalance).toBe(0);

    });
    
    it('Should calcLoanRepayments 12% interest long term', () => {
        loan = loanContractHandler.calcLoanRepayments({
            principal: 100,
            annualInterest: 12,
            repaymentAmount: 10,
            repaymentSchedule: 12,
            percision: 2
        });
        expect(loan.repaymentsNumber).toBe(11);
        expect(loan.totalRePaymentAmount).toBe(105.84);

        expect(loan.repayments[0].balance).toBe(100);
        expect(loan.repayments[0].paidInterest).toBe(1);
        expect(loan.repayments[0].paidPrincipal).toBe(9);
        expect(loan.repayments[0].repaymentAmount).toBe(10);

        expect(loan.repayments[1].balance).toBe(91);
        expect(loan.repayments[1].paidInterest).toBe(0.91);
        expect(loan.repayments[1].paidPrincipal).toBe(9.09);

        expect(loan.repayments[2].balance).toBe(81.91);
        expect(loan.repayments[2].paidInterest).toBe(0.81);
        expect(loan.repayments[2].paidPrincipal).toBe(9.19);

        expect(loan.repayments[3].balance).toBe(72.72);
        expect(loan.repayments[3].paidInterest).toBe(0.72);
        expect(loan.repayments[3].paidPrincipal).toBe(9.28);
        
        expect(loan.repayments[10].balance).toBe(5.79);
        expect(loan.repayments[10].paidInterest).toBe(0.05);
        expect(loan.repayments[10].paidPrincipal).toBe(5.79);
        expect(loan.repayments[10].repaymentAmount).toBe(5.84);
        expect(loan.repayments[10].newBalance).toBe(0);

    });

    // it('Should calcLoanRepayments 10% interest long term', () => {
    //     loan = loanContractHandler.calcLoanRepayments({
    //         principal: 10000000,
    //         annualInterest: 4,
    //         repaymentAmount: 227272.7,
    //         repaymentSchedule: 12,
    //         percision: 3
    //     });
    // });

})