const crypto = require('../../../crypto/crypto');
const CONSTANTS = require('../../../config/constants');

describe('bech32', () => {
    it('Should encode then decod and return same text', () => {
        ['', '1', 'q', 'ij8yhdy475fhwi', '3', 'iJk',
            'de9i9ihuyuyguyguygjnjnjhgyu7cxdxzyaytsfystyasf87'
        ].forEach(pkey => {
            let enc = crypto.bech32_encode(pkey, 'im');
            let reDec = crypto.bech32_decode(enc.encoded);
            expect(reDec.decoded).toBe(pkey);
            expect(reDec.decoded).toEqual(pkey);
        });
    });

    it('Should fail on longer than 90 char input', () => {
        ['ij8yhdy475fhwidjkfkadfhkdjfkdajfhkajdhkajshdfkjhdde9i9ihuyuyguyguygjnjnjhgyu7cxdxzyaytsfystyasf87'].forEach(pkey => {
            let enc = crypto.bech32_encode(pkey, 'im');
            expect(enc.err != false).toEqual(crypto.bech32_MAX_LEN_ERR);
            expect(enc.encoded).toBeNull();
        });
    });

    it('Should decode correctly', () => {
        ['im1csry39', 'im1xyhc49h7', 'im1wy5xk2a8', 'abcdef1qpzry9x8gf2tvdw0s3jn54khce6mua7lmqqqxw'].forEach(s => {
            let dec = crypto.bech32_decode(s);
            expect(dec.err != false).toBeNull();
        });
    });

    it('Should decode valide addresses correctly', () => {
        [
            'im1xp3nyce3vgergenyv93xxcenxesnqetrv5mnwvnrvdnrzcfsv9jkvc33xycnxepjxcdxjcnh',
            'im1xpnrvvt9v4jk2c3sxs6nxd3j89nxyer98p3kxcfjvd3xgwtpx5ekvdmrvyuqexlwyx',
            'im1xq6x2ef3xvenjef3xa3kgctxxcenycmxv9jrwwtyv5crqvecvcekywf4jl7re0',
            'im1xpjkyet9xgunxcn9xpnrsdp4xcergwp5xd3rzdtrvy6nxwf3xdsnquxr2e6',
            'im1x9nrjcecxqunzwtxxgenzvmrv5unsvenv43kze3kvgmxxep3v5ux2z5k8j9',
            'im18ymxvd34vcckye3exejrqdnrxvmkzd3nxv6rjepcxfjrzwtzxcer22u4yqn',
            'tb1qrp33g0q5c5txsp9arysrx4k6zdkfs4nce4xj0gdcccefvpysxf3q0sl5k7',
            'BC1SW50QA3JX3S',
            'tb1qqqqqp399et2xygdj5xreqhjjvcmzhxw4aywxecjdzew6hylgvsesrxh6hy'
        ].forEach(s => {
            let dec = crypto.bech32_decode(s.toLowerCase());
            expect(dec.err != false).toBeNull();
        });
    });

    it('Should fail  invalide addresses correctly', () => {
        [
            'BC1QW508D6QEJXTDG4Y5R3ZARVARY0C5XW7KV8F3T4',
            'bc1pw508d6qejxtdg4y5r3zarvary0c5xw7kw508d6qejxtdg4y5r3zarvary0c5xw7k7grplx',
            'bc1zw508d6qejxtdg4y5r3zarvaryvg6kdaj'
        ].forEach(s => {
            let dec = crypto.bech32_decode(s.toLowerCase());
            expect(dec.err.length != 0).toBe(true)
        });
    });

    it('Should fail because of Upper Case chars', () => {
        ['im1cSry39', 'im1Xyhc49h7', 'im1wy5xk2A8', 'abcdef1qpzry9x8gF2tvdw0s3jn54khce6mua7lmqqqxw'].forEach(s => {
            let dec = crypto.bech32_decode(s);
            expect(dec.err != false).toEqual(crypto.bech32_LOWERCASE_ERR);
            expect(dec.decoded).toBeNull();
        });
    });

    //09715043005464e54d425ac8cd59fdb82990944cc39030c2b79e0169df115ddf      private key
    //03f84de85e612e3e89e8ee5a49c0c243dddceb048bd8e979d77e795ba2d4d377bb    publick key
    //e2fc205742d477732531c47d130c41f553f06b9b9fb59848919e2af60e3890be      sha256 of publick key
    //17d8a5d8135a47d2c4f8b2cb6f03ca3df16063d70                             trunacted 41
    //0.0.017d8a5d8135a47d2c4f8b2cb6f03ca3df16063d70                        version added 
    //im1xqhrqt3sxymkgwrpx4jrsvfnx4sngdmyxf3nge3cvgexxc3kvccrxcmpxdjxvvfkxqd3493k   bech32

    it('Should generate a pair key and encode publicKey then decode it', () => {
        let pariKey = crypto.generatePairKey();
        let enc = crypto.bech32_encodePub(pariKey.pubKey);
        let dec = crypto.bech32_decodeAddress(enc.encoded);
        expect(dec.err != false).toBeNull();
        expect(dec.prefix).toEqual('im');
        expect(dec.decoded).toEqual((CONSTANTS.BECH32_ADDRESS_VER + pariKey.pubKey).substr(0, CONSTANTS.TRUNCATE_FOR_BECH32_ADDRESS));
    });

    it('Should validate bech32 addresses', () => {
        let pariKey = crypto.generatePairKey();
        let enc = crypto.bech32_encodePub(pariKey.pubKey);
        let dec = crypto.bech32_decodeAddress(enc.encoded);
        expect(dec.err != false).toBeNull();
        expect(dec.prefix).toEqual('im');
        expect(dec.decoded).toEqual((CONSTANTS.BECH32_ADDRESS_VER + pariKey.pubKey).substr(0, CONSTANTS.TRUNCATE_FOR_BECH32_ADDRESS));
    });

});