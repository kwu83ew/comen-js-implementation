const _ = require('lodash');
// const merkle = require('../../../../crypto/merkle/merkle').merkle;
// const merkleHandler = require('../../../../crypto/merkle/merkle');
// const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
// const noHash = require('../../../../crypto/merkle/merkle').noHash;
// const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
// const keccak256 = require('../../../../crypto/keccak').keccak256;
const blockUtils = require('../../../../dag/block-utils');


describe('Merkle', () => {
    it('Should calculate and verify', () => {
        let proofs = blockUtils.getDocumentMerkleProof({
            block,
            docHash: '9f24d12315a76b7241ca5fc392d6bc823fe9933379801d48e49a579792b673a1'
        });
        let proved = blockUtils.verifyDocumentMerkleProof({
            docsRootHash: block.docsRootHash,
            docHash: '9f24d12315a76b7241ca5fc392d6bc823fe9933379801d48e49a579792b673a1',
            proofs
        });
        expect(proved).toEqual(true);
    });
});

let block = {
    "net": "im",
    "bVer": "0.0.0",
    "bType": "Normal",
    "descriptions": "",
    "blockLength": "0004843",
    "blockHash": "ecc18555cb51c01ece744eb86835592fd88d77292a54812fa45be346863eb6ba",
    "ancestors": [
        "48ba8cb0bb7fb1283cfdfc19ddd3bd27fffa3829cdbd4e1140b6933945479e55",
        "c4e512c91cc9b38225445458e1ced5efdc88076c4d2a5f4bc180197d1e0b1b82",
        "ca71d6125e009a7b1078d6b3f23ed41ed23f200bb95d4c0fd0c07cfdb4b815b7",
        "eb5e378ec998a3ec61ec6312596709e253ffa73b5a759148442b865609bf721c"
    ],
    "signals": [
    ],
    "fVotes": [],
    "backer": "im1xqmryc3cvscnxdrpxyex2vrxvc6xxwf4vcenvvnp8pjrvdpe8ycrvvzlqa6",
    "bExtHash": "84a9922170a4af6dada9d1235499be49df77336b371d6557020b98eb5c402fe1",
    "docsRootHash": "9ebbc600515bb2f801476d4bcb078bcd97ba1c3ce565bb2ebc5bc0863fafc264",
    "docs": [
        {
            "hash": "abc2210a7e99ea4cf3499beb62399aff4d04e99517ad626662737d357108eacf",
            "dType": "DPCostPay",
            "dClass": "",
            "dVer": "0.0.0",
            "description": "Data & Process Cost Payment",
            
        },
        {
            "hash": "b40985a77dbcd48025ff0e78c4055ba14cea6314bce3ed073f14105f762bade6",
            "dLen": "0002059",
            "dType": "BasicTx",
            "dClass": "SimpleTx",
            "dPIs": [
                2
            ],
        },
        {
            "hash": "9f24d12315a76b7241ca5fc392d6bc823fe9933379801d48e49a579792b673a1",
            "dLen": "0000998",
            "dType": "INameReg",
            "dClass": "NoDomain",
            "dVer": "0.0.0",
        }
    ],
    
}