const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/crypto').merkleGetRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should return the root for 100', () => {
        // Generate the tree
        let leaves = [];
        for (let i = 0; i < 100; i++) {
            leaves.push(keccak256(i.toString()))
        }
        let m = merkle(leaves, 'hashed', keccak256);
        // clog.app.info(merklePresenter(m));
        expect(m.root).toEqual(getRootByAProve(keccak256("1"), m.proofs[keccak256("1")].hashes, m.proofs[keccak256("1")].lHash, 'hashed', keccak256))
        expect(m.root).toEqual(getRootByAProve(keccak256("50"), m.proofs[keccak256("50")].hashes, m.proofs[keccak256("50")].lHash, 'hashed', keccak256))
        expect(m.root).toEqual(getRootByAProve(keccak256("71"), m.proofs[keccak256("71")].hashes, m.proofs[keccak256("71")].lHash, 'hashed', keccak256))
            // expect(m.root).toEqual("a")
            // expect(m.proofs).toEqual(null)
    });



});