const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should generate merkle tree root for 1,2,3,4,5 noHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5"
        ], 'hashed', noHash);
        expect(m.root).toEqual("12345555");
        // clog.app.info(merklePresenter(m));
        // clog.app.info(_.map(m.proofs["1"].hashes, "val"));
        expect(m.proofs["1"].hashes).toEqual(["r.2", "r.34", "r.5555"])
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs["4"].hashes, m.proofs["4"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("5", m.proofs["5"].hashes, m.proofs["5"].lHash, 'hashed', noHash))
    });
    it('Should generate merkle tree root for 1,2,3,4,5 aliasHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5"
        ], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(h(12)h(34))h(h(55)h(55)))")
    });
    it('Should generate merkle tree root for 1,2,3,4,5 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5"
        ]);
        expect(m.root).toEqual("49b63ec056b90a1ba0bc7f4df188a548be3ae18b7cd23dd9ca09a34388bc69c9")
    });
    it('Should generate merkle tree root for 1,2,3,4,5 keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3", "4", "5"], 'string');
        expect(m.root).toEqual("f8ca5965cf60986b91138aa760a60163ba2553b80470d051aae6a345afc6cb0f")
            // clog.app.info(merklePresenter(m))
        expect(m.root).toEqual(getRootByAProve("1", m.proofs[keccak256("1")].hashes, m.proofs[keccak256("1")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs[keccak256("2")].hashes, m.proofs[keccak256("2")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs[keccak256("3")].hashes, m.proofs[keccak256("3")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs[keccak256("4")].hashes, m.proofs[keccak256("4")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("5", m.proofs[keccak256("5")].hashes, m.proofs[keccak256("5")].lHash, 'string', keccak256))

    });

    it('Should generate merkle tree root', () => {
        // Generate the tree
        let m = merkle([
            "98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0",
            "ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f",
            "3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d",
            "9c005dd47633f54816133136a980dac48968c3ddb1d5c6d4f20d76e2295034ae",
            "c27f85771711ec1c70129714ed5c9083c96f1f12506203f46590c2146a93fae2"
        ]);
        expect(m.root).toEqual("76c68a494d6fd4bd8ece796b3bd9e125d6a29884c0afae5470dbd2162eae75e4")
    });

});