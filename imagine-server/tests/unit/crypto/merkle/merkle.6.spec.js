const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should generate merkle tree root for 1,2,3,4,5,6 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3", "4", "5", "6"], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(h(12)h(34))h(h(56)h(56)))")
        expect(m.root).toEqual("h(h(h(12)h(34))h(h(56)h(56)))");
        expect(m.proofs["1"].hashes).toEqual(["r.2", "r.h(34)", "r.h(h(56)h(56))"])
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs["4"].hashes, m.proofs["4"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("5", m.proofs["5"].hashes, m.proofs["5"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("6", m.proofs["6"].hashes, m.proofs["6"].lHash, 'hashed', aliasHash))

    });
    it('Should generate merkle tree root for 1,2,3,4,5,6 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5", "6"
        ]);
        expect(m.root).toEqual("b0cfa47052627dc1647916b821dde2369f43884399f2447fa67df0551704c8e4")
    });

});