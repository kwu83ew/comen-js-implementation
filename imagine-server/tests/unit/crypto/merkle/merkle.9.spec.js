const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should generate merkle tree root for 1,2,3,4,5,6,7,8,9 aliasHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5", "6", "7", "8", "9"
        ], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(h(h(12)h(34))h(h(56)h(78)))h(h(h(99)h(99))h(h(99)h(99))))");
        // clog.app.info('================== end=================');
        // clog.app.info(merklePresenter(m));
    });
    it('Should generate merkle tree root for 1,2,3,4,5,6,7,8,9 keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3", "4", "5", "6", "7", "8", "9"]);
        expect(m.root).toEqual("d201388ce9f35d859f2c9f212effba0c51c0a663d9f4e3bdc8f5d9dd120a5841")
    });

});