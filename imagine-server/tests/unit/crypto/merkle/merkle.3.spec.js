const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {

    it('Should generate merkle tree root for 1,2,3 noHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'hashed', noHash);
        expect(m.root).toEqual("1233")
        expect(m.proofs["1"].lHash).toEqual(null)
        expect(m.proofs["1"].hashes[0].substring(0, 1)).toEqual("r")
        expect(m.proofs["1"].hashes[0].substring(2)).toEqual("2")
        expect(m.proofs["1"].hashes[1].substring(0, 1)).toEqual("r")
        expect(m.proofs["1"].hashes[1].substring(2)).toEqual("33")

        expect(m.proofs["2"].lHash).toEqual("1")
        expect(m.proofs["2"].hashes[0].substring(2)).toEqual("33")
        expect(m.proofs["2"].hashes[0].substring(0, 1)).toEqual("r")

        expect(m.proofs["3"].lHash).toEqual("3");
        expect(m.proofs["3"].hashes[0].substring(0, 1)).toEqual("l");
        expect(m.proofs["3"].hashes[0].substring(2)).toEqual("12");
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))

    });
    it('Should generate merkle tree root for 1,2,3 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(12)h(33))")
    });
    it('Should generate merkle tree root for 1,2,3 aliasHash direct', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'string', aliasHash);
        expect(m.root).toEqual("h(h(h(1)h(2))h(h(3)h(3)))")
    });
    it('Should generate merkle tree root for 1,2,3 aliasHash direct', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'string');
        expect(m.root).toEqual(keccak256(keccak256(keccak256("1") + keccak256("2")) + keccak256(keccak256("3") + keccak256("3"))))
        expect(m.proofs[keccak256("1")].hashes).toEqual(['r.' + keccak256("2"), 'r.' + keccak256(keccak256("3") + keccak256("3"))]);
        expect(m.root).toEqual(getRootByAProve("1", m.proofs[keccak256("1")].hashes, m.proofs[keccak256("1")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs[keccak256("2")].hashes, m.proofs[keccak256("2")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs[keccak256("3")].hashes, m.proofs[keccak256("3")].lHash, 'string', keccak256))
    });
    it('Should generate merkle tree root for 1,2,3 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3"
        ]);
        expect(m.root).toEqual("04735db037244d0798e86e350ac5b51a913a3778a03c46899bbd5500fe6eec98")
    });

    it('Should generate merkle tree root for 1,2,3 noHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'hashed', noHash);
        expect(m.root).toEqual("1233")
        expect(m.proofs["1"].lHash).toEqual(null)
        expect(m.proofs["1"].hashes[0].substring(0, 1)).toEqual("r")
        expect(m.proofs["1"].hashes[0].substring(2)).toEqual("2")
        expect(m.proofs["1"].hashes[1].substring(0, 1)).toEqual("r")
        expect(m.proofs["1"].hashes[1].substring(2)).toEqual("33")

        expect(m.proofs["2"].lHash).toEqual("1")
        expect(m.proofs["2"].hashes[0].substring(2)).toEqual("33")
        expect(m.proofs["2"].hashes[0].substring(0, 1)).toEqual("r")

        expect(m.proofs["3"].lHash).toEqual("3");
        expect(m.proofs["3"].hashes[0].substring(0, 1)).toEqual("l");
        expect(m.proofs["3"].hashes[0].substring(2)).toEqual("12");
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))

    });
    it('Should generate merkle tree root for 1,2,3 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(12)h(33))")
    });
    it('Should generate merkle tree root for 1,2,3 aliasHash direct', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'string', aliasHash);
        expect(m.root).toEqual("h(h(h(1)h(2))h(h(3)h(3)))")
    });
    it('Should generate merkle tree root for 1,2,3 keccak256 direct', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3"], 'string');
        // clog.app.info(merklePresenter(m));
        expect(m.root).toEqual(keccak256(keccak256(keccak256("1") + keccak256("2")) + keccak256(keccak256("3") + keccak256("3"))))
        expect(m.proofs[keccak256("1")].hashes).toEqual(['r.' + keccak256("2"), 'r.' + keccak256(keccak256("3") + keccak256("3"))]);
        expect(m.root).toEqual(getRootByAProve("1", m.proofs[keccak256("1")].hashes, m.proofs[keccak256("1")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs[keccak256("2")].hashes, m.proofs[keccak256("2")].lHash, 'string', keccak256))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs[keccak256("3")].hashes, m.proofs[keccak256("3")].lHash, 'string', keccak256))
    });
    it('Should generate merkle tree root for 1,2,3 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3"
        ]);
        expect(m.root).toEqual("04735db037244d0798e86e350ac5b51a913a3778a03c46899bbd5500fe6eec98")
    });

    it('Should generate merkle tree root direct keccak 3', () => {
        // Generate the tree
        let m = merkle([
            "98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0",
            "ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f",
            "3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d",
        ]);
        expect(m.root).toEqual(keccak256(
            keccak256("98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f") +
            keccak256("3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d")))
    });

});