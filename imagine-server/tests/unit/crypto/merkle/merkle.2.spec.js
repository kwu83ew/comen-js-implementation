const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {

    it('Should generate merkle tree root for 1,2 noHash', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'hashed', noHash);
        expect(m.root).toEqual("12")
        expect(m.proofs["1"].hashes).toEqual(["r.2"])
        expect(m.proofs["1"].lHash).toEqual(null)
        expect(m.proofs["2"].hashes).toEqual([])
        expect(m.proofs["2"].lHash).toEqual("1")
        expect(m.leaves).toEqual(2)
        expect(m.levels).toEqual(2)
    });

    it('Should generate merkle tree root for 1,2 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'hashed', aliasHash);
        expect(m.root).toEqual("h(12)")
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', aliasHash))

    });

    it('Should generate merkle tree root for 1,2 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'string', aliasHash);
        // expect(m.root).toEqual("h(12)")
        expect(m.proofs["h(1)"].hashes).toEqual(["r.h(2)"])
        expect(m.proofs["h(2)"].hashes).toEqual([])
        expect(m.proofs["h(2)"].lHash).toEqual("h(1)")
    });

    it('Should generate merkle tree root for 1,2 direct keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'string');
        expect(m.root).toEqual(keccak256(keccak256("1") + keccak256("2")))
        expect(m.proofs[keccak256("1")].hashes).toEqual(['r.' + keccak256("2")])
        expect(m.proofs[keccak256("2")].hashes).toEqual([])
    });

    it('Should generate merkle tree root for 1,2 direct keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"]);
        expect(m.root).toEqual(keccak256("12"))
    });

    it('Should generate merkle tree root for 1,2 keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"]);
        expect(m.root).toEqual("7f8b6b088b6d74c2852fc86c796dca07b44eed6fb3daf5e6b59f7c364db14528")
    });

    it('Should generate merkle tree root for 1,2 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'hashed', aliasHash);
        expect(m.root).toEqual("h(12)")
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', aliasHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', aliasHash))

    });

    it('Should generate merkle tree root for 1,2 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'string', aliasHash);
        // clog.app.info(merklePresenter(m));
        expect(m.proofs["h(1)"].hashes).toEqual(["r.h(2)"])
            // expect(m.proofs["h(2)"].hashes).toEqual([])
            // expect(m.proofs["h(2)"].lHash).toEqual("h(1)")
    });

    it('Should generate merkle tree root for 1,2 direct keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"], 'string');
        expect(m.root).toEqual(keccak256(keccak256("1") + keccak256("2")))
        expect(m.proofs[keccak256("1")].hashes).toEqual(['r.' + keccak256("2")])
        expect(m.proofs[keccak256("2")].lHash).toEqual(keccak256("1"))
        expect(m.proofs[keccak256("2")].hashes).toEqual([])
    });

    it('Should generate merkle tree root for 1,2 direct keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"]);
        expect(m.root).toEqual(keccak256("12"))
    });

    it('Should generate merkle tree root for 1,2 keccak256', () => {
        // Generate the tree
        let m = merkle(["1", "2"]);
        expect(m.root).toEqual("7f8b6b088b6d74c2852fc86c796dca07b44eed6fb3daf5e6b59f7c364db14528")
    });

    it('Should generate merkle tree root direct keccak 2', () => {
        // Generate the tree
        let m = merkle([
            "98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0",
            "ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f",
        ]);
        expect(m.root).toEqual(keccak256("98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f"))
    });


});