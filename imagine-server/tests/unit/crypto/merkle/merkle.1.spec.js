const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should return the root for 1', () => {
        // Generate the tree
        let m = merkle([
            "a"
        ], 'hashed', noHash);
        expect(m.root).toEqual("a")
        expect(m.proofs).toEqual(null)
    });
    it('Should generate merkle tree root for 1', () => {
        // Generate the tree
        let m = merkle(["1"], 'string');
        expect(m.root).toEqual(keccak256("1"))
    });


});