const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {
    it('Should generate merkle tree root for 1,2,3,4,5,6,7,8 aliasHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5", "6", "7", "8"
        ], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(h(12)h(34))h(h(56)h(78)))")
    });
    it('Should generate merkle tree root for 1,2,3,4,5,6,7,8 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5", "6", "7", "8"
        ]);
        expect(m.root).toEqual("33b46d5f7ce89248fa6186813bed29411bf35798fe2793e2eb9ae04f8e2caca2")
    });


});