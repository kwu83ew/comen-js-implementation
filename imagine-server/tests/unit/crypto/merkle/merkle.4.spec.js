const merkle = require('../../../../crypto/merkle/merkle').merkle;
const getRootByAProve = require('../../../../crypto/merkle/merkle').getRootByAProve;
const merklePresenter = require('../../../../crypto/merkle/merkle').merklePresenter;
const noHash = require('../../../../crypto/merkle/merkle').noHash;
const aliasHash = require('../../../../crypto/merkle/merkle').aliasHash;
const keccak256 = require('../../../../crypto/keccak').keccak256;
const _ = require('lodash');

describe('Merkle', () => {

    it('Should generate merkle tree root for 1,2,3,4 noHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ], 'hashed', noHash);
        expect(m.root).toEqual("1234")
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs["4"].hashes, m.proofs["4"].lHash, 'hashed', noHash))
    });
    it('Should generate merkle tree root for 1,2,3,4 aliasHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(12)h(34))")
    });
    it('Should generate merkle tree root for 1,2,3,4 direct keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ]);
        expect(m.root).toEqual(keccak256(keccak256("12") + keccak256("34")))
    });
    it('Should generate merkle tree root for 1,2,3,4 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ]);
        expect(m.root).toEqual("24c6c49569242c584966312605780526b3f3de62c9d19f183b58045fad55ee8b")
    });

    it('Should generate merkle tree root for 1,2,3,4,5 noHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4", "5"
        ], 'hashed', noHash);
        expect(m.root).toEqual("12345555");
        expect(m.proofs["1"].hashes).toEqual(["r.2", "r.34", "r.5555"])
        expect(m.proofs["2"].hashes).toEqual(["r.34", "r.5555"])
        expect(m.proofs["3"].hashes).toEqual(["r.4", "l.12", "r.5555"])
        expect(m.proofs["4"].hashes).toEqual(["l.12", "r.5555"])
        expect(m.proofs["5"].hashes).toEqual(["r.55", "l.1234"])
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs["4"].hashes, m.proofs["4"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("5", m.proofs["5"].hashes, m.proofs["5"].lHash, 'hashed', noHash))
    });
    it('Should generate merkle tree root for 1,2,3,4,5 aliasHash', () => {
        // Generate the tree
        let m = merkle(["1", "2", "3", "4", "5"], "hashed", aliasHash);
        expect(m.root).toEqual("h(h(h(12)h(34))h(h(55)h(55)))")
    });



    it('Should generate merkle tree root for 1,2,3,4 noHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ], 'hashed', noHash);
        expect(m.root).toEqual("1234")
        expect(m.root).toEqual(getRootByAProve("1", m.proofs["1"].hashes, m.proofs["1"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("2", m.proofs["2"].hashes, m.proofs["2"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("3", m.proofs["3"].hashes, m.proofs["3"].lHash, 'hashed', noHash))
        expect(m.root).toEqual(getRootByAProve("4", m.proofs["4"].hashes, m.proofs["4"].lHash, 'hashed', noHash))
    });
    it('Should generate merkle tree root for 1,2,3,4 aliasHash', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ], 'hashed', aliasHash);
        expect(m.root).toEqual("h(h(12)h(34))")
    });
    it('Should generate merkle tree root for 1,2,3,4 direct keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ]);
        expect(m.root).toEqual(keccak256(keccak256("12") + keccak256("34")))
    });
    it('Should generate merkle tree root for 1,2,3,4 keccak256', () => {
        // Generate the tree
        let m = merkle([
            "1", "2", "3", "4"
        ]);
        expect(m.root).toEqual("24c6c49569242c584966312605780526b3f3de62c9d19f183b58045fad55ee8b")
    });


});