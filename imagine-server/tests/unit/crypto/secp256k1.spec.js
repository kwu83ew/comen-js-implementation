const crypto = require('../../../crypto/crypto');
const iConsts = require('../../../config/constants');

describe('secp256k1', () => {
    it('Should generate 32 byte random number', () => {
        let r = crypto.rand({ asBytes: true });
        expect(r.length).toEqual(32);
    });

    it('Should generate asBytes 32 Private key', () => {
        let prvKey = crypto.generatePrivateKey({ asBytes: true });
        expect(prvKey.length).toEqual(32);
    });
    it('Should generate asBinString 32 byte Private key', () => {
        let prvKey = crypto.generatePrivateKey({ asBinString: true });
        // clog.app.info('-------------------------asBinString');
        // clog.app.info(prvKey);
        expect(prvKey.length).toEqual(32);
    });
    it('Should generate 64 char Hex Private key', () => {
        let prvKey = crypto.generatePrivateKey();
        // clog.app.info('-------------------------hex');
        // clog.app.info(prvKey);
        expect(prvKey.length).toEqual(64);
    });

    it('Should generate some full 3 format Public key', () => {
        let prvKey = crypto.generatePrivateKey({ asBytes: true });
        let pubKey = crypto.drivePublicKey(prvKey, { full: true });
        expect(pubKey.hex.length).toEqual(66);
        expect(pubKey.bytes.length).toEqual(33);
        expect(pubKey.binString.length).toEqual(33);
    });

    it('Should generate a Pair-Key', () => {
        let pairKey = crypto.generatePairKey();
        expect(pairKey.prvKey.length).toEqual(64);
        expect(pairKey.pubKey.length).toEqual(66);
    });

    it('Should Sign a message, then verify it\'s sign', () => {
        let pairKey = crypto.generatePairKey();
        ['hell000000000000000000000000000000 world',
            'dij i I Iu i w  e 8 8 2 3 4 7 7 9 1 8 * & ( 5%4ghGFhGfh',
            'pplplplpl@@////@@@@@@@@@@@@@@@@;"',
            '\sdewerererw\essddsssss ssdsd,,,,,,,,,,,,r',
            JSON.stringify({
                "type": "transaction",
                "ver": "0.0.0",
                "From": "x",
                "to": "y",
                "value": "102.00809",
                "ref": "sdjfkjshfjshdkjsfjsd",
                "desc": "sdjfkjshfjshdkjsfjsd",
                "vin": [
                    { "trxA.inx0": "293.90009" },
                ],
            })
        ].forEach(msg => {
            mSHA = crypto.keccak256(msg.toString());
            mSHA = mSHA.substring(0, iConsts.SIGN_MSG_LENGTH);
            let sigObj = crypto.signMsg(mSHA, pairKey.prvKey, { asBytes: true });
            let verifyRes = crypto.verifySignature(mSHA, sigObj, pairKey.pubKey);
            expect(verifyRes).toBe(true);
        });

    });

    it('Should Sign (OpenSSL key pair) a message, then verify it\'s sign', () => {
        let pairKeys = [{
                prvKey: '88a377d14e90ad19e8d262bc3ce247e2c4a0e3519b01487d689b97afdcf58cb2',
                pubKey: '043e5c31af4550013046a5904127e658a6576766a355a6c61b5e901d0ca0a3905a5d755c57fbd89dea7090fd34019661428ac80ed4c6da52ad883d69155dcf461f'
            },
            {
                prvKey: '833d6214c5f7bc14b956578ab97005b00dd004cc820a41a595dba6f1adc0551d',
                pubKey: '04ea34067c483776c5b0dc4a5bb6e7f45c99ef8860fe9b41c65d3aa449a7eb9ca29a3283658f52c2a9ca7f0850c45248f71e44e2facd1b3895e2ed0faec245f1ee'
            },
            {
                prvKey: 'ee13a8773bce5dd13964c2a0b99a289aefdf65c39472819e9c0a6fa7bbf0b420',
                pubKey: '04417701a34098d7ac76ae75f19f05cef1c03494ebe000ce848938529c12bbd0d6f09b430619a1c5a7cc2b85cb0e447765cf0ea9a4e7d866c52d6a7ba2cdc976e4'
            },
            {
                prvKey: '9a5e274ba83ed5991f5c55ea71d0f8003d69e67964242bfb231db06f1cb83d05',
                pubKey: '04371ec1c23927e2ac4381a6fee9199195a3294452a1758d8596a3471f4d295d848cec7df67e335bfcafaaad334ac3161893d2b9ef7035f616e94366fc6f7bd645'
            },
            {
                prvKey: 'aea12019f6805bda74de513662ce424a02af1d3559f8bc991a5e191ad0a80003',
                pubKey: '046eb4d16f48cf1fe31ce94f0c072b7a2179b325c1746980dfd2c471ac1afea01047d4bb2d10d47c8aefe3fc60a020810c2a3d81e00a4698c91ffeeb15e008e674'
            },

        ];
        pairKeys.forEach(pairKey => {
            ['hell000000000000000000000000000000 world',
                'dij i I Iu i w  e 8 8 2 3 4 7 7 9 1 8 * & ( 5%4ghGFhGfh',
                'pplplplpl@@////@@@@@@@@@@@@@@@@;"',
                '\sdewerererw\essddsssss ssdsd,,,,,,,,,,,,r',
                JSON.stringify({
                    "type": "transaction",
                    "ver": "0.0.0",
                    "From": "x",
                    "to": "y",
                    "value": "102.00809",
                    "ref": "sdjfkjshfjshdkjsfjsd",
                    "desc": "sdjfkjshfjshdkjsfjsd",
                    "vin": [
                        { "trxA.inx0": "293.90009" },
                    ],
                })
            ].forEach(msg => {
                mSHA = crypto.keccak256(msg.toString());
                mSHA = mSHA.substring(0, iConsts.SIGN_MSG_LENGTH);
                let sigObj = crypto.signMsg(mSHA, pairKey.prvKey, { asBytes: true });
                let verifyRes = crypto.verifySignature(mSHA, sigObj, pairKey.pubKey);
                expect(verifyRes).toBe(true);
            });

        });

    });

});