const c = require('../../../crypto/crypto');

describe('sha256', () => {
    it('Should hash some strings to sha256 string', () => {
        ['', '1', 'q', 'ij8yhdy475fhwi', '3', 'iJk', 'de9i9ihuyuyguyguygjnjnjhgyu7cxdxzyaytsfystyasf87'].forEach(s => {
            let r = c.sha256(s);
            expect(r.length).toEqual(64);
        });
    });

    it('Should hash some strings to sha256 asByte', () => {
        ['', '1', 'q', 'ij8yhdy475fhwi', '3', 'iJk', 'de9i9ihuyuyguyguygjnjnjhgyu7cxdxzyaytsfystyasf87'].forEach(s => {
            let r = c.sha256(s, { asBytes: true });
            expect(r.length).toEqual(32);
        });
    });
    it('Should hash some strings to sha256.x2 asByte', () => {
        ['', '1', 'q', 'ij8yhdy475fhwi', '3', 'iJk', 'de9i9ihuyuyguyguygjnjnjhgyu7cxdxzyaytsfystyasf87'].forEach(s => {
            let r = c.sha256.x2(s, { asBytes: true });
            expect(r.length).toEqual(32);
        });
    });

});

//
//     let h = sha256('hello', { asBytes: true });
// clog.app.info(h);