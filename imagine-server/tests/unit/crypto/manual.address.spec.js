const iConsts = require('../../../config/constants');
const crypto = require('../../../crypto/crypto');
const secp256k1 = require('secp256k1');
const secp256k1W = require('../../../crypto/secp256k1');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');


describe('create & test manula address', () => {

    it('Should generate a random private key, drive public key, sign message, then verify signature', () => {
        let dummyPrvKey, dummyPubKey;

        // let newPrvKey = crypto.generatePrivateKey({ full: true });
        // console.log('newPrvKey', newPrvKey);
        // let newPubKey = crypto.drivePublicKey(newPrvKey.bytes, { full: true });
        // console.log('newPubKey', newPubKey.hex);
        // expect(newPubKey.hex.length).toEqual(66);
        // expect(newPubKey.bytes.length).toEqual(33);
        // expect(newPubKey.binString.length).toEqual(33);



        // dummyPrvKey = Buffer.from([0xc4, 0x7f, 0xea, 0x9a, 0x0c, 0x5d, 0xf6, 0xa9, 0x6e, 0x71, 0x9f, 0xd5, 0x01, 0xc7, 0x93, 0x52, 0x71, 0x64, 0xdb, 0x95, 0x1d, 0x41, 0xfd, 0x5e, 0xa4, 0x5a, 0xe8, 0xd6, 0x0b, 0x23, 0x28, 0x20]);
        // dummyPubKey = crypto.drivePublicKey(dummyPrvKey, { full: true });
        // console.log('dummyPubKey', dummyPubKey.hex);

        /**
         * newPrvKey {
              binString: 'Äê\f]ö©nqÕ\u0001ÇRqdÛ\u001dAý^¤ZèÖ\u000b#( ',
              hex: 'CB117580CE0137B6069568BE5B6230786D6C6F1CD2D5F523579122D3C7C70561',
              bytes: <Buffer c4 7f ea 9a 0c 5d f6 a9 6e 71 9f d5 01 c7 93 52 71 64 db 95 1d 41 fd 5e a4 5a e8 d6 0b 23 28 20>
            }
        
          console.log tests/unit/crypto/manual.address.spec.js:16
            newPubKey {
              binString: '\u0003Ìmð«üÎ\u0005ïf¬,ÃLi.Ç×(yë²ûr§Ð*l¨',
              hex: '03cc826df0abfcce05ef66ac2cc34c692ec7d7932879eb81b2fb72a7d08f2a6ca8',
              bytes: <Buffer 03 cc 82 6d f0 ab fc ce 05 ef 66 ac 2c c3 4c 69 2e c7 d7 93 28 79 eb 81 b2 fb 72 a7 d0 8f 2a 6c a8>
            }
        
         */


        var buf = Buffer.from('d263A9EAC5417A33879BaE71F28BDE3B0FAC373C83F60B2f0B9EC3C9059DC417', 'hex');
        console.log('manBuf1', buf.length, buf.toString('utf8'), buf);
        let newPubKey1 = crypto.drivePublicKey(buf, { full: true });
        console.log('newPubKey1', newPubKey1);


        dummyPrvKey = [0x00, 0x01, 0x02, 0x46, 0x61, 0x69, 0x72, 0x45, 0x66, 0x66, 0x6f, 0x72, 0x74, 0x2c, 0x46, 0x61, 0x69, 0x72, 0x47, 0x61, 0x69, 0x6e, 0x2c, 0x57, 0x69, 0x6e, 0x57, 0x69, 0x6e, 0x57, 0x69, 0x6e]
        dummyPrvKey = Buffer.from(dummyPrvKey);
        console.log(dummyPrvKey.toString('utf8'), 'Private: ', dummyPrvKey.toString('hex'));
        expect(secp256k1.privateKeyVerify(dummyPrvKey)).toEqual(true);
        dummyPubKey = secp256k1.publicKeyCreate(dummyPrvKey);
        expect(dummyPubKey.length).toEqual(33);
        console.log('dummyPubKey', dummyPubKey.toString('hex'), dummyPubKey);
        let newPubKey = crypto.drivePublicKey(dummyPrvKey, { full: true });
        console.log('newPubKey', newPubKey);



        let keyPairs = []
        let prvKeys = [
            [0xf7, 0xff, 0x01, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x64],
            [0xf8, 0xff, 0x01, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x64],
            [0xf9, 0xff, 0x01, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x64],
        ]
        for (let aPrvKey of prvKeys) {


            expect(aPrvKey.length).toEqual(32);
            aPrvKeyBuf = Buffer.from(aPrvKey);
            expect(secp256k1.privateKeyVerify(aPrvKeyBuf)).toEqual(true);
            let aPubKey = secp256k1.publicKeyCreate(aPrvKeyBuf);
            expect(aPubKey.length).toEqual(33);

            // console.log(`aPrvKey: `, aPrvKey.length, aPrvKey.toString('hex'));
            // console.log(`aPubKey: `, aPubKey.length, aPubKey.toString('hex'));
            // 

            keyPairs.push({ prvKey: aPrvKeyBuf.toString('hex'), pubKey: aPubKey.toString('hex') });
        }
        // console.log('keyPairs', keyPairs);

        let sSets = [];
        let pubToPrvMap = {};
        let signatureType = iConsts.SIGNATURE_TYPES.Strict;
        let signatureVersion = '0.0.1';
        let mSignaturesCount = 2;
        let privContainer = {};
        for (let i = 0; i < keyPairs.length; i++) {
            pubToPrvMap[keyPairs[i].pubKey] = keyPairs[i].prvKey
            let aSimpleDefaultSignSet = {
                sKey: keyPairs[i].pubKey,
                pPledge: iConsts.CONSTS.NO,
                pDelegate: iConsts.CONSTS.NO
            }; // it is default of only existing pubKey{sKey:publkey}
            if (i == 0) {
                aSimpleDefaultSignSet.pPledge = iConsts.CONSTS.YES;  // only one signature permitted to pledge account
                aSimpleDefaultSignSet.pDelegate = iConsts.CONSTS.YES;  // only one signature permitted to delegate 
            }
            sSets.push([iutils.doHashObject(aSimpleDefaultSignSet), aSimpleDefaultSignSet]);
        }

        let trxUnlockMerkle = mOfNHandler.createCompleteUnlockSets({
            customSalt: 'PURE_LEAVE',
            sType: signatureType,
            sVer: signatureVersion,
            sSets,
            neccessarySignaturesCount: mSignaturesCount
        });

        for (let anUnlockerSet of trxUnlockMerkle.uSets) {
            privContainer[anUnlockerSet.salt] = [];
            for (let aSignSet of anUnlockerSet.sSets)
                privContainer[anUnlockerSet.salt].push(pubToPrvMap[aSignSet.sKey]);

            // test unlock structure & signature 
            let isValidUnlock = mOfNHandler.validateSigStruct({
                address: trxUnlockMerkle.accountAddress,
                uSet: anUnlockerSet
            });
            expect(isValidUnlock).toEqual(true);
        }
        trxUnlockMerkle.privContainer = privContainer;
        console.log(utils.stringify(trxUnlockMerkle));

        let signMsg = iutils.convertTitleToHash('Imagine all the people living life in peace').substring(0, iConsts.SIGN_MSG_LENGTH);
        let fetchedPrivContainer = trxUnlockMerkle.privContainer;
        for (let aUSet of trxUnlockMerkle.uSets) {
            for (let inx = 0; inx < aUSet.sSets.length; inx++) {
                let aSignature = crypto.signMsg(signMsg, fetchedPrivContainer[aUSet.salt][inx]);
                let verifyRes = crypto.verifySignature(signMsg, aSignature, aUSet.sSets[inx].sKey);
                expect(verifyRes).toEqual(true);
            }
        }



    });


});