const iConsts = require('../../../config/constants');
const crypto = require('../../../crypto/crypto');

describe('native secp256', () => {
    it('Should generate pair key', () => {
        let pair = crypto.nativeGenerateKeyPairSync();
    });


    it('Should generate pair key, sign message, then verify it\'s sign', () => {
        let pair = crypto.nativeGenerateKeyPairSync();
        let signature;
        let verify;
        let nativeSignMsgs = ['', '1', 'hello', 'dijiIIuiwe8823477918*&(5%4ghGFhGfh', 'pplplplpl;"', '\sdewerererwer',
            JSON.stringify({
                "type": "transaction",
                "ver": "0.0.0",
                "From": "x",
                "to": iConsts.CONSTS.YES,
                "value": "102.00809",
                "ref": "sdjfkjshfjshdkjsfjsd",
                "desc": "sdjfkjshfjshdkjsfjsd",
                "desc": "im1xqmk2c33xuunqdpjvgenge35xanxvdf5vger2dtrvdjnyvejvvexv4g798c",
                "vin": [
                    { "trxA.inx0": "293.90009" },
                ],
            }),
            'im1xqmk2c33xuunqdpjvgenge35xanxvdf5vger2dtrvdjnyvejvvexv4g798c',
            'b4926fdf114eb0c2632400f78b7445237a04704acdf49a4abf7ab743c7227788',
        ];
        for (let message of nativeSignMsgs) {
            signature = crypto.nativeSign(pair.prvKey, message);
            verify = crypto.nativeVerifySignature(pair.pubKey, message, signature);
            expect(verify).toEqual(true);
        };
    });

    it('Should generate pair key, encrypt message with pubkey , then decrypt with prvKey', () => {
        let pair = crypto.nativeGenerateKeyPairSync();
        ['', '1', 'hello', 'dijiIIuiwe8823477918*&(5%4ghGFhGfh', 'pplplplpl;"', '\sdewerererwer',
            JSON.stringify({
                "type": "transaction",
                "ver": "0.0.0",
                "From": "x",
                "to": iConsts.CONSTS.YES,
                "value": "102.00809",
                "ref": "sdjfkjshfjshdkjsfjsd",
                "desc": "sdjfkjshfjshdkjsfjsd",
                "vin": [
                    { "trxA.inx0": "293.90009" },
                ],
            })
        ].forEach((message) => {
            encrypted = crypto.nativeEncryptWithPublicKey(pair.pubKey, message)
            // clog.app.info('++++++++++++++++++++++encrypted++++++++++++')
            // clog.app.info(encrypted)
            decrypted = crypto.nativeDecryptWithPrivateKey(pair.prvKey, encrypted)
            // clog.app.info('---------------dec-----------')
            // clog.app.info(decrypted)
            expect(decrypted).toEqual(message)
        })
    });

});