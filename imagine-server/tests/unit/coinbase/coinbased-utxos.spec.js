const iutils = require("../../../utils/iutils");
const iConsts = require('../../../config/constants');
const getMoment = require("../../../startup/singleton").instance.getMoment;
const coinbaseUTXOs = require("../../../dag/coinbase/import-utxo-from-coinbases");
let expectGain = (12 * 60) / iConsts.getCycleByMinutes(); // in normal situation expectGain will be 1, for 12 hour cycles

describe('Coinbase utxo cycle calculations ', () => {
    it('Should control coinbase utxo date range is valid', () => {
        // let t = getMoment().format('YYYY-MM-DD HH:mm:ss');
        let dRange;

        if (iConsts.TIME_GAIN == 5) {

            if (iConsts.COINBASE_MATURATION_CYCLES == 1) {

                // for the range (22:50:00 to 22:54:59) the result must be (22:45:00 to 22:49:59)
                dRange = iutils.getCbUTXOsDateRange('2017-07-22 22:50:00')
                expect(dRange.minCreationDate).toEqual('2017-07-22 22:45:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 22:49:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 22:50:01')
                expect(dRange.minCreationDate).toEqual('2017-07-22 22:45:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 22:49:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 22:54:59')
                expect(dRange.minCreationDate).toEqual('2017-07-22 22:45:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 22:49:59');


            } else if (iConsts.COINBASE_MATURATION_CYCLES == 2) {

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 23:50:00')
                expect(dRange.minCreationDate).toEqual('2017-07-22 23:40:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 23:44:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 23:54:59')
                // expect(dRange.minCreationDate).toEqual('2017-07-22 23:35:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-22 23:39:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:00:00')
                // expect(dRange.minCreationDate).toEqual('2017-07-21 23:45:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-21 23:49:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:05:00')
                // expect(dRange.minCreationDate).toEqual('2017-07-21 23:50:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-21 23:54:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:10:00')
                // expect(dRange.minCreationDate).toEqual('2017-07-21 23:55:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-21 23:59:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:15:00')
                // expect(dRange.minCreationDate).toEqual('2017-07-22 00:00:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-22 00:04:59');

                // dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:20:00')
                // expect(dRange.minCreationDate).toEqual('2017-07-22 00:05:00');
                // expect(dRange.maxCreationDate).toEqual('2017-07-22 00:09:59');
            
            } else if (iConsts.COINBASE_MATURATION_CYCLES == 3) {

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 23:50:00')
                expect(dRange.minCreationDate).toEqual('2017-07-22 23:35:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 23:39:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 23:54:59')
                expect(dRange.minCreationDate).toEqual('2017-07-22 23:35:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 23:39:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:00:00')
                expect(dRange.minCreationDate).toEqual('2017-07-21 23:45:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-21 23:49:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:05:00')
                expect(dRange.minCreationDate).toEqual('2017-07-21 23:50:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-21 23:54:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:10:00')
                expect(dRange.minCreationDate).toEqual('2017-07-21 23:55:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-21 23:59:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:15:00')
                expect(dRange.minCreationDate).toEqual('2017-07-22 00:00:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 00:04:59');

                dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:20:00')
                expect(dRange.minCreationDate).toEqual('2017-07-22 00:05:00');
                expect(dRange.maxCreationDate).toEqual('2017-07-22 00:09:59');
            }

        } else if (iConsts.TIME_GAIN == 1) {
            dRange = iutils.getCbUTXOsDateRange('2017-07-22 00:00:00')
            expect(dRange.minCreationDate).toEqual('2017-07-20 12:00:00');
            expect(dRange.maxCreationDate).toEqual('2017-07-20 23:59:59');

            dRange = iutils.getCbUTXOsDateRange('2017-07-22 11:59:59')
            expect(dRange.minCreationDate).toEqual('2017-07-20 12:00:00');
            expect(dRange.maxCreationDate).toEqual('2017-07-20 23:59:59');

            dRange = iutils.getCbUTXOsDateRange('2017-07-22 12:00:00')
            expect(dRange.minCreationDate).toEqual('2017-07-21 00:00:00');
            expect(dRange.maxCreationDate).toEqual('2017-07-21 11:59:59');

            dRange = iutils.getCbUTXOsDateRange('2017-07-22 23:59:00')
            expect(dRange.minCreationDate).toEqual('2017-07-21 00:00:00');
            expect(dRange.maxCreationDate).toEqual('2017-07-21 11:59:59');

        }

    });

});
