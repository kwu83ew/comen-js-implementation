const iutils = require("../../../utils/iutils");

describe('getCoinbaseInfo', () => {
    it('Should getCoinbaseInfo', () => {
        let cbInfo = iutils.getCoinbaseInfo({ cycle: '2016-01-01 00:00:00' });
        expect(cbInfo.cycleStamp).toBe('2016-01-01 00:00:00');
        expect(cbInfo.from).toBe('2016-01-01 00:00:00');
        expect(cbInfo.fromHour).toBe('00:00:00');
        expect(cbInfo.to).toBe('2016-01-01 11:59:59');
        expect(cbInfo.toHour).toBe('11:59:59');
        
        cbInfo =iutils.getCoinbaseInfo({ cycle: '2016-01-01 12:00:00' });
        expect(cbInfo.cycleStamp).toBe('2016-01-01 12:00:00');
        expect(cbInfo.from).toBe('2016-01-01 12:00:00');
        expect(cbInfo.fromHour).toBe('12:00:00');
        expect(cbInfo.to).toBe('2016-01-01 23:59:59');
        expect(cbInfo.toHour).toBe('23:59:59');

    });
});