const iutils = require("../../../utils/iutils");
const iConsts = require('../../../config/constants');
const getMoment = require("../../../startup/singleton").instance.getMoment;

let expectGain = (12 * 60) / iConsts.getCycleByMinutes(); // in normal situation expectGain will be 1, for 12 hour cycles

describe('Coinbase cycle calculations ', () => {
    it('Should control coinbase date range is valid (12 hour per cycle)', () => {
        let cyclePerDay = iutils.getCycleCountPerDay();
        let cb, smapleTime;
        let out = `
        expectGain: ${expectGain}
        Cycle length: ${iConsts.getCycleByMinutes()} minutes
        Cycles Per Day: ${iutils.getCycleCountPerDay()}
        Now By Minutes: ${iutils.getNowByMinutes()}
        Current cycle number: ${iutils.getCoinbaseCycleNumber(null)}`;
        // clog.app.info(out);

        if (iConsts.TIME_GAIN == 1) {

            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:00:01')).toEqual('00:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 01:08:00')).toEqual('00:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 07:08:00')).toEqual('00:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 11:59:59')).toEqual('00:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:00')).toEqual('12:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:01')).toEqual('12:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 13:08:00')).toEqual('12:00:00');
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 23:59:59')).toEqual('12:00:00');

            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 00:00:00')).toEqual(0);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 00:01:01')).toEqual(1);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 07:00:01')).toEqual(420);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 12:00:01')).toEqual(0);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 13:00:01')).toEqual(60);

            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 11:59:59')).toEqual(719);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 23:59:59')).toEqual(719);

            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').from).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').to).toEqual('2012-11-05 11:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 11:59:59').from).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 11:59:59').to).toEqual('2012-11-05 11:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').from).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').to).toEqual('2012-11-05 23:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').from).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').to).toEqual('2012-11-05 23:59:59');

            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:00')).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:01')).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 11:59:59')).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:00')).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:01')).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 23:59:59')).toEqual('2012-11-05 12:00:00');

        }
    });

    it('Should control coinbase date range is valid (60 minutes per cycle)', () => {
        let cyclePerDay = iutils.getCycleCountPerDay();
        let cb, smapleTime;
        let out = `
        expectGain: ${expectGain}
        Cycle length: ${iConsts.getCycleByMinutes()} minutes
        Cycles Per Day: ${iutils.getCycleCountPerDay()}
        Now By Minutes: ${iutils.getNowByMinutes()}
        Current cycle number: ${iutils.getCoinbaseCycleNumber(null)}`;
        // clog.app.info(out);

        if (iConsts.getCycleByMinutes() == 60) {

            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:00:01')).toEqual(0);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:29:59')).toEqual(0);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:30:00')).toEqual(0);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 01:00:00')).toEqual(1);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 01:29:59')).toEqual(1);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 07:08:00')).toEqual(7);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 11:59:59')).toEqual(11);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:00')).toEqual(12);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:01')).toEqual(12);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 13:08:00')).toEqual(13);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 13:48:00')).toEqual(13);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 23:29:59')).toEqual(23);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 23:59:59')).toEqual(23);

            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 00:01:01')).toEqual(1);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 01:59:59')).toEqual(59);
            expect(iutils.getCoinbaseAgeByMinutes('2012-11-05 21:12:19')).toEqual(12);

            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').from).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').to).toEqual('2012-11-05 00:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 01:59:59').from).toEqual('2012-11-05 01:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 01:59:59').to).toEqual('2012-11-05 01:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').from).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').to).toEqual('2012-11-05 12:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').from).toEqual('2012-11-05 23:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').to).toEqual('2012-11-05 23:59:59');

            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:00')).toEqual('2012-11-05 0');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:01')).toEqual('2012-11-05 0');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 11:59:59')).toEqual('2012-11-05 11');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:00')).toEqual('2012-11-05 12');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:01')).toEqual('2012-11-05 12');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 23:59:59')).toEqual('2012-11-05 23');

        }
    });

    it('Should control coinbase date range is valid (30 minutes per cycle)', () => {
        let cyclePerDay = iutils.getCycleCountPerDay();
        let cb, smapleTime;
        let out = `
        expectGain: ${expectGain}
        Cycle length: ${iConsts.getCycleByMinutes()} minutes
        Cycles Per Day: ${iutils.getCycleCountPerDay()}
        Now By Minutes: ${iutils.getNowByMinutes()}
        Current cycle number: ${iutils.getCoinbaseCycleNumber(null)}`;
        // clog.app.info(out);

        if (iConsts.getCycleByMinutes() == 30) {

            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:00:01')).toEqual(0);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:29:59')).toEqual(0);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 00:30:00')).toEqual(1);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 01:00:00')).toEqual(2);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 01:29:59')).toEqual(2);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 07:08:00')).toEqual(14);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 11:59:59')).toEqual(23);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:00')).toEqual(24);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 12:00:01')).toEqual(24);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 13:08:00')).toEqual(26);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 13:48:00')).toEqual(27);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 23:29:59')).toEqual(46);
            expect(iutils.getCoinbaseCycleNumber('2012-11-05 23:59:59')).toEqual(47);

            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').from).toEqual('2012-11-05 00:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 00:00:01').to).toEqual('2012-11-05 00:29:59');
            expect(iutils.getCoinbaseRange('2012-11-05 01:59:59').from).toEqual('2012-11-05 01:30:00');
            expect(iutils.getCoinbaseRange('2012-11-05 01:59:59').to).toEqual('2012-11-05 01:59:59');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').from).toEqual('2012-11-05 12:00:00');
            expect(iutils.getCoinbaseRange('2012-11-05 12:00:00').to).toEqual('2012-11-05 12:29:59');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').from).toEqual('2012-11-05 23:30:00');
            expect(iutils.getCoinbaseRange('2012-11-05 23:59:59').to).toEqual('2012-11-05 23:59:59');

            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:00')).toEqual('2012-11-05 0');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 00:00:01')).toEqual('2012-11-05 0');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 11:59:59')).toEqual('2012-11-05 23');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:00')).toEqual('2012-11-05 24');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 12:00:01')).toEqual('2012-11-05 24');
            expect(iutils.getCoinbaseCycleStamp('2012-11-05 23:59:59')).toEqual('2012-11-05 47');

        }

    });
});
