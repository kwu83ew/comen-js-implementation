const iConsts = require("../../../config/constants");
const coinbaseUTXOs = require('../../../dag/coinbase/import-utxo-from-coinbases');




describe('coinbase_plan', () => {
    it('Should encode then decod and return same text', () => {
        if (iConsts.getCycleByMinutes() == 5) {
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 00:00:00')).toBe('2019-05-10 00:15:00');
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 00:02:00')).toBe('2019-05-10 00:15:00');
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 00:04:59')).toBe('2019-05-10 00:15:00');


        }



        if (iConsts.getCycleByMinutes() == 720) {
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 00:00:00')).toBe('2019-05-11 12:00:00');
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 11:59:59')).toBe('2019-05-11 12:00:00');

            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 12:00:00')).toBe('2019-05-12 00:00:00');
            expect(coinbaseUTXOs.calcCoinbasedOutputMaturationDate('2019-05-10 23:59:59')).toBe('2019-05-12 00:00:00');

        }

    });

})