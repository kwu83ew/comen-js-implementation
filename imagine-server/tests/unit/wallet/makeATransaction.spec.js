const _ = require('lodash');
const utils = require('../../../utils/utils');
const iutils = require('../../../utils/iutils');
const crypto = require('../../../crypto/crypto');
const walletHandler = require('../../../web-interface/wallet/wallet-handler');
const mOfNHandler = require('../../../transaction/signature-structure-handler/general-m-of-n-handler');
const verifyInputOutputsSignature = require('../../../transaction/input-output-signer').verifyInputOutputsSignature;
const trxSigValidator = require('../../../transaction/validators/trx-signature-validator');

// console.log(crypto.keccak256('258d9d0485fc7d58d4f67eefdbca7f4655650d5e01b4bccf1fff2f5f499b0bf1'));

let block = {
    // "net": "im",
    // "bVer": "0.0.0",
    // "bType": "Normal",
    // "descriptions": "",
    // "blockLength": "0002521",
    // "blockHash": "925381eb601590ce78b3c8942acada9dc06f158bd80fe289082d56222ed5b227",
    // "ancestors": "49047d30d37b4263a20e988b9606df1016e44e3e3f7bafe6256e7e957791c6ba,b2a26df988be182e41c4a1c8b4f16f3e8bb119d4b315ae90d3fbb9af1931a2e4,dcdf12c0d8c7b487374589e227d1a9afc025195c40a53d13006a32021c726cc9,f7cd652dac1bb35adf19fffc2abf18005ff6cc8f7ea4e24ee364e40edb368a91",
    // "descendents": "",
    // "signals": [
    //     "iNReputation"
    // ],
    // "backerAddress": "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
    // "creationDate": "2019-10-28 19:39:05",
    // "dExtInfos": [
    //     "null",
    //     [
    //         {
    //             "uSet": {
    //                 "sSets": [
    //                     [
    //                         "0308616faa9c82891e0510e09f341ac63d96dd70a263e9bffbadb835d48466a6e6"
    //                     ],
    //                     [
    //                         "032172dbbcd8798b0f29943fce8685460855203a42856e54d03d5e5f85723b9930"
    //                     ]
    //                 ],
    //                 "proofs": [
    //                     "r.03f92de177d5e4adedaeff3154000a9d4f9d1a86205dbd4da89600ca695f78b4",
    //                     "r.59738833eb562a82c22bb3c1a56b9d20f750d805bbca87862792186b162f46d0"
    //                 ],
    //                 "lHash": null,
    //                 "salt": "c3c306d7b43d0b33fa32d026c36e69a63ca15060f7c0cca751ddf7f2550a9fe9"
    //             },
    //             "signatures": [
    //                 [
    //                     "43275e091c4744758768f378b1e60138b2b51548560411d1cc6ba02caa89e4071e71d3953c11b404f25b3556976bb5b36c80f13b881a473edc36a170ea2b72da",
    //                     "ALL"
    //                 ],
    //                 [
    //                     "7399e90719dd135accb5185ba80e7b3d41e918354a0b0798a08d7e404630e5285ab69afc5935d811505f7a006ace17e714c5702f45239eed9e921f6ba730bf78",
    //                     "ALL"
    //                 ]
    //             ]
    //         }
    //     ]
    // ],
    // "dExtHash": "2742f63dc785d99779d355e87bf5351c69262c91556fef12d81b324b87be7348",
    // "docsRootHash": "48710befc1bdb55e5012627d132322356fabcfb1aa0ed20ba6c2801641b6cf4c",
    // "docs": [
    //     {
    //         "hash": "9d51438006996b441a309ed57cf963be3216311bec6efec18183b733d07be2af",
    //         "length": "0000348",
    //         "dType": "DPCostPay",
    //         "dClass": "",
    //         "dVer": "0.0.0",
    //         "description": "Data & Process Cost Payment",
    //         "creationDate": "2019-10-28 19:39:05",
    //         "outputs": [
    //             [
    //                 "TP_DP",
    //                 870000000
    //             ],
    //             [
    //                 "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
    //                 2130000000
    //             ]
    //         ]
    //     },
    //     {
    //         "hash": "f3932b461a0d9dad2bbc0d6a4ef942a9b8bfe0665e318e935133e669af02fc01",
    //         "length": "0001380",
    //         "dType": "BasicTx",
    //         "dClass": "SimpleTx",
    //         "dPIs": [
    //             1
    //         ],
    //         "dVer": "0.0.0",
    //         "ref": "",
    //         "description": "",
    //         "creationDate": "2019-10-28 19:39:01",
    //         "inputs": [
    //             [
    //                 "7f3af95d60982ca22cf3d294ce368a8971b62e31646602b122d3a443dd48006f",
    //                 "1"
    //             ]
    //         ],
    //         "outputs": [
    //             [
    //                 "im1xpnrjd3nxver2ef5x43rxet9vgukxdn9xcmrwvf3x43kzceevv6nqp63d0j",
    //                 4000000000
    //             ],
    //             [
    //                 "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
    //                 3000000000
    //             ],
    //             [
    //                 "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
    //                 375383979018
    //             ]
    //         ],
    //         "dExtHash": "bafafcbaec4739de747d7c6cbe35cc6b802557b96dd15ce4f1817ef345ce9ee0"
    //     }
    // ]
}

describe('walletHandler.makeATransaction ', () => {
    it('makes a trensaction', async () => {
        let trxDtl = walletHandler.makeATransaction({
            "dDPCost": 3000000000,
            "DPCostChangeIndex": 2,
            "dType": "BasicTx",
            "dClass": "SimpleTx",
            "description": "",
            "inputs": {

                "1e84990db25720d8021d567d384eac30695c1d01430947339f5f10373cb2b4c4:1": {
                    "value": 382383979018,
                    "address": "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
                    "uSet": {
                        "sSets": [
                            [
                                "0308616faa9c82891e0510e09f341ac63d96dd70a263e9bffbadb835d48466a6e6"
                            ],
                            [
                                "032172dbbcd8798b0f29943fce8685460855203a42856e54d03d5e5f85723b9930"
                            ],
                        ],
                        "proofs": [
                            "r.03f92de177d5e4adedaeff3154000a9d4f9d1a86205dbd4da89600ca695f78b4",
                            "r.59738833eb562a82c22bb3c1a56b9d20f750d805bbca87862792186b162f46d0"
                        ],
                        "lHash": null,
                        "salt": "c3c306d7b43d0b33fa32d026c36e69a63ca15060f7c0cca751ddf7f2550a9fe9"
                    },
                    "privateKeys": {
                        "032172dbbcd8798b0f29943fce8685460855203a42856e54d03d5e5f85723b9930": {
                            "3e66983f050af5d9d6031790b7af033f1a834ccf999ede44359695ed8bbdb5e4": "f686df1826825ffd0eb7699688364655d5b3f96cf71e491d34a0fbf8cdcb096b"
                        },
                        "0308616faa9c82891e0510e09f341ac63d96dd70a263e9bffbadb835d48466a6e6": {
                            "799089ab489cddc747419436a344d6a27a931dc07e024ac6fa968cdb3055d4cb": "5590eae13c66f2767af03ed3aa376e5e0a531f407fcd631701e596fb59582a6b"
                        }
                    }
                },

                "404f411bd7b065365656f1ea76b90c8aa8b89f96b2165a2b3658982e8be17ec9:1": {
                    "value": 382383979018,
                    "address": "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
                    "uSet": {
                        "sSets": [
                            [
                                "032172dbbcd8798b0f29943fce8685460855203a42856e54d03d5e5f85723b9930"
                            ],
                            [
                                "0308616faa9c82891e0510e09f341ac63d96dd70a263e9bffbadb835d48466a6e6"
                            ]
                        ],
                        "proofs": [
                            "r.03f92de177d5e4adedaeff3154000a9d4f9d1a86205dbd4da89600ca695f78b4",
                            "r.59738833eb562a82c22bb3c1a56b9d20f750d805bbca87862792186b162f46d0"
                        ],
                        "lHash": null,
                        "salt": "c3c306d7b43d0b33fa32d026c36e69a63ca15060f7c0cca751ddf7f2550a9fe9"
                    },
                    "privateKeys": {
                        "032172dbbcd8798b0f29943fce8685460855203a42856e54d03d5e5f85723b9930": {
                            "3e66983f050af5d9d6031790b7af033f1a834ccf999ede44359695ed8bbdb5e4": "f686df1826825ffd0eb7699688364655d5b3f96cf71e491d34a0fbf8cdcb096b"
                        },
                        "0308616faa9c82891e0510e09f341ac63d96dd70a263e9bffbadb835d48466a6e6": {
                            "799089ab489cddc747419436a344d6a27a931dc07e024ac6fa968cdb3055d4cb": "5590eae13c66f2767af03ed3aa376e5e0a531f407fcd631701e596fb59582a6b"
                        }
                    }
                }

            },
            "outputs": [
                [
                    "im1xpnrjd3nxver2ef5x43rxet9vgukxdn9xcmrwvf3x43kzceevv6nqp63d0j",
                    4000000000
                ],
                [
                    "im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9",
                    757767958036
                ]
            ]
        });
        let trx = _.clone(trxDtl.trx);

        let buggyTrx = _.clone(trxDtl.trx);
        // let swap = buggyTrx.dExtInfo[0].signatures[0];
        // buggyTrx.dExtInfo[0].signatures[0] = buggyTrx.dExtInfo[0].signatures[1];
        // buggyTrx.dExtInfo[0].signatures[1] = swap;
        //  swap = buggyTrx.dExtInfo[1].signatures[0];
        // buggyTrx.dExtInfo[1].signatures[0] = buggyTrx.dExtInfo[1].signatures[1];
        // buggyTrx.dExtInfo[1].signatures[1] = swap;

        // console.log('swap', buggyTrx.dExtInfo.length, swap);
        let validateRes;

        validateRes = trxSigValidator.trxSignatureValidator({
            block: {
                blockHash: "ee921a2afa134aa050a3c70ea106bfe562b2f6b7eb9124247fb4e2219deb8566",
                docs: [null, buggyTrx],
                bExtInfo: [null, buggyTrx.dExtInfo]
            },
            docInx: 1,
            coinsDict: {
                '1e84990db25720d8021d567d384eac30695c1d01430947339f5f10373cb2b4c4:1': { o_address: 'im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9' },
                '404f411bd7b065365656f1ea76b90c8aa8b89f96b2165a2b3658982e8be17ec9:1': { o_address: 'im1xqux2v34xscrgdmxvfsnvdeex33rzvehxaskxctpvymr2wf3vvukz2zw6r9' },
            },
        });
        console.log(validateRes);
        expect(validateRes.err != false).toEqual(false);
        return;

    });






});
