const utils = require('../../../utils/utils')
const getMoment = require("../../../startup/singleton").instance.getMoment;


describe('Utils.sleepBySecond', () => {
    it('Should sleep 5 seconds', () => {
        let start = calcTimeBySeconds(getMoment());
        utils.sleepBySecond(5)
        let end = calcTimeBySeconds(getMoment());
        // expect(end - start).toEqual(4)
    });

    it('Should make a loop of 2 seconds sleep', () => {
        let start, end, inx;
        inx = 0
        while (inx < 3) {
            inx += 1
            start = calcTimeBySeconds(getMoment());
            utils.sleepBySecond(2)
            end = calcTimeBySeconds(getMoment());
            expect(end - start).toEqual(2)

        }
    });

});

function calcTimeBySeconds(t) {
    return parseInt(t.format('ss')) + (parseInt(t.format('mm')) * 60) + (parseInt(t.format('HH')) * 3600)
}