const utils = require('../../../utils/utils')


describe('Utils._nilEmptyFalse', () => {
    it('Should return true for null', () => {
        expect(utils._nilEmptyFalse(null)).toEqual(true);
    });

    it('Should return true for an empty', () => {
        expect(utils._nilEmptyFalse('')).toEqual(true);
    });

    it('Should return true for false', () => {
        expect(utils._nilEmptyFalse(false)).toEqual(true);
    });

    it('Should return false for an empty array', () => {
        expect(utils._nilEmptyFalse([])).toEqual(false);
    });

    
    it('_nil Should return false for an empty array', () => {
        expect(utils._nil([])).toEqual(false);
    });
    
    it('_empty Should return false for an empty array', () => {
        expect(utils._empty([])).toEqual(false);
    });
    
    it('_false Should return false for an empty array', () => {
        expect(utils._false([])).toEqual(false);
    });
    
    // it('_nil Should return false for 0 string', () => {
    //     expect(utils._nil('0')).toEqual(false);
    // });
    // it('_empty Should return false for 0 string', () => {
    //     expect(utils._empty('0')).toEqual(false);
    // });
    // it('_false Should return false for 0 string', () => {
    //     expect(utils._false('0')).toEqual(false);
    // });
    // it('Should return false for 0 string', () => {
    //     expect(utils._nilEmptyFalse('0')).toEqual(false);
    // });


});