const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.minutesAfter', () => {
    it('Should calculate time minus 1 minutes', () => {
        expect(utils.minutesBefore(1, '2000-01-01 12:00:00')).toEqual('2000-01-01 11:59:00');   // a minute before
        expect(utils.minutesBefore(1, '2000-02-02 00:00:00')).toEqual('2000-02-01 23:59:00');   // a day before
        expect(utils.minutesBefore(1, '2000-02-01 00:00:00')).toEqual('2000-01-31 23:59:00');   // a month before
        expect(utils.minutesBefore(1, '2000-01-01 00:00:00')).toEqual('1999-12-31 23:59:00');   // a year before
    });

    it('Should calculate time minus 12 hours', () => {
        expect(utils.minutesBefore(12 * 60, '2000-01-01 12:00:01')).toEqual('2000-01-01 00:00:01');   
        expect(utils.minutesBefore(12 * 60, '2000-02-02 00:00:00')).toEqual('2000-02-01 12:00:00');   // a day before
        expect(utils.minutesBefore(12 * 60, '2000-02-01 11:00:00')).toEqual('2000-01-31 23:00:00');   // a month before
        expect(utils.minutesBefore(12 * 60, '2000-01-01 11:00:00')).toEqual('1999-12-31 23:00:00');   // a year before
    });

});