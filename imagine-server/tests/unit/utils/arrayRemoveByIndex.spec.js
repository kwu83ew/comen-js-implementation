
const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.arrayRemoveByIndex', () => {
    it('Should calculate arrayRemoveByIndex', () => {
        let arr = [1, 2, 3, "a", 5, 6];
        expect(utils.arrayRemoveByIndex(arr, 1)).toEqual([1, 3, "a", 5, 6]);
    });



});