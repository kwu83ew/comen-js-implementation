const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.microPAIToPAI', () => {
    it('Should calculate microPAIToPAI', () => {
        expect(utils.microPAIToPAI6(1)).toEqual('0.000001');
        expect(utils.microPAIToPAI6(10)).toEqual('0.000010');
        expect(utils.microPAIToPAI6(21)).toEqual('0.000021');
        expect(utils.microPAIToPAI6(100)).toEqual('0.000100');
        expect(utils.microPAIToPAI6(321)).toEqual('0.000321');
        expect(utils.microPAIToPAI6(4321)).toEqual('0.004321');
        expect(utils.microPAIToPAI6(654321)).toEqual('0.654321');
        expect(utils.microPAIToPAI6(7654321)).toEqual('7.654321');
        expect(utils.microPAIToPAI6(87654321)).toEqual('87.654321');
        expect(utils.microPAIToPAI6(987654321)).toEqual('987.654321');
        expect(utils.microPAIToPAI6(1987654321)).toEqual('1,987.654321');
        expect(utils.microPAIToPAI6(21987654321)).toEqual('21,987.654321');
        expect(utils.microPAIToPAI6(321987654321)).toEqual('321,987.654321');
        expect(utils.microPAIToPAI6(4321987654321)).toEqual('4,321,987.654321');
    });

    it('Should calculate microPAIToPAI for negativ e values', () => {
        expect(utils.microPAIToPAI6(-1)).toEqual('-0.000001');
        expect(utils.microPAIToPAI6(-10)).toEqual('-0.000010');
        expect(utils.microPAIToPAI6(-21)).toEqual('-0.000021');
        expect(utils.microPAIToPAI6(-100)).toEqual('-0.000100');
        expect(utils.microPAIToPAI6(-321)).toEqual('-0.000321');
        expect(utils.microPAIToPAI6(-4321)).toEqual('-0.004321');
        expect(utils.microPAIToPAI6(-654321)).toEqual('-0.654321');
        expect(utils.microPAIToPAI6(-987654321)).toEqual('-987.654321');
        expect(utils.microPAIToPAI6(-989666547891355)).toEqual('-989,666,547.891355');
    });

});