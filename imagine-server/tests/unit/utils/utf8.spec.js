const utils = require("../../../utils/utils");
var LZUTF8 = require('lzutf8');

describe('Utils.encode_utf8, decode_utf8', () => {
    it('decode_utf8', () => {
        expect(utils.decode_utf8(utils.encode_utf8('a'))).toEqual('a');
        expect(utils.decode_utf8(utils.encode_utf8('Marché'))).toEqual('Marché');
        expect(utils.decode_utf8(utils.encode_utf8('العربية'))).toEqual('العربية');
        expect(utils.decode_utf8(utils.encode_utf8('सिन्धी'))).toEqual('सिन्धी');
        expect(utils.decode_utf8(utils.encode_utf8('español'))).toEqual('español');
        expect(utils.decode_utf8(utils.encode_utf8('ضریب '))).toEqual('ضریب ');
        expect(utils.decode_utf8(utils.encode_utf8('a'))).toEqual('a');
        expect(utils.decode_utf8(utils.encode_utf8('Türkçe'))).toEqual('Türkçe');
        expect(utils.decode_utf8(utils.encode_utf8('a'))).toEqual('a');


        let contentOrg = 'Marché '
        // console.log('content', content);
        content = utils.encode_utf8(contentOrg)
        console.log('content', content);
        content = LZUTF8.compress(content, { outputEncoding: 'Base64' });
        content = LZUTF8.decompress(content, { inputEncoding: "Base64" });
        content = utils.decode_utf8(content)
        console.log('content', content);
        expect(content == contentOrg).toEqual(true);

        // let b = Buffer.alloc(content.length, content);
        // let content2 = b.toString('base64');
        // console.log('content2', content2);
        // let buffer = Buffer.from(content2, "base64");
        // let content3 = buffer.toString("utf8");
        // console.log('content3', content3);
    });

});