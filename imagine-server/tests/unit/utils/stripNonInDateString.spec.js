const utils = require("../../../utils/utils");


describe('Utils.stripNonInDateString', () => {
    it('Should remove Non In Date String chars', () => {
        expect(utils.stripNonInDateString('2010-10-10 10:10:10')).toEqual('2010-10-10 10:10:10')
        expect(utils.stripNonInDateString('2010-10-10 10:10:10am')).toEqual('2010-10-10 10:10:10')
        expect(utils.stripNonInDateString('2010-10-10 10:10:10xyz')).toEqual('2010-10-10 10:10:10')
        expect(utils.stripNonInDateString('(2010-10-10) 10:10:10')).toEqual('2010-10-10 10:10:10')
    });

});

