const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.minutesAfter', () => {
    it('Should calculate time minus 1 minutes', () => {
        expect(utils.minutesAfter(1, '2000-01-01 12:00:00')).toEqual('2000-01-01 12:01:00');   // a minute before
        expect(utils.minutesAfter(1, '2000-02-02 00:00:00')).toEqual('2000-02-02 00:01:00');   // a day before
        expect(utils.minutesAfter(1, '2000-02-01 00:00:00')).toEqual('2000-02-01 00:01:00');   // a month before
        expect(utils.minutesAfter(1, '1999-12-31 23:59:00')).toEqual('2000-01-01 00:00:00');   // a year before
    });

    it('Should calculate time minus 12 hours', () => {
        expect(utils.minutesAfter(12 * 60, '2000-01-01 00:00:01')).toEqual('2000-01-01 12:00:01');   
        expect(utils.minutesAfter(12 * 60, '2000-02-01 12:00:00')).toEqual('2000-02-02 00:00:00');   // a day before
        expect(utils.minutesAfter(12 * 60, '2000-01-31 23:00:00')).toEqual('2000-02-01 11:00:00');   // a month before
        expect(utils.minutesAfter(12 * 60, '1999-12-31 23:00:00')).toEqual('2000-01-01 11:00:00');   // a year before
    });

    it('Should calculate time minus 12 hours', () => {
        expect(utils.minutesAfter(24 * 60 * 1.5, '2011-12-05 20:45:00')).toEqual('2011-12-07 08:45:00');   
    });

    it('Should calculate time after 1 second', () => {
        expect(utils.secondsAfter(1, '2011-12-05 20:45:00')).toEqual('2011-12-05 20:45:01');   
        expect(utils.secondsAfter(1, '2011-12-05 20:45:59')).toEqual('2011-12-05 20:46:00');   
        expect(utils.secondsAfter(1, '2011-12-05 23:59:59')).toEqual('2011-12-06 00:00:00');   
    });

});