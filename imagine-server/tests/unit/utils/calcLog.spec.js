const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.calcLog', () => {
    it('Should calculate logarithm', () => {
        expect(utils.calcLog(0, 100, 17).gain).toEqual(100);
        expect(utils.calcLog(50, 100, 17).gain).toEqual(84.9485002168);
        expect(utils.calcLog(95, 100, 17).gain).toEqual(34.9485002168);
        expect(utils.calcLog(96, 100, 17).gain).toEqual(30.10299956639);
        expect(utils.calcLog(97, 100, 17).gain).toEqual(23.85606273598);
        expect(utils.calcLog(98, 100, 17).gain).toEqual(15.05149978319);
        expect(utils.calcLog(99, 100, 17).gain).toEqual(0);
        expect(utils.calcLog(100, 100, 17).gain).toEqual(0);
    });

    it('Should calculate logarithm for positive vote in an election time', () => {
        let electionTime = 2 * 24 * 60; // for example for election in 2 day
        expect(utils.calcLog(0, electionTime, 17).gain).toEqual(100);
        expect(utils.calcLog(electionTime / 4, electionTime, 17).gain).         toEqual(96.3884197283);
        expect(utils.calcLog(electionTime / 2, electionTime, 17).gain).         toEqual(91.2981832293); // at the start of second day
        expect(utils.calcLog(electionTime * 3 / 4, electionTime, 17).gain).     toEqual(82.59636645861); // at the mid of second day
        expect(utils.calcLog(electionTime * 40 / 48, electionTime, 17).gain).   toEqual(77.5061299596); // at the latest 8 hours
        expect(utils.calcLog(electionTime * 44 / 48, electionTime, 17).gain).   toEqual(68.80431318891); // at the latest 4 hours
        expect(utils.calcLog(electionTime * 46 / 48, electionTime, 17).gain).   toEqual(60.10249641821); // at the latest 2 hours
        expect(utils.calcLog(electionTime * 47 / 48, electionTime, 17).gain).   toEqual(51.40067964752); // at the latest 1 hour
        expect(utils.calcLog(electionTime * 47.5 / 48, electionTime, 17).gain). toEqual(42.69886287682); // at the latest half hours
        expect(utils.calcLog(electionTime * 47.75 / 48, electionTime, 17).gain).toEqual(33.99704610613); // at the latest 15 minutes
        expect(utils.calcLog(electionTime * 47.84 / 48, electionTime, 17).gain).toEqual(28.39432751603); // at the latest 10 minutes
        expect(utils.calcLog(electionTime * 47.917/ 48, electionTime, 17).gain).toEqual(20.15467586366); // at the latest 5 minutes
        expect(utils.calcLog(100, 100, 17).gain).toEqual(0);
    });

    it('Should calculate logarithm for negative vote in an election time', () => {
        let electionTime = 2 * 24 * 60; // for example for election in 2 day
        let negativeVoteTime = electionTime *(3/2);
        expect(utils.calcLog(0, negativeVoteTime, 17).gain).toEqual(100);
        expect(utils.calcLog(negativeVoteTime / 4, negativeVoteTime, 17).gain).         toEqual(96.56335317912);
        expect(utils.calcLog(negativeVoteTime / 2, negativeVoteTime, 17).gain).         toEqual(91.71967153125); // at the start of second day
        expect(utils.calcLog(negativeVoteTime * 54 / 72, negativeVoteTime, 17).gain).   toEqual(83.43934306251); // at the mid of second day
        expect(utils.calcLog(negativeVoteTime * 60 / 72, negativeVoteTime, 17).gain).   toEqual(78.59566141464); // at the latest 12 hours
        expect(utils.calcLog(negativeVoteTime * 64 / 72, negativeVoteTime, 17).gain).   toEqual(73.75197976678); // at the latest 8 hours
        expect(utils.calcLog(negativeVoteTime * 70 / 72, negativeVoteTime, 17).gain).   toEqual(57.19132282929); // at the latest 2 hours
        expect(utils.calcLog(negativeVoteTime * 71 / 72, negativeVoteTime, 17).gain).   toEqual(48.91099436055); // at the latest 1 hour
        expect(utils.calcLog(negativeVoteTime * 71.50 / 72, negativeVoteTime, 17).gain).toEqual(40.63066589181); // at the latest half hours
        expect(utils.calcLog(negativeVoteTime * 71.75 / 72, negativeVoteTime, 17).gain).toEqual(32.35033742307); // at the latest 15 minutes
        expect(utils.calcLog(negativeVoteTime * 71.834/ 72, negativeVoteTime, 17).gain).toEqual(27.4587759964); // at the latest 10 minutes
        expect(utils.calcLog(negativeVoteTime * 71.917/ 72, negativeVoteTime, 17).gain).toEqual(19.17844752766); // at the latest 5 minutes
        expect(utils.calcLog(100, 100, 17).gain).toEqual(0);
    });



});