const utils = require("../../../utils/utils");


describe('Utils.orderbyKey', () => {
    it('Should return a list 1', () => {
        let dict = {
            3: "value 3",
            1: { dummy: 'object' },
            2: ['array']
        };
        expect(utils.orderbyKey(dict)).toEqual(
            [
                { dummy: 'object' },
                ['array'],
                "value 3"
            ]
        );
    });

});