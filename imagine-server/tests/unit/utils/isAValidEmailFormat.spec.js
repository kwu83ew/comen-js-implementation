

const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");



describe('utils.isAValidEmailFormat', () => {
    it('Should validate an URL', () => {
        for (let anEmail of trueEmails) {
            expect(utils.isAValidEmailFormat(anEmail)).toEqual(true);
        }
    });
});

describe('utils.isAValidEmailFormat', () => {
    it('Should validate an URL', () => {
        for (let anEmail of falseEmails) {
            expect(utils.isAValidEmailFormat(anEmail)).toEqual(false);
        }
    });
});

let trueEmails = [
    'a@a.ab',
    'ab@ab.ab',
    'abc@abc.ab',
    'abcd@abcd.ab',
    'abcd@ab.cd.ab',
    'ab.cd@ab.cd.ab',
    'ab.cde@ab.cde.abc',
    'ab.cd.e@ab.cd.e.abc',
    'ab.cd@ab.cd.ab',
];

let falseEmails = [
    'a@a.a',
    'abcd@abcdab',
    'ab.cd@abcdab',
];
