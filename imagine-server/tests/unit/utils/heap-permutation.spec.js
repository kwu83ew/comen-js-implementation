const _ = require('lodash');
const utils = require("../../../utils/utils");


describe('Utils.heapPermutation', () => {

    it('Should Permutation 1/1', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a']
        hp._heapP(n, 1)
        expect(hp.premutions.length).toEqual(1)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a')).toEqual(1)

    });

    it('Should Permutation 1/2', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b']
        hp._heapP(n, 1)
        expect(hp.premutions.length).toEqual(2)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a')).toEqual(1)
        expect(getCount(processed, 'a') + getCount(processed, 'b')).toEqual(2)

    });

    it('Should Permutation 2/2', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b']
        hp._heapP(n, 2)
            // clog.app.info(hp.premutions);
        expect(hp.premutions.length).toEqual(1)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a,b') + getCount(processed, 'b,a')).toEqual(1)

    });

    it('Should Permutation 1/3', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c']
        hp._heapP(n, 1)
        expect(hp.premutions.length).toEqual(3)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'c')).toEqual(1)
        expect(getCount(processed, 'a') + getCount(processed, 'b')).toEqual(2)
        expect(getCount(processed, 'a') + getCount(processed, 'b') + getCount(processed, 'b')).toEqual(3)

    });

    it('Should Permutation 2/3', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c']
        hp._heapP(n, 2)
            // clog.app.info(hp);
        expect(hp.premutions.length).toEqual(3)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a,b') + getCount(processed, 'b,a')).toEqual(1)

        expect(getCount(processed, 'a,b') + getCount(processed, 'b,a') +
            getCount(processed, 'a,c') + getCount(processed, 'c,a')).toEqual(2)

    });

    it('Should Permutation 3/3', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c']
        hp._heapP(n, 3)
        expect(hp.premutions.length).toEqual(1)

        let processed = processIt(hp.premutions);
        expect(
            getCount(processed, 'a,b,c') +
            getCount(processed, 'a,c,b') +
            getCount(processed, 'b,a,c') +
            getCount(processed, 'b,c,a') +
            getCount(processed, 'c,a,b') +
            getCount(processed, 'c,b,a')
        ).toEqual(1)



    });

    it('Should Permutation 2/4', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c', 'd']
        hp._heapP(n, 2)
        expect(hp.premutions.length).toEqual(6)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a,b') + getCount(processed, 'b,a')).toEqual(1)
        expect(getCount(processed, 'a,d') + getCount(processed, 'd,a')).toEqual(1)

    });

    it('Should Permutation 2/5', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c', 'd', 'e']
        hp._heapP(n, 2)
        expect(hp.premutions.length).toEqual(10)

        let processed = processIt(hp.premutions);
        expect(getCount(processed, 'a,b') + getCount(processed, 'b,a')).toEqual(1)
        expect(getCount(processed, 'a,d') + getCount(processed, 'd,a')).toEqual(1)
        expect(getCount(processed, 'a,e') + getCount(processed, 'e,a')).toEqual(1)

    });

    it('Should Permutation 3/5', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c', 'd', 'e']
        hp._heapP(n, 3)
        expect(hp.premutions.length).toEqual(10)

        let processed = processIt(hp.premutions);
        expect(
            getCount(processed, 'a,b,c') + getCount(processed, 'a,c,b') +
            getCount(processed, 'b,a,c') + getCount(processed, 'b,c,a') +
            getCount(processed, 'c,a,b') + getCount(processed, 'c,b,a')
        ).toEqual(1)

    });

    it('Should Permutation 4/5', () => {
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        let n = ['a', 'b', 'c', 'd', 'e']
        hp._heapP(n, 4)
        expect(hp.premutions.length).toEqual(5)

    });


});

function processIt(arr) {
    let out = [];
    arr.forEach(element => {
        out.push(element.join());
    });
    const r = _.values(_.groupBy(out)).map(d => ({ name: d[0], count: d.length }));
    let result = {}
    r.forEach(elm => {
        result[elm.name] = elm.count;
    });
    return result
}

function getCount(o, k) {
    return _.has(o, k) ? o[k] : 0
}