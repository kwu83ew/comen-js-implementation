const utils = require('../../../utils/utils')

describe('Utils._nil', () => {
    it('Should return true for null', () => {
        expect(utils._nil(null)).toEqual(true)
    });
    it('Should return true for undefined', () => {
        expect(utils._nil(undefined)).toEqual(true)
    });
    it('Should return true for undefined', () => {
        expect(utils._nil()).toEqual(true)
    });
    it('Should return false for empty', () => {
        expect(utils._nil('')).toEqual(false)
    });
    it('Should return false for false', () => {
        expect(utils._nil(false)).toEqual(false)
    });

});


describe('Utils._false', () => {
    it('Should return true for false', () => {
        expect(utils._false(false)).toEqual(true)
    });
    it('Should return false for true', () => {
        expect(utils._false(true)).toEqual(false)
    });
    it('Should return fasle for empty', () => {
        expect(utils._false('')).toEqual(true)
    });
    it('Should return false for null', () => {
        expect(utils._false(null)).toEqual(false)
    });
    it('Should return false for undefined', () => {
        expect(utils._false()).toEqual(false)
    });
    it('Should return false for undefined', () => {
        expect(utils._false(undefined)).toEqual(false)
    });

});