const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.intReverseSort', () => {
    it('Should calculate intReverseSort', () => {
        expect(utils.intReverseSort([1])).toEqual([1]);
        expect(utils.intReverseSort([1, 3, 2])).toEqual([3, 2, 1]);
        expect(utils.intReverseSort([1, 13, 2])).toEqual([13, 2, 1]);
        expect(utils.intReverseSort([1, 13, 2, 25])).toEqual([25, 13, 2, 1]);
    });

    it('Should calculate intReverseSort', () => {
        expect(utils.intReverseSort(["1"])).toEqual([1]);
        expect(utils.intReverseSort([1, "3", '2'])).toEqual([3, 2, 1]);
        expect(utils.intReverseSort([1, 13, '02'])).toEqual([13, 2, 1]);
        expect(utils.intReverseSort([1, "0013", 2, '25'])).toEqual([25, 13, 2, 1]);
    });



});