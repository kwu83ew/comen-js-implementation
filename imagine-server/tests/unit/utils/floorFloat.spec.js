const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.iFloorFloat', () => {
    it('Should calculate iFloorFloat', () => {
        expect(utils.iFloorFloat(1)).toEqual(1);
        expect(utils.iFloorFloat(134217728)).toEqual(134217728);
        expect(utils.iFloorFloat(9896665478913551)).toEqual(9896665478913551);
        expect(utils.iFloorFloat(134217728.9896665478913551)).toEqual(134217728.9896665478);
        expect(utils.iFloorFloat(0.989666547891355) == utils.iFloorFloat(0.9896665478913551)).toEqual(true);
        expect(utils.customFloorFloat(0.9896665478913551, 10)).toEqual(0.9896665478);
    });

    it('Should calculate iFloorFloat', () => {
        expect(utils.iFloorFloat(1, 3)).toEqual(1, 3);
        expect(utils.iFloorFloat(1000000.002)).toEqual(1000000.002);
        expect(utils.iFloorFloat(1000000.002, 3)).toEqual(1000000.002);
        expect(utils.iFloorFloat(1000000.002, 4)).toEqual(1000000.002);
        expect(utils.iFloorFloat(1000000.002)).toEqual(1000000.002);
        expect(utils.iFloorFloat(1000000.002)).toEqual(1000000.0020);
        expect(utils.iFloorFloat(1000000.002)).toEqual(1000000.00200);

        expect(utils.iFloorFloat(1.0000000000001)).toEqual(1);
        expect(utils.iFloorFloat(1.000000000001)).toEqual(1);
        expect(utils.iFloorFloat(1.00000000001)).toEqual(1.00000000001);    // drops all digit after 11th (if is less than -1 or greater 1)
        expect(utils.iFloorFloat(1.00000000001234)).toEqual(1.00000000001);    // drops all digit after 11th (if is less than -1 or greater 1)
        expect(utils.iFloorFloat(0.00000000001)).toEqual(0);                // drops all digit after 10th (if is between -1 and 1)
        expect(utils.iFloorFloat(0.0000000001)).toEqual(0.0000000001);
        
        expect(utils.customFloorFloat(10.001, 0)).toEqual(10);
        expect(utils.customFloorFloat(0.001, 0)).toEqual(0);
        expect(utils.customFloorFloat(0.00001, 2)).toEqual(0);
        // expect(utils.iFloorFloat(.00001, 3)).toEqual(0.0);
    });

    console.log(Math.floor('.01'));
    // 52039905881413
});
