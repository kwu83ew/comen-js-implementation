const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.', () => {
    it('Should return a list 1', () => {
        let t = utils.getNow()
        expect(iutils.isInCurrentCycle(t)).toBeTruthy();
        expect(iutils.isInCurrentCycle(t.split(' ')[0]+' 00:00:00')).toBeFalsy();

    });

});