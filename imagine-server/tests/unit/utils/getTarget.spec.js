const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.getTarget', () => {
    it('Should getTarget', () => {
        expect(utils.getTarget('2019-10-23 25:23:34')).toEqual('191023');
        expect(utils.getTarget('2010-10-10 25:23:34')).toEqual('101010');
        expect(utils.getTarget('2020-01-01 25:23:34')).toEqual('200101');
        expect(utils.getTarget('2117-10-31 25:23:34')).toEqual('171031');
    });


});