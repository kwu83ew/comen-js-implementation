const utils = require('../../../utils/utils')


describe('Utils.objLength', () => {

    it('Should return zero for NaN', () => {
        expect(utils.objLength(NaN)).toEqual(0)
    });
    it('Should return zero for true', () => {
        expect(utils.objLength(true)).toEqual(0)
    });
    it('Should return zero for false', () => {
        expect(utils.objLength(false)).toEqual(0)
    });
    it('Should return zero for ""', () => {
        expect(utils.objLength("")).toEqual(0)
    });
    it('Should return zero for {}', () => {
        expect(utils.objLength({})).toEqual(0)
    });
    it('Should return 1 for { "": "" }', () => {
        expect(utils.objLength({ "": "" })).toEqual(1)
    });
    it('Should return 1 for { "": {} }', () => {
        expect(utils.objLength({ "": {} })).toEqual(1)
    });
    it('Should return 1 for { "": {"":""} }', () => {
        expect(utils.objLength({ "": { "": "" } })).toEqual(1)
    });
    it('Should return 1 for { "": { "": "" }, "": "" }', () => {
        expect(utils.objLength({ "": { "": "" }, "": "" })).toEqual(1)
    });
    it('Should return 1 for { "": { "": "" }, " ": "" }', () => {
        expect(utils.objLength({ "": { "": "" }, " ": "" })).toEqual(2)
    });
});