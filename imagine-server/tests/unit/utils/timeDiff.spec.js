const utils = require("../../../utils/utils");


describe('Utils.timeDiff', () => {
    it('Should timeDiff zero', () => {
        let diff;
        diff = utils.timeDiff('2000-01-01 00:00:00', '2000-01-01 00:00:00');
        expect(diff).toEqual({
            "asDays": 0,
            "asHours": 0,
            "asMinutes": 0,
            "asMonths": 0,
            "asSeconds": 0,
            "asYears": 0,
            "days": 0,
            "hours": 0,
            "minutes": 0,
            "months": 0,
            "seconds": 0,
            "years": 0,
        });
    });

    it('Should timeDiff 1 second', () => {
        let diff;
        diff = utils.timeDiff('2000-01-01 00:00:00', '2000-01-01 00:00:01');
        expect(diff).toEqual({
            "asDays": 0,
            "asHours": 0,
            "asMinutes": 0,
            "asMonths": 0,
            "asSeconds": 1,
            "asYears": 0,
            "days": 0,
            "hours": 0,
            "minutes": 0,
            "months": 0,
            "seconds": 1,
            "years": 0,
        });
    });

    it('Should timeDiff 61 second = 1 min and 1 second', () => {
        let diff;
        diff = utils.timeDiff('2000-01-01 00:00:00', '2000-01-01 00:01:01');
        expect(diff).toEqual({
            "asDays": 0,
            "asHours": 0,
            "asMinutes": 1,
            "asMonths": 0,
            "asSeconds": 61,
            "asYears": 0,
            "days": 0,
            "hours": 0,
            "minutes": 1,
            "months": 0,
            "seconds": 1,
            "years": 0,
        });
    });

    it('Should timeDiff 1 hour and 1 min and 1 second', () => {
        let diff;
        diff = utils.timeDiff('2000-01-01 00:00:00', '2000-01-01 01:01:01');
        expect(diff).toEqual({
            "asDays": 0,
            "asHours": 1,
            "asMinutes": 61,
            "asMonths": 0,
            "asSeconds": 3661,
            "asYears": 0,
            "days": 0,
            "hours": 1,
            "minutes": 1,
            "months": 0,
            "seconds": 1,
            "years": 0,
        });
    });

    it('Should timeDiff 1 day 1 hour and 1 min and 1 second', () => {
        let diff;
        diff = utils.timeDiff('2000-01-01 00:00:00', '2000-01-02 01:01:01');
        expect(diff).toEqual({
            "asDays": 1,
            "asHours": 25,
            "asMinutes": 1501,
            "asMonths": 0,
            "asSeconds": 90061,
            "asYears": 0,
            "days": 1,
            "hours": 1,
            "minutes": 1,
            "months": 0,
            "seconds": 1,
            "years": 0,
        });
    });

});