const utils = require("../../../utils/utils");


describe('Utils.removeFromArray', () => {
    it('Should remove 1 from array', () => {
        expect(utils.removeFromArray([1, 2, 3], 1)).toEqual([2, 3])
    });
    it('Should remove two times 1 from array', () => {
        expect(utils.removeFromArray([1, 2, 3, 1], 1)).toEqual([2, 3])
    });
    it('Should remove "1" and 1 from array', () => {
        expect(utils.removeFromArray([1, 2, 3, "1"], "1")).toEqual([2, 3])
    });
    it('Should remove ""  from array', () => {
        expect(utils.removeFromArray([1, 2, 3, ""], "")).toEqual([1, 2, 3])
    });
    it('Should remove null from array', () => {
        expect(utils.removeFromArray([1, null, 2, 3], null)).toEqual([1, 2, 3])
    });
    it('Should remove only "null" from array', () => {
        expect(utils.removeFromArray([1, null, 2, 3, "null"], "null")).toEqual([1, null, 2, 3])
    });

});

describe('Utils.arrayDiff', () => {
    expect(utils.arrayDiff([], [])).toEqual([])
    expect(utils.arrayDiff([1], [1])).toEqual([])
    expect(utils.arrayDiff([1, 2], [1])).toEqual([2])
    expect(utils.arrayDiff([1, 2, 3], [2, 3])).toEqual([1])
    expect(utils.arrayDiff([1, 2, 3], [2, 3, 4])).toEqual([1])
    expect(utils.arrayDiff([1, 2, 3], [2, 3, 1])).toEqual([])
    expect(utils.arrayDiff([1, 2], [2, 3, 1])).toEqual([])
    expect(utils.arrayDiff(['1', '2'], ['2', '3', '1'])).toEqual([])
});

describe('Utils.arrayUnique', () => {
    it('Should return unique for empty', () => {
        expect(utils.arrayUnique('')).toEqual([])
    });
    it('Should return unique for empty array', () => {
        expect(utils.arrayUnique([''])).toEqual([''])
    });
    it('Should return unique for empty array', () => {
        expect(utils.arrayUnique(['a', 'a', 'a', 'a'])).toEqual(['a'])
    });
    it('Should return unique for empty array', () => {
        expect(utils.arrayUnique([5, 5, 5, 5, 5, 5, 5, 5, 5])).toEqual([5])
    });
    it('Should return unique for multi array', () => {
        expect(utils.arrayUnique(['', 1, '', 2, 1])).toEqual(['', 1, 2])
    });
    it('Should return unique for multi array', () => {
        expect(utils.arrayUnique(["''", 31, '', 2, 1, ''])).toEqual(["''", 31, '', 2, 1])
    });
    it('Should return unique for multi array', () => {
        expect(utils.arrayUnique([31, -1, 2, 1, '', -1, "", 31])).toEqual([31, -1, 2, 1, ''])
    });

});