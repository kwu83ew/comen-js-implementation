const utils = require('../../../utils/utils')


describe('Utils.objKeys', () => {
    it('Should return empty array for empty', () => {
        expect(utils.objKeys({})).toEqual([])
    });
    it('Should return array of empty for empty attr', () => {
        expect(utils.objKeys({ '': '' })).toEqual([''])
    });
    it('Should return array of 1 for 1 attr', () => {
        expect(utils.objKeys({ '1': '' })).toEqual(['1'])
    });
    it('Should return array of attr keys', () => {
        expect(utils.objKeys({ '1': '', 'a': '' })).toEqual(['1', 'a'])
    });
    it('Should return array of attr keys', () => {
        expect(utils.objKeys({ '1': '', 'a': '', '1': '1' })).toEqual(['1', 'a'])
        expect(utils.objKeys({ '1': '', 'a': '', '1': '1' })).not.toEqual(['a', '1'])
    });


});