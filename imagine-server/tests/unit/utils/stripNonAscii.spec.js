const utils = require("../../../utils/utils");


describe('utils.stripNonAscii', () => {
    it('Should stripNonAscii', () => {
        expect(utils.stripNonAscii('Date 2012-01-01 12:00:00')).toEqual('Date 2012-01-01 12:00:00');
        expect(utils.stripNonAscii('`~\\|"\'<,.>;:!@#$%^&*()-_=+[]{ }')).toEqual('`~\\|"\'<,.>;:!@#$%^&*()-_=+[]{ }');
        expect(utils.stripNonAscii('¡Hola!')).toEqual('Hola!');
        expect(utils.stripNonAscii('¡Aló!')).toEqual('Al!');
        expect(utils.stripNonAscii('Allô!')).toEqual('All!');
        expect(utils.stripNonAscii('你好')).toEqual('');
        expect(utils.stripNonAscii('ନମସ୍କାର')).toEqual('');
        expect(utils.stripNonAscii('٧عشرة')).toEqual('');
    });

});


