const utils = require('../../../utils/utils')


describe('Utils._empty', () => {
    it('Should return true for empty', () => {
        expect(utils._empty('')).toEqual(true)
    });
    it('Should return false for null', () => {
        expect(utils._empty(null)).toEqual(false)
    });
    it('Should return false for undefined', () => {
        expect(utils._empty()).toEqual(false)
    });
    it('Should return false for undefined', () => {
        expect(utils._empty(undefined)).toEqual(false)
    });
    it('Should return false for false', () => {
        expect(utils._empty(false)).toEqual(false)
    });
    it('Should return false for true', () => {
        expect(utils._empty(true)).toEqual(false)
    });

});

// describe('Utils._notEmpty', () => {
//     it('Should return true for not empty', () => {
//         expect(utils._notEmpty('1')).toEqual(true)
//     });
//     it('Should return false for empty', () => {
//         expect(utils._notEmpty('')).toEqual(false)
//     });
//     // it('Should return false for null', () => {
//     //     expect(utils._notEmpty(null)).toEqual(false)
//     // });
//     it('Should return true for undefined', () => {
//         expect(utils._notEmpty()).toEqual(true)
//     });
//     // it('Should return false for undefined', () => {
//     //     expect(utils._notEmpty(undefined)).toEqual(false)
//     // });
//     // it('Should return false for false', () => {
//     //     expect(utils._notEmpty(false)).toEqual(false)
//     // });
//     // it('Should return false for true', () => {
//     //     expect(utils._notEmpty(true)).toEqual(false)
//     // });

// });