const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('iUtils.microPAIToPAI', () => {
    it('Should calculate microPAIToPAI', () => {
        expect(utils.microPAIToPAI(1)).toEqual('0.000');
        expect(utils.microPAIToPAI(10)).toEqual('0.000');
        expect(utils.microPAIToPAI(21)).toEqual('0.000');
        expect(utils.microPAIToPAI(100)).toEqual('0.000');
        expect(utils.microPAIToPAI(321)).toEqual('0.000');
        expect(utils.microPAIToPAI(4321)).toEqual('0.004');
        expect(utils.microPAIToPAI(654321)).toEqual('0.654');
        expect(utils.microPAIToPAI(9876543210)).toEqual('9,876.543');
        expect(utils.microPAIToPAI(989666547891355)).toEqual('989,666,547.891');
    });

    it('Should calculate microPAIToPAI for negativ e values', () => {
        expect(utils.microPAIToPAI(-1)).toEqual('0.000');
        expect(utils.microPAIToPAI(-10)).toEqual('0.000');
        expect(utils.microPAIToPAI(-21)).toEqual('0.000');
        expect(utils.microPAIToPAI(-100)).toEqual('0.000');
        expect(utils.microPAIToPAI(-321)).toEqual('0.000');
        expect(utils.microPAIToPAI(-4321)).toEqual('-0.004');
        expect(utils.microPAIToPAI(-654321)).toEqual('-0.654');
        expect(utils.microPAIToPAI(-9876543210)).toEqual('-9,876.543');
        expect(utils.microPAIToPAI(-989666547891355)).toEqual('-989,666,547.891');
    });

});