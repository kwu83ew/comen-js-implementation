const utils = require("../../../utils/utils");
const iutils = require("../../../utils/iutils");


describe('utils.isValidDateForamt', () => {
    it('Should validate date format', () => {
        expect(utils.isValidDateForamt()).toEqual(false);
        expect(utils.isValidDateForamt('')).toEqual(false);
        expect(utils.isValidDateForamt(null)).toEqual(false);
        expect(utils.isValidDateForamt(undefined)).toEqual(false);
        expect(utils.isValidDateForamt(NaN)).toEqual(false);
        expect(utils.isValidDateForamt(utils.getNow())).toEqual(true);
    });
});





