const _ = require('lodash');

describe('lodash.has', () => {
    it('Should control _.has', () => {
        let args = {};
        expect(_.has(args, 'keys')).toEqual(false);
        
        args['level1'] = {};
        expect(_.has(args, 'level1')).toEqual(true);
        
        args['level1']['level2'] = {};
        expect(_.has(args['level1'], 'level2')).toEqual(true);
        expect(_.has(args['level1'], 'level3')).toEqual(false);
        
        args['level1']['level2']['level3'] = {};
        expect(_.has(args['level1']['level2'], 'level3')).toEqual(true);
        expect(!_.has(args['level1']['level2'], 'level4')).toEqual(true);
        
        args['level1']['level2']['level3']['level4'] = {};
        expect(_.has(args['level1']['level2']['level3'], 'level4')).toEqual(true);
        expect(!_.has(args['level1']['level2']['level3'], 'level5')).toEqual(true);

    });



});