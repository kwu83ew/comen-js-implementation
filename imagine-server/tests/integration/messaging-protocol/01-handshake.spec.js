const _ = require('lodash');
const utils = require("../../../utils/utils");
const request = require('supertest');
const constants = require('../../../config/constants')
const db = require('../../../startup/db2')
const machine = require("../../../machine/machine-handler");
// const parseHandshake = require("../../../messaging-protocol/parsers/01-handshake")
const testInit = require("../init")
const crypto = require("../../../crypto/crypto");

let server;



jest.setTimeout(7000);



describe('handshake', () => {


    testInit.emptyHardCopy()

    const shakerEmail = 'hand@shaker.com'
    const myNeighborEmail = 'myNeighbor@test.com'
    const neighborOfNeighbor = 'neighborOfNeighbor@hello.com'

    beforeEach(() => { server = require('../../../index'); });

    afterEach(() => {
        server.close();
    });

    it('Should handshake to a neighbor', async () => {
        require("../init").initAlterServerTEST();
        // server = require('../../../index');

        // retreive machine info
        let machineSettings = machine.getMProfileSettingsSync()

        //change sender's email
        machineSettings.pubEmail.address = shakerEmail;
        machine.updateMachineEmailSettingsSync({ params: machineSettings.pubEmail, emailType: 'pub' });

        //also remove sender email from receiver's neighbor(if exists)
        // db.togglePool(db.asyncAlterDB);
        db.setAppCloneId(1);
        machine.deleteNeighborSync({
            query: [
                ['n_email', shakerEmail]
            ]
        });
        // add a neighbor for my-neighbor
        let PGPpairKey = crypto.nativeGenerateKeyPairSync();
        machine.neighborHandler.addANeighborSync({
            'nEmail': neighborOfNeighbor,
            'nPGPPubKey': PGPpairKey.pubKey,
        });
        // retreive & set alter machine info
        let alterMachineSettings = machine.getMProfileSettingsSync();
        alterMachineSettings.pubEmail.address = myNeighborEmail;
        alterMachineSettings.alreadyPresentedNeighbors = []
        machine.updateMachineSettings({ machineSettings: alterMachineSettings });
        // db.togglePool(db.asyncDB);
        db.setAppCloneId(0);

        // delete if myNeighborEmail exists
        machine.deleteNeighborSync({
            query: [
                ['email', myNeighborEmail]
            ]
        })

        // insert a new neighbor
        machine.neighborHandler.addANeighborSync({
            nEmail: myNeighborEmail,
            nPGPPubKey: ''
        });

        // retreive newly inserted neightbor
        let newN = machine.neighborHandler.getNeighborsSync({
            query: [
                ['email', myNeighborEmail]
            ]
        })

        // calling http
        let r = await request(server).post('/api/machine/neighbor/handshake').send({ "id": newN[0].id, "isPublic": iConsts.CONSTS.YES })
        expect(r.status).toBe(200);

        if (constants.ACTIVE_FAKE_NETWORK == true) {
            //  db.togglePool(db.asyncAlterDB)


            // // parse hand shake message
            // let packet = await networker.iPull({ target: myNeighborEmail })
            // expect(packet.title).not.toBe('');
            // expect(packet.sender).toBe(machineSettings.pubEmail.address);
            // expect(parseInt(packet.creation_date)).toBeGreaterThan(0);
            // expect(packet.fileId).toBe([packet.creation_date, machineSettings.pubEmail.address, myNeighborEmail].join(','));

            // await unwrapPacketSync(packet);
            // // TODO: maybe some tests!

            // // analyse decrypted message in receiver part
            // let rawPacket = await networker.iPull({
            //     target: 'ITL_decryptedMessage', // Integration Test label
            //     sender: machineSettings.pubEmail.address
            // });
            // expect(rawPacket.title).not.toBe('');
            // expect(rawPacket.fileId).toBe([rawPacket.creation_date, machineSettings.pubEmail.address, 'ITL_decryptedMessage'].join(','));
            // let payloadMessage = JSON.parse(rawPacket.message.split(iConsts.msgTags.customStartEnvelope)[1].split(hardCopy.customEndEnvelope)[0])
            // expect(payloadMessage.error).toBe(null);
            // expect(payloadMessage.verified).toBe(false);
            // expect(payloadMessage.isAuthenticated).toBe(false);
            // expect(payloadMessage.isSigned).toBe(false);
            // expect(payloadMessage.isCompressed).toBe(true);
            // expect(payloadMessage.iPGPVersion).toBe('0.0.0');
            // let orginalHandshakeMessage = JSON.parse(payloadMessage.message)
            // expect(orginalHandshakeMessage.type).toBe(MESSAGE_TYPES.HANDSHAKE);
            // expect(orginalHandshakeMessage.mVer).toBe('0.0.0');
            // expect(orginalHandshakeMessage.connectionType).toBe(iConsts.CONSTS.YES);
            // expect(orginalHandshakeMessage.pubEmail).toBe(machineSettings.pubEmail.address);
            // expect(orginalHandshakeMessage.PGPPubKey).toBe(machineSettings.pubEmail.PGPPubKey);


            // // check if new neighbor is added to alterDB.i_machine_neighbors?
            // let alterNeighbor = await machine.getNeighbors({ query: [['email', machineSettings.pubEmail.address ]] })
            // expect(alterNeighbor[0].email).toBe(machineSettings.pubEmail.address);

            // // check if niceToMeetYouMessage is sent
            //  db.togglePool(db.asyncDB);
            // let niceToMeetYouMessage = await networker.iPull({
            //     sender: myNeighborEmail,
            //     target: machineSettings.pubEmail.address,
            //     title: 'niceToMeetYou'
            // });
            // expect(niceToMeetYouMessage.title.search([myNeighborEmail, machineSettings.pubEmail.address, 'niceToMeetYou'].join(','))).toBeGreaterThan(0);
            // //check if niceToMeetYouMessage is valid
            // await unwrapPacketSync(niceToMeetYouMessage);
            // // check if the machine has updated pgp public key for myNeighborEmail?
            // let refreshedNewN = await neighbors.getNeighbors({ query: [['email', myNeighborEmail ]] });
            // expect(refreshedNewN[0].n_pgp_public_key).toBe(alterMachineSettings.pubEmail.PGPPubKey);

            // // check broadcast email to neighbors (take only one)
            // let hereIsNewNeighbor = await networker.iPull({
            //     sender: myNeighborEmail,
            //     target: neighborOfNeighbor,
            //     title: 'hereIsNewNeighbor'
            // });
            // await unwrapPacketSync(hereIsNewNeighbor);
            // hereIsNewNeighbor = null


            // // switch back to normal db
            //  db.togglePool(db.asyncDB);
        }

    })

});
