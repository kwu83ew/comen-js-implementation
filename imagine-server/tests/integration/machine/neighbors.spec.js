const request = require('supertest');
const _ = require('lodash');
const utils = require("../../../utils/utils");
let server;

describe('/api/neighbors/list', () => {
    beforeEach(() => { server = require('../../../index'); })
    afterEach(() => { server.close(); })

    it('Should return all neighbors list', async() => {
        let res = await request(server).get('/api/machine/neighbors/list')
        let neighbors = JSON.parse(res.res.text)
            // _.forOwn(res.res, (value, key) => {
            //     clog.app.info(key);
            // })
        expect(res.status).toBe(200);
        expect(neighbors.length).toBeGreaterThan(0);
        expect(parseInt(neighbors[0].id)).toBeGreaterThan(0);
        expect(utils._notEmpty(neighbors[0].email)).toBeTruthy();
        // expect(neighbors[0]).toHaveProperty('email', 'hu1@hu.hu');
        // expect(utils._empty(neighbors[0].n_pgp_public_key)).toBeFalsy();
    })
});