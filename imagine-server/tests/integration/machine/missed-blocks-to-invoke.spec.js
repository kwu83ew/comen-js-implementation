const _ = require('lodash')
const db = require('../../../startup/db2')
const missedBlocksHandler = require("../../../dag/missed-blocks-handler");

// const clog = require('../../../../loggers/console_logger');
// const getMoment = require("../../../../startup/singleton").instance.getMoment
// const utils = require("../../../../utils/utils");
// const iutils = require("../../../../utils/iutils");
// const cb = require("../../../../dag/coinbase/interface");
// const lib1 = require("../../lib");
// const iConsts = require("../../../../config/constants");
// const model = require("../../../../models/interface")
// const networker = require('../../../../network-adapter/networker');
// const networkListener = require('../../../../startup/threads/network-listener');
// const kvHandler = require("../../../../models/kvalue");
// const leavesHandler = require("../../../../dag/leaves-handler");
// const machine = require("../../../../machine/interface");
// const bdLogger = require("../../../../loggers/broadcast-logger")
// const DNAHandler = require("../../../../dna/dna-handler");
let server;


jest.setTimeout(20 * 1000);

describe('Missed blocks to invoke', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(() => { server.close(); });

    it('Should empty imgdb_test & do some insertion delete', () => {
        db.sEmptyDB();
        let recorderHashes;
        missedBlocksHandler.addMissedBlocksToInvoke('a');
        recorderHashes = missedBlocksHandler.getMissedBlocksToInvoke();
        expect(recorderHashes.length).toBe(1);
        expect(recorderHashes).toBe('a');

        missedBlocksHandler.addMissedBlocksToInvoke('b');
        recorderHashes = missedBlocksHandler.getMissedBlocksToInvoke();
        expect(recorderHashes.length).toBe(3);
        expect(recorderHashes).toBe('a,b');

        missedBlocksHandler.addMissedBlocksToInvoke('a');
        recorderHashes = missedBlocksHandler.getMissedBlocksToInvoke();
        expect(recorderHashes.length).toBe(3);
        expect(recorderHashes).toBe('a,b');

        missedBlocksHandler.addMissedBlocksToInvoke('c');
        recorderHashes = missedBlocksHandler.getMissedBlocksToInvoke();
        expect(recorderHashes.length).toBe(5);
        expect(recorderHashes).toBe('a,b,c');




    });

});

function processIt(arr) {
    let out = {};
    arr.forEach(element => {
        out[element] = true
    });
    return out;
}
