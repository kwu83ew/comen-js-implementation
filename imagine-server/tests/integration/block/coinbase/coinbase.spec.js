const _ = require('lodash')
const db = require('../../../../startup/db2')
const clog = require('../../../../loggers/console_logger');
const getMoment = require("../../../../startup/singleton").instance.getMoment
const utils = require("../../../../utils/utils");
const iutils = require("../../../../utils/iutils");
const coinbaseCreator = require('../../../../dag/coinbase/create-coinbase-block');
const lib1 = require("../../lib");
const iConsts = require("../../../../config/constants");
const model = require("../../../../models/interface")
// const networker = require('../../../../network-adapter/networker');
// const networkListener = require('../../../../startup/threads/network-listener');
const kvHandler = require("../../../../models/kvalue");
const parseQHandler = require("../../../../services/parsing-q-manager/parsing-q-handler");
const leavesHandler = require("../../../../dag/leaves-handler");
const machine = require("../../../../machine/machine-handler");
const sendQ = require("../../../../services/sending-q-manager/sending-q-manager");
const bdLogger = require("../../../../loggers/broadcast-logger")
const DNAHandler = require("../../../../dna/dna-handler");

let server;


jest.setTimeout(60 * 1000);

// "scripts": {
//     "test": "jest --watchAll "
// },

describe('Coinbase blocks testing', () => {
    beforeEach(() => { server = require('../../../../index'); });
    afterEach(() => { server.close(); });

    it('Should empty both imgdb_test & imgdb1_test', () => {
        let clonesDict = {
            0: { name: 'neighbor', email: 'neighbor@imagine.com' },
            1: { name: 'hu1', email: 'hu1@imagine.com' },
        }

        // db.appId = 0;
        // db.getAppCloneId();
        db.setAppCloneId(0);

        iConsts.CBM_SETTED = undefined
        iConsts.setCycleByMinutes(2);
        iConsts.FORCED_LAUNCH_DATE = null
        iConsts.LD_SETTED = undefined
        iConsts.setLaunchDate(getMoment().subtract({ 'minutes': iConsts.getCycleByMinutes() - 1 }).format('YYYY-MM-DD HH:mm:ss'));
        clog.app.info(`LaunchDate: ${iConsts.getLaunchDate()}`);
        lib1.setServerAndAlterServer();
        lib1.cleanFolderAndAlterFoler();


        // remove info
        db.setAppCloneId(0);
        machine.deleteNeighborSync({
            query: [
                ['email', 'hu2@imagine.com']
            ]
        })
        // db.setAppCloneId(1);

        let res, inx;

        // check pgpkeys
        db.setAppCloneId(0);
        let neighborMachineSettings = machine.getMProfileSettingsSync();
        let neighborNeighbors = machine.neighborHandler.getNeighborsSync();
        // clog.app.info(neighborMachineSettings);
        // clog.app.info(neighborNeighbors);
        db.setAppCloneId(1);
        let hu1MachineSettings = machine.getMProfileSettingsSync();
        let hu1Neighbors = machine.neighborHandler.getNeighborsSync();
        // db.setAppCloneId(2);
        // let aliceMachineSettings = machine.getMProfileSettingsSync();
        // let aliceNeighbors = machine.neighborHandler.getNeighborsSync();
        // db.setAppCloneId(3);
        // let bobMachineSettings = machine.getMProfileSettingsSync();
        // let bobNeighbors = machine.neighborHandler.getNeighborsSync();
        // db.setAppCloneId(4);
        // let eveMachineSettings = machine.getMProfileSettingsSync();
        // let eveNeighbors = machine.neighborHandler.getNeighborsSync();
        // clog.app.info(hu1MachineSettings);
        // clog.app.info(hu1Neighbors);
        expect(neighborMachineSettings.pubEmail.address).toBe('neighbor@imagine.com');
        expect(neighborMachineSettings.pubEmail.PGPPubKey).toBe(hu1Neighbors[0].n_pgp_public_key);

        expect(hu1MachineSettings.pubEmail.address).toBe('hu1@imagine.com');
        expect(hu1Neighbors[0].email).toBe('neighbor@imagine.com');
        expect(hu1MachineSettings.pubEmail.PGPPubKey).toBe(neighborNeighbors[0].n_pgp_public_key);





        // controll genesis block insertion for both master & neighbor
        db.setAppCloneId(0);
        let gBlock, gBody
        gBlock = model.sCustom({
            query: 'SELECT * FROM i_blocks',
            values: []
        })
        expect(gBlock.length).toBe(1);
        gBlock = gBlock[0]
        gBody = JSON.parse(gBlock.body);
        expect(gBody.bType).toBe('Genesis');
        expect(gBody.ancestors).toBe('');
        expect(gBody.descriptions).toBe('Imagine all the people sharing all the world');
        expect(gBody.docs.length).toBe(1);
        let leaves = leavesHandler.getLeaveBlocks();
        expect(utils.objKeys(leaves).length).toBe(1);
        let leaveHash = utils.objKeys(leaves)[0]
        expect(leaveHash).toBe(gBody.blockHash);
        db.setAppCloneId(1);
        gBlock = model.sCustom({
            query: 'SELECT * FROM i_blocks',
            values: []
        })
        expect(gBlock.length).toBe(1);
        gBlock = gBlock[0]
        gBody = JSON.parse(gBlock.body);
        expect(gBody.bType).toBe('Genesis');
        expect(gBody.ancestors).toBe('');
        expect(gBody.descriptions).toBe('Imagine all the people sharing all the world');
        expect(gBody.docs.length).toBe(1);
        leaves = leavesHandler.getLeaveBlocks();
        expect(utils.objKeys(leaves).length).toBe(1);
        leaveHash = utils.objKeys(leaves)[0]
        expect(leaveHash).toBe(gBody.blockHash);

        let dummyShareTime = getMoment().format('YYYY-MM-DD HH:mm:ss')


        clog.app.info('******************************************');

        //////////////////////////////////  loop  coin bases /////////////////////////////////

        let prevBlock = gBlock;
        for (let cycles = 0; cycles < 2; cycles++) {
            clog.app.info(`################################### cycle ${cycles}`);

            // try to create first coinbase block either by master or neighbor
            let masterCB = null;
            let neighborCB = null;
            let coinbaseCreated = false;
            inx = 0;
            while (!coinbaseCreated) {
                inx += 1
                db.setAppCloneId(1);
                clog.app.info(`> > > Coinbase Creating by Hu ${inx}`);
                masterCB = coinbaseCreator.createCoinbaseBlock();
                coinbaseCreated = _.has(masterCB, 'coinbaseCreated') ? masterCB.coinbaseCreated : false
                if (coinbaseCreated == false) {
                    utils.sleepBySecond(iConsts.getCycleByMinutes() * 7)
                    clog.app.info(`> > > Coinbase Creating by Neighbor ${inx}`);
                    db.setAppCloneId(0);
                    neighborCB = coinbaseCreator.createCoinbaseBlock();
                    coinbaseCreated = _.has(neighborCB, 'coinbaseCreated') ? neighborCB.coinbaseCreated : false
                }
            }
            if (_.has(masterCB, 'coinbaseCreated'))
                clog.app.info(`masterCB.coinbaseCreated ${masterCB.coinbaseCreated}`);
            if (_.has(neighborCB, 'coinbaseCreated'))
                clog.app.info(`neighborCB.coinbaseCreated ${neighborCB.coinbaseCreated}`);
            let CBCreator, creatorApp, notCreatorApp;
            if (_.has(masterCB, 'coinbaseCreated') && (masterCB.coinbaseCreated == true)) {
                CBCreator = masterCB;
                creatorApp = 1
                notCreatorApp = 0
            }
            if (_.has(neighborCB, 'coinbaseCreated') && (neighborCB.coinbaseCreated == true)) {
                CBCreator = neighborCB;
                creatorApp = 0
                notCreatorApp = 1
            }
            let coinbaseCycleStamp = iutils.getCoinbaseCycleStamp()
            clog.app.info(`<<< coinbase creator: ${clonesDict[creatorApp].name} >>> ${coinbaseCycleStamp}`);
            expect(CBCreator.coinbaseCreated).toBe(true);
            //retrieve from table and create proper file on outbox folder
            db.setAppCloneId(creatorApp);
            sendQ.sendOutThePacket()



            // switch to not creator which is teorically receiver, parser, confirmer and publisher of this coinbase block
            db.setAppCloneId(notCreatorApp);
            let packet = null;
            inx = 0;
            while (utils._nilEmptyFalse(packet) && (inx < 10)) {
                inx += 1
                clog.app.info(`> > > read from ${clonesDict[notCreatorApp].name} hard-drive ${inx}`);
                packet = networker.iPullSync()
                utils.sleepBySecond(iConsts.getCycleByMinutes() * 7)
            }
            expect(packet.sender).toBe(clonesDict[creatorApp].email);
            let unwrapRes = networkListener.unwrapPacketSync(packet)
            clog.app.info(unwrapRes);
            expect(unwrapRes.err != false).toBe(null);
            expect(unwrapRes.connection_type).toBe(iConsts.CONSTS.PUBLIC);
            expect(unwrapRes.shouldPurgeMessage).toBe(true);
            let toParseMessage = unwrapRes.message
            expect(toParseMessage.bType).toBe('coinbase');
            expect(toParseMessage.ancestors).toBe(prevBlock.block_hash);
            let blocksToParse = model.sCustom({
                query: 'SELECT * FROM i_parsing_q ORDER BY pq_last_modified',
                values: []
            })
            expect(blocksToParse.length).toBe(1);
            blocksToParse = blocksToParse[0]
            expect(blocksToParse.sender).toBe(clonesDict[creatorApp].email);
            //manual pull q
            let toparse = null;
            inx = 0
            while (utils._nilEmptyFalse(toparse)) {
                inx += 1
                clog.app.info(`> > > fetchParseQSync on ${clonesDict[notCreatorApp].name} ${inx}`);
                toparse = parseQHandler.fetchParseQSync()
                utils.sleepBySecond(iConsts.getCycleByMinutes() * 7)
            }
            expect(toparse.length).toBe(1);
            toparse = toparse[0]
            // clog.app.info(JSON.parse(toparse.payload));
            // smart pull q to parsing
            let sPQRes = parseQHandler.qPicker.smartPullQSync()
            clog.app.info(sPQRes);
            // controlling i_blocks
            cbReceiverBlocks = model.sCustom({
                query: 'SELECT * FROM i_blocks ORDER BY creation_date',
                values: []
            })
            expect(parseInt(cbReceiverBlocks.length)).toBe(2 + cycles);
            expect(cbReceiverBlocks[1 + cycles].ancestors).toBe(cbReceiverBlocks[0 + cycles].block_hash);
            let body0 = JSON.parse(cbReceiverBlocks[0 + cycles].body);
            if (body0.bType == 'Genesis') {
                expect(body0.ancestors).toBe('');
                expect(body0.descriptions).toBe('Imagine all the people sharing all the world');
                expect(body0.docs.length).toBe(1);
                let doc0 = body0.docs[0 + cycles]
                expect(doc0.dType).toBe("DNAProposal");
                expect(doc0.helpHours).toBe(61355);
                expect(doc0.helpLevel).toBe(6);
                expect(doc0.shares).toBe(368130);
                expect(doc0.votesY).toBe(368130);
                expect(doc0.votesN).toBe(0);

            } else {

            }

            // control dna
            switch (cycles) {
                case 0:
                    expect(body0.docs.length).toBe(0);
                    break;

                case 1:
                case 2:
                    expect(body0.docs.length).toBe(1);
                    clog.app.info('xxffffffffffffffff f        fffffffffffffffffffff');
                    clog.app.info(body0.docs[0].outputs);
                    expect(body0.docs[0].outputs.length).toBe(0);
                    break;

                case 3:
                    expect(body0.docs.length).toBe(1);
                    expect(body0.docs[0].outputs.length).toBe(1);
                    expect(body0.docs[0].outputs[0][0]).toBe(iConsts.HU_DNA_SHARE_ADDRESS);
                    expect(body0.docs[0].outputs[0][1]).toBe(134217728);
                    break;

                default:
                    expect(body0.docs.length).toBe(1);
                    expect(body0.docs[0].outputs.length).toBe(2);
                    expect(body0.docs[0].outputs[0][0]).toBe(iConsts.HU_DNA_SHARE_ADDRESS);
                    expect(body0.docs[0].outputs[0][1]).toBe(utils.floor((134217728 / 368142) * 368130));
                    expect(body0.docs[0].outputs[1][1]).toBe(utils.floor((134217728 / 368142) * 12));
            }

            let body1 = JSON.parse(cbReceiverBlocks[1 + cycles].body);
            expect(body1.bType).toBe('coinbase');
            expect(cbReceiverBlocks[1 + cycles].ancestors).toBe(cbReceiverBlocks[0 + cycles].block_hash);
            expect(body1.ancestors).toBe(cbReceiverBlocks[1 + cycles].ancestors);
            expect(body1.docs.length).toBe(1);
            let trx1 = body1.docs[0]
            expect(trx1.dType).toBe('coinbase');
            expect(trx1.treasuryIncomes).toBe(0);
            expect(trx1.mintedCoins).toBe(134217728);
            leaves = leavesHandler.getLeaveBlocks();
            expect(utils.objKeys(leaves).length).toBe(1);
            leaveHash = utils.objKeys(leaves)[0]
            expect(leaveHash).toBe(body1.blockHash);
            let blocksToSend = model.sCustom({
                query: 'SELECT * FROM i_sending_q ORDER BY creation_date',
                values: []
            })
            expect(blocksToSend.length).toBe(1);
            blocksToSend = blocksToSend[0]
            expect(blocksToSend.sender).toBe(clonesDict[notCreatorApp].email);
            expect(blocksToSend.receiver).toBe(clonesDict[creatorApp].email);
            // fetch msg from q and write it on hard-drive(send email)
            sendQ.sendOutThePacket()







            // dummy increase share holders
            // later replace it with more realistic DNA shae increasing(e.g. proposal->voting->approval->share calculating)
            if (cycles == 1) {
                dummyAddNewShareholder(dummyShareTime)
            }

            // switch to creator which is now received a confirmed coinbase block
            db.setAppCloneId(creatorApp);
            // dummy increase share holders
            if (cycles == 1) {
                dummyAddNewShareholder(dummyShareTime)
            }
            packet = null;
            inx = 0;
            while (utils._nilEmptyFalse(packet) && (inx < 10)) {
                inx += 1
                clog.app.info(`> > > read from hard-drive on ${clonesDict[creatorApp].name} ${inx}`);
                packet = networker.iPullSync()
                utils.sleepBySecond(iConsts.getCycleByMinutes() * 7)
            }
            expect(packet.sender).toBe(clonesDict[notCreatorApp].email);
            unwrapRes = networkListener.unwrapPacketSync(packet)
            expect(unwrapRes.err != false).toBe(null);
            expect(unwrapRes.connection_type).toBe(iConsts.CONSTS.PUBLIC);
            expect(unwrapRes.shouldPurgeMessage).toBe(true);
            toParseMessage = unwrapRes.message
            expect(toParseMessage.bType).toBe('coinbase');
            expect(toParseMessage.ancestors).toBe(prevBlock.block_hash);
            blocksToParse = model.sCustom({
                query: 'SELECT * FROM i_parsing_q ORDER BY pq_last_modified',
                values: []
            })
            expect(blocksToParse.length).toBe(1);
            blocksToParse = blocksToParse[0]
            expect(blocksToParse.sender).toBe(clonesDict[notCreatorApp].email);
            //manual pull q
            toparse = null;
            inx = 0
            while (utils._nilEmptyFalse(toparse)) {
                inx += 1
                clog.app.info(`> > > fetchParseQSync on ${clonesDict[creatorApp].name} ${inx}`);
                toparse = parseQHandler.fetchParseQSync()
                utils.sleepBySecond(iConsts.getCycleByMinutes() * 7)
            }
            expect(toparse.length).toBe(1);
            toparse = toparse[0]
            // clog.app.info(JSON.parse(toparse.payload));
            // smart pull q to parsing
            sPQRes = parseQHandler.qPicker.smartPullQSync()
            clog.app.info(sPQRes);
            // controlling i_blocks
            cbReceiverBlocks = model.sCustom({
                query: 'SELECT * FROM i_blocks ORDER BY creation_date',
                values: []
            })
            expect(parseInt(cbReceiverBlocks.length)).toBe(2 + cycles);
            expect(cbReceiverBlocks[1].ancestors).toBe(cbReceiverBlocks[0].block_hash);
            body0 = JSON.parse(cbReceiverBlocks[0 + cycles].body);
            if (body0.bType == 'Genesis') {
                expect(body0.ancestors).toBe('');
                expect(body0.descriptions).toBe('Imagine all the people sharing all the world');
                expect(body0.docs.length).toBe(1);
                doc0 = body0.docs[0]
                expect(doc0.dType).toBe("DNAProposal");
                expect(doc0.helpHours).toBe(61355);
                expect(doc0.helpLevel).toBe(6);
                expect(doc0.shares).toBe(368130);
                expect(doc0.votesY).toBe(368130);
                expect(doc0.votesN).toBe(0);
            } else {

            }
            body1 = JSON.parse(cbReceiverBlocks[1 + cycles].body);
            expect(body1.bType).toBe('coinbase');
            expect(cbReceiverBlocks[1 + cycles].ancestors).toBe(cbReceiverBlocks[0 + cycles].block_hash);
            expect(body1.ancestors).toBe(cbReceiverBlocks[1 + cycles].ancestors);
            trx1 = body1.docs[0]
            expect(trx1.dType).toBe('coinbase');
            expect(trx1.treasuryIncomes).toBe(0);
            expect(trx1.mintedCoins).toBe(134217728);
            leaves = leavesHandler.getLeaveBlocks();
            expect(utils.objKeys(leaves).length).toBe(1);
            leaveHash = utils.objKeys(leaves)[0]
            expect(leaveHash).toBe(body1.blockHash);
            blocksToSend = model.sCustom({
                query: 'SELECT * FROM i_sending_q ORDER BY creation_date',
                values: []
            })
            expect(blocksToSend.length).toBe(0); // will not send coinbase to neighbor. because the sender,reciver,hash combination is already sent
            // blocksToSend = blocksToSend[0]
            let key = [clonesDict[creatorApp].email, clonesDict[notCreatorApp].email, body1.blockHash].join();
            expect(bdLogger.isBroadcasted(key)).toBe(true); // the sender,reciver,hash combination is already sent

            prevBlock = cbReceiverBlocks[1 + cycles]


        }





        return




        let masterBlocks = model.sCustom({
            query: 'SELECT * FROM i_blocks ORDER BY creation_date',
            values: []
        })
        expect(parseInt(masterBlocks.length)).toBe(2);
        expect(masterBlocks[1].ancestors).toBe(masterBlocks[0].block_hash);
        // let body0 = JSON.parse(masterBlocks[0].body);
        // // expect(body0.bType).toBe('Genesis');
        // // expect(body0.ancestors).toBe('');
        // // expect(body0.descriptions).toBe('Imagine all the people sharing all the world');
        // // expect(body0.docs.length).toBe(1);
        // // let documents0 = body0.docs[0]
        // // expect(documents0.dType).toBe("DNAProposal");
        // // expect(documents0.helpHours).toBe(61355);
        // // expect(documents0.helpLevel).toBe(6);
        // // expect(documents0.shares).toBe(368130);
        // // expect(documents0.votesY).toBe(368130);
        // // expect(documents0.votesN).toBe(0);
        // // let body1 = JSON.parse(masterBlocks[1].body);
        // // expect(body1.bType).toBe('coinbase');
        // // expect(body1.ancestors).toBe(masterBlocks[1].ancestors);
        // // let transaction = body1.docs[0]
        // // expect(transaction.bType).toBe('coinbase');
        // // expect(transaction.treasuryIncomes).toBe(0);
        // // expect(transaction.mintedCoins).toBe(134217728);
        // // let leaves = leavesHandler.getLeaveBlocks();
        // // // clog.app.info('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% do Create CoinbaseBlock');
        // // // clog.app.info(leaves);
        // // expect(utils.objKeys(leaves).length).toBe(1);
        // // let leaveHash = utils.objKeys(leaves)[0]
        // // expect(leaveHash).toBe(masterBlocks[1].block_hash);


        // // // neighbor node read file from hard drive
        // // db.setAppCloneId(1);
        // // let slaveBlocks = model.sCustom({
        // //     query: 'SELECT * FROM i_blocks',
        // //     values: []
        // // })
        // // expect(slaveBlocks.length).toBe(1);
        // // slaveBlocks = slaveBlocks[0]
        // // expect(slaveBlocks.block_hash).toBe(masterBlocks[0].block_hash);


        //master node

        // db.setAppCloneId(0);
        // coinbaseCreated = false;
        // inx = 0
        // while (coinbaseCreated == false) {
        //     inx += 1
        //     clog.app.info(`> > > create CoinbaseBlock on master ${inx}`);
        //     res = await coinbaseCreator.createCoinbaseBlock();
        //     utils.sleepBySecond(iConsts.getCycleByMinutes() / 10)
        //     coinbaseCreated = res.coinbaseCreated

        // }
        // clog.app.info(res);

        // blockCount = model.sCustom({
        //     query: 'SELECT COUNT(*) FROM i_blocks',
        //     values: []
        // })
        // expect(parseInt(blockCount[0].count)).toBe(3);


    });

});


function dummyAddNewShareholder(t) {

    let dnaProposalDoc = DNAHandler.getDNATmpShareDoc(); // add initialise DNA to genesis block
    dnaProposalDoc.title = '2 hours documenting project'
    dnaProposalDoc.descriptions = 'my humble contribute for society'
    dnaProposalDoc.tags = 'document, security, logging'
    dnaProposalDoc.helpHours = 2
    dnaProposalDoc.helpLevel = 6
    dnaProposalDoc.shares = dnaProposalDoc.helpHours * dnaProposalDoc.helpLevel;
    dnaProposalDoc.votesY = 10000;
    dnaProposalDoc.votesN = 4;
    dnaProposalDoc.votesA = 3;
    dnaProposalDoc.creationDate = t;
    dnaProposalDoc.approvalDate = t;
    dnaProposalDoc.shareholder = 'im12222222zxqurzcfevsuxgwtpvv6xvenpvv6njdryxuerqdtpx9sn2222222';
    dnaProposalDoc.hash = DNAHandler.hashDNARegDoc(dnaProposalDoc);
    // insert in db DNA shares
    // clog.app.info(dnaProposalDoc);
    DNAHandler.insertAShare(dnaProposalDoc);
}
