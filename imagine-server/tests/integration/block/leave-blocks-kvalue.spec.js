const utils = require("../../../utils/utils");
const leaveLib = require("../../../dag/leaves-handler");
let server;

let date1 = '2018-01-11 18:12:18';
let date2 = '2018-01-12 18:12:18';
let date2_2 = '2018-01-12 19:12:18';
let date3 = '2018-02-12 19:12:18';
let date4_1 = '2018-04-01 19:12:18';
let date4_2 = '2018-04-02 19:12:18';
let date7 = '2018-07-02 19:12:18';

describe('Init alterdb test', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(() => { server.close(); });



    it('Should initiate leave blocks', () => {
        leaveLib.resetLeaveBlocks();
        leaveLib.addToLeaveBlocks('hash1', date1, 'testType');
        let current = leaveLib.getLeaveBlocks();
        expect(utils.objLength(current)).toBe(1);
        expect(current['hash1'].date).toBe(date1);

    });

    it('Should initiate leave blocks', () => {
        leaveLib.resetLeaveBlocks();
        leaveLib.addToLeaveBlocks('hash2', date2, 'testType');
        leaveLib.addToLeaveBlocks('hash1', date1, 'testType');
        leaveLib.addToLeaveBlocks('hash2', date2_2, 'testType');
        let current = leaveLib.getLeaveBlocks();
        expect(utils.objLength(current)).toBe(2);
        expect(current['hash1'].date).toBe(date1);
        expect(current['hash2'].date).toBe(date2_2);

    });

    it('Should add/remove leave blocks', () => {
        leaveLib.resetLeaveBlocks();
        leaveLib.addToLeaveBlocks('hash2', date2, 'testType');
        leaveLib.addToLeaveBlocks('hash1', date1, 'testType');
        leaveLib.addToLeaveBlocks('hash7', date7, 'testTypeZ');
        leaveLib.addToLeaveBlocks('hash4.1', date4_1, 'testType');
        leaveLib.addToLeaveBlocks('hash3', date3, 'testType');
        leaveLib.addToLeaveBlocks('hash4.1', date4_2, 'testType');
        let current = leaveLib.getLeaveBlocks();
        expect(utils.objLength(current)).toBe(5);

        leaveLib.removeFromLeaveBlocks(['hash3']);
        current = leaveLib.getLeaveBlocks();
        expect(utils.objLength(current)).toBe(4);

        leaveLib.removeFromLeaveBlocks(['hash4.1']);
        current = leaveLib.getLeaveBlocks();
        expect(utils.objLength(current)).toBe(3);

        expect(current['hash1'].date).toBe(date1);
        expect(current['hash2'].date).toBe(date2);

    });

});