const parsingQHandler = require('../../../services/parsing-q-manager/parsing-q-handler');
const blockTpl = require('../../../docs-structure/block/block-tpl-handler');
const model = require('../../../models/interface');
const clog = require('../../../loggers/console_logger');


let server;

jest.setTimeout(7000);

describe('Init db_test.i_parsing_q ', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(() => { server.close(); });

    it('Should initiate some blocks to parse', () => {

        try {
            parsingQHandler.rmoveFromParsingQ();

            let block;
            let dag = [
                { blockHash: 'b1', prerequisites: [] },
                { blockHash: 'b2', prerequisites: ['b1'] },
                { blockHash: 'b3', prerequisites: ['b1'] },
                { blockHash: 'b4', prerequisites: ['b3'] },
                { blockHash: 'b5', prerequisites: ['b4'] },
                { blockHash: 'b6', prerequisites: ['b5', 'b2'] },
                { blockHash: 'b7', prerequisites: ['b6', 'b1'] },
                { blockHash: 'b8', prerequisites: ['b1', 'b3', 'b2', 'b6'] },
            ];
            for (const blk of dag) {
                block = blockTpl.getCoinbaseBlockTemplate();
                block.bType = 'test';
                block.blockHash = blk.blockHash;
                pushRes = parsingQHandler.inOutHandler.pushToParsingQSync({
                    type: 'testPacketT',
                    code: 'testPacketC',
                    sender: 'test_sender@test.com',
                    prerequisites: blk.prerequisites.join().toString("utf8"),
                    message: block,
                    connection_type: iConsts.CONSTS.PUBLIC
                });
            }

            let resDict = {};
            parsingQHandler.qUtils.removePrerequisitesSync('b2');
            res = model.sRead({ table: 'i_parsing_q', fields: ["block_hash", "prerequisites"] });
            resDict = {};
            for (const blk of res) {
                resDict[blk.block_hash] = blk.prerequisites;
            }
            expect(resDict['b6']).toBe('b5');
            expect(resDict['b8']).toBe('b1,b3,b6');

            parsingQHandler.qUtils.removePrerequisitesSync('b1');
            res = model.sRead({ table: 'i_parsing_q', fields: ["block_hash", "prerequisites"] });
            resDict = {};
            for (const blk of res) {
                resDict[blk.block_hash] = blk.prerequisites;
            }
            expect(resDict['b2']).toBe('');
            expect(resDict['b3']).toBe('');
            expect(resDict['b7']).toBe('b6');
            expect(resDict['b8']).toBe('b3,b6');
        } catch (e) {
            clog.app.info(new Error(e));
        }
    });

});
