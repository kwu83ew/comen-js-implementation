const db = require('../../startup/db2')
    // const db2 = require('../../startup/db2')
const getMoment = require("../../startup/singleton").instance.getMoment
const utils = require("../../utils/utils");
let initializeMachine = require("../../startup/boot-server").initializeMachine;
const fileHandler = require('../../hard-copy/file-handler')
const settings = require("../../hard-copy/settings");

let alterIsBlocked = false;



function setServerAndAlterServer() {

    db.setAppCloneId(0);
    db.sEmptyDB();

    db.setAppCloneId(1);
    db.sEmptyDB();

    db.setAppCloneId(0);
    initializeMachine(0);
    db.setAppCloneId(2);
    initializeMachine(2);
    db.setAppCloneId(3);
    initializeMachine(3);
    db.setAppCloneId(4);
    initializeMachine(4);

    db.setAppCloneId(1);
    if (alterIsBlocked == false) {
        alterIsBlocked = true;
        initializeMachine(1);
        alterIsBlocked = false;
    }

}

function cleanFolderAndAlterFoler() {
    fileHandler.eFRemoveSync(settings.inbox)
    fileHandler.eFRemoveSync(settings.outbox)
    fileHandler.eFRemoveSync(settings.inbox + '1')
    fileHandler.eFRemoveSync(settings.outbox + '1')
}

module.exports.setServerAndAlterServer = setServerAndAlterServer
module.exports.cleanFolderAndAlterFoler = cleanFolderAndAlterFoler