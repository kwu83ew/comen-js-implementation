let server;

describe('Init alterdb test', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(() => { server.close(); });

    it('Should initialize alterdb test', async() => {
        require("./init").initAlterServerTEST();
    })
});