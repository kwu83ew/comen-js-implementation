const db = require('../../startup/db2')
const fileHandler = require('../../hard-copy/file-handler')

async function emptyHardCopy() {
    await fileHandler.iDelete()
}


let alterIsBlocked = false;

function initAlterServerTEST() {
    // if (alterIsBlocked == true)
    //     return;
    // alterIsBlocked = true;
    // db.togglePool(db.asyncAlterDB)
    // db.sEmptyDB();
    // let bootServer = require("../../startup/boot-server").bootServer;
    // bootServer();
    // db.togglePool(db.asyncDB)
    // alterIsBlocked = false;
}


module.exports.initAlterServerTEST = initAlterServerTEST
module.exports.emptyHardCopy = emptyHardCopy