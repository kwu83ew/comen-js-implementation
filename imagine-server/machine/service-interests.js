const _ = require('lodash');
const iConsts = require('../config/constants');
const clog = require('../loggers/console_logger');
const utils = require("../utils/utils");
const iutils = require("../utils/iutils");


class ServiceInterests {

    static getMachineServiceInterests(args) {
        let dType = args.dType;
        let dClass = args.dClass;

        // TODO: these values must be costumizable for backers
        let servicePrices = {}
        servicePrices[iConsts.DOC_TYPES.BasicTx] = 1.000;    // the output must be 1 or greater. otherwise other nodes reject the block

        if (_.has(servicePrices, dType))
            return servicePrices[dType];

        // node doesn't support this type of documents so accept it as a base feePerByte
        return 1;
    }

}

module.exports = ServiceInterests;
