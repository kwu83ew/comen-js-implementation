const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const model = require('../models/interface');
const crypto = require('../crypto/crypto');
const addressHandler = require('../address/address-handler');
const clog = require('../loggers/console_logger');
const db = require('../startup/db2')
const fileHandler = require('../hard-copy/file-handler');


class MaWaHandler {

    static restoreMaWa(args = {}) {
        // console.log(`restoreMaWa args`, args);
        let content = args.content;
        content = utils.parse(content);
        // console.log(`restoreMaWa content`, content);

        // 1. set profiles
        let mapmpCodeToNewMPCode = {};
        let profiles = content['profiles'];
        for (let aProf of profiles) {
            let mp_code = aProf.mp_code;

            // control if profile exist in db
            let existProf = this._super.getProfile(mp_code);
            console.log('existProf', existProf);
            if (existProf.pCount != 0) {
                let postFix = utils.hash6c(crypto.keccak256(utils.getNowSSS()));
                mapmpCodeToNewMPCode[aProf.mp_code] = [aProf.mp_code, postFix].join('-');
            } else {
                mapmpCodeToNewMPCode[aProf.mp_code] = aProf.mp_code;
            }
            let prfUpserMsg = `The profile (${aProf.mp_name}) (${aProf.mp_code} -> ${mapmpCodeToNewMPCode[aProf.mp_code]}) Upserted`
            console.log(prfUpserMsg);
            clog.app.info(prfUpserMsg);
            this._super.upsertProfile({
                mpCode: mapmpCodeToNewMPCode[aProf.mp_code],
                mpName: aProf.mp_name,
                mpProfie: aProf.mp_settings
            });
        }

        // 2. import neighbors
        let neighbors = content['neighbors'];
        for (let aNei of neighbors) {
            let existNei = this._super.neighborHandler.getNeighborsSync({
                query: [
                    ['n_mp_code', mapmpCodeToNewMPCode[aNei.n_mp_code]],
                    ['n_connection_type', aNei.n_connection_type],
                    ['n_email', aNei.n_email],
                ]
            });
            let emailImportMsg = `The neighbor ${aNei.n_email} (${aNei.n_connection_type}-${mapmpCodeToNewMPCode[aNei.n_mp_code]})`;
            if (existNei.length == 0) {
                // insert new neighbor
                this._super.neighborHandler.addANeighborSync({
                    mpCode: mapmpCodeToNewMPCode[aNei.n_mp_code],
                    nEmail: aNei.n_email,
                    nConnectionType: aNei.n_connection_type,
                    nPGPPubKey: aNei.n_pgp_public_key,
                    nIsActive: aNei.n_is_active,
                    nInfo: aNei.n_info,
                    nCreationDate: aNei.n_creation_date
                });
                emailImportMsg += ' inserted'
                console.log(emailImportMsg);
                clog.app.info(emailImportMsg);

            } else {
                // update existed neighbor
                this._super.neighborHandler.updateNeighbor({
                    query: [
                        ['n_mp_code', mapmpCodeToNewMPCode[aNei.n_mp_code]],
                        ['n_connection_type', aNei.n_connection_type],
                        ['n_email', aNei.n_email],
                    ],
                    updates: {
                        n_mp_code: mapmpCodeToNewMPCode[aNei.n_mp_code],
                        n_email: aNei.n_email,
                        n_connection_type: aNei.n_connection_type,
                        n_pgp_public_key: aNei.n_pgp_public_key,
                        n_is_active: aNei.n_is_active,
                        n_info: utils.stringify(aNei.n_info),
                        n_creation_date: aNei.n_creation_date,
                        n_last_modified: utils.getNow()
                    }
                });
                emailImportMsg += ' updated'
                console.log(emailImportMsg);
                clog.app.info(emailImportMsg);

            }

        }

        // 3. import wallet addresses
        const walletAddressHandler = require('../web-interface/wallet/wallet-address-handler');
        let addresses = content['addresses'];
        // console.log(`addresses: ${utils.stringify(addresses[0])}`);
        for (let anAddress of addresses) {
            let theMPCode = mapmpCodeToNewMPCode[anAddress.wa_mp_code];
            walletAddressHandler.upsertAnAddress({
                wampCode: theMPCode,
                waAddress: anAddress.wa_address,
                waTitle: anAddress.wa_title,
                waDetail: anAddress.wa_detail,
                waCreationDate: anAddress.wa_creation_date,
            });
        }

        // 4. import iName binded properties 
        let bindedInfo = content['bindedInfo'];
        const flensHandler = require('../contracts/flens-contract/flens-handler');
        for (let aBindedEntity of bindedInfo) {
            let theMPCode = mapmpCodeToNewMPCode[aBindedEntity.mcb_mp_code];
            flensHandler.binder.createABinding({
                mpCode: theMPCode,
                mcb_in_hash: aBindedEntity.mcb_in_hash,
                mcb_bind_type: aBindedEntity.mcb_bind_type,
                mcb_conf_info: aBindedEntity.mcb_conf_info,
                mcb_label: aBindedEntity.mcb_label,
                mcb_comment: aBindedEntity.mcb_comment,
                mcb_creation_date: aBindedEntity.mcb_creation_date
            });
        }



        // 5. extract onchain contracts, based on wallet addresses.  TODO: implement it


    }

    static controllMaWa(args = {}) {
        // controll if machine addressed are already in wallet or not

        //the control whole addresses in wallet and the signatures if are valid?

    }

    static backupMaWa() {
        let bkData = {
            bacupVersion: '0.0.0'
        };



        /**
         * retrieve profiles
         */
        let profiles = model.sRead({
            table: 'i_machine_profiles',
            order: [
                ['mp_code', 'ASC'],
                ['mp_name', 'ASC'],
            ]
        });
        bkData['profiles'] = profiles;



        /**
         * retrieve wallet addresses
         */
        let addresses = model.sRead({
            table: 'i_machine_wallet_addresses',
            fields: ['wa_mp_code', 'wa_address', 'wa_title', 'wa_detail', 'wa_creation_date'],
            order: [
                ['wa_mp_code', 'ASC'],
                ['wa_creation_date', 'ASC'],
                ['wa_address', 'ASC'],
            ]
        });
        bkData['addresses'] = addresses;



        /**
         * retrieve neighbors
         */
        let neighbors = model.sRead({
            table: 'i_machine_neighbors',
            order: [
                ['n_mp_code', 'ASC'],
                ['n_email', 'ASC'],
            ]
        });
        bkData['neighbors'] = neighbors;
        bkData['ExportMessage'] = `Was exported ${neighbors.length} neighbors, ${addresses.length} addresses in ${profiles.length} profiles`;
        bkData['ExportVerifyCode'] = utils.hash16c(crypto.keccak256(utils.stringify({
            neighbors: neighbors.length,
            addresses: addresses.length,
            profiles: profiles.length
        })));




        /**
         * retrieve binded properties to iName
         * e.g. iPGP (private keys)
         */
        let bindedInfo = model.sRead({
            table: 'i_machine_iname_bindings',
            order: [
                ['mcb_mp_code', 'ASC'],
                ['mcb_in_hash', 'ASC'],
                ['mcb_bind_type', 'ASC'],
                ['mcb_label', 'ASC']
            ]
        });
        bkData['bindedInfo'] = bindedInfo;



        // TODO: ExportVerifyCode must be more complete, in order to verify & guaranty correct importing data 

        console.log(`bkData: ${utils.stringify(bkData)}`);

        let dirName = iConsts.HD_PATHES.hd_backup_dag;
        let appCloneId = db.getAppCloneId();
        // if (appCloneId > 0) {
        //     dirName = dirName + appCloneId.toString()
        // }
        let fileName = `${utils.getNow()}_machine_backup.txt`;
        let res = fileHandler.packetFileCreateSync({
            dirName,
            fileName,
            fileContent: utils.stringify(bkData)
        });

        return { err: false, msg: `Backup created on "${dirName}/${fileName}"` }
    }

}

module.exports = MaWaHandler;
