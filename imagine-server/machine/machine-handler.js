const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const model = require('../models/interface');
const crypto = require('../crypto/crypto');
const addressHandler = require('../address/address-handler');
const clog = require('../loggers/console_logger');
const kvHandler = require('../models/kvalue');

const table = 'i_machine_profiles';

/**
 * the machine can have 1 or more diffrent profile(s)
 * each profile has it's own public/private email & public/private iPGP key pairs & it's neightbors set & 
 * and it's wallet addresses and
 * machine_onchain_contracts
 * and maybe kvalue 
 * 
 * adding profile field to all tables
 * machine_tmp_documents, machine_buffer_documents
 * machine_neighbors, machine_wallet_addresses, machine_wallet_funds
 * machine_draft_proposals, machine_used_utxos, machine_ballots, machine_draft_pledges 
 * 
 */
const mpSettingsTpl = {

    // the status be booting, synching, ready
    // booting: when nodes starts to connecting to network for first time
    // synching: when latest confirmed blocks are created before 12 hours ago
    // ready: the node has some confirmed blocks created in last 12 hour in his locl DB
    // status: constants.NODE_IS_BOOTING,
    // lastConfirmedBlockDate: IMAGINE_LAUNCH_DATE,

    // machine email setting
    // each node has 2 email addreess, public & private to resist against the spamming...
    // TODO: maybe machine have to have ability to have more than one email to comunicate to prevent against any censorship
    pubEmail: {
        address: "abc@def.gh",
        pwd: "12345678",
        incomeIMAP: "993",
        incomePOP3: "995",
        incomingMailServer: "",
        outgoingMailServer: "",
        outgoingSMTP: "465",
        fetchingIntervalByMinute: "5", // it depends on smtp server, but less than 5 minute is useless
        PGPPrvKey: "",
        PGPPubKey: ""
    },

    prvEmail: {
        address: "private_email@def.gh",
        pwd: "12345678",
        incomeIMAP: "993",
        incomePOP3: "995",
        incomingMailServer: "",
        outgoingMailServer: "",
        outgoingSMTP: "465",
        fetchingIntervalByMinute: "5", // it depends on smtp server, but less than 5 minute is useless
        PGPPrvKey: "",
        PGPPubKey: ""
    },

    machineAlias: "imagineNode",
    backerAddress: null,
    backerTitle: null,
    backerDetail: null,
};

class MachineHandler {

    static initDefaultProfile() {
        let dbl = model.sRead({
            table,
            query: [['mp_code', iConsts.CONSTS.DEFAULT]]
        });
        if (dbl.length == 1)
            return { err: false, msg: 'The Default profile Already initialized' };

        let defSetting = utils.stringify({});
        model.sCreate({
            table,
            values: {
                mp_code: iConsts.CONSTS.DEFAULT,
                mp_name: iConsts.CONSTS.DEFAULT,
                mp_settings: defSetting,
                mp_last_modified: utils.getNow(),
            }
        });
        return { err: false, msg: 'The Default profile initialized' };
    }

    static setActiveMP(args) {
        let selectedMPCode = _.has(args, 'selectedMPCode') ? args.selectedMPCode : '';
        let res = model.sUpdate({
            table: 'i_kvalue',
            query: [['kv_key', 'selected_profile']],
            updates: {
                kv_value: selectedMPCode,
                kv_last_modified: utils.getNow()
            }
        });
        return { err: false, msg: `Profile (${selectedMPCode}) activated` }
    }

    static addNewMProfile(args) {
        console.log(`\nadd NewMProfile args: ${utils.stringify(args)}`);

        let newProfileCode = args.newProfileCode;
        let newProfileName = args.newProfileName;

        if (utils._nilEmptyFalse(newProfileCode) || utils._nilEmptyFalse(newProfileName))
            return { err: true, msg: 'missed profile code or name' };


        let dbl = model.sRead({
            table,
            query: [['mp_name', newProfileCode]]
        });
        if (dbl.length == 1)
            return { err: false, msg: `The profile code(${newProfileCode}) already used, chose another one` };

        let mpSettings = _.clone(mpSettingsTpl);
        // prepare machine pair keys
        mpSettings.termOfServices = iConsts.CONSTS.NO;
        mpSettings.language = 'eng';
        let address = addressHandler.createANewAddress({
            signatureType: iConsts.SIGNATURE_TYPES.Strict
        });
        mpSettings.backerTitle = address.title
        mpSettings.backerDetail = address.detail
        mpSettings.backerAddress = address.address
        mpSettings.machineAlias = `node-${utils.hash6c(crypto.keccak256(address.address))}`;

        // adding backer address to wallet too
        // move it to address handler 
        const walletAddressHandler = require('../web-interface/wallet/wallet-address-handler');
        walletAddressHandler.addAnAddressToWallet({
            mpCode: newProfileCode,
            waAddress: address.address,
            waTitle: address.title,
            waCreationDate: utils.getNow(),
            waDetail: utils.stringify(address.detail)
        })

        // initializing email PGP pair keys (it uses native node.js crypto lib. TODO: probably depricated and must refactor to use new one)
        let PGPpairKey = crypto.nativeGenerateKeyPairSync();
        mpSettings.pubEmail.PGPPrvKey = PGPpairKey.prvKey
        mpSettings.pubEmail.PGPPubKey = PGPpairKey.pubKey
        PGPpairKey = crypto.nativeGenerateKeyPairSync();
        mpSettings.prvEmail.PGPPrvKey = PGPpairKey.prvKey
        mpSettings.prvEmail.PGPPubKey = PGPpairKey.pubKey
        mpSettings = utils.stringify(mpSettings);
        model.sCreate({
            table,
            values: {
                mp_code: newProfileCode,
                mp_name: newProfileName,
                mp_settings: mpSettings,
                mp_last_modified: utils.getNow(),
            }
        });
        return { err: false, msg: `New profile ${newProfileName}(${newProfileCode}) creatd` };
    }

    static getProfile(mpCode = null) {
        if (mpCode == null)
            mpCode = iutils.getSelectedMProfile();
        let mProfie = model.sRead({
            table,
            query: [['mp_code', mpCode]]
        });
        if (mProfie.length == 0)
            return { err: true, msg: `machine has not profile(${mpCode})`, pCount: 0 };

        mProfie = this.convertFields(mProfie[0]);
        mProfie.mpSettings = utils.parse(mProfie.mpSettings);
        return { err: false, profie: mProfie, pCount: mProfie.length };
    }

    static deleteMProfile(args) {
        console.log(`deleteMProfile args`, args);
        let mpCode = args.mpCode;

        // delete neighbors
        this.neighborHandler.dropNeighborsBecauseOfRemoveProfile(mpCode);

        // delete wallet addresses
        const walletAddressHandler = require('../web-interface/wallet/wallet-address-handler');
        walletAddressHandler.deleteAddressesOffAProfile(mpCode);

        // delete onchain contracts from table "machine_onchain_contracts"  TODO: implement it ASAP

        // delete profile
        model.sDelete({
            table,
            query: [[
                'mp_code', mpCode
            ]]
        })
        return { err: false, msg: `The profile(${mpCode} removed` }
    }

    static upsertProfile(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let mpName = _.has(args, 'mpName') ? args.mpName : mpCode;
        let mpProfie = _.has(args, 'mpProfie') ? args.mpProfie : {};
        model.sUpsert({
            table,
            idField: 'mp_code',
            idValue: mpCode,
            updates: {
                mp_name: mpName,
                mp_settings: mpProfie,
                mp_last_modified: utils.getNow()
            }
        });
        return { err: false, msg: `Profile settings was updated` }
    }


    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'mp_code'))
            out.mpCode = elm.mp_code;
        if (_.has(elm, 'mp_name'))
            out.mpName = elm.mp_name;
        if (_.has(elm, 'mp_settings'))
            out.mpSettings = elm.mp_settings;
        if (_.has(elm, 'mp_last_modified'))
            out.mpLastModified = elm.mp_last_modified;
        return out;
    }

    static getBackerAddress() {
        return this.getMProfileSettingsSync().backerAddress;
    }


    static getMachineUnlockStruct(unlockIndex = 0) {
        let machineSettings = this.getMProfileSettingsSync();
        let uSet = machineSettings.backerDetail.uSets[unlockIndex];
        return uSet;
    }

    static signByMachineKey(args) {
        let signMsg = args.signMsg;
        let unlockIndex = _.has(args, 'unlockIndex') ? args.unlockIndex : 0;

        let machineSettings = this.getMProfileSettingsSync();
        // clog.app.info(`machineSettings in signByMachineKey: ${utils.stringify(machineSettings)}`);
        let uSet = machineSettings.backerDetail.uSets[unlockIndex];
        let signatures = [];
        for (let setIndex = 0; setIndex < uSet.sSets.length; setIndex++) {
            // let aSignSet = uSet.sSets[setIndex];
            // signing block by backer private key (
            // TODO: using delegated private key for the sake of security to not keeping private key of real-share-holder in online machine!)
            let prvKey = machineSettings.backerDetail.privContainer[uSet.salt][setIndex];
            let signature = crypto.signMsg(signMsg, prvKey);
            let verifyRes = crypto.verifySignature(signMsg, signature, uSet.sSets[setIndex].sKey);
            if (verifyRes != true) {
                let msg = `sign By Machine Key creates invalid signature! ${utils.stringify(args)}`;
                return { err: true, msg }
            }
            signatures.push(signature);
        }
        return {
            backer: machineSettings.backerAddress,
            uSet,
            signatures
        };
    }

    static initMachine() {

        let r = this.initDefaultProfile();
        if (r.err != false)
            return r;

        let mProfie = model.sRead({
            table,
            query: [['mp_code', iConsts.CONSTS.DEFAULT]]
        });
        if (mProfie.length == 0)
            return { err: true, msg: 'machine default profile didnot set!' };

        mProfie = mProfie[0];

        let mpSettings = utils.parse(mProfie.mp_settings);
        let machineAlias = _.has(mpSettings, 'machineAlias') ? mpSettings.machineAlias : null;
        if (machineAlias != null)
            return { err: false, msg: 'machine Alias & ... already configurated' }

        // initialize mpSettings
        mpSettings = _.clone(mpSettingsTpl);

        mpSettings.termOfServices = iConsts.CONSTS.NO;
        mpSettings.language = 'eng';
        let address = addressHandler.createANewAddress({
            signatureType: iConsts.SIGNATURE_TYPES.Strict
        });
        mpSettings.backerTitle = address.title
        mpSettings.backerDetail = address.detail
        mpSettings.backerAddress = address.address
        mpSettings.machineAlias = `node-${utils.hash6c(crypto.keccak256(address.address))}`;

        // adding backer address to wallet too
        let existAdd = model.sRead({
            table: 'i_machine_wallet_addresses',
            query: [
                ['wa_mp_code', iConsts.CONSTS.DEFAULT],
                ['wa_address', address.address],
            ]
        });
        if (existAdd.length == 0) {
            model.sCreate({
                table: 'i_machine_wallet_addresses',
                values: {
                    wa_mp_code: iConsts.CONSTS.DEFAULT,
                    wa_address: address.address,
                    wa_title: address.title,
                    wa_creation_date: utils.getNow(),
                    wa_detail: utils.stringify(address.detail)
                }
            });

        } else {
            model.sUpdate({
                table: 'i_machine_wallet_addresses',
                query: [
                    ['wa_mp_code', iConsts.CONSTS.DEFAULT],
                    ['wa_address', address.address],
                ],
                updates: {
                    wa_title: address.title,
                    wa_creation_date: utils.getNow(),
                    wa_detail: utils.stringify(address.detail)
                }
            });
        }

        // initializing email PGP pair keys (it uses native node.js crypto lib. TODO: probably depricated and must refactor to use new one)
        let PGPpairKey = crypto.nativeGenerateKeyPairSync();
        mpSettings.pubEmail.PGPPrvKey = PGPpairKey.prvKey
        mpSettings.pubEmail.PGPPubKey = PGPpairKey.pubKey
        PGPpairKey = crypto.nativeGenerateKeyPairSync();
        mpSettings.prvEmail.PGPPrvKey = PGPpairKey.prvKey
        mpSettings.prvEmail.PGPPubKey = PGPpairKey.pubKey
        mpSettings = utils.stringify(mpSettings);
        model.sUpdate({
            table,
            query: [
                ['mp_code', iConsts.CONSTS.DEFAULT]
            ],
            updates: { mp_settings: mpSettings }

            // values: {
            //     kv_key: 'machine_settings',
            //     kv_value: settings,
            //     kv_last_modified: utils.getNow()
            // }
        });

        // set selected profile=default
        model.sUpsert({
            table: 'i_kvalue',
            idField: 'kv_key',
            idValue: 'selected_profile',
            updates: { kv_value: iConsts.CONSTS.DEFAULT, kv_last_modified: utils.getNow() }
        });

        return { err: false }
    }


    static async searchMProfilesAsync(args = {}) {
        args.table = table;
        let res = await model.aRead(args);
        let newRes = [];
        for (let aRes of res) {
            let tmpRes = this.convertFields(aRes);
            tmpRes.mpSettings = utils.parse(tmpRes.mpSettings);
            newRes.push(tmpRes);
        }
        return newRes;
    }


    static async getMProfileSettingsAsyncSecure(args = {}) {
        // let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        // drop some private parts in order to security and xss attack
        let res = await this.getMProfileSettingsAsync(args);
        let safeRes = {
            pubEmail: {
                address: res.pubEmail.address,
                PGPPubKey: res.pubEmail.PGPPubKey,
            },
            prvEmail: {
                address: res.pubEmail.address,
                PGPPubKey: res.pubEmail.PGPPubKey,
            },
            backerAddress: res.backerAddress,
        }
        return safeRes;
    }

    static async getMProfileSettingsAsync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        let res = await model.aRead({
            table,
            query: [
                ['mp_code', mpCode]
            ]
        });
        if (res.length == 1) {
            let mprofile = utils.parse(res[0].mp_settings);
            mprofile.err = false;
            return mprofile;
        }

        return { Err: true };
    }

    static getMProfileSettingsSync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        let res = model.sRead({
            table,
            query: [
                ['mp_code', mpCode]
            ]
        });
        if (res.length != 1) {
            let msg = `There is not machine profile(${mpCode})!`;
            clog.app.error(msg);
            return { err: true, msg };
        }

        let mprofile = utils.parse(res[0].mp_settings);
        mprofile.err = false;
        return mprofile;

    }

    static updateMachineEmailSettingsSync(args) {
        let params = args.params;
        let emailType = args.emailType;
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        // read current settings
        let mProfile = model.sRead({
            table,
            query: [
                ['mp_code', mpCode]
            ]
        });
        let machineSettings = utils.parse(mProfile[0].mp_settings);
        // modify settings
        if (emailType == iConsts.CONSTS.PRIVATE) {
            machineSettings.prvEmail.address = params.address;
            machineSettings.prvEmail.pwd = params.pwd;
            machineSettings.prvEmail.outgoingSMTP = params.outgoingSMTP;
            machineSettings.prvEmail.incomeIMAP = params.incomeIMAP;
            machineSettings.prvEmail.incomePOP3 = params.incomePOP3;
            machineSettings.prvEmail.incomingMailServer = params.incomingMailServer;
            machineSettings.prvEmail.outgoingMailServer = params.outgoingMailServer;

        } else {
            machineSettings.pubEmail.address = params.address;
            machineSettings.pubEmail.pwd = params.pwd;
            machineSettings.pubEmail.outgoingSMTP = params.outgoingSMTP;
            machineSettings.pubEmail.incomeIMAP = params.incomeIMAP;
            machineSettings.pubEmail.incomePOP3 = params.incomePOP3;
            machineSettings.pubEmail.incomingMailServer = params.incomingMailServer;
            machineSettings.pubEmail.outgoingMailServer = params.outgoingMailServer;
        }
        // real update
        return model.sUpdate({
            table,
            query: [
                ['mp_code', mpCode]
            ],
            updates: { mp_settings: utils.stringify(machineSettings), mp_last_modified: utils.getNow() }
        });
    }

    static async updateMachineEmailSettingsAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        // console.log(args);
        let updateValues = args.updateValues;

        // read current settings
        let machineSettings = await this.getMProfileSettingsAsync({ mpCode });

        // to avoid overwrite by client data
        updateValues.prvEmail.PGPPrvKey = machineSettings.prvEmail.PGPPrvKey;
        updateValues.prvEmail.PGPPubKey = machineSettings.prvEmail.PGPPubKey;
        updateValues.pubEmail.PGPPrvKey = machineSettings.pubEmail.PGPPrvKey;
        updateValues.pubEmail.PGPPubKey = machineSettings.pubEmail.PGPPubKey;

        machineSettings.prvEmail = updateValues.prvEmail;
        machineSettings.pubEmail = updateValues.pubEmail;
        machineSettings.prvEmail.address = machineSettings.prvEmail.address.trim();
        machineSettings.pubEmail.address = machineSettings.pubEmail.address.trim();
        machineSettings.machineAlias = updateValues.machineAlias;
        machineSettings.language = updateValues.language;
        machineSettings.termOfServices = updateValues.termOfServices;

        await model.aUpdate({
            table,
            query: [
                ['mp_code', mpCode]
            ],
            updates: { mp_settings: utils.stringify(machineSettings), mp_last_modified: utils.getNow() }
        });
        return { err: false, msg: `Profile(${mpCode}) Updated` }
    }

    static updateMachineSettings(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let machineSettings = args.machineSettings;

        // read current settings
        let currentSettings = model.sRead({
            table,
            query: [
                ['mp_code', mpCode]
            ]
        });
        currentSettings = utils.parse(currentSettings[0].mp_settings)
        // modify settings
        _.forOwn(machineSettings, (value, key) => {
            _.set(currentSettings, key, value)
        });
        return model.sUpdate({
            table,
            query: [
                ['mp_code', mpCode]
            ],
            updates: { mp_settings: utils.stringify(machineSettings) }
        });
    }


    // static comonSetting() {
    //     return comonSetting;
    // }


    static addNeighborM(args) {
        return MachineHandler.neighborHandler.addANeighborSync(args);
    }

    static cutNeighbor(args) {
        return MachineHandler.neighborHandler.deleteNeighborSync(args.nId);
    }

    static async handshakeNeighbor(args) {
        return await MachineHandler.neighborHandler.doHandshakeNeighbor(args);
    }

    /**
     * if the creationdate of latest recorded block in DAG is older than 2 cycle, so machine must go to synching mode
     * @param {*} args 
     */
    static isInSyncProcess(args = {}) {
        let forsToControlBasedOnDAGStatus = _.has(args, 'forsToControlBasedOnDAGStatus') ? args.forsToControlBasedOnDAGStatus : false;
        const dagHandler = require('../dag/graph-handler/dag-handler');
        let lastSyncStatus = kvHandler.getValueSync('lastSyncStatus');
        if (utils._nilEmptyFalse(lastSyncStatus)) {
            MachineHandler.initLastSyncStatus();
            lastSyncStatus = kvHandler.getValueSync('lastSyncStatus');
        }
        lastSyncStatus = utils.parse(lastSyncStatus);

        let cycleByMinutes = iConsts.getCycleByMinutes();
        // control if the last status-check is still valid (is younger than 30 minutes?= 24 times in a cycle)
        if (!forsToControlBasedOnDAGStatus && (lastSyncStatus.checkDate > utils.minutesBefore((cycleByMinutes / 24)))) {
            return (lastSyncStatus.lastDAGBlockCreationDate < utils.minutesBefore(2 * cycleByMinutes))

        } else {
            // re-check graph info & update status-check info too
            let lastWBLock = dagHandler.walkThrough.getLatestBlock().wBlock;
            let isInSyncProcess = (lastWBLock.bCreationDate < utils.minutesBefore(2 * cycleByMinutes))

            lastSyncStatus.isInSyncMode = isInSyncProcess ? 'Y' : 'N';
            lastSyncStatus.checkDate = utils.getNow();
            lastSyncStatus.lastDAGBlockCreationDate = lastWBLock.bCreationDate;
            if (isInSyncProcess)
                lastSyncStatus.lastTimeMachineWasInSyncMode = utils.getNow();
            kvHandler.upsertKValueSync('lastSyncStatus', utils.stringify(lastSyncStatus));
            return isInSyncProcess;

        }

    }

    static getLastSyncStatus() {
        let lastSyncStatus = kvHandler.getValueSync('lastSyncStatus');
        if (utils._nilEmptyFalse(lastSyncStatus)) {
            MachineHandler.initLastSyncStatus();
            lastSyncStatus = kvHandler.getValueSync('lastSyncStatus');
        }
        lastSyncStatus = utils.parse(lastSyncStatus);
        return lastSyncStatus;
    }

    static initLastSyncStatus() {
        let lastSyncStatus = {
            isInSyncMode: 'Unknown',
            lastTimeMachineWasInSyncMode: utils.minutesBefore(iConsts.getCycleByMinutes() * 2),
            checkDate: utils.minutesBefore(iConsts.getCycleByMinutes()),
            lastDAGBlockCreationDate: 'Unknown'
        }
        kvHandler.upsertKValueSync('lastSyncStatus', utils.stringify(lastSyncStatus));
    }

}
MachineHandler.neighborHandler = require('./neighbors/neighbors-handler');
MachineHandler.neighborHandler._super = MachineHandler;

MachineHandler.backupHandler = require('./machine-backup-restore');
MachineHandler.backupHandler._super = MachineHandler;

MachineHandler.getMachineServiceInterests = require('./service-interests').getMachineServiceInterests;


module.exports = MachineHandler;
