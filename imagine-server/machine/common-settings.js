// const _ = require('lodash');
// const model = require("../models/interface");
// const utils = require("../utils/utils");
// const getMoment = require("../startup/singleton").instance.getMoment

// let defSetting = {
//     language: ""
// }

// const table = 'i_kvalue';

// class CommonSettingsHandler {

//     static async getCommonSettingsAsync() {
//         let res = await model.aRead({
//             table,
//             fields: ['kv_value'],
//             query: [
//                 ['kv_key', 'img_common_setting']
//             ]
//         });
//         if (_.has(res, 'rows') && _.has(res.rows, 'length') && (res.rows.length > 0))
//             return JSON.parse(res.rows[0]);
//         return {};
//     }

//     static getCommonSettingsSync() {
//         let res = model.sRead({
//             table,
//             fields: ['kv_value'],
//             query: [
//                 ['kv_key', 'img_common_setting']
//             ]
//         });
//         if (res.length > 0)
//             return utils.parse(res[0].kv_value);
//         return {};
//     }



//     // static async updateCommonSettingAsync(args = {}) {
//     //     // load current settings
//     //     let commonSettings = await this.getCommonSettingsAsync();

//     //     // modify settings
//     //     if (_.has(args, 'language'))
//     //         commonSettings.language = args.language

//     //     // update settings
//     //     return await model.aUpdate({
//     //         table,
//     //         query: [
//     //             ['kv_key', 'img_common_setting']
//     //         ],
//     //         updates: { kv_value: JSON.stringify(commonSettings), kv_last_modified: getMoment().format('YYYY-MM-DD HH:mm:ss') }
//     //     });
//     // }

//     static updateCommonSettingSync(args = {}) {
//         // load current settings
//         let commonSettings = this.getCommonSettingsSync();

//         // modify settings
//         if (_.has(args, 'language'))
//             commonSettings.language = args.language


//         // update settings
//         return model.sUpdate({
//             table,
//             query: [
//                 ['kv_key', 'img_common_setting']
//             ],
//             updates: { kv_value: JSON.stringify(commonSettings), kv_last_modified: getMoment().format('YYYY-MM-DD HH:mm:ss') }
//         });
//     }
// }

// module.exports = CommonSettingsHandler;