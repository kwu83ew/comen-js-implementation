const _ = require('lodash');
const iConsts = require('../../config/constants');
const clog = require('../../loggers/console_logger');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const crypto = require('../../crypto/crypto');
const seedNeighbors = require('./seed-neighbors');
const packetGenerators = require('../../messaging-protocol/greeting/greeting-generators');
const networker = require('../../network-adapter/network-pusher');
const GQLHandler = require('../../services/graphql/graphql-handler');
const sendingQ = require('../../services/sending-q-manager/sending-q-manager');



const table = 'i_machine_neighbors';

class NeighborsHandler {

    static addSeedNeighbors() {
        for (let aNeighbor of seedNeighbors) {
            // dbl chk
            let dbl = model.sRead({
                table,
                query: [['n_email', aNeighbor.email]]
            });
            if (dbl.length == 0) {
                model.sCreate({
                    table,
                    values: {
                        n_mp_code: iConsts.CONSTS.DEFAULT,
                        n_email: aNeighbor.email,
                        n_pgp_public_key: aNeighbor.publicKey,
                        n_is_active: aNeighbor.isActive,
                        n_connection_type: aNeighbor.connectionType,
                        n_info: utils.stringify({}),
                        n_creation_date: utils.getNow(),
                        n_last_modified: utils.getNow()
                    }
                });
            };
        };
    }

    static async deleteNeighborAsync(args) {
        args.table = table
        let res = await model.aDelete(args);
        return res;
    }

    static dropNeighborsBecauseOfRemoveProfile(mpCode) {
        model.sDelete({
            table,
            query: [['n_mp_code', mpCode]]
        });
        return { err: false, msg: `The neighbors of profile(${mpCode}) delete` }
    }

    static deleteNeighborSync(args) {
        let msg;
        let nId = _.has(args, 'nId') ? args.nId : null;
        let neiInfo = model.sRead({
            table,
            query: [['n_id', nId]]
        });
        if (neiInfo.length != 1)
            return { err: true, msg: `Neighbor id(${nId}) not exist` }
        neiInfo = neiInfo[0];

        let machineSettings = NeighborsHandler._super.getMProfileSettingsSync();
        let machinePGPPrvKey = machineSettings.pubEmail.PGPPrvKey;
        let machineEmail = machineSettings.pubEmail.address;
        let neighborEmailAddress = neiInfo.n_email;

        if (utils._nilEmptyFalse(machinePGPPrvKey) || utils._nilEmptyFalse(machineEmail)) {
            msg = `write Please Remove Me, missed parameters ${utils.stringify(args)}`;
            clog.app.info(msg);
            return { err: true, msg }
        }

        if (!utils._nilEmptyFalse(neighborEmailAddress)) {

            setTimeout(() => {
                let card = {
                    cdType: GQLHandler.cardTypes.pleaseRemoveMeFromYourNeighbors,
                    cdVer: '0.0.1',
                    emailToBeRemoved: machineEmail,
                }

                let nativeSignMsg = crypto.keccak256(utils.stringify(card));
                card.signature = crypto.nativeSign(machinePGPPrvKey, nativeSignMsg);
                clog.app.info(`signed card to send remove ${utils.stringify(card)}`);

                let { code, body } = GQLHandler.makeAPacket({
                    cards: [card]
                });
                let pushRes = sendingQ.pushIntoSendingQ({
                    sqType: iConsts.CONSTS.GQL,
                    sqCode: code,
                    sqPayload: body,
                    sqReceivers: [neighborEmailAddress],
                    sqTitle: `GQL PleaseRemoveMe packet(${utils.hash6c(code)})`,
                });
                if (pushRes.err != false) {
                    return pushRes;
                }
            }, 40000);
        }

        if (!utils._nilEmptyFalse(nId)) {
            model.sDelete({
                table,
                query: [['n_id', nId]]
            });
            return { err: false, msg: `Neighbor id(${nId}) was delete` }
        }

        return { err: false, msg: `GQL PleaseRemoveMe sent to neighbor` }
    }

    static doDeleteNeighbor(args) {
        console.log(`do Delete Neighbor args${utils.stringify(args)}`);
        clog.app.info(`do Delete Neighbor args${utils.stringify(args)}`);
        let msg;
        let payload = args.payload;
        let emailToBeRemoved = payload.emailToBeRemoved;
        emailToBeRemoved = model.SQLInjectionSanitize.doSan(emailToBeRemoved)
        let signature = payload.signature;

        let cdVer = _.has(payload, 'cdVer') ? payload.cdVer : null;
        if (utils._nilEmptyFalse(cdVer) || !iutils.isValidVersionNumber(cdVer)) {
            msg = `missed cdVer gql ${utils.stringify(args)}`;
            clog.app.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        }

        let theNeighbor = model.sRead({
            table,
            query: [['n_email', emailToBeRemoved]]
        });
        if (theNeighbor.length == 0) {
            msg = `The requested email to remove, not exist as a neighbor(${emailToBeRemoved})`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true };
        };
        let neighborPGPPubKey = theNeighbor[0].n_pgp_public_key;

        if (!utils._nilEmptyFalse(neighborPGPPubKey)) {// signature control
            let nativeSignMsg = crypto.keccak256(utils.stringify({
                cdType: GQLHandler.cardTypes.pleaseRemoveMeFromYourNeighbors,
                cdVer: '0.0.1',
                emailToBeRemoved: emailToBeRemoved
            }));
            let verify = crypto.nativeVerifySignature(neighborPGPPubKey, nativeSignMsg, signature);
            if (!verify) {
                msg = `The requested email to remove has not valid signature neighbor(${emailToBeRemoved})`;
                clog.sec.error(msg);
                return { err: true, msg, shouldPurgeMessage: true };
            }
        }

        // delete
        model.sDelete({
            table,
            query: [['n_email', emailToBeRemoved]]
        });
        msg = `The requested email to remove has been deleted (${emailToBeRemoved})`;
        clog.app.info(msg);
        return { err: false, msg, shouldPurgeMessage: true };
    }

    static addANeighborSync(args) {
        clog.app.info(`add new Neighbor args: ${utils.stringify(args)}`);
        // console.log('addA NeighborSync, add ANeighborSync', args);
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let nConnectionType = _.has(args, 'nConnectionType') ? args.nConnectionType : null;
        let nIsActive = _.has(args, 'nIsActive') ? args.nIsActive : iConsts.CONSTS.YES;
        let nInfo = _.has(args, 'nInfo') ? args.nInfo : utils.stringify({});
        let nEmail = _.has(args, 'nEmail') ? args.nEmail : null;
        let nPGPPubKey = _.has(args, 'nPGPPubKey') ? args.nPGPPubKey : '';
        let nCreationDate = _.has(args, 'nCreationDate') ? args.nCreationDate : utils.getNow();
        if (utils._nilEmptyFalse(nConnectionType))
            return { err: true, msg: 'THe neighbor connection type is missed!' }

        let exists = model.sRead({
            table,
            query: [
                ["n_mp_code", mpCode],
                ["n_connection_type", nConnectionType],
                ["n_email", nEmail]
            ]
        });
        if (exists.length > 0) {
            if (!utils._nilEmptyFalse(nPGPPubKey) && (nPGPPubKey != '')) {
                //update pgp key
                model.sUpdate({
                    table,
                    query: [
                        ["n_mp_code", mpCode],
                        ["n_connection_type", nConnectionType],
                        ["n_email", nEmail]
                    ],
                    updates: { n_pgp_public_key: nPGPPubKey, n_last_modified: utils.getNow() }
                })
                return { err: false, msg: `the iPGP key for email(${nEmail}) Activated connection(${nConnectionType}) profile(${mpCode})` }
            } else {
                return { err: nPGPPubKey, msg: `the iPGP key for email(${nEmail}) connection(${nConnectionType}) profile(${mpCode}) was missed` }
            }
        }
        let values = {
            n_mp_code: mpCode,
            n_email: nEmail,
            n_pgp_public_key: nPGPPubKey,
            n_is_active: nIsActive,
            n_connection_type: nConnectionType,
            n_creation_date: nCreationDate,
            n_info: nInfo,
            n_last_modified: utils.getNow()
        };
        clog.app.info(`goint to add new Neighbor: ${utils.stringify(values)}`);
        model.sCreate({
            table,
            values
        });
        return { err: false, msg: `new Neighbor(${nEmail}) was added. connection(${nConnectionType}) profile(${mpCode})` };
    }

    static async doHandshakeNeighbor(args) {
        clog.app.info(`handshake Neighbor args: ${utils.stringify(args)}`);

        try {
            let res = await packetGenerators.writeHandshake({
                receiverId: args.nId,
                connectionType: args.nConnectionType
            });
            clog.app.info(`packet Generators.write Handshake res : ${utils.stringify(res)}`);
            if (res.err != false)
                return res;

            // the concept is the node public email is propagated to more neighbors in order to strength connectivity, 
            // but the node private email will be used as a second plan to defence against the any kind of spaming/DOS Attacks ...
            return networker.iPushSync(res.pkt);
        } catch (e) {
            console.log(e);
            return { err: true, msg: e }
        }
    }

    static updateNeighbor(args) {
        args.table = table;
        model.sUpdate(args);
        return { err: false }
    }


    static async getNeighborsAsync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'n_mp_code'))
            query.push(['n_mp_code', mpCode]);
        args.query = query;
        args.table = table;
        let res = await model.aRead(args);
        res = res.map(x => this.convertFields(x));
        if (res.length == 0)
            return [];
        return res;
    }

    static getNeighborsSync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let query = _.has(args, 'query') ? args.query : [];
        if (!utils.queryHas(query, 'n_mp_code'))
            query.push(['n_mp_code', mpCode]);
        args.query = query;
        args.table = table;
        let res = model.sRead(args);
        if (res.length == 0)
            return [];
        return res.map(x => this.convertFields(x));
    }


    // TODO: adding some criteria to remove lazy, old, obsolute neigbors

    static async getActiveNeighborsAsync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        let neighbors = await model.aRead({
            table,
            fields: ["n_email", "n_pgp_public_key", "n_connection_type"],
            query: [
                ['n_is_active', iConsts.CONSTS.YES],
                ['n_mp_code', mpCode]
            ],
            order: [
                ['n_connection_type', 'DESC']
            ],
        });
        // convert psql to js convention
        let newNeighbors = [];
        for (let aNeighbor of neighbors) {
            newNeighbors.push(this.convertFields(aNeighbor))
        }
        return newNeighbors;
    }

    static getActiveNeighborsSync(args = {}) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();

        let neighbors = model.sRead({
            table,
            fields: ["n_email", "n_pgp_public_key", "n_connection_type"],
            query: [
                ['n_is_active', iConsts.CONSTS.YES],
                ['n_mp_code', mpCode]
            ],
            order: [
                ['n_connection_type', 'DESC']
            ],
        });
        if (neighbors.length == 0)
            return [];

        // convert psql to js convention
        let newNeighbors = [];
        for (let aNeighbor of neighbors) {
            newNeighbors.push(this.convertFields(aNeighbor))
        }
        return newNeighbors;
    }

    static floodEmailToNeighbors(email, PGPPubKey = '') {
        clog.app.info(`flood Email To Neighbors: ${utils.stringify(email)}`);

        let msg;
        const machine = require('../../machine/machine-handler');
        const packetGenerator = require('../../messaging-protocol/greeting/greeting-generators');
        if (utils._nilEmptyFalse(PGPPubKey)) {
            let emailInfo = NeighborsHandler.getNeighborsSync({
                query: [
                    ['n_connection_type', iConsts.CONSTS.PUBLIC],
                    ['n_email', email]
                ]
            });
            if (emailInfo.legnth == 0) {
                msg = `email(${email}) doesn't exist as a neighbor!`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            PGPPubKey = emailInfo[0].nPGPPublicKey;
        }

        let machineSettings = machine.getMProfileSettingsSync();

        /**
         * avoiding duplicate sending email
         * [{vertice: "neighborEmail->targetEmail", date:"presenting date"}]
         */
        let alreadyPresentedNeighbors = _.has(machineSettings, 'alreadyPresentedNeighbors') ? machineSettings.alreadyPresentedNeighbors : [];
        clog.app.info(`already Presented to these Neighbors ${utils.stringify(alreadyPresentedNeighbors)}`);

        let activeNeighbors = NeighborsHandler.getNeighborsSync({
            query: [
                ['n_connection_type', iConsts.CONSTS.PUBLIC],
                ['n_is_active', iConsts.CONSTS.YES]
            ]
        });
        clog.app.info(`active Neighbors to flood email to neigbor ${utils.stringify(activeNeighbors)}`);
        let vertice;
        let isAlreadySent;
        for (let neighbor of activeNeighbors) {
            if (neighbor.nEmail == email)
                continue;   // not presenting machine to itself

            isAlreadySent = false;
            vertice = email + "__to__" + neighbor.nEmail
            alreadyPresentedNeighbors.forEach(vert => {
                clog.app.info(`verts ${utils.stringify(vert)}`)
                if (vert.vertice == vertice) {
                    clog.sec.info(`!!! the email already broadcasted ${vertice} `)
                    isAlreadySent = true
                }
            });
            if (!isAlreadySent || true) {
                //TODO: adding some expiration control to have availabality to re-broadcast email
                alreadyPresentedNeighbors.push({
                    vertice: vertice,
                    date: utils.getNow()
                });

                let packet = packetGenerator.writeHereIsNewNeighbor({
                    connectionType: iConsts.CONSTS.PUBLIC,
                    machineEmail: machineSettings.pubEmail.address,
                    machinePGPPrvKey: machineSettings.pubEmail.PGPPrvKey,

                    target: neighbor.nEmail,
                    targetPGPPubKey: neighbor.nPGPPublicKey,

                    newNeighborEmail: email,
                    newNeighborPGPPubKey: PGPPubKey,
                });
                clog.app.info(`packet ready to flood email to neigbor ${utils.stringify(packet)}`);
                clog.app.info(`the machine(${utils.stringify(packet)}) presents(${email}) to (${neighbor.nEmail})`);
                console.log(`the machine(${utils.stringify(packet)}) presents(${email}) to (${neighbor.nEmail})`);

                let sent = networker.iPushSync(packet)
            }
        }

        // update machine settings
        machineSettings.alreadyPresentedNeighbors = alreadyPresentedNeighbors

        machine.updateMachineSettings({ machineSettings })
        let machineSettingsNew = machine.getMProfileSettingsSync();

        return { err: false }
    }


    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'n_mp_code'))
            out.nmpCode = elm.n_mp_code;
        if (_.has(elm, 'n_id'))
            out.nId = elm.n_id;
        if (_.has(elm, 'n_email'))
            out.nEmail = elm.n_email;
        if (_.has(elm, 'n_pgp_public_key'))
            out.nPGPPublicKey = elm.n_pgp_public_key;
        if (_.has(elm, 'n_is_active'))
            out.nIsActive = elm.n_is_active;
        if (_.has(elm, 'n_connection_type'))
            out.nConnectionType = elm.n_connection_type;
        if (_.has(elm, 'n_creation_date'))
            out.nCreationDate = elm.n_creation_date;
        if (_.has(elm, 'n_info'))
            out.nInfo = elm.n_info;
        if (_.has(elm, 'n_last_modified'))
            out.nLastModified = elm.n_last_modified;

        return out;
    }

}

module.exports = NeighborsHandler;
