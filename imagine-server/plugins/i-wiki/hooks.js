

class Hooks{

    static ih_before_push_doc_to_buffer_async(args){
        console.log('>>> in WikiManager ih_before_push_doc_to_buffer_async', args);
    }

    static ih_create_coinbase_block(args){
        console.log('>>> in WikiManager ih_create_coinbase_block', args);
    }
    
    static ih_control_if_missed_block(args){
        console.log('>>> in WikiManager ih_control_if_missed_block', args);
    }


}

module.exports = Hooks