const _ = require('lodash');


class Hooks {

    static SPSH_failed_on_validate_FSBlock(args) {
    }

    static SASH_signals(args) {
        args.signals.push({ sig: 'iNReputation', ver: '0.0.0' });
        // console.log(`inside plugin SASH_signals args: ${JSON.stringify(args)}`);
        return args;
    }

    static async SPSH_after_parse_packet(args) {
        let PLUGIN_KEY = 'Plugin_ReputationManager'
        let current = await args.imApp.kvHandler.getValueAsync(PLUGIN_KEY);
        if (current == null) {
            current = {}
        } else {
            current = JSON.parse(current);
        }

        let senderKey = [args.packet.sender, args.parsePacketRes.connection_type].join()
        // console.log('ReputationManager SPSH_after_parse_packet key:', senderKey);
        if (!_.has(current, senderKey)) {
            current[senderKey] = {
                successPackets: 0,
                corruptedPackets: 0,
            }
        }

        if (args.parsePacketRes.err != false) {
            current[senderKey].corruptedPackets++;
        } else {
            current[senderKey].successPackets++;

        }

        await args.imApp.kvHandler.upsertKValueAsync(PLUGIN_KEY, JSON.stringify(current));
    }


}

module.exports = Hooks