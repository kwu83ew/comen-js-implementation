


// here each node(group of nodes) can implement any customized document(e.g. file-storage-docs, arbitary-daocument-about-proof-of-existance...) validation



module.exports = {
    'iNReputation': {
        name: 'Imagine Neighbors Reputation System',
        version: '0.0.0',
        path: 'i-neightbors-reputation',
        status: 'enabled',
        description: 'a system to filing neighbors(and probably adversaries)'
    },

    'iWiki': {
        name: 'Imagine Distributed Wiki (IDW)',
        version: '0.0.0',
        path: 'i-wiki',
        status: 'enabled'
    },

    'iVC': {
        name: 'Imagine Distributed Versioning Control (IVC)',
        version: '0.0.0',
        path: 'i-version-control',
        status: 'disabled'
    },

    'iTM': {
        name: 'Imagine Distributed Task Manager (ITM)',
        version: '0.0.0',
        path: 'i-task-manager',
        status: 'disabled'
    },

}