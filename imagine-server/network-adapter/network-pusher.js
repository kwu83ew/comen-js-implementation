const _ = require('lodash');
const iConsts = require('../config/constants')
const utils = require('../utils/utils');
const emailHandler = require('../email_adapter/email');
const clog = require('../loggers/console_logger');
const fileHandler = require('../hard-copy/file-handler')
const listener = require('../plugin-handler/plugin-handler');
const kvHandler = require('../models/kvalue');

class NetworkPusher {

    static iPushSync(args) {
        let msg;
        try {
            clog.app.info(`iPush Sync args: ${utils.stringify(args)}`);
            listener.doCallSync('SPSH_push_packet', args);// Synchronous Passive Server Hook

            let title = _.has(args, 'title') ? args.title : null;
            let sender = _.has(args, 'sender') ? args.sender : null;
            let receiver = _.has(args, 'target') ? args.target : null;
            let message = _.has(args, 'message') ? args.message : null;

            let res;
            if (iConsts.DO_HARDCOPY_OUTPUT_EMAILS) {
                // create emails and write it on local hard drive
                // create an email copy in local hard drive
                // in such a way user can send email manually
                // she can sign some transactions and create a proper block, but not send it immideately
                // keeps it in safe place and when it need just send it manually to one or more backers
                res = fileHandler.emailFileHandler.writeEmailAsFile(args)
                clog.app.info(`write on HD res: ${utils.stringify(res)}`);
            }

            if (!iConsts.EMAIL_IS_ACTIVE)
                return res;

            // avoid duplicate sending
            let sentEmails = kvHandler.getValueSync('sentEmails');
            if (utils._nilEmptyFalse(sentEmails)) {
                sentEmails = {};
                kvHandler.upsertKValueSync('sentEmails', utils.stringify(sentEmails));
            } else {
                sentEmails = utils.parse(sentEmails);
            }
            if (_.has(sentEmails, res.hashHandle)) {
                msg = `email already was sent TO: ${receiver} (${title}) ${utils.stringify(res.hashHandle)}`;
                clog.app.info(msg);
                return res;

            } else {
                sentEmails[res.hashHandle] = utils.getNow();
                kvHandler.upsertKValueSync('sentEmails', utils.stringify(sentEmails));
            }
            let refreshSents = {};
            let cDate = utils.minutesBefore(iConsts.getCycleByMinutes());
            for (let aHash of utils.objKeys(sentEmails)) {
                if (sentEmails[aHash] > cDate)
                    refreshSents[aHash] = sentEmails[aHash];
            }
            kvHandler.upsertKValueSync('sentEmails', utils.stringify(refreshSents));

            // send email to via SMTP server
            res = emailHandler.sendEmailWrapper({
                sender,
                title,
                message: res.richEmail,
                receiver
            });
            return res;
        } catch (e) {
            console.log(e);
            return { err: true, msg: e }
        }
    }

    // // read local hard-drive
    // static async iPullAsync(args = {}) {
    //     return; // no one use this
    //     let popRes = null;

    //     popRes = await fileHandler.eFReadAsync(args)
    //     clog.app.info(`popRes ${utils.stringify(popRes)}`);
    //     if (utils._nilEmptyFalse(popRes))
    //         return null;

    //     return popRes
    // }

    // static iPullSync(args = {}) {
    //     let popRes = null;

    //     popRes = fileHandler.emailFileHandler.read EmFile()
    //     clog.app.info(`popRes ${utils.stringify(popRes)}`);
    //     if (utils._nilEmptyFalse(popRes))
    //         return null;

    //     return popRes
    // }


    // static async purgeMessageAsync(fileName) {
    //     clog.app.info(`purge MessageAsync ${fileName}`)
    //     // delete from inbox folder on local hard drive
    //     try {
    //         await fileHandler.eFDeleteAsync({ fileName: fileName })

    //         // TODO: delete from ticketing queue too

    //     } catch (e) {
    //         clog.app.error(e)
    //     }

    // }

    // static purgeMessageSync(fileName) {
    //     clog.sql.info(`purgeMessage Sync ${fileName}`)
    //     // delete from inbox folder on local hard drive
    //     try {
    //         fileHandler.eFDeleteSync({ fileName: fileName })

    //         // TODO: delete from ticketing queue too

    //     } catch (e) {
    //         clog.app.error(e)
    //     }

    // }
}

module.exports = NetworkPusher;