const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const networker = require('./network-pusher');
const clog = require('../loggers/console_logger');
const model = require('../models/interface');
const crypto = require('../crypto/crypto');
const machine = require('../machine/machine-handler');
const dispatchMessageSync = require('../messaging-protocol/dispatcher');
const messageTicketing = require('../models/message-ticketing/interface');
const listener = require('../plugin-handler/plugin-handler');
const emailHandler = require('../email_adapter/email');
const fileHandler = require('../hard-copy/file-handler');
const parsingQHandler = require('../services/parsing-q-manager/parsing-q-handler');
const kvHandler = require('../models/kvalue');

let packet;
let pullCounter = 0;
let popCounter = 0;
let machineSettings;

class NetListener {

    static launchHardCopyReading() {
        // retreive machine settings 
        NetListener.recursive_reading_harddisk();
        NetListener.recursive_pluginIngress();
        NetListener.maybeBootDAGFromBundle();
    };

    static maybeBootDAGFromBundle() {
        
        const DAGDlHandler = require('../messaging-protocol/dag/download-full-dag');
        let bundle = DAGDlHandler.readDAGBundleIfExist();
        if ((bundle.err != false) || utils._nilEmptyFalse(bundle.content))
            return;

        let DAGBundle = utils.parse(bundle.content);
        let blocks = DAGBundle.blocks;
        let ballots = DAGBundle.ballots;

        clog.app.info(`Read & Dispatching (${blocks.length})blocks and (${utils.objKeys(ballots).length})Ballots from DAGBundle`);
        // normalizing/sanitize Ballots Receive Dates and upsert into kv
        try {
            let sanBallots = {};
            for (let aBlt of utils.objKeys(ballots)) {
                sanBallots[utils.stripNonAlphaNumeric(aBlt)] = {
                    baReceiveDate: utils.stripNonInDateString(ballots[aBlt].baReceiveDate.toString()),
                    baVoteRDiff: utils.stripNonNumerics(ballots[aBlt].baVoteRDiff.toString()),
                }
            }
            kvHandler.upsertKValueSync('ballotsReceiveDates', utils.stringify(sanBallots));
            dspchRes = { err: false, shouldPurgeMessage: true }
        } catch (e) {
            clog.sec.error(e);

        }

        // dispatching blocks to sending q
        blocks.forEach(aBlock => {
            dispatchMessageSync({
                sender: 'DAGBundle',
                message: aBlock,
                connectionType: iConsts.CONSTS.PRIVATE
            });
        });

        // Archive DAGBundle file in tmp folder 
        DAGDlHandler.archiveDAGBundle();

        return { err: false };

    }

    static recursive_reading_harddisk() {
        setTimeout(NetListener.recursive_reading_harddisk, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getHardDiskReadingGap())
        NetListener.do_reading_harddisk();
    }

    static do_reading_harddisk() {

        pullCounter += 1
        let packet = fileHandler.emailFileHandler.readEmailFile();
        clog.app.info(`have read packet from HD ${utils.stringify(packet)}`);
        if (packet.secIssue) {
            clog.sec.error(`secIssue: ${utils.stringify(packet)}`);
            return packet;
        }
        if (packet.err != false) {
            clog.sec.error(utils.stringify(packet));
            return packet;
        }

        //developer log
        if (iConsts.isDevelopMode())
            model.sCreate({
                'table': 'idev_inbox_logs',
                values: {
                    il_creation_date: utils.getNow(),
                    il_title: packet.fileName
                }
            });

        if (!packet.thereIsPacketToParse)
            return { err: false }

        clog.app.info(`unwrapPacket input: ${utils.stringify(packet)}`);
        listener.doCallSync('SPSH_before_parse_packet', packet);

        let parsePacketRes = NetListener.decryptAndParsePacketSync(packet);
        listener.doCallSync('SPSH_after_parse_packet', { packet, parsePacketRes });

        clog.app.info(`parsePacketRes: ${utils.shortenLog(utils.stringify(parsePacketRes))}`);
        if (parsePacketRes.err != false) {
            //TODO: implement a reputation system based on sender email address toavoid pottentially attacks (e.g DOS)
            NetListener.maybePurgeMessage(packet, parsePacketRes.shouldPurgeMessage);
            return parsePacketRes;
        }

        let dispatchRes = dispatchMessageSync({
            sender: packet.sender,
            message: parsePacketRes.message,
            connectionType: parsePacketRes.connection_type
        })

        clog.app.info(`========= parsing res: ${JSON.stringify(dispatchRes)}`);
        parsePacketRes.err = dispatchRes.err
        parsePacketRes.shouldPurgeMessage = dispatchRes.shouldPurgeMessage

        //should purge packet?
        if (packet.fileName != '') {
            let shouldPurgeMessage = _.has(dispatchRes, 'shouldPurgeMessage') ? dispatchRes.shouldPurgeMessage : false;
            if (shouldPurgeMessage == false) {
                clog.sec.error(`why shouldPurgeMessage == false? \npacket: ${utils.stringify(packet)}\ndispatchRes: ${utils.stringify(dispatchRes)}`);
            }
            NetListener.maybePurgeMessage(packet, (_.has(dispatchRes, 'shouldPurgeMessage') ? dispatchRes.shouldPurgeMessage : false));
        }

        let dispatchResErr = _.has(dispatchRes, 'error') ? dispatchRes.error : null;
        if (utils._notNil(dispatchResErr)) {
            //TODO:  some log to db denoting to "unable to parse a message"
            clog.app.error(dispatchRes)
        }

        return parsePacketRes
    }

    /**
     * 
     * @param {*} packet 
     * packets which are received by plugins can be pushed to imagine core code to parse & react via pluginIngress method
     * specially the plugin which prepare a connection to other nodes via socket and wants to forward the received packet 
     * to imagine core code
     */
    static recursive_pluginIngress() {
        setTimeout(NetListener.recursive_pluginIngress, iConsts.ONE_MINUTE_BY_MILISECOND * iConsts.getHardDiskReadingGap())

        pullCounter += 1
        let packet = listener.doCallSync('SPSH_packet_ingress');
        if (!utils._nilEmptyFalse(packet)) {
            clog.app.info(`plugin relayed a packet to imagine core`);
            NetListener.decryptAndParsePacketSync(packet);
        }
    }

    static decryptAndParsePacketSync(packet) {
        let msg;
        let parseRes = { err: false }

        if (utils._nilEmptyFalse(packet.message)) {
            msg = `packet has empty message ${JSON.stringify(packet)}`;
            clog.app.info(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }

        machineSettings = machine.getMProfileSettingsSync();

        let dummyTitle = `parse Packet Sync: sender(${packet.sender} receiver(${packet.receiver} name(${packet.fileName})`
        clog.app.info(dummyTitle)

        // later again try this message or purge it after a certain try and fail. but not blocked on this message.
        messageTicketing.tLog(packet.fileName)

        // retreive sender's info
        let senderInfo = machine.neighborHandler.getNeighborsSync({
            query: [
                ['n_email', packet.sender]
            ]
        });
        let senderPubKey = null;

        if (senderInfo.length > 0) {
            senderPubKey = senderInfo[0].nPGPPublicKey
        } else {
            /**
             * sender is not in my neighbors, so i add it as neighbor
             * TODO: probably security issue! machine must not add all new random emails.
             * instead must list them and user decides about that
             * so it must be implemented
             */
            let params = {
                nEmail: packet.sender,
                nPGPPubKey: '',
                nIsActive: iConsts.CONSTS.YES,
                nConnectionType: iConsts.CONSTS.PUBLIC,
            };
            clog.sec.error(`Unknown email addresse sent msg, so add it automatically as a new neighbor ${utils.stringify(params)}`);
            machine.neighborHandler.addANeighborSync(params);

            // retrieve id of newly inserted email
            let newNeiInfo = machine.neighborHandler.getNeighborsSync({
                query: [
                    ['n_email', packet.sender]
                ]
            });
            if (newNeiInfo.length == 0) {
                msg = `coludn't insert unknown email as a new neighbor(${packet.sender})`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // and now do handshake
            setTimeout(() => {
                let hParams = {
                    nId: newNeiInfo[0].nId,
                    nConnectionType: iConsts.CONSTS.PUBLIC
                }
                clog.sec.error(`automatically handshake with Unknown email addresse ${utils.stringify(hParams)}`);
                machine.neighborHandler.doHandshakeNeighbor(hParams);
            }, 5000);

        }
        // if (senderPubKey == '')
        //     senderPubKey = null;    // for new neighbors

        let machinePrvPGPKey;
        if (packet.receiver == machineSettings.prvEmail.address) {
            machinePrvPGPKey = machineSettings.prvEmail.PGPPrvKey;
            parseRes.connection_type = iConsts.CONSTS.PRIVATE;

        } else if (packet.receiver == machineSettings.pubEmail.address) {
            machinePrvPGPKey = machineSettings.pubEmail.PGPPrvKey;
            parseRes.connection_type = iConsts.CONSTS.PUBLIC;

        } else {
            msg = `in parse PacketSync unknown email packet:${utils.stringify(packet)} \n\n`;//+` machineSettings:${utils.stringify(machineSettings)}`;
            clog.sec.error(msg);
            return { err: true, msg, shouldPurgeMessage: true }
        }
        let decryptedMessage;
        try {
            decryptedMessage = crypto.decryptPGP({
                prvKey: machinePrvPGPKey,
                senderPubKey,
                message: packet.message
            });

        } catch (e) {
            msg = `Failed decrypt msg ${utils.stringify(e)}`
            clog.sec.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }

        if (decryptedMessage.err != false) {
            clog.sec.warn(`Decryption failed msg from ${packet.sender} ${JSON.stringify(decryptedMessage.err != false)} ${JSON.stringify(packet)}`);
            this.dropInvalidOrInsecureMessage(packet);
            parseRes.err = true;
            parseRes.msg = `droped Invalid Or Insecure Message from ${packet.sender} (Decryption failed) ${decryptedMessage.msg}`;
            return parseRes;
        }

        if (decryptedMessage.isSigned && !decryptedMessage.verified) {
            msg = 'droped Invalid Or Insecure Message (Wrong signature)';
            clog.sec.error(msg)
            this.dropInvalidOrInsecureMessage(packet)
            parseRes.err = true
            parseRes.msg = msg;
            return parseRes
        }
        if (utils._nilEmptyFalse(decryptedMessage.message)) {
            msg = 'droped Invalid Or Insecure Message (empty message body)';
            clog.sec.info(msg);
            this.dropInvalidOrInsecureMessage(packet);
            parseRes.err = true;
            parseRes.msg = msg;
            return parseRes;
        }

        try {
            message = utils.parse(decryptedMessage.message);
        } catch (e) {
            msg = `Failed parse msg ${utils.stringify(e)} ${decryptedMessage.message}`
            clog.sec.error(msg)
            return { err: true, msg, shouldPurgeMessage: true }
        }

        parseRes.message = message;
        return parseRes;

    }

    static dropInvalidOrInsecureMessage(pckt) {
        clog.sec.info(`!!! drop InvalidOrInsecureMessage(${pckt.fileName})`)
        if (utils._nilEmptyFalse(pckt.fileName)) {
            clog.app.error(`drop Invalid Or Insecure Message, got empty fileName!`);
            return;
        }
        fileHandler.emailFileHandler.deleteEmailFile({ fileName: pckt.fileName });
    }

    static maybePurgeMessage(packet, shouldPurgeMessage) {
        //should purge packet?
        let expired = false;
        if (_.has(packet, 'creation_date')) {
            let creation_date = new Date(packet.creation_date);
            expired = utils.isItExpired(creation_date, 60); //after 60 minutes
        }
        let reachedTL = this.richedTryLimitation(packet);
        if (shouldPurgeMessage || expired || reachedTL) {
            clog.app.info(`should-Purge-Message ${shouldPurgeMessage} is-expired ${expired}  reached-Try-Limitation ${reachedTL}`);
            if (utils._nilEmptyFalse(packet.fileName)) {
                clog.app.error(`maybe Purge Message, got empty fileName! ${utils.stringify(packet)}`);
                return;
            }
            fileHandler.emailFileHandler.deleteEmailFile({ fileName: packet.fileName });
        }
    }

    static richedTryLimitation(packet) {
        try {
            let tryCount = messageTicketing.getTry(packet.fileName)
            if (tryCount > iConsts.MAX_PARSE_ATTEMPS_COUNT)
                return true
            return false
        } catch (e) {
            throw new Error(e)
        }
    };

    // the popEmail does POP3/IMAP to pop emails from server and download it to local folder
    static launchEmailPoper() {
        setTimeout(() => {
            NetListener.fetchPrvEmailAndWriteOnHardDisk();
        }, 10);
        setTimeout(() => {
            NetListener.fetchPubEmailAndWriteOnHardDisk();
        }, 20);
    };

    static async fetchPrvEmailAndWriteOnHardDisk() {
        clog.app.info(`fetch Prv Email AndWriteOnHardDisk`);
        let msg;
        popCounter += 1;
        let machineInfo = machine.getMProfileSettingsSync();
        // console.log('machineInfo', machineInfo);
        let prvEmail = machineInfo.prvEmail;
        setTimeout(NetListener.fetchPrvEmailAndWriteOnHardDisk, 60000 * prvEmail.fetchingIntervalByMinute);

        // fetch private inbox
        let params = {
            emailAddress: prvEmail.address,
            password: prvEmail.pwd,
            host: prvEmail.incomingMailServer,
            port: prvEmail.incomeIMAP,
            funcMode: 'readUNSEENs'
        }
        if (
            utils._nilEmptyFalse(params.emailAddress) ||
            utils._nilEmptyFalse(params.password) ||
            utils._nilEmptyFalse(params.host) ||
            utils._nilEmptyFalse(params.port)
        ) {
            msg = `missed some parameter of Private IMAP fetching ${params}`;
            clog.app.info(`${msg} `);
            return { err: true, msg }
        } else {
            let popRes = await emailHandler.IMAPFetcher.fetchInbox(params);
            clog.app.info(`${popCounter}. incomeIMAP prv mailbox res: ${popRes}`);
            return popRes;
        }
    }

    static async fetchPubEmailAndWriteOnHardDisk() {
        clog.app.info(`fetch Pub Email AndWriteOnHardDisk`);
        let msg;
        popCounter += 1;
        let machineInfo = machine.getMProfileSettingsSync();
        // console.log('machineInfo', machineInfo);
        let pubEmail = machineInfo.pubEmail;
        setTimeout(NetListener.fetchPubEmailAndWriteOnHardDisk, 60000 * pubEmail.fetchingIntervalByMinute);

        // fetch private inbox
        let params = {
            emailAddress: pubEmail.address,
            password: pubEmail.pwd,
            host: pubEmail.incomingMailServer,
            port: pubEmail.incomeIMAP,
            funcMode: 'readUNSEENs'
        }
        if (
            utils._nilEmptyFalse(params.emailAddress) ||
            utils._nilEmptyFalse(params.password) ||
            utils._nilEmptyFalse(params.host) ||
            utils._nilEmptyFalse(params.port)
        ) {
            msg = `missed some parameter of Public IMAP fetching`;
            console.log(`msg`, msg, params);
            clog.app.info(`msg ${msg} ${params}`);
            return { err: true, msg }
        }
        let popRes = await emailHandler.IMAPFetcher.fetchInbox(params);

        clog.app.info(`${popCounter}. incomeIMAP pub mailbox ${popRes}`);
    }
}


module.exports = NetListener;
