
const _ = require('lodash');
const utils = require('../utils/utils');
const db = require('../startup/db2');
const clog = require('../loggers/console_logger');

class DBModel {
    static async aRead(args = {}) {
        let log = _.has(args, 'log') ? args.log : true;
        let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args);
        let q = `SELECT ${_fields}  FROM ${args.table} ${_clauses} ${_order} ${_limit}`;
        return await db.aQuery(q, _values, log);
    }

    static sRead(args = {}) {
        try {
            let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
            let log = _.has(args, 'log') ? args.log : true;
            let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args);
            let q = 'SELECT ' + _fields + ' FROM ' + args.table + _clauses + _order + _limit;
            return db.sQuery(q, _values, lockDb, log);

        } catch (e) {
            throw new Error(e);
        }
    }

    static async aCustom(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let values = _.has(args, 'values') ? args.values : [];
            return await db.aQuery(args.query, values, log);

        } catch (e) {
            throw new Error(e);
        }
    }

    static sCustom(args = {}) {
        try {
            let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
            let log = _.has(args, 'log') ? args.log : true;
            let values = _.has(args, 'values') ? args.values : [];
            return db.sQuery(args.query, values, lockDb, log);

        } catch (e) {
            throw new Error(e);
        }
    }

    static async aCreate(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let { _fields, _values } = db.insertQueryGenerator(args.values)
            if (utils._nilEmptyFalse(_fields))
                console.error(new Error(`Nothing to insert in ${args.table}!`));

            let q = 'INSERT INTO ' + args.table + _fields;
            // clog.app.info('?????___________???? ');
            // clog.app.info(q, _values);
            // clog.app.info('?????___________???? ');
            return await db.aQuery(q, _values, log);

        } catch (err) {
            console.error(new Error(err))
        }
    }

    static sCreate(args) {
        let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
        let log = _.has(args, 'log') ? args.log : true;
        let { _fields, _values } = db.insertQueryGenerator(args.values)
        if (utils._nilEmptyFalse(_fields))
            console.error(new Error(`Nothing to insert in ${args.table}!`));

        let q = 'INSERT INTO ' + args.table + _fields;
        // clog.sql.info('?????___________???? ');
        // clog.sql.info(q, _values);
        // clog.sql.info('?????___________???? ');
        return db.sQuery(q, _values, lockDb, log);
    }

    static async aUpdate(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let upd = db.updateQueryGenerator(args);
            // clog.app.info(upd);
            return await db.aQuery(upd._query, upd._values, log);

        } catch (e) {
            console.error(new Error(e))
        }
    }

    static sUpdate(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let lockDb = _.has(args, 'lockDb') ? args.lockDb : true;
            let upd = db.updateQueryGenerator(args);
            // clog.app.info(upd);
            let res = db.sQuery(upd._query, upd._values, lockDb, log);
            // clog.app.info(res);
            return res

        } catch (e) {
            console.error(new Error(e))
        }
    }

    static async aUpsert(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let upsrt = db.upsertQueryGenerator(args);
            // clog.app.info(upsrt);
            let res = await db.aQuery(upsrt._query, upsrt._values, log);
            // clog.app.info(res);
            return res
        } catch (e) {
            console.error(new Error(e))
        }
    }

    static sUpsert(args) {
        try {
            let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
            let log = _.has(args, 'log') ? args.log : true;
            let upsrt = db.upsertQueryGenerator(args);
            let res = db.sQuery(upsrt._query, upsrt._values, lockDb, log);
            // clog.app.info(res);
            return res
        } catch (e) {
            throw new Error(e)
        }
    }

    static async aDelete(args) {
        try {
            let log = _.has(args, 'log') ? args.log : true;
            let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args);
            // clog.app.info(_clauses);
            // clog.app.info(_values);
            let _query = 'DELETE FROM ' + args.table + _clauses;
            let res = await db.aQuery(_query, _values, log);
            // clog.app.info(res);
            return res

        } catch (e) {
            console.error(new Error(e))
        }
    }

    static sDelete(args) {
        try {
            let lockDb = _.has(args, 'lockDb') ? args.lockDb : false;
            let log = _.has(args, 'log') ? args.log : true;
            let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args);
            let _query = 'DELETE FROM ' + args.table + _clauses;
            let res = db.sQuery(_query, _values, lockDb, log);
            return res

        } catch (e) {
            console.error(new Error(e))
        }
    }

    static async aQuery(query, params) {
        try {
            let log = _.has(params, 'log') ? params.log : true;
            return db.aQuery(query, params, log);
        } catch (e) {
            clog.app.info(new Error(e));
            return null;
        }
    }

    static sQuery(query, params) {
        try {
            let lockDb = _.has(params, 'lockDb') ? params.lockDb : false;
            let log = _.has(params, 'log') ? params.log : true;
            return db.sQuery(query, params, lockDb, log);
        } catch (e) {
            clog.app.info(new Error(e));
            return null;
        }
    }

}

DBModel.makeMultiPlacehoders = require('./make-multi-placehoders');
DBModel.SQLInjectionSanitize = require('./sql-injection-sanitize');

module.exports = DBModel;
