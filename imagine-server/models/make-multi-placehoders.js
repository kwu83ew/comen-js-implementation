function makeMultiPlacehoders(placeholdersCount, offset = 1) {
    let placeholders = []
    for (let i = offset; i <= placeholdersCount; i++) {
        placeholders.push('$' + i)
    }
    return placeholders;
}
module.exports = makeMultiPlacehoders;