const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const db = require('../startup/db2')
const model = require('../models/interface');

class KValueHander {

    static async getValueAsync(kvKey) {
        let res = await model.aRead({
            table: 'i_kvalue',
            query: [
                ['kv_key', kvKey]
            ]
        })
        if (res.length == 0)
            return null;
        return res[0].kv_value;
    }

    static getValueSync(kvKey) {
        let res = model.sRead({
            table: 'i_kvalue',
            query: [
                ['kv_key', kvKey]
            ]
        })
        if (res.length == 0)
            return null;
        return res[0].kv_value;
    }

    static deleteSync(kvKey) {
        model.sDelete({
            table: 'i_kvalue',
            query: [
                ['kv_key', kvKey]
            ]
        });
        return;
    }

    static async serachAsync(args) {
        try {
            if (!_.has(args, 'query'))
                return ([]);

            args.table = 'i_kvalue';
            let { _query, _values } = KValueHander.prepareIt(args)
            let res = await db.aQuery(_query, _values);
            if (res.length == 0)
                return [];
            return res;
        } catch (e) {
            return (new Error(e));
        }
    }

    static prepareIt(args) {
        args.table = 'i_kvalue';
        let { _fields, _clauses, _values, _order, _limit } = db.clauseQueryGenerator(args)
        let _query = 'SELECT ' + _fields + ' FROM i_kvalue ' + _clauses + _order + _limit;
        return { _query, _values }
    }

    static serachSync(args) {
        try {
            if (!_.has(args, 'query'))
                return ([]);

            args.table = 'i_kvalue';
            let { _query, _values } = KValueHander.prepareIt(args)
            let res = db.sQuery(_query, _values);
            if (res.length == 0)
                return [];
            return res;
        } catch (e) {
            return (new Error(e));
        }
    }

    static async updateKValueAsync(key, value) {
        return await model.aUpdate({
            table: 'i_kvalue',
            query: [
                ['kv_key', key]
            ],
            updates: { kv_value: value, kv_last_modified: utils.getNow() }
        });
    }

    static updateKValueSync(key, value) {
        return model.sUpdate({
            table: 'i_kvalue',
            query: [
                ['kv_key', key]
            ],
            updates: { kv_value: value, kv_last_modified: utils.getNow() }
        });
    }

    static async upsertKValueAsync(key, value, log = true) {
        return await model.aUpsert({
            table: 'i_kvalue',
            idField: 'kv_key',
            idValue: key,
            updates: { kv_value: value, kv_last_modified: utils.getNow() },
            log: log
        });
    }

    static upsertKValueSync(key, value, log = true) {
        return model.sUpsert({
            table: 'i_kvalue',
            idField: 'kv_key',
            idValue: key,
            updates: { kv_value: value, kv_last_modified: utils.getNow() },
            log: log
        });
    }

    static async setAsync(key, value, log = true) {
        return await KValueHander.upsertKValueAsync(key, value, log);
    }

    static setSync(key, value, log = true) {
        return KValueHander.upsertKValueSync(key, value, log);
    }
}

module.exports = KValueHander;