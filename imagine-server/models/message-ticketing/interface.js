module.exports = {
    tLog: require('./update').tLog,
    iRead: require('./read').iRead,
    getTry: require('./read').getTry,
    iDel: require('./delete').iDel
}