const db = require('../../startup/db2')
const _ = require('lodash');
const getMoment = require('../../startup/singleton').instance.getMoment




module.exports = {
    iCreate: async function(args) {
        let file_id = _.has(args, 'file_id') ? args.file_id : null;
        return new Promise((resolve, reject) => {
            try {
                values = [file_id, 0, getMoment(), getMoment()]
                    //runQ
                db.query('INSERT INTO i_message_ticketing (msg_file_id, msg_try_count, msg_creation_date, msg_last_modified) VALUES ($1, $2, $3, $4) ', values, (err, res) => {
                    if (err) {
                        return reject(new Error(err));
                    }
                    return resolve(res);
                });
            } catch (err) {
                return reject(new Error(err))
            }
        });
    }
}