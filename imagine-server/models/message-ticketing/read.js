const db = require('../../startup/db2')
const _ = require('lodash');

async function iRead(fileName) {
    return new Promise((resolve, reject) => {
        db.sQuery('SELECT * FROM i_message_ticketing WHERE msg_file_id=$1', [fileName], (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res.rows);
        });
    });
}

function getTry(fileName) {
    try {
        let res = db.sQuery('SELECT msg_try_count FROM i_message_ticketing WHERE msg_file_id=$1', [fileName]);
        if (res.length > 0)
            return res[0].msg_try_count
        return 0
    } catch (e) {
        throw new Error(e)
    }
}

module.exports.iRead = iRead;
module.exports.getTry = getTry;