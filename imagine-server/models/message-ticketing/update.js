const db = require('../../startup/db2')
const _ = require('lodash')
const utils = require("../../utils/utils");
const iRead = require('./read').iRead
const iCreate = require('./create').iCreate


async function iUpdate(fileName) {
    return new Promise((resolve, reject) => {
        db.query('UPDATE i_message_ticketing SET msg_last_modified=$1, msg_try_count=msg_try_count+1 WHERE msg_file_id=$2', [utils.getNow(), fileName], (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res);
        });
    });
}

async function tLog(fileName) {
    try {
        let res = await iRead(fileName)
        if (res.length == 0) {
            res = await iCreate({
                file_id: fileName
            })
        } else {
            res = await iUpdate(fileName)
        }
    } catch (e) {
        return e
    }

}

module.exports.tLog = tLog