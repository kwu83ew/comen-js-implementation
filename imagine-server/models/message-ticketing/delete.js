const db = require('../../startup/db2')

async function iDel(fileId) {
    return new Promise((resolve, reject) => {
        db.query('DELETE FROM i_message_ticketing WHERE msg_file_id=$1', [fileId], (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res);
        });
    });
}

module.exports.iDel = iDel