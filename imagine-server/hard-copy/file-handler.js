const _ = require('lodash');
const fs = require('fs');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const config = require('config');
const clog = require('../loggers/console_logger');
const db = require('../startup/db2');
const getMoment = require('../startup/singleton').instance.getMoment
// const settings = require('./settings');
const crypto = require('../crypto/crypto');


// // console.log(__dirname);
// var encoding1 = 'binary';
// var encoding2 = 'binary';
// let buffer = new Buffer(fs.readFileSync(`${__dirname}/fb.png`, encoding1), encoding2);
// console.log('....buffer', buffer);
// let content = buffer.toString('base64');
// console.log('....content', content);

// let buffer2 = Buffer.from(content, "base64");
// fs.writeFileSync(`${__dirname}/fb2.png`, buffer2);



class FileHandler {

    static packetFileCreateSync(args) {

        let fileName = args.fileName;
        let fileContent = args.fileContent;

        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let dirName = _.has(args, 'dirName') ? args.dirName : null;
        if (dirName == null)
            return { err: true, msg: 'direname 00 null!' }

        if (appCloneId > 0)
            dirName = dirName + appCloneId.toString();

        fs.writeFileSync(dirName + '/' + fileName, fileContent, { flag: 'w' });
        return { err: false, msg: `File is created (${fileName})` }
    }

    static packetFileReadSync(args = {}) {

        let fileName = args.fileName;

        let encoding = _.has(args, 'encoding') ? args.encoding : 'utf8';
        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let dirName = _.has(args, 'dirName') ? args.dirName : null;
        if (dirName == null)
            return { err: true, msg: 'direname 01 null!' }

        if (appCloneId > 0)
            dirName = dirName + appCloneId.toString();

        let fullpath = dirName + '/' + fileName;
        let content = fs.readFileSync(fullpath, encoding);
        return { err: false, content };
    }



    // TODO: improve these 2 fCreateBinarySync, fReadBinarySync in order to be more robust and error tolorance
    static fCreateBinarySync(args) {
        let fileName = args.fileName;
        let fileContent = args.fileContent;
        let overWrite = _.has(args, 'overWrite') ? args.overWrite : false;
        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let dirName = _.has(args, 'dirName') ? args.dirName : null;
        if (dirName == null)
            return { err: true, msg: 'direname 00 null!' }
        if (appCloneId > 0)
            dirName = dirName + appCloneId.toString();
        let buffer = Buffer.from(fileContent, "base64");
        fs.writeFileSync(dirName + '/' + fileName, buffer, { flag: 'w' });
        return `File is created (${fileName})`
    }

    static fReadBinarySync(args = {}) {
        let fileName = args.fileName;
        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let dirName = _.has(args, 'dirName') ? args.dirName : null;
        if (dirName == null)
            return { err: true, msg: 'direname 01 null!' }
        if (appCloneId > 0)
            dirName = dirName + appCloneId.toString();
        let fullpath = dirName + '/' + fileName;
        let buffer = Buffer.from(fs.readFileSync(fullpath, 'binary'), 'binary');
        let content = buffer.toString('base64');
        return content;
    }


    static fCopySync(srcFileName, dstFileName) {
        // do not use it for delicate cases. copyFileSync does not work properly!
        fs.copyFileSync(srcFileName, dstFileName, (err) => {
            if (err) throw err;
            console.log('source.txt was copied to destination.txt');
        });
    }

    static deleteFileSync(args) {
        let msg;
        clog.app.info(`--- Deleting File args ${utils.stringify(args)}`);
        let fileName = args.fileName;

        let appCloneId = _.has(args, 'appCloneId') ? args.appCloneId : db.getAppCloneId();
        let dirName = _.has(args, 'dirName') ? args.dirName : null;
        if (dirName == null)
            return { err: true, msg: 'direname 01 null!' }

        if (appCloneId > 0)
            dirName = dirName + appCloneId.toString();

        let fullpath = dirName + '/' + fileName
        fs.unlinkSync(fullpath);
        return { err: false }
    }









    // email file handling wrappers
    static async eFCreateAsync(args) {
        let title = _.has(args, 'title') ? args.title : '';
        let target = _.has(args, 'target') ? args.target : '';
        let sender = _.has(args, 'sender') ? args.sender : '';
        let message = _.has(args, 'message') ? args.message : '';
        let isCustom = _.has(args, 'isCustom') ? args.isCustom : iConsts.CONSTS.NO;
        let mom = getMoment();

        if (isCustom == iConsts.CONSTS.YES) {
            message = iConsts.msgTags.customStartEnvelope + message + iConsts.msgTags.customEndEnvelope
        }

        let outbox = iConsts.HD_PATHES.outbox;
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0)
            outbox = outbox + appCloneId.toString();

        let emailBody = utils.getNow() + '\n'
        emailBody += 'unix: ' + mom.unix() + '\n'
        emailBody += 'mom.valueOf: ' + mom.valueOf() + '\n'
        emailBody += 'js: ' + new Date().getTime() + '\n'
        emailBody += 'sender: ' + sender + '\n'
        emailBody += 'target: ' + target + '\n'
        emailBody += 'message:\n' + message + '\n'
        let fileName;
        if (iConsts.isDevelopMode) {
            fileName = [utils.getNowSSS(), sender, target, title + '.txt'].join(',');
        } else {
            fileName = [utils.getNowSSS(), target, '.txt'].join(' ');
        }
        fs.writeFile(outbox + '/' + fileName, emailBody, function (err) {
            if (err) {
                console.error(err);
            }
            clog.app.info(`file (${outbox + '/' + fileName}) was created on disk Async`)
            return true
        });
    }

    // static eFCreateSync(args) {
    //     let title = _.has(args, 'title') ? args.title : '';
    //     let target = _.has(args, 'target') ? args.target : '';
    //     let sender = _.has(args, 'sender') ? args.sender : '';
    //     let message = _.has(args, 'message') ? args.message : '';
    //     let isCustom = _.has(args, 'isCustom') ? args.isCustom : iConsts.CONSTS.NO;
    //     let mom = getMoment();

    //     if (isCustom == iConsts.CONSTS.YES) {
    //         message = iConsts.msgTags.customStartEnvelope + message + iConsts.msgTags.customEndEnvelope
    //     }

    //     let outbox = iConsts.HD_PATHES.outbox;
    //     let appCloneId = db.getAppCloneId();
    //     if (appCloneId > 0)
    //         outbox = outbox + appCloneId.toString();

    //     let emailBody = utils.getNow() + '\n'
    //     emailBody += 'unix: ' + mom.unix() + '\n'
    //     emailBody += 'mom.valueOf: ' + mom.valueOf() + '\n'
    //     emailBody += 'js: ' + new Date().getTime() + '\n'
    //     emailBody += 'target: ' + target + '\n'
    //     emailBody += 'sender: ' + iConsts.msgTags.senderStartTag + sender + iConsts.msgTags.senderEndTag + '\n'
    //     emailBody += 'message:\n' + message + '\n'
    //     let fileName;
    //     if (iConsts.isDevelopMode) {
    //         fileName = [utils.getNowSSS(), sender, target, title + '.txt'].join(',');
    //     } else {
    //         fileName = [target, utils.getNowSSS(), '.txt'].join(' ');
    //     }
    //     fs.writeFileSync(outbox + '/' + fileName, emailBody, function (err) {
    //         if (err) {
    //             console.error(err);
    //         }
    //         clog.app.info(`file (${outbox + '/' + fileName}) was created on disk Sync`)
    //         return `File is created (${fileName})`
    //     });
    // }




    static async eFDeleteAsync(args = {}) {
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox;
        let appCloneId = db.getAppCloneId();
        // clog.app.info`-|||--------||||||||------- appCloneId ${appCloneId}`);
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }

        // let inbox = (config.get("dbPostfix") == '_test') ? iConsts.HD_PATHES.outbox : iConsts.HD_PATHES.inbox
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let fileId = _.has(args, 'fileId') ? args.fileId : null; // if looking for special fileId

        let files = await this.eFListAsync({
            sender: sender,
            target: target,
            fileId: fileId,
        })

        return new Promise((resolve, reject) => {
            let fullpath;
            try {
                files.forEach(file => {
                    fullpath = dirName + '/' + file.fileName
                    clog.app.info(`--- Deleting file ${fullpath}`);
                    fs.unlink(fullpath)
                });
                return resolve(null)

            } catch (e) {
                if (e.code === 'ENOENT')
                    return resolve(null);
                return reject(new Error(e))
            }

        });
    }

    static eFDeleteSync(args = {}) {
        clog.app.info(`--- Deleting e FDeleteSync args ${args}`);
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox;
        let appCloneId = db.getAppCloneId();
        // clog.app.info`-|||--------||||||||------- appCloneId ${appCloneId}`);
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }

        // let inbox = (config.get("dbPostfix") == '_test') ? iConsts.HD_PATHES.outbox : iConsts.HD_PATHES.inbox
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let fileName = _.has(args, 'fileName') ? args.fileName : null; // if looking for special fileName

        let files = this.eFListSync({
            sender: sender,
            target: target,
            fileName: fileName,
        })
        clog.app.info(JSON.stringify({
            sender: sender,
            target: target,
            fileName: fileName,
        }));

        let fullpath;
        try {
            files.forEach(file => {
                fullpath = dirName + '/' + file.fileName
                clog.app.info(`--- Deleting file ${fullpath}`);
                fs.unlinkSync(fullpath)
            });
            return (null)

        } catch (e) {
            if (e.code === 'ENOENT')
                return (null);
            return (new Error(e))
        }
    }

    static async eFListAsync(args = {}) {
        // clog.app.info(args);
        // the live system never delete outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }

        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let fileId = _.has(args, 'fileId') ? args.fileId : null; // if looking for special fileId
        let title = _.has(args, 'title') ? args.title : null; // if looking for special title

        let filesInfo = []
        let shouldPush;
        return new Promise((resolve, reject) => {
            // clog.app.info(`dirName::::::::::::::::::${dirName}`);
            fs.readdir(dirName, (err, fileNames) => {
                if (err) {
                    return reject(new Error(err));
                }
                let aFileInfo = []
                let titleINfo = []
                if (fileNames.length == 0)
                    return resolve(filesInfo);

                fileNames.forEach(fileName => {
                    // clog.app.info(fileName);
                    shouldPush = true
                    aFileInfo = fileName.split(',')
                    titleINfo = aFileInfo[3].split('.')

                    if (!utils._nilEmptyFalse(sender) && (sender != aFileInfo[1]))
                        shouldPush = false

                    if (!utils._nilEmptyFalse(target) && (target != aFileInfo[2]))
                        shouldPush = false

                    if (!utils._nilEmptyFalse(title) && (title != titleINfo[0]))
                        shouldPush = false
                    if (!utils._nilEmptyFalse(fileId) && (fileId != [aFileInfo[0], aFileInfo[1], aFileInfo[2]].join(',')))
                        shouldPush = false

                    if (shouldPush)
                        filesInfo.push({
                            creation_date: aFileInfo[0],
                            sender: aFileInfo[1],
                            target: aFileInfo[2],
                            title: titleINfo[0],
                            extension: titleINfo[1],
                            fileName: fileName
                        });
                });
                return resolve(filesInfo)
            });
        });

    }

    static eFListSync(args = {}) {
        // the live system never delete outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }

        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let lookingFileName = _.has(args, 'fileName') ? args.fileName : null; // if looking for special fileName
        let title = _.has(args, 'title') ? args.title : null; // if looking for special title

        let filesInfo = []
        let shouldPush;
        // clog.app.info(`dirName::::::::::::::::::${dirName}`);

        let files = fs.readdirSync(dirName)
        let aFileInfo = []
        let titleINfo = []

        if (files.length == 0)
            return filesInfo;


        files.forEach(aFileName => {

            shouldPush = true
            aFileInfo = aFileName.split(',');
            titleINfo = aFileInfo[3].split('.');

            if (!utils._nilEmptyFalse(sender) && (sender != aFileInfo[1]))
                shouldPush = false

            if (!utils._nilEmptyFalse(target) && (target != aFileInfo[2]))
                shouldPush = false

            if (!utils._nilEmptyFalse(title) && (title != titleINfo[0]))
                shouldPush = false
            if (!utils._nilEmptyFalse(lookingFileName) && (lookingFileName != [aFileInfo[0], aFileInfo[1], aFileInfo[2]].join(',')))
                shouldPush = false

            if (shouldPush)
                filesInfo.push({
                    creation_date: aFileInfo[0],
                    sender: aFileInfo[1],
                    target: aFileInfo[2],
                    title: titleINfo[0],
                    extension: titleINfo[1],
                    fileName: aFileName
                });
        });
        return filesInfo;
    }







    static async eFReadAsync(args = {}) {
        // clog.app.info(args);
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }
        // clog.app.info(`-|||--------||||||||------- appCloneId ${inbox}`);


        // let inbox = (config.get("dbPostfix") == '_test') ? iConsts.HD_PATHES.outbox : iConsts.HD_PATHES.inbox
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let title = _.has(args, 'title') ? args.title : null; // if looking for special title

        let orderBy = _.has(args, 'orderBy') ? args.orderBy : 'creation_date'; // if looking for special order 
        let orderByDirection = _.has(args, 'orderByDirection') ? args.orderByDirection : 'asc'; // if looking for special order Direction

        let files = await this.eFListAsync({
            sender: sender,
            target: target,
            title: title,
        })

        // clog.app.info(files);


        return new Promise((resolve, reject) => {

            if (files.length == 0)
                return resolve(null);

            files = _.orderBy(files, orderBy, orderByDirection);
            let file = files[0] // TODO: adding some limit count
            let fullpath = dirName + '/' + file.fileName
            clog.app.info(`--- Reading file ${fullpath}`);
            clog.app.info(`--xxxx- Reading file ${fullpath}`);
            fs.readFile(fullpath, 'utf-8', (err, content) => {
                if (err) {
                    return reject(new Error(err));
                }
                return resolve({
                    title: file.fileName,
                    message: content,
                    sender: file.sender,
                    fileId: [file.creation_date, file.sender, file.target].join(','),
                    creation_date: file.creation_date
                });
            });
        });
    }

    static eFReadSync(args = {}) {
        // clog.app.info(args);
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }


        // let inbox = (config.get("dbPostfix") == '_test') ? iConsts.HD_PATHES.outbox : iConsts.HD_PATHES.inbox
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let sender = _.has(args, 'sender') ? args.sender : null; // if looking for special sender
        let target = _.has(args, 'target') ? args.target : null; // if looking for special target
        let title = _.has(args, 'title') ? args.title : null; // if looking for special title

        let orderBy = _.has(args, 'orderBy') ? args.orderBy : 'creation_date'; // if looking for special order 
        let orderByDirection = _.has(args, 'orderByDirection') ? args.orderByDirection : 'asc'; // if looking for special order Direction
        // clog.app.info(`-|||--------||||||||------- appCloneId ${inbox}`);

        let files = this.eFListSync({
            sender: sender,
            target: target,
            title: title,
        })

        // clog.app.info(files);


        if (files.length == 0)
            return (null);

        files = _.orderBy(files, orderBy, orderByDirection);
        let file = files[0] // TODO: adding some limit count
        let fullpath = dirName + '/' + file.fileName
        // clog.app.info(`--- Reading file ${fullpath}`);
        // clog.app.info(`--xxxx- Reading file ${fullpath}`);
        let content = fs.readFileSync(fullpath, 'utf8');
        return {
            title: file.fileName,
            message: content,
            sender: file.sender,
            target: file.target,
            fileId: [file.creation_date, file.sender, file.target].join(','),
            creation_date: file.creation_date
        };
    }












    static async eFRemoveAsync(path = '') {

        return new Promise((resolve, reject) => {
            fs.readdir(path, (err, files) => {
                if (err) {
                    return reject(new Error(err));
                }

                try {
                    files.forEach(file => {
                        fullpath = path + '/' + file
                        fs.unlink(fullpath)
                    });
                    return resolve(true)

                } catch (e) {
                    if (e.code === 'ENOENT')
                        return resolve(null);
                    return reject(new Error(e))
                }

            });

        });
    }

    static eFRemoveSync(path = '') {
        let files = fs.readdirSync(path);
        try {
            files.forEach(file => {
                fullpath = path + '/' + file
                fs.unlinkSync(fullpath)
            });
            return (true)

        } catch (e) {
            if (e.code === 'ENOENT')
                return (null);
            return (new Error(e))
        }

    }





}

FileHandler.emailFileHandler = require('./message-utils.js');
module.exports = FileHandler;
