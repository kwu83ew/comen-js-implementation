const _ = require('lodash');
const fs = require('fs');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const config = require('config');
const clog = require('../loggers/console_logger');
const db = require('../startup/db2');
const crypto = require('../crypto/crypto');


class MessageFileUtils {

    /**
     * email handler functions
     */


    static writeEmailAsFile(args) {
        try {
            clog.app.info(`write Em File args: ${utils.stringify(args)}`);
            let title = _.has(args, 'title') ? args.title : '';
            let target = _.has(args, 'target') ? args.target : '';
            let sender = _.has(args, 'sender') ? args.sender : '';
            let message = _.has(args, 'message') ? args.message : '';
            let isCustom = _.has(args, 'isCustom') ? args.isCustom : iConsts.CONSTS.NO;

            if (isCustom == iConsts.CONSTS.YES) {
                message = iConsts.msgTags.customStartEnvelope + message + iConsts.msgTags.customEndEnvelope
            }

            let outbox = iConsts.HD_PATHES.outbox;
            let appCloneId = db.getAppCloneId();
            if (appCloneId > 0)
                outbox = outbox + appCloneId.toString();

            let emailBody = utils.getNow() + '\n';
            emailBody += 'time: ' + utils.getNowSSS() + '\n';
            emailBody += iConsts.msgTags.senderStartTag + sender + iConsts.msgTags.senderEndTag + '\n';
            emailBody += iConsts.msgTags.receiverStartTag + target + iConsts.msgTags.receiverEndTag + '\n';
            emailBody += message + '\n';
            let hash = utils.hash16c(crypto.keccak256(`${sender}${target}${message}`));
            emailBody += iConsts.msgTags.hashStartTag + hash + iConsts.msgTags.hashEndTag + '\n';
            clog.app.info(`emailBody: ${utils.stringify(emailBody)}`);
            let fileName;
            if (iConsts.isDevelopMode) {
                fileName = [utils.getNowSSS(), sender, target, title + '.txt'].join(',');
            } else {
                fileName = [target, utils.getNowSSS(), (Math.random() * 10000).toString(), '.txt'].join(' ');
            }
            clog.app.info(`file Name: ${utils.stringify(fileName)}`);
            clog.app.info(`Try to write1: ${utils.stringify(outbox + '/' + fileName)}`);
            fs.writeFileSync(outbox + '/' + fileName, emailBody, { flag: 'w' });
            return { err: false, msg: `File is created (${fileName})`, richEmail: emailBody, hashHandle: hash }

        } catch (e) {
            console.log(e);
            return { err: true, msg: e }
        }
    }


    static readEmailFile(args = {}) {
        let msg;
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox;
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString();
        }
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let files = fs.readdirSync(dirName);
        if (files.length == 0)
            return {
                err: false,
                secIssue: false,
                thereIsPacketToParse: false,
                msg: 'There is no file to read',
                fileName: '',
                sender: '',
                receiver: '',
                message: '',
            }
        if (!_.has(MessageFileUtils, 'readedFiles'))
            MessageFileUtils.readedFiles = [];
        clog.app.info(`MessageFileUtils.readedFiles: ${MessageFileUtils.readedFiles}`);
        let fileName = null;
        for (let aFile of files) {
            if (fileName == null) {
                if (MessageFileUtils.readedFiles.includes(aFile))
                    continue;
                MessageFileUtils.readedFiles.push(aFile);
                fileName = aFile;
            }
        }
        if (fileName == null)
            return {
                err: false,
                secIssue: false,
                thereIsPacketToParse: false,
                msg: 'There is no file to read',
                fileName: '',
                sender: '',
                receiver: '',
                message: '',
            }

        let fullpath = dirName + '/' + fileName;
        // clog.app.info(`--- Reading file ${fullpath}`);
        // clog.app.info(`--xxxx- Reading file ${fullpath}`);
        // console.log(`--xxxx- Reading file ${fullpath}`);
        let content = fs.readFileSync(fullpath, 'utf8');
        // console.log('content', content);
        if (utils._nilEmptyFalse(content)) {
            return {
                err: true,
                thereIsPacketToParse: true,
                secIssue: false,
                msg: 'File has no content',
                fileName,
                sender: '',
                receiver: '',
                message: '',
            }
        }
        if (
            content.includes(iConsts.msgTags.senderStartTag) &&
            content.includes(iConsts.msgTags.senderEndTag) &&
            content.includes(iConsts.msgTags.receiverStartTag) &&
            content.includes(iConsts.msgTags.receiverEndTag) &&
            content.includes(iConsts.msgTags.iPGPStartEnvelope) &&
            content.includes(iConsts.msgTags.iPGPEndEnvelope)
        ) {
            let sender = content.split(iConsts.msgTags.senderStartTag)[1].split(iConsts.msgTags.senderEndTag)[0];
            let receiver = content.split(iConsts.msgTags.receiverStartTag)[1].split(iConsts.msgTags.receiverEndTag)[0];
            let pureContent = content.split(iConsts.msgTags.iPGPStartEnvelope)[1].split(iConsts.msgTags.iPGPEndEnvelope)[0];
            if (!utils._nilEmptyFalse(pureContent))
                pureContent = utils.stripBR(pureContent);

            return {
                err: false,
                secIssue: false,
                thereIsPacketToParse: true,
                fileName,
                sender,
                receiver,
                message: pureContent,
                // creation_date: file.creation_date
            };
        } else {
            // delete invalid message
            msg = `received invalid msg which missed either sender, receiver or iPGP tag`;
            clog.app.info(msg);
            fs.unlinkSync(fullpath);
            return {
                err: true,
                thereIsPacketToParse: true,
                secIssue: false,
                msg,
                fileName
                // creation_date: file.creation_date
            };
        }
    }

    static fileList(args) {
        let msg;
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox;
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString();
        }
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;

        let files = fs.readdirSync(dirName);
        return files;
    }


    static deleteEmailFile(args = {}) {
        let msg;
        clog.app.info(`--- Deleting Em File args ${utils.stringify(args)}`);
        // the live system never delet outbox, instead can delete inbox after parsing
        let inbox = iConsts.HD_PATHES.inbox;
        let appCloneId = db.getAppCloneId();
        if (appCloneId > 0) {
            inbox = inbox + appCloneId.toString()
        }
        let dirName = _.has(args, 'dirName') ? args.dirName : inbox;
        let fileName = _.has(args, 'fileName') ? args.fileName : '';
        if (utils._nilEmptyFalse(fileName)) {
            msg = `can not delete null file name`;
            clog.app.error(msg);
            return { err: true, msg }
        }
        let fullpath = dirName + '/' + fileName
        fs.unlinkSync(fullpath);
        return { err: false }
    }

}
module.exports = MessageFileUtils;
