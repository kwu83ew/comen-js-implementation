
let cancell_transaction = {

    dType: iConsts.DOC_TYPES.cancellTrx,


    // the version indicates how to treat/validate this transaction
    ver: "0.0.0",


    doubleSpents: [

        // the place of spending a unique input
        {
            block: null, // block hash of corresponding transaction
            tx: null, // transaction hash in which the user spent input

        },

        // the another place of spending a unique input
        {
            block: null, // block hash of corresponding transaction
            tx: null, // transaction hash in which the user spent input

        },

        // if there are more places of spending a unique input ...
    ],

};

// the nodes by finding this kind of transactions, put cancelled transaction (more acuratley the outputs) in cancelled transactions list 
// to avoid considering the outputs as a valid outputs. these are all because the DAG crawling starts from end to begin. 
// so in such a way the node recognizes the cancelled transactions before head.
// also the "48 hour delay for input spending the outputs" apply implicitly by recording the cancell transaction in blockchain 
// 48 hour limitation starts from cancell transaction-creationdate