"uSets": [
    {
        "sType": "Basic",
        "sVer": "0.0.0",
        "sSets": [
            {
                "sKey": "0258a47cd232af7b34ffe2befa31dd70c0d45e4aed3334bec19a6f1aa59e2e419a"
            },
            {
                "sKey": "025d0c30bfdc7704fdec4eacc4b6a2cf9e164abc224366ab26e711f6812894af9f"
            }
        ],
        "proofs": [
            "r.6178689b4097793097774c520849251a5524928e5ff707fe995a9c66c8f1f7f0",
            "r.53657d6c15d6d38cf4ac3e92b08c01481b6875ff5a9d7a60f2297410c0b432c2"
        ],
        "lHash": null,
        "salt": "74231c1464f972d3"
    },
    {
        "sType": "Basic",
        "sVer": "0.0.0",
        "sSets": [
            {
                "sKey": "02502fb11d32aee926e8055b77a9aee57fa9c03cd00d2d09cb81a364b6b35a3ae6"
            },
            {
                "sKey": "0258a47cd232af7b34ffe2befa31dd70c0d45e4aed3334bec19a6f1aa59e2e419a"
            }
        ],
        "proofs": [
            "r.53657d6c15d6d38cf4ac3e92b08c01481b6875ff5a9d7a60f2297410c0b432c2"
        ],
        "lHash": "5dfb4e8a38097574fe859548847e4d3a2e3429d2d477f0f2be68e7303603ff33",
        "salt": "6f8c7a169a1c6e4e"
    },
    {
        "sType": "Basic",
        "sVer": "0.0.0",
        "sSets": [
            {
                "sKey": "025d0c30bfdc7704fdec4eacc4b6a2cf9e164abc224366ab26e711f6812894af9f"
            },
            {
                "sKey": "02502fb11d32aee926e8055b77a9aee57fa9c03cd00d2d09cb81a364b6b35a3ae6"
            }
        ],
        "proofs": [
            "l.66f1871688eaa3206bdafc1494bb1f27e9eb8d00c823f9ebb592caa0ad108f92"
        ],
        "lHash": "1a6737604c37b05106481f191c66277821d11ed9abf2605b653872ce10e499d8",
        "salt": "67fb3f7a197df98e"
    }
],