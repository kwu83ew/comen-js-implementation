
// module.exports.pay4pay1 = {
//     // the transaction hash is a truncated double sha256 of partial part of transaction.
//     // the parts which are participate in this hash are all inputs & outputs and their's value, ref

//     // it is used as a reference of this transaction to spend the transaction's output.
//     hash: null,

//     //seq: maybe sequence id?


//     dType: iConsts.DOC_TYPES.BasicTx,
//     dClass: iConsts.TRX_CLASSES.P4P,

//     // the version indicates how to treat/validate this transaction
//     dVer: "0.0.0",

//     // an obligatori field, which is referr to transaction we want to pay for
//     // this ref must be a transaction-hash
//     ref: null,

//     //arbitrary text message, tag, key, or wahtever... (limited to max block size e.g. 10 MB)
//     desc: null,

//     // the creation date & time of transaction. it will be part of trx-root-hash
//     // the time is pasific time and not machin local time.
//     // this value initialized by wallet
//     creationDate: 19287364632, // int value equal to "2019-01-12 18:25:36"

//     // each transaction can have 1 or many inputs
//     // who creates this transaction, for each input must sign transaction (whole or partially)
//     inputs: [{
//             trx: "p0sferfrtrwtewiruieybcriurq", // transaction hash
//             inx: "0", // output index


//         },
//         {
//             trx: "ligpopoioioiooop",
//             inx: "7",
//             // The transaction signer must sign this transaction (whole or partially) with corresponding private key of this public key
//             // to prove of ownership of this input and signing for new owner
//             pub: "02fcffd079e18bc057f798f59e2922fd223c7b80c98e5f47c9527005662f942e1f",
//         }
//     ],

//     // if some cloned transaction exist, they must be listed here to force it as spent outputs
//     clonedInputs: [
//         {},
//     ],


//     // each transaction can have at least two or more outputs
//     // every transaction has cost and will be payed based on entire serialized transaction by byte. 
//     // it is for every system mantainers to incentivised them to support the system
//     // the creation date of a transaction implicity calculated by block creation time
//     outputs: [{
//             address: "im1xpjkyet9xgunxcn9xpnrsdp4xcergwp5xd3rzdtrvy6nxwf3xdsnquxr2e6", // bech32 format address. payee 1
//             value: 3.401 // value by pai
//         },
//         {
//             address: "im1xpjkyet9xgunxcn9xpnrsdp4xcergwp5xd3rzdtrvy6nxwf3xdsnquxr2e6", // bech32 format address payee 2
//             value: 74.5069 // value by pai
//         },
//         {
//             address: "miner Address", // bech32 format address, this address wil be used to calculate the memory-sharing dividend
//             value: 0.801 // value by pai. it is the transaction fee and every transaction has to have this value explicitly. it can not be less than certain value
//         },

//     ]

// };