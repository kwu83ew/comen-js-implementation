{
    "merkleVersion": "0.0.0",
    "merkleRoot": "d22eb74633170abe5a85277ee4f3296c610cab8337b4782323136d600d3dbec3",
    "accountAddress": "im1xpjnzdtp8quxvces89jrgvp3vcenjwfjvdjx2ctpxdjkxwf5xe3nw0qk6wn",
    "uSets": [
        {
            "sSets": [
                {
                    sKey:"03e184499b27757adc3a4ff1a44ba345f882e7a3bd95c8a4789d1446f2f141b6e4",
                    iTLock:"0",
                    "im1aajnzdtp8quxvces89jrgvp3vcenjwfjvdjx2ctpxdkhjif5xe3nw0qffdev"
                },
                [
                    "0269b64756ac72332ecf31a0eaef36e07e5571ee6ee74564d690919bc3b411cfb2",
                    "0"
                ]
            ],
            "proofs": [
                "r.2439efa0c626c3d7f052f5042ee73e8d443b1ea794b41c7b4b2c63118056e551",
                "r.12201aeb4a79f89d8ef98299faa5f3e880c472a43e483e9c3e68bbf05626ab9d"
            ],
            "lHash": null
        },
        {
            "sSets": [
                [
                    "032c3c1148c67d09b75197ec6d1732abd29a77960bc1a1995d4fd989c93c639b5b",
                    "0"
                ],
                [
                    "03e184499b27757adc3a4ff1a44ba345f882e7a3bd95c8a4789d1446f2f141b6e4",
                    "0"
                ]
            ],
            "proofs": [
                "r.12201aeb4a79f89d8ef98299faa5f3e880c472a43e483e9c3e68bbf05626ab9d"
            ],
            "lHash": "77c6fa1d4a5e280c6b949207016d03ab848e9484161d84e43f66c53f695e51d9"
        },
        {
            "sSets": [
                [
                    "0269b64756ac72332ecf31a0eaef36e07e5571ee6ee74564d690919bc3b411cfb2",
                    "0"
                ],
                [
                    "032c3c1148c67d09b75197ec6d1732abd29a77960bc1a1995d4fd989c93c639b5b",
                    "0"
                ]
            ],
            "proofs": [
                "l.fd836caf4521429c2b6ace16c7fbe4055c2b8c4c4de2f983d79599b5f26b7e28"
            ],
            "lHash": "c8fe6748992f12dc97b8a95aef4fc5ad278afd4be250511e1dae278706ff14a9"
        }
    ]
}
