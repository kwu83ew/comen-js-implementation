// redeem structure is nothing than a json object in which the redeem clauses structered
// payee creates this structure and after double sha256 creates a bech32 address
// in such a way the other will send money to this address
// when the address owner wants to spend the money she/he must send this structure alongside her/his signed transaction by corresponding private-key
// in each node, machine ri-generates the double-sha256 & bech32 to prove the ownership of inputs.
// also node uses the public-key to validate signed transaction in which transport the value to new person.

module.exports.simpleUnlock = {
    type: "simple", // type mofn simple
    ver: "0.0.0",
    pubKey: "02f148535c489b798a467f4d6fc924ce7c276f904d6e5f9c380d2829ebcb0a691d",
};

module.exports.mofnUnlock = {
    type: "mofn",
    ver: "0.0.0",
    m: "2",
    n: "3",
    signers: [{
            pubKey: "02f148535c489b798a467f4d6fc924ce7c276f904d6e5f9c380d2829ebcb0a691d", // Alice
        },
        {
            pubKey: "020e1a5ef507d8a08edb91085975c24d3ae3bf860e1e670dd1e1d5636895b8202c", // Bob
        },
        {
            pubKey: "0264ff31759417a58672427b8b41997bfd5b93e800018d46fbbe85334d5994f446", // Ken as a
            // Redeem Time by minutes. it meens after recordin transaction in blockchain, if Ken want to spend this money he has to wait t minutes 
            // to be able to sign and spend this tokens. (this redeemTime do not applay to after singing by Ken)
            // BTW redeemTime can not be less than 12 hour(which is normal delay time to be spendable a found)
            redeemTime: ""
        },
    ]
};


module.exports.simpleUnlock_privateInfo = {
    prvKey: "a5800109ab8ddd046b62605b1f49649798821300fc0552a7b606388615397e26",
    prvKey: "6017e8a6541c1466a4970733f00868f1abdc154f6e2337f54c511b2a18123ca1",
    prvKey: "a00551a974beb55196cfa5783068e936e38ccbee21f00497ec1b4b208c452142",
};