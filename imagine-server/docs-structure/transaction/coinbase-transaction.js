const _ = require('lodash');
const iConsts = require('../../config/constants');

let coinbaseTemplate = {
    hash: "88474a56242abe2b901e29e6440ab9903ce733f581ffdaaabf3ac7e11c156642",
    dType: iConsts.DOC_TYPES.Coinbase,
    dVer: "0.0.0",
    cycle: "", // 'yyyy-mm-dd am' / 'yyyy-mm-dd pm'
    treasuryFrom: "", // incomes from date
    treasuryTo: "", // incomes to date
    treasuryIncomes: 0, // incomes value
    mintedCoins: 0,
    outputs: []
};

function getCoinbaseTemplate() {
    let trx = _.cloneDeep(coinbaseTemplate);
    return trx;
}


module.exports.coinbaseTemplate = coinbaseTemplate;
module.exports.getCoinbaseTemplate = getCoinbaseTemplate;