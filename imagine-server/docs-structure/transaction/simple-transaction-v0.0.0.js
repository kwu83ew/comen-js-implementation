const iConsts = require('../../config/constants');

module.exports.t1 = {

    // the transaction hash is a truncated keccak of partial part of transaction.
    // the parts which are participate in this hash are all inputs & outputs and their's value, ref


    // to generate trx hash call this method extractTransactionHashableParts
    // it generates based on definitiveHash and some otherFields in transaction.
    // it is used as a reference of this transaction to spend the transaction's output.
    hash: null,

    //seq: maybe sequence id?


    // type could be also wiki, SimpleNameService (SNS), StaticSmartContract(SSC), DistributedVersionControl(DVC)...
    dType: iConsts.DOC_TYPES.BasicTx,

    // the version indicates how to treat/validate this transaction
    dVer: "0.0.0",

    // an optional field
    // if this transaction is used to pay fee for sending in wiki or registering a name
    // then the hash of refered docsument must be inserted here
    // this ref must be a part of block-root-hash
    ref: null,

    //arbitrary text message, tag, key, or wahtever... (limited to max block size e.g. 10 MB)
    desc: null,

    // the creation date & time of transaction. it will be part of trx-root-hash
    // the time is pasific time and not machin local time.
    // this value initialized by wallet
    creationDate: 19287364632, // int value equal to "2019-01-12 18:25:36"

    // each transaction can have 1 or many inputs
    // how ceates this transaction, for each input must sign transaction (whole or partially)
    inputs: [{
            trx: "p0sferfrtrwtewiruieybcriurq", // transaction hash
            inx: "0", // output index


        },
        {
            trx: "ligpopoioioiooop",
            inx: "7",
            // The transaction signer must sign this transaction (whole or partially) with corresponding private key of this public key
            // to prove of ownership of this input and signing for new owner
            pub: "02fcffd079e18bc057f798f59e2922fd223c7b80c98e5f47c9527005662f942e1f",
        }
    ],

    // if some cloned transaction exist, they must be listed here to force it as spent outputs
    clonedInputs: [
        {},
    ],


    // each transaction must have at least one output to pay trx-fee
    // every transaction has cost and will be payed based on entire serialized transaction by byte. 
    // it is for every system mantainers to incentivised them to support the system
    // the creation date of a transaction implicity calculated by block creation time
    outputs: [{
            address: "im1xpjkyet9xgunxcn9xpnrsdp4xcergwp5xd3rzdtrvy6nxwf3xdsnquxr2e6", // bech32 format address. payee 1
            value: 3401 // value by pai
        },
        {
            address: "im1xpjkyet9xgunxcn9xpnrsdp4xcergwp5xd3rzdtrvy6nxwf3xdsnquxr2e6", // bech32 format address payee 2
            value: 75069 // value by pai
        },
        {
            address: "miner Address", // bech32 format address, this address wil be used to calculate the memory-sharing dividend
            value: 801 // value by pai. it is the transaction fee and every transaction has to have this value explicitly. it can not be less than certain value
        },

    ],

    dExtInfo: [],
    dExtHash: ""

};


module.exports.t1_dExtInfo = {
    inputs: [
        // each input must have one member in this array. this member is an array with atleast one member

        // the unlock issue related to input with index 0
        [
            // it is possible one input need more than one sign to be valid (e.g. m of n signature), so here ther are the different signatures.
            {
                // The transaction signer must sign this transaction (whole or partially) with corresponding private key of this public key
                // to prove of ownership of this input. also this public key must be valid in unlock Structure. 
                // and finally it uses to confirmation of signed output to new owner
                sigHash: null, // which part(s) of transaction is signed. it could be all/none/single in combination of anyoneCanPay modifier (same as current bitcoin core)
                signature: null, // the signature
                pubKey: "0216fb2cd919798e15e7ba27de6ada6ba5b22e50235dd9f0dcfbf47b3c53f3aa5c",
                uSet: {}, // depends on transaction type each one has a different unlock-structure and different treatment in node
            },

            {
                // second signature for input index 0
            },
        ],

        // the unlock issue related to input with index 1

    ]
};

module.exports.t1_creator_info = {
    "input0_privateKey": "69a5564f9a5fe9c38c180754e1b3e5da74adca182707d88cf71e458cd69363c0",
    "input1_privateKey": "b8b4bdf44c21c1a7ebe169e317175f71856b1d467890acc3f2a518f65fe99681",
};