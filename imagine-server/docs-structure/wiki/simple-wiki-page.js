// let wiki = {
//     subType: null,
//     // ref  is a reference to a transaction in which user paid for sending this content to imagine blockchain.
//     // this payment depends on life time of document. lets say the price of kilobyte per year kb/y is 1 pai.
//     // so for a wiki page with size 2 kb, the author have to pay 2 pai for one year (one year is minimum payment)
//     ref: null,
//     // it is time to live a doc on blockchain and presented by year. after expire the ttl its up to nodes to keep doc or drop it.
//     // BTW user always can renew ttl a doc. and if you run a full node on your machin, you can keep it for ever.
//     ttl: null,

//     creationDate: 2344234, // the date by second

//     // the content type is html, so you can put everything, even external links
//     content: "",

//     // accessibility to document provided by it's hash. it means entire document will be convert to an string then after double sha256 and after bech32
//     // and make an immutable link like imagine/iw1k8jh75hf48jfshd48few8h4y  note to prefix iw
//     // you can link wiki pages by useing this links.
//     link: "iw1k8jh75hf48jfshd48few8h4y", // this is create automatically during the record doc in blockchain



// }