const _ = require('lodash');
const iutils = require("../../utils/iutils");
const coinbaseBlockVersion0 = require("./coinbase-block-v-0.0.0");
const blockVersion0 = require("./normal-block-v-0.0.0");
const susBlockVersion0 = require("./sus-block-v-0.0.0");
class BlockTplHandler {

    static getCoinbaseBlockTemplate() {
        let block = _.cloneDeep(coinbaseBlockVersion0);
        block.signals = iutils.getMachineSignals();
        block.docs = []
        return block;
    }

    static getNormalBlockTemplate() {
        let block = _.cloneDeep(blockVersion0);
        block.signals = iutils.getMachineSignals();
        block = this.nullBlock(block)
        return block;
    }

    static getSusBlockTemplate() {
        let block = _.cloneDeep(susBlockVersion0);
        block.signals = iutils.getMachineSignals();
        block = this.nullBlock(block)
        return block;
    }

    static nullBlock(block) {
        block.docs = []
        block.bExtInfo = []
        return block;
    }
}



module.exports = BlockTplHandler;