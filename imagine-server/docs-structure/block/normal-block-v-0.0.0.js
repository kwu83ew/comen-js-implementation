const iConsts = require('../../config/constants');

module.exports = {

    // coud be imagine test net (it) or main net (im)
    net: "im",

    bVer: "0.0.0",
    bType: iConsts.BLOCK_TYPES.Normal,
    blockLength: "0000000", // seialized block size by byte (char). this number is also a part of block root hash

    // miner trx-fee address is implicitly is in coinbase transaction, the email address of node or it's public-key I am not sure it is usefull or not
    // or even compromise some un-necessary informations for adversary
    // miners publick key, has many uses such as open-pgp comunicating to other miners via email to broadcasting the block
    // minerKey: "02e90f689391564243f22082996b75e41ba832733fb25810c576d0cf1cb83f1b54", // dab5b531d6366b298d143175bf47b2ddf671813bd493c9b32dc235102a77a596

    // root hash of all doc hashes 
    // it is maked based on merkle tree root of transactions, segwits, wikis, SNSs, SSCs, DVCs, ...
    blockHash: "0000000000000000000000000000000000000000000000000000000000000000",

    // a list of ancestors blocks, these ancestor's hash also recorded in block root hash
    // if a block linked to an ancestors block, it must not liks to the father of that block
    // a <--- b <---- c
    // if the block linked to b, it must not link to a 
    // the new block must be linked to as many possible as previous blocks (leave blocks)
    // maybe some discount depends on how many block you linked as an ancester!
    // when a new block linke t another block, it MUST not linked to that's block ancester
    ancestors: [],

    // a list of coming feature to signaling whether the node supports or not
    signals: [], // e.g. ["mimblewimble", "taproot", "schnorrMusic", "outputTimelockActivated"]

    fVotes: [], // floating votes(e.g. coinbase confirmations, susVotes, cloneTransaction...)

    // the address which is used to collect transaction-fees, 
    // NOTE: every transaction(except p4p transactions which has 2 or more backer addresses) MUST have backer-address as an output, 
    // and also backer-fee for each backer-address
    backer: "",

    // creation time timestamp, it is a part of block root-hash
    // it also used to calculate spendabality of an output. each output is not spendable befor passing 12 hours of creation time.
    // creation date must be greater than all it's ancesters
    creationDate: "",

    // note in imagine the transaction feea to miner is already written in each transaction
    // and the block generator uses an special transaction to gatter all this outputs and send back to himself (70%).
    // and send (30%) to Treasury to avoid spaming the network and redistribute these Pais between all nodes in last 12 month 
    // note: this transaction is the only transaction which uses the un-maturated outputs. 
    // BTW the output of this transaction will be spendable after 12 hours (like the other transactions)
    bExtInfo: [],
    bExtHash: "",

    // it is possible, same transaction registered in two or more diffrent bloacks by diffrent miners.
    // in fact the wallet can ask to register transaction by more miners (so she/he pays seperate fee for each miner)
    // in each node the blocks receiving in different order and doesn-t matter.
    // as soon as a node discover there are some cloned trx, updates the clonedTransactions in proper blocks.
    // clonedTransactions is not a part of block-root-hash and it must be created in each local node by its own
    // by knowing the clone, when user spends an output, 
    // the node have to create a transaction and includes the output which user intended and also all related clone transactions 
    // clonedTransactionsRootHash: "",
    // clonedTransactions: [],



    docsRootHash: "", // the hash root of merkle tree of transactions
    docs: [],

};