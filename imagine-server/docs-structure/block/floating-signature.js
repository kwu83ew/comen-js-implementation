const _ = require('lodash');
const iConsts = require('../../config/constants');
const iutils = require("../../utils/iutils");

const floatingSignatureVersion0 = {

    // coud be imagine test net (it) or main net (im)
    net: "im",

    bVer: "0.0.0",
    bType: iConsts.BLOCK_TYPES.FSign,
    descriptions: null,
    // coud be imagine test net (it) or main net (im)
    blockLength: "0000000", // seialized block size by byte (char). this number is also a part of block root hash

    // root hash of all doc hashes 
    // it is maked based on merkle tree root of transactions, segwits, wikis, SNSs, SSCs, DVCs, ...
    blockHash: "0000000000000000000000000000000000000000000000000000000000000000",
    confidence: 0.0, // dentos to signer/backer's shares by percent on signing time

    ancestors: [], // floating signatures MUST be linked to only one ancestor to whom which are signed

    // the structure in which contain signature of shareholders(which are backers too) and 
    // by their sign, they confirm the value of shares & DAG screen-shoot on that time.
    // later this confirmation will be used in validating the existance of a "sus-block" at the time of that cycle of coinbase
    // bExtInfo.signature: {}, // there must be only one signature of block backer/signer. it contains also backerAddress(the backer/signer address in which has DNA shares)

    // a list of ancestors blocks, these ancestor's hash also recorded in block root hash
    // if a block linked to an ancestors block, it must not liks to the father of that block
    // a <--- b <---- c
    // if the block linked to b, it must not link to a 
    // the new block must be linked to as many possible as previous blocks (leave blocks)
    // maybe some discount depends on how many block you linked as an ancester!
    // when a new block linke t another block, it MUST not linked to that's block ancester


    // a list of coming feature to signaling whether the node supports or not
    signals: [], // e.g. "mimblewimble,taproot"

    // creation time timestamp, it is a part of block root-hash
    // it also used to calculate spendabality of an output. each output is not spendable befor passing 12 hours of creation time.
    // creation date must be greater than all it's ancesters
    creationDate: "", // 'yyyy-mm-dd hh:mm:ss'


};

function getFloatingSignatureTemplate() {
    let block = _.cloneDeep(floatingSignatureVersion0);
    block.signals = iutils.getMachineSignals();
    return block;
}


module.exports = getFloatingSignatureTemplate;