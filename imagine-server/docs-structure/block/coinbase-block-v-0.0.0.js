const _ = require('lodash');
const iConsts = require('../../config/constants');
var basicTx = require('../transaction/simple-transaction-v0.0.0');
const iutils = require("../../utils/iutils");

let coinbaseBlockVersion0 = {

    // coud be imagine test net (it) or main net (im)
    net: "im",

    bVer: "0.0.0",
    bType: iConsts.BLOCK_TYPES.Coinbase,
    descriptions: null,
    // coud be imagine test net (it) or main net (im)
    blockLength: "0000000", // seialized block size by byte (char). this number is also a part of block root hash
    confidence: 0.0, // aggregating of all floating signatures which are created in previous cycle and linked to previous coinbase block

    // miner trx-fee address is implicitly is in coinbase transaction, the email address of node or it's public-key I am not sure it is usefull or not
    // or even compromise some un-necessary informations for adversary
    // miners publick key, has many uses such as open-pgp comunicating to other miners via email to broadcasting the block
    // minerKey: "02e90f689391564243f22082996b75e41ba832733fb25810c576d0cf1cb83f1b54", // dab5b531d6366b298d143175bf47b2ddf671813bd493c9b32dc235102a77a596

    // root hash of all doc hashes 
    // it is maked based on merkle tree root of transactions, segwits, wikis, SNSs, SSCs, DVCs, ...
    blockHash: null,

    // the structure in which contain signature of shareholders(which are backers too) and 
    // by their sign, they confirm the value of shares & DAG screen-shoot on that time.
    // later this confirmation will be used in validating the existance of a "sus-block" at the time of that cycle of coinbase

    // a list of ancestors blocks, these ancestor's hash also recorded in block root hash
    // if a block linked to an ancestors block, it must not liks to the father of that block
    // a <--- b <---- c
    // if the block linked to b, it must not link to a 
    // the new block must be linked to as many possible as previous blocks (leave blocks)
    // maybe some discount depends on how many block you linked as an ancester!
    // when a new block linke t another block, it MUST not linked to that's block ancester

    ancestors: [],

    // a list of descendent blocks. this list can not be exist when the node receive block.
    // the node update these information whenever receive new childs blocks. and in sequence node send this information ( if exist)

    // a list of coming feature to signaling whether the node supports or not
    signals: [], // e.g. ["mimblewimble", "taproot"]

    // creation time timestamp, it is a part of block root-hash
    // it also used to calculate spendabality of an output. each output is not spendable befor passing 12 hours of creation time.
    // creation date must be greater than all it's ancesters
    creationDate: "", // 'yyyy-mm-dd hh:mm:ss'

    // the time in which block is received in node, this time is valid only on local node and not published
    // receiveDate: null,


    docsRootHash: "", //the hash root of merkle tree of transactions
    docs: [
        basicTx.t1 // transactions fee agregator
    ],


};


module.exports = coinbaseBlockVersion0;