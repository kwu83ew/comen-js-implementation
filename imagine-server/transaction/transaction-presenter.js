const _ = require('lodash');
const utils = require("../utils/utils");
const iutils = require("../utils/iutils");
const utxoHandler = require("../transaction/utxo/utxo-handler");


function transactionPresenter(trx, indetns = '') {
    let out = '';
    // clog.app.info(trx);
    out += '\n' + indetns + 'Type: ' + trx.dType + ' \tVersion: ' + trx.version + ' \tLength: ' + trx.length;
    if (trx.description != '')
        out += '\n' + indetns + 'Description: ' + trx.description;
    out += '\n' + indetns + 'Creation Date: ' + trx.creationDate + ' \tReference: ' + trx.ref;
    out += '\n' + indetns + 'Hash: ' + trx.hash
    let inx = 0;
    if (_.has(trx, 'inputs') && !utils._nilEmptyFalse(trx.inputs)) {
        out += '\n' + indetns + 'Inputs: ' + trx.inputs.length
        for (let i = 0; i < trx.inputs.length; i++) {
            out += `\n\t${indetns}${i}. ${iutils.packCoinRef(trx.inputs[i][0], trx.inputs[i][1])}`;
            if (_.has(trx, 'segwits')) {
                out += segwitsPresenterForOneInput(trx.segwits[i]);

            }
            out += `\n`;
        }

    }

    if (_.has(trx, 'outputs')) {
        inx = 0;
        out += '\n' + indetns + 'Outputs: ' + trx.outputs.length
        trx.outputs.forEach(anOutput => {
            out += '\n\t' + indetns + inx + '. ' + anOutput[0] + '\t' + _.padStart(utils.microPAIToPAI(anOutput[1]), 15, ' ');
            inx += 1;
        });
    }
    return out;

}

function segwitsPresenterForOneInput(segwits) {
    let out = `\n\tSigners: ${segwitsuSets.sSets.length}`;
    segwitsuSets.sSets.forEach(aRedeem => {
        out += `\n\t\tPublicKey + RedeemTime: ${aRedeem[0]}`;
    });
    out += `\n\tProofs: ${segwitsuSets.proofs.length}`;
    segwitsuSets.proofs.forEach(aProof => {
        out += `\n\t\t${aProof}`;
    });
    out += `\n\tLeft Hash: ${segwitsuSets.lHash}`;
    out += `\n\tSignatures: ${segwits.signatures.length}`;
    segwits.signatures.forEach(aSig => {
        out += `\n\t\t${aSig[0]}\t${aSig[1]}`;
    });
    return out;
}

module.exports.transactionPresenter = transactionPresenter;
module.exports.segwitsPresenterForOneInput = segwitsPresenterForOneInput;
