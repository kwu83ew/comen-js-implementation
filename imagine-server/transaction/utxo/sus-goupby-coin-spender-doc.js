const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');


class CoinSpenderGroupper {

    static doGroupByCoinAndSpender(args) {
        let invokedDocHash = args.invokedDocHash;
        let coinsAndVotersDict = args.coinsAndVotersDict;
        let doDBLog = _.has(args, 'doDBLog') ? args.doDBLog : false;

        let coinsAndOrderedSpendersDict = {};
        let coins = utils.objKeys(coinsAndVotersDict);
        for (let aCoin of coins) {
            if (!_.has(coinsAndOrderedSpendersDict, aCoin))
                coinsAndOrderedSpendersDict[aCoin] = [];

            for (let voter of utils.objKeys(coinsAndVotersDict[aCoin])) {

                let aCoinVoter = coinsAndVotersDict[aCoin][voter];

                let orderedKeys = utils.objKeys(aCoinVoter.rOrders).sort();
                let consideredDocs = [];
                for (let ordIndex = 0; ordIndex < orderedKeys.length; ordIndex++) {
                    let docHash = aCoinVoter.rOrders[orderedKeys[ordIndex]];
                    if (consideredDocs.includes(docHash))
                        continue;

                    let theDoc = aCoinVoter.docsInfo[docHash];

                    if (!_.has(coinsAndOrderedSpendersDict[aCoin], ordIndex))
                        coinsAndOrderedSpendersDict[aCoin][ordIndex] = {};

                    if (!_.has(coinsAndOrderedSpendersDict[aCoin][ordIndex], docHash))
                        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash] = {
                            voteData: {
                                docHash,
                                inside6VotersCount: 0,
                                inside6VotesGain: 0,
                                outside6VotersCount: 0,
                                outside6VotesGain: 0,
                                voteGain: 0,
                                details: {}
                            },
                            docs: []
                        };

                    // let votePower = utils.calcLog(theDoc.gapTime, (iConsts.getCycleBySeconds() * 2)).gain;
                    // if (votePower == 0)
                    //     votePower = utils.iFloorFloat(iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);
                    let votePower = 1;  // TODO improve votePower
                    let voteGain = utils.iFloorFloat(theDoc.voterPercentage * votePower);

                    coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.voteGain += voteGain;

                    if (aCoinVoter.spendsLessThan6H) { //spendsLessThan6HNew
                        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.inside6VotersCount += 1;
                        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.inside6VotesGain += voteGain;
                    } else {
                        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.outside6VotersCount += 1;
                        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.outside6VotesGain += voteGain;
                    }

                    coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].voteData.details[voter] = theDoc;
                    coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].docs.push(theDoc);

                }
            }
        }
        clog.trx.info(`coins And Spenders Dict for doc(${utils.hash6c(invokedDocHash)}): ${utils.stringify(coinsAndOrderedSpendersDict)}`);
        if (doDBLog)
            this._super.logSus({
                lkey: '3.coinsAndOrderedSpendersDict',
                blockHash: '-',
                docHash: invokedDocHash,
                coins,
                logBody: utils.stringify(coinsAndOrderedSpendersDict)
            });

        return {
            err: false,
            coinsAndOrderedSpendersDict
        }
    }
}

module.exports = CoinSpenderGroupper;


/**
 *
coinsAndOrderedSpendersDict sample: {
    "e45630b91c0df1323e8a861464e9fdb8deb705c523cb977bca09d58c5409ef5c:0": [

        first position
        {
            "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd": {
                "voteData": {
                    "docHash": "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd",
                    "inside6VotersCount": 0,
                    "inside6VotesGain": 0,
                    "outside6VotersCount": 2,
                    "outside6VotesGain": 99.99999999999,
                    "voteGain": 99.99999999999,
                    "details": {
                        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
                            "creationDate": "2019-04-01 22:22:36",
                            "rOrder": 0,
                            "shares": 99.98680174217,
                            "gapTime": 1723
                        },
                        "im1xpsnyd3k8q6kvvrzxq6xyctyxp3xxc3cxdnryep5xg6rgvehvgcx2fu6vs2": {
                            "creationDate": "2019-04-01 22:22:36",
                            "rOrder": 0,
                            "shares": 0.01319825782,
                            "gapTime": 1730
                        }
                    }
                },
                "docs": [
                    {
                        "creationDate": "2019-04-01 22:22:36",
                        "rOrder": 0,
                        "shares": 99.98680174217,
                        "gapTime": 1723
                    },
                    {
                        "creationDate": "2019-04-01 22:22:36",
                        "rOrder": 0,
                        "shares": 0.01319825782,
                        "gapTime": 1730
                    }
                ]
            }
        },

        second position
        {
            "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb": {
                "voteData": {
                    "docHash": "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb",
                    "inside6VotersCount": 0,
                    "inside6VotesGain": 0,
                    "outside6VotersCount": 2,
                    "outside6VotesGain": 99.99999999999,
                    "voteGain": 99.99999999999,
                    "details": {
                        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
                            "creationDate": "2019-04-01 22:50:58",
                            "rOrder": 1,
                            "shares": 99.98680174217,
                            "gapTime": 21
                        },
                        "im1xpsnyd3k8q6kvvrzxq6xyctyxp3xxc3cxdnryep5xg6rgvehvgcx2fu6vs2": {
                            "creationDate": "2019-04-01 22:50:58",
                            "rOrder": 1,
                            "shares": 0.01319825782,
                            "gapTime": 28
                        }
                    }
                },
                "docs": [
                    {
                        "creationDate": "2019-04-01 22:50:58",
                        "rOrder": 1,
                        "shares": 99.98680174217,
                        "gapTime": 21
                    },
                    {
                        "creationDate": "2019-04-01 22:50:58",
                        "rOrder": 1,
                        "shares": 0.01319825782,
                        "gapTime": 28
                    }
                ]
            }
        }
    ]
}
 *
 */
