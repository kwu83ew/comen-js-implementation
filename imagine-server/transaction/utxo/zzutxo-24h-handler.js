const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const Moment = require("../../startup/singleton").instance.Moment;
const model = require("../../models/interface");
const clog = require('../../loggers/console_logger');
const utxoHandler = require("./utxo-handler");
// const dagHandler = require("../../dag/graph-handler/dag-handler");
// const crypto = require('../../crypto/crypto');

class UTXO24Hanlder {

    static search(args) {
        let params = {
            table: 'i_trx_24h_map'
        }

        if (_.has(args, 'fields'))
            params.fields = args.fields

        params.query = []

        if (_.has(args, 'refLocs'))
            params.query.push(['ref_loc', ['IN', refLocs]])

        let res = model.sRead(params)
        return res;
    }

    static getTrxFromRef(refs) {
        let res = model.sRead({
            table: 'i_trx_24h_map',
            query: [
                ['ref_loc', ['IN', refs]]
            ]
        })
        return res;
    }

    static addBlockMap(block) {
        let params = [];
        block.docs.forEach(trx => {
            trx.inputs.forEach(input => {
                params.push({
                    ref_loc: iutils.packCoinRef(input[0], input[1]),
                    block_hash: block.blockHash,
                    trx_hash: trx.hash,
                    creation_date: block.creationDate
                })
            });
        });
        this.addNewMap(params);
    }

    static addNewMap(maps) {
        if (!Array.isArray(maps))
            maps = [maps]
        maps.forEach(element => {
            model.sCreate({
                table: 'i_trx_24h_map',
                values: {
                    ref_loc: element.ref_loc,
                    block_hash: element.block_hash,
                    trx_hash: element.trx_hash,
                    creation_date: element.creation_date,
                    insert_date: utils.getNow()
                }
            });
        });
    }
}

module.exports = UTXO24Hanlder;
