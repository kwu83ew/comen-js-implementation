const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');


class CoinPositionGroupper {

    static doGroupByCoinAndPosition(args) {
        let coinsAndOrderedSpendersDict = args.coinsAndOrderedSpendersDict;
        let invokedDocHash = args.invokedDocHash;
        let doDBLog = _.has(args, 'doDBLog') ? args.doDBLog : false;

        let coinsAndPositionsDict = {};
        let coins = utils.objKeys(coinsAndOrderedSpendersDict);
        for (let aCoin of coins) {
            if (!_.has(coinsAndPositionsDict, aCoin))
                coinsAndPositionsDict[aCoin] = {};

            for (let position = 0; position < coinsAndOrderedSpendersDict[aCoin].length; position++) {
                if (!_.has(coinsAndPositionsDict[aCoin], position))
                    coinsAndPositionsDict[aCoin][position] = {
                        inside6h: [],
                        outside6h: [],
                        outsideTotal: 0,
                        insideTotal: 0,
                    };

                // integrating votes of all documents in this position
                let documentsInThisPosition = coinsAndOrderedSpendersDict[aCoin][position];
                for (let docHash of utils.objKeys(documentsInThisPosition)) {
                    let voteData = coinsAndOrderedSpendersDict[aCoin][position][docHash].voteData;
                    if (voteData.outside6VotesGain > voteData.inside6VotesGain) {
                        coinsAndPositionsDict[aCoin][position].outsideTotal += voteData.outside6VotesGain;
                        coinsAndPositionsDict[aCoin][position].outside6h.push({
                            docHash,
                            votes: voteData.outside6VotesGain,
                            voters: voteData.outside6VotersCount
                        });

                    } else if (voteData.outside6VotesGain < voteData.inside6VotesGain) {
                        coinsAndPositionsDict[aCoin][position].insideTotal += voteData.inside6VotesGain;
                        coinsAndPositionsDict[aCoin][position].inside6h.push({
                            docHash,
                            votes: voteData.inside6VotesGain,
                            voters: voteData.inside6VotersCount
                        });

                    } else if (voteData.outside6VotesGain == voteData.inside6VotesGain) {
                        if (voteData.outside6VotersCount > voteData.inside6VotersCount) {
                            coinsAndPositionsDict[aCoin][position].outsideTotal += voteData.outside6VotesGain;
                            coinsAndPositionsDict[aCoin][position].outside6h.push({
                                docHash,
                                votes: voteData.outside6VotesGain,
                                voters: voteData.outside6VotersCount
                            });

                        } else if (voteData.outside6VotersCount < voteData.inside6VotersCount) {
                            coinsAndPositionsDict[aCoin][position].insideTotal += voteData.inside6VotesGain;
                            coinsAndPositionsDict[aCoin][position].inside6h.push({
                                docHash,
                                votes: voteData.inside6VotesGain,
                                voters: voteData.inside6VotersCount
                            });

                        } else if (voteData.outside6VotersCount == voteData.inside6VotersCount) {
                            let outHash = crypto.keccak256(utils.stringify([voteData.outside6VotersCount, voteData.outside6VotesGain]));
                            let inHash = crypto.keccak256(utils.stringify([voteData.inside6VotersCount, voteData.inside6VotesGain]));
                            if (outHash > inHash) {
                                coinsAndPositionsDict[aCoin][position].outsideTotal += voteData.outside6VotesGain;
                                coinsAndPositionsDict[aCoin][position].outside6h.push({
                                    docHash,
                                    votes: voteData.outside6VotesGain,
                                    voters: voteData.outside6VotersCount
                                });

                            } else {
                                coinsAndPositionsDict[aCoin][position].insideTotal += voteData.inside6VotesGain;
                                coinsAndPositionsDict[aCoin][position].inside6h.push({
                                    docHash,
                                    votes: voteData.inside6VotesGain,
                                    voters: voteData.inside6VotersCount
                                });

                            }
                        }
                    }

                }
            }
        }
        clog.trx.info(`coinsAndPositionsDict for doc(${utils.hash6c(invokedDocHash)}): ${utils.stringify(coinsAndPositionsDict)}`);
        if (doDBLog)
            this._super.logSus({
                lkey: '4.coinsAndPositionsDict',
                blockHash: '-',
                docHash: invokedDocHash,
                coins,
                logBody: utils.stringify(coinsAndPositionsDict)
            });

        return {
            err: false,
            coinsAndPositionsDict
        }
    }
}

module.exports = CoinPositionGroupper;



/**
 *

coinsAndPositionsDict:
{
    "e45630b91c0df1323e8a861464e9fdb8deb705c523cb977bca09d58c5409ef5c:0": {
        "0": {
            "inside6h": [],
            "outside6h": [
                {
                    "docHash": "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd",
                    "votes": 99.99999999999,
                    "voters": 2
                }
            ],
            "outsideTotal": 99.99999999999,
            "insideTotal": 0
        },
        "1": {
            "inside6h": [],
            "outside6h": [
                {
                    "docHash": "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb",
                    "votes": 99.99999999999,
                    "voters": 2
                }
            ],
            "outsideTotal": 99.99999999999,
            "insideTotal": 0
        }
    }
}

it means for the coin e45630:0 in position zero there is only one spender which is out of 6 hours
and this spender is valid, while there is another spender in position 2 and difinetaly must be rejected

 */
