//spend TxOs handler
const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const utxoHandler = require('./utxo-handler');
const model = require('../../models/interface');
const dagHandler = require('../../dag/graph-handler/dag-handler');
const blockUtils = require('../../dag/block-utils');

const table = 'i_trx_spend';

class SpentCoinsHandler {

    static markAsSpentAllBlockInputs(args) {
        let block = args.block;
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        for (let doc of block.docs) {
            if (!iutils.trxHasInput(doc.dType))
                continue;

            // TODO FIXME: discover cloned transactions and mark them too
            for (let input of doc.inputs) {
                this.markSpentAnInput({
                    ref_loc: iutils.packCoinRef(input[0], input[1]),
                    spend_loc: iutils.packCoinSpendLoc(block.blockHash, doc.hash),
                    spend_date: block.creationDate,
                    cDate
                });
            }
        }
    }

    static markSpentAnInput(args) {

        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        /**
         * 
         * remove old records
         * TODO: infact we should not remove history(at least for some resonably percent of nodes)
         * they have to keep ALL data, specially for long-term loans, to un-pledge the account, nodes needed this information.
         * wherase repayments took place in RpBlocks.RpDocs and most of time are sufficient to close a pledge, 
         * but sometimes pledger can pay all the loan in one transaction to get ride of loan
         * or pays a big part of loan to reduce interest rate.
         * in all of these case, the nodes which are engaged in loan(e.g. the arbiters nodes) must have these information. 
         * although all trx info are reachable via blocks, but this table provides a faster & easier crawling 
         * so in pledge time, the pledgee (can or must) add a bunch of long-term-data-backers(LTDB or LoTeDB) 
         * as evidence of repayments.
         * in such a way, to unpledge an account either pledgee signature or these LoTeDB signatures will be sufficient,
         * so, there is not obligue to mantain all data by all nodes
         * obviously these LoteDbs will be payed by pledge contract based on repayments longth 
         * TODO: must be implemented
         */

        model.sDelete({
            lockDb: true,
            table,
            query: [
                ['sp_spend_date', ['<', utils.minutesBefore(iConsts.getCycleByMinutes() * iConsts.KEEP_SPENT_COINS_BY_CYCLE, cDate)]]
            ]
        });

        model.sCreate({
            lockDb: true,
            table,
            values: {
                sp_coin: args.ref_loc,
                sp_spend_loc: args.spend_loc,
                sp_spend_date: args.spend_date,
            }
        });
    }

    /**
     * accepts given coins and prepares an ordered history of coins spent
     */
    static makeSpentCoinsDict(args) {
        let coins = args.coins;
        // TODO: maybe optimize it via bloom filters for daily(cyclic) spent coins
        let coinsInSpentTable = {};
        let spendsOrder = {};
        let inDAGRecordedCoins = this.searchInSpentCoins({
            coins,
            order: [['sp_spend_date', 'ASC']]
        });
        if (inDAGRecordedCoins.length > 0) {
            for (let sp of inDAGRecordedCoins) {
                if (!_.has(coinsInSpentTable, sp.spCoin))
                    coinsInSpentTable[sp.spCoin] = [];
                let spendInfo = iutils.unpackCoinSpendLoc(sp.spSpendLoc);
                coinsInSpentTable[sp.spCoin].push({
                    spendDate: sp.spSpendDate,
                    spendBlockHash: spendInfo.blockHash,
                    spendDocHash: spendInfo.docHash,
                });

                /**
                 * making a dictionary of history of spending each unique coin, 
                 * and ordering by spent time in machine's point of view.
                 * later it will be used to vote about transactions priority.
                 * it is totally possible in this step machine can not retrieve very old spending 
                 * (because the spent table periodicly truncated), in this case machine will vote a doublespended doc as a first document
                 * later in "doGroupByCoinAndVoter" method we add second index(vote date) to securing the spend order
                 * 
                 */
                // if (!_.has(spendsOrder, sp.spCoin))
                //     spendsOrder[sp.spCoin] = [];
                // spendsOrder[sp.spCoin].push({
                //     date: sp.spSpendDate,
                //     blockHash: spendInfo.blockHash,
                //     docHash: spendInfo.docHash,
                // });
            }
            clog.trx.error(`already Spent And Recorded Inputs Dict: ${utils.stringify(coinsInSpentTable)}`);
        }
        return { coinsInSpentTable, spendsOrder }
    }

    static searchInSpentCoins(args) {
        let query = [];
        if (_.has(args, 'coins'))
            query.push(['sp_coin', ['IN', args.coins]]);

        let order = _.has(args, 'order') ? args.order : [];

        let res = model.sRead({
            lockDb: true,
            table,
            query,
            order
        });
        if (res.length == 0)
            return [];

        res = res.map(x => this.convertFields(x));
        return res;
    }

    static getInputCreationLocationsForAddresses(addresses = []) {

        let alreadySpendRefInfo = {};
        let alreadySpendsRefsDocsHashes = [];
        for (let anSpend of this.searchInSpentCoins()) {
            let { docHash, outputIndex } = iutils.unpackCoinRef(anSpend.spCoin);
            alreadySpendRefInfo[anSpend.spCoin] = anSpend;
            alreadySpendsRefsDocsHashes.push(docHash);  // collecting docHash of all docs in which used the refs
        }
        clog.app.info(`already SpendRefInfo ${utils.stringify(alreadySpendRefInfo)}`);
        clog.app.info(`already SpendsRefsDocsHashes ${utils.stringify(alreadySpendsRefsDocsHashes)}`);

        // retrieve the blocks in which the refLocs are created
        let { blockHashes } = dagHandler.getBlockHashesByDocHashes({ docsHashes: alreadySpendsRefsDocsHashes });
        let docs = [];
        let wBlocks = dagHandler.searchInDAGSync({
            fields: ['b_body'],
            query: [
                ['b_hash', ['IN', blockHashes]]
            ]
        });
        let spentlist = [];
        for (let wBlock of wBlocks) {

            let block = blockUtils.openDBSafeObject(wBlock.bBody).content;
            // console.log('------------------------wBlock.bBody => block', block);
            for (let docIndex = 0; docIndex < block.docs.length; docIndex++) {
                let doc = block.docs[docIndex];
                if (iutils.trxHasOutput(doc.dType)) {
                    for (let outInx = 0; outInx < doc.outputs.length; outInx++) {
                        let createdOutPut = iutils.packCoinRef(doc.hash, outInx);

                        // select only outputs which can be controled by wallet
                        if (addresses.includes(doc.outputs[outInx][0])) {

                            // select only used refs
                            if (_.has(alreadySpendRefInfo, createdOutPut)) {
                                let spLocDtl = iutils.unpackCoinSpendLoc(alreadySpendRefInfo[createdOutPut].spSpendLoc);
                                spentlist.push({
                                    blockHash: block.blockHash,
                                    docIndex,
                                    docHash: doc.hash,
                                    outInx,
                                    address: doc.outputs[outInx][0],
                                    value: doc.outputs[outInx][1],

                                    coin: createdOutPut,
                                    spendDate: alreadySpendRefInfo[createdOutPut].spSpendDate,
                                    spendBlock: spLocDtl.blockHash,
                                    spendDoc: spLocDtl.docHash,
                                    spendLoc: alreadySpendRefInfo[createdOutPut].spSpendLoc
                                });
                            }
                        }

                    }
                }
            }
        }
        return spentlist;
    }

    static findCoinsSpendLocations(args) {
        let coins = args.coins;
        // finding the block(s) which are used these coins and already are registerg in DAG(if they did)
        // this function just writes some logs and have not effect on block validation accept/denay
        let recordedSpendInDAG = {};
        for (let aCoin of coins) {
            clog.trx.info(`SCUDS: looking for aCoin ${iutils.shortCoinRef(aCoin)} `);
            let { docHash } = iutils.unpackCoinRef(aCoin);
            // find broadly already recorded block(s) which used(or referenced) this input-docHash
            let wBlocks = dagHandler.getWBlocksByDocHash({ docsHashes: [docHash] }).wBlocks;
            clog.trx.info(`SCUDS: looking for doc(${utils.hash6c(docHash)}) returned ${wBlocks.length} potentially blocks(${wBlocks.map(x => utils.hash6c(x.b_hash))})`);
            if (wBlocks.length > 0) {
                for (let wBlock of wBlocks) {
                    let refBlock = blockUtils.openDBSafeObject(wBlock.bBody).content;
                    clog.trx.info(`SCUDS: controlling block(${utils.hash6c(refBlock.blockHash)}) `);
                    if (_.has(refBlock, 'docs') && Array.isArray(refBlock.docs)) {
                        clog.trx.info(`SCUDS: block ${utils.hash6c(refBlock.blockHash)} has ${refBlock.docs.length} docs`);
                        for (let doc of refBlock.docs) {
                            if (_.has(doc, 'inputs')) {
                                clog.trx.info(`SCUDS: doc has ${doc.inputs.length} inputs`);
                                for (let inputIndex = 0; inputIndex < doc.inputs.length; inputIndex++) {
                                    let trxInput = doc.inputs[inputIndex];
                                    // if the docHash is referenced as an input index, select id
                                    if (trxInput[0] == docHash) {
                                        let tmpCoin = iutils.packCoinRef(trxInput[0], trxInput[1])
                                        clog.trx.info(`SCUDS: controlling input ${iutils.shortCoinRef(tmpCoin)}`);
                                        if (coins.includes(tmpCoin)) {
                                            if (!_.has(recordedSpendInDAG, tmpCoin))
                                                recordedSpendInDAG[tmpCoin] = [];
                                            recordedSpendInDAG[tmpCoin].push({
                                                blockHash: refBlock.blockHash,
                                                docHash,
                                                inputIndex
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return { err: false, spendsDict: recordedSpendInDAG }
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'sp_coin'))
            out.spCoin = elm.sp_coin;
        if (_.has(elm, 'sp_spend_loc'))
            out.spSpendLoc = elm.sp_spend_loc;
        if (_.has(elm, 'sp_spend_date'))
            out.spSpendDate = elm.sp_spend_date;

        return out;
    }

}

module.exports = SpentCoinsHandler;

