// const _ = require('lodash');
// const iConsts = require('../../config/constants');
// const utils = require("../../utils/utils");
// const iutils = require("../../utils/iutils");
// const Moment = require("../../startup/singleton").instance.Moment;
// const model = require("../../models/interface");
// const utxoHandler = require("./utxo-handler");
// const dagHandler = require("../../dag/graph-handler/dag-handler");
// const clog = require('../../loggers/console_logger');
// const crypto = require('../../crypto/crypto');

// const table = 'i_trx_12h_spend';

// class UTXO12Hanlder {

//     /**
//      * 
//      * @param {array} refLocations 
//      * can accept a list of refLocations and filter the used one in last 12 hours
//      * 
//      */
//     static getUsedOutputsInsideLast12H(refLocations) {
//         if (Array.isArray(refLocations) && (refLocations.length == 0))
//             return [];

//         let query = [
//             ['creation_date', ['>=', Moment(utils.getNow(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss")]]
//         ]
//         if (!utils._nilEmptyFalse(refLocations))
//             query.push(['ref_loc', ['IN', refLocations]]);

//         let res = model.sRead({ table, query });
//         return res;
//     }

//     static markIn12hUtxoes(block) {
//         for (let doc of block.docs) {
//             if (!iutils.trxHasInput(doc.dType))
//                 continue;

//             // TODO FIXME: discover cloned transactions and mark them too
//             doc.inputs.forEach(input => {
//                 this.mark12hUtxo({
//                     ref_loc: iutils.packCoinRef(input[0], input[1]),
//                     spend_loc: iutils.packCoinSpendLoc(block.blockHash, doc.hash),
//                     creation_date: block.creationDate
//                 });
//             });
//         }
//     }

//     static mark12hUtxo(args) {
//         // remove old records
//         model.sDelete({
//             table,
//             query: [
//                 ['insert_date', ['<', Moment(utils.getNow(), "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss")]]
//             ]
//         });

//         model.sCreate({
//             table,
//             values: {
//                 ref_loc: args.ref_loc,
//                 spend_loc: args.spend_loc,
//                 creation_date: args.creation_date,
//                 insert_date: _.has(args, 'insert_date') ? args.insert_date : utils.getNow()
//             }
//         });
//     }

//     static considerDoubleSpendingInLast12H(block, usedOutputsInLast12H) {

//         let nowT = utils.getNow();
//         let msg = `The block(${utils.hash6c(block.blockHash)}) uses some used utxo in last 12 hours`;
//         clog.trx.error(msg);
//         clog.trx.error(JSON.stringify(usedOutputsInLast12H));

//         let conflictedBlockHashes = dagHandler.retrieveBlocksInWhichARefLocHaveBeenProduced(usedOutputsInLast12H.map(x => x.ref_loc)).blockHashes;

//         let refDict = {};
//         usedOutputsInLast12H.forEach(aROw => {
//             refDict[aROw.ref_loc] = {}
//             refDict[aROw.ref_loc]['spent'] = aROw
//         });

//         let refs = utils.objKeys(refDict)
//         for (let doc of block.docs) {
//             if (!iutils.trxHasInput(doc.dType))
//                 continue;

//             for (let input of doc.inputs) {
//                 let ref = iutils.packCoinRef(input[0], input[1]);
//                 if (refs.indexOf(ref) != -1) {
//                     refDict[ref]['newSpending'] = {
//                         'ref_loc': ref,
//                         'spend_loc': iutils.packCoinSpendLoc(block.blockHash, doc.hash),
//                         'creation_date': block.creationDate,
//                         'insert_date': nowT,

//                     }
//                 }
//             }
//         }

//         //refDict used for P4P transactions
//         let allConflictedAreUsedInSameLoc = true;
//         let suspisciousRefs = []
//         utils.objKeys(refDict).forEach(ref => {
//             if (refDict[ref]['spent'].spend_loc != refDict[ref]['newSpending'].spend_loc) {
//                 allConflictedAreUsedInSameLoc = false;
//                 suspisciousRefs.push(ref)
//             }
//         });
//         let refsSHorted = utils.objKeys(refDict).map(x => utils.hash6c(x) + ':' + x.split(':')[1]).join()
//         if (allConflictedAreUsedInSameLoc == true) {
//             clog.trx.warn(`All confllicted trxs spent the fund in same location, so potentialy it is a P4P transaction. refs: ${refsSHorted} 
//             new block (${utils.hash6c(block.blockHash)})
//             with blocks ${conflictedBlockHashes.map(x => utils.hash6c(x))}`)
//             // it could be a p4p transaction
//             // TODO implement it

//         } else {
//             clog.trx.warn(`it is definately a double spending! refs: ${refsSHorted} 
//             new block (${utils.hash6c(block.blockHash)})
//             with blocks ${conflictedBlockHashes.map(x => utils.hash6c(x))}`)

//             // it is definately a double spending
//             // now machine must decide to drops block or add it do DAG

//             let allConflictedBlocksReceivedInLessThan12HourTimeDifference = true;
//             // find the blocks containing suspisciousRefs
//             let trxHasehs = suspisciousRefs.map(x => iutils.unpackCoinRef(x).docHash);
//             let { blockHashes, mapDocToBlock } = dagHandler.getBlockHashesByDocHashes(trxHasehs)
//             blockHashes.forEach(block => {
//                 if (utils.timeDiff(block.receive_date).asMinutes > iConsts.getCycleByMinutes()) {
//                     allConflictedBlocksReceivedInLessThan12HourTimeDifference = false
//                 }
//             });


//             if (allConflictedBlocksReceivedInLessThan12HourTimeDifference == false) {
//                 process.exit();
//             }

//         }



//         // let creationDateDiff;
//         // // controll stated creation date
//         // creationDateDiff = (block.creationDate < ref.creation_date) ? utils.timeDiff(block.creationDate, ref.creation_date) : utils.timeDiff(ref.creation_date, block.creationDate);
//         // receiveDateDiff = utils.timeDiff(ref.creation_date, nowT);
//         // if (creationDateDiff.asMinutes < iConsts.getCycleByMinutes()) {
//         //     msg = `Atleast one trx-input-conflict is between 2 trx with more than 12 hours different in creation time`
//         //     allBlocksStatedOver12HCreationDate = false
//         //     // for local machine the newly received block is invalid and must be droped, unless has descendent link and reffers to another block 
//         //     // in which mentioned adjusting info

//         // }












//         //TODO:control if it is not P4P, or cloned coinbase blocks

//         //controll if beetween creation date of 2 conflicted block is less or more than 12 hours
//         let dblRefs = []
//         let atleastOneConflictOut12H = false
//         let allBlocksStatedOver12HCreationDate = true
//         usedOutputsInLast12H.forEach(ref => {
//             let creationDateDiff;

//             // controll stated creation date
//             creationDateDiff = (block.creationDate < ref.creation_date) ? utils.timeDiff(block.creationDate, ref.creation_date) : utils.timeDiff(ref.creation_date, block.creationDate);
//             receiveDateDiff = utils.timeDiff(ref.creation_date, nowT);
//             if (creationDateDiff.asMinutes < iConsts.getCycleByMinutes()) {
//                 msg = `Atleast one trx-input-conflict is between 2 trx with more than 12 hours different in creation time`
//                 allBlocksStatedOver12HCreationDate = false
//                 // for local machine the newly received block is invalid and must be droped, unless has descendent link and reffers to another block 
//                 // in which mentioned adjusting info

//             }
//         });



//         // if received block has descendents, it means it is a double-spending

//     }

//     static get12hShot() {
//         let res = model.sRead({
//             fields: ['ref_loc', 'spend_loc'],
//             table,
//             order: [
//                 ['ref_loc', 'ASC'],
//                 ['spend_loc', 'ASC'],
//                 ['creation_date', 'ASC'],
//             ]
//         });
//         let hash = '#' + utils.hash6c(crypto.keccak256(JSON.stringify(res)));
//         let count = res.length;
//         let hint = []
//         res.forEach(element => {
//             hint.push('#' + utils.hash6c(element.ref_loc))
//         });
//         hint = hint.join('\n')
//         return { hash, count, hint }
//     }
// }

// module.exports = UTXO12Hanlder;
