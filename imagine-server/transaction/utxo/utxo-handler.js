const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');

let dagHandler = null;

const table = 'i_trx_utxos';


let resObj1 = {
    calledAncestors: [],
    ref: null,
    err: false,
    msg: ''
}
function setter1(args) {
    utils.objKeys(args).forEach(key => {
        resObj1[key] = args[key];
    });

    // if (_.has(args, 'ref'))
    //     resObj1.ref = args.ref;

    // if (_.has(args, 'err'))
    //     resObj1.err = args.err;

    // if (_.has(args, 'msg'))
    //     resObj1.msg = args.msg;

}
function getter1(k = null) {
    if (k == null)
        return resObj1;

    clog.trx.info(`SuperValidate, getter1111 called for : ${k}`);
    if (_.has(resObj1, k))
        return resObj1[k];
    return null;
}


class UTXOHandler {


    /**
     * method, takes UTXO visibility and replace with new blocks in future of block
     */
    static refreshVisibility(args = {}) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();

        if (dagHandler == null)
            dagHandler = require('../../dag/graph-handler/dag-handler');
        let query = `SELECT ut_visible_by, MIN(ut_creation_date) FROM i_trx_utxos 
        WHERE ut_creation_date < $1 group by ut_visible_by order by MIN(ut_creation_date) `;
        let values = [utils.minutesBefore(iConsts.getCycleByMinutes() * 4, cDate)];
        let distinctBlocks = model.sCustom({
            query,
            values,
            lockDb: true
        });
        for (let aVis of distinctBlocks) {
            let blockHashes = dagHandler.walkThrough.getDescendents({ blockHashes: [aVis.ut_visible_by] });  // first generation of descendents
            blockHashes = utils.arrayAdd(blockHashes, dagHandler.walkThrough.getDescendents({ blockHashes }));  // second generation
            blockHashes = utils.arrayAdd(blockHashes, dagHandler.walkThrough.getDescendents({ blockHashes }));  // third generation
            blockHashes = utils.arrayUnique(blockHashes);
            let wBlocksEFS = dagHandler.excludeFloatingBlocks({ blockHashes }); // exclude floating blocks
            clog.trx.info(`visibleBys after exclude floating signature blocks(NB): ${wBlocksEFS.map(x => utils.hash6c(x.bHash))}`);

            for (let wBlock of wBlocksEFS) {
                // avoid dulicate constraint error
                let candidRefLocs = model.sRead({
                    lockDb: true,
                    fields: ['ut_coin'],
                    table,
                    query: [['ut_visible_by', aVis.ut_visible_by]],
                    log: false
                });
                candidRefLocs = candidRefLocs.map(x => x.ut_coin);
                // console.log(`\n\n------------candidRefLocs) ${utils.stringify(candidRefLocs)}`);

                let existRefLocs = model.sRead({
                    lockDb: true,
                    fields: ['ut_coin'],
                    table,
                    query: [['ut_visible_by', wBlock.bHash]],
                    log: false
                });
                existRefLocs = existRefLocs.map(x => x.ut_coin);
                let updateables = utils.arrayDiff(candidRefLocs, existRefLocs);
                // console.log(`\n\n------------existRefLocs) ${utils.stringify(existRefLocs)}`);
                // console.log(`\n\n------------updateables) ${utils.stringify(updateables)}`);
                // block db, and update visibility, then delete not shifted reflocs
                if ((Array.isArray(updateables)) && (updateables.length > 0)) {
                    model.sUpdate({
                        lockDb: true,
                        table,
                        query: [
                            ['ut_visible_by', aVis.ut_visible_by],
                            ['ut_coin', ['IN', updateables]]
                        ],
                        updates: {
                            ut_visible_by: wBlock.bHash
                        },
                        log: false
                    });
                }
                model.sDelete({
                    lockDb: true,
                    table,
                    query: [
                        ['ut_visible_by', aVis.ut_visible_by]
                    ],
                    log: false
                });
            }
        }
    }


    static extractUTXOsBYAddresses(addresses) {
        clog.trx.info(`extract UTXOs BY Addresses ${addresses.map(x => iutils.shortBech8(x))}`);

        let placeholders = model.makeMultiPlacehoders(addresses.length);
        let q = `SELECT ut_coin, ut_o_address, ut_o_value, min(ut_ref_creation_date) AS ref_creation_date
        FROM ${table} 
        WHERE ut_o_address IN(${placeholders.join()}) 
        GROUP BY ut_coin, ut_o_address, ut_o_value 
        ORDER BY min(ut_ref_creation_date), ut_o_address, ut_o_value;`
        let utxos = model.sQuery(q, addresses);
        clog.trx.info(`UTXOs BY Addresses -> ${JSON.stringify(utxos)}`);
        let newUTXOs = [];
        for (let element of utxos) {
            newUTXOs.push({
                utRefCreationDate: element.ref_creation_date,
                utCoin: element.ut_coin,
                utOAddress: element.ut_o_address,
                utOValue: iutils.convertBigIntToJSInt(element.ut_o_value)
            });
        };
        return newUTXOs;
    }

    static removeUsedCoinsByBlock(block) {
        clog.trx.info(`remove spent UXTOs of Block(${utils.hash6c(block.blockHash)})`);
        for (let doc of block.docs) {
            if (iutils.trxHasInput(doc.dType)) {
                for (let input of doc.inputs) {
                    UTXOHandler.removeCoin({
                        refLoc: iutils.packCoinRef(input[0], input[1]),
                    });
                }
            }
        }
    }

    static removeCoin(args) {
        clog.trx.info(`remove spent UXTO (${iutils.shortCoinRef(args.refLoc)})`);
        model.sDelete({
            lockDb: true,
            table,
            query: [
                ['ut_coin', args.refLoc],
            ],
            log: false
        });
    }

    /**
     * 
     * @param {*} hashes 
     * Only used for mitigate table load
     */
    static removeVisibleOutputsByBlocks(blockHashes) {
        let msg;
        if (dagHandler == null)
            dagHandler = require('../../dag/graph-handler/dag-handler');
        if (!Array.isArray(blockHashes))
            blockHashes = [blockHashes]
        let removeCandids = model.sRead({
            lockDb: true,
            table,
            query: [
                ['ut_visible_by', ['IN', blockHashes]]
            ],
            log: false
        });

        // sceptical tests before removing
        // TODO: take cae about repayment blocks, since they are created now but block creation date is one cycle before
        for (let aUtxo of removeCandids) {

            // control if the utxo already is visible by some newer blocks?
            let youngerVisibilityOfRefLoc = model.sRead({
                lockDb: true,
                table,
                query: [
                    ['ut_coin', aUtxo.ut_coin],
                    ['ut_creation_date', ['>', aUtxo.ut_creation_date]],
                ],
                log: false
            });
            if (youngerVisibilityOfRefLoc.length == 0) {
                // security issue
                msg = `The ut_coin which want to remove can not be seen by newer entries! ${utils.stringify(aUtxo)}`;
                clog.sec.error(msg);
                return { err: true, msg }
            }
            // clog.trx.info(`youngerVisibilityOfRefLoc res: ${utils.stringify(youngerVisibilityOfRefLoc)}`);

            // if the newer block has the old one in his history?
            let isVisByAnc = false;
            for (let anVis of youngerVisibilityOfRefLoc) {
                if (isVisByAnc)
                    continue;

                // retrieve whole ancestors of the utxo
                let allAncestorsOfAYoungerBlock = dagHandler.walkThrough.returnAncestorsYoungerThan({
                    blockHashes: [anVis.ut_visible_by],
                    byDate: aUtxo.ut_creation_date
                });
                if (allAncestorsOfAYoungerBlock.length == 0) {
                    continue;
                }
                if (allAncestorsOfAYoungerBlock.includes(aUtxo.ut_visible_by))
                    isVisByAnc = true;
            }
            if (!isVisByAnc) {
                // security issue
                msg = `The ut_coin which want to remove does not exist in history of newer entries! aUTXO:${utils.stringify(aUtxo)} younger Visibility Of RefLoc:${utils.stringify(youngerVisibilityOfRefLoc)} `;
                clog.sec.error(msg);
                return { err: true, msg }
            }

            // finally remove utxo which is visible by his descendents
            model.sDelete({
                lockDb: true,
                table,
                query: [
                    ['ut_visible_by', ['IN', blockHashes]],
                    ['ut_coin', aUtxo.ut_coin]
                ],
                log: false
            });
        }
    }

    static addNewUTXO(args) {
        let dblChk = model.sRead({
            lockDb: true,
            table,
            fields: ["ut_coin"],
            query: [
                ['ut_coin', args.refLoc],
                ['ut_visible_by', args.visibleBy],
            ],
            log: false
        });
        if (dblChk.length > 0)
            return;
        // clog.trx.info(`add NewUTXO maturated block(${utils.hash6c(args.visibleBy)}) cycle/cloneCode: ${args.cloneCode} ${utils.hash8c(args.address)} ${utils.microPAIToPAI(args.value)} pai`);
        model.sCreate({
            lockDb: true,
            table,
            values: {
                ut_creation_date: args.creationDate,
                // ut_clone_code: args.cloneCode,
                ut_coin: args.refLoc,
                ut_visible_by: args.visibleBy,
                ut_o_address: args.address,
                ut_o_value: args.value,
                ut_ref_creation_date: args.refCreationDate,
            },
            log: false
        });
    }

    /**
     * 
     * @param {*} args function clons entire entries are visibleby given block(ancestors) 
     * to new entries which are visibleby new-block
     */
    static inheritAncestorsVisbility(args) {
        // clog.trx.info(`inherit AncestorsVisbility: ${JSON.stringify(args)}`)
        let blockHashes = args.blockHashes;
        // clog.trx.info(`blockHashes==============================: ${blockHashes}`)
        let currentVisibility = model.sRead({
            lockDb: true,
            table,
            fields: ['ut_coin', 'ut_o_address', 'ut_o_value', 'ut_ref_creation_date'],
            query: [
                ['ut_visible_by', ['IN', blockHashes]]
            ],
            log: false
        });
        // clog.trx.info(`currentVisibility: ${JSON.stringify(currentVisibility)}`);

        for (let aUTXO of currentVisibility) {
            UTXOHandler.addNewUTXO({
                creationDate: args.creationDate,
                // cloneCode: aUTXO.ut_clone_code,
                refLoc: aUTXO.ut_coin,
                visibleBy: args.newBlockHash,
                address: aUTXO.ut_o_address,
                value: aUTXO.ut_o_value,
                refCreationDate: aUTXO.ut_ref_creation_date
            });
        };
    }

    static prepareUTXOQuery(args) {
        let coins = _.has(args, 'coins') ? args.coins : null;
        let visibleBy = _.has(args, 'visibleBy') ? args.visibleBy : null;

        let query = [];

        let hasValidCoins = false;
        if (!utils._nilEmptyFalse(coins)) {

            if (Array.isArray(coins)) {
                if (coins.length > 0)
                    hasValidCoins = true;
            } else {
                if (coins.length > 0)
                    hasValidCoins = true;
            }
            if (hasValidCoins == true)
                query.push(['ut_coin', ['IN', coins]])

        }

        if (!utils._nilEmptyFalse(visibleBy))
            query.push(['ut_visible_by', ['IN', visibleBy]])


        return query

    }

    static getCoinsInfo(args) {
        let query = this.prepareUTXOQuery(args);
        let res = model.sRead({
            lockDb: true,
            table,
            fields: ['DISTINCT ut_coin', 'ut_o_address', 'ut_o_value', 'ut_ref_creation_date'],
            query,
            log: false
        });
        if (res.length == 0)
            return [];

        res.forEach(element => {
            element.ut_o_value = iutils.convertBigIntToJSInt(element.ut_o_value)
        });
        res = res.map(x => this.convertFields(x));
        return res;
    }

    static async getCoinsInfoASync(args) {
        let query = this.prepareUTXOQuery(args);
        let res = await model.aRead({
            lockDb: true,   // TODO: implement lockDB for async connection too
            table,
            fields: ['DISTINCT ut_coin', 'ut_o_address', 'ut_o_value', 'ut_ref_creation_date'],
            query,
            log: false
        });
        if (res.length == 0)
            return [];

        res.forEach(element => {
            element.ut_o_value = iutils.convertBigIntToJSInt(element.ut_o_value)
        });
        res = res.map(x => this.convertFields(x));
        return res;
    }



    // static filterMauratedUTXOs(utxos) {
    //     if (!Array.isArray(utxos))
    //         utxos = [utxos]

    //     let minCreationDate = UTXOHandler.calculateMinDateForMaturationNormalTransaction()

    //     let res = model.sRead({
    //         table,
    //         fields: ['DISTINCT ref_loc '],
    //         query: [
    //             ['ref_loc', ['IN', utxos]],
    //             ['creation_date', ['<', minCreationDate]],
    //         ],
    //     });
    //     return res;
    // }

    // static calculateMinDateForMaturationNormalTransaction(t = null) {
    //     if (utils._nilEmptyFalse(t))
    //         t = utils.getNow()
    //     return Moment(t, "YYYY-MM-DD HH:mm:ss").subtract({ 'minutes': iConsts.getCycleByMinutes() }).format("YYYY-MM-DD HH:mm:ss");
    // }


    static getSpendablesInfo() {
        let res = model.sRead({
            lockDb: true,
            table,
            fields: ['ut_coin', 'ut_o_value', 'ut_ref_creation_date', 'ut_visible_by'],
            order: [
                ['ut_ref_creation_date', 'ASC'],
                ['ut_o_value', 'DESC']
            ]
        });
        let sum = 0;
        let utxos = [];
        let utxosDICT = {};
        for (let aRef of res) {
            if (!_.has(utxosDICT, aRef.ut_coin)) {
                utxosDICT[aRef.ut_coin] = {
                    refLocCreationDate: aRef.ut_ref_creation_date,
                    outValue: iutils.convertBigIntToJSInt(aRef.ut_o_value),
                    visibleBy: [],
                }
                utxos.push({
                    refLoc: aRef.ut_coin,
                    refLocCreationDate: aRef.ut_ref_creation_date,
                    outValue: iutils.convertBigIntToJSInt(aRef.ut_o_value),
                })
                sum += iutils.convertBigIntToJSInt(aRef.ut_o_value);
            }
            utxosDICT[aRef.ut_coin].visibleBy.push(aRef.ut_visible_by)
        }
        return { sum, utxos, utxosDICT };
    }

    static calcAddressFundSum(refLocs) {
        //TODO need to install bigint and complete this func
        if (utils._nilEmptyFalse(refLocs))
            return 0;

        if (!Array.isArray(refLocs))
            refLocs = [refLocs]

    }



    // interface methods
    static async getUTXOReportAsync(args) {
        let mpCode = _.has(args, 'mpCode') ? args.mpCode : iutils.getSelectedMProfile();
        let q = `SELECT DISTINCT ut_coin, ut_o_value FROM ${table} 
            WHERE ut_o_address IN (SELECT wa_address FROM i_machine_wallet_addresses WHERE wa_mp_code=$1) ORDER BY ut_coin`;
        let res = await model.aQuery(q, [mpCode]);
        res.forEach(element => {
            element.ut_o_value = iutils.convertBigIntToJSInt(element.ut_o_value)
        });
        return res;
    }

    static getUTXOSHoot(args) {
        let res = model.sRead({
            lockDb: true,
            fields: ['ut_visible_by', 'ut_coin', 'ut_o_address', 'ut_o_value'],
            table,
            order: [
                ['ut_visible_by', 'ASC'],
                ['ut_coin', 'ASC'],
                // ['ut_clone_code', 'ASC'],
                ['ut_o_address', 'ASC'],
                ['ut_o_value', 'ASC']
            ],
            log: false
        });
        let visShoot = '#' + utils.hash6c(crypto.keccak256(utils.stringify(res)));

        let UTXOs = model.sCustom({
            query: `SELECT DISTINCT ut_coin FROM ${table} ORDER BY ut_coin`,
            values: []
        });
        let UTXOShoot = '#' + utils.hash6c(crypto.keccak256(JSON.stringify(UTXOs)));
        let UTXOsCount = UTXOs.length;

        UTXOs = model.sCustom({
            query: `SELECT ut_coin, COUNT(*) _count FROM ${table} GROUP BY ut_coin ORDER BY _count ASC, ut_coin`,
            values: []
        });
        let UTXOsInfo = [];
        if (UTXOsCount > 0) {
            let maxBlocksCount = 30;
            if (maxBlocksCount > UTXOs.length)
                maxBlocksCount = UTXOs.length;
            for (let b = 0; b < maxBlocksCount; b++) {
                let refLoc = UTXOs[b].ut_coin.split(':');
                UTXOsInfo.push([
                    '#' + utils.hash6c(refLoc[0]) + ':' + refLoc[1] + ' (' + UTXOs[b]._count + ')'
                ]);
            }
        }
        UTXOsInfo = UTXOsInfo.join('\n');
        return { visShoot, UTXOShoot, UTXOsInfo, UTXOsCount }
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'ut_creation_date'))
            out.utCreationDate = elm.ut_creation_date;
        // if (_.has(elm, 'ut_clone_code'))
        //     out.utCloneCode = elm.ut_clone_code;
        if (_.has(elm, 'ut_coin'))
            out.utCoin = elm.ut_coin;
        if (_.has(elm, 'ut_o_address'))
            out.utOAddress = elm.ut_o_address;
        if (_.has(elm, 'ut_o_value'))
            out.utOValue = elm.ut_o_value;
        if (_.has(elm, 'ut_visible_by'))
            out.utVisibleBy = elm.ut_visible_by;
        if (_.has(elm, 'ut_ref_creation_date'))
            out.utRefCreationDate = elm.ut_ref_creation_date;
        return out;
    }

}


module.exports = UTXOHandler
