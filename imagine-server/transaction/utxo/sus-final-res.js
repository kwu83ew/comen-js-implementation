const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');



class FinalRes {

    static doSusVoteRes(args) {
        let invokedDocHash = args.invokedDocHash;
        let coinsAndPositionsDict = args.coinsAndPositionsDict;
        let doDBLog = _.has(args, 'doDBLog') ? args.doDBLog : false;

        let susVoteRes = {};

        // preparing final response for invoked document, based on position 0 (the fist spender)
        // responses for susvote
        let coins = utils.objKeys(coinsAndPositionsDict);
        for (let aCoin of coins) {
            if (coinsAndPositionsDict[aCoin][0].outsideTotal <= coinsAndPositionsDict[aCoin][0].insideTotal) {
                susVoteRes[aCoin] = {
                    valid: false,
                    action: 'donate',
                    voters: coinsAndPositionsDict[aCoin][0].inside6h[0].voters,
                    votes: coinsAndPositionsDict[aCoin][0].inside6h[0].votes,
                };
            }


            if (coinsAndPositionsDict[aCoin][0].outside6h.length == 1) {
                if (coinsAndPositionsDict[aCoin][0].outside6h[0].docHash == invokedDocHash) {
                    susVoteRes[aCoin] = {
                        valid: true,
                        voters: coinsAndPositionsDict[aCoin][0].outside6h[0].voters,
                        votes: coinsAndPositionsDict[aCoin][0].outside6h[0].votes,
                    };
                } else {
                    susVoteRes[aCoin] = {
                        valid: false,
                        action: 'reject',
                        voters: coinsAndPositionsDict[aCoin][0].outside6h[0].voters,
                        votes: coinsAndPositionsDict[aCoin][0].outside6h[0].votes,
                    };
                }
            }


            if (coinsAndPositionsDict[aCoin][0].outside6h.length > 1) {
                // in firt position, there are more than one docs with more than 6h gap time
                let voteAndHashDict = {}
                for (let anOutside of coinsAndPositionsDict[aCoin][0].outside6h) {
                    let theKey = `${_.padStart(anOutside.votes, 10, '0').toString()}${_.padStart(anOutside.voter, 10, '0').toString()}${anOutside.docHash}`;
                    voteAndHashDict[theKey] = anOutside
                }
                let maxVote = utils.objKeys(voteAndHashDict).sort().reverse()[0];
                maxVote = voteAndHashDict[maxVote];

                if (maxVote.docHash == invokedDocHash) {
                    susVoteRes[aCoin] = {
                        valid: true,
                        voters: maxVote.voters,
                        votes: maxVote.votes
                    };
                } else {
                    susVoteRes[aCoin] = {
                        valid: false,
                        action: 'reject',
                        voters: maxVote.voters,
                        votes: maxVote.votes
                    };
                }

                // let maxVotes = 0;
                // let maxVotedDocs = []
                // for (let anOutside of coinsAndPositionsDict[aCoin][0].outside6h) {
                //     if (anOutside.votes > maxVotes) {
                //         maxVotes = anOutside.votes;
                //         maxVotedDocs = [anOutside.docHash];
                //     } else if (anOutside.votes == maxVotes) {
                //         maxVotedDocs.push(anOutside.docHash);
                //     }
                // }
                // if (maxVotedDocs.length == 1) {
                //     if (maxVotedDocs[0] == invokedDocHash) {
                //         susVoteRes[aCoin] = {
                //             valid: true
                //         };
                //     } else {
                //         susVoteRes[aCoin] = {
                //             valid: false,
                //             action: 'reject'
                //         };
                //     }
                // }
                // if (maxVotedDocs.length > 1) {
                //     if (maxVotedDocs.sort()[0] == invokedDocHash) {
                //         susVoteRes[aCoin] = {
                //             valid: true
                //         };
                //     } else {
                //         susVoteRes[aCoin] = {
                //             valid: false,
                //             action: 'reject'
                //         };
                //     }
                // }
            }
        }
        if (doDBLog)
            this._super.logSus({
                lkey: '5.susVoteRes',
                blockHash: '-',
                docHash: invokedDocHash,
                coins,
                logBody: utils.stringify({ susVoteRes })
            });

        return {
            err: false,
            susVoteRes
        }
    }
};

module.exports = FinalRes;



/**
 *
susVoteRes: {
    "e45630b91c0df1323e8a861464e9fdb8deb705c523cb977bca09d58c5409ef5c:0": {
        "valid": true,
        "voters": 2,
        "votes": 99.99999999999
    }
}

 *
 */
