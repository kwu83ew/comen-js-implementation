const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const DNAHandler = require('../../dna/dna-handler');


class CoinVoterGroupper {

    static doGroupByCoinAndVoter(args) {
        let cDate = _.has(args, 'cDate') ? args.cDate : utils.getNow();
        let rawVotes = args.rawVotes;
        let invokedDocHash = args.invokedDocHash;
        let doDBLog = _.has(args, 'doDBLog') ? args.doDBLog : false;
        let doConsoleLog = _.has(args, 'doConsoleLog') ? args.doConsoleLog : false;

        let coinsAndVotersDict = {};

        // preparing coinsAndVotersDict 
        let coinSpenderDocs = []; // will be used to recognize colned transactions(if exist)
        for (let aRawVote of rawVotes) {

            let aCoin = aRawVote.st_coin;

            // integrate coin spends
            coinSpenderDocs.push(aRawVote.st_spender_doc);

            // group by coins
            if (!_.has(coinsAndVotersDict, aCoin))
                coinsAndVotersDict[aCoin] = {};

            // group by voters
            let voter = aRawVote.st_voter;
            if (!_.has(coinsAndVotersDict[aCoin], voter))
                coinsAndVotersDict[aCoin][voter] = {
                    spendOIBSBSCD: {},  //spend Orders Info By Spender Block's Stated Creation Date
                    docsInfo: {},
                    rOrders: {},
                    spendTimes: [],
                    firstSpenderInfo: {
                        spDoc: '',
                        spTime: null
                    } // will be used to recognize if the 2 usage of coins have less than 6 hours different or not
                };

            // integrating spendtimes by voter's POV
            if (
                (coinsAndVotersDict[aCoin][voter].firstSpenderInfo.spTime == null) ||
                (coinsAndVotersDict[aCoin][voter].firstSpenderInfo.spTime > aRawVote.st_spend_date)
            )
                coinsAndVotersDict[aCoin][voter].firstSpenderInfo = {
                    spDoc: aRawVote.st_spender_doc,
                    spTime: aRawVote.st_spend_date
                };
            coinsAndVotersDict[aCoin][voter].spendTimes.push([aRawVote.st_spend_date, aRawVote.st_spender_doc].join(','));
            coinsAndVotersDict[aCoin][voter].spendOIBSBSCD[`${aRawVote.st_spend_date}:${aRawVote.st_spender_doc}`] = {
                spdDate: aRawVote.st_spend_date,
                spdDoc: aRawVote.st_spender_doc,
            }

            /**
             * the time between voting time and now, more old more trusty
             * older than 12 hours defenitely approved vote 
             */
            coinsAndVotersDict[aCoin][voter].docsInfo[aRawVote.st_spender_doc] = {
                coinSpendDate: aRawVote.st_spend_date,
                rOrder: aRawVote.st_receive_order, // order of receiving spends for same refLoc
                voterPercentage: aRawVote.voterPercentage,
                voteDate: aRawVote.st_vote_date
                // gapTime,
                // inside6: (gapTime < halfCycle)
            };
            let gapInxSeg1 = _.padStart(aRawVote.st_receive_order, 4, '0').toString();
            /**
             * it is totaly possible when the node creates votes(because of spending same coin in different times), 
             * hasn't the total spends in history, so creates a partially ordered votes.
             * and did not send the vote about early usages of coin and only votes for 2 recently usage.
             * say one years a go the coin C was spent in Trx1, and after 1 month cheater again tried to spend it in Trx2
             * and now again want to spend in Trx3.
             * since machine has not Trx1 in history will say Trx2 is valid while it is not.
             * to cover this issue here we add a second index (vote date).
             * since the vote table NEVERE truncated, we have entire spends(if they are case of doublespend) 
             * and related doublespends attempttion's history
             */
            let gapInxSeg2 = aRawVote.st_vote_date;
            let gapInx = [gapInxSeg1, gapInxSeg2].join();
            coinsAndVotersDict[aCoin][voter].rOrders[gapInx] = aRawVote.st_spender_doc;
        }
        let coins = utils.objKeys(coinsAndVotersDict).join();
        clog.trx.info(`coinsAndVotersDict for doc(${utils.hash6c(invokedDocHash)}): ${utils.stringify(coinsAndVotersDict)}`);
        if (doDBLog)
            this._super.logSus({
                lkey: '2.coinsAndVotersDict',
                blockHash: '-',
                docHash: invokedDocHash,
                coins,
                logBody: utils.stringify(coinsAndVotersDict)
            });

        coinSpenderDocs = utils.arrayUnique(coinSpenderDocs)
        if ((coinSpenderDocs.length == 1) && (coinSpenderDocs == invokedDocHash)) {
            /**
             * all voters for all coins are agree, that all coins are used in same document with same hash
             */
            // so it is a cloned transaction
            let response = {
                err: false,
                valid: true,
                cloned: 'cloned',
                rawVotes,
                coinsAndVotersDict,
                coinsAndOrderedSpendersDict: {},
                coinsAndPositionsDict: {},
                susVoteRes: {}
            };
            if (doDBLog)
                this._super.logSus({
                    lkey: '3.cloned response',
                    blockHash: '-',
                    docHash: invokedDocHash,
                    coins,
                    logBody: utils.stringify(response)
                });
            return response;

        }

        // control if voter belives the coins are spended in less than 6 hours
        let fullCycle = iConsts.getCycleBySeconds();
        let halfCycle = fullCycle / 2;

        for (let aCoin of utils.objKeys(coinsAndVotersDict)) {
            for (let aVoter of utils.objKeys(coinsAndVotersDict[aCoin])) {
                let spendOIBSBSCDKeys = utils.objKeys(coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD).sort();

                // controll if spendOIBSBSCDKeys are for one uniq doc or diffferent docs
                let keyGroup = {}
                for (let aCompoundKey of spendOIBSBSCDKeys) {
                    if (!_.has(keyGroup, coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[aCompoundKey].spdDoc))
                        keyGroup[coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[aCompoundKey].spdDoc] = 0;
                    keyGroup[coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[aCompoundKey].spdDoc]++;
                }

                if (doConsoleLog)
                    console.log('o-o-o-o-o-o-o0-0-0-0-0-0-0', keyGroup);
                coinsAndVotersDict[aCoin][aVoter].spendTimes = utils.arrayUnique(coinsAndVotersDict[aCoin][aVoter].spendTimes).sort();
                let spendTimes = coinsAndVotersDict[aCoin][aVoter].spendTimes;

                coinsAndVotersDict[aCoin][aVoter]['spendsLessThan6HNew'] = false;
                coinsAndVotersDict[aCoin][aVoter]['spendsLessThan6H'] = false;
                if (utils.objKeys(keyGroup).length == 1) {
                    // there is only on spender document, so it is not the case of double-spending
                } else if (utils.objKeys(keyGroup).length > 1) {
                    let canControlLess6Condition = true;
                    for (let aKey of utils.objKeys(keyGroup)) {
                        if (keyGroup[aKey] > 1) {
                            /**
                             * it means there is at least one group of docs(maybe cloned) in combine with the another group(s)
                             * the other groups can be simple double-spend doc or a clone of double-spend
                             */
                            canControlLess6Condition = false;
                        }
                    }
                    /**
                     * it is possible existing 2 different groups of documents
                     * 1. a document group with magiority of occurrence which are cloned docs
                     * 2. and another group of doc which are cheating double spend docs
                     * defeniately both groups are issued by one entity who can sign the coins
                     * 
                     * cloned       cloned      double
                     * cloned       cloned      cloned
                     *    |         double      double
                     *    |            |           |
                     *    v            |           |
                     * double          v           v 
                     * cloned       cloned      cloned
                     * 
                     */
                    coinsAndVotersDict[aCoin][aVoter]['canControlLess6Condition'] = canControlLess6Condition;
                    coinsAndVotersDict[aCoin][aVoter]['spendsLessThan6HNew'] = false;
                    if (canControlLess6Condition) {

                        let firstSpendInfo = spendTimes[0].split(',');
                        let comparableDoc = utils.objKeys(coinsAndVotersDict[aCoin][aVoter].rOrders).sort()[1];
                        comparableDoc = coinsAndVotersDict[aCoin][aVoter].rOrders[comparableDoc]; // it is second doc by machine's POV
                        if (utils.timeDiff(
                            firstSpendInfo[0],
                            coinsAndVotersDict[aCoin][aVoter].docsInfo[comparableDoc].voteDate
                        ).asSeconds < fullCycle
                        ) {
                            /**
                             * if voter received a Trx before of 12 hours after stated spend time, 
                             * she can vote on it.
                             * after 12 hours of spending a coin any claim is inacceptable
                             */

                            coinsAndVotersDict[aCoin][aVoter]['spendsLessThan6H'] = (utils.timeDiff(
                                firstSpendInfo[0], spendTimes[1].split(',')[0]).asSeconds < halfCycle);
                        }


                        for (let inx = 1; inx < spendOIBSBSCDKeys.length; inx++) {
                            if (coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[spendOIBSBSCDKeys[inx - 1]].spdDoc !=
                                coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[spendOIBSBSCDKeys[inx]].spdDoc) {
                                /**
                                 * they are not 2 cloned doc in sequence!
                                 * so must controll time diff
                                 */
                                if ((utils.timeDiff(
                                    coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[spendOIBSBSCDKeys[inx - 1]].spdDate,
                                    coinsAndVotersDict[aCoin][aVoter].spendOIBSBSCD[spendOIBSBSCDKeys[inx]].spdDate
                                ).asSeconds < halfCycle)) {
                                    /**
                                     * two different docs are created in less than half a cylce time diff
                                     * so 
                                     */
                                    coinsAndVotersDict[aCoin][aVoter]['spendsLessThan6HNew'] = true;
                                }
                            }

                        }



                    }
                } else {

                }

            }
        }

        return { err: false, coinsAndVotersDict }
    }
}

module.exports = CoinVoterGroupper;

/**
 *
sample coinsAndVotersDict:

{
    "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0": {
        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
            "spendOIBSBSCD": {
                "2019-04-06 18:36:33:305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "spdDate": "2019-04-06 18:36:33",
                    "spdDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61"
                },
                "2019-04-06 19:16:58:0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "spdDate": "2019-04-06 19:16:58",
                    "spdDoc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
                }
            },
            "docsInfo": {
                "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "coinSpendDate": "2019-04-06 18:36:33",
                    "rOrder": 0,
                    "voterPercentage": 99.9700089973
                },
                "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "coinSpendDate": "2019-04-06 19:16:58",
                    "rOrder": 1,
                    "voterPercentage": 99.9700089973
                }
            },
            "rOrders": {
                "0000:2019-04-06 19:17:15": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "0001:2019-04-06 19:17:15": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
            },
            "spendTimes": [
                "2019-04-06 18:36:33",
                "2019-04-06 19:16:58"
            ],
            "firstSpenderInfo": {
                "spDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "spTime": "2019-04-06 18:36:33"
            },
            "spendsLessThan6HNew": false,
            "spendsLessThan6H": false
        },

        "im1xqukxefe8p3xxwpk89jx2wfjvdjkycfhvscrsde4xymnxvrrxy6xxjljrvp": {
            "spendOIBSBSCD": {
                "2019-04-06 18:36:33:305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "spdDate": "2019-04-06 18:36:33",
                    "spdDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61"
                },
                "2019-04-06 19:16:58:0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "spdDate": "2019-04-06 19:16:58",
                    "spdDoc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
                }
            },
            "docsInfo": {
                "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "coinSpendDate": "2019-04-06 18:36:33",
                    "rOrder": 0,
                    "voterPercentage": 0.02999100269
                },
                "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "coinSpendDate": "2019-04-06 19:16:58",
                    "rOrder": 1,
                    "voterPercentage": 0.02999100269
                }
            },
            "rOrders": {
                "0000:2019-04-06 19:17:09": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "0001:2019-04-06 19:17:09": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
            },
            "spendTimes": [
                "2019-04-06 18:36:33",
                "2019-04-06 19:16:58"
            ],
            "firstSpenderInfo": {
                "spDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "spTime": "2019-04-06 18:36:33"
            },
            "spendsLessThan6HNew": false,
            "spendsLessThan6H": false
        }
    }
}

*/
