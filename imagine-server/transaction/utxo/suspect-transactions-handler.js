const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const model = require('../../models/interface');
const clog = require('../../loggers/console_logger');
const crypto = require('../../crypto/crypto');
const DNAHandler = require('../../dna/dna-handler');


const table = 'i_trx_suspect_transactions';
const tblLogs = 'i_logs_suspect_transactions';

class SuspectTrxHandler {

    static logSus(args) {
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        model.sCreate({
            table: tblLogs,
            values: {
                ls_lkey: args.lkey,
                ls_block_hash: args.blockHash,
                ls_doc_hash: args.docHash,
                ls_coins: args.coins,
                ls_log_body: args.logBody,
                ls_creation_date: creationDate
            }
        });
    }

    static controlCloneZZ(votes) {
        // control if a particulare refLoc spended in more than one docHash? if yes it is not a cloned transaction
        let votesForACoin = {};
        for (let vote of votes) {
            let theCoin = vote.st_coin;
            if (!_.has(votesForACoin, theCoin))
                votesForACoin[theCoin] = {}

            if (!_.has(votesForACoin[theCoin], 'spenderDocs'))
                votesForACoin[theCoin].spenderDocs = []

            if (!_.has(votesForACoin[theCoin], 'spendInfo'))
                votesForACoin[theCoin].spendInfo = {}
            if (!_.has(votesForACoin[theCoin].spendInfo, vote.st_spender_doc))
                votesForACoin[theCoin].spendInfo[vote.st_spender_doc] = []

            votesForACoin[theCoin].spenderDocs.push(vote.st_spender_doc);
            votesForACoin[theCoin].spendInfo[vote.st_spender_doc].push(vote.st_spender_block);
        }
        let res = {};
        for (let aCoin of utils.objKeys(votesForACoin)) {
            let isCloned;

            // unifying spend-documents
            votesForACoin[aCoin].spenderDocs = utils.arrayUnique(votesForACoin[aCoin].spenderDocs);

            // unifying spend-blocks
            for (let docHash of utils.objKeys(votesForACoin[aCoin].spendInfo)) {
                votesForACoin[aCoin].spendInfo[docHash] = utils.arrayUnique(votesForACoin[aCoin].spendInfo[docHash]);
            }

            if (votesForACoin[aCoin].spenderDocs.length == 1) {
                // it is possible this aCoin is used in some cloned transactions
                // so controlling all related transactions
                isCloned = true;

            } else {
                isCloned = false

            }
            res[aCoin] = isCloned;
        }
        clog.trx.info(`cloneStatus votes For A Coin isCloned(${isCloned}) coin(${iutils.shortCoinRef(aCoin)})  ${utils.stringify(votesForACoin[aCoin].spendInfo)}`);
        clog.trx.info(`cloneStatus res ${utils.stringify(res)}`);

        return res;
    }

    static getSusInfoByBlockHash(blockHash) {
        // retrieve votes about ALL votes about all coins which are used in given block and are spent in other blocks too
        let _q = `
            SELECT st_voter, st_coin, st_spender_block, st_spender_doc, st_vote_date FROM ${table} 
            WHERE st_coin IN (SELECT st_coin FROM ${table} WHERE st_spender_block=$1) 
            ORDER BY st_voter, st_coin`;
        let votes = model.sQuery(_q, [blockHash]);
        if (votes.length == 0) {
            clog.trx.error(`Didn't recognized as an suspicious doc, block(${utils.hash6c(blockHash)}) !`);
            return { hasSusRecords: false };
        }
        clog.trx.info(`Found sus trxes(${votes.length} record) for block(${utils.hash6c(blockHash)})`);


        // control if they are cloned trx?


        // calculate already votes
        let votesDict = {};
        for (let vote of votes) {
            if (!_.has(votesDict, vote.st_spender_block))
                votesDict[vote.st_spender_block] = { votes: [], sumPercent: 0 };

            let sharesPercent = DNAHandler.getAnAddressShares(vote.st_voter, vote.st_vote_date).percentage; // dna handler to calculate
            if (sharesPercent == 0)
                sharesPercent = utils.iFloorFloat(iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);

            votesDict[vote.st_spender_block].votes.push({ voter: vote.st_voter, sharesPercent })

        }
        for (let blkHsh of utils.objKeys(votesDict)) {
            let uniqVoters = {}
            for (let aVote of votesDict[blkHsh].votes) {
                uniqVoters[aVote.voter] = aVote.sharesPercent
            };
            votesDict[blkHsh].sumPercent = 0;
            for (let voter of utils.objKeys(uniqVoters)) {
                votesDict[blkHsh].sumPercent += uniqVoters[voter];
            };
            votesDict[blkHsh].sumPercent = utils.iFloorFloat(votesDict[blkHsh].sumPercent);
        };
        return { hasSusRecords: true, votesDict }//cloneStatus
    }

    static getSusInfoByDocHash(args) {
        let invokedDocHash = args.invokedDocHash;

        let rawVotes = [];
        // retrieve all trx which using same inputs of given trx
        let _q = `
        SELECT * FROM ${table} WHERE st_coin IN (SELECT st_coin FROM ${table} WHERE st_spender_doc=$1) 
        ORDER BY st_voter, st_receive_order`;
        rawVotes = model.sQuery(_q, [invokedDocHash]);
        if (rawVotes.length == 0) {
            clog.trx.error(`invokedDocHash(${utils.hash6c(invokedDocHash)}) didn't recognized as an suspicious doc!`);
            return {
                'allCoinsAreValid': true,
                rawVotes: [],
                coinsAndVotersDict: {},
                coinsAndOrderedSpendersDict: {},
                coinsAndPositionsDict: {},
                susVoteRes: {}
            };
        }

        return {
            rawVotes,
            coinsAndVotersDict: {},
            coinsAndOrderedSpendersDict: {},
            coinsAndPositionsDict: {},
            susVoteRes: {}
        };

    }

    static retrieveVoterPercentages(args) {
        let rawVotes = args.rawVotes;

        // retrieve voter shares
        let tmpVoterDateSharesDIct = {}
        for (let aRawVote of rawVotes) {
            let key = `${aRawVote.st_voter}:${aRawVote.st_vote_date}`;//TODO optimaized it to use start date f period instead of analog date in range, in  order to reduce map table
            if (!_.has(tmpVoterDateSharesDIct, key)) {
                let percentage = DNAHandler.getAnAddressShares(aRawVote.st_voter, aRawVote.st_vote_date).percentage;
                if (percentage < iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER) {
                    clog.trx.info(`shares == 0 for (${iutils.shortBech(voter)}): ${percentage}`);
                    percentage = iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER;
                    clog.trx.info(`shares == 0 for (${iutils.shortBech(voter)}): ${percentage}`);
                }
                tmpVoterDateSharesDIct[key] = percentage;
            }
            aRawVote.voterPercentage = tmpVoterDateSharesDIct[key];
        }

        return { err: false, rawVotes };
    }

    static checkDocValidity(args) {
        let invokedDocHash = args.invokedDocHash;
        let rawVotes = args.rawVotes;
        let doDBLog = _.has(args, 'doDBLog') ? args.doDBLog : false;
        let doConsoleLog = _.has(args, 'doConsoleLog') ? args.doConsoleLog : false;
        
        if (doConsoleLog)
            console.log(`>>>>>>>>>>>>>invokedDocHash: ${invokedDocHash}`);


        clog.trx.info(`trx suspect transactions for doc(${utils.hash6c(invokedDocHash)}): ${utils.stringify(rawVotes)}`);
        if (doDBLog) this.logSus({
            lkey: '1.suspect transactions',
            blockHash: '-',
            docHash: invokedDocHash,
            refLocs: '-',
            logBody: utils.stringify(rawVotes)
        });
        if (doConsoleLog) {
            console.log('rawVotes----------------');
            console.log('rawVotes----------------');
            console.log(utils.stringify(rawVotes));
            console.log('rawVotes----------------');
        }





        let cvRes = this.cvGroupper.doGroupByCoinAndVoter({
            doDBLog,
            rawVotes,
            invokedDocHash
        });
        if (_.has(cvRes, 'valid'))
            return cvRes;
        let coinsAndVotersDict = cvRes.coinsAndVotersDict;
        if (doConsoleLog) {
            console.log('coinsAndVotersDict+++++++++++');
            console.log('coinsAndVotersDict+++++++++++');
            console.log(utils.stringify(coinsAndVotersDict));
            console.log('coinsAndVotersDict+++++++++++');
        }






        // preparing coinsAndOrderedSpendersDict
        let csRes = this.csGroupper.doGroupByCoinAndSpender({
            doDBLog,
            invokedDocHash,
            coinsAndVotersDict
        });
        let coinsAndOrderedSpendersDict = csRes.coinsAndOrderedSpendersDict;

        if (doConsoleLog) {
            console.log('coinsAndOrderedSpendersDict*********');
            console.log('coinsAndOrderedSpendersDict*********');
            console.log(utils.stringify(coinsAndOrderedSpendersDict));
            console.log('coinsAndOrderedSpendersDict*********');
        }





        // preparing coinsAndPositionsDict
        let cpRes = this.cpGroupper.doGroupByCoinAndPosition({
            doDBLog,
            invokedDocHash,
            coinsAndOrderedSpendersDict
        });
        let coinsAndPositionsDict = cpRes.coinsAndPositionsDict;
        if (doConsoleLog) {
            console.log('coinsAndPositionsDict@@@@@@@@@@');
            console.log('coinsAndPositionsDict@@@@@@@@@@');
            console.log(utils.stringify(coinsAndPositionsDict));
            console.log('coinsAndPositionsDict@@@@@@@@@@');
        }







        let fRes = this.finalRes.doSusVoteRes({
            doDBLog,
            invokedDocHash,
            coinsAndPositionsDict
        });
        let susVoteRes = fRes.susVoteRes;
        if (doConsoleLog) {
            console.log('susVoteRes#######');
            console.log('susVoteRes#######');
            console.log(utils.stringify(susVoteRes));
            console.log('susVoteRes#######');
        }



        return { rawVotes, coinsAndVotersDict, coinsAndOrderedSpendersDict, coinsAndPositionsDict, susVoteRes };
    }

    static retrieveSuspectTransactions(args) {
        args.table = table;
        let res = model.sRead(args);
        return res;
    }

    static async retrieveSuspectTransactionsAsync(args) {
        args.table = table;
        let res = await model.aRead(args);
        return res;
    }

    static addSuspectTransaction(args) {
        let values = {
            st_voter: args.voter,
            st_vote_date: args.voteDate,
            st_coin: args.coin,
            st_logger_block: args.loggerBlock,
            st_spender_block: args.spenderBlock,
            st_spender_doc: args.spenderDoc,
            st_spend_date: args.spendDate,   // the dae in which spenderblock is created. it stated by block backer and is not trusty
            st_receive_order: args.receiveOrder
        }
        clog.app.info(`addSuspectTransaction: ${utils.stringify(values)}`);
        // console.log('ppppppppppppppppppppppppppppp');
        // console.log('ppppppppppppppppppppppppppppp');
        // console.log(values);
        // console.log('ppppppppppppppppppppppppppppp');
        model.sCreate({
            table,
            values
        });
        return;
    }

    static convertFields(elm) {
        let out = {};
        if (_.has(elm, 'st_voter'))
            out.stVoter = elm.st_voter;
        if (_.has(elm, 'st_vote_date'))
            out.stVoteDate = elm.st_vote_date;
        if (_.has(elm, 'st_coin'))
            out.stCoin = elm.st_coin;
        if (_.has(elm, 'st_logger_block'))
            out.stLoggerBlock = elm.st_logger_block;
        if (_.has(elm, 'st_spender_block'))
            out.stSpenderBlock = elm.st_spender_block;
        if (_.has(elm, 'st_spender_doc'))
            out.stSpenderDoc = elm.st_spender_doc;
        if (_.has(elm, 'st_receive_order'))
            out.stReceiveOrder = elm.st_receive_order;
        if (_.has(elm, 'st_spend_date'))
            out.stSpendDate = elm.st_spend_date;

        return out;
    }

}

SuspectTrxHandler.cvGroupper = require('./sus-goupby-coin-voter');
SuspectTrxHandler.cvGroupper._super = SuspectTrxHandler;

SuspectTrxHandler.csGroupper = require('./sus-goupby-coin-spender-doc');
SuspectTrxHandler.csGroupper._super = SuspectTrxHandler;

SuspectTrxHandler.cpGroupper = require('./sus-goupby-coin-position');
SuspectTrxHandler.cpGroupper._super = SuspectTrxHandler;

SuspectTrxHandler.finalRes = require('./sus-final-res');
SuspectTrxHandler.finalRes._super = SuspectTrxHandler;

module.exports = SuspectTrxHandler;




/**
 *
rawVotes sample:
[
    {
        "st_id": "130",
        "st_voter": "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl",
        "st_vote_date": "2019-04-06 19:17:15",
        "st_coin": "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0",
        "st_logger_block": "92a15cc9d65f792490c735d39df57062a804e0aab83495ea14b2378b71a32a7e",
        "st_spender_block": "36b2c42018dca8a3a9d4e16db974d87fb614d5db57d5b9348181c2b88cb3c265",
        "st_spender_doc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
        "st_receive_order": 0,
        "st_spend_date": "2019-04-06 18:36:33",
        "voterPercentage": 99.9700089973
    },
    {
        "st_id": "131",
        "st_voter": "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl",
        "st_vote_date": "2019-04-06 19:17:15",
        "st_coin": "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0",
        "st_logger_block": "92a15cc9d65f792490c735d39df57062a804e0aab83495ea14b2378b71a32a7e",
        "st_spender_block": "01a3d7b68f0f9453c9f323bbd6fe53d0df0ac163a6705689bec6f9f1f6842d14",
        "st_spender_doc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1",
        "st_receive_order": 1,
        "st_spend_date": "2019-04-06 19:16:58",
        "voterPercentage": 99.9700089973
    },
    {
        "st_id": "132",
        "st_voter": "im1xqukxefe8p3xxwpk89jx2wfjvdjkycfhvscrsde4xymnxvrrxy6xxjljrvp",
        "st_vote_date": "2019-04-06 19:17:09",
        "st_coin": "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0",
        "st_logger_block": "d13a5a3c54bfb28ea37db2bf6a6340d628fac954e45a6459509e547bc7ad4407",
        "st_spender_block": "36b2c42018dca8a3a9d4e16db974d87fb614d5db57d5b9348181c2b88cb3c265",
        "st_spender_doc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
        "st_receive_order": 0,
        "st_spend_date": "2019-04-06 18:36:33",
        "voterPercentage": 0.02999100269
    },
    {
        "st_id": "133",
        "st_voter": "im1xqukxefe8p3xxwpk89jx2wfjvdjkycfhvscrsde4xymnxvrrxy6xxjljrvp",
        "st_vote_date": "2019-04-06 19:17:09",
        "st_coin": "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0",
        "st_logger_block": "d13a5a3c54bfb28ea37db2bf6a6340d628fac954e45a6459509e547bc7ad4407",
        "st_spender_block": "01a3d7b68f0f9453c9f323bbd6fe53d0df0ac163a6705689bec6f9f1f6842d14",
        "st_spender_doc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1",
        "st_receive_order": 1,
        "st_spend_date": "2019-04-06 19:16:58",
        "voterPercentage": 0.02999100269
    }
]

 */
