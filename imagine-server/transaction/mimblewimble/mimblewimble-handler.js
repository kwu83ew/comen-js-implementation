


class MimblewimbleHandler{

    static getMimbleFundsValue(){
        /**
         * aggregates out put of all transaction in type MWIngress, in order to have strong binding, and to ensuring no new coin created in obfuscated transactions
         */
        let value = 0;
        return value;
    }

    /**
     * 
     * @param {*} value 
     * controls if inside MW doesn't create new coins!
     */
    static canTransformFromMWToMain(value){
        let MWCurrentValues = this.getMimbleFundsValue();
        return (value>=MWCurrentValues);
    }

}