const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const trxHashHandler = require('../../transaction/hashable');

/**
 * the PAIs participated in these kind of transations will be transparently traceable until certain time(by minutes or changes between parties)
 * e.g. after 5 time changing between people or/and 3 years 
 * this kind of transparency is NECESSARY for paying Taxes, or donations,... in order to trace the actual use of the payments
 * 
 * output of this transactions, can not be used as input in any kind of transactions without dAttr included "TTT". 
 * specially can not be used as input in any privacy protected transactions
 * in sequence the outputs always must be spent in another Transparent Tx(in which the dAttr includes iConsts.TRX_ATTRS.TTT)
 * 
 * the coins come in this kind of transaction will be tagged as TTT until a certain change or/and time to be able to return to normal coins
 * if sBM(survival By Minutes) is setted, the longivity of transparency will be started/renewd from transaction creationDate(in fact block.creationDate) 
 * if sBC(survival By Change) is setted, the longivity of transparency will be started/renewd from transaction and will be counted from container transaction(the container is index zero)
 * sCC(survival Conditions Combination) can be OR: for "or", AD: for "and" between sBM and sBC
 * 
 * if non of those 3(sBM, sBC, sCC) not mentioned in transaction but the trxType is "TTT" the 3 parameteres must be retieved from inputs.
 * and the STRICTEST rolls MUST be applied on new transaction
 * it means AND is prior than OR
 * heigher sBM or sBC will be based of calculating
 * 
 * efter expiring sBM, sBC the new transaction can nulled the dAttr
 * 
 * 
 * TODO: implementation & tests should take 16 hours, 
 */

let trx = {
    hash: "0000000000000000000000000000000000000000000000000000000000000000",
    dLen: "0000000",
    dType: iConsts.DOC_TYPES.BasicTx,
    dClass: iConsts.TRX_CLASSES.SimpleTx,
    dVer,
    dAttr: [iConsts.TRX_ATTRS.TTT],
    sBM: 60 * 24 * 30, // a time by minute
    sBC: 6, // changing between two parties
    sCC: 'OR', // either after 6 changing time or one month
    dPIs: ['0000000000000000000000000000000000000000000000000000000000000000'],   // data & process cost, which are payed by one output(or more output for cloned transactions)
    ref,
    description,
    creationDate,
    inputs: inputsTuple,
    outputs,
    dExtInfo,
    dExtHash: '0000000000000000000000000000000000000000000000000000000000000000',
};
