const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const model = require("../models/interface");



class InputTimeLockHandler {

    static canHaveInputTimeLock(trx) {
        return (
            iutils.isBasicTransaction(trx.dType) &&
            [iConsts.TRX_CLASSES.SimpleTx, iConsts.TRX_CLASSES.P4P].includes(trx.dClass)
        );
    }

    static validateInputTimeLock(args) {
        console.log(`validate Input Time Lock args: ${utils.stringify(args)}`);
    }

}

module.exports = InputTimeLockHandler;
