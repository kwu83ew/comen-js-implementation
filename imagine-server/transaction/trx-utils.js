const _ = require('lodash');
const utils = require("../utils/utils");
const crypto = require("../crypto/crypto")

function mofnPresenter(trx) {
    let out = `\n------- M of N Presentation -------------------------------------------------------------------`;
    out += `\nAccount Address: ${trx.accountAddress}`;
    if (_.has(trx, 'merkleRoot'))
        out += `\nMerkle Root: ${trx.merkleRoot}`;
    out += `\nuSets: ${Object.keys(trx.uSets).length}`;
    out += `\nMerkle Version: ${trx.merkleVersion} `; //\n  Merkle Root: ${trx.merkleRoot} 

    let signSetInx = 0;
    trx.uSets.forEach(value => {
        signSetInx += 1;
        out += `\n  signSet ${signSetInx}`;
        value.sSets.forEach(kpr => {
            out += `\n\t\t\t${kpr[0]}`;
            if (kpr.signSetTime > 0)
                out += `\tsignSet: ${kpr[1]} Minute`;
        });
        if (!utils._nilEmptyFalse(value.lHash))
            out += `\n\t\tleftHash: ${value.lHash}`;
        out += `\n\t\tProofs:`
        if (utils._nilEmptyFalse(value.proofs)) {
            out += ` null`
        } else {
            value.proofs.forEach(prf => {
                out += `\n\t\t\t${prf}`
            });

        }
    });
    return out;
}

module.exports.mofnPresenter = mofnPresenter