const trxHashHandler = require("../hashable");
const utils = require("../../utils/utils");

function validateGeneralRulsForTransaction(blockHash, transaction) {

    let localHash = trxHashHandler.doHashTransaction(transaction);
    if (localHash != transaction.hash) {
        msg = `Mismatch trx hash locally recalculated:${localHash} received: ${transaction.hash} block(${utils.hash6c(blockHash)})`;
        return { err: true, msg: msg };
    }


    if (utils.isGreaterThanNow(transaction.creationDate)) {
        msg = `Transaction whith future creation date is not acceptable ${transaction.hash} ${transaction.creationDate}`
        clog.sec.error(msg)
        return { err: true, msg: msg }
    }


    return { err: false };
}

module.exports = validateGeneralRulsForTransaction;
