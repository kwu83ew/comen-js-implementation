const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils');
const iutils = require('../../utils/iutils');
const clog = require('../../loggers/console_logger');
const machine = require('../../machine/machine-handler');
const cnfHandler = require('../../config/conf-params');


function calcTrxDPCost(args) {
    clog.trx.info(`calc TrxDPCost args: ${utils.stringify(args)}`);
    let cDate = args.cDate;
    let trx = args.trx;
    let dLen = parseInt(trx.dLen);

    if (args.stage == iConsts.STAGES.Creating)
        dLen += args.extraLength + iConsts.TRANSACTION_PADDING_LENGTH;

    if (args.stage == iConsts.STAGES.Validating) {
        if (dLen != utils.stringify(trx).length)
            return { err: true, msg: `${args.stage} the trx len dLen(${dLen})!=calculated length(${utils.stringify(trx).length}): ${utils.stringify(trx)}` }
    } else {
        if (dLen < utils.stringify(trx).length)
            return { err: true, msg: `${args.stage} the trx len dLen(${dLen})!=calculated length(${utils.stringify(trx).length}): ${utils.stringify(trx)}` }
    }

    if (trx.dClass == iConsts.TRX_CLASSES.P4P) {
        dLen = dLen * args.supportedP4PTrxLength;  // the transaction which new transaction is going to pay for
    }

    let theCost =
        dLen *
        cnfHandler.getBasePricePerChar({ cDate }) *
        cnfHandler.getDocExpense({ cDate, dType: trx.dType, dClass: trx.dClass, dLen });

    if (args.stage == iConsts.STAGES.Creating)
        theCost = theCost * machine.getMachineServiceInterests({
            dType: trx.dType,
            dClass: trx.dClass,
            dLen,
            extraLength: args.extraLength,
            supportedP4PTrxLength: args.supportedP4PTrxLength
        });

    return { err: false, cost: Math.floor(theCost) };
}

module.exports = calcTrxDPCost;
