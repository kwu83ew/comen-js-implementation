const _ = require('lodash');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const mOfNHandler = require("../../transaction/signature-structure-handler/general-m-of-n-handler");
const clog = require('../../loggers/console_logger');
const inOutSignHandler = require("../input-output-signer");
const inputTimeLockHandler = require("../../transaction/input-time-lock-handler");

class TrxSigValidator {
    
    static trxSignatureValidator(args) {
        // clog.trx.info(`trx Signature Validator args:${utils.stringify(args)}`);
        let block = args.block;
        let trx = block.docs[args.docInx];
        let unlockInfo = block.bExtInfo[args.docInx];
        let refLocsDict = args.coinsDict;
        let excludedRefLocs = _.has(args, 'excludedRefLocs') ? args.excludedRefLocs : [];

        let msg;
        // console.log('++++++++++++++++++++++++++++++++++++', JSON.stringify(args));
        let nowT = utils.getNow();

        // for each input must control if given unlock structutr will be finished in a right(and valid) output address?
        // hte order of inputs and ext Info ARE IMPORTANT. the wallet MUST sign and send inputs in order to bip 69
        clog.app.info(`validating trx:${utils.hash6c(trx.hash)}`);
        let theRefLocs_mustBeSignedByASingleSignSet = {}
        for (let inputIndex = 0; inputIndex < trx.inputs.length; inputIndex++) {
            // for each input must find proper block and bech32 address of output and insert into validate function
            let refLoc = iutils.packCoinRef(trx.inputs[inputIndex][0], trx.inputs[inputIndex][1]);
            // scape validating invalid inputs(in this case double-spended inputs)
            if (excludedRefLocs.includes(refLoc))
                continue;

            let anUnlockSet = unlockInfo[inputIndex].uSet;

            let isValidUnlock = mOfNHandler.validateSigStruct({
                address: refLocsDict[refLoc].utOAddress,
                uSet: anUnlockSet
            });
            if (isValidUnlock != true) {
                msg = `block(${utils.hash6c(block.blockHash)}) Invalid! given unlock structure for trx(${utils.hash6c(trx.hash)}) 
            input(${inputIndex}) unlock:${utils.stringify({
                    address: refLocsDict[refLoc].utOAddress,
                    anUnlockSet
                })}`;
                console.log(`msg ${msg} oo`);
                clog.sec.error(msg);
                return { err: true, msg: msg, shouldPurgeMessage: true }
            }

            // prepare a signature dictionary
            theRefLocs_mustBeSignedByASingleSignSet[inputIndex] = {}
            for (let singatureInx = 0; singatureInx < anUnlockSet.sSets.length; singatureInx++) {
                let sigInfo = unlockInfo[inputIndex].signatures[singatureInx];
                let signetInputs = inOutSignHandler.extractInputOutputs({
                    inputs: trx.inputs,
                    outputs: trx.outputs,
                    sigHash: sigInfo[1],
                }).inputs;
                theRefLocs_mustBeSignedByASingleSignSet[inputIndex][singatureInx] = signetInputs.map(x => iutils.packCoinRef(x[0], x[1]));
            }
        }


        for (let inputIndex = 0; inputIndex < trx.inputs.length; inputIndex++) {
            // for each input must find proper block and bech32 address of output and insert into validate function
            let refLoc = iutils.packCoinRef(trx.inputs[inputIndex][0], trx.inputs[inputIndex][1]);
            // scape validating invalid inputs(in this case double-spended inputs)
            if (excludedRefLocs.includes(refLoc))
                continue;

            let anUnlockSet = unlockInfo[inputIndex].uSet;

            // for each input and proper spending way, must control if the signature is valid?
            for (let singatureInx = 0; singatureInx < anUnlockSet.sSets.length; singatureInx++) {
                let sigInfo = unlockInfo[inputIndex].signatures[singatureInx];
                let aSignSet = anUnlockSet.sSets[singatureInx];
                let inOutSig = inOutSignHandler.verifyInputOutputsSignature({
                    signature: sigInfo[0],
                    sigHash: sigInfo[1],
                    creationDate: trx.creationDate,
                    inputs: trx.inputs,
                    outputs: trx.outputs,
                    pubKey: aSignSet.sKey,
                });
                if (inOutSig != true) {
                    msg = `block(${utils.hash6c(block.blockHash)}) Invalid given signature for trx(${utils.hash6c(trx.hash)}) input(${inputIndex})`;
                    clog.sec.error(msg);
                    return { err: true, msg, shouldPurgeMessage: true }
                }

                if (inputTimeLockHandler.canHaveInputTimeLock(trx)) {
                    // control input timeLocks
                    if (_.has(aSignSet, 'iTLock') && (aSignSet.iTLock > 0)) {
                        clog.trx.info(`>>>>>>>>>>>>> signed RefLocs By A Single SignSet ${utils.stringify(theRefLocs_mustBeSignedByASingleSignSet)}`);
                        let iTLock = iutils.convertBigIntToJSInt(aSignSet.iTLock);
                        for (aRefLoc of theRefLocs_mustBeSignedByASingleSignSet[inputIndex][singatureInx]) {
                            let inputCreationDate = refLocsDict[aRefLoc].utRefCreationDate;
                            let inputRedeemDate = utils.minutesAfter(iTLock, inputCreationDate);

                            msg = `info::: inputTimeLock compareTime(${nowT}) block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)}) `;
                            msg += `input(${inputIndex} locked for ${iTLock} Minutes after creation) created on(${inputCreationDate}) `;
                            msg += `has inputTimeLock and can be redeemed after(${inputRedeemDate}) `;
                            if (nowT < inputRedeemDate) {
                                msg = `\ninput is not useable because of ${msg}`;
                                clog.sec.error(msg);
                                return { err: true, msg, shouldPurgeMessage: true }
                            } else {
                                msg = `\ninput is released ${msg}`;
                                clog.trx.info(msg);
                            }
                        }
                    }
                }

            }
        }

        return { err: false, msg: `block(${utils.hash6c(block.blockHash)}) all trx has valid signature`, shouldPurgeMessage: true }
    }
}

module.exports = TrxSigValidator;
