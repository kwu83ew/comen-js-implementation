const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require("../../utils/utils");
const iutils = require("../../utils/iutils");
const clog = require('../../loggers/console_logger');

function trxUTXOsValidator(block, dblSpendReferences=[]) {
    let msg;
    let currentAncestors = block.ancestors;
    let inputsSum = {};
    let outputsSum = {};
    let maybeDoublespended = [];
    let usedRefsInCurrentBlock = [];

    // retrieve all usedRefsInCurrentBlock. i.e. all outputs which are reffered by inputs of this block
    let mapRefToTrxs = {}
    block.docs.forEach(trx => {
        trx.inputs.forEach(input => {
            let ref = iutils.packCoinRef(input[0], input[1]);
            usedRefsInCurrentBlock.push(ref);
            mapRefToTrxs[ref] = trx.hash
        });
    });
    clog.trx.info(`usedRefsInCurrentBlock: ${usedRefsInCurrentBlock}`);

    





    // out of 12h control
    // let invalidRefOutOf12H = false;
    // let analyzeRefsRes = utxoHandler.analyzeRefs({
    //     block: block,
    //     candidRefs: usedRefsInCurrentBlock,
    //     onlyOver12HDiff: true,
    //     inside12H: conflictedOutputsInsideLast12H
    // });
    // clog.trx.info(`analyzeRefsRes: ${JSON.stringify(analyzeRefsRes)}`)
    // if (_.has(analyzeRefsRes, 'err') && (analyzeRefsRes.err != false))
    //     return analyzeRefsRes;

    // if (_.has(analyzeRefsRes, 'localMissedSomeBlocks')) {
    //     msg = `The Block(${utils.hash6c(block.blockHash)}) missed some block for refLocs(${analyzeRefsRes.missedRefs.map(x => iutils.shortCoinRef(x)).join()}).
    //         so giving time to local machine to download missed block(s)`;
    //     clog.trx.warn(msg);
    //     // return and give time to machine to dwonload missed block
    //     return { err: true, msg: msg, shouldPurgeMessage: false };
    // }

    // if (analyzeRefsRes.invalidRefs.length > 0) {
    //     invalidRefOutOf12H = true;
    //     // if (_.has(res12H, 'err'))
    //     //     return res12H
    //     // return { err: true, msg: 'some uncomplete codeing!', shouldPurgeMessage: false }
    // }

    // // thtere is no double spending :)
    // if (!invalidRefOutOf12H && !invalidRefsInside12H)
    //     return { err: false, shouldPurgeMessage: true };

    // if (invalidRefOutOf12H && !invalidRefsInside12H) {
    //     // if there is not eve one conflict with in 12 hour and all conflicts are about out of 12h, drop block if hasn't descendents
    //     // in fact local machine aimed the first block(which is received 12 hour bfore) is right one
    //     // and the new one(which receive to this machine after 12 hours) is double-spended found
    //     // unless the block has a descendent link which means this block received to another machine between 12 hours
    //     // and that machine considered the block as double-spending and justified block by a tail sus-block
    //     let blockUnsettedDescendents = false;
    //     if (!_.has(block, 'descendents') || utils._nilEmptyFalse(block.descendents) || (block.descendents.length == 0))
    //         blockUnsettedDescendents = true;
    //     clog.trx.warn(`blockUnsettedDescendents: ${blockUnsettedDescendents}`)

    //     if (blockUnsettedDescendents) {
    //         msg = `The Block(${utils.hash6c(block.blockHash)}) has some double-spend refs older than 12 hours 
    //         ${analyzeRefsRes.invalidRefs.map(x => iutils.shortCoinRef(x)).join()}`
    //         clog.trx.warn(msg)
    //         clog.sec.warn(msg)
    //         return { err: true, shouldPurgeMessage: true, msg: msg };
    //     }
    // }

    // if we are here means at least one node in imagine network got 2 conflicted blocks in less than 12 hour time difference 
    // between creationDate of first block and receiveDate of second block. 
    // so local machine has to do some controls

    // let invalidRefs = utils.arrayUnique(utils.arrayAdd(conflictedOutputsInsideLast12H, analyzeRefsRes.invalidRefs))

    // // if creationDate of current block is greter than even 
    // one of blocks(contained conflicted transactions) 
    // // so machine must drop this block
    // let ref24 = utxo24Handler.search({ refLocs: invalidRefs })
    // let conflictContainerBlocks = []
    // let alreadyRecordedConflictedTrxs = []
    // ref24.forEach(element => {
    //     conflictContainerBlocks.push(element.block_hash)
    //     alreadyRecordedConflictedTrxs.push(element.trx_hash)
    // });
    // conflictContainerBlocks = utils.arrayUnique(conflictContainerBlocks)
    // // so differnece of creationDate of both blocks MUST be less than 12 hours



    // // control UTXO accessibility & visibility
    // for (let trxInx = 0; trxInx < block.docs.length; trxInx++) {
    //     let trx = block.docs[trxInx];
    //     inputsSum[trxInx] = 0;
    //     outputsSum[trxInx] = 0;
    //     clog.trx.info(`validating trx ${utils.hash6c(trx.hash)}`)
    //     for (let inputInx = 0; inputInx < trx.inputs.length; inputInx++) {
    //         // control UTXO accessibility & visibility
    //         let refCode = iutils.packCoinRef(trx.inputs[inputInx][0], trx.inputs[inputInx][1]);
    //         clog.trx.info(`validating input ${inputInx} Refers to: ${utils.hash6c(trx.inputs[inputInx][0])}:${trx.inputs[inputInx][1]}`)
    //         let utInfo = utxoHandler.getOneRowUTXOInfoSync({ refLoc: refCode, visibleBy: currentAncestors });
    //         clog.trx.info(`utInfo utInfo utInfo ${JSON.stringify(utInfo)}`);
    //         if (utInfo.length == 0) {
    //             // check if the refCode container block exist in local?
    //             let containerBlockHashes = dagHandler.retrieveBlocksInWhichARefLocHaveBeenProduced(refCode).blockHashes;

    //             maybeDoublespended.push({ trxInx, inputInx, refCode });

    //         }

    //         inputsSum[trxInx] += utInfo[0].o_value;
    //     }

    //     for (let outputInx = 0; outputInx < trx.outputs.length; outputInx++) {
    //         outputsSum[trxInx] += trx.outputs[outputInx].value;
    //     }
    // }



}

module.exports = trxUTXOsValidator
