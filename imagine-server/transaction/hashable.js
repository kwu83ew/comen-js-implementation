const iConsts = require('../config/constants');
const crypto = require('../crypto/crypto');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const merkleHandler = require('../crypto/merkle/merkle');
const clog = require('../loggers/console_logger');
const machine = require('../machine/machine-handler');
const vHandler = require('../services/version-handler/version-handler');

class TrxHashHandler {
    static extractTransactionHashableParts(trx) {
        switch (trx.dType) {
            case iConsts.DOC_TYPES.BasicTx:
                return { needPure: true, hashables: this.extractHParts_simple(trx) };
                break;

            case iConsts.DOC_TYPES.Coinbase:
                return { needPure: false, hashables: this.extractHParts_coinbase(trx) };
                break;
            case iConsts.DOC_TYPES.RpDoc:
                return { needPure: false, hashables: this.extractHParts_RpDoc(trx) };
                break;

            case iConsts.DOC_TYPES.DPCostPay:
                return { needPure: false, hashables: this.extractHParts_DPCostPay(trx) };
                break;
        }
    }

    static extractTransactionPureHashableParts(trx) {
        switch (trx.dType) {
            case iConsts.DOC_TYPES.BasicTx:
                return this.extractHPureParts_simple(trx);
                break;

        }
    }

    static normalizeInputs(inputs = [], sortIt = true) {
        // BIP69
        let normalizedInputs = [];
        for (let anInput of inputs) {
            // entry control
            if (anInput.length != 2)
                return { err: true, msg: `the input ${utils.stringify(inputs)} is invalid` }

            normalizedInputs.push(anInput);
        }

        if (sortIt && (normalizedInputs.length > 1)) {
            let dict = {};
            for (let anInput of normalizedInputs) {
                let key = [anInput[0], anInput[1].toString().padStart(5, '0')].join()
                dict[key] = anInput
            };
            normalizedInputs = [];
            for (let k of utils.objKeys(dict).sort()) {
                normalizedInputs.push(dict[k]);
            }
        }

        return { err: false, normalizedInputs };
    }

    static getOutputIndexByAddressValue(outputs, addressValue) {
        for (let inx = 0; inx < outputs.length; inx++) {
            if (outputs[inx].join() == addressValue)
                return inx;
        }
        return null;
    }

    static normalizeOutputs(outputs = [], sortIt = true) {
        // BIP69
        let msg;
        let normalizedOutputs = [];
        for (let anOutput of outputs) {

            // entry control
            if (anOutput.length != 2) {
                msg = `invalid trx output ${utils.stringify(anOutput)}`;
                clog.trx.error(msg);
                return { err: true, msg };
            }

            // address control
            if (!iConsts.TREASURY_PAYMENTS.includes(anOutput[0]) && !crypto.bech32_isValidAddress(anOutput[0])) {
                msg = `invalid trx output ${utils.stringify(anOutput)}`;
                clog.trx.error(msg);
                return { err: true, msg };
            }

            // value controll
            // TODO: implement some control like no-floatingpoint-part etc ...

            normalizedOutputs.push(anOutput);

        }

        if (sortIt && (normalizedOutputs.length > 1)) {
            let dict = {};
            for (let inx = 0; inx < normalizedOutputs.length; inx++) {
                let anOutput = normalizedOutputs[inx];
                let key = [anOutput[0], anOutput[1].toString().padStart(20, '0'), inx.toString().padStart(5, '0')].join();
                dict[key] = anOutput
            };
            normalizedOutputs = []
            for (let key of utils.objKeys(dict).sort()) {
                normalizedOutputs.push(dict[key]);
            }
        }

        return { err: false, normalizedOutputs };
    }

    static calcTrxExtRootHash(extInfos) {
        let hashes = [];
        for (let anExtInfo of extInfos) {
            hashes.push(iutils.doHashObject(anExtInfo));
        }
        return merkleHandler.merkle(hashes).root;
    }

    static extractHParts_simple(trx) {
        // the hTrx MUST be constant and NEVER change the order of attribiutes (alphabetical)
        // in case of change the version MUST be changed and the code treat it in new manner

        let hahsableTrx = {
            creationDate: trx.creationDate,
            description: trx.description,
            dClass: trx.dClass,
            dLen: trx.dLen,
            dPIs: trx.dPIs,
            dType: trx.dType,
            dVer: trx.dVer,
            dExtHash: trx.dExtHash,
            inputs: trx.inputs,
            outputs: trx.outputs,
            pureHash: trx.pureHash,
            ref: trx.ref
        };
        return hahsableTrx;
    }

    // TODO: some unit test for pure hashable
    static extractHPureParts_simple(trx) {
        // the hTrx MUST be constant and NEVER change the order of attribiutes (alphabetical)
        // in case of change the version MUST be changed and the code treats it in new manner
        clog.app.info(`ytrxyyyyyyyyyyyyy1 ${utils.stringify(trx)}`);
        let inputNormRes = this.normalizeInputs(trx.inputs, true);
        clog.app.info(`ytrxyyyyyyyyyyyyy2 ${utils.stringify(inputNormRes)}`);
        
        if (inputNormRes.err != false)
            return inputNormRes;
        clog.app.info(`ytrxyyyyyyyyyyyyy3`);
        clog.app.info(`ytrxyyyyyyyyyyyyy33 ${vHandler.isNewerThan(trx.dVer, '0.0.1')}`);
        if (vHandler.isNewerThan(trx.dVer, '0.0.1')) {
            let outputNormRes = this.normalizeOutputs(trx.outputs, true);
            clog.app.info(`ytrxyyyyyyyyyyyyy4 ${utils.stringify(outputNormRes)}`);
            if (outputNormRes.err != false)
                return outputNormRes;

            let hahsableTrx = {
                inputs: inputNormRes.normalizedInputs,
                outputs: outputNormRes.normalizedOutputs
            };
            clog.app.info(`extract H Pure Parts _simple ${utils.stringify(hahsableTrx)} `);
            return hahsableTrx;

        } else {
            return inputNormRes.normalizedInputs;

        }


    }

    static extractHParts_coinbase(trx) {
        // the hTrx MUST be constant and NEVER change the order of attribiutes
        // in case of change the version MUST be changed and the code treat it in new manner
        // clog.app.info('--------------trx----------------');

        let hahsableTrx = {
            creationDate: trx.creationDate,
            cycle: trx.cycle,
            dClass: trx.dClass,
            dType: trx.dType,
            dVer: trx.dVer,
            mintedCoins: trx.mintedCoins,
            outputs: trx.outputs,
            treasuryFrom: trx.treasuryFrom,
            treasuryIncomes: trx.treasuryIncomes,
            treasuryTo: trx.treasuryTo
        };
        return hahsableTrx;
    }

    static extractHParts_RpDoc(rpDoc) {
        // alphabetical order
        let hahsableTrx = {
            cycle: rpDoc.cycle,
            dClass: rpDoc.dClass,
            dType: rpDoc.dType,
            inputs: rpDoc.inputs,
            outputs: rpDoc.outputs,
        }
        return hahsableTrx;
    }

    static extractHParts_DPCostPay(trx) {
        // the hTrx MUST be constant and NEVER change the order of attribiutes
        // in case of change the version MUST be changed and the code treat it in new manner

        let hahsableTrx = {
            creationDate: trx.creationDate,
            dClass: trx.dClass,
            dLen: trx.dLen,
            dType: trx.dType,
            dVer: trx.dVer,
            outputs: trx.outputs
        };
        return hahsableTrx;
    }

    static doHashTransaction(trx) {
        let hashInfo = this.extractTransactionHashableParts(trx);
        let hash = crypto.keccak256Dbl(utils.stringify(hashInfo.hashables));    // NOTE: absolutely using double hash for more security
        if (hashInfo.needPure) {
            // generate deterministic part of trx hash
            let pureHash = this.getPureHash(trx);
            // console.log('pureHash', pureHash);
            if (vHandler.isNewerThan(trx.dVer, '0.0.0')) {
                hash = [pureHash.toString().substr(32), hash.toString().substr(32)].join('');
            } else {
                hash = [pureHash.toString().substr(0, 32), hash.toString().substr(0, 32)].join('');
            }
        }
        return hash;
    }

    static getPureHash(trx) {
        let hashableTrx = this.extractTransactionPureHashableParts(trx);
        clog.app.info(`hashableTrxXXXXXXXXXXXXXXXX ${utils.stringify(hashableTrx)}`);
        return crypto.keccak256Dbl(utils.stringify(hashableTrx));    // NOTE: useing double hash for more security
    }


}

module.exports = TrxHashHandler;
