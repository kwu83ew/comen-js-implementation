const _ = require('lodash');
const iConsts = require('../config/constants');
const utils = require('../utils/utils');
const iutils = require('../utils/iutils');
const clog = require('../loggers/console_logger');
const model = require("../models/interface");
const dagHandler = require("../dag/graph-handler/dag-handler");
const utxoHandler = require("./utxo/utxo-handler");
const suspectTrxHandler = require("./utxo/suspect-transactions-handler");


const table = 'i_trx_output_time_locked';
const logTable = 'i_logs_time_locked';

class OutputTimeLockHandler {

    static logOTimeLock(args) {
        let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
        model.sCreate({
            table: logTable,
            values: {
                lkey: args.lkey,
                block_hash: args.blockHash,
                doc_hashes: args.docHashes,
                ref_locs: args.refLocs,
                log_body: args.logBody,
                creation_date: creationDate
            }
        });
    }

    static insertATimeLocked(args) {
        model.sCreate({
            table,
            values: {
                'block_hash': args.blockHash,
                'doc_hash': args.docHash,
                'pure_hash': args.pureHash,
                'ref_loc': args.refLoc,
                'doc_body': utils.stringify(args.doc),
                'doc_max_redeem': args.docMaxRedeem,
                'redeem_time': args.redeemTime,
                'cycle': args.cloneCode,
                'ref_creation_date': args.refCreationDate
            }
        });
    }

    static importTimeLocked() {
        // control if there is some importable record?
        let nowT = utils.getNow();
        let res = model.sRead({
            table,
            query: [
                ['redeem_time', ['<', nowT]],   // this redeem_time claimed by client(and could be wrong)
                ['utxo_imported', iConsts.CONSTS.NO],
            ],
            order: [['redeem_time', 'ASC']]
        });
        if (res.length == 0) {
            clog.trx.info(`There is no importable redeemed utxo before (${nowT})`);
            return;
        }

        // retrieve entire redeems of engaged refLocs & docHashes
        let refLocs = [];
        let docHashes = [];
        for (let aRecord of res) {
            refLocs.push(aRecord.ref_loc);
            docHashes.push(aRecord.doc_hash);
        }
        refLocs = utils.arrayUnique(refLocs)
        docHashes = utils.arrayUnique(docHashes)

        // retreive all related records
        let potentialyImportables = model.sRead({
            table,
            query: [
                ['ref_loc', ['IN', refLocs]],
                ['doc_hash', ['IN', docHashes]]
            ],
            order: [['redeem_time', 'ASC']]
        });
        clog.trx.info(`import redeemed refLocs(${refLocs.map(x => iutils.shortCoinRef(x))}) docHashes(${docHashes.map(x => utils.hash6c(x))})  potentialyImportables: ${JSON.stringify(potentialyImportables)}`);

        // find max redeem for each doc/transaction
        let maxRdmForADoc = {};
        for (let aRecord of potentialyImportables) {
            console.log('aRecord', aRecord);
            if (!_.has(maxRdmForADoc, aRecord.doc_hash))
                maxRdmForADoc[aRecord.doc_hash] = aRecord.redeem_time;// this redeem_time claimed by client(and could be wrong)
            if (maxRdmForADoc[aRecord.doc_hash] < aRecord.redeem_time)
                maxRdmForADoc[aRecord.doc_hash] = aRecord.redeem_time;
        }
        console.log('maxRdmForADoc', maxRdmForADoc);
        let importableDocs = []
        for (let docHash of utils.objKeys(maxRdmForADoc)) {
            if (maxRdmForADoc[docHash] < nowT)
                importableDocs.push(docHash);
        }
        if (importableDocs.length == 0) {
            clog.trx.info(`There is no fully importable redeemed doc`);
            return;
        }
        clog.trx.info(`importableDocs: ${importableDocs.map(x => utils.hash6c(x)).join()}`);


        let docsDict = {};
        for (let aRecord of potentialyImportables) {
            if (!importableDocs.includes(aRecord.doc_hash))
                continue;

            aRecord.doc_body = utils.parse(aRecord.doc_body)
            if (!_.has(docsDict, aRecord.doc_hash))
                docsDict[aRecord.doc_hash] = [];
            docsDict[aRecord.doc_hash].push(aRecord);
        }
        clog.trx.info('docsDict', docsDict);


        for (let docHash of utils.objKeys(docsDict)) {
            // analyze votes and decide about dobiuos transactions
            let validity = suspectTrxHandler.checkDocValidity({ doLog: true, invokedDocHash: docHash });
            clog.trx.info(`sus-validity: ${utils.stringify(validity)}`);

            if (_.has(validity, 'allCoinsAreValid')) {
                // there is no conflict for given doc, so it could be imported
                // for (let entry of docsDict[docHash]) {
                //     clog.trx.info('entry', entry);
                //     let wBlocks = dagHandler.walkThrough.getAllDescendents({blockHash: entry.block_hash});
                //     wBlocks.forEach(wBlockEFS => {
                //         for (let outputIndex = 0; outputIndex < entry.doc_body.outputs.length; outputIndex++) {
                //             let utxo = entry.doc_body.outputs[outputIndex];
                //             let newCoin = iutils.packCoinRef(entry.doc_body.hash, parseInt(outputIndex));
                //             let uArgs = {
                //                 cloneCode: entry.cycle,
                //                 refLoc: newCoin,
                //                 creationDate: wBlockEFS.creation_date,
                //                 visibleBy: wBlockEFS.block_hash,
                //                 address: utxo[0],
                //                 value: utxo[1].toString(),
                //                 refCreationDate: entry.ref_creation_date
                //             };
                //             clog.trx.info(`insert new utxo(NB) ${JSON.stringify(uArgs)}`);
                //             utxoHandler.add NewUTXO(uArgs)
                //         }
                //     });
                // }
                model.sUpdate({
                    table,
                    query: [
                        ['doc_hash', docHash],
                    ],
                    updates: { utxo_imported: iConsts.CONSTS.YES }
                });
                continue;
            } else {

            }
        }
        return

        // grouping by pure hash
        let pureHashDict = {}
        for (let docHash of utils.objKeys(docsDict)) {
            for (let aRec of docsDict[docHash]) {
                if (!_.has(pureHashDict, aRec.pure_hash))
                    pureHashDict[aRec.pure_hash] = [];
                pureHashDict[aRec.pure_hash].push(aRec);
            }
        }
        clog.trx.info('pureHashDict', pureHashDict);
        this.logOTimeLock({
            lkey: '1. pureHashDict',
            blockHash: '-',
            docHashes: utils.objKeys(docsDict).map(x => utils.hash6c(x)).join(),
            refLocs: '-',
            logBody: JSON.stringify(pureHashDict)
        });

        for (let pureHash of utils.objKeys(pureHashDict)) {
            if (pureHashDict[pureHash].length == 1) {
                // there is only one uniq transaction, and it is importable so import it
                // we must be sure the block and entire it's precedents has visibility of these UTXOs
                let entry = pureHashDict[pureHash][0];
                console.log('entry', entry);
                let wBlocks = dagHandler.walkThrough.getAllDescendents({ blockHash: entry.block_hash }).wBlocks;
                wBlocks.forEach(wBlockEFS => {
                    for (let outputIndex = 0; outputIndex < entry.doc_body.outputs.length; outputIndex++) {
                        let utxo = entry.doc_body.outputs[outputIndex];
                        let newCoin = iutils.packCoinRef(entry.doc_body.hash, parseInt(outputIndex));
                        let uArgs = {
                            cloneCode: entry.cycle,
                            refLoc: newCoin,
                            creationDate: wBlockEFS.creation_date,
                            visibleBy: wBlockEFS.block_hash,
                            address: utxo[0],
                            value: utxo[1].toString(),
                            refCreationDate: entry.ref_creation_date
                        };
                        clog.trx.info(`insert new utxo(NB) ${JSON.stringify(uArgs)}`);
                        utxoHandler.addNewUTXO(uArgs)
                    }
                });

                // update as an imported utxo
                continue;
                model.sUpdate({
                    table,
                    query: [
                        ['pure_hash', entry.pure_hash],
                    ],
                    updates: { utxo_imported: iConsts.CONSTS.YES }
                });

            } else {
                // machine has to chose the lowest-maxRedeem for each pureHash
                // if ther are more than one lowest-maxRedeem for each pureHash, must decide based on susVote mechanisem




                continue;
                // update as an imported utxo
                model.sUpdate({
                    table,
                    query: [
                        ['pure_hash', entry.pure_hash],
                    ],
                    updates: { utxo_imported: iConsts.CONSTS.YES }
                });


            }
        }


    }

    static isTimeLockRelated(refLocs) {
        let res = model.sRead({
            table,
            query: [
                ['ref_loc', ['IN', refLocs]]
            ],
            order: [['doc_max_redeem', 'ASC']]
        });
        return (res.length > 0);
    }

    static getDocInputsAndMaxRedeem(args) {
        let doc = args.doc;
        let docExtInfo = args.docExtInfo;

        let docMaxRedeem = 0;
        let docInputs = [];
        if (_.has(doc, 'inputs')) {
            for (let inputIndex = 0; inputIndex < doc.inputs.length; inputIndex++) {
                docInputs.push(iutils.packCoinRef(doc.inputs[inputIndex][0], doc.inputs[inputIndex][1]));
                if (_.has(docExtInfo, inputIndex)) {
                    for (let aSignature of docExtInfo[inputIndex].uSet.sSets) {
                        if (docMaxRedeem < iutils.convertBigIntToJSInt(aSignature[2]))
                            docMaxRedeem = iutils.convertBigIntToJSInt(aSignature[2]);
                    };
                }
            }
        }
        return { docInputs, docMaxRedeem };
    }
}

module.exports = OutputTimeLockHandler;
