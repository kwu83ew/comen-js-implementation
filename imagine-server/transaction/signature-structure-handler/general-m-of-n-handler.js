const _ = require('lodash');
const iConsts = require('../../config/constants');
const utils = require('../../utils/utils')
const crypto = require('../../crypto/crypto')
const clog = require('../../loggers/console_logger');

// TODO: optimization to using Buffer

class SignatureStructureHandler {

    /*
    this is the main func to create m of n address and proper unlock structures
     */
    static createCompleteUnlockSets(args) {
        let sSets = args.sSets;
        let neccessarySignaturesCount = args.neccessarySignaturesCount;
        let sType = _.has(args, 'sType') ? args.sType : false;
        let sVer = _.has(args, 'sVer') ? args.sVer : false;
        let customSalt = _.has(args, 'customSalt') ? args.customSalt : false;
        let customSalts = (customSalt) ? {} : false;
        let customTypes = (sType && sVer) ? {} : false;

        let handlerHashes = sSets.map(x => x[0]);
        let signersByKey = {}
        for (let aSigner of sSets) {
            signersByKey[aSigner[0]] = aSigner[1];
        };

        // generate permutaion of signatures. later will be used as tree leaves
        let hp = new utils.heapPermutation();
        hp.shouldBeUnique = true;
        hp._heapP(handlerHashes, neccessarySignaturesCount);
        let uSets = []
        for (let an_unlock of hp.premutions) {
            let a_signature_combination = []
            for (let handlerHash of an_unlock) {
                a_signature_combination.push(signersByKey[handlerHash])
            };
            uSets.push(a_signature_combination);

            let customKey = utils.hash16c(crypto.keccak256(JSON.stringify(a_signature_combination)));
            if (sType && sVer) {
                customTypes[customKey] = {
                    sType, sVer
                }
            }

            if (customSalt) {
                if (customSalt == 'PURE_LEAVE') {
                    customSalts[customKey] = customKey;
                } else {
                    customSalts[customKey] = customSalt;
                }
            }
        };

        // console.log(`\n ----uSets: ${utils.stringify(uSets)}`);
        let trxUnlockMerkle = SignatureStructureHandler.createMOfNMerkle({ uSets, customTypes, customSalts })
        return trxUnlockMerkle

    }

    /**
     * use it to create a cusomized signature combination
     * @param {*} uSets 
     * @param {*} inputType 
     * @param {*} hashAlgorithm 
     */
    static createMOfNMerkle(args) {
        let customTypes = _.has(args, 'customTypes') ? args.customTypes : false;
        let customSalts = _.has(args, 'customSalts') ? args.customSalts : false;
        let uSets = _.has(args, 'uSets') ? args.uSets : false;
        let inputType = _.has(args, 'inputType') ? args.inputType : 'hashed';
        let hashAlgorithm = _.has(args, 'hashAlgorithm') ? args.hashAlgorithm : crypto.keccak256;
        console.log(`create MOfN Merkle args ${JSON.stringify(args)}`);

        let hashedUnlocks = []
        let tmpUnlockers = {}
        // console.log(`uSets->`);
        // console.log(`\n\n uSets->${utils.stringify(uSets)}\n`);
        for (let a_signature_combination of uSets) {
            let customKey = utils.hash16c(crypto.keccak256(JSON.stringify(a_signature_combination)));

            let sType, sVer;
            if (customTypes) {
                sType = customTypes[customKey].sType;
                sVer = customTypes[customKey].sVer;
            } else {
                sType = 'Basic';
                sVer = '0.0.0';
            }

            // adding random salt to obsecure/unpredictable final address
            // TODO: maybe use crypto secure random generator
            let salt;
            if (customSalts) {
                salt = customSalts[customKey];
            } else {
                let rnd = crypto.rand();
                salt = utils.hash16c(crypto.keccak256(`${JSON.stringify(a_signature_combination)}${Date.now()}.${rnd}`));
            }
            let hashedLeave = hashAlgorithm(`${sType}:${sVer}:${JSON.stringify(a_signature_combination)}:${salt}`)
            hashedUnlocks.push(hashedLeave)
            tmpUnlockers[hashedLeave] = { sType, sVer, a_signature_combination, salt };
        }
        let merkle_tree = crypto.merkleGenerate(hashedUnlocks, inputType, hashAlgorithm);
        let unlockDocument = {
            merkleVersion: merkle_tree.version,
            merkleRoot: merkle_tree.root,
            accountAddress: crypto.bech32_encodePub(crypto.keccak256Dbl(merkle_tree.root)).encoded,   // because of securiy, MUST use double hash
            uSets: []
        }
        // asign merkle proofs to transaction itself
        for (let key of utils.objKeys(tmpUnlockers)) {
            unlockDocument.uSets.push({
                sType: tmpUnlockers[key].sType,
                sVer: tmpUnlockers[key].sVer,
                sSets: tmpUnlockers[key].a_signature_combination,
                proofs: _.has(merkle_tree.proofs, key) ? merkle_tree.proofs[key].hashes : null,
                lHash: _.has(merkle_tree.proofs, key) ? merkle_tree.proofs[key].lHash : null,
                salt: tmpUnlockers[key].salt
            });
        };
        return unlockDocument;

    };

    static validateStructureStrictions(args) {
        // console.log(`validate StructureStrictions.args: ${utils.stringify(args)}`);
        let unlock = args.uSet;

        if (utils._nilEmptyFalse(unlock))
            return false;

        switch (unlock.sType) {

            /**
             * this strict type of signature MUST have and ONLY have these 3 features
             * sKey: can be a public key(and later can be also another bech32 address, after implementing nested signature feature)
             * pPledge: means the signer Permitted to Pleadge this account
             * pDelegate: means the signer Permited to Delegate some rights (binded to this address) to others
             */
            case iConsts.SIGNATURE_TYPES.Strict:
                if (utils.hash16c(crypto.keccak256(JSON.stringify(unlock.sSets))) != unlock.salt) {
                    clog.trx.error(`invalid strict structure of signature of salt(${unlock.salt})`)
                    return false;
                }
                for (let aSignSet of unlock.sSets) {
                    if (
                        (utils.objKeys(aSignSet).length != 3) ||
                        !_.has(aSignSet, 'sKey') ||
                        !_.has(aSignSet, 'pPledge') ||
                        !_.has(aSignSet, 'pDelegate')
                    ) {
                        clog.trx.error(`invalid strict structure of signature. salt(${unlock.salt})`)
                        return false;
                    }
                }
                break;
        }
        return true;

    };

    static validateSigStruct(args) {
        return SignatureStructureHandler.validateStructureOfAnUnlockMOfN({
            uSet: {
                address: args.address,
                sType: args.uSet.sType,
                sVer: args.uSet.sVer,
                sSets: args.uSet.sSets,
                lHash: args.uSet.lHash,
                proofs: args.uSet.proofs,
                salt: args.uSet.salt
            }
        });
    };

    static validateStructureOfAnUnlockMOfN(args) {
        // console.log(`validate StructureOfAnUnlockMOfN.args: ${utils.stringify(args)}`);
        let sStrict = SignatureStructureHandler.validateStructureStrictions(args);
        if (!sStrict)
            return sStrict;

        let unlockSet = args.uSet;
        let inputType = _.has(args, 'inputType') ? args.inputType : 'hashed';
        let hashAlgorithm = _.has(args, 'hashAlgorithm') ? args.hashAlgorithm : crypto.keccak256;
        let doPermutation = _.has(args, 'doPermutation') ? args.doPermutation : false;
        try {
            // calculate leave Hash
            let merkleRoot, bech32, leaveHash;

            // normally the wallets SHOULD send the saved order of a sSets, so we do not need to Permutation
            leaveHash = hashAlgorithm(`${unlockSet.sType}:${unlockSet.sVer}:${JSON.stringify(unlockSet.sSets)}:${unlockSet.salt}`);
            merkleRoot = crypto.merkleGetRootByAProve(leaveHash, unlockSet.proofs, unlockSet.lHash, inputType, hashAlgorithm);
            if (_.has(unlockSet, 'address')) {
                bech32 = crypto.bech32_encodePub(crypto.keccak256Dbl(merkleRoot)).encoded;  // because of securiy, MUST use double hash
                if (unlockSet.address == bech32) {
                    return true;
                }
            } else if (_.has(unlockSet, 'merkleRoot')) {
                if (unlockSet.merkleRoot == merkleRoot) {
                    return true;
                }
            }

            if (!doPermutation)
                return false;

            // in case of disordinating inside sSets
            let hp = new utils.heapPermutation();
            hp.heapP(unlockSet.sSets)
            for (let premu of hp.premutions) {
                leaveHash = hashAlgorithm(`${unlockSet.sType}:${unlockSet.sVer}:${JSON.stringify(premu)}:${unlockSet.salt}`);
                merkleRoot = crypto.merkleGetRootByAProve(leaveHash, unlockSet.proofs, unlockSet.lHash, inputType, hashAlgorithm);
                bech32 = crypto.bech32_encodePub(crypto.keccak256Dbl(merkleRoot)).encoded;  // because of securiy, MUST use double hash
                if (_.has(unlockSet, 'address')) {
                    if (unlockSet.address == bech32) {
                        return true;
                    }
                } else if (_.has(unlockSet, 'merkleRoot')) {
                    if (unlockSet.merkleRoot == merkleRoot) {
                        return true;
                    }
                }
            };
            return false;
        } catch (e) {
            console.error(e);
            clog.sec.error(`validate StructureOfAnUnlockMOfN faild ${utils.stringify(args)}`);
            clog.sec.error(e);
            return false;
        }
    }

}
module.exports = SignatureStructureHandler;