const _ = require('lodash');
const utils = require("../utils/utils");
const iConsts = require('../config/constants');
const crypto = require('../crypto/crypto');
const trxHashHandler = require("./hashable");
const clog = require('../loggers/console_logger');

function extractInputOutputs(args) {
    let inputs, outputs;
    switch (args.sigHash) {
        case iConsts.SIGHASH.ALL:
            inputs = args.inputs;
            outputs = args.outputs;
            break;

        case iConsts.SIGHASH.NONE:
        case iConsts.SIGHASH.ALL_INPUTS:
            inputs = args.inputs;
            outputs = [];
            break;

        //TODO: implement CUSTOM
        case iConsts.SIGHASH.CUSTOM:
            let cust = {
                inputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4'],
                outputs: ['ALL']
            }
            cust = {
                inputs: 'ALL',
                outputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4']
            }
            cust = {
                inputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4'],
                outputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4']
            }
            inputs = args.inputs;
            outputs = args.outputs;
            break;





        // since they have conflict with BIP69 all are diabled
        // instead, use custom

        // case iConsts.SIGHASH['SINGLE']:
        //     inputs = args.inputs;
        //     outputs = [args.outputs[args.selectedIndex]];
        //     break;

        // case iConsts.SIGHASH['ALL|ANYONECANPAY']:
        //     inputs = [args.inputs[args.selectedIndex]];
        //     outputs = args.outputs;
        //     break;

        // case iConsts.SIGHASH['NONE|ANYONECANPAY']:
        //     inputs = [args.inputs[args.selectedIndex]];
        //     outputs = [];
        //     break;

        // case iConsts.SIGHASH['SINGLE|ANYONECANPAY']: 
        //     inputs = [args.inputs[args.selectedIndex]];
        //     outputs = [args.outputs[args.selectedIndex]];
        //     break;

        default:
            inputs = args.inputs;
            outputs = args.outputs;
            break;
    }

    return { inputs, outputs }
}

function signingInputOutputs(args) {
    let { inputs, outputs } = extractInputOutputs(args);
    let res = {
        sigHash: args.sigHash,
        creationDate: args.creationDate,
        inputs,
        outputs
    };
    let hash = crypto.keccak256Dbl(JSON.stringify(res));      // because of securiy, MUST use double hash
    let signature = crypto.signMsg(hash.substring(0, iConsts.SIGN_MSG_LENGTH), args.prvKey);
    return signature;
}

function verifyInputOutputsSignature(args) {
    // console.log(`verifyInputOutputsSignature args: ${utils.stringify(args)}`);
    let { inputs, outputs } = extractInputOutputs(args);

    let res = {
        sigHash: args.sigHash,
        creationDate: args.creationDate,
        inputs,
        outputs
    };
    let hash = crypto.keccak256Dbl(JSON.stringify(res));      // because of securiy, MUST use double hash

    // let sigObj = c.signMsg(mSHA, pairKey.prvKey, { asBytes: true });
    try {
        let verifyRes = crypto.verifySignature(hash.substring(0, iConsts.SIGN_MSG_LENGTH), args.signature, args.pubKey);
        return verifyRes;
    } catch (e) {
        clog.trx.error(`error in verifySignature: ${utils.stringify(args)}`);
        clog.trx.error(e);
        return false;
    }

}

module.exports.extractInputOutputs = extractInputOutputs;
module.exports.signingInputOutputs = signingInputOutputs;
module.exports.verifyInputOutputsSignature = verifyInputOutputsSignature;