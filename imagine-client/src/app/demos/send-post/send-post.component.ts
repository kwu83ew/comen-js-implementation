import { Component, OnInit, Input } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { CookieService } from 'src/app/services/cookie.service';
import { DemosService } from '../services/demos.service';

@Component({
  selector: 'ima-send-post',
  templateUrl: './send-post.component.html',
  styleUrls: ['./send-post.component.css']
})
export class SendPostComponent implements OnInit {
  @Input('replyArgs') replyArgs;

  public sendPostNotiMsg = '';
  public voteOptions: any[];

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private demosService: DemosService) { }

  ngOnInit() {
    this.setSelectedAgoraInfo();
    this.voteOptions = [];
    this.initVoteOptions();
  }

  public initVoteOptions() {
    let val = 100
    while (val > -101) {
      if (val > 0) {
        this.voteOptions.push([val, `${val}% Agree`]);
      } else if (val == 0) {
        this.voteOptions.push([val, `Abstain`]);
      } else {
        this.voteOptions.push([val, `${val}% Disagree`]);
      }
      val = val - 10;
    }
  }

  public napIName = '';
  public napAgTitle = '';
  public setSelectedAgoraInfo() {
    let urlDtl = this.gUtils.dummyURLParser();
    console.log('urlDtl', urlDtl);
    if ((urlDtl.segmetns.length > 1) && (urlDtl.segmetns[1] == 'demos')) {
      // we are in an Agora room 
      this.napIName = decodeURIComponent(urlDtl.segmetns[0]);
      this.napAgTitle = decodeURIComponent(urlDtl.segmetns[2]);
      this.getAgorasInfo({
        iName: this.napIName,
        AgTitle: this.napAgTitle
      });
    }
  }


  // this.mapTitleToAgHash = {};
  // for (let anAg of this.onchainAgoras) {
  //   this.mapTitleToAgHash[anAg['ag_title']] = anAg;
  // }
  // console.log('this.mapTitleToAgHash++', this.mapTitleToAgHash);


  public napAgUqHash = ''; // agora uniq hash
  public napReplyPoint = 0; // a number between -100 to +100 to present how much you are agree or disagree with what you are replying in response
  public napOpinion = '';
  public sendNewPost(dTarget, costPayMode = 'Normal') {
    this.sendPostNotiMsg = dTarget;
    if ((this.napOpinion == null) || (this.napOpinion == '')) {
      this.sendPostNotiMsg = 'Missed Opinion Description!';
      return;
    }
    console.log('this.replyArgs', this.replyArgs);

    this.demosService.sendNewPost({
      dTarget,
      napCostPayMode: costPayMode,
      napAgUqHash: this.napAgUqHash,
      napReply: this.replyArgs.docHash,
      napReplyPoint: this.napReplyPoint,
      napOpinion: this.napOpinion,
    }).subscribe(res => {
      console.log(res);
      this.sendPostNotiMsg = res.msg;
    });
  }

  public getAgorasInfo(args) {
    this.demosService.retrieveAgorasInfo(args).subscribe(res => {
      console.log(res);
      this.napAgUqHash = res.agoraInfo.ag_unique_hash;
    });
  }

  public rejectReply() {
    this.replyArgs = {docHash:'', citation:''};
  }
}
