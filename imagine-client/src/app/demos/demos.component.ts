import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { CookieService } from '../services/cookie.service';
import { DemosService } from './services/demos.service';

@Component({
  selector: 'im-demos',
  templateUrl: './demos.component.html',
  styleUrls: ['./demos.component.css']
})
export class DemosComponent implements OnInit {

  logoUrl = '../assets/imagine.jpg';

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private demosService: DemosService
  ) { }

  ngOnInit() {
    this.handlePostFormDisplay();

    this.getOnchainAgoras();
  }


  public dspAgoraForm = false;
  public dspNewAgoraForm() {
    this.dspAgoraForm = !this.dspAgoraForm;
  }

  public onchainAgoras: [];
  public getOnchainAgoras() {
    this.demosService.getOnchainAgoras({}).subscribe(res => {
      this.onchainAgoras = res.records;
      this.organaizeIndenting();
    });
  }

  public indentedAgoras = [];
  public organaizeIndenting() {
    // TODO: improve it
    console.log('onchainAgoras', this.onchainAgoras);
    let sub1 = [];
    let anAg: {
      class: string
    };
    let parentIndex: number;
    for (let anAgo of this.onchainAgoras) {
      anAg = anAgo;
      if (anAg['ag_parent'] == '') {
        anAg.class = 'indent0';
        anAg['ddTitle'] = `..${anAg['ag_title']}`;
        this.indentedAgoras.push(anAg);
        sub1.push(anAg['ag_unique_hash']);
      }
    }
   
    // injecting indented layers
    for (let level = 1; level < 10; level++) {
      let subs = [];
      for (let anAgo of this.onchainAgoras) {
        anAg = anAgo;
        if (sub1.includes(anAg['ag_parent'])) {
          parentIndex = this.getParentIndex(anAg['ag_parent']);
          anAg['class'] = `indent${level}`;
          let dots = '..'.repeat(level + 1)
          anAg['ddTitle'] = `${dots}${anAg['ag_title']}`;
          this.indentedAgoras.splice(parentIndex + 1, 0, anAg);
          subs.push(anAg['ag_unique_hash']);
        }
      }
      sub1 = subs;
    }

  }

  public getParentIndex(parentHash) {
    for (let i = 0; i < this.indentedAgoras.length; i++)
      if (this.indentedAgoras[i].ag_unique_hash == parentHash)
        return i;
    return 0;
  }

  public showPostForm = false;
  public handlePostFormDisplay() {
    let urlDtl = this.gUtils.dummyURLParser();
    if ((urlDtl.segmetns.length > 2) && (urlDtl.segmetns[1] == 'demos')) {
      this.showPostForm = true;
    } else {
      this.showPostForm = false;
    }
  }

  public replyArgs = { docHash: '', citation: '' };
  public dspReply(args) {
    this.replyArgs = args;
  }
}
