import { Component, OnInit, Input } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { CookieService } from 'src/app/services/cookie.service';
import { DemosService } from '../services/demos.service';

@Component({
  selector: 'build-agora-form',
  templateUrl: './build-agora-form.component.html',
  styleUrls: ['./build-agora-form.component.css']
})
export class BuildAgoraFormComponent implements OnInit {
  @Input('indentedAgoras') indentedAgoras;

  public newAgNotiMsg;
  public languagesList = [];

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private demosService: DemosService) { }

  ngOnInit() {
    this.getMachineAccountInfo();
    this.reIndentAgoras();
    if (this.gUtils.machineProfileG === null) {
      this.gUtils.loadMachineProfileG({
        callback: this.setProfileCallback.bind(this)
      });
    }
    if (this.languagesList.length == 0)
    this.getLanguagesList();
  }

  public getLanguagesList() {
    this.gUtils.getLanguagesList({});
  }

  public agSelectedLang = 'eng';
  public setProfileCallback() {
    this.agSelectedLang = this.gUtils.machineProfileG.selectedMP.language;
  }

  public selectedINHash = '';
  public setSelectedIName() {
    console.log('setSelectedIName', this.aginHash);
    this.selectedINHash = this.aginHash;
    this.reIndentAgoras();
  }

  public reIndentedAgoras = [];
  public reIndentAgoras() {
    this.reIndentedAgoras = [{
      ag_unique_hash: '',
      ddTitle: 'Root'
    }];
    console.log('this.selectedINHash', this.selectedINHash);
    console.log('this.indentedAgoras', this.indentedAgoras);

    for (let anAg of this.indentedAgoras) {
      if (anAg.ag_in_hash == this.selectedINHash)
        this.reIndentedAgoras.push(anAg)
    }

    console.log('this.reIndentedAgoras, this.reIndentedAgoras', this.reIndentedAgoras);

  }


  public machineAccountInfo;
  public agCreatorAccount;
  public getMachineAccountInfo() {
    this.gUtils.getMachineAccountInfo({}).subscribe(res => {
      // console.log('getMachineAccountInfo', res);
      this.machineAccountInfo = res;
      if (this.agCreatorAccount == undefined)
        this.agCreatorAccount = this.machineAccountInfo.backerAddress;
      console.log('machineINames ', this.machineAccountInfo.machineINames);
      this.setAgorasLinkDropDown();
    });
  }

  public agorasLinkDropDown = [];
  public setAgorasLinkDropDown() {
    this.agorasLinkDropDown = [];
    this.agorasLinkDropDown.push({
      url: `/imagine/demos/ - New Agora's name - `,
      aginHash: this.gUtils.IMAGINE_HASH,
    });
    for (let anIName of this.machineAccountInfo.machineINames) {
      this.agorasLinkDropDown.push({
        url: `${anIName.iName}/demos/ - New Agora's name - `,
        aginHash: anIName.inHash
      });
    }
    console.log('v', this.agorasLinkDropDown);
  }

  public aginHash = '';
  public agParent = '';
  public agTitle = '';
  public agDescription = '';
  public agTags = '';
  public buildANewAgora(dTarget) {
    this.newAgNotiMsg = dTarget;

    if ((this.agTitle == null) || (this.agTitle == '')) {
      this.newAgNotiMsg = 'Missed title!';
      return;
    }
    if ((this.agCreatorAccount == null) || (this.agCreatorAccount == '')) {
      this.newAgNotiMsg = 'Missed Creator Aaddress!';
      return;
    }
    if ((this.agDescription == null) || (this.agDescription == '')) {
      this.newAgNotiMsg = 'Missed Description!';
      return;
    }

    this.demosService.buildANewAgora({
      dTarget,
      agSelectedLang: this.agSelectedLang,
      aginHash: this.aginHash,
      agCreatorAccount: this.agCreatorAccount,
      agParent: this.agParent,
      agTitle: this.agTitle,
      agDescription: this.agDescription,
      agTags: this.agTags
    }).subscribe(res => {
      console.log(res);
      this.newAgNotiMsg = res.msg;
    });

  }



}
