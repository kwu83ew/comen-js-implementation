import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildAgoraFormComponent } from './build-agora-form.component';

describe('BuildAgoraFormComponent', () => {
  let component: BuildAgoraFormComponent;
  let fixture: ComponentFixture<BuildAgoraFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildAgoraFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildAgoraFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
