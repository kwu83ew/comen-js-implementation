import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { DemosService } from '../services/demos.service';

@Component({
  selector: 'im-agora',
  templateUrl: './agora.component.html',
  styleUrls: ['./agora.component.css']
})
export class AgoraComponent implements OnInit {
  @Output() dspReplyParent = new EventEmitter<any>();

  public showWelcome = true;
  constructor(
    public gUtils: GUtils,
    private demosService: DemosService
  ) { }

  ngOnInit() {
    this.handleWelcomeMsg();
  }

  public handleWelcomeMsg() {
    let urlDtl = this.gUtils.dummyURLParser();
    if ((urlDtl.segmetns.length > 2) && (urlDtl.segmetns[1] == 'demos')) {
      this.showWelcome = false;
      this.getAgorasInfo(urlDtl);
    } else {
      this.showWelcome = true;
    }
  }

  public dspReply(args) {
    this.dspReplyParent.next(args);
  }

  public agoraInfo;
  public posts = [];
  public getAgorasInfo(urlDtl) {
    if ((urlDtl.segmetns.length > 2) && (urlDtl.segmetns[1] == 'demos')) {
      this.demosService.retrieveAgorasInfo({
        iName: urlDtl.segmetns[0],
        AgTitle: urlDtl.segmetns[2],
        getPosts: true
      }).subscribe(res => {
        console.log('=======', res);
        this.agoraInfo = res.agoraInfo;
        this.posts = res.posts;

        // replace post author's address by iName(if has)
        for (let aPost of this.posts) {
          aPost.shortBech = this.gUtils.shortBech8(aPost.ap_creator);
        }
      });
    }
  }

  public convertCPostToPLRBundle(docHash) {
    this.demosService.convertCPostToPLRBundle({ docHash }).subscribe(res => {
      console.log('=======', res);
      if (res.err == false)
        this.gUtils.redirectToPage('imagine/contracts');
    });
  }

  public reActAttr(act, params) {
    console.log('act', act);
    console.log('params', params);
    this[act](params);
  }
  public alert(msg) {
    alert(msg);
  }
}
