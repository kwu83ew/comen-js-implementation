import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class DemosService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public buildANewAgora(args) {
    return this.http.put(this.url + '/api/demos/buildANewAgora', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public getOnchainAgoras(args) {
    return this.http.put(this.url + '/api/demos/getOnchainAgoras', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public retrieveAgorasInfo(args) {
    return this.http.put(this.url + '/api/demos/retrieveAgorasInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public convertCPostToPLRBundle(args) {
    return this.http.put(this.url + '/api/demos/convertCPostToPLRBundle', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public sendNewPost(args) {
    return this.http.put(this.url + '/api/demos/sendNewPost', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


}
