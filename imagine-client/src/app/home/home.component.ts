import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'img-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  logoUrl = '../assets/imagine.jpg';
  constructor() { }

  ngOnInit() {
  }

}
