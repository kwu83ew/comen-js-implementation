import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InameComponent } from 'src/app/iname/iname.component';
import { AnInameComponent } from 'src/app/iname/an-iname/an-iname.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: 'iname', component: InameComponent},
      {
          path: 'iname/:uid',
          // canActivate: [GameDetailGuard],
          component: AnInameComponent
      },
  ]),
  ]
})
export class InameModule { }
