import { NgModule } from '@angular/core';
import {
  MatCheckboxModule,
  MatDialogModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatToolbarModule,
  MatSelectModule, MatTabsModule, MatDividerModule, MatList, MatFormFieldModule
} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  exports: [
    MatCheckboxModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatDialogModule,
    MatDividerModule,
    MatGridListModule,
    MatRadioModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatChipsModule
  ]
})
export class MatComponentsModule { }
