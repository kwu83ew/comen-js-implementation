import { Component } from '@angular/core';
import { CookieService } from "./services/cookie.service";
import { MatTabChangeEvent } from "@angular/material";
import { GUtils } from './services/utils';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'imagine';
  logoUrl = '../assets/imglogo.png';
  selectedTabIndex = '0';
  selectedTabName = 'Home';
  imgClientPort = 'cls_4200';
  appCloneId: number;

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
  ) {
    if (this.cookie.getCookie('selectedTabName') == null || this.cookie.getCookie('selectedTabName') === '') {
      this.cookie.setCookie('selectedTabIndex', '0', 99);
      this.cookie.setCookie('selectedTabName', 'Home', 99);
    }
    this.selectedTabIndex = this.cookie.getCookie('selectedTabIndex');
    this.selectedTabName = this.cookie.getCookie('selectedTabName');
    this.imgClientPort = 'cls_' + window.location.port;

    let urlDtl = this.gUtils.dummyURLParser();
    let seg0s = ['about', 'home', 'wallet', 'demos',
      'wiki', 'messenger', 'contributes', 'settings',
      'misc', 'contracts', 'plugins',
      'iname', 'watcher', 'monitor'];
    console.log('app-urlDtl', urlDtl);
    if (seg0s.includes(urlDtl.segmetns[0].toLowerCase())) {
      this.gUtils.setSelectedTab(urlDtl.segmetns[0]);
      this.selectedTabIndex = this.gUtils.WI_TABS[urlDtl.segmetns[0].toLowerCase()];
    }
    if ((urlDtl.segmetns.length > 1) && seg0s.includes(urlDtl.segmetns[1].toLowerCase())
      && (urlDtl.segmetns[0] == 'imagine')) {
      this.gUtils.setSelectedTab(urlDtl.segmetns[1]);
      this.selectedTabIndex = this.gUtils.WI_TABS[urlDtl.segmetns[1].toLowerCase()];
    }
  }

  ngOnInit() {
    this.appCloneId = this.gUtils.getAppCloneId();
  }

  onTabClick(e: MatTabChangeEvent) {
    this.selectedTabIndex = e.index.toString();
    this.selectedTabName = e.tab.textLabel.toLowerCase();
    this.cookie.setCookie('selectedTabIndex', this.selectedTabIndex, 99);
    this.cookie.setCookie('selectedTabName', this.selectedTabName, 99);
    history.replaceState('', this.selectedTabName, `/${this.selectedTabName.toLowerCase()}`);
  }


}
