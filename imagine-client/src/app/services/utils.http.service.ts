import { Injectable } from '@angular/core';
import { HttpService } from "./http.service";
import { catchError, map } from "rxjs/operators";
import { throwError } from "rxjs";
import { AppError } from "../errors/app-error";

@Injectable({
  providedIn: 'root'
})
export class UtilsHTTPService {
  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public getClientVersion(args) {
    return this.http.put(this.url + '/api/utils/getClientVersion', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getMachineSignals(args) {
    return this.http.put(this.url + '/api/utils/getMachineSignals', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getNetworkSignals(args) {
    return this.http.put(this.url + '/api/utils/getNetworkSignals', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public calcLoanRepayments(args) {
    return this.http.put(this.url + '/api/utils/calcLoanRepayments', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public flushBufferedDocs(args) {
    return this.http.put(this.url + '/api/utils/flushBufferedDocs', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public voteReleaseReseve(args) {
    return this.http.put(this.url + '/api/misc/voteReleaseReseve', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadRecordedINames(args) {
    return this.http.put(this.url + '/api/utils/loadRecordedINames', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public refreshPollingStatus(args) {
    return this.http.put(this.url + '/api/utils/refreshPollingStatus', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getDNASharesInfo(args) {
    return this.http.put(this.url + '/api/utils/getDNASharesInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public predictFutureCoinIssuance(args) {
    return this.http.put(this.url + '/api/utils/predictFutureCoinIssuance', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getRegisteredINames(args) {
    return this.http.put(this.url + '/api/utils/getRegisteredINames', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


  public getReservesDetails(args) {
    return this.http.put(this.url + '/api/utils/getReservesDetails', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadPollingProfiles(args) {
    return this.http.put(this.url + '/api/utils/loadPollingProfiles', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getMachineAccountInfo(args) {
    return this.http.put(this.url + '/api/utils/getMachineAccountInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public predictReleaseableMicroPAIsPerOneCycle(args) {
    return this.http.put(this.url + '/api/utils/predictReleaseableMicroPAIsPerOneCycle', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadMachineProfileG(args) {
    return this.http.put(this.url + '/api/settings/loadMachineProfileG', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getLanguagesList(args) {
    return this.http.put(this.url + '/api/settings/getLanguagesList', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getMyOnchainFiles(args) {
    return this.http.put(this.url + '/api/utils/getMyOnchainFiles', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }




  public onUploadH(selectedFile) {
    const uploadData = new FormData();
    uploadData.append('theFile', selectedFile, selectedFile.name);
    return this.http.post(
      this.url + '/api/utils/uploadFile',
      uploadData
    ).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  public recordFile(args) {
    return this.http.put(this.url + '/api/utils/recordFile', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public fetchInboxManually(args) {
    return this.http.put(this.url + '/api/utils/fetchInboxManually', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public readingHarddiskManually(args) {
    return this.http.put(this.url + '/api/utils/readingHarddiskManually', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public manInvokeLeavesFromNeighbors(args) {
    return this.http.put(this.url + '/api/utils/manInvokeLeavesFromNeighbors', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public dropBroadcastLogs(args) {
    return this.http.put(this.url + '/api/utils/dropBroadcastLogs', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public pullFromQeueManually(args) {
    return this.http.put(this.url + '/api/utils/pullFromQeueManually', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public sendOutThePacketManually(args) {
    return this.http.put(this.url + '/api/utils/sendOutThePacketManually', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public dlDAGBundle(args) {
    return this.http.put(this.url + '/api/utils/dlDAGBundle', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public dlNodeScreenShot(args) {
    return this.http.put(this.url + '/api/utils/dlNodeScreenShot', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadScreenshotsList(args) {
    return this.http.put(this.url + '/api/utils/loadScreenshotsList', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public compareScreenShot(args) {
    return this.http.put(this.url + '/api/utils/compareScreenShot', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public deleteSS(args) {
    return this.http.put(this.url + '/api/utils/deleteSS', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public generateTestData(args) {
    return this.http.put(this.url + '/api/utils/generateTestData', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


}
