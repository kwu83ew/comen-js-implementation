import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { UtilsHTTPService } from './utils.http.service';
import { CookieService } from './cookie.service';

@Injectable({
  providedIn: 'root'
})
export class GUtils {

  LAUNCH_YEAR = 2020;
  HALVING_PERIOD = 20;
  CONTRIBUTE_HOURS = 100000;
  TIME_GAIN = 1;
  DEFAULT_VOTE_LONGIVITY = 24; // 2 cycle  = 24 hours
  IMAGINE_HASH = '58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c';
  HU_DNA_SHARE_ADDRESS = 'im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl';
  HU_INAME_OWNER_ADDRESS = 'im1xqmryc3cvscnxdrpxyex2vrxvc6xxwf4vcenvvnp8pjrvdpe8ycrvvzlqa6';

  CONTRIBUTION_APPRECIATING_PERIOD = 7;
  GLOBAL_SALARY_PER_HOUR = 8.95; // = 8.95 $
  DEFAULT_MAX_REPAYMENTS = 2 * 365; // 2 cycle per day * 365 days
  PROPOSAL_APPLY_COST_SCALE = 3;
  WI_TABS = {
    'about': '0',
    'home': '1',
    'wallet': '2',
    'demos': '3',
    'wiki': '4',
    'messenger': '5',
    'contributes': '6',
    'settings': '7',
    'misc': '8',
    'contracts': '9',
    'plugins': '10',
    'iname': '11',
    'watcher': '12',
    'monitor': '12'
  };

  public fileImage = 'file.png';
  public imageExtensions = ['jpg', 'jpeg', 'png', 'webp', 'gif', 'svg', 'tiff'];
  public calcLoanRepaymentsRes;
  public loadRecordedINamesRes;
  public dummyCallbackRes = {};
  public getMachineSignalsRes;
  public getNetworkSignalsRes;

  constructor(public utilsHTTPService: UtilsHTTPService, public cookie: CookieService) { }

  public isSelectedTab(tabName) {
    return (this.cookie.getCookie('selectedTabName') == tabName.toLowerCase())
    // return (this.cookie.getCookie('selectedTab') == this.WI_TABS[tabName])
  }

  public setSelectedTab(tabName) {
    this.cookie.setCookie('selectedTabName', tabName.toLowerCase(), 99);
    // this.cookie.setCookie('selectedTab', this.WI_TABS[tabName].toString(), 99);
  }

  public setCookie(key, value) {
    this.cookie.setCookie(key, value, 99, '');
  }

  public getCookie(key) {
    return this.cookie.getCookie(key);
  }

  public encode_utf8(s) {
    return unescape(encodeURIComponent(s));
  }

  public decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  public getCycleByMinutes() {
    let cycleByMinutes = (this.TIME_GAIN == 1) ? 720 : this.TIME_GAIN
    return cycleByMinutes;
  }

  public dummyURLParser() {
    /**
     * TODO: fix it in oorder to use Angular standard Routing
     * sorry for this dirty implementation
     */
    let delimiter = '';
    let url = window.location.href;
    if (url.includes('localhost:')) {
      delimiter = 'localhost:';
    } else if (url.includes('127.0.0.1:')) {
      delimiter = '127.0.0.1:';
    }
    let segmetns = window.location.href.split(delimiter)[1].split('/');
    let urlDtl = {
      port: segmetns[0],
      segmetns: []
    }
    segmetns.shift();
    let queryParams = {};
    let newSegments = [];
    for (let aSeg of segmetns) {
      if (aSeg.includes('?')) {
        let pDtlQuestionMark = aSeg.split('?');
        newSegments.push(pDtlQuestionMark[0]);
        newSegments.push(pDtlQuestionMark[1]);
        let pDtl = pDtlQuestionMark[1].split('&');
        for (let aParam of pDtl) {
          let aParamDtl = aParam.split('=');
          queryParams[aParamDtl[0]] = aParamDtl[1]
        }
      } else {
        newSegments.push(aSeg);
      }
    }
    urlDtl['segmetns'] = newSegments;
    urlDtl['queryParams'] = queryParams;

    /**
     * handling segments with parameters
     * http://localhost:4200/demos/developers/
     * http://localhost:4200/demos/developers/home => http://localhost:4200/demos/developers/
     * http://localhost:4200/demos/developers/?page=77
     * http://localhost:4200/demos/developers/?post=323112
     * http://localhost:4200/demos/developers/?uid=hu&pid=post12
     * 
     */
    // console.log('url urlDtl', urlDtl);

    // if (urlDtl.segmetns.length > 1) {
    //   // maybe cookie settings
    //   if (urlDtl.segmetns[0] == 'imagine') {
    //     if (['demos', 'wiki', 'messenger'].includes(urlDtl.segmetns[1])) {
    //       this.setSelectedTab(urlDtl.segmetns[1]);
    //     }
    //   }
    // }

    return urlDtl;
  }

  public redirectToPage(url) {
    window.location.replace(`${url}`);
  }

  INITIAL_WORKS = 150000.0
  myWorkHours = 1
  myWorkLevel = 1
  myWork = this.myWorkHours * this.myWorkLevel;
  // console.log(changeRate);



  public refreshPollingStatus(args) {
    return this.utilsHTTPService.refreshPollingStatus(args);
  }

  public getDNASharesInfo(args) {
    return this.utilsHTTPService.getDNASharesInfo(args);
  }

  public predictFutureCoinIssuance(args) {
    return this.utilsHTTPService.predictFutureCoinIssuance(args);
  }

  public getRegisteredINames(args) {
    return this.utilsHTTPService.getRegisteredINames(args);
  }

  public getReservesDetails(args) {
    return this.utilsHTTPService.getReservesDetails(args);
  }

  public loadPollingProfiles(args) {
    return this.utilsHTTPService.loadPollingProfiles(args);
  }

  public getMachineAccountInfo(args) {
    return this.utilsHTTPService.getMachineAccountInfo(args);
  }

  public fetchInboxManually(args) {
    return this.utilsHTTPService.fetchInboxManually(args);
  }

  public manInvokeLeavesFromNeighbors(args) {
    return this.utilsHTTPService.manInvokeLeavesFromNeighbors(args);
  }

  public dropBroadcastLogs(args) {
    return this.utilsHTTPService.dropBroadcastLogs(args);
  }

  public readingHarddiskManually(args) {
    return this.utilsHTTPService.readingHarddiskManually(args);
  }

  public pullFromQeueManually(args) {
    return this.utilsHTTPService.pullFromQeueManually(args);
  }

  public sendOutThePacketManually(args) {
    return this.utilsHTTPService.sendOutThePacketManually(args);
  }


  public getNow() {
    return moment().format('YYYY-MM-DD HH:mm:ss');   // all dates in imaginge must following ISO 8601
  }

  public minutesAfter(forwardInTimesByMinutes, t = null) {
    if (t == null) {
      t = moment().format('YYYY-MM-DD HH:mm:ss');
    }

    return moment(t, "YYYY-MM-DD HH:mm:ss").add({ 'minutes': forwardInTimesByMinutes }).format("YYYY-MM-DD HH:mm:ss");
  }




  public getAllMethodNames(obj) {
    return Object.getOwnPropertyNames(obj).concat(Object.getOwnPropertyNames(obj.__proto__))
  }

  public shortCoinRef(refLoc) {
    return [
      this.hash6c(refLoc.split(':')[0]),
      refLoc.split(':')[1]
    ].join(':')
  }

  public customFloorFloat(number, percision = 11) {
    const per = Math.pow(10, percision);
    return Math.trunc(number * per) / per;
  }

  public iFloorFloat(number) {
    return this.customFloorFloat(number, 11);
  }


  public calcLog(x, range, exp = 17) {
    let hundredPercent = Math.log(Math.pow(range, exp));
    let y = (x >= range) ? 0 : Math.log(Math.pow((range - x), exp));
    let gain = this.iFloorFloat((y * 100) / hundredPercent);
    return {
      x,
      y,
      gain,
      revGain: (1 / gain)
    };
  }

  public isNanEmptyNull(inp) {
    if ((inp == NaN) || (inp === NaN) || (inp == 'NaN'))
      return true;
    if ((inp == null) || (inp === null))
      return true;
    if ((inp == '') || (inp === ''))
      return true;

    return false;
  }

  /**
   * format numbers
   * @param s
   */
  public sepNum(s) {
    return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  /**
   * 
   * @param {*} microPAI 
   * PAIs are always Integer, but their value is mili.
   * it means every presentation of PAIs must be in floatingpoint with 3 digit at the right side of .
   */
  public microPAIToPAI(microPAI) {
    microPAI = Math.floor(microPAI);
    let PAI = this.customFloorFloat(microPAI / 1000000, 0);
    return this.sepNum(PAI);
  }

  public microPAIToPAI3(microPAI) {
    microPAI = Math.floor(microPAI).toString();
    let negativeSign = '';
    if (microPAI.substring(0, 1) == '-') {
      if (microPAI.toString().length > 4)
        negativeSign = '-';
      microPAI = microPAI.substring(1);
    }
    microPAI = microPAI.toString().padStart(7, '0');
    return negativeSign + [this.sepNum(microPAI.substr(0, microPAI.length - 6)), '.', microPAI.substr(-6, 3)].join('');
  }


  public microPAIToPAI6(microPAI) {
    microPAI = Math.floor(microPAI).toString();
    let negativeSign = '';
    if (microPAI.substring(0, 1) == '-') {
      negativeSign = '-';
      microPAI = microPAI.substring(1);
    }
    if (microPAI.length < 8) {
      microPAI = microPAI.toString().padStart(7, '0');
    }
    return negativeSign + [this.sepNum(microPAI.substr(0, microPAI.length - 6)), '.', microPAI.substr(-6, 6)].join('');
  }

  public microPAIToPAI6Seg(microPAI) {
    microPAI = Math.floor(microPAI).toString();
    let negativeSign = '';
    if (microPAI.substring(0, 1) == '-') {
      negativeSign = '-';
      microPAI = microPAI.substring(1);
    }
    if (microPAI.length < 8) {
      microPAI = microPAI.toString().padStart(7, '0');
    }
    let num = negativeSign + [this.sepNum(microPAI.substr(0, microPAI.length - 6)), '.', microPAI.substr(-6, 6)].join('');
    return num.split('.');
  }

  public shortBech(bech32) {
    return [bech32.substr(0, 3), bech32.substr(-4)].join('.')
  }
  public shortBech6(bech32) {
    return [bech32.substr(0, 3), bech32.substr(-6)].join('.')
  }
  public shortBech8(bech32) {
    return [bech32.substr(0, 3), bech32.substr(-8)].join('.')
  }
  public shortBech16(bech32) {
    return [bech32.substr(0, 3), bech32.substr(-16)].join('.')
  }
  public getAppCloneId() {
    const imgClientPort = parseInt(window.location.port);
    if (imgClientPort > 0) {
      return imgClientPort - 4200;
    }
    return 0;
  }

  public stripNonNumerics(s) {
    return s.toString().replace(/\D/g, '');
  }

  /**
   * format numbers
   * @param s
   */
  public castToInt(s) {
    return parseInt(this.stripNonNumerics(s));
  }

  public getNow1(mili = false) {
    const d = new Date();
    let dateStr = d.getUTCFullYear() + '-' +
      ('00' + (d.getUTCMonth() + 1)).slice(-2) + '-' +
      ('00' + d.getUTCDate()).slice(-2) + ' ' +
      ('00' + d.getUTCHours()).slice(-2) + ':' +
      ('00' + d.getUTCMinutes()).slice(-2) + ':' +
      ('00' + d.getUTCSeconds()).slice(-2);
    if (mili === true) {
      dateStr += '.' + ('000' + d.getUTCMilliseconds()).slice(-3);
    }
    return dateStr;
  }

  public stringToDate(s) {
    // i am not sure it work!
    // s = s.split(/[-: ]/);
    // return new Date(s[0], s[1] - 1, s[2], s[3], s[4], s[5]);

    let match = s.match(/^(\d+)-(\d+)-(\d+) (\d+)\:(\d+)\:(\d+)$/);
    let date = new Date(match[1], match[2] - 1, match[3], match[4], match[5], match[6]);
    return date;


  }

  public hasProp(obj, prop) {
    return obj.hasOwnProperty(prop);
    // return Object.prototype.hasOwnProperty.call(obj, prop);
  }

  public hash6c(hash) {
    return hash.substr(0, 6);
  }

  public hash16c(hash) {
    return hash.substr(0, 16);
  }

  public objKeys(o) {
    return Object.keys(o);
  }

  public getBGColorByType(blockType) {
    let color = 'rgba(97,195,238,0.5)';
    switch (blockType) {
      case 'Genesis':
        color = 'lime';
        break;

      case 'Coinbase':
        color = 'rgba(247, 74, 44, 0.91)';
        break;

      case 'RpBlock':
        color = 'rgba(223, 131, 118, 0.89)';
        break;

      case 'FSign':
        color = 'rgb(94, 94, 255)';
        break;

      case 'Normal':
        color = 'rgba(97,195,238,0.5)';
        break;

      case 'POW':
        color = 'rgba(205, 204, 93, 0.99)';
        break;

      case 'SusBlock':
        color = 'rgba(197,95,238,0.5)';
        break;

      case 'FVote':
        color = '#9E6Ecc';
        break;

      default:
        color = 'rgba(17,225,238,0.5)';
    }
    return color;
  }


  // TODO: refactore it & implement some nice shared http service & callback
  public getClientVersion(args) {
    this.utilsHTTPService.getClientVersion(args).subscribe(res => {
      this.dummyCallbackRes['getClientVersionRes'] = res;
      args.callback();
    });
  }

  public getMachineSignals(args) {
    this.utilsHTTPService.getMachineSignals(args).subscribe(res => {
      this.dummyCallbackRes['getMachineSignalsRes'] = res;
      args.callback();
    });
  }

  public getNetworkSignals(args) {
    this.utilsHTTPService.getNetworkSignals(args).subscribe(res => {
      this.dummyCallbackRes['getNetworkSignalsRes'] = res;
      args.callback();
    });
  }

  public calcLoanRepayments(args) {
    this.utilsHTTPService.calcLoanRepayments(args).subscribe(res => {
      this.calcLoanRepaymentsRes = res;
      args.callback();
    });
  }

  public flushBufferedDocs(args) {
    this.utilsHTTPService.flushBufferedDocs(args).subscribe(res => {
      this.dummyCallbackRes['flushBufferedDocs'] = res;
      if (args && args.callback)
        args.callback();
    });
  }

  public voteReleaseReseve(args) {
    this.utilsHTTPService.voteReleaseReseve(args).subscribe(res => {
      console.log(res);
      this.dummyCallbackRes['voteReleaseReseve'] = res;
      if (args)
        args.callback();
    });
  }

  public loadRecordedINames(args) {
    this.utilsHTTPService.loadRecordedINames(args).subscribe(res => {
      this.loadRecordedINamesRes = res;
      // console.log(res);
      if (args)
        args.callback();
    });
  }

  public machineProfileG = null;
  public loadMachineProfileG(args) {
    this.utilsHTTPService.loadMachineProfileG({}).subscribe(res => {
      console.log('loadMachineProfileG', res);
      this.machineProfileG = res;
      if (args && args.callback)
        args.callback();
    });
  }

  public languagesList = [];
  public getLanguagesList(args) {
    this.utilsHTTPService.getLanguagesList({}).subscribe(res => {
      console.log('getLanguagesList', res);
      this.languagesList = res;
      if (args && args.callback)
        args.callback();
    });
  }

  public myOnchainFiles = { records: [], iCache: '' };
  public getMyOnchainFiles(args) {
    this.utilsHTTPService.getMyOnchainFiles({}).subscribe(res => {
      // console.log('get MyOnchainFiles', res);
      this.myOnchainFiles = res;
      if (args && args.callback)
        args.callback();
    });
  }

  public cPFileLabel: string;
  public selectedFile: File;
  public uploadRes = { msg: '' };
  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log('file', this.selectedFile);
  }
  public onUploadG(args) {
    this.utilsHTTPService.onUploadH(this.selectedFile).subscribe(res => {
      args['uploadRes'] = res;
      this.recordFile(args);
    });
  }
  public recordFile(args) {
    args.cPFileLabel = this.cPFileLabel;
    console.log('recordFile args', args);
    this.utilsHTTPService.recordFile(args).subscribe(res => {
      this.uploadRes = res;
      console.log('res', res);
      args.callback();
    });
  }

  public dlDAGBundle(args) {
    this.utilsHTTPService.dlDAGBundle(args).subscribe(res => {
      this.dummyCallbackRes['dlDAGBundleRes'] = res;
      args.callback();
    });
  }

  public dlNodeScreenShot(args) {
    this.utilsHTTPService.dlNodeScreenShot(args).subscribe(res => {
      this.dummyCallbackRes['dlNodeScreenShotRes'] = res;
      args.callback();
    });
  }

  public loadScreenshotsList(args) {
    this.utilsHTTPService.loadScreenshotsList(args).subscribe(res => {
      this.dummyCallbackRes['loadScreenshotsListRes'] = res;
      args.callback();
    });
  }

  public compareScreenShot(args) {
    this.utilsHTTPService.compareScreenShot(args).subscribe(res => {
      this.dummyCallbackRes['compareScreenShotRes'] = res;
      args.callback();
    });
  }

  public deleteSS(args) {
    this.utilsHTTPService.deleteSS(args).subscribe(res => {
      this.dummyCallbackRes['deleteSSRes'] = res;
      args.callback();
    });
  }

  public generateTestData(args) {
    this.utilsHTTPService.generateTestData(args).subscribe(res => {
      this.dummyCallbackRes['generateTestDataRes'] = res;
      args.callback();
    });
  }

}
