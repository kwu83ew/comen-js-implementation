import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  imgServerPort = 1717;
  domain = 'http://localhost:';
  url = '';

  constructor(public http: HttpClient) {
    this.http = http;
    // dummy solution to cloning app for loacl-tests
    const imgClientPort = parseInt(window.location.port);
    if (imgClientPort > 4200) {
      this.imgServerPort = 1717 + imgClientPort - 4200;
    }
    this.url = this.domain + this.imgServerPort;
  }
}
