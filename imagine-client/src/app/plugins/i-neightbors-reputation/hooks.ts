import { Injectable } from '@angular/core';
import { GUtils } from 'src/app/services/utils';


@Injectable({
    providedIn: 'root'
})

export class Hooks {

    constructor(public gUtils: GUtils) { }



    public async ch_before_plugin_content(args) {
        // console.log('ch_before_plugin_content args', args);
        return new Promise((resolve, reject) => {
            args.imApp.pluginService.getKValue('Plugin_ReputationManager').subscribe(res => {
                // console.log('we are in ch_before_plugin_content args', args);
                // console.log(res);
                let outHtml = `
                <table class="NeighReport"><tr>
                <td>Neighbor</td>   
                <td>success Packets</td>
                <td>&nbsp;</td>
                <td>corrupted Packets</td>
                </tr>`;
                for (let aNeighbor of args.imApp.gUtils.objKeys(res)){
                    outHtml += `<tr>
                    <td>${aNeighbor}</td>   
                    <td>${res[aNeighbor].successPackets}</td>
                    <td>&nbsp;</td>
                    <td>${res[aNeighbor].corruptedPackets}</td>
                    </tr>`;
                    
                }
                outHtml += '</table>';
                return resolve(outHtml);
              });

        });
        
    }

    public async  ch_after_plugin_content(args) {
        // console.log('we are in ch_after args', args);
        // return '';
    }


}
