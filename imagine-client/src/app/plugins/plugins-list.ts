import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class IPluginListHandler {
    pluginsList;

    ngOnInit() {
        
    }

    constructor(){

    }

    public getPluginsList() {
        this.pluginsList = {
            'iNReputation': {
                name: 'Imagine Neighbors Reputation System',
                version: '0.0.0',
                path: 'i-neightbors-reputation',
                className: 'ReputationManager',
                status: 'enabled',
                description: 'a system to filing neighbors(and probably adversaries)'
            }
        }
        return this.pluginsList;
    }
}
