import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { WatcherService } from './services/watcher.service';
import { Network, DataSet, Node, Edge, IdType, Timeline } from 'vis';
import { Md5 } from 'ts-md5/dist/md5';
import { Block } from '../models/watcher/Block';
import { utils } from 'protractor';
import { CookieService } from '../services/cookie.service';

@Component({
  selector: 'im-watcher',
  templateUrl: './watcher.component.html',
  styleUrls: ['./watcher.component.css']
})
export class WatcherComponent implements OnInit {

  blocks; //: Block[]
  public blocksDict: {};
  public machinesInfoObj: {};
  public machineByBacker: {};
  public machineByCBKey: {};
  public machinesInfoArr: any[];
  public machinesOrderedByCBPriority: string;
  public equalIn5DagsInx: number;
  public equalIn5DagsDate: string;
  public equalIn5DagsHash: string;
  public fullSort;

  public selectedMachine;
  public imgInfo;
  public wInboxes: any[];
  // public parsingQ: any[];
  public sendingQ: any[];
  public wOutboxes: any[];

  public recursiveRefreshOn = false;
  public recursiveRefreshIcon = ' > Play ';

  public nodes: Node;
  public edges: Edge;
  public network: Network;

  public MIN_MONEY_RECEIVER = 2;
  public MAX_MONEY_RECEIVER = 6;

  // sending cloned trax
  public candidTransactions = [];
  public candidBackers = {};
  public cloneBroadcastDelay = 10000;



  constructor(
    public gUtils: GUtils,
    private watcherService: WatcherService,
    public cookie: CookieService
  ) { }//, private md5: Md5

  ngOnInit() {
    this.equalIn5DagsInx = 0;
    this.equalIn5DagsDate = '';
    this.machinesInfoArr = [];

    this.wInboxes = [];
    // this.parsingQ = [];
    this.sendingQ = [];
    this.wOutboxes = [];

    this.machineByBacker = {}
    this.machineByCBKey = {}

    this.blocksDict = {}

    let DAGBalancesTmp = {
      sumCoinbase: 0, spendables: 0,
      waitedCoinbases: { sum: 0, dblSpends: [] },
      waitedNormals: { sum: 0, dblSpends: [] },
      waitedIncomes: 0,
      floorishMicroPAIs: 0,
      sumCoinbaseColor: '#f9f9f9',
      spendablesColor: '#f9f9f9',
      waitedCoinbasesColor: '#f9f9f9',
      waitedNormalsColor: '#f9f9f9',
      waitedIncomesColor: '#f9f9f9',
      floorishMicroPAIsColor: '#f9f9f9',
      finalBalanceColor: '#f9f9f9',
      nodeWealthColor: '#f9f9f9'
    };
    this.machinesInfoObj = {
      0: {
        machineEmail: 'neighbor@imagine.com', machineName: 'neighbor',
        askForLeavesInfo: null, lastBLockInfo: {}, utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
        parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
        DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
        isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
        blockGenReport: {}
      },

      1: {
        machineEmail: 'hu@imagine.com', machineName: 'hu',
        askForLeavesInfo: null, lastBLockInfo: {}, utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
        parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
        DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
        isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
        blockGenReport: {}
      },

      2: {
        machineEmail: 'alice@imagine.com', machineName: 'alice',
        askForLeavesInfo: null, lastBLockInfo: {}, utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
        parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
        DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
        isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
        blockGenReport: {}
      },

      3: {
        machineEmail: 'bob@imagine.com', machineName: 'bob',
        askForLeavesInfo: null, lastBLockInfo: {}, utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
        parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
        DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
        isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
        blockGenReport: {}
      },

      4: {
        machineEmail: 'eve@imagine.com', machineName: 'eve',
        askForLeavesInfo: null, lastBLockInfo: {}, utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
        parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
        DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
        isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
        blockGenReport: {}
      }
    };

    this.makeBackersList();
    this.refreshDAG();
    this.getCheatingInfos();
  }

  recursiveDAGRefresher() {
    this.refreshDAG();
    if (this.recursiveRefreshOn) {
      setTimeout(() => {
        this.recursiveDAGRefresher();
      }, 30000);
    }
  }

  prepareInboxList(rawList) {
    const md5 = new Md5();
    let outbox = [];
    rawList.forEach(elm => {
      const hint = elm.fileName.replace(',', '\n');
      const hash = this.gUtils.hash6c(md5.appendStr(elm.fileName).end().toString());
      outbox.push({
        hint,
        hash
      });
    });
    return outbox;
  }

  prepareOutboxList(rawList) {
    const md5 = new Md5();
    let outbox = [];
    rawList.forEach(elm => {
      let hint = elm;
      if ((hint != undefined) && hint.includes(','))
        hint = hint.replace(',', '\n');
      const hash = this.gUtils.hash6c(md5.appendStr(elm).end().toString());
      outbox.push({
        hint,
        hash
      });
    });
    let start = outbox.length - 3;
    if (start < 0) {
      start = 0;
    }
    outbox = outbox.slice(start);
    return outbox;
  }

  // refreshAndAnalyze() {
  //   this.refreshDAG();
  //   // for (let i = 0; i < 5; i++) {
  //   //   this.analyzeDag(null, i);
  //   // }
  // }

  prepareUTXOStat(utxos) {
    let sum = 0;
    let hint = '';
    utxos.forEach(elm => {
      sum += elm.o_value;
      hint += '\n' + elm.hash + '\t ' + this.gUtils.microPAIToPAI(elm.value);
    });
    return { sum, hint, count: utxos.length };
  }

  makeBackersList() {
    for (let i = 0; i < 5; i++) {
      this.getBackerInfo(i)
    }
  }

  getMachinesInfo() {
    for (let i = 0; i < 5; i++) {
      try {
        this.getMachineInfo(i);
      } catch (e) {
        console.log(e);
      }
    }
  }

  getBackerInfo(cloneId) {
    this.watcherService.getBackerInfo(cloneId).subscribe(res => {
      cloneId = res.cloneId;
      // console.log('OOOOOOOOOOOO');
      // console.log(res);
      this.machineByBacker[res.data.backerAddress] = cloneId;
      this.machineByCBKey[res.data.machineKey] = cloneId;

    });
  }


  getCheatingInfos() {
    for (let i = 0; i < 5; i++) {
      this.getCheatingInfo(i)
    }
  }
  getCheatingInfo(cloneId) {
    this.watcherService.getCheatingInfo(cloneId).subscribe(res => {
      // console.log(res);
      cloneId = res.cloneId;
      this.machinesInfoObj[res.cloneId].cheatingCreationDate = res.data.cheatingCreationDate;
      this.machinesInfoObj[res.cloneId].cheatingAncestors = res.data.cheatingAncestors;

    });
  }


  detailedDag(cloneId) {
    this.watcherService.detailedDag(cloneId).subscribe(res => {
      cloneId = res.cloneId;
      let machineInfo = res.data.machineInfo;

      const bPrep = this.prepareBlocks(machineInfo.blocks);
      const nodes = new DataSet(bPrep.blockNodes);
      const edges = new DataSet(bPrep.blockEdges);
      let gData = {
        nodes: nodes,
        edges: edges
      };

      let options = {
        width: '900px',
        height: '5000px',
        edges: {
          font: {
            size: 12,
            strokeWidth: 4
          },
          widthConstraint: {
            maximum: 90
          }
        },
        nodes: {
          shape: 'box',
          margin: 5,
          widthConstraint: {
            maximum: 200
          }
        },

        physics: {
          enabled: false
        }
      };
      setTimeout(() => {
        try {
          const dtlGraph = new Network(document.getElementById('dagDtlId'), gData, options);
        } catch (e) {
          console.log(e);
        }

      }, 200);
    });
  }

  getMachineInfo(cloneId) {
    this.watcherService.getMachineInfo(cloneId).subscribe(res => {
      // console.log(res);
      const md5 = new Md5();
      cloneId = res.cloneId;
      let machineInfo = res.data.machineInfo;
      machineInfo.machineName = this.machinesInfoObj[cloneId].machineName
      machineInfo.utxo12h = this.machinesInfoObj[cloneId].utxo12h
      machineInfo.UTXOShoot = this.machinesInfoObj[cloneId].UTXOShoot
      machineInfo.missedBlocks = this.machinesInfoObj[cloneId].missedBlocks
      machineInfo.sendingQ = this.machinesInfoObj[cloneId].sendingQ
      machineInfo.DAGBalances = this.machinesInfoObj[cloneId].DAGBalances
      machineInfo.DAGBalancesPrev = this.machinesInfoObj[cloneId].DAGBalancesPrev
      machineInfo.DAGBalancesLock = this.machinesInfoObj[cloneId].DAGBalancesLock
      machineInfo.isBalanceLocked = this.machinesInfoObj[cloneId].isBalanceLocked
      machineInfo.blockGenReport = this.machinesInfoObj[cloneId].blockGenReport
      machineInfo.cheatingCreationDate = this.machinesInfoObj[cloneId].cheatingCreationDate
      machineInfo.cheatingAncestors = this.machinesInfoObj[cloneId].cheatingAncestors
      this.imgInfo = res.data.imgInfo;

      let timeDtl;
      timeDtl = this.imgInfo.machineTime.split((':'));
      this.imgInfo.machineTimeNormal = timeDtl[0] + ':'
      this.imgInfo.machineTimeBold = timeDtl[1] + ':' + timeDtl[2];

      timeDtl = this.imgInfo.cycleFrom.split((':'));
      this.imgInfo.cycleFromNormal = timeDtl[0] + ':'
      this.imgInfo.cycleFromBold = timeDtl[1] + ':' + timeDtl[2];

      timeDtl = this.imgInfo.cycleTo.split((':'));
      this.imgInfo.cycleToNormal = timeDtl[0] + ':'
      this.imgInfo.cycleToBold = timeDtl[1] + ':' + timeDtl[2];

      let cbs = []
      this.gUtils.objKeys(this.machineByCBKey).sort().forEach(element => {
        cbs.push(this.machinesInfoObj[this.machineByCBKey[element]].machineName);
      });
      machineInfo.machinesOrderedByCBPriority = machineInfo.coinbaseOrder.join(`<br>`);

      const bPrep = this.prepareBlocks(machineInfo.blocks);
      const nodes = new DataSet(bPrep.blockNodes);
      const edges = new DataSet(bPrep.blockEdges);
      machineInfo['dagId'] = 'dagId' + cloneId;
      machineInfo['gData'] = {
        nodes: nodes,
        edges: edges
      };

      let sendingQ = [];
      machineInfo.sendingQ.forEach(elm => {
        const hint = [elm.title, elm.sender, '->', elm.receiver, '(' + elm.sq_type + ' ' + elm.sq_code + ')'].join('\n');
        const hash = '\t' + elm.sq_type + ' ' + elm.sq_code; // this.gUtils.hash6c(md5.appendStr(hint).end().toString()) + ' ' +
        sendingQ.push({
          hint,
          hash
        });
      });
      machineInfo.sendingQ = sendingQ;

      machineInfo['outbox'] = this.prepareOutboxList(machineInfo.outbox);
      // UTXOsInfo = this.prepareUTXOStat(machineInfo.UTXOs);
      // machineInfo['utxo'] = this.prepareUTXOStat(machineInfo.UTXOs);
      // machineInfo['utxo'].sum = this.gUtils.microPAIToPAI(machineInfo['utxo'].sum);

      this.machinesInfoObj[cloneId] = machineInfo;
      this.machinesInfoArr = [];
      for (let i = 0; i < 5; i++) {
        this.machinesInfoArr.push(this.machinesInfoObj[i])
      }

      this.recursiveRefreshOn = false;
      // if (machineInfo['container'] === null) {
      //   this.recursiveRefreshOn = false;
      // }


      // console.log(machineInfo.histBlocks);
      if (machineInfo)
        machineInfo.histBlocks.forEach(aBlock => {
          this.blocksDict[aBlock.hash] = aBlock;
        });
      machineInfo.blocksDict = this.blocksDict;


      // coloring dag leaves

      let options = {
        width: '400px',
        height: '2400px',
        edges: {
          font: {
            size: 12,
            strokeWidth: 4
          },
          widthConstraint: {
            maximum: 90
          }
        },
        nodes: {
          shape: 'box',
          margin: 5,
          widthConstraint: {
            maximum: 200
          }
        },

        physics: {
          enabled: false
        }
      };

      // cookie check to launch watcher DAG graph tab
      setTimeout(() => {
        const neighborGraph = new Network(document.getElementById('dagId0'), this.machinesInfoObj[0]['gData'], options);
        try {
          const huGraph = new Network(document.getElementById('dagId1'), this.machinesInfoObj[1]['gData'], options);
        } catch (e) {
          console.log(e);
        }
        try {
          const aliceGraph = new Network(document.getElementById('dagId2'), this.machinesInfoObj[2]['gData'], options);
        } catch (e) {
          console.log(e);
        }
        try {
          const bobGraph = new Network(document.getElementById('dagId3'), this.machinesInfoObj[3]['gData'], options);
        } catch (e) {
          console.log(e);
        }
        try {
          const eveGraph = new Network(document.getElementById('dagId4'), this.machinesInfoObj[4]['gData'], options);
        } catch (e) {
          // console.log(e);
        }

      }, 200);

    });
  }

  equalnessDAG() {
    let blocksCount = 99999999;
    for (let i = 0; i < 5; i++) {
      // console.log(this.machinesInfoObj[i]);
      if ((this.machinesInfoObj[i] !== undefined) && (this.machinesInfoObj[i].histBlocks !== undefined)) {
        // console.log('kkkkkkkkkkkk');
        // console.log(this.machinesInfoObj[i].histBlocks.length);
        const len = this.machinesInfoObj[i].histBlocks.length;
        if (len > 0) {
          blocksCount = Math.min(len, blocksCount);
        }
      }
    }

    // console.log(`min blocksCount: ${blocksCount}`);
    this.equalIn5DagsInx = 0;
    for (let i = 0; i < blocksCount; i++) {
      let allAreEqual = true;
      if ((this.machinesInfoObj[1].histBlocks[i] !== undefined) && (this.machinesInfoObj[0].histBlocks[i].hash !== this.machinesInfoObj[1].histBlocks[i].hash)) {
        allAreEqual = false;
      }
      if ((this.machinesInfoObj[2].histBlocks[i] !== undefined) && (this.machinesInfoObj[0].histBlocks[i].hash !== this.machinesInfoObj[2].histBlocks[i].hash)) {
        allAreEqual = false;
      }
      if ((this.machinesInfoObj[3].histBlocks[i] !== undefined) && (this.machinesInfoObj[0].histBlocks[i].hash !== this.machinesInfoObj[3].histBlocks[i].hash)) {
        allAreEqual = false;
      }
      if ((this.machinesInfoObj[4].histBlocks[i] !== undefined) && (this.machinesInfoObj[0].histBlocks[i].hash !== this.machinesInfoObj[4].histBlocks[i].hash)) {
        allAreEqual = false;
      }

      if (allAreEqual) {
        this.equalIn5DagsInx = i;
        this.equalIn5DagsDate = this.machinesInfoObj[0].histBlocks[i].date;
        this.equalIn5DagsHash = this.machinesInfoObj[0].histBlocks[i].hash;
      } else {
        return i;
      }
    }
  }

  refreshDAG() {
    this.getMachinesInfo();
  }

  public getMachineNameByBackerAddress(backerAddress) {
    // console.log('this.machineByBacker', this.machineByBacker);
    // console.log('backerAddress', backerAddress);
    try {
      return this.machinesInfoObj[this.machineByBacker[backerAddress]].machineName;

    } catch (e) {
      return 'yohoo'
    }
  }

  public prepareBlocks(blocks) {
    let blockNodes = [];
    let blockEdges = [];
    let huY = -350;
    blocks.forEach(block => {
      huY += 171;
      let bId = this.gUtils.hash6c(block.blockHash);

      let lbl = `(${bId}) Anc:${block.ancestorsCount}
      \n${block.bType} ${block.confidence}% (${this.getMachineNameByBackerAddress(block.backerAddress)})
      \n${block.creationDate.split(' ')[1].split('.')[0]}
      \n${block.receiveDate ? block.receiveDate.split(' ')[1].split('.')[0] : 'unduu'}
      `;
      let color = this.gUtils.getBGColorByType(block.bType);

      blockNodes.push({
        id: bId,
        color: color,
        widthConstraint: { minimum: 120 },
        heightConstraint: { minimum: 70, valign: 'top' },
        y: huY,
        label: lbl,
        title: lbl
      });

      let ancestors = block.ancestors;
      ancestors.forEach(dad => {
        blockEdges.push({ from: bId, to: this.gUtils.hash6c(dad), arrows: 'to', dashes: true })
      });

    });

    return { blockNodes, blockEdges }
  }

  public toggleDAGReq() {
    this.recursiveRefreshOn = !this.recursiveRefreshOn;
    if (this.recursiveRefreshOn) {
      this.recursiveRefreshIcon = ' || Pause ';
      this.recursiveDAGRefresher();
    } else {
      this.recursiveRefreshIcon = ' > Play ';
    }
  }

  invokeBlock(args, cloneId) {
    // console.log(args);
    // console.log(`cloneIdW11: ${cloneId}`);
    // console.log(`blockShortHashW11: ${args.blockShortHash}`);
    this.watcherService.invokeBlock(cloneId, args.blockShortHash).subscribe(res => {
      // console.log(res);
    });
  }

  invokeAllMissed(eventObj, cloneId) {
    // console.log(args);
    // console.log(`cloneIdW11: ${cloneId}`);
    // console.log(`blockShortHashW11: ${args.blockShortHash}`);
    this.watcherService.invokeAllMissed(eventObj, cloneId).subscribe(res => {
      // console.log(res);
    });
  }

  refreshMissedBlock(cloneId) {
    this.watcherService.refreshMissedBlock(cloneId).subscribe(res => {
    });
  }

  dlTrxList(cloneId) {
    this.watcherService.dlTrxList(cloneId).subscribe(res => {
    });
  }

  notInHistory(cloneId, args) {
    console.log('not InHistory', args);
    this.watcherService.notInHistory(cloneId, args).subscribe(res => {
      console.log(`not InHistory`, res);
    });
  }

  public wAncestorsRes = [];
  getWAncestors(cloneId, args) {
    console.log('get WAncestors', args);
    this.watcherService.getWAncestors(cloneId, args).subscribe(res => {
      console.log(`get WAncestors`, res);
      this.wAncestorsRes = res.data.ancestors.join();
    });
  }

  alterBalanceLock(cloneId) {
    console.log('cloneId', cloneId);
    console.log('cloneId', cloneId);

    if (this.machinesInfoObj[cloneId].isBalanceLocked == 'unlocked') {
      this.machinesInfoObj[cloneId].isBalanceLocked = 'locked';
    } else {
      this.machinesInfoObj[cloneId].isBalanceLocked = 'unlocked';
    }
  }

  machineBalanceChk(cloneId) {
    if (this.machinesInfoObj[cloneId].isBalanceLocked == 'unlocked')
      this.machinesInfoObj[cloneId].DAGBalancesLock = this.machinesInfoObj[cloneId].DAGBalancesPrev;
    this.machinesInfoObj[cloneId].DAGBalancesPrev = this.machinesInfoObj[cloneId].DAGBalances;
    this.watcherService.machineBalanceChk(cloneId).subscribe(res => {
      cloneId = res.cloneId;
      // console.log('+++machineBalanceChk.res', res.data);
      this.machinesInfoObj[cloneId].DAGBalances = res.data;
    });
  }

  blockGenReport(cloneId) {
    this.watcherService.blockGenReport(cloneId).subscribe(res => {
      cloneId = res.cloneId;
      console.log('+++blockGenReport.res', res.data);
      this.machinesInfoObj[cloneId].blockGenReport = res.data;
    });
  }

  definitPushP4P(cloneId, eObj) {
    console.log(`cloneId ${cloneId}`);
    console.log(`testCase ${eObj.testCase}`);

    //invoke a signed noraml trasaction
    this.watcherService.signP4PTrx(cloneId, { testCase: eObj.testCase }).subscribe(ceasedTrx => {
      console.log(ceasedTrx);

      // make a dict for backers addresses
      [0, 1, 2, 3, 4].forEach(node => {
        this.watcherService.getLocalBackerAddress(node).subscribe(backerRes => {
          // console.log('+++backerRes.data', backerRes.data);
          this.candidBackers[backerRes.data.backerAddress] = node
        });
      });

      setTimeout(() => {
        // console.log(`candidBackers:`, this.candidBackers);
        let backersInfo = {};

        // testCase=1: 
        //  first ceased then one P4P
        //  delay 10s           30s  
        let orgDelay = 10000;
        backersInfo[ceasedTrx.data.trxDtl.orgBacker] = orgDelay;
        console.log(`P4P Org node(${this.candidBackers[ceasedTrx.data.trxDtl.orgBacker]}) afetr(${Math.floor(orgDelay / 1000)}s)`);
        setTimeout(() => {
          // make p4p org
          this.watcherService.sendP4POrg(cloneId, ceasedTrx.data.trxDtl).subscribe(p4pOrgRes => {
            console.log('p4pOrgRes: ', p4pOrgRes.data);
          });
        }, orgDelay);

        // to another backer
        let backerInx = Math.floor(Math.random() * this.gUtils.objKeys(this.candidBackers).length);
        let backerAddress = this.gUtils.objKeys(this.candidBackers)[backerInx];
        if (!this.gUtils.hasProp(backersInfo, backerAddress) && (backerAddress != undefined)) {
          let P4Pdelay = 30000;
          backersInfo[backerAddress] = P4Pdelay;
          let p4pNodeId = this.candidBackers[backerAddress];
          console.log(`P4P Wrapper node(${p4pNodeId}) afetr(${Math.floor(P4Pdelay / 1000)}s)`);
          setTimeout(() => {
            // make p4p wrapper
            this.watcherService.sendP4PWrapper(p4pNodeId, ceasedTrx.data.trxDtl).subscribe(p4pWRes => {
              console.log(`p4p pushed to machie(${p4pNodeId})`);
              // console.log('pushP4P p4pWRes.data', p4pWRes.data);
            });
          }, P4Pdelay);
        }
      }, 4000);
    });

  }

  pushP4P(cloneId) {
    this.candidBackers = {};
    this.cloneBroadcastDelay = 50000;

    //invoke a signed noraml trasaction
    this.watcherService.signP4PTrx(cloneId, {}).subscribe(ceasedTrx => {
      console.log(ceasedTrx);

      // make a dict for backers addresses
      [0, 1, 2, 3, 4].forEach(node => {
        this.watcherService.getLocalBackerAddress(node).subscribe(backerRes => {
          // console.log('+++backerRes.data', backerRes.data);
          this.candidBackers[backerRes.data.backerAddress] = node
        });
      });

      setTimeout(() => {

        // console.log(`candidBackers:`, this.candidBackers);
        let backersInfo = {};

        let delay = Math.floor(Math.random() * this.cloneBroadcastDelay);
        backersInfo[ceasedTrx.data.trxDtl.orgBacker] = delay;
        console.log(`P4P Org node(${this.candidBackers[ceasedTrx.data.trxDtl.orgBacker]}) afetr(${Math.floor(delay / 1000)}s)`);
        setTimeout(() => {
          // make p4p org
          this.watcherService.sendP4POrg(cloneId, ceasedTrx.data.trxDtl).subscribe(p4pOrgRes => {
            console.log('p4pOrgRes: ', p4pOrgRes.data);
          });
        }, delay);

        // init random delay and backer(s)
        let P4PBackersCount = Math.floor(Math.random() * (this.gUtils.objKeys(this.candidBackers).length - 1)) + 1;// send to how many backer?
        while (P4PBackersCount > 0) {
          P4PBackersCount--;
          let backerInx = Math.floor(Math.random() * this.gUtils.objKeys(this.candidBackers).length);
          let backerAddress = this.gUtils.objKeys(this.candidBackers)[backerInx];
          if (!this.gUtils.hasProp(backersInfo, backerAddress) && (backerAddress != undefined)) {
            let delay = Math.floor(Math.random() * this.cloneBroadcastDelay);
            backersInfo[backerAddress] = delay;
            let p4pNodeId = this.candidBackers[backerAddress];
            console.log(`P4P Wrapper node(${p4pNodeId}) afetr(${Math.floor(delay / 1000)}s)`);
            setTimeout(() => {
              // make p4p wrapper
              this.watcherService.sendP4PWrapper(p4pNodeId, ceasedTrx.data.trxDtl).subscribe(p4pWRes => {
                console.log(`p4p pushed to machie(${p4pNodeId})`);
                // console.log('pushP4P p4pWRes.data', p4pWRes.data);
              });
            }, delay);
          }
        }
        // console.log('backersInfo', backersInfo);
      }, 4000);
    });
  }

  sendClonedTrx(cloneId) {
    // this.candidTransactions = [];
    console.log(`start cloning by node(${cloneId})`);
    this.candidBackers = {};
    this.cloneBroadcastDelay = 50000;
    [0, 1, 2, 3, 4].forEach(node => {
      try {
        this.watcherService.getLocalBackerAddress(node).subscribe(backerRes => {
          // console.log('+++backerRes.data', backerRes.data);
          this.candidBackers[backerRes.data.backerAddress] = node
        });
      } catch (e) {
      }
    });

    setTimeout(() => {
      console.log(`candid Backers for participate in clone: `, this.candidBackers);
      this.watcherService.signClonedTrx(cloneId, { candidBackers: this.candidBackers }).subscribe(signRes => {
        // console.log('this.create clone trx---');
        console.log(`signed clon Trx Res:`, signRes);

        let delays = []
        signRes.data.selectedBackers.forEach(backerAddress => {
          let rand = Math.floor(Math.random() * this.cloneBroadcastDelay);
          delays.push([backerAddress, rand]);
          this.cloneBroadcastDelay *= 7;  // increase sending time for clones
          console.log(`sending clone to(${signRes.data.candidBackers[backerAddress]}:${Math.floor(rand / 1000)}s)`);
        });

        let delayInx = 0;
        delays.forEach(delyInf => {
          setTimeout(() => {
            let cloneId = signRes.data.candidBackers[delyInf[0]];
            console.log(`broadcast Cloned Trx to clone(${cloneId})`);
            this.watcherService.broadcastClonedTrx(cloneId, { trx: signRes.data.trx }).subscribe(braodRes => {
              console.log('broadcast Cloned Trx braodRes.data', braodRes.data);
            });
          }, delays[delayInx][1]);
          delayInx++;
        });
      });
    }, 2000);
  }

  pushCoinbase(cloneId) {
    this.watcherService.pushCoinbase(cloneId).subscribe(res => {
      console.log('pushCoinbase res: ', res);
    });
  }

  wBroadcastBlock(cloneId, eObj) {
    this.watcherService.wBroadcastBlock(cloneId, eObj).subscribe(res => {
      console.log('wBroadcastBlock res: ', res);
    });
  }

  wRaiseShares(cloneId) {
    this.watcherService.wGetShares(cloneId).subscribe(res => {
      this.doWRaiseShares(res.data);
    });
  }

  doWRaiseShares(shareInfo) {
    // console.log('shareInfo', shareInfo);
    for (let n = 0; n < 5; n++) {
      this.watcherService.wRaiseShares(n, shareInfo).subscribe(res => {
        // console.log(n, res);
      });
    }
  }

  public makeTrx(cloneId, eventObj) {
    console.log(`makeTrx2222 cloneId ${cloneId}`);
    // console.log(`makeTrx2222 eventObj: `, eventObj);
    // console.log(this.machinesInfoObj);

    let receiversCount = Math.floor(Math.random() * (this.MAX_MONEY_RECEIVER - 1)) + this.MIN_MONEY_RECEIVER;
    // console.log('receiversCount', receiversCount);

    let receivers = [];
    switch (eventObj.target) {
      case 'certainNode':
        receivers = [];
        for (let i = 0; i < receiversCount; i++) {
          let machineId = eventObj.paiReceiverNode;
          let walletAddresses = this.machinesInfoObj[machineId].walletAddresses;
          let receiver = walletAddresses[Math.floor(Math.random() * walletAddresses.length)];
          receivers.push(receiver);
        }
        break;

      case 'random':
        receivers = [];
        for (let i = 0; i < receiversCount; i++) {
          let machineId = Math.floor(Math.random() * 5);
          console.log('+++machineId: ', machineId);
          console.log('+++this.machinesInfoObj[machineId]: ', this.machinesInfoObj[machineId]);
          let walletAddresses = this.machinesInfoObj[machineId].walletAddresses;
          let receiver = walletAddresses[Math.floor(Math.random() * walletAddresses.length)];
          receivers.push(receiver);
        }
        break;

      default:
        receivers = [];
        break;

    }
    // console.log('receivers: ', receivers);
    this.watcherService.makeTrx({
      cloneId,
      mode: eventObj.mode,
      unEqua: eventObj.unEqua,
      receivers,
      paiReceiverNode: eventObj.paiReceiverNode,
      createDateType: eventObj.createDateType
    }).subscribe(res => {
      // console.log(res);
      this.getCheatingInfo(cloneId);
    });
  }
}
