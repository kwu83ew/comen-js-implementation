import { Injectable } from '@angular/core';
import { HttpService } from "../../services/http.service";
import { Block } from '../../models/watcher/Block';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from '../../errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class WatcherService {
  http;
  domain;
  url;
  blocks: Block[];

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.domain = this.httpService.domain;
    this.url = this.httpService.url;
  }


  getMachineInfo(cloneId) {
    const url = this.getMachineURL(cloneId)
    return this.http.get(url + '/api/watcher/getMachineInfo').pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );

  }

  detailedDag(cloneId) {
    const url = this.getMachineURL(cloneId)
    return this.http.get(url + '/api/watcher/detailedDag').pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  wGetShares(cloneId) {
    const url = this.getMachineURL(cloneId)
    return this.http.get(url + '/api/watcher/wGetShares').pipe(
      map(data => {
        return { data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  wRaiseShares(cloneId, shareInfo) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/wRaiseShares', shareInfo).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  pushCoinbase(cloneId) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/pushCoinbase', {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  wBroadcastBlock(cloneId, eObj) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/wBroadcastBlock', eObj).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getSomeSpendableRefs(cloneId) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/getSomeSpendableRefs', {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  machineBalanceChk(cloneId) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/machineBalanceChk', {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  blockGenReport(cloneId) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/blockGenReport', {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getLocalBackerAddress(cloneId) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/getLocalBackerAddress', {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  signP4PTrx(cloneId, args) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/signP4PTrx', args).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  sendP4POrg(cloneId, args) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/sendP4POrg', args).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  sendP4PWrapper(cloneId, args) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/sendP4PWrapper', args).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  signClonedTrx(cloneId, args) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/signClonedTrx', args).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  broadcastClonedTrx(cloneId, args) {
    const url = this.getMachineURL(cloneId);
    return this.http.put(url + '/api/watcher/broadcastClonedTrx', args).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  invokeBlock(cloneId, blockShortHash) {
    // console.log(`blockShortHash222: ${blockShortHash}`);
    const url = this.getMachineURL(cloneId)
    return this.http.put(url + `/api/watcher/doInvokeBlock`, { blockShortHash }).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  invokeAllMissed(evenObj, cloneId) {
    // console.log(`blockShortHash222: ${blockShortHash}`);
    const url = this.getMachineURL(cloneId)
    return this.http.put(url + `/api/watcher/invokeAllMissed`, {}).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  refreshMissedBlock(cloneId) {
    const url = this.getMachineURL(cloneId)
    return this.http.get(url + `/api/watcher/refreshMissedBlock`).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  dlTrxList(cloneId) {
    const url = this.getMachineURL(cloneId)
    return this.http.put(url + `/api/watcher/dlTrxList`).pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getBlockInfo(args) {
    const url = this.getMachineURL(args.cloneId)
    return this.http.put(url + `/api/watcher/getBlockInfo`, args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  notInHistory(cloneId, args) {
    const url = this.getMachineURL(cloneId)
    return this.http.put(url + `/api/watcher/notInHistory`, args).pipe(
      map(data => {
        return { args, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getWAncestors(cloneId, args) {
    const url = this.getMachineURL(cloneId)
    return this.http.put(url + `/api/watcher/getWAncestors`, args).pipe(
      map(data => {
        return { args, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getBackerInfo(cloneId) {
    let url = this.getMachineURL(cloneId)
    return this.http.get(url + '/api/watcher/getBackerInfo').pipe(
      map(data => {
        return { cloneId: cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getCheatingInfo(cloneId) {
    let url = this.getMachineURL(cloneId)
    return this.http.get(url + '/api/watcher/getCheatingInfo').pipe(
      map(data => {
        return { cloneId, data };
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  rmvFromBuffer(args) {
    let url = this.getMachineURL(args.cloneId)
    return this.http.put(url + '/api/watcher/rmvFromBuffer', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  // public analyzeDag(args) {
  //   let url = this.getMachineURL(args.cloneId)
  //   return this.http.put(url + '/api/watcher/aMachineStat', args).pipe(
  //     map(data => {
  //       return { cloneId: args.cloneId, data };
  //     }),
  //     catchError(error => {
  //       return throwError(new AppError());
  //     })
  //   );
  // }

  public getMachineURL(cloneId) {
    cloneId = parseInt(cloneId);
    let port = 1717
    if (cloneId > 0)
      port += cloneId;
    let url = this.domain + port;
    // console.log(url);
    return url
  }

  public makeTrx(args) {
    let url = this.getMachineURL(args.cloneId)
    return this.http.put(url + '/api/watcher/makeTrx', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }



}
