import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ANodeComponent } from './a-node.component';

describe('ANodeComponent', () => {
  let component: ANodeComponent;
  let fixture: ComponentFixture<ANodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ANodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ANodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
