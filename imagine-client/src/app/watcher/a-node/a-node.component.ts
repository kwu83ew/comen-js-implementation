import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'a-node',
  templateUrl: './a-node.component.html',
  styleUrls: ['./a-node.component.css']
})
export class ANodeComponent implements OnInit {
  @Input('machineInfo') machineInfo;
  @Input('wAncestorsRes') wAncestorsRes;
  @Output() getMachineInfoParent = new EventEmitter<any>();
  @Output() detailedDagParent = new EventEmitter<any>();
  @Output() wRaiseSharesParent = new EventEmitter<any>();
  @Output() pushCoinbaseParent = new EventEmitter<any>();
  @Output() wBroadcastBlockParent = new EventEmitter<any>();
  @Output() pushP4PParent = new EventEmitter<any>();
  @Output() definitPushP4PParent = new EventEmitter<any>();
  @Output() sendClonedTrxParent = new EventEmitter<any>();
  @Output() machineBalanceChkParent = new EventEmitter<any>();
  @Output() machineBalanceChk3xParent = new EventEmitter<any>();
  @Output() alterBalanceLockParent = new EventEmitter<any>();
  @Output() blockGenReportParent = new EventEmitter<any>();
  @Output() invokeBlockParent = new EventEmitter<any>();
  @Output() invokeAllMissedParent = new EventEmitter<any>();
  @Output() refreshMissedBlockParent = new EventEmitter<any>();
  @Output() dlTrxListParent = new EventEmitter<any>();
  @Output() notInHistoryParent = new EventEmitter<any>();
  @Output() getWAncestorsParent = new EventEmitter<any>();
  @Output() makeTrxParent = new EventEmitter<any>();
  @Input('equalIn5DagsHash') equalIn5DagsHash;

  public selectedMachine;

  public blockShortHash = '';
  public createDateType = 'real';
  public paiReceiverNode = '';
  public calledTime = 0;

  constructor(public gUtils: GUtils) { }

  ngOnInit() {
  }

  getMachineInfo() {
    // this.selectedMachine = i;
    this.getMachineInfoParent.next();
  }

  detailedDag(i) {
    this.selectedMachine = i;
    this.detailedDagParent.next();
  }

  wRaiseShares() {
    this.wRaiseSharesParent.next();
  }

  pushP4P() {
    this.pushP4PParent.next();
  }

  definitPushP4P(testCase) {
    this.definitPushP4PParent.next({ testCase });
  }

  wBroadcastBlock() {
    ;
    this.wBroadcastBlockParent.next({ createDateType: this.createDateType });
  }

  pushCoinbase() {
    this.pushCoinbaseParent.next();
  }
  sendClonedTrx() {
    this.sendClonedTrxParent.next();
  }

  machineBalanceChk(i) {
    this.machineBalanceChkParent.next();
  }

  machineBalanceChk3x(i) {
    this.calledTime = 0;
    this.callerr3();
  }

  callerr3() {
    if (this.calledTime < 3) {
      this.calledTime++;
      setTimeout(() => {
        this.machineBalanceChkParent.next();
        this.callerr3();
      }, 713);
    }
  }

  alterBalanceLock() {
    this.alterBalanceLockParent.next();
  }

  blockGenReport(i) {
    this.blockGenReportParent.next();
  }

  invokeBlock(e, cloneId) {
    this.selectedMachine = cloneId;
    this.invokeBlockParent.next({ blockShortHash: this.blockShortHash });
  }

  invokeAllMissed(e, cloneId) {
    // this.selectedMachine = cloneId;
    this.invokeAllMissedParent.next();
  }
  refreshMissedBlock() {
    this.refreshMissedBlockParent.next();
  }
  dlTrxList() {
    this.dlTrxListParent.next();
  }


  public leaveShortHash;
  notInHistory() {
    this.notInHistoryParent.next({ leaveShortHash: this.leaveShortHash });
  }

  public stepBack = 1;
  getWAncestors() {
    this.getWAncestorsParent.next({ leaveShortHash: this.leaveShortHash, stepBack: this.stepBack });
  }

  makeTrx(args) {//{mode:, target:, unEqua:}
    console.log(`makeTrx111 ${args}`);
    args.paiReceiverNode = this.paiReceiverNode;
    args.createDateType = this.createDateType;
    this.makeTrxParent.next(args);
  }

}
