import { Injectable } from '@angular/core';
import { HttpService } from "../../services/http.service";
import { catchError, map } from "rxjs/operators";
import { throwError } from "rxjs";
import { AppError } from "../../errors/app-error";

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }


  public loadMachineProfile(args) {
    return this.http.put(this.url + '/api/settings/loadMachineProfile', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


  public loadMachineNeighbors(args) {
    return this.http.put(this.url + '/api/settings/loadMachineNeighbors', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


  public getLanguagesList(args) {
    return this.http.put(this.url + '/api/settings/getLanguagesList', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public setActiveMP(args) {
    return this.http.put(this.url + '/api/settings/setActiveMP', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getActiveMP(args) {
    return this.http.put(this.url + '/api/settings/getActiveMP', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getMProfilesList(args) {
    return this.http.put(this.url + '/api/settings/getMProfilesList', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public addNewMProfile(args) {
    return this.http.put(this.url + '/api/settings/addNewMProfile', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public saveEmailsSettings(args) {
    return this.http.put(this.url + '/api/settings/saveEmailsSettings', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public testEmailServerIncomeConnection(args) {
    return this.http.put(this.url + '/api/settings/testEmailServerIncomeConnection', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  public testEmailServerOutgoingConnection(args) {
    return this.http.put(this.url + '/api/settings/testEmailServerOutgoingConnection', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public addNeighbor(args) {
    return this.http.put(this.url + '/api/settings/addNeighbor', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public handshakeNeighbor(args) {
    return this.http.put(this.url + '/api/settings/handshakeNeighbor', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public presentEmailToNeighbors(args) {
    return this.http.put(this.url + '/api/settings/presentEmailToNeighbors', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public cutNeighbor(args) {
    return this.http.put(this.url + '/api/settings/cutNeighbor', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public refreshNeighbors() {
    return this.http.get(this.url + '/api/settings/refreshNeighbors').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public deleteMProfile(args) {
    return this.http.put(this.url + '/api/settings/deleteMProfile', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public backupMaWa(args) {
    return this.http.put(this.url + '/api/settings/backupMaWa', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public controllMaWa(args) {
    return this.http.put(this.url + '/api/settings/controllMaWa', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public restoreMaWa(args) {
    return this.http.put(this.url + '/api/settings/restoreMaWa', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


  // public cutNeighbor(id) {
  //   return this.http.delete(this.url + '/api/machine/neighbor/delete/' + id).pipe(
  //     map(data => {
  //       return data;
  //     }),
  //     catchError(error => {
  //       return throwError(new AppError());
  //     })
  //   );
  // }


}
