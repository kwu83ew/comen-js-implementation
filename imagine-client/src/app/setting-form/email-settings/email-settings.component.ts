import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'img-email-settings',
  templateUrl: './email-settings.component.html',
  styleUrls: ['./email-settings.component.css']
})
export class EmailSettingsComponent implements OnInit {

  @Input('IMAPNotiMsg') IMAPNotiMsg;
  @Input('SMTPNotiMsg') SMTPNotiMsg;
  @Input('emailSettings') emailSettings;
  @Input('emailType') emailType;
  @Output() validateSettingsFormParent = new EventEmitter<any>();
  @Output() testEmailServerIncomeConnectionParent = new EventEmitter<any>();
  @Output() testEmailServerOutgoingConnectionParent = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  validateSettingsForm() {
    this.validateSettingsFormParent.next();
  }

  testEmailServerIncomeConnection(emailType) {
    this.testEmailServerIncomeConnectionParent.next({ emailType });
  }
  public testReceiver = '';
  public testEmailServerOutgoingConnection(emailType) {
    if (this.testReceiver == '') {
      alert('please insert Receiver email address!');
      return
    }
    let params = { emailType, receiver: this.testReceiver };
    console.log('params', params);
    alert(`Please chek (${this.testReceiver}) inbox!`);
    this.testEmailServerOutgoingConnectionParent.next(params);
  }


  public dspEmailSendForm = false;
  public alterSendFormDsp() {
    this.dspEmailSendForm = !this.dspEmailSendForm;
  }
}
