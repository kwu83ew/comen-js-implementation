import { Component, OnInit, Input } from '@angular/core';
import { SettingService } from "./services/setting.service";
import { GUtils } from 'src/app/services/utils';
import { CookieService } from 'src/app/services/cookie.service';

@Component({
  selector: 'setting-form',
  templateUrl: './setting-form.component.html',
  styleUrls: ['./setting-form.component.css']
})
export class SettingFormComponent implements OnInit {

  public isSettingsFormValid = false;
  public mProfiles = null; //[{ mpCode: 'Default', mpName: 'Default' }];
  public selectedMProfileRadio = null;
  public selectedMPCode = null;
  public selectedMP = null;
  public languagesList = [];
  public sFNoti = '';
  public pNotiMsg2 = '';
  public pNotiMsg3 = '';
  public dspMProfilePanel = false;
  public termOfServices = false;

  public publicEmailSettings;
  public privateEmailSettings;
  public Public = 'Public';
  public publicNeighbors;
  public Private = 'Private';
  public privateNeighbors;

  public newProfileCode = '';
  public newProfileName = '';


  public neighbors = null;

  constructor(
    public gUtils: GUtils,
    private sService: SettingService,
    public cookie: CookieService
  ) {
    if (this.selectedMPCode == null || this.selectedMPCode === '') {
      this.getActiveMP();
    }
  }

  ngOnInit() {
    if (this.languagesList.length == 0)
      this.getLanguagesList();

    if (this.mProfiles === null) {
      this.getMProfilesList();
    }
    if (this.selectedMP === null) {
      this.loadMachineProfile();
    }
    if (this.neighbors === null) {
      this.loadMachineNeighbors();
    }
  }

  public setActiveMP() {
    console.log('setActiveMP', this.selectedMPCode);
    this.sFNoti = '';

    this.sService.setActiveMP({
      selectedMPCode: this.selectedMPCode
    }).subscribe(res => {
      this.loadMachineProfile();
      this.loadMachineNeighbors();
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }


  public getActiveMP(doReload = '') {
    this.sService.getActiveMP({}).subscribe(res => {
      this.selectedMPCode = res.activeMP;
      this.cookie.setCookie('selectedMPCode', this.selectedMPCode, 99);
      if (doReload != '') {
        this[doReload]();
      }
    });
  }

  public getMProfilesList() {
    this.sService.getMProfilesList({}).subscribe(res => {
      this.mProfiles = res;
    });
  }

  public addNewMProfile() {
    this.sFNoti = '';
    if ((this.newProfileCode == '') || (this.newProfileName == '')) {
      this.sFNoti = `invalid Profile Code(${this.newProfileCode}) or Profile Name(${this.newProfileName})`;
      return;
    }
    this.sService.addNewMProfile({
      newProfileCode: this.newProfileCode,
      newProfileName: this.newProfileName,
    }).subscribe(res => {
      this.getMProfilesList();
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }

  public loadMachineProfile() {
    if (this.selectedMPCode == null) {
      this.getActiveMP('loadMachineProfile');
      return;
    }
    this.sService.loadMachineProfile({
      selectedMPCode: this.selectedMPCode
    }).subscribe(res => {
      console.log('loadMachineProfile', res);
      this.selectedMP = res.selectedMP;
      this.publicEmailSettings = this.selectedMP.pubEmail;
      this.privateEmailSettings = this.selectedMP.prvEmail;
      this.termOfServices = this.selectedMP.termOfServices == 'Y';
    });
  }

  public loadMachineNeighbors() {
    if (this.selectedMPCode == null) {
      this.getActiveMP('loadMachineNeighbors');
      return;
    }
    this.sService.loadMachineNeighbors({
      selectedMPCode: this.selectedMPCode
    }).subscribe(res => {
      // console.log('loadMachineNeighbors', res);
      this.neighbors = res;
      this.publicNeighbors = this.neighbors.publicNeighbors;
      this.privateNeighbors = this.neighbors.privateNeighbors;
    });
  }

  public getLanguagesList() {
    this.sService.getLanguagesList({}).subscribe(res => {
      this.languagesList = res;
    });
  }

  public pubIMAPNotiMsg = '';
  public prvIMAPNotiMsg = '';
  public testEmailServerIncomeConnection(e) {
    console.log('in  testEmailServerIncomeConnection', e);
    let params;
    if (e.emailType == 'Private') {
      params = {
        emailType: e.emailType,
        address: this.selectedMP.prvEmail.address,
        pwd: this.selectedMP.prvEmail.pwd,
        incomingMailServer: this.selectedMP.prvEmail.incomingMailServer,
        incomeIMAP: this.selectedMP.prvEmail.incomeIMAP,
        incomePOP3: this.selectedMP.prvEmail.incomePOP3
      };
    } else {
      params = {
        emailType: e.emailType,
        address: this.selectedMP.pubEmail.address,
        pwd: this.selectedMP.pubEmail.pwd,
        incomingMailServer: this.selectedMP.pubEmail.incomingMailServer,
        incomeIMAP: this.selectedMP.pubEmail.incomeIMAP,
        incomePOP3: this.selectedMP.pubEmail.incomePOP3
      };
    }

    this.sService.testEmailServerIncomeConnection(params).subscribe(res => {
      console.log('res0000', res);
      if (e.emailType == 'Private') {
        this.prvIMAPNotiMsg = res.msg;
      } else {
        this.pubIMAPNotiMsg = res.msg;
      }
    });
  }

  public pubSMTPNotiMsg = '';
  public prvSMTPNotiMsg = '';
  public testEmailServerOutgoingConnection(e) {
    console.log('in  testEmailServerOutgoingConnection', e);
    let params;
    if (e.emailType == 'Private') {
      params = {
        emailType: e.emailType,
        address: this.selectedMP.prvEmail.address,
        receiver: e.receiver,
        pwd: this.selectedMP.prvEmail.pwd,
        outgoingMailServer: this.selectedMP.prvEmail.outgoingMailServer,
        outgoingSMTP: this.selectedMP.prvEmail.outgoingSMTP
      };
    } else {
      params = {
        emailType: e.emailType,
        address: this.selectedMP.pubEmail.address,
        receiver: e.receiver,
        pwd: this.selectedMP.pubEmail.pwd,
        outgoingMailServer: this.selectedMP.pubEmail.outgoingMailServer,
        outgoingSMTP: this.selectedMP.pubEmail.outgoingSMTP
      };
    }

    this.sService.testEmailServerOutgoingConnection(params).subscribe(res => {
      console.log('res0000', res);
      if (e.emailType == 'Private') {
        if (res.msg)
          this.prvSMTPNotiMsg = res.msg;
      } else {
        if (res.msg)
          this.pubSMTPNotiMsg = res.msg;
      }
    });
  }

  public validateSettingsForm() {
    this.isSettingsFormValid = false;
    this.sFNoti = ``;

    if (this.selectedMP.machineAlias == '') {
      this.sFNoti = `invalid machine Alias(${this.selectedMP.machineAlias}) name`;
      return false;
    }
    // validating only public email
    if (this.selectedMP.pubEmail.address == '') {
      this.sFNoti = `invalid Public email(${this.selectedMP.pubEmail.address}) address`;
      return false;
    }
    if (this.selectedMP.pubEmail.pwd == '') {
      this.sFNoti = `invalid pwd(${this.selectedMP.pubEmail.pwd})`;
      return false;
    }
    if (this.selectedMP.pubEmail.outgoingSMTP == '') {
      this.sFNoti = `invalid smtp(${this.selectedMP.pubEmail.outgoingSMTP})`;
      return false;
    }
    if (this.selectedMP.pubEmail.incomeIMAP == '') {
      this.sFNoti = `invalid incomeIMAP(${this.selectedMP.pubEmail.incomeIMAP})`;
      return false;
    }
    // if (this.selectedMP.pubEmail.incomingMailServer == '') {
    //   this.sFNoti = `invalid incomingMailServer(${this.selectedMP.pubEmail.incomingMailServer})`;
    //   return false;
    // }
    if (this.selectedMP.pubEmail.outgoingMailServer == '') {
      this.sFNoti = `invalid outgoingMailServer(${this.selectedMP.pubEmail.outgoingMailServer})`;
      return false;
    }
    if ((this.selectedMP.pubEmail.fetchingIntervalByMinute == '') || !(parseInt(this.selectedMP.pubEmail.fetchingIntervalByMinute) > 0)) {
      this.sFNoti = `invalid Checking Interval(${this.selectedMP.pubEmail.fetchingIntervalByMinute})`;
      return false;
    }

    console.log(this.termOfServices);
    if (!this.termOfServices) {
      this.sFNoti = `Please check the term of services`;
      return false;
    }
    this.isSettingsFormValid = true;

  }

  public saveEmailsSettings() {
    this.validateSettingsForm();
    if (!this.isSettingsFormValid)
      return;

    let updateValues = this.selectedMP;
    updateValues.mpCode = this.selectedMPCode;
    updateValues.termOfServices = 'Y';
    this.sService.saveEmailsSettings({ updateValues }).subscribe(res => {
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }




  // neighbor part

  addNeighbor(e) {
    e.mpCode = this.selectedMPCode;
    console.log('conncetionType conncetionType', e);
    this.sService.addNeighbor(e).subscribe(res => {
      this.loadMachineNeighbors();
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }

  cutNeighbor(nId) {
    this.sService.cutNeighbor({ nId }).subscribe(res => {
      this.loadMachineNeighbors();
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }

  handshakeNeighbor(e) {
    e.mpCode = this.selectedMPCode;
    console.log('conncetionType conncetionType', e);
    this.sService.handshakeNeighbor(e).subscribe(res => {
      // this.loadMachineNeighbors();
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }

  presentEmailToNeighbors(e) {
    console.log('presentEmailToNeighbors', e);
    this.sService.presentEmailToNeighbors(e).subscribe(res => {
      if (res.msg)
        this.sFNoti = res.msg;
    });
  }

  deleteMProfile() {
    console.log(`selectedMProfileRadio ${this.selectedMProfileRadio}`);
    this.sService.deleteMProfile({ mpCode: this.selectedMProfileRadio }).subscribe(res => {
      if (res.msg)
        this.pNotiMsg2 = res.msg;
      this.getMProfilesList();
    });
  }

  alterDspProfSetting() {
    this.dspMProfilePanel = !this.dspMProfilePanel;
  }


  backupMaWa() {
    this.sService.backupMaWa({}).subscribe(res => {
      console.log(res);
      if (res.msg) {
        this.pNotiMsg3 = res.msg;
      }
    });
  }

  controllMaWa() {
    this.sService.controllMaWa({}).subscribe(res => {
      console.log(res);
      if (res.err === true) {
        this.pNotiMsg3 = res.msg;
      } else {
        this.pNotiMsg3 = '';
      }
    });
  }

  public restoreMaWa(dTarget) {  //uploadNodeScreenShot
    this.gUtils.onUploadG({
      dTarget,
      callback: this.callback_restoreMaWa.bind(this),
    });
    setTimeout(() => {

    }, 2000);
  }

  // public dlNodeScreenShot(dTarget) {
  //   this.gUtils.dlNodeScreenShot({
  //     dTarget,
  //     dNeighbor: this.screenShotToNeighbor,
  //     callback: this.callback_dlNodeScreenShot.bind(this),
  //   });
  // }
  public callback_restoreMaWa() {
    console.log('Done callback_restoreMaWa this', this);
    this.getMProfilesList();
  }

}
