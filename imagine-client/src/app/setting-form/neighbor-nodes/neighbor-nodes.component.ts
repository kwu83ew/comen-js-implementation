import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'neighbor-nodes',
  templateUrl: './neighbor-nodes.component.html',
  styleUrls: ['./neighbor-nodes.component.css']
})
export class NeighborNodesComponent implements OnInit {
  public isEmailFormValid = false;

  neighbors: any = [];
  nEmail = '';
  nPubKey = '';


  @Input('neighborsList') neighborsList;
  @Input('nConnectionType') nConnectionType;
  @Output() addNeighborParent = new EventEmitter<any>();
  @Output() cutNeighborParent = new EventEmitter<any>();
  @Output() handshakeNeighborParent = new EventEmitter<any>();
  @Output() presentEmailToNeighborsParent = new EventEmitter<any>();


  constructor(private sService: SettingService) {
    // if (this.neighbors.length == 0) {
    //   this._refreshNeighbors();
    // }
  }

  ngOnInit() {
    // if (this.neighbors == 0) {
    //   this._refreshNeighbors();
    // }
  }

  addNeighbor() {
    console.log(' in child addNeighbor ', this.nConnectionType);
    this.addNeighborParent.next({
      nConnectionType: this.nConnectionType,
      nEmail: this.nEmail,
      nPubKey: this.nPubKey
    });
  }

  cutNeighbor(nId) {
    console.log(' in child cutNeighbor ', nId);
    this.cutNeighborParent.next({ nId });
  }


  handshakeNeighbor(nId, nConnectionType) {
    this.handshakeNeighborParent.next({nId, nConnectionType});
  }

  public presentEmailToNeighbors(nEmail) {
    console.log('nEmail', nEmail);
    this.presentEmailToNeighborsParent.next({nEmail});
  }
  public notImplementedYet(){
    alert('not implemented yet');
  }


  // _refreshNeighbors() {
  //   this.sService.refreshNeighbors()
  //     .subscribe(
  //       response => {
  //         // this.defSetting = response;
  //         // console.log('eeeeeeeeeeeeeeeeeeeeeee')
  //         // console.log(response)
  //         this.neighbors = response;
  //         // this._refreshList();
  //       },
  //       (error: Response) => {
  //         if (error.status==404)
  //           alert('Default setting does not exist');
  //       });
  // }

  // // _refreshList() {
  // // }

  // cutNeighbor(id) {
  //   this.sService.cutNeighbor(id)
  //   .subscribe(
  //     response => {
  //       this.neighbors = response;
  //       // this.neighbors = JSON.parse(response);
  //       console.log(response);
  //     },
  //     (error: Response) => {
  //       if (error.status==404)
  //         alert('Default setting does not exist');
  //     });
  // }



}
