import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeighborNodesComponent } from './neighbor-nodes.component';

describe('NeighborNodesComponent', () => {
  let component: NeighborNodesComponent;
  let fixture: ComponentFixture<NeighborNodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeighborNodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeighborNodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
