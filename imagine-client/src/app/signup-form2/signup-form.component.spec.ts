import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupFormComponent2 } from './signup-form.component';

describe('SignupFormComponent', () => {
  let component: SignupFormComponent2;
  let fixture: ComponentFixture<SignupFormComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupFormComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupFormComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
