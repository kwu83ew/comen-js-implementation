import { Component } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {UsernameValidators} from "./username.validators";

@Component({
  selector: 'signup-form2',
  templateUrl: './setting-form.component.html',
  styleUrls: ['./setting-form.component.css']
})
export class SignupFormComponent2 {
  form = new FormGroup({
    username: new FormControl('',
      [
        Validators.required,
        Validators.minLength(3),
        UsernameValidators.canNotContainSpace,
        UsernameValidators.shouldBeUniq
      ]),
    password: new FormControl('', Validators.required)
  });

  get username(){
    return this.form.get('username');
  }
  get password(){
    return this.form.get('password');
  }
}
