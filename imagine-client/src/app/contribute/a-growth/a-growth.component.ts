import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { Graph2d, DataSet } from 'vis';

@Component({
  selector: 'a-growth',
  templateUrl: './a-growth.component.html',
  styleUrls: ['./a-growth.component.css']
})
export class AGrowthComponent implements OnInit {
  @Input('coinsInfo') coinsInfo;

  years = [0, 1, 2, 3, 4, 5, 6];
  graph2dMyPAIs;

  constructor(public gUtils: GUtils) { }

  ngOnInit() {
    this.dspMyPAIin7Estimat();
  }

  public dspMyPAIin7Estimat() {
    setTimeout(() => {
      let myPAIsContainer = document.getElementById(`myPAIsIn7Years${this.coinsInfo.cAGrowth}`);
      if (!myPAIsContainer)
        return;
      let graphItems = [];
      for (let aMonth of this.coinsInfo.monthlyIncomes) {
        graphItems.push({ x: aMonth.due, y: Math.floor(aMonth.incomePerMonth / 100000), group: 0 });
        graphItems.push({ x: aMonth.due, y: Math.floor(aMonth.sumShares / 100), group: 1 });
      }


      let options = {
        legend: true,
        start: this.coinsInfo.firstIncomeDate.split(' ')[0],
        end: this.coinsInfo.lastIncomeDate.split(' ')[0]
      };

      let coinsGroups = new DataSet();
      coinsGroups.add({
        id: 0,
        content: 'My Incomes',
        options: {
          drawPoints: false
        }
      });
      coinsGroups.add({
        id: 1,
        content: 'Total Shares',
        options: {
          drawPoints: false
        }
      });

      let graphDataset = new DataSet(graphItems);
      delete (this.graph2dMyPAIs);
      this.graph2dMyPAIs = new Graph2d(myPAIsContainer, graphDataset, coinsGroups, options);

    }, 200);
  }

  public formatIt(inp) {
    return this.gUtils.microPAIToPAI(inp);
    // return this.gUtils.sepNum(inp);
  }

}
