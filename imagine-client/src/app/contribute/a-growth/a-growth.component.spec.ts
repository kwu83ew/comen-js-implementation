import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AGrowthComponent } from './a-growth.component';

describe('AGrowthComponent', () => {
  let component: AGrowthComponent;
  let fixture: ComponentFixture<AGrowthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AGrowthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AGrowthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
