import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordedProposalsComponent } from './recorded-proposals.component';

describe('RecordedProposalsComponent', () => {
  let component: RecordedProposalsComponent;
  let fixture: ComponentFixture<RecordedProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordedProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordedProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
