import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'recorded-proposals',
  templateUrl: './recorded-proposals.component.html',
  styleUrls: ['./recorded-proposals.component.css']
})
export class RecordedProposalsComponent implements OnInit {
  @Input('recordedProposals') recordedProposals;
  @Input('pdErrMsgLv') pdErrMsgLv;

  public voteValue: number;
  public voteComment: string;
  public voteOptions: any[];

  public pollChkProposal = true;
  public pollChkReqRelRes = true;
  public pollChkPricing = true;

  @Output() voteProposalParent = new EventEmitter<any>();
  @Output() reviewBallotAndCommentsParent = new EventEmitter<any>();
  @Output() refreshPollingStatusParent = new EventEmitter<any>();

  constructor(public gUtils: GUtils) { }

  ngOnInit() {
    this.voteValue = 0;
    this.voteComment = '';
    this.voteOptions = [];
    this.initVoteOptions();
  }

  public initVoteOptions() {
    let val = 100
    while (val > -101) {
      if (val > 0) {
        this.voteOptions.push([val, `${val}% Yes`]);
      } else if (val == 0) {
        this.voteOptions.push([val, `Abstain`]);
      } else {
        this.voteOptions.push([val, `${val}% No`]);
      }
      val = val - 10;
    }
  }


  voteProposal(pllHash, dTarget) {
    this.voteProposalParent.next({ pllHash, dTarget, vote: this.voteValue, voteComment: this.voteComment });
  }

  reviewBallotAndComments(pllHash) {
    this.reviewBallotAndCommentsParent.next({ pllHash });
  }

  public refreshPollingStatus(pllHash) {
    console.log('refreshPollingStatus', { pllHash });
    this.refreshPollingStatusParent.next({ pllHash });
  }

}
