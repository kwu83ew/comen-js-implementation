import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { ContributeService } from './services/contribute.service';
import { CookieService } from '../services/cookie.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Promise } from 'q';

@Component({
  selector: 'im-contribute',
  templateUrl: './contribute.component.html',
  styleUrls: ['./contribute.component.css']
})
export class ContributeComponent implements OnInit {

  public coinsInfoObj = {};
  public coinsInfoArr = [];

  years = [0, 1, 2, 3, 4, 5, 6];
  levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  hours = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 30, 50, 100, 200, 500, 1000, 10000];
  oHoursLevelGrowth = [1, 2, 3, 5, 10, 20, 50, 100, 200, 500, 1000, 1500, 2500, 5000, 10000];
  machineAccountInfo;
  pollingProfiles = [];
  pollingProfilesDict = [];
  pollPerformType: '';
  votesCountingMethod: '';
  pollingVersion: '';


  pdErrMsgLv = '';

  cAGrowth = 10;

  pdErrMsg1;
  mDPNotiMsg;
  pdTitle;
  pdDescription;
  pdTags;
  pdCotributerAccount;
  pdHelpLevel;
  pdHelpHours;
  pdPollingProfile;
  pdVotingLongevity;
  proposalCost;
  fileUrl;

  lrErrMsg;
  lrTitle;
  lrTags;
  lrDescription;
  lrPollingProfile;
  lrVotingLongevity;  //voting period in which community can up-vote (approval or YES) to proposal
  lrAccountAddress;  //pledger
  lrPledgee;  // who pays loan
  lrArbiter;
  lrRef;  // proposal hash
  lrHelpHours;
  lrHelpLevel;
  lrOneCycleIncome;
  lrPrincipal;  // loan amount
  lrAnnualInterest;
  lrRepaymentsNumber;
  lrRepaymentAmount;
  lrRepaymentDetails: any[];
  lrTotalRePaymentAmount;
  lrPledgeeEmails;

  public myDrafts = [];
  public sharesInfo = [];
  public sumShares: number;
  public holdersByKey = {};
  public holdersOrderByShares = [];

  incomeForPorp;
  definiteIncomes;
  reserveIncomes;
  treasuryIncomes;
  firstIncomeDate;
  lastIncomeDate;
  monthlyIncomes = [];

  recordedProposals = [];
  relResCoinsPollings = [];

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private contributeService: ContributeService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {

    this.coinsInfoObj = {};
    this.coinsInfoArr = [];

    this.pdTitle = '';
    this.pdDescription = '';
    this.pdTags = '';

    this.pdHelpLevel = 6;
    this.pdHelpHours = 1;
    this.pdVotingLongevity = this.gUtils.DEFAULT_VOTE_LONGIVITY;
    this.pdPollingProfile = null

    this.lrErrMsg = '';
    this.lrTitle = '';
    this.lrTags = '';
    this.lrDescription = '';
    this.lrVotingLongevity = this.gUtils.DEFAULT_VOTE_LONGIVITY;
    this.lrAccountAddress = '';
    this.lrPledgee = this.gUtils.HU_DNA_SHARE_ADDRESS;
    this.lrArbiter = '';
    this.lrRef = '';
    this.lrHelpHours = '';
    this.lrHelpHours = '';
    this.lrOneCycleIncome = 0;
    this.lrPrincipal = 0;
    this.lrAnnualInterest = 0.5;
    this.lrRepaymentsNumber = 0;
    this.lrRepaymentAmount = 0;
    this.lrRepaymentDetails = [];
    this.lrTotalRePaymentAmount = 0;
    this.lrPledgeeEmails = {};


    this.proposalCost = 0;
    this.incomeForPorp = 0;
    this.definiteIncomes = 0;
    this.reserveIncomes = 0;
    this.treasuryIncomes = 0;
    this.firstIncomeDate = '';
    this.lastIncomeDate = '';
    this.monthlyIncomes = [];


    this.getMachineAccountInfo();
    this.loadPollingProfiles();
    this.loadMyDraftProposals();
    this.getOnchainProposalsList();
    // this.getCoinsReleaseReq();
    this.getDNASharesInfo();
    // this.calcAllGrowthRates();
  }

  public loadPollingProfiles() {
    this.gUtils.loadPollingProfiles({}).subscribe(res => {
      this.pollingProfiles = res;
      // console.log('pollingProfiles pollingProfiles', this.pollingProfiles);
      for (let aPrf of this.pollingProfiles) {
        this.pollingProfilesDict[aPrf.ppName] = aPrf;
      }

    });
  }

  public getMachineAccountInfo() {
    this.gUtils.getMachineAccountInfo({}).subscribe(res => {
      // console.log('getMachineAccountInfo', res);
      this.machineAccountInfo = res;
      if (this.pdCotributerAccount == undefined)
        this.pdCotributerAccount = this.machineAccountInfo.backerAddress;
    });
  }

  public fillPollProfDtl(e) {
    this.pollPerformType = this.pollingProfilesDict[this.pdPollingProfile].pprPerformType;
    this.votesCountingMethod = this.pollingProfilesDict[this.pdPollingProfile].pprVotesCountingMethod;
    this.pollingVersion = this.pollingProfilesDict[this.pdPollingProfile].pprVersion;
  }

  public getOnchainProposalsList() {
    this.contributeService.getOnchainProposalsList({}).subscribe(res => {
      this.recordedProposals = res.records;
      // console.log('Onchain Proposals', this.recordedProposals);
    });
  }

  // public getCoinsReleaseReq() {
  //   this.contributeService.getCoinsReleaseReq({}).subscribe(res => {
  //     this.relResCoinsPollings = res.records;
  //     console.log('relResCoins Pollings', this.relResCoinsPollings);
  //   });
  // }

  public loadMyDraftProposals() {
    this.contributeService.loadMyDraftProposals({}).subscribe(res => {
      this.myDrafts = res;

      // setting emails
      for (let aRes of res) {
        if (aRes.type == 'pledgeReq')
          this.lrPledgeeEmails[aRes.id] = '';
      }
    });
  }

  public calcAllGrowthRates() {
    for (let cAGrowth of this.oHoursLevelGrowth) {
      this.predictFutureIncomes(cAGrowth);
    }
    setTimeout(() => {
      this.coinsInfoArr = [];
      let growths = this.gUtils.objKeys(this.coinsInfoObj).map(x => x.toString().padStart(7, '0')).sort().map(x => parseInt(x));
      // console.log(growths);
      for (let cAGrowth of growths) {
        this.coinsInfoArr.push(this.coinsInfoObj[cAGrowth]);
      }
      // console.log(this.coinsInfoArr);
    }, 3000);

  }

  public prepareDraftProposal() {
    this.pdErrMsg1 = '';

    if ((this.pdVotingLongevity == null) || (this.pdVotingLongevity == '')) {
      this.pdErrMsg1 = 'Invalid Voting Longevity';
      return;
    }
    if (this.pdVotingLongevity * 60 < this.gUtils.getCycleByMinutes() * 2) {
      this.pdErrMsg1 = 'Voting Longevity can not be less than 24 Hours';
      return;
    }
    if (this.pdPollingProfile == null) {
      this.pdErrMsg1 = 'You must select a Polling profile';
      return;
    }

    this.contributeService.prepareDraftProposal({
      pdTitle: this.pdTitle,
      pdDescription: this.pdDescription,
      pdTags: this.pdTags,
      pdVotingLongevity: this.pdVotingLongevity,
      pdPollingProfile: this.pdPollingProfile,
      pdCotributerAccount: this.pdCotributerAccount,
      pdHelpLevel: this.pdHelpLevel,
      pdHelpHours: this.pdHelpHours
    }).subscribe(res => {
      console.log(res);
      if (res.err != false) {
        this.pdErrMsg1 = res.msg;
        return;
      }
      this.proposalCost = (res.mintedDividend * 2 * 60) + 1000000;  // proposal cost will be equal of potentially earning in 60 days
      this.loadMyDraftProposals();
      this.emptyDarfForm();
    });
  }

  public deleteDraftProposal(pdId) {
    this.contributeService.deleteDraftProposal({ pdId }).subscribe(res => {
      this.loadMyDraftProposals();
    });

  }

  public emptyDarfForm() {
    this.pdTitle = '';
    this.pdDescription = '';
    this.pdTags = '';
    this.pdCotributerAccount = '';
    this.pdHelpLevel = 6;
    this.pdHelpHours = 1;
  }

  public predictFutureIncomes(cAGrowth = null) {
    if (cAGrowth == null)
      cAGrowth = this.cAGrowth;

    this.contributeService.predictFutureIncomes({
      pdHelpLevel: this.pdHelpLevel,
      pdHelpHours: this.pdHelpHours,
      cAGrowth
    }).subscribe(res => {
      // console.log(res);
      this.definiteIncomes = res.definiteIncomes;
      this.reserveIncomes = res.reserveIncomes;
      this.treasuryIncomes = res.treasuryIncomes;
      this.monthlyIncomes = res.monthlyIncomes;
      this.firstIncomeDate = res.firstIncomeDate;
      this.lastIncomeDate = res.lastIncomeDate;

      this.proposalCost = this.monthlyIncomes[0].incomePerMonth * this.gUtils.PROPOSAL_APPLY_COST_SCALE;  // proposal cost will be equal of potentially earning in 3 month
      res.pdHelpLevel = this.pdHelpLevel;
      res.pdHelpHours = this.pdHelpHours;
      res.proposalCost = this.proposalCost;
      res.cAGrowth = cAGrowth;
      this.coinsInfoObj[cAGrowth] = res;
    });
  }

  public signAndPayProposalCosts(args) {
    this.contributeService.signAndPayProposalCosts(args).subscribe(res => {
      if (res.msg)
        this.mDPNotiMsg = res.msg;
    });
  }

  public fillLoanRequestForm(pdId) {
    this.contributeService.fillLoanRequestForm({ pdId }).subscribe(res => {
      console.log(res);
      this.lrTitle = res.lrTitle;
      this.lrTags = res.lrTags;
      this.lrDescription = res.lrDescription;
      this.lrVotingLongevity = res.lrVotingLongevity;
      this.lrPollingProfile = res.pollingProfile;
      this.lrRef = res.lrRef;
      this.lrHelpHours = res.lrHelpHours;
      this.lrHelpLevel = res.lrHelpLevel;
      this.lrAccountAddress = res.lrAccountAddress;
      this.lrOneCycleIncome = res.lrOneCycleIncome;
      this.lrPrincipal = res.lrPrincipal;
      this.lrRepaymentsNumber = this.gUtils.DEFAULT_MAX_REPAYMENTS;
      this.lrRepaymentAmount = Math.floor(((res.lrPrincipal * (1 + (this.lrAnnualInterest * 2) / 100)) / (this.lrRepaymentsNumber - 1)) / 1000000);  // raughly returns loan in 100 repayments which is 50 days
      this.reCalcRepaymentInfo();
    });
  }

  public reCalcRepaymentInfo() {
    this.lrErrMsg = '';
    // console.log('reCalcRepaymentInfo', this.lrPrincipal);
    let repaymentAmount = Math.floor(this.lrRepaymentAmount * 1000000);  // the prices always is shown in PAIs, whereas in system are in micro PAIs
    if (repaymentAmount > this.lrOneCycleIncome) {
      this.lrErrMsg = `Repayment amount can not exceed cycle income!`;
      return;
    }
    this.gUtils.calcLoanRepayments({
      principal: this.lrPrincipal,
      annualInterest: this.lrAnnualInterest,
      repaymentAmount,
      repaymentSchedule: 365 * 2,
      callback: this.callback_reCalcRepaymentInfo.bind(this),
    });
  }

  public callback_reCalcRepaymentInfo() {
    let loanDtl = this.gUtils.calcLoanRepaymentsRes;
    if (loanDtl.err != false) {
      this.lrErrMsg = loanDtl.msg;
      return;
    }
    this.lrRepaymentDetails = loanDtl.repayments;
    this.lrTotalRePaymentAmount = loanDtl.totalRePaymentAmount;
    this.lrRepaymentsNumber = loanDtl.repaymentsNumber;
  }

  public bindProposalLoanPledge() {
    this.lrErrMsg = '';
    this.contributeService.bindProposalLoanPledge({
      lrPledgee: this.lrPledgee,
      lrArbiter: this.lrArbiter,
      lrRef: this.lrRef,
      lrAccountAddress: this.lrAccountAddress,
      lrPrincipal: this.lrPrincipal,
      lrAnnualInterest: this.lrAnnualInterest,
      lrRepaymentAmount: this.lrRepaymentAmount * 1000000, // the prices always is shown in PAIs, whereas in system are in micro PAIs
    }).subscribe(res => {
      console.log(res);
      if (res.err != false)
        this.lrErrMsg = res.msg;
      this.loadMyDraftProposals();
    });
  }

  public sendLoanRequest(dplId) {
    this.mDPNotiMsg = '';
    console.log(this.lrPledgeeEmails);
    this.contributeService.sendLoanRequest({ dplId, lrPledgeeEmail: this.lrPledgeeEmails[dplId] }).subscribe(res => {
      console.log(res);
      if (res.msg)
        this.mDPNotiMsg = res.msg;
    });
  }

  public sendLReqToAgora(dplId, costPayMode = 'Normal', dTarget) {
    this.mDPNotiMsg = '';
    console.log(this.lrPledgeeEmails);
    this.contributeService.sendLReqToAgora({ dplId, costPayMode, dTarget }).subscribe(res => {
      console.log(res);
      if (res.msg)
        this.mDPNotiMsg = res.msg;
    });
  }

  public deleteDraftPledgeReq(dplId) {
    console.log('deleteDraftPledgeReq', dplId);
    this.mDPNotiMsg = '';
    this.contributeService.deleteDraftPledgeReq({ dplId }).subscribe(res => {
      console.log(res);
      this.loadMyDraftProposals();
      if (res.msg)
        this.mDPNotiMsg = res.msg;
    });
  }

  public voteProposal(obj) {
    // console.log('voteProposal', obj);
    this.pdErrMsgLv = '';
    this.contributeService.voteProposal(obj).subscribe(res => {
      console.log(res);
      this.getOnchainProposalsList();
      if (res.msg)
        this.pdErrMsgLv = res.msg;
    });
  }
  
  public reviewBallotAndComments(obj) {
    // console.log('reviewBallotAndComments', obj);
    this.pdErrMsgLv = '';
    this.contributeService.reviewBallotAndComments(obj).subscribe(res => {
      console.log(res);

    });
  }

  public refreshPollingStatus(obj) {
    // console.log('refreshPollingStatus', obj);
    this.pdErrMsgLv = '';
    this.gUtils.refreshPollingStatus(obj).subscribe(res => {
      this.getOnchainProposalsList();
      if (res.msg)
        this.pdErrMsgLv = res.msg;
    });
  }

  public getDNASharesInfo() {
    // console.log('refreshPollingStatus');
    // this.mDPNotiMsg = '';
    this.gUtils.getDNASharesInfo({}).subscribe(res => {
      console.log('000', res);
      this.sumShares = res.sumShares;
      this.holdersByKey = res.holdersByKey;
      this.holdersOrderByShares = res.holdersOrderByShares;
    });
  }


}
