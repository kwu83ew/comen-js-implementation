import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'recorded-coin-release-pollings',
  templateUrl: './recorded-coin-release-pollings.component.html',
  styleUrls: ['./recorded-coin-release-pollings.component.css']
})
export class RecordedCoinReleasePollingsComponent implements OnInit {

  @Input('relResCoinsPollings') relResCoinsPollings;
  @Output() refreshPollingStatusParent = new EventEmitter<any>();

  public voteValue: number;
  public voteComment: string;
  

  constructor(
    public gUtils: GUtils
  ) { }

  ngOnInit() {
    this.voteValue = 0;
    this.voteComment = '';
    this.voteOptions = [];
    this.initVoteOptions();
  }

  public voteOptions: any[];
  public initVoteOptions() {
    let val = 100
    while (val > -101) {
      if (val > 0) {
        this.voteOptions.push([val, `${val}% Yes`]);
      } else if (val == 0) {
        this.voteOptions.push([val, `Abstain`]);
      } else {
        this.voteOptions.push([val, `${val}% No`]);
      }
      val = val - 10;
    }
  }


  public refreshPollingStatus(pllHash) {
    console.log('refreshPollingStatus', { pllHash });
    this.refreshPollingStatusParent.next({ pllHash });
  }

  public voteReleaseReseve(pllHash, dTarget) {
    this.gUtils.voteReleaseReseve({
      pllHash, 
      dTarget, 
      vote: this.voteValue, 
      voteComment: this.voteComment,
      callback: this.callback_voteReleaseReseve.bind(this)
    });
  }

  public callback_voteReleaseReseve() {
    console.log('get MyOnchainFiles', this.gUtils.dummyCallbackRes['voteReleaseReseve']);
    
  }

}
