import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordedCoinReleasePollingsComponent } from './recorded-coin-release-pollings.component';

describe('RecordedCoinReleasePollingsComponent', () => {
  let component: RecordedCoinReleasePollingsComponent;
  let fixture: ComponentFixture<RecordedCoinReleasePollingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordedCoinReleasePollingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordedCoinReleasePollingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
