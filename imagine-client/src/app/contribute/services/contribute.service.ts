import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AppError } from '../../errors/app-error';



@Injectable({
  providedIn: 'root'
})
export class ContributeService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }
  
  public predictFutureIncomes(args) {
    return this.http.put(this.url + '/api/contribute/predictFutureIncomes', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public fillLoanRequestForm(args) {
    return this.http.put(this.url + '/api/contribute/fillLoanRequestForm', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  
  public signAndPayProposalCosts(args) {
    return this.http.put(this.url + '/api/contribute/signAndPayProposalCosts', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public bindProposalLoanPledge(args) {
    return this.http.put(this.url + '/api/contribute/bindProposalLoanPledge', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public sendLoanRequest(args) {
    return this.http.put(this.url + '/api/contribute/sendLoanRequest', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  

  public sendLReqToAgora(args) {
    return this.http.put(this.url + '/api/contribute/sendLReqToAgora', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public deleteDraftPledgeReq(args) {
    return this.http.put(this.url + '/api/contribute/deleteDraftPledgeReq', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public voteProposal(args) {
    return this.http.put(this.url + '/api/contribute/voteProposal', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public reviewBallotAndComments(args) {
    return this.http.put(this.url + '/api/contribute/reviewBallotAndComments', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public prepareDraftProposal(args) {
    return this.http.put(this.url + '/api/contribute/prepareDraftProposal', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public deleteDraftProposal(args) {
    return this.http.put(this.url + '/api/contribute/deleteDraftProposal', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadMyDraftProposals(args) {
    return this.http.put(this.url + '/api/contribute/loadMyDraftProposals', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public loadPollingProfiles(args) {
    return this.http.put(this.url + '/api/contribute/loadPollingProfiles', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getOnchainProposalsList(args) {
    return this.http.put(this.url + '/api/contribute/getOnchainProposalsList', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getCoinsReleaseReq(args) {
    return this.http.put(this.url + '/api/misc/getCoinsReleaseReq', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


}
