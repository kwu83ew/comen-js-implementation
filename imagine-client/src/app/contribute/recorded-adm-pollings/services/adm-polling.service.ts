import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';



@Injectable({
  providedIn: 'root'
})
export class AdmPollingService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public getOnchainAdmPollings(args) {
    return this.http.put(this.url + '/api/adm-pollings/getOnchainAdmPollings', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public voteAdmPolling(args) {
    return this.http.put(this.url + '/api/adm-pollings/voteAdmPolling', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

}
