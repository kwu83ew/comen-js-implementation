import { TestBed } from '@angular/core/testing';

import { AdmPollingService } from './adm-polling.service';

describe('AdmPollingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmPollingService = TestBed.get(AdmPollingService);
    expect(service).toBeTruthy();
  });
});
