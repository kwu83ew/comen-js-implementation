import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { AdmPollingService } from './services/adm-polling.service';

@Component({
  selector: 'recorded-adm-pollings',
  templateUrl: './recorded-adm-pollings.component.html',
  styleUrls: ['./recorded-adm-pollings.component.css']
})
export class RecordedAdmPollingsComponent implements OnInit {

  constructor(
    public gUtils: GUtils,
    private admPollingService: AdmPollingService,
  ) { }

  ngOnInit() {
    this.getOnchainAdmPollings();
    this.voteValue = 0;
    this.initVoteOptions();
  }

  public voteOptions: any[];
  public voteValue = 0;
  public initVoteOptions() {
    this.voteOptions = [];
    let val = 100
    while (val > -101) {
      if (val > 0) {
        this.voteOptions.push([val, `${val}% Yes`]);
      } else if (val == 0) {
        this.voteOptions.push([val, `Abstain`]);
      } else {
        this.voteOptions.push([val, `${val}% No`]);
      }
      val = val - 10;
    }
  }

  public onchainAdmPollings = [];
  public getOnchainAdmPollings() {
    this.admPollingService.getOnchainAdmPollings({}).subscribe(res => {
      this.onchainAdmPollings = res.records;
      console.log('relResCoins Pollings', this.onchainAdmPollings);

      // prepare presentation
      for (let aRec of this.onchainAdmPollings) {
        switch (aRec.apr_subject) {

          case 'RFRfMinS2Wk':
            aRec.pollingSubject = `Refine Minimum shares needed for "Wiki Activities" to ${aRec.apr_values.share} % of all network shares`
            // share: 0.05555, longevity: 0.99
            break;

          case 'RFRfMinS2DA':
            aRec.pollingSubject = `Refine Minimum shares needed for "Discussing in Agoras" to ${aRec.apr_values.share} % of all network shares`
            // share: 0.05555, longevity: 0.99
            break;

          case 'RFRfMinS2V':
            aRec.pollingSubject = `Refine Minimum shares needed for "Voting" to ${aRec.apr_values.share} % of all network shares`
            // share: 0.05555, longevity: 0.99
            break;

          case 'RFRfMinFSign':
            aRec.pollingSubject = `Refine Minimum shares needed for "Sgining Coinbase Block" to ${aRec.apr_values.share} % of all network shares`
            // share: 0.05555, longevity: 0.99
            break;

          case 'RFRfBasePrice':
            aRec.pollingSubject = `Refine Base price to ${aRec.apr_values.pFee} micro PAI per Character`
            // share: 0.05555, longevity: 0.99
            break;

        }

      }

    });
  }

  public notifyCntnr = '';
  public refreshPollingStatus(pllHash) {
    // console.log('refreshPollingStatus', obj);
    this.notifyCntnr = '';
    this.gUtils.refreshPollingStatus({ pllHash }).subscribe(res => {
      this.getOnchainAdmPollings();
      if (res.msg)
        this.notifyCntnr = res.msg;
    });
  }

  public voteComment = '';
  voteAdmPolling(pllHash, dTarget) {
    // console.log('voteAdmPolling', obj);
    this.notifyCntnr = '';
    this.admPollingService.voteAdmPolling({ pllHash, dTarget, vote: this.voteValue, voteComment: this.voteComment }).subscribe(res => {
      console.log(res);
      this.getOnchainAdmPollings();
      if (res.msg)
        this.notifyCntnr = res.msg;
    });
  }
}
