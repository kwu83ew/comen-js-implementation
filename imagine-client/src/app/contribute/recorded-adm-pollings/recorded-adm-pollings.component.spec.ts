import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordedAdmPollingsComponent } from './recorded-adm-pollings.component';

describe('RecordedAdmPollingsComponent', () => {
  let component: RecordedAdmPollingsComponent;
  let fixture: ComponentFixture<RecordedAdmPollingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordedAdmPollingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordedAdmPollingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
