import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatcherDetailedComponent } from './watcher-detailed.component';

describe('WatcherDetailedComponent', () => {
  let component: WatcherDetailedComponent;
  let fixture: ComponentFixture<WatcherDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatcherDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatcherDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
