import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AppError } from '../../errors/app-error';


@Injectable({
  providedIn: 'root'
})
export class WalletService {
  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public createNewAddress(args) {
    return this.http.put(this.url + '/api/wallet/new-address', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getWalletAddresses() {
    return this.http.get(this.url + '/api/wallet/getWalletAddresses').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getWalletBalance() {
    return this.http.get(this.url + '/api/wallet/getWalletBalance').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public restorUnUsedUTXOs() {
    return this.http.get(this.url + '/api/wallet/restorUnUsedUTXOs').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public sendPai(args) {
    console.log(args);
    return this.http.put(this.url + '/api/wallet/sendPai', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getWalletBufferedTrxs() {
    return this.http.get(this.url + '/api/wallet/buffered').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public pushTransactionsToNetwork() {
    return this.http.get(this.url + '/api/wallet/push-trx').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }



  getTrxList() {
    console.log('getTrxList');
    return this.http.get(this.url + '/api/wallet/trxList').pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  getIsActive() {
    return true;
  }



}
