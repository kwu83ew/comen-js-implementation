import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { WalletService } from '../services/wallet.service';

@Component({
  selector: 'imw-addresses',
  templateUrl: './imw-addresses.component.html',
  styleUrls: ['./imw-addresses.component.css']
})
export class ImwAddressesComponent implements OnInit {
  public addressesChild: any[] = [];
  public totalMatSum: number = 0;
  public totalUnmatSum: number = 0;
  public totalMatCount: number = 0;
  public totalUnmatCount: number = 0;
  public customBechUnlocker;

  public signatureModes: Array<{ disabled: boolean, value: string }> = [
    { disabled: false, value: "1 of 1" },
    { disabled: false, value: "1 of 2" },
    { disabled: false, value: "2 of 2" },
    { disabled: true, value: "1 of 3" },
    { disabled: false, value: "2 of 3" },
    { disabled: false, value: "3 of 3" },
    { disabled: true, value: "1 of 4" },
    { disabled: true, value: "2 of 4" },
    { disabled: false, value: "3 of 4" },
    { disabled: false, value: "4 of 4" },
    { disabled: true, value: "1 of 5" },
    { disabled: true, value: "2 of 5" },
    { disabled: false, value: "3 of 5" },
    { disabled: false, value: "4 of 5" },
    { disabled: false, value: "5 of 5" },
    { disabled: true, value: "1 of 6" },
    { disabled: true, value: "2 of 6" },
    { disabled: true, value: "3 of 6" },
    { disabled: true, value: "4 of 6" },
    { disabled: false, value: "5 of 6" },
    { disabled: false, value: "6 of 6" },
    { disabled: true, value: "1 of 7" },
    { disabled: true, value: "2 of 7" },
    { disabled: true, value: "3 of 7" },
    { disabled: false, value: "4 of 7" },
    { disabled: false, value: "5 of 7" },
    { disabled: false, value: "6 of 7" },
    { disabled: false, value: "7 of 7" }
  ];
  public neededSignCounts: number;
  public totalSignCounts: any[];

  @Input()
  set addresses(addresses: any[]) {
    this.addressesChild = addresses || [];

    this.addressesChild.forEach(elm => {
      let tmpMat = elm.matSum;
      let tmpUnMat = elm.unmatSum;
      let tmpMatCount = elm.matCount;
      let tmpUnMatCount = elm.unmatCount;
      if (this.gUtils.isNanEmptyNull(tmpMat)) {
        tmpMat = 0;
      } else {
        tmpMat = parseInt(tmpMat);
      }
      if (this.gUtils.isNanEmptyNull(tmpUnMat)) {
        tmpUnMat = 0;
      } else {
        tmpUnMat = parseInt(tmpUnMat);
      }

      if (this.gUtils.isNanEmptyNull(tmpMatCount)) {
        tmpMatCount = 0;
      } else {
        tmpMatCount = parseInt(tmpMatCount);
      }
      if (this.gUtils.isNanEmptyNull(tmpUnMatCount)) {
        tmpUnMatCount = 0;
      } else {
        tmpUnMatCount = parseInt(tmpUnMatCount);
      }

      this.totalMatSum += tmpMat;
      this.totalUnmatSum += tmpUnMat;

      this.totalMatCount += tmpMatCount;
      this.totalUnmatCount += tmpUnMatCount;

    });
    // console.log(`matSum : ${this.matSum}`);
    // console.log(`unmatSum : ${this.unmatSum}`);
  }
  @Output() refreshAddressesParent = new EventEmitter<any[]>();

  constructor(public gUtils: GUtils, public walletService: WalletService) { }

  ngOnInit() {
    this.neededSignCounts = 1;
    this.totalSignCounts = [['', 0]];
  }

  createNewAddress(signatureType, signatureMod) {
    this.walletService.createNewAddress({
      signatureType,
      signatureMod,
      customBechUnlocker: this.customBechUnlocker
    }).subscribe(newAdd => {
      console.log(`createNewAddress ${signatureType}:${signatureMod}`);
      console.log(newAdd);
      this.refreshAddressesParent.next();
    });
  }

  setSignType(signType) {
    console.log('signType', signType);
    signType = signType.value.split(' of ');
    console.log('signType', signType);
    this.neededSignCounts = parseInt(signType[0]);
    this.totalSignCounts = [];
    for (let inx = 0; inx < parseInt(signType[1]); inx++) {
      this.totalSignCounts.push({
        name: `person ${inx + 1}`, redeemTime: 0, pubKey: ''
      });

    }
  }
}
