import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwAddressesComponent } from './imw-addresses.component';

describe('ImwAddressesComponent', () => {
  let component: ImwAddressesComponent;
  let fixture: ComponentFixture<ImwAddressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwAddressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
