import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwBalanceComponent } from './imw-balance.component';

describe('ImwBalanceComponent', () => {
  let component: ImwBalanceComponent;
  let fixture: ComponentFixture<ImwBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
