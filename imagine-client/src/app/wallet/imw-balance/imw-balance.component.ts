import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import * as _ from 'lodash';
import { WalletService } from '../services/wallet.service';
import { Injectable } from '@angular/core';
import { GUtils } from '../../services/utils';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'imw-balance',
  templateUrl: './imw-balance.component.html',
  styleUrls: ['./imw-balance.component.css']
})
export class ImwBalanceComponent implements OnInit {
  public balanceChild: any[] = [];
  public selectedToSpend: any[] = [];

  @Input()
  set balance(balance: any[]) {
    this.balanceChild = balance || [];
  }
  @Output() utxoSelectionParent = new EventEmitter<any[]>();

  // @Output('selectedToSpend any[]') selectedToSpend;
  // @Output() selectedUTXOs = new EventEmitter<any[]>();

  constructor(public gUtils: GUtils, public walletService: WalletService) {
    // this.selectedToSpend = selectedToSpend;
  }

  ngOnInit() {
  }


  deselectAll() {
    this.selectedToSpend = [];
  }

  onUtxoSelect(i) {
    if (this.selectedToSpend === undefined) {
      this.selectedToSpend = [];
    }
    if (!this.selectedToSpend.includes(i)) {
      this.selectedToSpend.push(i);
    } else {
      this.selectedToSpend.splice(this.selectedToSpend.indexOf(i), 1);
    }
    // console.log('onUtxoSelect');
    // console.log(this.selectedToSpend);
    this.utxoSelectionParent.next(this.selectedToSpend);
  }

  // public getSelectedUtxos() {
  //   return this.selectedToSpend;
  // }

  get selectedUTXOsToSpend(): any[] { return this.selectedToSpend; }



  public sepNum(v) {
    return this.gUtils.microPAIToPAI(v);
  }
}
