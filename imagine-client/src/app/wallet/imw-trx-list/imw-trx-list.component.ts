import { Component, OnInit } from '@angular/core';
import { WalletService } from '../services/wallet.service';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'imw-trx-list',
  templateUrl: './imw-trx-list.component.html',
  styleUrls: ['./imw-trx-list.component.css']
})
export class ImwTrxListComponent implements OnInit {
  trxList: [];

  constructor(public gUtils: GUtils, public walletService: WalletService) {
    // this.selectedToSpend = selectedToSpend;
  }

  ngOnInit() {
      this.refreshTrxList();
  }

  
  public refreshTrxList() {
    // console.log(this.imwBalanceComponent.selectedUTXOsToSpend);
    this.walletService.getTrxList().subscribe(trxList => {
      this.trxList = this.processTrxList(trxList);
    });
  }

  public processTrxList(trxList) {
    return trxList;
  }
}
