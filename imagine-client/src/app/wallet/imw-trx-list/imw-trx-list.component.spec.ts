import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwTrxListComponent } from './imw-trx-list.component';

describe('ImwTrxListComponent', () => {
  let component: ImwTrxListComponent;
  let fixture: ComponentFixture<ImwTrxListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwTrxListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwTrxListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
