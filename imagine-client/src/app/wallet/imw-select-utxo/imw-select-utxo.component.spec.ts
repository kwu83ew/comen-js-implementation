import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwSelectUtxoComponent } from './imw-select-utxo.component';

describe('ImwSelectUtxoComponent', () => {
  let component: ImwSelectUtxoComponent;
  let fixture: ComponentFixture<ImwSelectUtxoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwSelectUtxoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwSelectUtxoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
