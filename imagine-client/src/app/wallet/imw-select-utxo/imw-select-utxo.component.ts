import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'imw-select-utxo',
  templateUrl: './imw-select-utxo.component.html',
  styleUrls: ['./imw-select-utxo.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ImwSelectUtxoComponent implements OnInit {
  @Input('is-spendable') isSpendable: Boolean;
  @Input('is-selected') isSelected: Boolean;
  @Input('value-by-pai') valByPai: number;
  @Output('utxo-sel-deselcted') change = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  getUTXOImg(){
    if (this.isSelected) {
      return '../../assets/bookmarkB.png';
    }else{
      return '../../assets/bookmarkW.png';
    }

  }

  onSelectUTXO(){
    this.isSelected = !this.isSelected;
    this.change.emit(this);
  }
}
