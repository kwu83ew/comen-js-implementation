import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[imwAddressFormat]'
})
export class AddressFormatDirective {

  constructor(private el: ElementRef) {

  }

  @HostListener('blur') didBlur(){
    this.validateAddress();
  }
  @HostListener('focus') didFocus(){
    this.validateAddress();
  }
  @HostListener('change') didChange(){
    this.validateAddress();
  }
  @HostListener('keyup') didKeyUp(){
    this.validateAddress();
  }

  private validateAddress(){
    console.log('bluuuuuuuuuuuuuuuuurrrrrr'+ this.el.nativeElement.value);
  }
}
