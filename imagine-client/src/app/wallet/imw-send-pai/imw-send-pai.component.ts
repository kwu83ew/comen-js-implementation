import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'imw-send-pai',
  templateUrl: './imw-send-pai.component.html',
  styleUrls: ['./imw-send-pai.component.css']
})
export class ImwSendPaiComponent implements OnInit {
  @Input('receiver') receiver;
  @Input('sendPaiErr') sendPaiErr;
  @Input('selectedToSpend') selectedToSpend;
  // @Output('receiver') ;

  @Output() reCalcPaymentParent = new EventEmitter<any[]>();
  @Output() reCalcOutputNumbersParent = new EventEmitter<any[]>();
  @Output() sendPaiParent = new EventEmitter();

  spReceiver = 'im1...';
  spChangeBack = '';
  spFunds = 0;
  spFundsDsp = '0';
  spOutputUnit = 0;
  unitNumbers = 1;
  spPais = 0;
  spDPCost = 0;
  changeBack = 0;

  constructor(public gUtils: GUtils) { }

  ngOnInit() {
  }

  onKeyUpp(recAddress=null) {
    // console.log(`receiver address : ${this.receiver} `);
  }

  openDialog() {

  }

  sendPai() {
    this.sendPaiParent.next();
  }

  reCalcPayment() {
    this.reCalcPaymentParent.next();
  }

  reCalcOutputNumbers() {
    this.reCalcOutputNumbersParent.next();
  }


}
