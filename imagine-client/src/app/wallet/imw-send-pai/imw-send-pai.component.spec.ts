import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwSendPaiComponent } from './imw-send-pai.component';

describe('ImwSendPaiComponent', () => {
  let component: ImwSendPaiComponent;
  let fixture: ComponentFixture<ImwSendPaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwSendPaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwSendPaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
