import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { WalletService } from '../services/wallet.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'imw-trx-buffer',
  templateUrl: './imw-trx-buffer.component.html',
  styleUrls: ['./imw-trx-buffer.component.css']
})
export class ImwTrxBufferComponent implements OnInit {
  public bufferedChild: any[] = [];

  @Input()
  set buffered(buffered: any[]) {
    this.bufferedChild = buffered || [];
  }
  @Input('sendBlockErr') sendBlockErr;
  @Output() refreshBufferedParent = new EventEmitter<any[]>();
  @Output() pushTransactionsToNetworkParent = new EventEmitter<any[]>();

  constructor(public gUtils: GUtils, public walletService: WalletService) { }

  ngOnInit() {
  }

  refreshBuffered() {
    this.refreshBufferedParent.next();
  }

  pushTransactionsToNetwork() {
    this.pushTransactionsToNetworkParent.next();
  }

}
