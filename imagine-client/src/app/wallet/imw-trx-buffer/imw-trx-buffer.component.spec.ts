import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImwTrxBufferComponent } from './imw-trx-buffer.component';

describe('ImwTrxBufferComponent', () => {
  let component: ImwTrxBufferComponent;
  let fixture: ComponentFixture<ImwTrxBufferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImwTrxBufferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImwTrxBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
