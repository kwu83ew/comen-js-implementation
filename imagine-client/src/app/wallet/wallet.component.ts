import { AfterViewInit, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { WalletService } from './services/wallet.service';
import { GUtils } from '../services/utils';
import * as _ from 'lodash';
import { ImwBalanceComponent } from './imw-balance/imw-balance.component';
import { ImwSendPaiComponent } from './imw-send-pai/imw-send-pai.component';
import { formatDate } from '@angular/common';


@Component({
  selector: 'im-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  title = 'Wallet';
  public selectedToSpend: any[] = [];
  public addresses: any[] = [];
  public balance: any[] = [];
  public buffered: any[] = [];
  public sendPaiErr = '';
  public sendBlockErr = '';
  isActive;

  public wNotiMsg2 = '';
  public walletNoti1 = '';
  public receiver = '';

  today = new Date();
  jstoday = '';

  @ViewChild(ImwBalanceComponent, { static: false })
  private imwBalanceComponent: ImwBalanceComponent;

  @ViewChild(ImwSendPaiComponent, { static: false })
  private imwSendPaiComponent: ImwSendPaiComponent;


  constructor(
    public gUtils: GUtils,
    public walletService: WalletService,
  ) {
    this.isActive = walletService.getIsActive();
    this.jstoday = formatDate(this.today, 'HH:mm:ss', 'en-US', '+0000');
  }

  ngOnInit() {
    this.refreshAddresses();
    this.getWalletBalance();
    this.refreshBuffered();
  }





  public refreshAddresses() {
    this.walletService.getWalletAddresses().subscribe(addresses => {
      console.log('addresses', addresses);
      this.addresses = this.processAddresses(addresses);
    });
  }

  public refreshBuffered() {
    this.walletService.getWalletBufferedTrxs().subscribe(buffered => {
      this.buffered = this.processBuffered(buffered);
    });
  }

  public pushTransactionsToNetwork() {
    this.walletService.pushTransactionsToNetwork().subscribe(res => {
      this.refreshBuffered();
      console.log(res);
      this.sendBlockErr = res.msg;
      if (res.err === true) {
        // change color to red
      } else {
        // change color to green
      }
    });
  }

  public getWalletBalance() {
    // console.log(this.imwBalanceComponent.selectedUTXOsToSpend);
    // this.imwBalanceComponent.deselectAll();
    // this.imwSendPaiComponent.spFunds = 0;
    this.walletService.getWalletBalance().subscribe(balance => {
      console.log('balance', balance);
      this.balance = this.processBalance(balance);
    });
  }

  public restorUnUsedUTXOs() {
    // console.log(this.imwBalanceComponent.selectedUTXOsToSpend);
    // this.imwBalanceComponent.deselectAll();
    // this.imwSendPaiComponent.spFunds = 0;
    this.walletService.restorUnUsedUTXOs().subscribe(balance => {
    });
  }


  public processAddresses(addresses) {
    addresses.forEach(anAddr => {
      // let detail = JSON.parse(anAddr.detail);
      // console.log(detail);
    });
    return addresses;
  }

  public processBuffered(buffered) {
    // TODO: present more info (e.g inputs/outputs address...)
    buffered.forEach(anBuffer => {
      anBuffer['pureBackerfee'] = Math.floor(anBuffer.dp_cost * 70 / 100);
      anBuffer['imagineFee'] = anBuffer.dp_cost - anBuffer['pureBackerfee'];
      let trxOutsHint = []
      anBuffer.outputs.forEach(out => {
        trxOutsHint.push(`${this.gUtils.hash16c(out[0])}: ${this.gUtils.microPAIToPAI(out[1])}`)
      });
      anBuffer['outsHint'] = trxOutsHint.join('\n');
      // let detail = JSON.parse(anBuffer.payload);
      // console.log(detail);
    });
    return buffered;
  }

  public processBalance(bl) {
    // console.log(this.gUtils.microPAIToPAI(balance[0]));
    const nowT = this.gUtils.getNow1();
    console.log('nowT', nowT);

    bl.forEach(aBal => {
      // aBal.o_value = this.gUtils.microPAIToPAI(aBal.o_value);
      aBal.isSpendable = (aBal.wfMatureDate <= nowT) ? true : false;
      // console.log(`${aBal.wfMatureDate} > ${nowT}`);
      // console.log(`${aBal.isSpendable}`);
    });
    return bl;
  }

  public utxoSelection(slctd) {
    this.selectedToSpend = slctd;
    let fund = 0;
    this.selectedToSpend.forEach(i => {
      fund += this.balance[i].wfOValue;
    });
    this.imwSendPaiComponent.spFunds = fund;
    this.imwSendPaiComponent.spFundsDsp = this.gUtils.microPAIToPAI(fund);
    this.reCalcPayment();
  }

  onSend(e) {
    e.stopPropagation();
    console.log(e);
  }

  reCalcOutputNumbers() {
    let spOutputUnit = this.gUtils.castToInt(this.imwSendPaiComponent.spOutputUnit);
    console.log('spOutputUnit', spOutputUnit);
    this.imwSendPaiComponent.unitNumbers = this.imwSendPaiComponent.spPais / this.imwSendPaiComponent.spOutputUnit;

  }

  reCalcPayment() {
    let changeBack = this.gUtils.castToInt(this.imwSendPaiComponent.spFunds);
    changeBack -= this.gUtils.castToInt(this.imwSendPaiComponent.spPais) * 1000000;
    changeBack -= this.gUtils.castToInt(this.imwSendPaiComponent.spDPCost) * 1000000;
    this.imwSendPaiComponent.changeBack = changeBack;
    this.reCalcOutputNumbers();
  }

  sendPai() {
    if (this.selectedToSpend.length === 0) {
      alert('Select at least one UTXO to spend it');
      return;
    }
    if (this.gUtils.castToInt(this.imwSendPaiComponent.spPais) === 0) {
      alert('Type how many Pai wana send!');
      return;
    }
    if (this.imwSendPaiComponent.spReceiver === 'im1...') {
      alert('Type receiver address!');
      return;
    }
    if (this.gUtils.castToInt(this.imwSendPaiComponent.spDPCost) === 0) {
      alert('Insert the transaction fee!');
      return;
    }

    const selectedUTXOs = [];
    this.selectedToSpend.forEach(i => {
      selectedUTXOs.push({
        address: this.balance[i].wfAddress,
        trx_hash: this.balance[i].wfTrxHash,
        o_index: this.balance[i].wfOIndex
      });
    });


    this.walletService.sendPai({
      spUTXOs: selectedUTXOs,
      spReceiver: this.imwSendPaiComponent.spReceiver,
      spChangeBack: this.imwSendPaiComponent.spChangeBack,
      spPais: this.gUtils.castToInt(this.imwSendPaiComponent.spPais),
      spDPCost: this.gUtils.castToInt(this.imwSendPaiComponent.spDPCost),
      spOutputUnit: this.gUtils.castToInt(this.imwSendPaiComponent.spOutputUnit)
    }).subscribe(res => {
      this.refreshBuffered();
      console.log(res);
      if (res.err === true) {
        this.sendPaiErr = res.msg;
      } else {
        this.sendPaiErr = '';
      }
    });
  }


}
