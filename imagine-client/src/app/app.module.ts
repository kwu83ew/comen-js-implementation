import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WalletComponent } from './wallet/wallet.component';
import { ImwBalanceComponent } from './wallet/imw-balance/imw-balance.component';
import { ImwSelectUtxoComponent } from './wallet/imw-select-utxo/imw-select-utxo.component';
import { ImwSendPaiComponent } from './wallet/imw-send-pai/imw-send-pai.component';
import { ImwTrxListComponent } from './wallet/imw-trx-list/imw-trx-list.component';
import { DemosComponent } from './demos/demos.component';
import { AddressFormatDirective } from './wallet/directives/address-format.directive';
import { SettingFormComponent } from './setting-form/setting-form.component';
import { HttpClientModule } from "@angular/common/http";
import { MatComponentsModule } from "./modules/mat-components.module";
import { InameModule } from "./modules/iname/iname.module";
import { NeighborNodesComponent } from './setting-form/neighbor-nodes/neighbor-nodes.component';
// import { MachineSettingComponent } from './setting-form/machine-setting/machine-setting.component';
import { ContributeComponent } from './contribute/contribute.component';
import { WatcherComponent } from './watcher/watcher.component';
import { DagComponent } from './watcher/dag/dag.component';
import { HomeComponent } from './home/home.component';
import { ImwAddressesComponent } from './wallet/imw-addresses/imw-addresses.component';
import { ImwTrxBufferComponent } from './wallet/imw-trx-buffer/imw-trx-buffer.component';
import { ANodeComponent } from './watcher/a-node/a-node.component';
import { WatcherDetailedComponent } from './watcher-detailed/watcher-detailed.component';
import { MiscComponent } from './misc/misc.component';
import { PluginCompComponent } from './plugin-comp/plugin-comp.component';
import { AGrowthComponent } from './contribute/a-growth/a-growth.component';
import { ContractsComponent } from './contracts/contracts.component';
import { RecordedAdmPollingsComponent } from './contribute/recorded-adm-pollings/recorded-adm-pollings.component';
import { RecordedProposalsComponent } from './contribute/recorded-proposals/recorded-proposals.component';
import { InameComponent } from './iname/iname.component';
import { RouterModule } from '@angular/router';
import { AnInameComponent } from './iname/an-iname/an-iname.component';
import { EmailSettingsComponent } from './setting-form/email-settings/email-settings.component';
import { MonitorComponent } from './monitor/monitor.component';
import { MessengerComponent } from './messenger/messenger.component';
import { InnerMessengerComponent } from './messenger/inner-messenger/inner-messenger.component';
import { CustomPostComponent } from './iname/custom-post/custom-post.component';
import { AboutComponent } from './about/about.component';
import { AgoraComponent } from './demos/agora/agora.component';
import { BuildAgoraFormComponent } from './demos/build-agora-form/build-agora-form.component';
import { SendPostComponent } from './demos/send-post/send-post.component';
import { WikiComponent } from './wiki/wiki.component';
import { SideComponent } from './wiki/side/side.component';
import { BodyComponent } from './wiki/body/body.component';
import { FormCreatePageComponent } from './wiki/form-create-page/form-create-page.component';
import { GraphComponent } from './monitor/graph/graph.component';
import { AdministratorPollingsComponent } from './misc/administrator-pollings/administrator-pollings.component';
import { RecordedCoinReleasePollingsComponent } from './contribute/recorded-coin-release-pollings/recorded-coin-release-pollings.component';
import { BlockPresenterComponent } from './monitor/block-presenter/block-presenter.component';
import { TransactionsObserverComponent } from './monitor/transactions-observer/transactions-observer.component';
import { CoinTraceComponent } from './monitor/coin-trace/coin-trace.component';

@NgModule({
  declarations: [
    AppComponent,
    WalletComponent,
    ImwBalanceComponent,
    ImwSelectUtxoComponent,
    ImwSendPaiComponent,
    ImwTrxListComponent,
    DemosComponent,
    AddressFormatDirective,
    SettingFormComponent,
    NeighborNodesComponent,
    // MachineSettingComponent,
    ContributeComponent,
    WatcherComponent,
    DagComponent,
    ImwAddressesComponent,
    ImwTrxBufferComponent,
    ANodeComponent,
    WatcherDetailedComponent,
    MiscComponent,
    PluginCompComponent,
    AGrowthComponent,
    ContractsComponent,
    RecordedProposalsComponent,
    InameComponent,
    AnInameComponent,
    EmailSettingsComponent,
    MonitorComponent,
    MessengerComponent,
    InnerMessengerComponent,
    CustomPostComponent,
    AboutComponent,
    HomeComponent,
    AgoraComponent,
    BuildAgoraFormComponent,
    SendPostComponent,
    WikiComponent,
    SideComponent,
    BodyComponent,
    FormCreatePageComponent,
    GraphComponent,
    AdministratorPollingsComponent,
    RecordedCoinReleasePollingsComponent,
    RecordedAdmPollingsComponent,
    BlockPresenterComponent,
    TransactionsObserverComponent,
    CoinTraceComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatComponentsModule,
    InameModule,
    RouterModule.forRoot([
      { path: '**', component: AppComponent }
    ]),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
