import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { MessengerService } from './services/messenger.service';

@Component({
  selector: 'im-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.css']
})
export class MessengerComponent implements OnInit {

  public recordedINames;
  public bindingDict;
  public iPGPHandle = '';
  public iPGPHandleMap = {};
  public iNamesMap = {};
  public iPGPHandleShort = '';

  constructor(
    public gUtils: GUtils,
    private msgService: MessengerService,
  ) { }

  ngOnInit() {
    this.openMsgConsole(this.gUtils.dummyURLParser().segmetns[0]);

    this.loadRecordedINames();
  }

  public loadRecordedINames() {
    this.gUtils.loadRecordedINames({
      callback: this.callback_loadRecordedINames.bind(this)
    });
  }
  public callback_loadRecordedINames() {
    let res = this.gUtils.loadRecordedINamesRes;
    this.recordedINames = res.records;
    this.bindingDict = res.bindingDict;
    this.makeBindedDicts();
  }

  public openMsgConsole(iPGPHandle) {
    this.iPGPHandle = iPGPHandle;
    this.iPGPHandleShort = this.gUtils.hash6c(iPGPHandle);
    this.gUtils.setCookie('iPGPHandle', iPGPHandle);
  }

  public makeBindedDicts() {
    for (let aRecord of this.recordedINames) {
      this.iNamesMap[aRecord['inHash']] = aRecord;
    }

    for (let anINameHash of this.gUtils.objKeys(this.bindingDict)) {
      for (let anPGP of this.bindingDict[anINameHash]['IPGP']) {
        this.iPGPHandleMap[anPGP['iPGPHandle']] = anPGP;
        if (this.iNamesMap[anINameHash] != undefined) {
          this.iPGPHandleMap[anPGP['iPGPHandle']]['iName'] = this.iNamesMap[anINameHash]['iName'];
        }
      }
    }

    // console.log('--------->------');
    // console.log(this.iPGPHandleMap);
    // console.log('---------<------');
  }
}
