import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerMessengerComponent } from './inner-messenger.component';

describe('InnerMessengerComponent', () => {
  let component: InnerMessengerComponent;
  let fixture: ComponentFixture<InnerMessengerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerMessengerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerMessengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
