import { Component, OnInit, Input } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { MessengerService } from '../services/messenger.service';

@Component({
  selector: 'inner-messenger',
  templateUrl: './inner-messenger.component.html',
  styleUrls: ['./inner-messenger.component.css']
})
export class InnerMessengerComponent implements OnInit {

  @Input('iPGPHandle') iPGPHandle;
  @Input('iPGPHandleShort') iPGPHandleShort;
  @Input('iPGPHandleMap') iPGPHandleMap;

  public notiMsgCntnr = '';
  public msgBody = '';
  public myIPGPBindings = [];
  public myIPGPBindingsDict = {};
  public selectedSenderPGP = '';
  public messages = [
    { msgTxt: 'hello', sender: 'other' },
    { msgTxt: 'hello Hu', sender: 'me' },
  ];

  constructor(
    public gUtils: GUtils,
    private msgService: MessengerService,
  ) { }

  ngOnInit() {
    console.log('iPGPHandleMap', this.iPGPHandleMap);
    this.loadMessages();
    this.listMyIPGPBindings();
    this.listMyNeighbors();
    this.getMyDirectMsgs();
  }

  public listMyIPGPBindings() {
    this.msgService.listMyIPGPBindings({}).subscribe(res => {
      console.log('myIPGPBindings, ', res);
      this.myIPGPBindings = res;
      for (let aP of this.myIPGPBindings) {
        this.myIPGPBindingsDict[aP.bindedDummyKey] = aP;
      }
    });
  }

  public listMyNeighbors() {
    this.msgService.listMyNeighbors({}).subscribe(res => {
      console.log('myNeighbors, ', res);
      this.myNeighbors = res;
    });
  }

  public loadMessages() {
    this.notiMsgCntnr = '';
    if (!this.iPGPHandleMap[this.iPGPHandle]) {
      console.log('no iPGPHandleMap!');
      return;
    }

    let receiverIPNHash = this.iPGPHandleMap[this.iPGPHandle].nbConfInfo.iPNHash;
    let receiverIPLabel = this.iPGPHandleMap[this.iPGPHandle].nbConfInfo.iPLabel;
    if (!receiverIPNHash || !receiverIPLabel) {
      this.notiMsgCntnr = `Missed Receiver's iName or iPGPTitle`;
      return;
    }

    this.msgService.loadMessages({
      senderIPNHash: receiverIPNHash,
      senderIPLabel: receiverIPLabel,
      receiverIPNHash: this.myIPGPBindingsDict[this.selectedSenderPGP].bindedIName,
      receiverIPLabel: this.myIPGPBindingsDict[this.selectedSenderPGP].bindedTitle,
    }).subscribe(res => {
      this.messages = res.records;
    });
  }

  public sendPlainTextMessage(dTarget) {
    this.notiMsgCntnr = '';
    if (this.selectedSenderPGP == '') {
      this.notiMsgCntnr = 'Please select an iPGP key of yours to send message';
      return;
    }
    if (this.msgBody == '') {
      this.notiMsgCntnr = 'Message is empty';
      return;
    }
    console.log('selectedSenderPGP', this.selectedSenderPGP);
    let params = {
      dTarget,
      msgBody: this.msgBody,
      senderverBbindedIName: this.myIPGPBindingsDict[this.selectedSenderPGP].bindedIName,
      senderverBbindedLabel: this.myIPGPBindingsDict[this.selectedSenderPGP].bindedTitle,
      receiverIPNHash: this.iPGPHandleMap[this.iPGPHandle].nbConfInfo.iPNHash,
      receiverIPLabel: this.iPGPHandleMap[this.iPGPHandle].nbConfInfo.iPLabel
    };
    // console.log('params, params', params);
    this.msgService.sendPlainTextMessage(params).subscribe(res => {
      this.msgBody = '';
      this.notiMsgCntnr = '';
      if (res.msg)
        this.notiMsgCntnr = res.msg;
    });
  }


  public myDirectMsgs = [{}];
  public getMyDirectMsgs() {
    this.msgService.getMyDirectMsgs({}).subscribe(res => {
      console.log(res);
      this.myDirectMsgs = res.records;
    });
  }

  public deleteDirectMsg(dmId) {
    console.log(`dmId`, dmId);
    this.msgService.deleteDirectMsg({ dmId }).subscribe(res => {
      this.getMyDirectMsgs();
    });
  }

  public refreshMyDirectMsgs() {
    this.msgService.refreshMyDirectMsgs({}).subscribe(res => {
      this.getMyDirectMsgs();
    });
    // second time
    setTimeout(() => {
      this.msgService.refreshMyDirectMsgs({}).subscribe(res => {
        this.getMyDirectMsgs();
      });
    }, 3000);
  }

  public directMsgBody = '';
  public myNeighbors = [];
  public myNeighborsDict = {};
  public notiDirMsgCntnr = '';
  public selectedNeighbor = '';
  public sendDirectMsgToNeighbor() {
    this.notiDirMsgCntnr = '';
    if (this.selectedNeighbor == '') {
      this.notiDirMsgCntnr = 'Please select a neighbor key of yours to send message';
      return;
    }
    if (this.directMsgBody == '') {
      this.notiDirMsgCntnr = 'Message is empty';
      return;
    }
    let params = {
      directMsgBody: this.directMsgBody,
      selectedNeighbor: this.selectedNeighbor
    };
    this.msgService.sendDirectMsgToNeighbor(params).subscribe(res => {
      this.directMsgBody = '';
      this.notiDirMsgCntnr = '';
      if (res.msg)
        this.notiDirMsgCntnr = res.msg;
    });
  }

}
