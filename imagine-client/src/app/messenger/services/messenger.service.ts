import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class MessengerService {
  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }
  
  public listMyIPGPBindings(args) {
    return this.http.put(this.url + '/api/messenger/listMyIPGPBindings', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public listMyNeighbors(args) {
    return this.http.put(this.url + '/api/messenger/listMyNeighbors', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public loadMessages(args) {
    return this.http.put(this.url + '/api/messenger/loadMessages', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public sendPlainTextMessage(args) {
    return this.http.put(this.url + '/api/messenger/sendPlainTextMessage', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public sendDirectMsgToNeighbor(args) {
    return this.http.put(this.url + '/api/messenger/sendDirectMsgToNeighbor', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public getMyDirectMsgs(args) {
    return this.http.put(this.url + '/api/messenger/getMyDirectMsgs', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public refreshMyDirectMsgs(args) {
    return this.http.put(this.url + '/api/messenger/refreshMyDirectMsgs', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public deleteDirectMsg(args) {
    return this.http.put(this.url + '/api/messenger/deleteDirectMsg', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
}



