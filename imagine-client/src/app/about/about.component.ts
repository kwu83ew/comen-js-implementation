import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { WalletService } from '../wallet/services/wallet.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'img-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  logoUrl = '../assets/imagine.jpg';

  public AngularInterfaceVersion = '0.0.0';
  public imgClientVersion = '0.0.-';
  public machineSignals;
  public networkSignals;

  constructor(
    public gUtils: GUtils,
    public walletService: WalletService,
  ) { }

  ngOnInit() {
    this.getClientVersion();
    this.getMachineSignals();
    this.getNetworkSignals();
  }

  public getClientVersion() {
    this.gUtils.getClientVersion({
      callback: this.callback_getClientVersion.bind(this),
    });
  }
  public callback_getClientVersion() {
    this.imgClientVersion = this.gUtils.dummyCallbackRes['getClientVersionRes'].imgClientVersion;
    console.log(this.imgClientVersion);
  }

  public getMachineSignals() {
    this.gUtils.getMachineSignals({
      callback: this.callback_getMachineSignals.bind(this),
    });
  }
  public callback_getMachineSignals() {
    let machineSignals = this.gUtils.dummyCallbackRes['getMachineSignalsRes'];
    let finalSignals = [];
    for (let aSig of machineSignals) {
      let aRecord = [];
      for (let enEntry of this.gUtils.objKeys(aSig)) {
        aRecord.push(`${enEntry}: ${aSig[enEntry]}`);
      }
      finalSignals.push(aRecord.join(', '));
    }
    this.machineSignals = finalSignals;

    console.log('this.machineSignals', this.machineSignals);
  }

  public getNetworkSignals() {
    this.gUtils.getNetworkSignals({
      callback: this.callback_getNetworkSignals.bind(this),
    });
  }
  public callback_getNetworkSignals() {
    this.networkSignals = this.gUtils.dummyCallbackRes['getNetworkSignalsRes'];
  }

}
