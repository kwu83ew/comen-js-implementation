import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InameComponent } from './iname.component';

describe('InameComponent', () => {
  let component: InameComponent;
  let fixture: ComponentFixture<InameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
