import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class InameService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public loadRecordedINames(args) {
    return this.http.put(this.url + '/api/iname/loadRecordedINames', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getUserINames() {
    return this.http.put(this.url + '/api/iname/getUserINames', {}).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public createAndBindIPGP(args) {
    return this.http.put(this.url + '/api/iname/createAndBindIPGP', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public onUpload(selectedFile) {
    const uploadData = new FormData();
    uploadData.append('theFile', selectedFile, selectedFile.name);
    return this.http.post(
      this.url + '/api/utils/uploadFile',
      uploadData
    ).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  public recordFile(args) {
    return this.http.put(this.url + '/api/utils/recordFile', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }


}
