import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GUtils } from 'src/app/services/utils';
import { InameService } from '../services/iname.service';

@Component({
  selector: 'in-custom-post',
  templateUrl: './custom-post.component.html',
  styleUrls: ['./custom-post.component.css']
})
export class CustomPostComponent implements OnInit {

  public cPNotiMsg = '';
  public cPMediaNotiMsg = '';

  constructor(
    private route: ActivatedRoute,
    public gUtils: GUtils,
    private iNameService: InameService
  ) { }

  ngOnInit() {
  }


  public cPFileLabel: string;
  public selectedFile: File;
  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log('file', this.selectedFile);
  }

  public onUpload(dTarget) {
    this.iNameService.onUpload(this.selectedFile).subscribe(res => {
      this.recordFile(dTarget, res);
    });
  }
  public recordFile(dTarget, args) {
    args.dTarget = dTarget;
    args.cPFileLabel = this.cPFileLabel;
    console.log('args', args);
    this.iNameService.recordFile(args).subscribe(res => {
      console.log('res', res);
    });
  }

}
