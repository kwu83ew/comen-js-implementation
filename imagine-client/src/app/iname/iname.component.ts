import { Component, OnInit, NgModule } from '@angular/core';
import { GUtils } from '../services/utils';
import { InameService } from './services/iname.service';
import { Router, Routes, RouterModule, RouterState, ActivatedRoute } from '@angular/router';
import { MessengerComponent } from '../messenger/messenger.component';

// const routes: Routes = [
//   // { path: '', redirectTo: '/', pathMatch: 'full' },
//   // { path: '/messenger', redirectTo: '/', pathMatch: 'full' },
//   // { path: '/Messenger', redirectTo: '/', component: MessengerComponent },
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
@Component({
  selector: 'im-iname',
  templateUrl: './iname.component.html',
  styleUrls: ['./iname.component.css']
})
export class InameComponent implements OnInit {

  public recordedINames: [];
  public userINames: [];
  public bindingDict: {};

  constructor(
    public gUtils: GUtils,
    private iNameService: InameService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.loadRecordedINames();
    this.getUserINames();
  }

  public loadRecordedINames() {
    this.gUtils.loadRecordedINames({
      callback: this.callback_loadRecordedINames.bind(this)
    });
  }
  public callback_loadRecordedINames() {
    let res = this.gUtils.loadRecordedINamesRes;
    this.recordedINames = res.records;
    this.bindingDict = res.bindingDict;

  }

  public getUserINames() {
    this.iNameService.getUserINames().subscribe(res => {
      console.log(res);
      this.userINames = res.records;
    });
  }

  public goToMessenger(iPGPHandle) {
    // console.log(iPGPHandle);
    // this.gUtils.setCookie('iPGPHandle', iPGPHandle);
    this.gUtils.setSelectedTab('messenger');
    // this.router.navigate(['/messenger']);
    this.gUtils.redirectToPage(`/${iPGPHandle}`);
  }

}
