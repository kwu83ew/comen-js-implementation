import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnInameComponent } from './an-iname.component';

describe('AnInameComponent', () => {
  let component: AnInameComponent;
  let fixture: ComponentFixture<AnInameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnInameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnInameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
