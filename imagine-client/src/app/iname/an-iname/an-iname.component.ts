import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GUtils } from 'src/app/services/utils';
import { InameService } from '../services/iname.service';

@Component({
  selector: 'im-an-iname',
  templateUrl: './an-iname.component.html',
  styleUrls: ['./an-iname.component.css']
})
export class AnInameComponent implements OnInit {

  public userINames = [];

  public params;
  public uid;
  public iName = '';
  public iNameNotiMsg = '';

  public bPGPNotiMsg;
  public nbINameHash = '';
  public iPLabel = 'Default';
  public iPComment = '';

  constructor(
    private route: ActivatedRoute,
    public gUtils: GUtils,
    private iNameService: InameService
  ) { 
    this.iName = this.gUtils.dummyURLParser().segmetns[0];
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {

      this.params = params;
      this.uid = params.get('uid');
      console.log('iname this.params', this.uid);
    });
    this.iName = this.gUtils.dummyURLParser().segmetns[0];
    this.getUserINames();

  }

  public getUserINames() {
    this.iNameService.getUserINames().subscribe(res => {
      this.userINames = res.records;
    });
  }

  public createAndBindIPGP(dTarget) {
    this.bPGPNotiMsg = '';
    if (this.nbINameHash == '')
      this.bPGPNotiMsg = 'Please select an already registered iName from DropDown.';

    this.iNameService.createAndBindIPGP({
      dTarget,
      nbINameHash: this.nbINameHash,
      iPLabel: this.iPLabel,
      iPComment: this.iPComment,
    }).subscribe(res => {
      console.log(res.records);
      if (res.msg)
        this.bPGPNotiMsg = res.msg;
    });
  }

}
