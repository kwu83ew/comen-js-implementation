import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AppError } from '../../errors/app-error';


@Injectable({
  providedIn: 'root'
})
export class PluginService {
  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public getKValue(kv_key) {
    return this.http.put(this.url + '/api/plugins/getKValue', { kv_key }).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

}
