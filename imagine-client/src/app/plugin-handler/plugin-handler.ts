import { Injectable } from '@angular/core';
import { GUtils } from '../services/utils';
import { ValidHooks } from './hooks-list';
import { IPluginListHandler } from '../plugins/plugins-list';
import { PluginService } from './services/plugin.service';

@Injectable({
  providedIn: 'root'
})

export class IPluginHandler {

  plugins = null;
  static registeredHooks = null;
  validHooksName = [];
  alreadyRegisterd = [];
  doCallRes = '';

  constructor(
    public gUtils: GUtils,
    public plgListHandler: IPluginListHandler,
    public validHooks: ValidHooks,
    public pluginService: PluginService,
  ) {

  }

  ngOnInit() {
    this.alreadyRegisterd = [];
    this.loadPlugins();
  }

  public loadPlugins() {

    if (IPluginHandler.registeredHooks == null) {
      IPluginHandler.registeredHooks = {};
      let pluginsList = this.plgListHandler.getPluginsList();
      let validHooks = this.validHooks.getValidHooks();
      this.validHooksName = this.gUtils.objKeys(validHooks);

      // console.log(validHooksName);

      for (let plugin of this.gUtils.objKeys(pluginsList)) {
        if (pluginsList[plugin].status == 'enabled') {
          let pluginPath = `../plugins/${pluginsList[plugin].path}/index`;
          // console.log(`load plugin: ${plugin}(${pluginPath})  `);
          // console.log('are eq ', '../plugins/i-neightbors-reputation/index' == pluginPath);
          import('../plugins/i-neightbors-reputation/index').then((plg) => {
            // let thePlg = new plg;
            let plgMainClass = new plg[pluginsList[plugin].className];
            let hooks = plgMainClass.getHooks();
            // console.log(hooks);
            let plgHookClass = new hooks;
            let methods = this.gUtils.getAllMethodNames(plgHookClass);
            // console.log('methods', methods);
            for (let aHook of methods) {
              if (this.validHooksName.includes(aHook)) {
                // console.log('registering Hook: ', aHook);
                if (!this.alreadyRegisterd.includes(aHook)) {
                  this.alreadyRegisterd.push(aHook);
                  IPluginHandler.registeredHooks[aHook] = [];
                }
                IPluginHandler.registeredHooks[aHook].push([plgHookClass, pluginPath]);
              }
            }
            // console.log('aaaaaaaaaaregisteredHooks1', registeredHooks);
          });
        }
      }
      // console.log('bbbbbbbbbbbbbregisteredHooks1', IPluginHandler.registeredHooks);
    }




  }

  public async doCallAsync(func, args) {
    return new Promise((resolve, reject) => {
      try {
        if (this.plugins == null)
          this.plugins = this.loadPlugins();

        let imApp = {
          gUtils: this.gUtils,
          pluginService: this.pluginService,
        }

        this.doCallRes = '';
        //todo: remove this stiupid timeout and use onload
        setTimeout(async () => {
          if (this.validHooksName.includes(func)) {
            // console.log(`doCall func ${func}`);
            // console.log(this.gUtils.objKeys(IPluginHandler.registeredHooks));
            // console.log('rrrrrrrrregisteredHooks3', IPluginHandler.registeredHooks[func]);
            if (IPluginHandler.registeredHooks.hasOwnProperty(func)) {

              for (let aCallBack of IPluginHandler.registeredHooks[func]) {
                try {
                  args['imApp'] = imApp;
                  this.doCallRes += await aCallBack[0][func](args);
                  // console.log(`aCallBack`, aCallBack, this.doCallRes);
                } catch (e) {
                  console.log('Hook calling error', e);
                }
              }
              // console.log(`func: ${func}  --- ${this.doCallRes}`);
              return resolve(this.doCallRes);
            }
          }
        }, 2000);





      } catch (e) {
        console.log(e);
        reject(e)
      }
    });
  }

}