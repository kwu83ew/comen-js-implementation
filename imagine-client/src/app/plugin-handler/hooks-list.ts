import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})

export class ValidHooks {

    validHooks;

    onInit() {}

    public getValidHooks() {
        this.validHooks = {
            'ch_before_plugin_content': {},
            'ch_after_plugin_content': {},
        }
        return this.validHooks;
    }
}