import { Component, OnInit } from '@angular/core';
import { MiscService } from './services/misc.service';
import { GUtils } from '../services/utils';

// import { DataSet, Timeline } from 'vis/index-timeline-graph2d';

@Component({
  selector: 'im-misc',
  templateUrl: './misc.component.html',
  styleUrls: ['./misc.component.css']
})
export class MiscComponent implements OnInit {
  totalForRange: number;
  yearlyMinting = [];
  oHoursLevelGrowth = [1, 2, 3, 5, 10, 20, 50, 100, 200, 500, 1000, 1500, 2500, 5000, 10000];
  cAGrowth = 100;


  fnNotiMsg = '';
  iName = '';
  registeredINames = [];
  reservesDetails;
  public appCloneId: number;

  // public Graph2d: Graph2d;
  constructor(
    public gUtils: GUtils,
    private miscService: MiscService
  ) { }


  ngOnInit() {

    this.fnNotiMsg = '';
    this.iName = '';
    // this.dspFullCoinAndWorthGraph();
    this.getRegisteredINames();
    this.getReservesDetails();

    this.appCloneId = this.gUtils.getAppCloneId();
  }

  public formatIt(inp) {
    return this.gUtils.microPAIToPAI(inp);
    // return this.gUtils.sepNum(inp);
  }

  public getRegisteredINames() {
    this.gUtils.getRegisteredINames({}).subscribe(res => {
      // console.log('getRegisteredINames', res);
      this.registeredINames = res;
    });
  }

  public getReservesDetails() {
    this.gUtils.getReservesDetails({}).subscribe(res => {
      console.log(res);
      this.reservesDetails = res;
    });
  }

  public calcWholeIssuance() {
    this.gUtils.predictFutureCoinIssuance({
      years: 20
    }).subscribe(res => {
      // console.log(res);
      this.completeReleaseInfo = res;
    });
  }


  ISSUANCE = Math.pow(2, 52)
  myWorkHours = 1
  myWorkLevel = 1
  myWork = this.myWorkHours * this.myWorkLevel;
  public completeReleaseInfo = [];

  public dspFullCoinAndWorthGraph() {
    console.log(this.gUtils.getNow());
    console.log(this.gUtils.minutesAfter(1440));

    setTimeout(() => {
      try {
        let cAGrowth = this.cAGrowth;
        this.gUtils.predictFutureCoinIssuance({
          years: 20
        }).subscribe(res => {
          // console.log(res);
          this.completeReleaseInfo = res;
        });
      } catch (e) {
        console.log(e);
      }
    }, 200);
  }

  public doesAcceptResponsibility;
  public registreIName(mod, dTarget) {
    this.fnNotiMsg = '';

    if ((this.iName == null) || (this.iName == '')) {
      this.fnNotiMsg = 'You must type an string as an iName, to register it';
      return;
    }

    if (!this.doesAcceptResponsibility) {
      this.fnNotiMsg = 'You must accept your responsibility of iName';
      return;
    }

    this.miscService.registreIName({
      mod,
      dTarget,
      iName: this.iName
    }).subscribe(res => {
      console.log(res);
      this.getRegisteredINames();
      this.fnNotiMsg = res.msg;
      // if ((res.mod == 'estimate') && (res.iNameRegCost != undefined)) {
      //   this.fnNotiMsg = `The register cost for iName(${res.iName}) is ` + ' &cong; ' + this.gUtils.microPAIToPAI(res.iNameRegCost);
      // }
    });

  }

  /**
   * request For Release Reserve
   */
  public reqForRelRes(blockHash, reserveNumber) {
    this.miscService.reqForRelRes({ blockHash, reserveNumber }).subscribe(res => {
      console.log(res);

    });
  }


  public generateTestData(datasetNumber) {
    this.gUtils.generateTestData({
      datasetNumber,
      callback: this.callback_generateTestData.bind(this),
    });
  }
  public callback_generateTestData() {
    console.log('Done callback generateTestData', this);
  }


}
