import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorPollingsComponent } from './administrator-pollings.component';

describe('AdministratorPollingsComponent', () => {
  let component: AdministratorPollingsComponent;
  let fixture: ComponentFixture<AdministratorPollingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministratorPollingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorPollingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
