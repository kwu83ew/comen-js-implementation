import { Component, OnInit } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { AdmPollingsService } from './adm-pollings/adm-pollings.service';

@Component({
  selector: 'administrator-pollings',
  templateUrl: './administrator-pollings.component.html',
  styleUrls: ['./administrator-pollings.component.css']
})
export class AdministratorPollingsComponent implements OnInit {

  constructor(
    public gUtils: GUtils,
    private admPollingsService: AdmPollingsService
  ) { }

  ngOnInit() {
    this.loadAdmPollings();
  }


  public admPollings = [];
  public pFormsDict = {};
  public loadAdmPollings() {
    this.admPollingsService.loadAdmPollings({}).subscribe(res => {
      this.admPollings = res.records;
      for (let aPl of this.admPollings) {
        this.pFormsDict[aPl.key] = aPl;
      }
      // console.log('admPollings Pollings', this.admPollings);
    });
  }

  public selectedPollingRadio = null;
  public slctdPolling(slctR) {
    this.selectedPollingRadio = slctR;
    console.log(`slctdPolling: ${slctR}`);
  }

  // public pForms = [
  //   'RFRlRsCoins',
  //   'RFRfBasePrice',
  //   'RFRfBlockFixCost',
  //   'RFRfMinS2V',
  //   'RFRfMinFSign',
  //   'RFRfMinFVote',
  //   'RFRfPLedgePrice',
  // ]

  public stPollNotifyMsg = '';
  public createAPollingFor() {
    this.stPollNotifyMsg = '';
    let args = {
      pSubject: this.selectedPollingRadio,
      pValues: this.pFormsDict[this.selectedPollingRadio].pValues
    }
    console.log(`create APollingFor args`, args);
    this.admPollingsService.createAPollingFor(args).subscribe(res => {
      console.log('admPollings Pollings', res);
      if (res.msg)
        this.stPollNotifyMsg = res.msg;
    });
  }

}
