import { TestBed } from '@angular/core/testing';

import { AdmPollingsService } from './adm-pollings.service';

describe('AdmPollingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmPollingsService = TestBed.get(AdmPollingsService);
    expect(service).toBeTruthy();
  });
});
