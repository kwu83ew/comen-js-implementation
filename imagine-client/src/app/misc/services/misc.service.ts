import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class MiscService {


  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }
  

  public registreIName(args) {
    return this.http.put(this.url + '/api/misc/registreIName', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public reqForRelRes(args) {
    return this.http.put(this.url + '/api/misc/reqForRelRes', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }



}
