import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class WikiService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public createNewWikiPage(args) {
    return this.http.put(this.url + '/api/wiki/createNewWikiPage', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getOnchainWkPages(args) {
    return this.http.put(this.url + '/api/wiki/getOnchainWkPages', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

    
  public getWkPageInfo(args) {
    return this.http.put(this.url + '/api/wiki/getWkPageInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
    


}
