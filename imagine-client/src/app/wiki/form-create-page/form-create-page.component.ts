import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GUtils } from 'src/app/services/utils';
import { WikiService } from '../services/wiki.service';

@Component({
  selector: 'wk-form-create-page',
  templateUrl: './form-create-page.component.html',
  styleUrls: ['./form-create-page.component.css']
})
export class FormCreatePageComponent implements OnInit {
  @Input('mpSelectedLang') mpSelectedLang;
  @Input('dspWkForm') dspWkForm;
  @Output() hideNewPageFormParent = new EventEmitter<any>();

  constructor(
    private sanitization: DomSanitizer,
    public gUtils: GUtils,
    private wikiService: WikiService
  ) { }

  ngOnInit() {
    this.getMyOnchainFiles();
  }

  public notiMsgCntnr = '';
  public wkTitle = '';
  public wkContent = '';
  public wkUniqueHash = '';
  public createNewWikiPage(dTarget) {
    this.notiMsgCntnr = '';
    if (!this.mpSelectedLang) {
      this.notiMsgCntnr = 'Please select a language';
      return;
    }
    if (this.wkTitle == '') {
      this.notiMsgCntnr = 'Title is empty';
      return;
    }
    if (this.wkContent == '') {
      this.notiMsgCntnr = 'Content is empty';
      return;
    }
    let params = {
      dTarget,
      wkUniqueHash: this.wkUniqueHash,
      wkTitle: this.gUtils.encode_utf8(this.wkTitle),
      wkContent: this.wkContent,
      mpSelectedLang: this.mpSelectedLang
    };
    console.log('params, params', params);
    this.wikiService.createNewWikiPage(params).subscribe(res => {
      console.log(res);
      if (res.err != false) {
        this.notiMsgCntnr = res.msg;
      } else {
        this.redirectTo('imagine/wiki');
      }
    });
  }

  public redirectTo(gg) {

  }

  public cPMediaNotiMsg = '';
  public getMyOnchainFiles() {
    this.gUtils.getMyOnchainFiles({
      callback: this.callback_getMyOnchainFiles.bind(this),
    });
  }

  public theFiles = [];
  public callback_getMyOnchainFiles() {
    console.log('get MyOnchainFiles', this.gUtils.myOnchainFiles);
    for (let aFile of this.gUtils.myOnchainFiles.records) {
      let extension = aFile.mpf_name.split('.').pop().toLowerCase();
      if (this.gUtils.imageExtensions.includes(extension)) {
        aFile.url = `${this.gUtils.myOnchainFiles.iCache}/${aFile.mpf_name}`

      } else {
        // aFile.url = `${this.gUtils.myOnchainFiles.iCache}/${this.gUtils.fileImage}`
        aFile.url = `../assets/file.png`

      }
      console.log('aFile.mpf_name', aFile.mpf_name.split('.').pop());
      this.theFiles.push(aFile);
    }
  }

  public closeForm() {
    this.dspWkForm = false;
    this.hideNewPageFormParent.next({ dspWkForm: this.dspWkForm });
  }



}
