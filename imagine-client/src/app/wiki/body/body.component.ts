import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { WikiService } from '../services/wiki.service';
import { GUtils } from 'src/app/services/utils';
import { DomSanitizer } from '@angular/platform-browser';
import { FormCreatePageComponent } from '../form-create-page/form-create-page.component';

@Component({
  selector: 'wiki-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  @Input('dspWkForm') dspWkForm;
  @Output() dspNewPageFormParent = new EventEmitter<any>();
  @Output() fillEditFormParent = new EventEmitter<any>();



  constructor(
    private sanitization: DomSanitizer,
    public gUtils: GUtils,
    private wikiService: WikiService
  ) { }

  ngOnInit() {
    this.setSelectedWkPageInfo();

  }



  public wkpIName = '';
  public wkpTitle = '';
  public setSelectedWkPageInfo() {
    let urlDtl = this.gUtils.dummyURLParser();
    console.log('urlDtl', urlDtl);
    if ((urlDtl.segmetns.length > 1) && (urlDtl.segmetns[1] == 'wiki')) {
      // we are in a wiki page
      this.wkpIName = decodeURIComponent(urlDtl.segmetns[0]);
      this.wkpTitle = decodeURIComponent(urlDtl.segmetns[2]);
      this.getWkPageInfo({
        iName: this.wkpIName,
        wkPgTitle: this.wkpTitle
      });
    }
  }

  public wkpInfo;
  public getWkPageInfo(args) {
    this.wikiService.getWkPageInfo(args).subscribe(res => {
      this.wkpInfo = res.record;
    });
  }

  public dspNewPageForm() {
    this.dspWkForm = true;
    this.dspNewPageFormParent.next({ dspWkForm: this.dspWkForm });
  }

  public fillEditForm(record) {
    this.fillEditFormParent.next({ record });
  }

  public dspPageEditForm(wkpUniqueHash) {
    this.dspNewPageForm();
    this.wikiService.getWkPageInfo({ wkpUniqueHash, renderToHtml: false }).subscribe(res => {
      console.log(res);
      if (res.err == false)
        this.fillEditForm(res.record);
    });

  }
}
