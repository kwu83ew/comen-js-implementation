import { Component, OnInit, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { CookieService } from 'src/app/services/cookie.service';
import { WikiService } from '../services/wiki.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'wiki-side',
  templateUrl: './side.component.html',
  styleUrls: ['./side.component.css']
})
export class SideComponent implements OnInit {

  @Input('dspWkForm') dspWkForm;
  @Output() alterDspPageEditFormParent = new EventEmitter<any>();

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private wikiService: WikiService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.getOnchainWkPages()
  }

  public wkPages = [];
  public getOnchainWkPages() {
    this.wikiService.getOnchainWkPages({}).subscribe(res => {
      console.log('getOnchainWkPages', res);
      this.wkPages = res.records;
    });
  }


  public alterDspPageEditForm() {
    this.dspWkForm = !this.dspWkForm;
    this.alterDspPageEditFormParent.next({ dspWkForm: this.dspWkForm });
  }

}
