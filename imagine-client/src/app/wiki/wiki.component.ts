import { Component, OnInit, ViewChild } from '@angular/core';
import { GUtils } from '../services/utils';
import { WikiService } from './services/wiki.service';
import { FormCreatePageComponent } from './form-create-page/form-create-page.component';

@Component({
  selector: 'im-wiki',
  templateUrl: './wiki.component.html',
  styleUrls: ['./wiki.component.css']
})
export class WikiComponent implements OnInit {

  @ViewChild(FormCreatePageComponent, { static: false })
  private formCreatePageComponent: FormCreatePageComponent;

  public dspWkForm = false;
  public showWelcome = true;
  constructor(
    public gUtils: GUtils,
    public wkService: WikiService
  ) { }

  ngOnInit() {
    if (this.gUtils.machineProfileG === null) {
      this.gUtils.loadMachineProfileG({
        callback: this.setProfileCallback.bind(this)
      });
    }

    if (this.gUtils.languagesList.length == 0)
      this.getLanguagesList();

  }

  public mpSelectedLang = 'eng';
  public setProfileCallback() {
    // console.log('this.gUtils.machineProfileG', this.gUtils.machineProfileG);
    // console.log('setProfileCallback: ', this.gUtils.machineProfileG.selectedMP.language);
    this.mpSelectedLang = this.gUtils.machineProfileG.selectedMP.language;
  }

  public getLanguagesList() {
    this.gUtils.getLanguagesList({});
  }

  public handleWelcomeMsg() {
    let urlDtl = this.gUtils.dummyURLParser();
    if ((urlDtl.segmetns.length > 2) && (urlDtl.segmetns[1] == 'wiki')) {
      this.showWelcome = false;
    } else {
      this.showWelcome = true;
    }
  }


  public alterDspPageEditForm(e){
    this.dspWkForm = e.dspWkForm;
  }
  
  public dspNewPageForm(e){
    this.dspWkForm = e.dspWkForm;
  }

  public hideNewPageForm(e){
    console.log('e in parent', e);
    this.dspWkForm = e.dspWkForm;
  }

  public fillEditForm(e){
    console.log('e in parent fillEditForm', e);
    this.formCreatePageComponent.wkUniqueHash = e.record.wkp_unique_hash;
    this.formCreatePageComponent.wkTitle = e.record.wkp_title;
    this.formCreatePageComponent.wkContent = e.record.content;
  }
}
