import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PluginCompComponent } from './plugin-comp.component';

describe('PluginCompComponent', () => {
  let component: PluginCompComponent;
  let fixture: ComponentFixture<PluginCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PluginCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PluginCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
