import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { IPluginHandler } from '../plugin-handler/plugin-handler';

@Component({
  selector: 'im-plugins',
  templateUrl: './plugin-comp.component.html',
  styleUrls: ['./plugin-comp.component.css', '../plugins/i-neightbors-reputation/plugin.css']
})
export class PluginCompComponent implements OnInit {

  public pluginContents;
  constructor(public gUtils: GUtils, public iPluginHandler: IPluginHandler) { }

  ngOnInit() {
    this.runHooks();

  }
  public async runHooks() {
    this.pluginContents = await this.iPluginHandler.doCallAsync('ch_before_plugin_content', {});
    // console.log('this.pluginContents', this.pluginContents);
    let tmpRes = await this.iPluginHandler.doCallAsync('ch_after_plugin_content', {});
    // console.log('this.pluginContents', tmpRes);
    this.pluginContents += tmpRes;
    // console.log('this.pluginContents', this.pluginContents);
    // this.pluginContents='<b>rrrrr</b>';
  }

}
