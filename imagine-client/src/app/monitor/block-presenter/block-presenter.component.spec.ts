import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockPresenterComponent } from './block-presenter.component';

describe('BlockPresenterComponent', () => {
  let component: BlockPresenterComponent;
  let fixture: ComponentFixture<BlockPresenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockPresenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
