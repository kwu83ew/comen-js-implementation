import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'block-presenter',
  templateUrl: './block-presenter.component.html',
  styleUrls: ['./block-presenter.component.css']
})
export class BlockPresenterComponent implements OnInit {

  @Input('wBlock') wBlock;
  @Output() traceCoinInfoParent = new EventEmitter<any>();
  
  public docSumOutputs =0;

  constructor(
    public gUtils: GUtils
  ) { }

  ngOnInit() {
  }

  public dspDic = {}
  public alterLogDsp(ttl) {
    if (this.dspDic[ttl] == undefined)
      this.dspDic[ttl] = false;

    if (this.dspDic[ttl] == true) {
      this.dspDic[ttl] = false;
    } else {
      this.dspDic[ttl] = true;
    }
    console.log(`this.dspDic`, this.dspDic);
  }

  siblTraceCoinInfo(coin) {
    console.log('traceCoinInfo', coin);
    this.traceCoinInfoParent.next({ coin, doRefresh: true });
  }

}
