import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';

@Component({
  selector: 'img-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {
  @Input('machineInfo') machineInfo;
  @Input('wAncestorsRes') wAncestorsRes;
  @Output() getMachineInfoParent = new EventEmitter<any>();
  @Output() detailedDagParent = new EventEmitter<any>();
  @Output() wRaiseSharesParent = new EventEmitter<any>();
  @Output() pushCoinbaseParent = new EventEmitter<any>();
  @Output() wBroadcastBlockParent = new EventEmitter<any>();
  @Output() pushP4PParent = new EventEmitter<any>();
  @Output() definitPushP4PParent = new EventEmitter<any>();
  @Output() sendClonedTrxParent = new EventEmitter<any>();
  @Output() machineBalanceChkParent = new EventEmitter<any>();
  @Output() machineBalanceChk3xParent = new EventEmitter<any>();
  @Output() alterBalanceLockParent = new EventEmitter<any>();
  @Output() dblclickedBlockShortHashParent = new EventEmitter<any>();
  @Output() blockGenReportParent = new EventEmitter<any>();
  @Output() invokeBlockParent = new EventEmitter<any>();
  @Output() invokeAllMissedParent = new EventEmitter<any>();
  @Output() refreshMissedBlockParent = new EventEmitter<any>();
  @Output() dlTrxListParent = new EventEmitter<any>();
  @Output() notInHistoryParent = new EventEmitter<any>();
  @Output() getWAncestorsParent = new EventEmitter<any>();
  @Output() makeTrxParent = new EventEmitter<any>();
  @Input('equalIn5DagsHash') equalIn5DagsHash;

  public selectedMachine;

  public blockShortHash = '';
  public createDateType = 'real';
  public paiReceiverNode = '';
  public calledTime = 0;
  public screenshotsList = [];
  public screenShotToNeighbor = '';

  constructor(public gUtils: GUtils) { }

  ngOnInit() {
    this.loadScreenshotsList();
  }

  loadScreenshotsList() {

    this.gUtils.loadScreenshotsList({
      callback: this.callback_loadScreenshotsList.bind(this),
    });
  }
  public theFiles = [];
  public callback_loadScreenshotsList() {
    console.log('get callback_loadScreenshotsList', this.gUtils.dummyCallbackRes['loadScreenshotsListRes'].err);
    this.screenshotsList = this.gUtils.dummyCallbackRes['loadScreenshotsListRes'].records;
    // for (let aFile of this.gUtils.myOnchainFiles.records) {
    //   let extension = aFile.mpf_name.split('.').pop().toLowerCase();
    //   if (this.gUtils.imageExtensions.includes(extension)) {
    //     aFile.url = `${this.gUtils.myOnchainFiles.iCache}/${aFile.mpf_name}`

    //   } else {
    //     // aFile.url = `${this.gUtils.myOnchainFiles.iCache}/${this.gUtils.fileImage}`
    //     aFile.url = `../assets/file.png`

    //   }
    //   console.log('aFile.mpf_name', aFile.mpf_name.split('.').pop());
    //   this.theFiles.push(aFile);
    // }
  }

  getMachineInfo() {
    // this.selectedMachine = i;
    this.getMachineInfoParent.next();
  }

  detailedDag(i) {
    this.selectedMachine = i;
    this.detailedDagParent.next();
  }

  wRaiseShares() {
    this.wRaiseSharesParent.next();
  }

  pushP4P() {
    this.pushP4PParent.next();
  }

  definitPushP4P(testCase) {
    this.definitPushP4PParent.next({ testCase });
  }

  pushCoinbase() {
    this.pushCoinbaseParent.next();
  }
  sendClonedTrx() {
    this.sendClonedTrxParent.next();
  }

  machineBalanceChk(i) {
    this.machineBalanceChkParent.next();
  }

  machineBalanceChk3x(i) {
    this.calledTime = 0;
    this.callerr3();
  }

  callerr3() {
    if (this.calledTime < 3) {
      this.calledTime++;
      setTimeout(() => {
        this.machineBalanceChkParent.next();
        this.callerr3();
      }, 713);
    }
  }

  dblclickedBlockShortHash(hash) {
    console.log(`hash--${hash}`);
    this.dblclickedBlockShortHashParent.next({ bShortHash: hash.substring(1) });
  }

  alterBalanceLock() {
    this.alterBalanceLockParent.next();
  }

  blockGenReport(i) {
    this.blockGenReportParent.next();
  }

  invokeBlock(e, cloneId) {
    console.log('this.blockShortHash', this.blockShortHash);//14af89ffbd2157b400fe3c404d558c0ac7514a5525c860134b33dddf97116a9c
    console.log('eeeee', e);//14af89ffbd2157b400fe3c404d558c0ac7514a5525c860134b33dddf97116a9c
    this.selectedMachine = cloneId;
    this.invokeBlockParent.next({ blockShortHash: this.blockShortHash });
  }

  invokeAllMissed(e, cloneId) {
    // this.selectedMachine = cloneId;
    this.invokeAllMissedParent.next();
  }
  refreshMissedBlock() {
    this.refreshMissedBlockParent.next();
  }
  dlTrxList() {
    this.dlTrxListParent.next();
  }


  public leaveShortHash;
  notInHistory() {
    this.notInHistoryParent.next({ leaveShortHash: this.leaveShortHash });
  }

  public stepBack = 1;
  getWAncestors() {
    this.getWAncestorsParent.next({ leaveShortHash: this.leaveShortHash, stepBack: this.stepBack });
  }

  makeTrx(args) {//{mode:, target:, unEqua:}
    console.log(`makeTrx111 ${args}`);
    args.paiReceiverNode = this.paiReceiverNode;
    args.createDateType = this.createDateType;
    this.makeTrxParent.next(args);
  }


  public manInvokeLeavesFromNeighbors() {
    this.gUtils.manInvokeLeavesFromNeighbors({}).subscribe(res => {
      console.log('manInvokeLeavesFromNeighbors', res);
    });
  }

  public dropBroadcastLogs() {
    this.gUtils.dropBroadcastLogs({}).subscribe(res => {
      console.log('dropBroadcastLogs', res);
    });
  }

  public dlDAGBundle(dTarget) {
    this.gUtils.dlDAGBundle({
      dTarget,
      callback: this.callback_dlDAGBundle.bind(this),
    });
  }
  public callback_dlDAGBundle() {
    console.log('Done callback dlDAGBundle', this);
  }


  public dlNodeScreenShot(dTarget) {
    this.gUtils.dlNodeScreenShot({
      dTarget,
      dNeighbor: this.screenShotToNeighbor,
      callback: this.callback_dlNodeScreenShot.bind(this),
    });
  }
  public callback_dlNodeScreenShot() {
    console.log('Done callback dlNodeScreenShot', this);
  }
  
  public uploadNodeScreenShot() {
    this.gUtils.onUploadG({ dTarget: 'nodeScreenShot' });
    setTimeout(() => {
      this.loadScreenshotsList();
    }, 2000);
  }

  public compareScreenShot(nss_id) {
    this.gUtils.compareScreenShot({
      nss_id,
      callback: this.callback_compareScreenShot.bind(this),
    });
  }
  public callback_compareScreenShot() {
    console.log('Done callback compareScreenShot', this);
    this.loadScreenshotsList();
  }

  public deleteSS(nss_id) {
    this.gUtils.deleteSS({
      nss_id,
      callback: this.callback_deleteSS.bind(this),
    });
  }
  public callback_deleteSS() {
    setTimeout(() => {
      this.loadScreenshotsList();
    }, 2000);
  }



}
