import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GUtils } from '../services/utils';
import { WatcherService } from '../watcher/services/watcher.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Network, DataSet, Node, Edge, IdType, Timeline } from 'vis';

@Component({
  selector: 'im-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.css']
})
export class MonitorComponent implements OnInit {

  @Output() machineBalanceChkParent = new EventEmitter<any>();
  @Output() machineBalanceChk3xParent = new EventEmitter<any>();
  // @Output() alterBalanceLockParent = new EventEmitter<any>();

  blocks; //: Block[]
  public blocksDict: {};
  public machinesInfoObj: {};
  public machineByBacker: {};
  public machineByCBKey: {};
  public machinesInfoArr: any[];
  public machinesOrderedByCBPriority: string;
  public equalIn5DagsInx: number;
  public equalIn5DagsDate: string;
  public equalIn5DagsHash: string;
  public fullSort;

  public selectedMachine;
  public imgInfo;
  public wInboxes: any[];
  // public parsingQ: any[];
  public sendingQ: any[];
  public wOutboxes: any[];

  public recursiveRefreshOn = false;
  public recursiveRefreshIcon = ' > Play ';

  public nodes: Node;
  public edges: Edge;
  public network: Network;

  public MIN_MONEY_RECEIVER = 2;
  public MAX_MONEY_RECEIVER = 6;

  // sending cloned trax
  public candidTransactions = [];
  public candidBackers = {};
  public cloneBroadcastDelay = 10000;

  constructor(
    public gUtils: GUtils,
    private watcherService: WatcherService,
  ) { }

  public localMachineInfoObj;
  public appCloneId;

  ngOnInit() {
    // this.dblclickedBlockShortHash(0, { bShortHash: '1f8d63' });//FV:67a00f, 1f8d63, double:b03d2c
    // this.pushBtnTrace = false;

    this.appCloneId = this.gUtils.getAppCloneId();

    this.wInboxes = [];
    // this.parsingQ = [];
    this.sendingQ = [];
    this.wOutboxes = [];

    this.blocksDict = {}

    let DAGBalancesTmp = {
      sumCoinbase: 0, spendables: 0,
      waitedCoinbases: { sum: 0, dblSpends: [] },
      waitedNormals: { sum: 0, dblSpends: [] },
      waitedIncomes: 0,
      floorishMicroPAIs: 0,
      sumCoinbaseColor: '#f9f9f9',
      spendablesColor: '#f9f9f9',
      waitedCoinbasesColor: '#f9f9f9',
      waitedNormalsColor: '#f9f9f9',
      waitedIncomesColor: '#f9f9f9',
      floorishMicroPAIsColor: '#f9f9f9',
      finalBalanceColor: '#f9f9f9',
      nodeWealthColor: '#f9f9f9'
    };

    this.localMachineInfoObj = {
      machineEmail: 'neighbor@imagine.com', machineName: 'neighbor',
      askForLeavesInfo: null, lastBLockInfo: {}, nodeBuffer: [], utxo12h: {}, UTXOShoot: '', missedBlocks: [], histBlocks: [],
      parsingQCount: [{ _count: 0 }], parsingQ: [], sendingQ: [],
      DAGBalances: DAGBalancesTmp, DAGBalancesPrev: DAGBalancesTmp, DAGBalancesLock: DAGBalancesTmp,
      isBalanceLocked: 'unlocked', cheatingCreationDate: '', cheatingAncestors: [],
      blockGenReport: {}
    };

    this.refreshDAG();
  }

  recursiveDAGRefresher() {
    this.refreshDAG();
    if (this.recursiveRefreshOn) {
      setTimeout(() => {
        this.recursiveDAGRefresher();
      }, 60000);
    }
  }

  refreshDAG() {
    this.getMachineInfo();
  }

  getMachineInfo() {
    this.watcherService.getMachineInfo(this.appCloneId).subscribe(res => {
      // console.log('getMachineInfo', res);
      // const md5 = new Md5();
      let machineInfo = res.data.machineInfo;
      machineInfo.machineName = this.localMachineInfoObj.machineName
      machineInfo.utxo12h = this.localMachineInfoObj.utxo12h
      machineInfo.UTXOShoot = this.localMachineInfoObj.UTXOShoot
      machineInfo.missedBlocks = this.localMachineInfoObj.missedBlocks
      machineInfo.sendingQ = this.localMachineInfoObj.sendingQ
      machineInfo.DAGBalances = this.localMachineInfoObj.DAGBalances
      machineInfo.DAGBalancesPrev = this.localMachineInfoObj.DAGBalancesPrev
      machineInfo.DAGBalancesLock = this.localMachineInfoObj.DAGBalancesLock
      machineInfo.isBalanceLocked = this.localMachineInfoObj.isBalanceLocked
      machineInfo.blockGenReport = this.localMachineInfoObj.blockGenReport
      machineInfo.cheatingCreationDate = this.localMachineInfoObj.cheatingCreationDate
      machineInfo.cheatingAncestors = this.localMachineInfoObj.cheatingAncestors
      this.imgInfo = res.data.imgInfo;

      let timeDtl;
      timeDtl = this.imgInfo.machineTime.split((':'));
      this.imgInfo.machineTimeNormal = timeDtl[0] + ':'
      this.imgInfo.machineTimeBold = timeDtl[1] + ':' + timeDtl[2];

      timeDtl = this.imgInfo.cycleFrom.split((':'));
      this.imgInfo.cycleFromNormal = timeDtl[0] + ':'
      this.imgInfo.cycleFromBold = timeDtl[1] + ':' + timeDtl[2];

      timeDtl = this.imgInfo.cycleTo.split((':'));
      this.imgInfo.cycleToNormal = timeDtl[0] + ':'
      this.imgInfo.cycleToBold = timeDtl[1] + ':' + timeDtl[2];
      console.log('machineInfo.coinbaseOrder', machineInfo.coinbaseOrder);
      machineInfo.machinesOrderedByCBPriority = machineInfo.coinbaseOrder.join(`<br>`);



      let bPrep = this.prepareBlocks(machineInfo.blocks);
      let nodes = new DataSet(bPrep.blockNodes);
      let edges = new DataSet(bPrep.blockEdges);
      machineInfo['dagId'] = 'dagId0';
      machineInfo['gData'] = {
        nodes,
        edges
      };

      let sendingQ = [];
      machineInfo.sendingQ.forEach(elm => {
        const hint = [elm.title, elm.sender, '->', elm.receiver, '(' + elm.sq_type + ' ' + elm.sq_code + ')'].join('\n');
        const hash = '\t' + elm.sq_type + ' ' + elm.sq_code; // this.gUtils.hash6c(md5.appendStr(hint).end().toString()) + ' ' +
        sendingQ.push({
          hint,
          hash
        });
      });
      machineInfo.sendingQ = sendingQ;

      machineInfo['outbox'] = this.prepareOutboxList(machineInfo.outbox);
      // UTXOsInfo = this.prepareUTXOStat(machineInfo.UTXOs);
      // machineInfo['utxo'] = this.prepareUTXOStat(machineInfo.UTXOs);
      // machineInfo['utxo'].sum = this.gUtils.microPAIToPAI(machineInfo['utxo'].sum);

      this.localMachineInfoObj = machineInfo;
      // this.machinesInfoArr = [];
      // for (let i = 0; i < 5; i++) {
      //   this.machinesInfoArr.push(this.localMachineInfoObj[i])
      // }

      this.recursiveRefreshOn = false;
      // if (machineInfo['container'] === null) {
      //   this.recursiveRefreshOn = false;
      // }

      if (machineInfo)
        machineInfo.histBlocks.forEach(aBlock => {
          this.blocksDict[aBlock.hash] = aBlock;
        });
      machineInfo.blocksDict = this.blocksDict;


      // coloring dag leaves

      let options = {
        width: '700px',
        height: '2400px',
        edges: {
          font: {
            size: 12,
            strokeWidth: 4
          },
          widthConstraint: {
            maximum: 90
          }
        },
        nodes: {
          shape: 'box',
          margin: 5,
          widthConstraint: {
            maximum: 200
          }
        },

        physics: {
          enabled: false
        }
      };

      // console.log(`nodeBuffer`, machineInfo.nodeBuffer);
      // cookie check to launch watcher DAG graph tab
      setTimeout(() => {
        new Network(document.getElementById('dagId0'), this.localMachineInfoObj['gData'], options);

      }, 200);

    });
  }

  prepareOutboxList(rawList) {
    const md5 = new Md5();
    let outbox = [];
    rawList.forEach(elm => {
      let hint = elm;
      if ((hint != undefined) && hint.includes(','))
        hint = hint.replace(',', '\n');
      const hash = this.gUtils.hash6c(md5.appendStr(elm).end().toString());
      outbox.push({
        hint,
        hash
      });
    });
    let start = outbox.length - 3;
    if (start < 0) {
      start = 0;
    }
    outbox = outbox.slice(start);
    return outbox;
  }

  public prepareBlocks(blocks) {
    let blockNodes = [];
    let blockEdges = [];
    let huY = -350;
    blocks.forEach(block => {
      huY += 171;
      let bId = this.gUtils.hash6c(block.blockHash);

      let lbl = `(${bId}) Anc:${block.ancestorsCount}
      \n${block.bType} ${block.confidence}% (${this.gUtils.shortBech8(block.backerAddress)})
      \n${block.creationDate}
      \n${block.receiveDate ? block.receiveDate : 'unduu'}
      `;
      let color = this.gUtils.getBGColorByType(block.bType);

      blockNodes.push({
        id: bId,
        color,
        widthConstraint: { minimum: 120 },
        heightConstraint: { minimum: 70, valign: 'top' },
        y: huY,
        label: lbl,
        title: lbl
      });

      let ancestors = block.ancestors;
      ancestors.forEach(dad => {
        blockEdges.push({ from: bId, to: this.gUtils.hash6c(dad), arrows: 'to', dashes: true })
      });

    });

    return { blockNodes, blockEdges }
  }


  public fetchInboxManually(emailType) {
    this.gUtils.fetchInboxManually({ emailType }).subscribe(res => {
      console.log('fetchInboxManually', res);
    });
  }

  public readingHarddiskManually() {
    this.gUtils.readingHarddiskManually({}).subscribe(res => {
      console.log('readingHarddiskManually', res);
    });
  }

  public pullFromQeueManually() {
    this.gUtils.pullFromQeueManually({}).subscribe(res => {
      console.log('pullFromQeueManually', res);
    });
  }

  public sendOutThePacketManually() {
    this.gUtils.sendOutThePacketManually({}).subscribe(res => {
      console.log('sendOutThePacketManually', res);
    });
  }


  machineBalanceChk() {
    if (this.localMachineInfoObj.isBalanceLocked == 'unlocked')
      this.localMachineInfoObj.DAGBalancesLock = this.localMachineInfoObj.DAGBalancesPrev;
    this.localMachineInfoObj.DAGBalancesPrev = this.localMachineInfoObj.DAGBalances;
    this.watcherService.machineBalanceChk(this.appCloneId).subscribe(res => {
      this.localMachineInfoObj.DAGBalances = res.data;
    });
  }

  refreshMissedBlock(cloneId) {
    this.watcherService.refreshMissedBlock(cloneId).subscribe(res => {
    });
  }

  rmvFromBuffer(cloneId, bufHash) {
    this.watcherService.rmvFromBuffer({ cloneId, bufHash }).subscribe(res => {
      this.refreshDAG();
    });
  }

  invokeAllMissed(eventObj, cloneId) {
    // console.log(args);
    // console.log(`cloneIdW11: ${cloneId}`);
    // console.log(`blockShortHashW11: ${args.blockShortHash}`);
    this.watcherService.invokeAllMissed(eventObj, cloneId).subscribe(res => {
      // console.log(res);
    });
  }

  dlTrxList(cloneId) {
    this.watcherService.dlTrxList(cloneId).subscribe(res => {
    });
  }


  public flshBuffDocMPnlNoti = '';
  public flushBufferedDocsInMonitorPanel() {
    this.flshBuffDocMPnlNoti = '';
    this.gUtils.flushBufferedDocs({
      callback: this.callback_flushBufferedDocsInMonitorPanel.bind(this)
    });
  }
  public callback_flushBufferedDocsInMonitorPanel() {
    let res = this.gUtils.dummyCallbackRes['flushBufferedDocs'];
    console.log('res', res);
    this.flshBuffDocMPnlNoti = res.msg;
    // maybe call push to network!
    this.refreshDAG();
  }

  public wBlock;
  public dblclickedBlockShortHash(cloneId, e) {
    e['cloneId'] = cloneId;
    this.watcherService.getBlockInfo(e).subscribe(res => {
      console.log('dblclicked Block Short Hash', res);
      this.wBlock = res;

    });
  }

  // public pushBtnTrace = false;
  public selectedCoin = ''; 
  public traceCoinInfo(e) {
    console.log(`traceCoinInfo in monitor comp`, e);
    this.selectedCoin = e.coin;
    if (e.doRefresh) {
      window['angularComponentReference'].zone.run(() =>
        { window['angularComponentReference'].componentTraceCoinInfon(); });
    }
    // console.log('--->>',this.pushBtnTrace);

  }

  alterBalanceLock() {
    if (this.localMachineInfoObj.isBalanceLocked == 'unlocked') {
      this.localMachineInfoObj.isBalanceLocked = 'locked';
    } else {
      this.localMachineInfoObj.isBalanceLocked = 'unlocked';
    }
  }

  invokeBlock(args, cloneId) {
    console.log(args);
    console.log(`cloneIdW11: ${cloneId}`);
    console.log(`blockShortHashW11: ${args.blockShortHash}`);
    this.watcherService.invokeBlock(cloneId, args.blockShortHash).subscribe(res => {
      console.log(res);
    });
  }

}
