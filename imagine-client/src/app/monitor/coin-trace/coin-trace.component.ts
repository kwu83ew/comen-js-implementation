import { Component, OnInit, Input, NgZone, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { CoinTraceService } from './services/coin-trace.service';
import { Network, DataSet, Node, Edge, IdType, Timeline } from 'vis';

@Component({
  selector: 'coin-trace',
  templateUrl: './coin-trace.component.html',
  styleUrls: ['./coin-trace.component.css']
})
export class CoinTraceComponent implements OnInit {

  @Input('selectedCoin') selectedCoin;
  // @Input('pushBtnTrace') pushBtnTrace;

  @Output() dblclickedBlockShortHashParent = new EventEmitter<any>();

  constructor(
    private zone: NgZone,
    public gUtils: GUtils,
    private coinTraceService: CoinTraceService,
  ) {

    window['angularComponentReference'] = {
      zone: this.zone,
      componentTraceCoinInfon: () => this.traceCoinInfo(),
      component: this,
    };

  }

  ngOnInit() {
  }

  public selectedCoinInfo = {};
  public coinTrack = [];
  public traceCoinInfo() {
    this.coinTraceService.traceCoinInfo({ selectedCoin: this.selectedCoin }).subscribe(res => {
      this.selectedCoinInfo = res;
      console.log('this.selectedCoinInfo', this.selectedCoinInfo);
      this.dspCoinGraph();
    });
  }

  public nodes: Node;
  public edges: Edge;
  public network: Network;
  public cTBlockNodes = [];
  public cTBlockEdges = [];
  public dspCoinGraph() {
    this.cTBlockNodes = [];
    this.cTBlockEdges = [];

    let huY = -350;
    let docId;
    for (let anStep of this.selectedCoinInfo['coinTrack']) {
      huY += 171;
      docId = this.gUtils.hash16c(anStep.descendent);
      let color = this.gUtils.getBGColorByType(anStep.bType);

      let lbl = `Block(${this.gUtils.hash16c(anStep.blockHash)}) Doc(${docId})
      \n Creation Date(${anStep.creationDate})
      `;
      // \n${block.bType} ${block.confidence}% (${this.gUtils.shortBech8(block.backerAddress)})
      // \n${block.creationDate}
      // \n${block.receiveDate ? block.receiveDate : 'unduu'}

      this.cTBlockNodes.push({
        id: docId,
        color: color,
        widthConstraint: { minimum: 120 },
        heightConstraint: { minimum: 70, valign: 'top' },
        y: huY,
        label: lbl,
        title: lbl
      });

      let ancestors = anStep.interestedDocs;
      // if (!Array.isArray(ancestors))
      //   ancestors = [ancestors];
      ancestors.forEach(dad => {
        this.cTBlockEdges.push({
          from: docId,
          to: this.gUtils.hash16c(dad),
          arrows: 'to',
          dashes: true
        });
      });
    }

    // add doublespend blocks(if exist)
    let x = 0;
    if (this.selectedCoinInfo['spendersDict']) {
      console.log('>>>', this.selectedCoinInfo['spendersDict']);
      huY += 171;
      for (let aBDKey of this.gUtils.objKeys(this.selectedCoinInfo['spendersDict'])) {
        let spender = this.selectedCoinInfo['spendersDict'][aBDKey];
        console.log('>>> spender', spender.spendReport.susVoteRes);
        let isValid = spender.spendReport.valid;
        let maxVoters = 0;
        let maxVotes = 0;
        for (let aCoin of this.gUtils.objKeys(spender.spendReport.susVoteRes)) {
          isValid = (isValid || spender.spendReport.susVoteRes[aCoin]['valid']);
          if (maxVoters < spender.spendReport.susVoteRes[aCoin]['voters'])
            maxVoters = spender.spendReport.susVoteRes[aCoin]['voters'];
          if (maxVotes < spender.spendReport.susVoteRes[aCoin]['votes'])
            maxVotes = spender.spendReport.susVoteRes[aCoin]['votes'];
        }
        let lbl = `Block (${this.gUtils.hash16c(spender.block)}) Doc (${this.gUtils.hash16c(spender.doc)})
        \n Creation Date (${spender.blockCreationDate})
        \n Voters (${maxVoters})
        \n Votes (${maxVotes})
        `;
        let bNodeId = `${spender.block}:${spender.doc}`;
        this.cTBlockNodes.push({
          id: bNodeId,
          color: isValid ? '#9f7' : '#eeeeee',
          widthConstraint: { minimum: 120 },
          heightConstraint: { minimum: 70, valign: 'top' },
          x,
          y: huY,
          label: lbl,
          title: lbl
        });

        this.cTBlockEdges.push({
          from: bNodeId,
          to: this.gUtils.hash16c(docId),
          arrows: 'to',
          dashes: true
        });
        x += 151;

      }
    }


    setTimeout(() => {
      this.refreshCointraceGraph();
    }, 100);

  }

  public refreshCointraceGraph() {
    let nodes = new DataSet(this.cTBlockNodes);
    let edges = new DataSet(this.cTBlockEdges);

    let coinTraceData = {
      nodes,
      edges
    };

    let options = {
      width: '800px',
      height: '800px',
      edges: {
        font: {
          size: 12,
          strokeWidth: 4
        },
        widthConstraint: {
          maximum: 90
        }
      },
      nodes: {
        shape: 'box',
        margin: 5,
        widthConstraint: {
          maximum: 300
        }
      },

      physics: {
        enabled: false
      }
    };

    new Network(document.getElementById('dagCoinId'), coinTraceData, options);
  }

  public dblclickedBlockShortHash(hash) {
    console.log(`hash}--${hash}`);
    this.dblclickedBlockShortHashParent.next({ bShortHash: hash });
  }


  public selectedDoc;
  public selectedDocInfo;
  public getDocInfo() {
    this.coinTraceService.getDocInfo({ selectedDoc: this.selectedDoc }).subscribe(res => {
      this.selectedDocInfo = res;
      console.log('this.selectedDocInfo', this.selectedDocInfo);
      this.dspCoinGraph();
    });
  }

}
