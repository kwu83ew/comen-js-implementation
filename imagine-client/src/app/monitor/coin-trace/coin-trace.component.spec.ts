import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinTraceComponent } from './coin-trace.component';

describe('CoinTraceComponent', () => {
  let component: CoinTraceComponent;
  let fixture: ComponentFixture<CoinTraceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinTraceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinTraceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
