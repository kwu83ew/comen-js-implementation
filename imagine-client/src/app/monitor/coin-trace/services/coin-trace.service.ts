import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class CoinTraceService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }

  public traceCoinInfo(args) {
    return this.http.put(this.url + '/api/tobserver/traceCoinInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }

  public getDocInfo(args) {
    return this.http.put(this.url + '/api/tobserver/getDocInfo', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
}
