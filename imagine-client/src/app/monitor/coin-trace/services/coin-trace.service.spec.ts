import { TestBed } from '@angular/core/testing';

import { CoinTraceService } from './coin-trace.service';

describe('CoinTraceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoinTraceService = TestBed.get(CoinTraceService);
    expect(service).toBeTruthy();
  });
});
