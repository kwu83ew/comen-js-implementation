import { TestBed } from '@angular/core/testing';

import { TobserverService } from './tobserver.service';

describe('TobserverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TobserverService = TestBed.get(TobserverService);
    expect(service).toBeTruthy();
  });
});
