import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsObserverComponent } from './transactions-observer.component';

describe('TransactionsObserverComponent', () => {
  let component: TransactionsObserverComponent;
  let fixture: ComponentFixture<TransactionsObserverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsObserverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsObserverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
