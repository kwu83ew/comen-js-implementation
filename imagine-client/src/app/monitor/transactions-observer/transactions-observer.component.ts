import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GUtils } from 'src/app/services/utils';
import { TobserverService } from './services/tobserver.service';

@Component({
  selector: 'transactions-observer',
  templateUrl: './transactions-observer.component.html',
  styleUrls: ['./transactions-observer.component.css']
})
export class TransactionsObserverComponent implements OnInit {

  @Output() traceCoinInfoParent = new EventEmitter<any>();
  @Output() dblclickedBlockShortHashParent = new EventEmitter<any>();

  constructor(
    public gUtils: GUtils,
    private tObserverService: TobserverService,
  ) { }

  ngOnInit() {
    this.selectedOrderSet = 'orderSet1';
    this.getRejectedTrxsList();
    this.getSusTrxsList();
  }

  public rejList = [];
  getRejectedTrxsList() {
    this.tObserverService.getRejectedTrxsList({}).subscribe(res => {
      this.rejList = res.records;
    });
  }

  public susTrxList = [];
  getSusTrxsList() {
    this.tObserverService.getSusTrxsList({ selectedOrderSet: this.selectedOrderSet }).subscribe(res => {
      this.susTrxList = res.records;
    });
  }

  public selectedOrderSet = 'orderSet1';
  public slctdSusOrder(orderSet) {
    this.selectedOrderSet = orderSet;
    console.log('this.selectedOrderSet', this.selectedOrderSet);
    this.getSusTrxsList();
  }

  siblTraceCoinInfo(coin) {
    console.log('traceCoinInfo', coin);
    this.traceCoinInfoParent.next({ coin, doRefresh: true });
  }

  public dblclickedSpenderBlock(hash) {
    console.log(`hash}--${hash}`);
    this.dblclickedBlockShortHashParent.next({ bShortHash: hash });
  }

  public dblclickedLoggerBlock(hash) {
    console.log(`hash}--${hash}`);
    this.dblclickedBlockShortHashParent.next({ bShortHash: hash });
  }

}
