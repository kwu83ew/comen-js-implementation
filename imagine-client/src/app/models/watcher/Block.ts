export class Block {
  blockHash: string;
  ancestors: string[];
  body: string;
  creatinDate: string;
  receiveDate: string;
  confirmDate: string;
}
