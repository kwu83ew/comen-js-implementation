import { Component, OnInit } from '@angular/core';
import { GUtils } from '../services/utils';
import { CookieService } from '../services/cookie.service';
import { ContractsService } from './services/contracts.service';

@Component({
  selector: 'im-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.css']
})
export class ContractsComponent implements OnInit {

  pledgeLoReNotiMsg;
  PLRBundles; // proposal loan request
  signeMsgNotiMsg;
  PPTBundles;  // proposal pledge transaction
  myOnChains;
  onChainNotiMsg;

  constructor(
    public gUtils: GUtils,
    public cookie: CookieService,
    private contractsService: ContractsService,
  ) { }

  ngOnInit() {

    this.pledgeLoReNotiMsg = '';
    this.PLRBundles = [];
    this.signeMsgNotiMsg = '';
    this.PPTBundles = [];


    this.getPLRBundles();
    this.getPPTBundles();
    this.getMyOnChains();
  }


  public doAction(args) {
    this.onChainNotiMsg = '';
    this.contractsService.doAction(args).subscribe(res => {
      console.log(res);
      if (res.msg)
        this.onChainNotiMsg = res.msg;
    });
  }

  public getPLRBundles() {
    this.contractsService.getPLRBundles({}).subscribe(res => {
      console.log(res);
      this.PLRBundles = res;
    });
  }

  public getPPTBundles() {
    this.contractsService.getPPTBundles({}).subscribe(res => {
      // console.log(res);
      this.PPTBundles = res;
    });
  }
  public getMyOnChains() {
    this.contractsService.getMyOnChains({}).subscribe(res => {
      console.log('getMyOnChains', res);
      this.myOnChains = res;
    });
  }

  public deletePLR(tdId) {
    console.log('deletePLR', tdId);
    this.contractsService.deletePLR({ tdId }).subscribe(res => {
      this.getPLRBundles();
    });
  }

  public signPLR(tdId) {
    console.log('signPLR', tdId);
    this.contractsService.signPLR({ tdId }).subscribe(res => {
      this.getPLRBundles();
      this.getPPTBundles();
      this.pledgeLoReNotiMsg = res.msg;
    });
  }

  public deleteBundle(tdId) {
    console.log('deleteBundle', tdId);
    this.contractsService.deleteBundle({ tdId }).subscribe(res => {
      this.getPPTBundles();
    });
  }

  public pushPPTToSendingQ(tdId) {
    console.log('pushPPTToSendingQ', tdId);
    this.contractsService.pushPPTToSendingQ({ tdId }).subscribe(res => {
      this.getPPTBundles();
      console.log('pushPPTToSendingQ', res);
      if (res.msg)
        this.signeMsgNotiMsg = res.msg;
    });
  }

  public brcstProposal() {
    this.signeMsgNotiMsg = '';
    this.gUtils.flushBufferedDocs({
      callback: this.callback_brcstProposal.bind(this)
    });
  }
  public callback_brcstProposal() {
    let res = this.gUtils.calcLoanRepaymentsRes;
    if (res.msg) {
      this.signeMsgNotiMsg = res.msg;
      return;
    }
  }
}
