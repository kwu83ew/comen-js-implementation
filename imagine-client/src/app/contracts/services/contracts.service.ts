import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppError } from 'src/app/errors/app-error';

@Injectable({
  providedIn: 'root'
})
export class ContractsService {

  http;
  url;

  constructor(private httpService: HttpService) {
    this.http = this.httpService.http;
    this.url = this.httpService.url;
  }
  
  public doAction(args) {
    return this.http.put(this.url + '/api/contracts/doAction', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public getPLRBundles(args) {
    return this.http.put(this.url + '/api/contracts/getPLRBundles', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  
  public getPPTBundles(args) {
    return this.http.put(this.url + '/api/contracts/getPPTBundles', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  
  public getMyOnChains(args) {
    return this.http.put(this.url + '/api/contracts/getMyOnChains', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public deletePLR(args) {
    return this.http.put(this.url + '/api/contracts/deletePLR', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  
  public signPLR(args) {
    return this.http.put(this.url + '/api/contracts/signPLR', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public deleteBundle(args) {
    return this.http.put(this.url + '/api/contracts/deleteBundle', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
  public pushPPTToSendingQ(args) {
    return this.http.put(this.url + '/api/contracts/pushPPTToSendingQ', args).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        return throwError(new AppError());
      })
    );
  }
  
}
